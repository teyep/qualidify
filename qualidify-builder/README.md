# qualidify-builder

## Introduction

The qualidify-builder image is a container image that builds the Qualidify QMS in a headless Docker environment.

## Preparation
On the host that will run the container, build the image from within the folder containing the file Dockerfile:
```bash
docker build -t qualidify-builder .
```
Create a volume that will contain the data:
```bash
docker volume create qualidifybuild-data
```
## Running the container

The container is run interactively, with arguments that are passed directly to Maven:
```bash
docker run --rm -it --mount source=qualidifybuild-data,target=/mnt qualidify-builder clean install
```
The first time this script runs, a new SSH keypair is generated and stored on the data volume, after which the public key is returned. Enter this public key in the Bitbucket settings to grant the container access to the repository, then run the build again.

## Using a custom SSH key
If the use of a previously created SSH key is wanted, this can be accomplished as follows:
Start a shell on a new instance of the container:
```bash
$ docker run --rm -it --mount source=qualidifybuild-data,target=/mnt --entrypoint=/bin/bash qualidify-builder
```
In another terminal, get the ID of the running container:
```bash
$ docker ps -a
```
Copy the private and public key to the volume (replace FILENAME and CONTAINER_ID with the appropriate values):
```bash
$ docker cp FILENAME CONTAINER_ID:/mnt/.ssh/FILENAME
```

## Environment variables
Two variables can be passed to the docker container:

NAMESPACE: An identifier that may be used to separate builds. This allows multiple users to build in overlapping time frames, using the same data volume. The project is checked out using git-clone once for every namespace, as soon as the namespace is used for the first time. When no namespace is given, the value 'default' is used.
BRANCH: The remote branch that is checked out before the build is initiated. By default, branch 'master' is used.

Example:
```bash
docker run --rm -it --mount source=qualidifybuild-data,target=/mnt -e BRANCH=master -e NAMESPACE=default qualidify-builder clean install -De2e
```

## Example: Running over SSH

```bash
ssh user@buildserver docker run --rm -it --mount source=qualidifybuild-data,target=/mnt -e BRANCH=master -e NAMESPACE=default qualidify-builder clean install
```

