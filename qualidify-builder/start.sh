#!/bin/bash

# Usable environment variables:
# - NAMESPACE
# - BRANCH

if [ -z $NAMESPACE ]
then
	NAMESPACE="default"
fi

NAMESPACE_PATH=/mnt/sources/${NAMESPACE}
PROJECT_PATH=${NAMESPACE_PATH}/qualidify
REMOTE_HOST=bitbucket.org
REMOTE_LOCATION=git@${REMOTE_HOST}:teyep/qualidify.git
SSH_KEYS_PATH=/mnt/.ssh
DEFAULT_BRANCH=master
# Update the path to include the node version fetched by Maven
export PATH=$PATH:~/.vaadin/node:~/.vaadin/node/node_modules/npm/bin


create_folders() {
	for arg in $@
	do
		if [ ! -d $arg ]
		then
			mkdir $arg
		fi
	done
}


# Create the namespace folder if it's used for the first time
if [ ! -d $NAMESPACE_PATH ]
then
	mkdir -p $NAMESPACE_PATH
fi

# Link folders to the current namespace
ln -s $NAMESPACE_PATH/.java /root/.java
ln -s $NAMESPACE_PATH/.m2 /root/.m2
ln -s $NAMESPACE_PATH/.npm /root/.npm
ln -s $NAMESPACE_PATH/.vaadin /root/.vaadin
create_folders $NAMESPACE_PATH/.java $NAMESPACE_PATH/.m2 $NAMESPACE_PATH/.npm $NAMESPACE_PATH/.vaadin

for arg in $@
do
	# Start the virtual framebuffer if necessary
	if [ $arg = "-De2e" ]
	then
		Xvfb :1 -screen 0 1024x768x24 &
		export DISPLAY=:1
	fi
done


# Create a keypair if necessary
if [ ! -d /mnt/.ssh ]
then
	mkdir $SSH_KEYS_PATH
	ssh-keygen -t rsa -b 2048 -f $SSH_KEYS_PATH/id_rsa -N ""
	# Trust the first key we get
	ssh-keyscan -H $REMOTE_HOST >> /mnt/.ssh/known_hosts
	echo "A new keypair has been created. Upload the key and run this container again."
	cat /mnt/.ssh/id_rsa.pub
	exit 1
fi


echo "Building for branch ${BRANCH} in namespace ${NAMESPACE}"

# Clone the repository if necessary
if [ ! -d $PROJECT_PATH ]
then
	cd $NAMESPACE_PATH
	git clone $REMOTE_LOCATION
fi

# Build the project

cd $PROJECT_PATH

git clean -df
git fetch
if [ -z $BRANCH ]
then
	git checkout $BRANCH
else
	git checkout $DEFAULT_BRANCH
fi


git pull

mvn $@

