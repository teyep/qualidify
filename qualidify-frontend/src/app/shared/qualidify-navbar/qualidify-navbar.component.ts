import {Component, Input, OnInit} from '@angular/core';
import {IconProp} from "@fortawesome/fontawesome-svg-core";

export interface QualidifyNavbarItem {
  caption: string
  routerLink: string
  icon: IconProp
}

@Component({
  selector: 'app-qualidify-navbar',
  templateUrl: './qualidify-navbar.component.html',
  styleUrls: ['./qualidify-navbar.component.less']
})
export class QualidifyNavbarComponent implements OnInit {

  @Input()
  items: Array<QualidifyNavbarItem> = []

  constructor() { }

  ngOnInit(): void {
  }

}
