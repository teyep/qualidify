import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualidifyNavbarComponent } from './qualidify-navbar.component';

describe('QualidifyNavbarComponent', () => {
  let component: QualidifyNavbarComponent;
  let fixture: ComponentFixture<QualidifyNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualidifyNavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QualidifyNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
