import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QualidifyNavbarComponent } from './qualidify-navbar/qualidify-navbar.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { RouterModule } from "@angular/router";
import { QualidifyReadonlyGridComponent } from './qualidify-readonly-grid/qualidify-readonly-grid.component';
import { AgGridModule } from 'ag-grid-angular';
import { QualidifyServiceReadonlyGridComponent } from './qualidify-service-readonly-grid/qualidify-service-readonly-grid.component';
import { QualidifyPaginationComponent } from './qualidify-pagination/qualidify-pagination.component';
import { QualidifyAccountNavbarComponent } from './qualidify-account-navbar/qualidify-account-navbar.component';
import { QualidifyGridActionsCellComponent } from './qualidify-grid-actions-cell/qualidify-grid-actions-cell.component';

// Module for shared view-related components

@NgModule({
  declarations: [
    QualidifyNavbarComponent,
    QualidifyReadonlyGridComponent,
    QualidifyServiceReadonlyGridComponent,
    QualidifyPaginationComponent,
    QualidifyAccountNavbarComponent,
    QualidifyGridActionsCellComponent
  ],
  exports: [
    QualidifyNavbarComponent,
    QualidifyReadonlyGridComponent,
    QualidifyServiceReadonlyGridComponent,
    QualidifyAccountNavbarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    AgGridModule
  ]
})
export class SharedModule { }
