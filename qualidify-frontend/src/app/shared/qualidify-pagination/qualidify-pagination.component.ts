import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-qualidify-pagination',
  templateUrl: './qualidify-pagination.component.html',
  styleUrls: ['./qualidify-pagination.component.less']
})
export class QualidifyPaginationComponent {
  @Input()
  currentPage: number = 1

  @Input()
  numPages: number = 1

  @Input()
  navAction?: (page: number) => any = undefined

  private callbackPagenum(): void {
    if(this.navAction) {
      this.navAction(this.currentPage)
    }
  }

  selectPage(pageNum: number): void {
    this.currentPage = pageNum
    this.callbackPagenum()
  }

  previous(): void {
    if(this.currentPage > 1) {
      this.currentPage--
      this.callbackPagenum()
    }

  }

  next(): void {
    if(this.currentPage < this.numPages) {
      this.currentPage++
      this.callbackPagenum()
    }
  }
}
