import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualidifyPaginationComponent } from './qualidify-pagination.component';

describe('QualidifyPaginationComponent', () => {
  let component: QualidifyPaginationComponent;
  let fixture: ComponentFixture<QualidifyPaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualidifyPaginationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QualidifyPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
