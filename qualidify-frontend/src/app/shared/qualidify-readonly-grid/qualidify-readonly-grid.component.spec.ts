import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualidifyReadonlyGridComponent } from './qualidify-readonly-grid.component';

describe('QualidifyReadonlyGridComponent', () => {
  let component: QualidifyReadonlyGridComponent;
  let fixture: ComponentFixture<QualidifyReadonlyGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualidifyReadonlyGridComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QualidifyReadonlyGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
