import {Component, Inject, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ColDef, GridOptions} from 'ag-grid-community'
import {metadataServiceIToken} from "../../views/metadata/metadata.module";
import {MetadataService} from "../../views/metadata/metadata-service";
import {DtoDef} from "../../core/interfaces/DtoDef";
import {ColumnDef} from "../../core/interfaces/ColumnDef";
import {ActionInfo} from "../../core/interfaces/ActionInfo";
import {forkJoin} from "rxjs";
import {QualidifyGridActionsCellComponent} from "../qualidify-grid-actions-cell/qualidify-grid-actions-cell.component";
import {DtoActionHandler} from "../../core/interfaces/DtoActionHandler";

export interface Dictionary {
  [key: string]: any
}

@Component({
  selector: 'app-qualidify-readonly-grid',
  templateUrl: './qualidify-readonly-grid.component.html',
  styleUrls: ['./qualidify-readonly-grid.component.less']
})
export class QualidifyReadonlyGridComponent implements OnInit, OnChanges {

  @Input()
  mnemonic: String | undefined = undefined

  @Input()
  inputData: Array<any> = []

  @Input()
  actionHandler: DtoActionHandler | undefined  = undefined

  @Input()
  refreshCallback?: () => void | null

  gridOptions: GridOptions

  rowData: Array<any> = []
  columnDefs: ColDef[] = []

  // We need to keep this, in case we want to fetch row data again
  actions: Array<ActionInfo> = []

  private createColumns(dtoDefinition: DtoDef): void {
    this.columnDefs = dtoDefinition.columns
      .filter((column: ColumnDef) => column.viewable && (!column.hidden))
      .map((column: ColumnDef) => {
        return {
          field: column.name,
          headerName: column.description
        }
      })
    if((this.actions.length > 0) && (this.actionHandler)) {
      this.columnDefs.push({
        field: "actions",
        headerName: "Actions",
        cellRenderer: QualidifyGridActionsCellComponent,
        cellRendererParams: {
          actionHandler: this.actionHandler,
          refreshCallback: this.refreshCallback
        }
      })
    }
  }

  private loadDtoDefinition(): void {
    if(this.mnemonic) {

      forkJoin({
        dtodef: this.metadataService.dtoDefinitionByMnemonic(this.mnemonic as string),
        actions: this.metadataService.actionsByMnemonic(this.mnemonic as string),
        batchactions: this.metadataService.batchActionsByMnemonic(this.mnemonic as string)
      }).subscribe(data => {
        this.actions = data.actions

        this.createColumns(data.dtodef)
        this.createRowData()
      })
    }
    else {
      console.log("No mnemonic known")
    }
  }


  constructor(
    @Inject(metadataServiceIToken) private metadataService: MetadataService
  ) {
    this.gridOptions = {
      rowHeight: 48,
      onGridReady: () => {
        this.gridOptions.api?.sizeColumnsToFit()
      }
    }
  }

  ngOnInit(): void {
  }

  createRowData(): void {
    if(this.columnDefs.length == 0) return
    this.rowData = this.inputData.map((row: any) => {
      let gridRow: Dictionary = {}
      // Note: We added one column definition here, might want to reconsider.
      this.columnDefs.forEach((column: ColDef) => {
        // Do this for all fields we received, not for the column we added!
        if(column.field != "actions") {
          gridRow[column.field!] = (row as Dictionary)[column.field!]
        }
      })
      // Add the column if we need it
      if(this.actionHandler && this.actions.length > 0) {
        gridRow["actions"] = this.actions
      }
      gridRow["rawData"] = row
      return gridRow
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['mnemonic']) {
      this.loadDtoDefinition()
    }
    this.createRowData()
  }
}
