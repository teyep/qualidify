import {Component, Inject} from '@angular/core';
import {QualidifyNavbarItem} from "../qualidify-navbar/qualidify-navbar.component";
import {faKey} from "@fortawesome/free-solid-svg-icons";
import {SessionService, sessionServiceInjectionToken} from "../../core/services/session-service";

@Component({
  selector: 'app-qualidify-account-navbar',
  templateUrl: './qualidify-account-navbar.component.html',
  styleUrls: ['./qualidify-account-navbar.component.less']
})
export class QualidifyAccountNavbarComponent {
  keyIcon = faKey


  loginState: boolean = false


  loggedOutItems: Array<QualidifyNavbarItem> = [
    {
      caption: "Login",
      routerLink: "/login",
      icon: faKey
    }
  ]

  constructor(
    @Inject(sessionServiceInjectionToken) private sessionService: SessionService
  ) {
    sessionService.isLoggedIn().subscribe(data => this.handleLoginState(data))
    sessionService.getLoginEventEmitter().subscribe(data => this.handleLoginState(data))
  }

  logout(): void {
    this.sessionService.logout()
    console.log("Logout")
  }

  private handleLoginState(newState: boolean) {
    this.loginState = newState
  }
}
