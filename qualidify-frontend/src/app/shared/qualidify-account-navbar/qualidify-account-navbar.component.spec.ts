import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualidifyAccountNavbarComponent } from './qualidify-account-navbar.component';

describe('QualidifyAccountNavbarComponent', () => {
  let component: QualidifyAccountNavbarComponent;
  let fixture: ComponentFixture<QualidifyAccountNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualidifyAccountNavbarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QualidifyAccountNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
