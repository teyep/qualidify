import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualidifyGridActionsCellComponent } from './qualidify-grid-actions-cell.component';

describe('QualidifyGridActionsCellComponent', () => {
  let component: QualidifyGridActionsCellComponent;
  let fixture: ComponentFixture<QualidifyGridActionsCellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualidifyGridActionsCellComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QualidifyGridActionsCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
