import { Component } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";
import {ICellRendererParams} from "ag-grid-community";
import {ActionInfo} from "../../core/interfaces/ActionInfo";
import {DtoActionHandler} from "../../core/interfaces/DtoActionHandler";

export interface ExtendedActionInfo extends ActionInfo {
  enabled: boolean
}

/**
 * This component draws buttons for starting actions in a grid. Use it as cell renderer in ag-grid.
 */
@Component({
  selector: 'app-qualidify-grid-actions-cell',
  templateUrl: './qualidify-grid-actions-cell.component.html',
  styleUrls: ['./qualidify-grid-actions-cell.component.less']
})
export class QualidifyGridActionsCellComponent implements ICellRendererAngularComp {

  // Received from parent
  actions: Array<ActionInfo> = []

  // Created after fetching enabled state
  statefulActions: Array<ExtendedActionInfo> = []

  row: any = {}

  actionHandler: DtoActionHandler | undefined = undefined

  refreshCallback?: () => void | null

  loadFromParams(params: ICellRendererParams): void {
    this.row = params.data
    if((params as any).actionHandler) {
      this.actions = (params.data as any).actions
      this.actionHandler = (params as any).actionHandler
      this.refreshCallback = (params as any).refreshCallback
      this.statefulActions = []
      this.getEnabledState()
    }
  }

  agInit(params: ICellRendererParams): void {
    this.loadFromParams(params)
  }

  refresh(params: ICellRendererParams<any>): boolean {
    this.loadFromParams(params)
    return true;
  }

  private getEnabledState(): void {
    if(this.actions === undefined) return
    this.actions.forEach(action => {
      if(this.actionHandler != undefined) {
        this.actionHandler.actionIsEnabled(this.row, action.action).subscribe(enabledState => {
          this.statefulActions.push(this.createActionWithState(action, enabledState))
        })
      }
    })
  }

  performAction(dto: any, action: String): void {
    if(this.actionHandler !== undefined) {
      this.actionHandler.handleAction(dto, action).subscribe(data => {
        if(this.refreshCallback) {
          this.refreshCallback()
        }
      })
    }
  }

  private createActionWithState(action: ActionInfo, enabled: boolean): ExtendedActionInfo {
    return {
      label: action.label,
      action: action.action,
      position: action.position,
      order: action.order,
      enabled: enabled
    }
  }
}
