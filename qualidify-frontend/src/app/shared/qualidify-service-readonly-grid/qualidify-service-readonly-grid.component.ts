import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {GenericDataService} from "../../core/services/generic-data-service";
import {RestQuery} from "../../core/interfaces/RestQuery";
import {DtoActionHandler} from "../../core/interfaces/DtoActionHandler";

@Component({
  selector: 'app-qualidify-service-readonly-grid',
  templateUrl: './qualidify-service-readonly-grid.component.html',
  styleUrls: ['./qualidify-service-readonly-grid.component.less']
})
export class QualidifyServiceReadonlyGridComponent implements OnInit, OnChanges {
  @Input()
  mnemonic: string = ""

  @Input()
  dataService: GenericDataService | undefined = undefined

  @Input()
  actionHandler: DtoActionHandler | undefined = undefined

  restQuery: RestQuery = {
    offset: 0,
    size: 10,
    criteria: [],
    order: []
  }

  data: Array<any> = []
  numPages: number = 1
  currentPage: number = 1

  selectPage?: (pageNum: number) => any = undefined

  // We may override this, if we want something else to happen when performing an action
  @Input()
  refreshCallback: () => void = () => {
    this.loadData()
  }

  ngOnInit(): void {
    this.selectPage = (pageNum: number) => {
      this.currentPage = pageNum
      this.restQuery.offset = (pageNum - 1) * this.restQuery.size
      this.loadData()
    }
  }
  private loadData(): void {
    if((this.mnemonic != "") && (this.dataService != undefined)) {
      this.dataService.genericFind(this.restQuery).subscribe(data => this.data = data)
      this.dataService.count(this.restQuery).subscribe(data => {
        this.numPages = Math.ceil(data.count / this.restQuery.size)
      })
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.loadData()
  }
}
