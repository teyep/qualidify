import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualidifyServiceReadonlyGridComponent } from './qualidify-service-readonly-grid.component';

describe('QualidifyServiceReadonlyGridComponent', () => {
  let component: QualidifyServiceReadonlyGridComponent;
  let fixture: ComponentFixture<QualidifyServiceReadonlyGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualidifyServiceReadonlyGridComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QualidifyServiceReadonlyGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
