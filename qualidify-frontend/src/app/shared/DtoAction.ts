export class DtoAction {
  name: string
  enabled: boolean
  visible: boolean
  label: string | null

  constructor(
    name: string,
    enabled: boolean,
    visible: boolean,
    label?: string
  ) {
    this.name = name
    this.enabled = enabled
    this.visible = visible
    if (label == null) { this.label = name } else { this.label = label }
  }


}
