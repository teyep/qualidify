import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProjectsModule} from './projects/projects.module';
import {Route, RouterModule} from "@angular/router";
import {HomeComponent} from './home/home.component';
import {MetadataModule} from './metadata/metadata.module';
import {LoginComponent} from './login/login.component';
import {ReactiveFormsModule} from "@angular/forms";
import {TasksModule} from "./tasks/tasks.module";
import {ReportsModule} from "./reports/reports.module";
import {ConfigurationModule} from "./configuration/configuration.module";
import {taskServiceInjectionToken} from "../core/services/tasks.service";
import {TasksServiceImpl} from "../core/services/tasks.service-impl";
import {projectsServiceInjectionToken} from "../core/services/project-instance.service";
import {ProjectInstanceServiceImpl} from "../core/services/project-instance.service-impl";
import {processServiceInjectionToken} from "../core/services/process-service";
import {SharedModule} from "../shared/shared.module";
import { DataModule } from './data/data.module';
import {ProcessServiceImpl} from "../core/services/process.service-impl";

const routes: Route[] = [
  {
    path: 'projects',
    loadChildren: () => ProjectsModule,
  },
  {
    path: 'tasks',
    loadChildren: () => TasksModule
  },
  {
    path: 'reports',
    loadChildren: () => ReportsModule
  },
  {
    path: 'configuration',
    loadChildren: () => ConfigurationModule
  },
  // Generic paths
  {
    path: 'metadata',
    loadChildren: () => MetadataModule
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'data',
    loadChildren: () => DataModule
  }
]

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ProjectsModule,
    TasksModule,
    ReportsModule,
    ConfigurationModule,
    MetadataModule,
    ReactiveFormsModule,
    SharedModule,
    DataModule
  ],
  providers: [
    {
      provide: taskServiceInjectionToken,
      useClass: TasksServiceImpl,
    },
    {
      provide: projectsServiceInjectionToken,
      useClass: ProjectInstanceServiceImpl,
    },
    {
      provide: processServiceInjectionToken,
      useClass: ProcessServiceImpl,
    }
  ]
})
export class ViewsModule {
}
