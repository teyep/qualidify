import {Component, Inject, OnInit} from '@angular/core';
import {RestQueryUtil} from "../../core/utils/RestQueryUtil";
import {TaskDto} from "../../core/interfaces/TaskDto";
import {DtoAction} from "../../shared/DtoAction";
import {taskServiceInjectionToken, TasksService} from "../../core/services/tasks.service";


@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.less']
})
export class TasksComponent implements OnInit {
  tasks: TaskDto[] = []

  constructor(
    @Inject(taskServiceInjectionToken) private tasksService: TasksService,
  ) {
  }

  ngOnInit(): void {
    this.tasksService.findTasks(RestQueryUtil.RestQuery())
      .subscribe(response => {
        this.tasks = response
      })
  }

  getActions(task: TaskDto): DtoAction[] {
    return this.tasksService.getTaskActions(task)
  }

  execute(action: DtoAction, task: TaskDto) {
    let execute = this.tasksService.executeTaskAction(action, task)
    execute.subscribe(task => {
      console.log(`Executed action '${action.name}' on task '${task.name}'`)
      this.ngOnInit()
    })
  }

}
