import {Component, Inject, OnInit} from '@angular/core';
import {taskServiceInjectionToken, TasksService} from "../../../core/services/tasks.service";
import {RestQueryUtil} from "../../../core/utils/RestQueryUtil";

@Component({
  selector: 'app-form-definitions',
  templateUrl: './form-definitions.component.html',
  styleUrls: ['./form-definitions.component.less']
})
export class FormDefinitionsComponent implements OnInit {
  forms: any = []

  constructor(
    @Inject(taskServiceInjectionToken) private tasksService: TasksService
  ) {
  }

  ngOnInit(): void {
    this.tasksService.findForms(RestQueryUtil.RestQuery())
      .subscribe(response => {
        this.forms = response
      })
  }


}
