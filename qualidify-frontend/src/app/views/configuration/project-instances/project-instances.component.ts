import {Component, Inject} from '@angular/core';
import {ProjectInstanceService, projectsServiceInjectionToken} from "../../../core/services/project-instance.service";

@Component({
  selector: 'app-project-instances',
  templateUrl: './project-instances.component.html',
  styleUrls: ['./project-instances.component.less']
})
export class ProjectInstancesComponent {

  constructor(
    @Inject(projectsServiceInjectionToken) public projectService: ProjectInstanceService
  ) {
  }
}
