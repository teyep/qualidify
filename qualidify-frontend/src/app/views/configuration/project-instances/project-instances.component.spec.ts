import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectInstancesComponent } from './project-instances.component';

describe('ProjectInstancesComponent', () => {
  let component: ProjectInstancesComponent;
  let fixture: ComponentFixture<ProjectInstancesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectInstancesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectInstancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
