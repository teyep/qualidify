import {Component, Inject, OnInit} from '@angular/core';
import {taskServiceInjectionToken, TasksService} from "../../../core/services/tasks.service";
import {TaskDto} from "../../../core/interfaces/TaskDto";
import {DtoAction} from "../../../shared/DtoAction";
import {RestQueryUtil} from "../../../core/utils/RestQueryUtil";

@Component({
  selector: 'app-case-tasks',
  templateUrl: './case-tasks.component.html',
  styleUrls: ['./case-tasks.component.less']
})
export class CaseTasksComponent implements OnInit {
  tasks: any = []

  constructor(
    @Inject(taskServiceInjectionToken) private tasksService: TasksService,
  ) {
  }

  ngOnInit(): void {
    this.tasksService.findTasks(RestQueryUtil.RestQuery())
      .subscribe(response => {
        this.tasks = response
      })
  }

  getActions(task: TaskDto): DtoAction[] {
    return this.tasksService.getTaskActions(task)
  }

  execute(action: DtoAction, task: TaskDto) {
    let execute = this.tasksService.executeTaskAction(action, task)
    execute.subscribe( response => {
      console.log(`Executed action '${action.name}' on task '${task.name}'`)
      this.ngOnInit()
    })
  }

}
