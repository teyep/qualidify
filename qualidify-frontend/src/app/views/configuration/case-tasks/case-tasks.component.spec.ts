import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseTasksComponent } from './case-tasks.component';

describe('CaseTasksComponent', () => {
  let component: CaseTasksComponent;
  let fixture: ComponentFixture<CaseTasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaseTasksComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CaseTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
