import {Component, Inject} from '@angular/core';
import {ProcessService, processServiceInjectionToken} from "../../../core/services/process-service";
import {PlanItemService, planItemServiceInjectionToken} from "../../../core/services/plan-item-service";

@Component({
  selector: 'app-plan-items',
  templateUrl: './plan-items.component.html',
  styleUrls: ['./plan-items.component.less']
})
export class PlanItemsComponent {

  constructor(
    @Inject(processServiceInjectionToken) private processService: ProcessService,
    @Inject(planItemServiceInjectionToken) public dataService: PlanItemService
  ) {
  }
}
