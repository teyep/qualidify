import {Component, Inject} from '@angular/core';
import {Router} from "@angular/router";
import {
  ProjectTemplateService,
  projectTemplateServiceInjectionToken
} from "../../../core/services/project-template.service";

@Component({
  selector: 'app-project-templates',
  templateUrl: './project-templates.component.html',
  styleUrls: ['./project-templates.component.less']
})
export class ProjectTemplatesComponent {

  constructor(
    @Inject(projectTemplateServiceInjectionToken) public projectTemplateService: ProjectTemplateService,
    private router: Router
  ) {
  }

  templateStarted: () => void = () => {
    this.router.navigate(["/projects"])
  }
}
