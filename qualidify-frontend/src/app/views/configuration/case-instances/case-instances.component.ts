import {Component, Inject, OnInit} from '@angular/core';
import {ProcessService, processServiceInjectionToken} from "../../../core/services/process-service";
import {CaseInstanceDto} from "../../../core/interfaces/CaseInstanceDto";
import {DtoAction} from "../../../shared/DtoAction";
import {RestQueryUtil} from "../../../core/utils/RestQueryUtil";

@Component({
  selector: 'app-case-instances',
  templateUrl: './case-instances.component.html',
  styleUrls: ['./case-instances.component.less']
})
export class CaseInstancesComponent implements OnInit {
  cases: any = []

  constructor(
    @Inject(processServiceInjectionToken) private processService: ProcessService
  ) {
  }

  ngOnInit(): void {
    this.processService.findCaseInstances(RestQueryUtil.RestQuery())
      .subscribe(response => {
        this.cases = response
      })
  }

  getActions(caseInstance: CaseInstanceDto): DtoAction[] {
    return this.processService.getCaseInstanceActions(caseInstance)
  }

  execute(action: DtoAction, caseInstance: CaseInstanceDto) {
    let execute = this.processService.executeCaseInstanceAction(action, caseInstance)
    execute.subscribe( response => {
      console.log(`Executed action '${action.name}' on case instance '${caseInstance.name}'`)
    })
  }


}
