import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseInstancesComponent } from './case-instances.component';

describe('CaseInstancesComponent', () => {
  let component: CaseInstancesComponent;
  let fixture: ComponentFixture<CaseInstancesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaseInstancesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CaseInstancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
