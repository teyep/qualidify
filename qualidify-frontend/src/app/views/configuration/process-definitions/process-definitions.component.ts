import {Component, Inject, OnInit} from '@angular/core';
import {ProcessService, processServiceInjectionToken} from "../../../core/services/process-service";
import {ProcessDefinitionDto} from "../../../core/interfaces/ProcessDefinitionDto";
import {DtoAction} from "../../../shared/DtoAction";
import {RestQueryUtil} from "../../../core/utils/RestQueryUtil";

@Component({
  selector: 'app-process-definitions',
  templateUrl: './process-definitions.component.html',
  styleUrls: ['./process-definitions.component.less']
})
export class ProcessDefinitionsComponent implements OnInit {
  processes : any = []

  constructor(
    @Inject(processServiceInjectionToken) private processService: ProcessService
  ) {
  }

  ngOnInit() {
    this.processService.findProcessDefinitions(RestQueryUtil.RestQuery())
      .subscribe(response => {
        this.processes = response
      })
  }

  getActions(dto: ProcessDefinitionDto): DtoAction[] {
    return this.processService.getProcessDefinitionActions(dto)
  }

  execute(action: DtoAction, dto: ProcessDefinitionDto) {
    let execute = this.processService.executeProcessDefinitionAction(action, dto)
    execute.subscribe(response => {
      console.log(`Executed action '${action.name}' on process definition '${dto.name}'`)
      this.ngOnInit()
    })
  }

}
