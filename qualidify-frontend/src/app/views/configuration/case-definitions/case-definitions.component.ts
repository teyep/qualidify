import {Component, Inject, OnInit} from '@angular/core';
import {ProcessService, processServiceInjectionToken} from "../../../core/services/process-service";
import {RestQueryUtil} from "../../../core/utils/RestQueryUtil";

@Component({
  selector: 'app-case-definitions',
  templateUrl: './case-definitions.component.html',
  styleUrls: ['./case-definitions.component.less']
})
export class CaseDefinitionsComponent implements OnInit {
  cases: any = []

  constructor(
    @Inject(processServiceInjectionToken) private processService: ProcessService
  ) {
  }

  ngOnInit(): void {
    this.processService.findCaseDefinitions(RestQueryUtil.RestQuery())
      .subscribe(response => {
        this.cases = response
      })
  }

}
