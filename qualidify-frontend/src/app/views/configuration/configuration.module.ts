import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {ConfigurationComponent} from './configuration.component';
import {ProjectTemplatesComponent} from "./project-templates/project-templates.component";
import {ProjectInstancesComponent} from './project-instances/project-instances.component';
import { CaseDefinitionsComponent } from './case-definitions/case-definitions.component';
import { CaseInstancesComponent } from './case-instances/case-instances.component';
import { PlanItemsComponent } from './plan-items/plan-items.component';
import { CaseTasksComponent } from './case-tasks/case-tasks.component';
import { FormDefinitionsComponent } from './form-definitions/form-definitions.component';
import { ProcessDefinitionsComponent } from './process-definitions/process-definitions.component';
import { ProcessTasksComponent } from './process-tasks/process-tasks.component';
import {SharedModule} from "../../shared/shared.module";


const routes: Routes = [
  {path: '', component: ConfigurationComponent},
  {path: 'project-templates', component: ProjectTemplatesComponent},
  {path: 'project-instances', component: ProjectInstancesComponent},
  {path: 'case-definitions', component: CaseDefinitionsComponent},
  {path: 'case-instances', component: CaseInstancesComponent},
  {path: 'plan-items', component: PlanItemsComponent},
  {path: 'case-tasks', component: CaseTasksComponent},
  {path: 'form-definitions', component: FormDefinitionsComponent},
  {path: 'process-definitions', component: ProcessDefinitionsComponent},
  {path: 'process-tasks', component: ProcessTasksComponent},
];

@NgModule({
  declarations: [
    ConfigurationComponent,
    ProjectTemplatesComponent,
    ProjectInstancesComponent,
    CaseDefinitionsComponent,
    CaseInstancesComponent,
    PlanItemsComponent,
    CaseTasksComponent,
    FormDefinitionsComponent,
    ProcessDefinitionsComponent,
    ProcessTasksComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule
    ]
})
export class ConfigurationModule {
}
