import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessTasksComponent } from './process-tasks.component';

describe('ProcessTasksComponent', () => {
  let component: ProcessTasksComponent;
  let fixture: ComponentFixture<ProcessTasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcessTasksComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProcessTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
