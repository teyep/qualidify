import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DataViewerComponent} from "./data-viewer/data-viewer.component";
import {Route, RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";


const routes: Route[] = [
  {path: 'view/:mnemonic', component: DataViewerComponent}
]

@NgModule({
  declarations: [
    DataViewerComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class DataModule { }
