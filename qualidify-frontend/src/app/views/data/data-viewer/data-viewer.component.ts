import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {GenericDataService} from "../../../core/services/generic-data-service";
import {metadataServiceIToken} from "../../metadata/metadata.module";
import {MetadataService} from "../../metadata/metadata-service";
import {RestQuery} from "../../../core/interfaces/RestQuery";
import {Observable} from "rxjs";
import {CountResponse} from "../../../core/interfaces/CountResponse";

@Component({
  selector: 'app-data-viewer',
  templateUrl: './data-viewer.component.html',
  styleUrls: ['./data-viewer.component.less']
})
export class DataViewerComponent implements OnInit {
  mnemonic: string = ""

  dataService?: GenericDataService = undefined

  constructor(
    @Inject(ActivatedRoute) private activatedRoute: ActivatedRoute,
    @Inject(metadataServiceIToken) private metadataService: MetadataService
  ) {
  }

  createService(component: DataViewerComponent, mnemonic: string): GenericDataService {
    return {
      count(query: RestQuery): Observable<CountResponse> {
        console.log("Calling count()")
        return component.metadataService.countByMnemonic(mnemonic, query)
      }, genericFind(query: RestQuery): Observable<Array<any>> {
        console.log("Calling find()")
        return component.metadataService.findByMnemonic(mnemonic, query)
      }
    }
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.mnemonic = params['mnemonic']
    })
    this.dataService = this.createService(this, this.mnemonic as string)
  }
}
