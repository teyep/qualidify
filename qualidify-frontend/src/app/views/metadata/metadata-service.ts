import {Observable} from "rxjs";
import { DtoDef } from "src/app/core/interfaces/DtoDef";
import {CustomFieldDef} from "src/app/core/interfaces/CustomFieldDef";
import {RestQuery} from "../../core/interfaces/RestQuery";
import {CountResponse} from "../../core/interfaces/CountResponse";
import {ActionInfo} from "../../core/interfaces/ActionInfo";

/**
 * Frontend service for handling metadata
 */
export interface MetadataService {

  customFieldsForReference(reference: String): Observable<Array<CustomFieldDef>>

  dtoDefinitionByMnemonic(mnemonic: string): Observable<DtoDef>

  countByMnemonic(mnemonic: string, query: RestQuery): Observable<CountResponse>

  findByMnemonic(mnemonic: string, query: RestQuery): Observable<Array<any>>

  actionsByMnemonic(mnemonic: string): Observable<Array<ActionInfo>>

  batchActionsByMnemonic(mnemonic: string): Observable<Array<ActionInfo>>
}
