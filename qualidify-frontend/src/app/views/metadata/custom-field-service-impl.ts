import {CustomFieldService} from "./custom-field-service";
import {RestQuery} from "src/app/core/interfaces/RestQuery";
import {CountResponse} from "src/app/core/interfaces/CountResponse";
import {CustomFieldDef} from "src/app/core/interfaces/CustomFieldDef";
import {Observable} from "rxjs";
import {AbstractHttpService} from "src/app/core/services/abstract.http.service";
import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CustomFieldServiceImpl extends AbstractHttpService implements CustomFieldService {

  constructor(@Inject(HttpClient) private http: HttpClient) {
    super()
  }

  count(query: RestQuery): Observable<CountResponse> {
    return this.http.post(this.endpoint + "/count", query) as Observable<CountResponse>
  }

  find(query: RestQuery): Observable<Array<CustomFieldDef>> {
    return this.http.post(this.endpoint + "/find", query) as Observable<Array<CustomFieldDef>>
  }

  create(dto: CustomFieldDef): Observable<object> {
    return this.http.post(this.endpoint + "/create", dto) as Observable<object>
  }

  delete(dto: CustomFieldDef): Observable<object> {
    return this.http.delete(this.endpoint + "/delete", {
      headers: undefined,
      body: dto
    }) as Observable<object>
  }


  update(dto: CustomFieldDef): void {
    this.http.delete(this.endpoint + "/delete", {
      headers: undefined,
      body: dto
    })
  }
}
