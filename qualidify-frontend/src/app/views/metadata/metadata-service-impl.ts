import {AbstractHttpService} from "../../core/services/abstract.http.service";
import {MetadataService} from "./metadata-service";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {CustomFieldDef} from "src/app/core/interfaces/CustomFieldDef";
import {DtoDef} from "src/app/core/interfaces/DtoDef";
import {RestQuery} from "../../core/interfaces/RestQuery";
import {CountResponse} from "../../core/interfaces/CountResponse";
import {ActionInfo} from "../../core/interfaces/ActionInfo";


@Injectable()
export class MetadataServiceImpl extends AbstractHttpService implements MetadataService {

  constructor(@Inject(HttpClient) private http: HttpClient) {
    super()
  }

  customFieldsForReference(reference: String): Observable<Array<CustomFieldDef>> {
    return this.http.get(this.endpoint + "/metadata/customfields/" + reference) as Observable<Array<CustomFieldDef>>
  }

  dtoDefinitionByMnemonic(mnemonic: string): Observable<DtoDef> {
    return this.http.get(this.endpoint + "/metadata/dtodef/" + mnemonic) as Observable<DtoDef>
  }

  countByMnemonic(mnemonic: string, query: RestQuery): Observable<CountResponse> {
    return this.http.post(this.endpoint + "/metadata/" + mnemonic + "/count", query) as Observable<CountResponse>
  }

  findByMnemonic(mnemonic: string, query: RestQuery): Observable<Array<any>> {
    return this.http.post(this.endpoint + "/metadata/" + mnemonic + "/find", query) as Observable<Array<any>>
  }

  actionsByMnemonic(mnemonic: string): Observable<Array<ActionInfo>> {
    return this.http.get(this.endpoint + "/metadata/actions/" + mnemonic) as Observable<Array<ActionInfo>>
  }

  batchActionsByMnemonic(mnemonic: string): Observable<Array<ActionInfo>> {
    return this.http.get(this.endpoint + "/metadata/batchactions/" + mnemonic) as Observable<Array<ActionInfo>>
  }
}
