import {InjectionToken, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {MetadataServiceImpl} from "./metadata-service-impl";
import {MetadataService} from "./metadata-service";
import {Route, RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import { CustomFieldService } from './custom-field-service';
import {CustomFieldServiceImpl} from "./custom-field-service-impl";

const routes: Route[] = [
]

export const metadataServiceIToken = new InjectionToken<MetadataService>('metadata.service')
export const customFieldServiceIToken = new InjectionToken<CustomFieldService>('custom-field.service')

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  providers: [
    MetadataServiceImpl,
    {
      provide: metadataServiceIToken,
      useClass: MetadataServiceImpl
    },
    {
      provide: customFieldServiceIToken,
      useClass: CustomFieldServiceImpl
    }
  ]
})
export class MetadataModule { }
