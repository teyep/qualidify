/**
 * Interface for a service that connects to the CustomFieldRestController in the backend
 */
import {Observable} from "rxjs";
import {RestQuery} from "src/app/core/interfaces/RestQuery";
import {CountResponse} from "src/app/core/interfaces/CountResponse";
import {CustomFieldDef} from "src/app/core/interfaces/CustomFieldDef";


export interface CustomFieldService {
  count(query: RestQuery): Observable<CountResponse>

  find(query: RestQuery): Observable<Array<CustomFieldDef>>

  create(dto: CustomFieldDef): void

  update(dto: CustomFieldDef): void

  delete(dto: CustomFieldDef): void
}
