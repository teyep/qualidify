import {Component, Inject} from '@angular/core';
import {RestQueryUtil} from "../../core/utils/RestQueryUtil";
import {ProjectInstanceDto} from "../../core/interfaces/ProjectInstanceDto";
import {ProjectInstanceService, projectsServiceInjectionToken} from "../../core/services/project-instance.service";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.less']
})
export class ProjectsComponent {
  projects: ProjectInstanceDto[] = [];

  constructor(
    @Inject(projectsServiceInjectionToken) private projectService: ProjectInstanceService,
  ) {
    projectService.find(RestQueryUtil.RestQuery())
      .subscribe(response => {
        this.projects = response
      })
  }

}
