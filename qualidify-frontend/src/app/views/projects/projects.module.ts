import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProjectsComponent} from './projects.component';
import {Route, RouterModule} from "@angular/router";
import {ProjectTemplatesComponent} from "../configuration/project-templates/project-templates.component";

const routes: Route[] = [
  {path: '', component: ProjectsComponent},
  {path: 'new', component: ProjectTemplatesComponent}
]

@NgModule({
  declarations: [
    ProjectsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
})
export class ProjectsModule {
}
