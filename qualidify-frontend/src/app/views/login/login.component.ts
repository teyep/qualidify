import {Component, Inject, OnInit} from '@angular/core';
import {SessionService, sessionServiceInjectionToken} from "../../core/services/session-service";
import {Router} from "@angular/router";
import {UntypedFormControl, UntypedFormGroup, Validators} from "@angular/forms";
import {AuthorizationService, authorizationServiceInjectionToken} from "../../core/services/authorization-service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  showLoginFailure: boolean = false

  loginForm: UntypedFormGroup = new UntypedFormGroup({
    userName: new UntypedFormControl(null, Validators.required),
    password: new UntypedFormControl(null, Validators.required)
  })

  constructor(
    @Inject(Router) private router: Router,
    @Inject(authorizationServiceInjectionToken) private authorizationService: AuthorizationService,
    @Inject(sessionServiceInjectionToken) private sessionService: SessionService
  ) { }

  ngOnInit(): void {
    //
  }

  loginClicked(): boolean {
    this.sessionService.login(
      this.loginForm.value.userName,
      this.loginForm.value.password
    ).subscribe({
      next: (data) => {
        if(data) {
          this.router.navigate([''])
        }
        else {
          alert("Unexpected reply while logging in.")
        }
      },
      error: (error) => {
        console.log(error)
        this.showLoginFailure = true
      },
      complete: undefined
    })

    return false
  }

}
