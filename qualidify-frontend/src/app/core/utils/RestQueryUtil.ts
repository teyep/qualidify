import {RestQuery} from "../interfaces/RestQuery";


export class RestQueryUtil {

  static RestQuery(
    offset: number = 0,
    size: number = 30,
    criteria: any = [],
    order: any = []
  ) {
    return {
      offset: offset,
      size: size,
      criteria: criteria,
      order : order
    } as RestQuery
  }

}
