import {CaseInstanceDto} from "./CaseInstanceDto";

export interface ProjectInstanceDto {
  id: string
  name: string
  description: string
  cmmnQualityShellId: string
  cmmnQualityShellInstance: string
  cmmnInstanceId: string
  cmmnInstanceDto: CaseInstanceDto
  cmmnInstanceState: any // CaseInstanceState
  startTime: number
  fields: Map<string, string | undefined>
  variables: any
  users: Array<string>
  completable: boolean
}
