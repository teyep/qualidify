import {PlanItemDto} from "./PlanItemDto";


export interface TaskDto {
  id: string
  name: string
  taskDefinitionKey: string
  projectInstanceId: string
  cmmnInstanceId: string
  caseDefinitionId: string | null
  planItemInstanceId: string
  planItemInstance: PlanItemDto | null
  formKey: string | null
  owner: string | null
  assignee: string | null
  type: any
  stage: string | null
  isQualityShell: boolean
  cmmnQualityShellId: string
  reviewState: any
  qualityLinkId: string | null
}
