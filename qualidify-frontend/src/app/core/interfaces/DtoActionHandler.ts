import {Observable} from "rxjs";

export interface DtoActionHandler {
  /**
   * Perform an action
   * @param dto The row
   * @param action The action
   */
  handleAction(dto: any, action: String): Observable<any>

  /**
   * Determines whether an action is enabled on a specified row
   * @param dto The row
   * @param action The action
   */
  actionIsEnabled(dto: any, action: String): Observable<boolean>

}
