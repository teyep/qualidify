
enum CaseInstanceState {
  ACTIVE = "active",
  COMPLETED = "completed",
  TERMINATED = "terminated",
  FAILED = "failed",
  SUSPENDED = "suspended",
  NEW = "new",
  CLOSED = "closed"
}
