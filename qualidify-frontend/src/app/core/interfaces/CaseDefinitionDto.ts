
export interface CaseDefinitionDto {
  id: string
  name: string
  key: string
  version: string
  deploymentResourceId: string | null
  graphicalNotation: any | null
}
