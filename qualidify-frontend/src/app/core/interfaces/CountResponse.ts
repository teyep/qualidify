/**
 * Controller response containing the number of items found
 */
export interface CountResponse {
  count: number
  lastEvent: number
}
