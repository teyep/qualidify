/**
 * Front-end version of the RestQuery object that is passed to find and count in the rest controllers.
 */
export interface RestQuery {
  offset: number
  size: number
  criteria: Array<any>
  order: Array<any>
}

