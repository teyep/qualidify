export interface ActionInfo {
  label: string,
  action: string,
  position: string,
  order: string
}
