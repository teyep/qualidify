import {DtoDef} from "./DtoDef";

export interface ColumnDef {
  name: string
  description: string
  dataType: any
  attributes: Map<string, string>
  hidden: boolean
  viewable: boolean
  editable: boolean
  searchable: boolean
  sortable: boolean
  useMapAccess: boolean
  dto?: DtoDef
}
