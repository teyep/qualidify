import {CaseDefinitionDto} from "./CaseDefinitionDto";

export interface ProjectTemplateDto {
  id: string,
  name: string,
  description: string,
  caseDefinition: CaseDefinitionDto,
  cmmnModel: string,
  variables: any,
  users: Array<string>,
  fields: Map<string, string | undefined>
}
