

export interface PlanItemDto {
  id: string
  name: string
  cmmnInstanceId: string
  projectInstanceId: string
  stage: string | null
  reference: string | null
  type: any
  state: any
  parent: string
  completable: boolean
  timeCreated: any
  historic: boolean
  isQualityShell: boolean
  cmmnQualityShellId: string | null
  reviewState: any
  qualityLinkId: string | null
}
