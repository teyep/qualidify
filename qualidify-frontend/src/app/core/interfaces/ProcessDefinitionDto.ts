
export interface ProcessDefinitionDto {
  id: string
  key: string
  name: string
  version: number
  graphicalNotation: boolean
}
