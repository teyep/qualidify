
export interface CustomFieldDef {
  id: string,
  reference: [ "USER" | "PROJECT_TEMPLATE" | "PROJECT_INSTANCE" ]
  field: string
  description: string
  dataType: string
  validator?: string
  mandatory: boolean
  active: boolean
}
