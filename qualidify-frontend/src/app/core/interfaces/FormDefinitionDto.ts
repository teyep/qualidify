export interface FormDefinitionDto {
  id: string
  name: string
  key: string
  version: number | null
  form: Map<string, string>
}
