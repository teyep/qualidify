import {CaseDefinitionDto} from "src/app/core/interfaces/CaseDefinitionDto";

export interface CaseInstanceDto {
  id: string,
  name: string,
  caseDefinition: CaseDefinitionDto
  caseDefinitionKey: string,
  state: any // CaseInstanceState,
  completable: boolean,
  isQualityShell: boolean,
  cmmnInstanceId: string | null,
  cmmnQualityShellId: string | null
}
