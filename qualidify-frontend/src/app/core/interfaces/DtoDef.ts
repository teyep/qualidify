import {ColumnDef} from "src/app/core/interfaces/ColumnDef";

export interface DtoDef {
  name: string,
  pluralName: string,
  dtoClass: string,
  addEnabled: boolean,
  updateEnabled: boolean
  deleteEnabled: boolean
  searchEnabled: boolean
  columns: Array<ColumnDef>
}
