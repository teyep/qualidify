import {AbstractHttpService} from "./abstract.http.service";
import {AuthorizationService} from "./authorization-service";
import {Observable} from "rxjs";
import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class AuthorizationServiceImpl extends AbstractHttpService implements AuthorizationService {

  constructor(@Inject(HttpClient) private http: HttpClient) {
    super()
  }

  private hasSessionCookie(): boolean {
    return document.cookie.indexOf('SESSIONID=') == 0
  }

  isLoggedIn(): Observable<boolean> {
    // Try the session cookie. If absent, return false already. If true, query the remote.
    if(this.hasSessionCookie()) {
      return new Observable<boolean>(subscriber => {
        subscriber.next(false)
      })
    }
    else {
      return this.http.get(this.endpoint + "/authorization/isuserloggedin") as Observable<boolean>
    }
  }
}
