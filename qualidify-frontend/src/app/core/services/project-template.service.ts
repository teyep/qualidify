import {RestQuery} from "../interfaces/RestQuery";
import {Observable} from "rxjs";
import {ProjectTemplateDto} from "../interfaces/ProjectTemplateDto";
import {InjectionToken} from "@angular/core";
import {GenericDataService} from "./generic-data-service";
import {DtoActionHandler} from "../interfaces/DtoActionHandler";


export const projectTemplateServiceInjectionToken =
  new InjectionToken<ProjectTemplateService>('project-template.service')

export interface ProjectTemplateService extends GenericDataService, DtoActionHandler {
  find(query: RestQuery): Observable<Array<ProjectTemplateDto>>

  startProject(dto: ProjectTemplateDto): Observable<string>
}
