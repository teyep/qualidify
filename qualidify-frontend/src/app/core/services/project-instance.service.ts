import {InjectionToken} from "@angular/core";
import {GenericDataService} from "./generic-data-service";
import {RestQuery} from "../interfaces/RestQuery";
import {Observable} from "rxjs";
import {ProjectInstanceDto} from "../interfaces/ProjectInstanceDto";

export const projectsServiceInjectionToken =
  new InjectionToken<ProjectInstanceService>('projects.service')

export interface ProjectInstanceService extends GenericDataService {
  find(query: RestQuery): Observable<Array<ProjectInstanceDto>>
}
