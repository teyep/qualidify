import {Observable} from "rxjs";
import {CountResponse} from "../interfaces/CountResponse";
import {RestQuery} from "../interfaces/RestQuery";

export interface GenericDataService {
  count(query: RestQuery): Observable<CountResponse>

  // Note: Did not name it 'find', because we might want a strongly-typed version as well.
  genericFind(query: RestQuery): Observable<Array<any>>
}
