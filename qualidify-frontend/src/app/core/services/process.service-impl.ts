import {Inject, Injectable} from "@angular/core";
import {AbstractHttpService} from "./abstract.http.service";
import {ProcessService} from "./process-service";
import {RestQuery} from "../interfaces/RestQuery";
import {Observable} from "rxjs";
import {CaseDefinitionDto} from "../interfaces/CaseDefinitionDto";
import {HttpClient} from "@angular/common/http";
import {CaseInstanceDto} from "../interfaces/CaseInstanceDto";
import {DtoAction} from "../../shared/DtoAction";
import {ProcessDefinitionDto} from "../interfaces/ProcessDefinitionDto";

@Injectable()
export class ProcessServiceImpl extends AbstractHttpService implements ProcessService {

  midpoint = this.endpoint + "/process"

  constructor(
    @Inject(HttpClient) private http: HttpClient
  ) {
    super();
  }

  countCaseDefinitions(query: RestQuery): Observable<number> {
    return this.http.post(this.midpoint + "/casedef/count", query) as Observable<number>
  }

  findCaseDefinitions(query: RestQuery): Observable<Array<CaseDefinitionDto>> {
    return this.http.post(this.midpoint + "/casedef/find", query) as Observable<Array<CaseDefinitionDto>>
  }

  countCaseInstances(query: RestQuery): Observable<number> {
    return this.http.post(this.midpoint + "/caseinst/count", query) as Observable<number>
  }

  findCaseInstances(query: RestQuery): Observable<Array<CaseInstanceDto>> {
    return this.http.post(this.midpoint + "/caseinst/find", query) as Observable<Array<CaseInstanceDto>>
  }

  getCaseInstanceActions(caseInstance: CaseInstanceDto): DtoAction[] {
    let actions: DtoAction[] = []
    actions.push(new DtoAction("Complete",
      caseInstance.completable,
      true
    ))
    actions.push(new DtoAction("Cancel",
      !caseInstance.isQualityShell,
      !caseInstance.isQualityShell,
    ))
    return actions
  }

  executeCaseInstanceAction(action: DtoAction, dto: CaseInstanceDto): Observable<any> {
    return this.http.put(
      this.midpoint + "/caseinst/action/" + action.name.toLowerCase(), dto
    ) as Observable<any>
  }

  countProcessDefinitions(query: RestQuery): Observable<number> {
    return this.http.post(this.midpoint + "/processdef/count", query) as Observable<number>
  }

  findProcessDefinitions(query: RestQuery): Observable<Array<ProcessDefinitionDto>> {
    return this.http.post(this.midpoint + "/processdef/find", query) as Observable<Array<ProcessDefinitionDto>>
  }

  getProcessDefinitionActions(dto: ProcessDefinitionDto): DtoAction[] {
    let actions: DtoAction[] = []
    actions.push(new DtoAction("Start",
      true,
      true))
    return actions
  }

  executeProcessDefinitionAction(action: DtoAction, dto: ProcessDefinitionDto): Observable<any> {
    return this.http.put(
      this.midpoint + "/processdef/action/" + action.name.toLowerCase(), dto
    ) as Observable<ProcessDefinitionDto>
  }

}
