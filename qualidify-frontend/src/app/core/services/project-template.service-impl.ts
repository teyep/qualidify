import {Inject, Injectable} from "@angular/core";
import {RestQuery} from "../interfaces/RestQuery";
import {Observable} from "rxjs";
import {ProjectTemplateDto} from "../interfaces/ProjectTemplateDto";
import {AbstractHttpService} from "./abstract.http.service";
import {HttpClient} from "@angular/common/http";
import {ProjectTemplateService} from "./project-template.service";
import {CountResponse} from "../interfaces/CountResponse";

@Injectable()
export class ProjectTemplateServiceImpl extends AbstractHttpService implements ProjectTemplateService {

  constructor(
    @Inject(HttpClient) private http: HttpClient
  ) {
    super()
  }

  genericFind(query: RestQuery): Observable<any[]> {
    return this.find(query) as Observable<any[]>
  }

  handleAction(dto: any, action: String): Observable<any> {
      if(action == "Start") {
        return this.startProject(dto as ProjectTemplateDto);
      }
      else {
        throw new Error("Method not implemented.");
      }
  }

  actionIsEnabled(dto: any, action: String): Observable<boolean> {
    // We can always start a project.
    return new Observable<boolean>(subscriber => {
      subscriber.next(true)
    })
  }

  count(query: RestQuery): Observable<CountResponse> {
    return this.http.post(this.endpoint + "/project/template/count", query) as Observable<CountResponse>
  }

  find(query: RestQuery): Observable<Array<ProjectTemplateDto>> {
    return this.http.post(this.endpoint + "/project/template/find", query) as Observable<Array<ProjectTemplateDto>>
  }

  startProject(dto: ProjectTemplateDto): Observable<string> {
    let options: Object = {responseType: 'text'}

    return this.http.put(
      this.endpoint+ "/project/template/action/start", dto, options
    ) as Observable<string>
  }
}
