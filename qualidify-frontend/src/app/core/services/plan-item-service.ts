import {GenericDataService} from "./generic-data-service";
import {DtoActionHandler} from "../interfaces/DtoActionHandler";
import {InjectionToken} from "@angular/core";

export const planItemServiceInjectionToken = new InjectionToken<PlanItemService>('planitem.service')

export interface PlanItemService extends GenericDataService, DtoActionHandler{
}
