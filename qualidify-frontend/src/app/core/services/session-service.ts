import {EventEmitter, InjectionToken} from "@angular/core";
import {Observable} from "rxjs";

export const sessionServiceInjectionToken = new InjectionToken<SessionService>('session.service')

export interface SessionService {
  login(username: String, password: String): Observable<boolean>

  logout(): Observable<boolean>

  isLoggedIn(): Observable<boolean>

  /**
   * Returns an subscribable object that emits an event indicating whether the user is logged in
   */
  getLoginEventEmitter(): EventEmitter<boolean>
}
