import {UserService} from "./user-service";
import {AbstractHttpService} from "./abstract.http.service";
import {Observable} from "rxjs";
import {CountResponse} from "../interfaces/CountResponse";
import {RestQuery} from "../interfaces/RestQuery";
import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class UserServiceImpl extends AbstractHttpService implements UserService {

  constructor(@Inject(HttpClient) private http: HttpClient) {
    super()
  }

  count(query: RestQuery): Observable<CountResponse> {
    return this.http.post(this.endpoint + "/authorization/user/count", query) as Observable<CountResponse>
  }

  find(query: RestQuery): Observable<Array<any>> {
    return this.http.post(this.endpoint + "/authorization/user/find", query) as Observable<Array<any>>
  }

  genericFind(query: RestQuery): Observable<Array<any>> {
    return this.find(query)
  }

}
