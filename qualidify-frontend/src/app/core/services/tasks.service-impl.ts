import {AbstractHttpService} from "./abstract.http.service";
import {TasksService} from "./tasks.service";
import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {RestQuery} from "../interfaces/RestQuery";
import {Observable} from "rxjs";
import {TaskDto} from "../interfaces/TaskDto";
import {DtoAction} from "../../shared/DtoAction";
import {FormDefinitionDto} from "../interfaces/FormDefinitionDto";

@Injectable()
export class TasksServiceImpl extends AbstractHttpService implements TasksService {

  midpoint = this.endpoint + "/tasks"

  constructor(
    @Inject(HttpClient) private http: HttpClient
  ) {
    super()
  }

  countTasks(query: RestQuery): Observable<number> {
    return this.http.post(this.midpoint + "/count", query) as Observable<number>
  }

  findTasks(query: RestQuery): Observable<Array<TaskDto>> {
    return this.http.post(this.midpoint + "/find", query) as Observable<Array<TaskDto>>
  }

  findTaskById(id: string): Observable<TaskDto> {
    return this.http.get(this.midpoint + "/find/" + id) as Observable<TaskDto>
  }

  getTaskActions(dto: TaskDto): DtoAction[] {
    let actions: DtoAction[] = []
    actions.push(new DtoAction("Complete",
      dto.formKey == null,
      !dto.isQualityShell
    ))
    actions.push(new DtoAction("Assign",
      true,
      true
    ))
    actions.push(new DtoAction("Claim",
      dto.assignee == null,
      true
    ))
    actions.push(new DtoAction("Unclaim",
      true,  //dto.assignee == currentUser, TODO(): implement Principal / currentUser
      true
    ))
    actions.push(new DtoAction("Approve",
      true,
      dto.isQualityShell
    ))
    actions.push(new DtoAction("Reject",
      true,
      dto.isQualityShell
    ))
    return actions
  }

  executeTaskAction(action: DtoAction, dto: TaskDto): Observable<any> {
    return this.http.put(
      this.midpoint + "/action/" + action.name.toLowerCase(), dto
    ) as Observable<any>
  }

  countForms(query: RestQuery): Observable<number> {
    return this.http.post(this.midpoint + "/form/count", query) as Observable<number>
  }

  findForms(query: RestQuery): Observable<Array<FormDefinitionDto>> {
    return this.http.post(this.midpoint + "/form/find", query) as Observable<Array<FormDefinitionDto>>
  }

}
