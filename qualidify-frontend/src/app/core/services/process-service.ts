import {InjectionToken} from "@angular/core";
import {Observable} from "rxjs";
import {CaseDefinitionDto} from "../interfaces/CaseDefinitionDto";
import {RestQuery} from "../interfaces/RestQuery";
import {CaseInstanceDto} from "../interfaces/CaseInstanceDto";
import {DtoAction} from "../../shared/DtoAction";
import {ProcessDefinitionDto} from "../interfaces/ProcessDefinitionDto";
export const processServiceInjectionToken = new InjectionToken<ProcessService>('process.service')

export interface ProcessService {

  countCaseDefinitions(query: RestQuery): Observable<number>

  findCaseDefinitions(query: RestQuery): Observable<Array<CaseDefinitionDto>>

  countCaseInstances(query: RestQuery): Observable<number>

  findCaseInstances(query: RestQuery): Observable<Array<CaseInstanceDto>>

  getCaseInstanceActions(caseInstance: CaseInstanceDto): DtoAction[]

  executeCaseInstanceAction(action: DtoAction, caseInstance: CaseInstanceDto): Observable<any>

  countProcessDefinitions(query: RestQuery): Observable<number>

  findProcessDefinitions(query: RestQuery): Observable<Array<ProcessDefinitionDto>>

  getProcessDefinitionActions(dto: ProcessDefinitionDto): DtoAction[]

  executeProcessDefinitionAction(action: DtoAction, dto: ProcessDefinitionDto): Observable<any>
}
