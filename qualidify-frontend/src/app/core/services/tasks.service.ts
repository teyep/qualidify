import {InjectionToken} from '@angular/core';
import {RestQuery} from "../interfaces/RestQuery";
import {Observable} from "rxjs";
import {TaskDto} from "../interfaces/TaskDto";
import {DtoAction} from "../../shared/DtoAction";
import {FormDefinitionDto} from "../interfaces/FormDefinitionDto";

export const taskServiceInjectionToken = new InjectionToken<TasksService>('tasks.service')

export interface TasksService {

  countTasks(query: RestQuery): Observable<number>

  findTasks(query: RestQuery): Observable<Array<TaskDto>>

  findTaskById(id: string): Observable<TaskDto>

  getTaskActions(dto: TaskDto): DtoAction[]

  executeTaskAction(action: DtoAction, dto: TaskDto): Observable<any>

  countForms(query: RestQuery): Observable<number>

  findForms(query: RestQuery): Observable<Array<FormDefinitionDto>>
}

