import {InjectionToken} from "@angular/core";
import {SessionService} from "./session-service";
import {Observable} from "rxjs";

export const authorizationServiceInjectionToken = new InjectionToken<SessionService>('authorization.service')

export interface AuthorizationService {

  /**
   * Returns an observable which resolves the session's current state of being logged in.
   */
  isLoggedIn(): Observable<boolean>

  //
}
