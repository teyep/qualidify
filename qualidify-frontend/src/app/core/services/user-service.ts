import {InjectionToken} from "@angular/core";
import {Observable} from "rxjs";
import {RestQuery} from "../interfaces/RestQuery";
import {GenericDataService} from "./generic-data-service";


export const userServiceInjectionToken = new InjectionToken<UserService>('user.service')

export interface UserService extends GenericDataService {
  find(query: RestQuery): Observable<Array<any>>
}
