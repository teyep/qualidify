import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AbstractHttpService} from "./abstract.http.service";
import {Observable} from "rxjs";
import {RestQuery} from "../interfaces/RestQuery";
import {ProjectInstanceDto} from "../interfaces/ProjectInstanceDto";
import {ProjectInstanceService} from "./project-instance.service";
import {CountResponse} from "../interfaces/CountResponse";

@Injectable()
export class ProjectInstanceServiceImpl extends AbstractHttpService implements ProjectInstanceService {

  midpoint = this.endpoint + "/project";

  constructor(
    @Inject(HttpClient) private http: HttpClient
  ) {
    super()
  }

  count(query: RestQuery): Observable<CountResponse> {
    return this.http.post(this.midpoint + "/instance/count", query) as Observable<CountResponse>
  }

  find(query: RestQuery): Observable<Array<ProjectInstanceDto>> {
    return this.http.post(this.midpoint + "/instance/find", query) as Observable<Array<ProjectInstanceDto>>
  }

  genericFind(query: RestQuery): Observable<Array<any>> {
    return this.find(query) as Observable<Array<any>>
  }

}
