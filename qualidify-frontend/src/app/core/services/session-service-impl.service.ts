import {EventEmitter, Inject, Injectable} from "@angular/core";
import {AbstractHttpService} from "./abstract.http.service";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "./session-service";
import {Observable} from "rxjs";

@Injectable()
export class SessionServiceImpl extends AbstractHttpService implements SessionService {

  private loggedIn?: boolean

  private sessionEvent: EventEmitter<boolean> = new EventEmitter<boolean>()

  constructor(@Inject(HttpClient) private http: HttpClient) {
    super()
    this.isLoggedIn().subscribe((data) => this.loggedIn = data)
  }

  private changeSessionState(newState: boolean) {
    this.loggedIn = newState
    this.sessionEvent.emit(newState)
  }

  login(username: String, password: String): Observable<boolean> {
    // TODO: QUAL-510
    return this.http.post(this.endpoint + "/session/login", { userName: username, password: password}) as Observable<boolean>
  }

  logout(): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      this.http.get(this.endpoint + "/session/logout").subscribe({
        next: value => {
          this.changeSessionState(false)
          subscriber.next(true)
        },
        error: value => {
          subscriber.next(false)
        }
      })
    })
  }

  isLoggedIn(): Observable<boolean> {
    return this.http.get(this.endpoint + "/session/isuserloggedin") as Observable<boolean>
  }

  getLoginEventEmitter(): EventEmitter<boolean> {
    return this.sessionEvent
  }
}
