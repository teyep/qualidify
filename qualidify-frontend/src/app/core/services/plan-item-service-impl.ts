import {PlanItemService} from "./plan-item-service";
import {RestQuery} from "../interfaces/RestQuery";
import {Observable} from "rxjs";
import {CountResponse} from "../interfaces/CountResponse";
import {AbstractHttpService} from "./abstract.http.service";
import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {PlanItemDto} from "../interfaces/PlanItemDto";

@Injectable()
export class PlanItemServiceImpl extends AbstractHttpService implements PlanItemService {

  constructor(
    @Inject(HttpClient) private http: HttpClient
  ) {
    super();
  }

  count(query: RestQuery): Observable<CountResponse> {
    return this.http.post(this.endpoint + "/process/planitem/count", query) as Observable<CountResponse>
  }

  genericFind(query: RestQuery): Observable<Array<any>> {
    return this.http.post(this.endpoint + "/process/planitem/find", query) as Observable<Array<PlanItemDto>>
  }


  handleAction(dto: any, action: String): Observable<any> {
    return this.http.put(this.endpoint + "/process/planitem/action/" + action.toLowerCase(), dto as PlanItemDto) as Observable<any>
  }

  actionIsEnabled(dto: any, action: String): Observable<boolean> {
    var planItem = dto as PlanItemDto
    switch(action) {
      case "Complete":
        return new Observable<boolean>(subscriber => subscriber.next(planItem.completable))
      case "Start":
        return new Observable<boolean>(subscriber => subscriber.next(
          (planItem.type == "USER_EVENT_LISTENER" && planItem.state == "AVAILABLE") || ("HUMAN_TASK" == planItem.type && planItem.state == "ENABLED")
        ))
      default:
        return new Observable<boolean>(subscriber => subscriber.next(false))
    }
  }

}
