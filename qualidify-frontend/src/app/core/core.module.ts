import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {sessionServiceInjectionToken} from "./services/session-service";
import {SessionServiceImpl} from "./services/session-service-impl.service";
import {authorizationServiceInjectionToken} from "./services/authorization-service";
import {AuthorizationServiceImpl} from "./services/authorization-service-impl";
import {userServiceInjectionToken} from "./services/user-service";
import {UserServiceImpl} from "./services/user-service-impl";
import {planItemServiceInjectionToken} from "./services/plan-item-service";
import {PlanItemServiceImpl} from "./services/plan-item-service-impl";
import {projectTemplateServiceInjectionToken} from "./services/project-template.service";
import {ProjectTemplateServiceImpl} from "./services/project-template.service-impl";
import {projectsServiceInjectionToken} from "./services/project-instance.service";
import {ProjectInstanceServiceImpl} from "./services/project-instance.service-impl";



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: sessionServiceInjectionToken,
      useClass: SessionServiceImpl
    },
    {
      provide: authorizationServiceInjectionToken,
      useClass: AuthorizationServiceImpl
    },
    {
      provide: userServiceInjectionToken,
      useClass: UserServiceImpl
    },
    {
      provide: planItemServiceInjectionToken,
      useClass: PlanItemServiceImpl
    },
    {
      provide: projectTemplateServiceInjectionToken,
      useClass: ProjectTemplateServiceImpl
    },
    {
      provide: projectsServiceInjectionToken,
      useClass: ProjectInstanceServiceImpl
    }
  ]
})
export class CoreModule { }
