import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ViewsModule} from "./views/views.module";
import {HomeComponent} from "./views/home/home.component";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent,
    loadChildren: () => ViewsModule
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
