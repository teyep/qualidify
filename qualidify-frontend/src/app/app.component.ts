import { Component } from '@angular/core';
import { QualidifyNavbarItem } from "./shared/qualidify-navbar/qualidify-navbar.component";
import {faFileText, faGear, faInbox, faTasks} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'qualidify-frontend';

  mainNavItems: Array<QualidifyNavbarItem> = [
    {
      caption: "Projects",
      routerLink: "/projects",
      icon: faInbox
    },
    {
      caption: "Tasks",
      routerLink: "/tasks",
      icon: faTasks
    },
    {
      caption: "Reports",
      routerLink: "/reports",
      icon: faFileText
    },
    {
      caption: "Configuration",
      routerLink: "/configuration",
      icon: faGear
    }]



}
