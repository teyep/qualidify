# README #

This README contains specific commands for the Qualidify environment

### What is this repository for? ###

This repo contains the code and bpmn schema's for Qualidify, a Quality and Risk Management System 
At this moment we are building still a snapshot version

### What do I need? ###

You will need an environment with: 
* Kotlin 1.7.10
* Java 17
* Apache Maven 3
* Docker - this is needed for tooling, see section about tooling

### Start up

Building:

    mvn clean install

Starting:

    cd Qualidify-main/Qualidify-gui
    mvn spring-boot:run
    
    Browser -> localhost:8080

Or start QualidifyGuiApplication manually in your IDE

### Information ###

* A short description on [Security](src/site/markdown/README_security.md) and the behaviour on different profiles

### Tooling ###    

* Information about how to use [Databases](src/site/markdown/README_database.md) and which one will be active on which
  profile
* The test environment and test execution is explained in [Testing](src/site/markdown/README_testing.md) 
* How to setup and check our code quality and test coverage can be read in
  the [Sonarqube](src/site/markdown/README_sonarqube.md) page
* How to setup de [Flowable](src/site/markdown/README_flowable.md) server
* Jasper Reports designer @Niels?

### Contribution guidelines ###

Todo

### Who do I talk to? ###

* marcel.pot@qualidify.com
* joanne.van.abbema@qualidify.com
* niels.visscher@qualidify.com
* menno.tol@qualidify.com