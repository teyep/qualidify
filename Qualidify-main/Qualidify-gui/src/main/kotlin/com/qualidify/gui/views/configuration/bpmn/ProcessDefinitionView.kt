package com.qualidify.gui.views.configuration.bpmn

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.bpmn.ProcessDefinitionView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.process.dto.BusinessProcessDefinitionDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A view for Business Process models
 *
 * @author Menno Tol
 * @since 1.0
 * */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(ProcessDefinitionView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Project.PROCESS_DEFINITION
)
open class ProcessDefinitionView(dataCrud: DataCrud<BusinessProcessDefinitionDto>) :
    AbstractDataCrudTemplate<BusinessProcessDefinitionDto>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val IDENTITY = "procesdefinitions"
        const val TITLE = "Process Definitions"
    }
}
