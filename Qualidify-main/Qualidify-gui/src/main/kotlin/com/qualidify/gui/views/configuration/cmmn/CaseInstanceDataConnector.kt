package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.gui.providers.CurrentUserProvider
import com.qualidify.metadata.gui.providers.ActionExecutor
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A Subscribable [DataConnector] taking care of querying and persisting [CaseInstance] data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
class CaseInstanceDataConnector(
    private val caseInstanceService: CaseInstanceService,
    private val currentUserProvider: CurrentUserProvider,
    ) : AbstractQueryableServiceDataProvider<CaseInstanceDto>(CaseInstanceDto::class, caseInstanceService),
    ActionExecutor<CaseInstanceDto> {

    val currentUser: String
        get() {
            // custom getter for when currentUser is actually called, returns anonymousUser in dev environment
            return currentUserProvider.getUserName()
        }

    override fun create(dto: CaseInstanceDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun update(dto: CaseInstanceDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun delete(dto: CaseInstanceDto): Outcome<Any> {
        return Outcome.OK
    }

    fun complete(dto: CaseInstanceDto) {
        caseInstanceService.complete(dto, currentUser)
        refresh()
    }

    fun cancel(dto: CaseInstanceDto) {
        caseInstanceService.cancel(dto, currentUser)
        refresh()
    }

    inner class InnerAction(
        val action: (CaseInstanceDto) -> Unit,
        val isEnabled: (CaseInstanceDto) -> Boolean,
        val isVisible: (CaseInstanceDto) -> Boolean,
    )

    /**
     *  Matrix to define under which conditions actions (buttons) are enabled or visible.
     *
     *  As a general rule:
     *  - all user actions are visible, actions for which to user has no right should not be visible
     *  - non-enabled actions are greyed-out
     */
    val innerActions: Map<String, InnerAction> = mapOf(
        "Complete" to InnerAction({complete(it)},
            { it.completable },
            { true }),
        "Cancel" to InnerAction({cancel(it)},
            { !it.isQualityShell },  // should not cancel a quality shell
            { !it.isQualityShell })
    )

    override fun isActionEnabled(action: String, dto: CaseInstanceDto): Boolean {
        return innerActions[action]?.isEnabled?.invoke(dto) ?: super.isActionEnabled(action, dto)
    }

    override fun isActionVisible(action: String, dto: CaseInstanceDto): Boolean {
        return innerActions[action]?.isVisible?.invoke(dto) ?: super.isActionVisible(action, dto)
    }

    override fun executeAction(action: String, dto: CaseInstanceDto): Outcome<Any> = try {
        innerActions[action]!!.action.invoke(dto)
        Outcome.OK
    } catch (e: Exception) {
        Outcome.fail(e)
    }

}