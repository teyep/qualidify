package com.qualidify.gui.components

import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component
import java.io.Serializable

@Component
@UIScope
abstract class AbstractGridRow<D: Serializable> : HorizontalLayout() {

    lateinit var dtoInstance: D

    init {
        isSpacing = false
        isPadding = false
        isMargin = false
        alignItems = FlexComponent.Alignment.CENTER
        justifyContentMode = FlexComponent.JustifyContentMode.BETWEEN
        height = "24px"
        style.set("gap", "10px")
    }

    fun setDto(dto: D) {
        this.dtoInstance = dto
        refresh()
    }

    fun refresh() {
        setupClickListener()

        prepare()
    }

    /**
     * Optional function to add a clicklistener to the row in a child class
     */
    open fun setupClickListener() {}

    /**
     * Abstract function for preparing the componenent to be inserted in the grid row
     */
    abstract fun prepare()

}