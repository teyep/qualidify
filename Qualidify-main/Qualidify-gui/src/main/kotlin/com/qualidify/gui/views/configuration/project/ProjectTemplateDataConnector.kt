package com.qualidify.gui.views.configuration.project

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.ActionExecutor
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.process.CaseDefinitionService
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A Subscribable [DataConnector] taking care of querying and persisting [ProjectTemplate] data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
class ProjectTemplateDataConnector(
    private val projectTemplateService: ProjectTemplateService,
    private val caseDefinitionService: CaseDefinitionService,
) : AbstractQueryableServiceDataProvider<ProjectTemplateDto>(ProjectTemplateDto::class, projectTemplateService),
    ActionExecutor<ProjectTemplateDto> {

    private val logger by LoggerDelegation()

    override fun create(dto: ProjectTemplateDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun update(dto: ProjectTemplateDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun delete(dto: ProjectTemplateDto): Outcome<Any> {
        return Outcome.OK
    }

    fun getColumnMetadataForProjectVariables(dto: ProjectTemplateDto): List<ColumnDef> {
        return projectTemplateService.getColumnDefinitionsForProjectVariables(dto)
    }

    fun getColumnMetadataForProjectInformation(dto: ProjectTemplateDto): List<ColumnDef> {
        return projectTemplateService.getColumnDefinitionsForProjectInfo(dto)
    }

    fun start(dto: ProjectTemplateDto): String? {
        val projectId = projectTemplateService.startProject(dto)
        logger.info("Started new project template ${dto.name}")
        refresh()
        return projectId
    }

    inner class InnerAction(
        val action: (ProjectTemplateDto) -> Unit,
    )

    val innerActions: Map<String, InnerAction> = mapOf(
        "Start" to InnerAction { start(it) }
    )

    override fun executeAction(action: String, dto: ProjectTemplateDto): Outcome<Any> {
        return try {
            innerActions[action]!!.action.invoke(dto)
            Outcome.OK
        } catch (e: Exception) {
            Outcome.fail(e)
        }
    }

    override fun refresh() {
        this.refreshAll()
    }

    fun getCaseDefinitionDtoByKey(key: String): CaseDefinitionDto =
        caseDefinitionService.getCaseDefinitionDtoByKey(key)

}