package com.qualidify.gui.providers

import com.qualidify.metadata.gui.providers.EnumItemProvider
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import org.springframework.stereotype.Component

@Component
class PlanItemTypeEnumItemProvider : EnumItemProvider<PlanItemType>(PlanItemType::class.java)

@Component
class PlanItemStateEnumItemProvider : EnumItemProvider<PlanItemState>(PlanItemState::class.java)