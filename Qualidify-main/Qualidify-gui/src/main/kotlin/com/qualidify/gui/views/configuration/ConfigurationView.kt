package com.qualidify.gui.views.configuration

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.security.SecurityUtils
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.ConfigurationView.Companion.ROUTE
import com.qualidify.gui.views.configuration.ConfigurationView.Companion.TITLE
import com.qualidify.gui.views.configuration.bpmn.ProcessDefinitionView
import com.qualidify.gui.views.configuration.bpmn.ProcessTaskView
import com.qualidify.gui.views.configuration.cmmn.*
import com.qualidify.gui.views.configuration.customfield.CustomFieldView
import com.qualidify.gui.views.configuration.domain.ConfigurationDomainView
import com.qualidify.gui.views.configuration.forms.FormView
import com.qualidify.gui.views.configuration.metadata.MetaDataDrivenView
import com.qualidify.gui.views.configuration.project.ProjectInstanceView
import com.qualidify.gui.views.configuration.project.ProjectTemplateView
import com.qualidify.gui.views.configuration.security.SecurityGroupView
import com.qualidify.gui.views.configuration.user.UserView
import com.qualidify.metadata.service.Metadata.Companion.getMnemonicList
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.accordion.Accordion
import com.vaadin.flow.component.accordion.AccordionPanel
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.details.DetailsVariant
import com.vaadin.flow.component.html.H5
import com.vaadin.flow.component.html.Hr
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.router.internal.HasUrlParameterFormat
import com.vaadin.flow.spring.annotation.UIScope
import javax.annotation.security.RolesAllowed


/**
 * Configuration view, a view for configuring everything
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Route(value = ROUTE, layout = MainLayout::class)
@PageTitle(TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@RolesAllowed(
    Roles.Configuration.VIEW
)
@UIScope
open class ConfigurationView(
    val securityUtils: SecurityUtils
) : VerticalLayout() {

    init {
        setId("view-$IDENTITY")
        val accordion = Accordion()
        accordion.setWidthFull()

        val projectConfigurationPanel = createAccordionPanel(
            VaadinIcon.AUTOMATION, "Project Configuration",
            *listOfNotNull(
                createButton("Project Templates", ProjectTemplateView::class.java),
                createButton("Project Instances", ProjectInstanceView::class.java),
                Hr(),
                createButton("Case Definitions", CaseDefinitionView::class.java),
                createButton("Case Instances", CaseInstanceView::class.java),
                createButton("Plan Items", PlanItemView::class.java),
                createButton("Case Tasks", TaskView::class.java),
                Hr(),
                createButton("Form Definitions", FormView::class.java),
                Hr(),
                createButton("Process Definitions", ProcessDefinitionView::class.java),
                createButton("Process Tasks", ProcessTaskView::class.java),
                Hr(),
                createButton("Historic Tasks", HistoricTaskView::class.java)
            ).toTypedArray()
        )
        accordion.add(projectConfigurationPanel)

        val securityConfigurationPanel = createAccordionPanel(
            VaadinIcon.KEY, "Security",
            *listOfNotNull(
                createButton("User Management", UserView::class.java),
                createButton("Group Management", SecurityGroupView::class.java)
            ).toTypedArray()
        )
        accordion.add(securityConfigurationPanel)

        val dataConfigurationPanel = createAccordionPanel(
            VaadinIcon.CUBES, "Data",
            *listOfNotNull(
                createButton("Configuration Domains", ConfigurationDomainView::class.java),
                createButton("Custom Fields", CustomFieldView::class.java),
          //      createButton("Metadata Definitions", MetaDataDrivenView::class.java, "Metadataobject")
            ).toTypedArray()
        )
        accordion.add(dataConfigurationPanel)

        val metadataConfigurationPanel = createAccordionPanel(
            VaadinIcon.EYE, "Data by Metadata",
            *getMnemonicList()
                .map { createButton(it, MetaDataDrivenView::class.java, it) }
                .filterNotNull()
                .toTypedArray()
        )
        accordion.add(metadataConfigurationPanel)

        add(accordion)
    }

    private fun createButton(title: String, navigationTarget: Class<out Component>): Component? {
        if (securityUtils.checkClassAccess(navigationTarget)) {
            val button = Button(Span(title))
            button.setWidthFull()
            button.addClickListener { UI.getCurrent().navigate(navigationTarget) }
            return button
        } else {
            return null
        }
    }

    private fun createButton(title: String, navigationTarget: Class<out Component>, parameter: String): Component? {
        if (securityUtils.checkClassAccess(navigationTarget)) {
            val button = Button(Span(title))
            button.setWidthFull()
            button.addClickListener {
                UI.getCurrent().navigate(
                    navigationTarget,
                    HasUrlParameterFormat.getParameters(parameter)
                )
            }
            return button
        } else {
            return null
        }

    }

    private fun createAccordionPanel(icon: VaadinIcon, title: String, vararg components: Component): AccordionPanel {
        val layout = VerticalLayout()
        layout.setWidthFull()

        layout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER)
        layout.add(*components)

        val panel = AccordionPanel(panelTitle(icon, title), layout)
        panel.addThemeVariants(DetailsVariant.REVERSE)
        return panel
    }

    private fun panelTitle(icon: VaadinIcon, title: String): Component {
        val panelTitle = H5(Span(icon.create()), Span(" $title"))
        panelTitle.setWidthFull()
        return panelTitle
    }

    companion object {
        const val TITLE = "Configuration"
        const val ROUTE = "configuration"
        const val IDENTITY = "config"
    }
}
