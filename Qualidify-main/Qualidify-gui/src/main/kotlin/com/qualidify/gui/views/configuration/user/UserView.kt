package com.qualidify.gui.views.configuration.user

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.user.UserView.Companion.IDENTITY
import com.qualidify.gui.views.configuration.user.UserView.Companion.TITLE
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.security.roles.Roles
import com.qualidify.service.user.dto.UserDto
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed


/**
 * A View for Users
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Security.USER
)
open class UserView(dataCrud: DataCrud<UserDto>) : AbstractDataCrudTemplate<UserDto>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val TITLE = "Users"
        const val IDENTITY = "users"
    }
}



