package com.qualidify.gui.views.configuration.security

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.security.SecurityService
import com.qualidify.service.security.dto.SecurityGroupDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A Subscribable DataConnector taking care of querying and persisting SecurityGroup data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@VaadinSessionScope
@Service
class SecurityGroupDataConnector(val securityService: SecurityService) :
    AbstractQueryableServiceDataProvider<SecurityGroupDto>(
        SecurityGroupDto::class,
        securityService
    ) {

    override fun create(dto: SecurityGroupDto): Outcome<Any> {
        return executeAndWrap { securityService.createGroup(dto) }
    }

    override fun update(dto: SecurityGroupDto): Outcome<Any> {
        return executeAndWrap { securityService.update(dto) }
    }

    override fun delete(dto: SecurityGroupDto): Outcome<Any> {
        TODO("Not yet implemented")
    }


}