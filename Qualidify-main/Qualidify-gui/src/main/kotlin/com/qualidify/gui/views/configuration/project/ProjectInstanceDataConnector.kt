package com.qualidify.gui.views.configuration.project

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service
import javax.annotation.security.RolesAllowed

/**
 * A Subscribable [DataConnector] taking care of querying and persisting [ProjectInstance] data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
@RolesAllowed(
    Roles.Configuration.Project.INSTANCE
)
open class ProjectInstanceDataConnector(private val projectInstanceService: ProjectInstanceService) :
    AbstractQueryableServiceDataProvider<ProjectInstanceDto>(
        ProjectInstanceDto::class,
        projectInstanceService
    ) {

    override fun create(dto: ProjectInstanceDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun update(dto: ProjectInstanceDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun delete(dto: ProjectInstanceDto): Outcome<Any> {
        return Outcome.OK
    }

    fun getById(id: String): ProjectInstanceDto? {
        return projectInstanceService.getProjectInstanceById(id)
    }

    fun getByCmmnInstanceId(id: String): ProjectInstanceDto? {
        return projectInstanceService.getProjectInstanceByCmmnId(id)
    }

    fun isProjectAvailable(id: String): Boolean {
        return projectInstanceService.isProjectAvailable(id)
    }
}