package com.qualidify.gui.views.tasks

import com.qualidify.core.util.html.toHtmlCase
import com.qualidify.gui.components.projectpresenter.AbstractPlanItemGridPanel
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.menubar.MenuBar
import com.vaadin.flow.component.menubar.MenuBarVariant
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.Query
import com.vaadin.flow.data.value.ValueChangeMode
import com.vaadin.flow.shared.Registration
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class TaskGridPanel(
    private val grid: TaskGrid,
    private val taskDataConnector: TaskDataConnector
) : AbstractPlanItemGridPanel<Nothing>() {

    override val title = H2("Tasks available")
    override val titleBar = HorizontalLayout()

    private val filterByUserButton = FilterButton("My tasks", "anonymousUser")
    private val filterByUnclaimedButton = FilterButton("Unclaimed", null)
    private val filterByProjectMenubar = FilterMenuBar("Project")
    private val searchField = TextField()

    override fun setStyle() {
        setSizeFull()
        isPadding = false
        isMargin = false
        setId("panel-${title.text.toHtmlCase()}")
        title.className = "gridpanel-title"
        titleBar.style.set("margin-top", "6px")  // to align with the project view
        titleBar.alignItems = FlexComponent.Alignment.CENTER
    }

    override fun addTitleBar() {
        titleBar.removeAll()
        titleBar.add(title)
        filterByUser()
        filterByUnclaimed()
        filterByProject()
        titleBar.addAndExpand(Div())  // empty Div() acts as spacer
        filterByTaskNameLike()
        add(titleBar)
    }

    override fun addGrid() {
        grid.clearConstraints()
        add(grid)
    }

    private fun filterByUser() {
        filterByUserButton.refresh()
        filterByUserButton.registration?.remove()  // remove existing clicklisteners
        filterByUserButton.registration = filterByUserButton.addClickListener {
            filterByUserButton.toggle()
            filterByUnclaimedButton.refresh()
            filterByProjectMenubar.refresh()
        }

        titleBar.add(filterByUserButton)
    }

    private fun filterByUnclaimed() {
        filterByUnclaimedButton.refresh()
        filterByUnclaimedButton.registration?.remove()  // remove existing clicklisteners
        filterByUnclaimedButton.registration = filterByUnclaimedButton.addClickListener {
            filterByUnclaimedButton.toggle()
            filterByUserButton.refresh()
            filterByProjectMenubar.refresh()
        }

        titleBar.add(filterByUnclaimedButton)
    }

    private fun filterByProject() {
        filterByProjectMenubar.refresh()

        titleBar.add(filterByProjectMenubar)
    }

    private fun filterByTaskNameLike() {
        searchField.setId("searchfield-datagrid-tasks")
        searchField.style.set("margin-top", "25px")
        searchField.style.set("margin-bottom", "9px")

        searchField.placeholder = "Search task by name"
        searchField.isClearButtonVisible = true
        searchField.valueChangeMode = ValueChangeMode.LAZY
        searchField.addValueChangeListener {
            grid.addConstraint("name", searchField.value)
        }

        titleBar.add(searchField)
    }


    inner class FilterButton(text: String, private val assigneeConstraint: String?) : Button(text) {

        var registration: Registration? = null
        private var isFilterActive = false

        init {
            width = "110px"
            style.set("margin-top", "25px")
            style.set("margin-bottom", "9px")
        }

        fun refresh() {
            isFilterActive = (grid.getConstraintByKey("assignee") == mapOf("assignee" to assigneeConstraint))
            if (isFilterActive) {
                addThemeVariants(ButtonVariant.LUMO_PRIMARY)
            }
            else {
                removeThemeVariants(ButtonVariant.LUMO_PRIMARY)
            }
        }

        fun toggle() {
            if (isFilterActive) setInactive() else setActive()
        }

        fun setActive() {
            isFilterActive = true
            addThemeVariants(ButtonVariant.LUMO_PRIMARY)

            // TODO(): If assigneeConstraint is set to null, the countQuery somehow always returns 0
            // and the fetchQuery function is never called. I.e. filtering does not work correctly.
            // another weird thing: if the constraint is an emptyString (i.e. "") it will work as a filter for any user
            grid.addConstraint("assignee", assigneeConstraint)
        }

        fun setInactive() {
            isFilterActive = false
            removeThemeVariants(ButtonVariant.LUMO_PRIMARY)
            grid.removeConstraint("assignee")
        }

    }


    inner class FilterMenuBar(text: String) : MenuBar() {

        private var isFilterActive = false
        private val menuItem = addItem(text)
        private val subMenu = menuItem.subMenu
        private val uniqueProjects = mutableSetOf<Triple<String, String, String>>()

        init {
            width = "110px"
            style.set("margin-top", "25px")
            style.set("margin-bottom", "9px")
            menuItem.add(Icon(VaadinIcon.ANGLE_DOWN))
            refresh()
        }

        fun refresh() {
            getUniqueProjects()
            prepareSubMenu()
        }

        private fun setActive() {
            isFilterActive = true
            this.addThemeVariants(MenuBarVariant.LUMO_PRIMARY)
        }

        private fun setInactive() {
            isFilterActive = false
            this.removeThemeVariants(MenuBarVariant.LUMO_PRIMARY)
        }

        private fun prepareSubMenu() {
            subMenu.removeAll()
            val noConstraint = grid.getConstraintByKey("cmmnInstanceId") == null
            uniqueProjects.forEach { project ->

                val hasConstraint = grid.getConstraintByKey("cmmnInstanceId") == mapOf("cmmnInstanceId" to project.third)
                if (hasConstraint) {
                    setActive()
                    val item = subMenu.addItem("${project.first} (${project.second})")
                    item.addClickListener {
                        grid.removeConstraint("cmmnInstanceId")
                        prepareSubMenu()
                        setInactive()
                    }
                } else {
                    setInactive()
                    val item = subMenu.addItem("${project.first} (${project.second})")
                    if (!noConstraint) {
                        // --lumo-tertiary-text-color: hsla(214, 45%, 20%, 0.52) for disabled text with sufficient contrast
                        item.element.style.set("color", "hsla(214, 45%, 20%, 0.52)")
                    }
                    item.addClickListener {
                        grid.addConstraint("cmmnInstanceId", project.third)
                        prepareSubMenu()
                        setActive()
                    }
                }

            }
        }

        private fun getUniqueProjects() {
            val gridItems = taskDataConnector.fetch(Query(grid.filter)).toList()

            uniqueProjects.clear()
            gridItems.forEach { taskDto ->
                val projectInstanceDto = taskDataConnector.getProjectInstanceDto(taskDto.projectInstanceId)
                uniqueProjects += Triple(
                    projectInstanceDto.name,
                    taskDto.projectInstanceId,
                    taskDto.cmmnInstanceId
                )
            }
        }

    }


}