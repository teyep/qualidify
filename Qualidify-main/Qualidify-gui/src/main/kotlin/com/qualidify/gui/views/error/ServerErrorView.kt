package com.qualidify.gui.views.error

import com.qualidify.gui.views.common.MainLayout
import com.vaadin.flow.router.*
import org.apache.http.HttpStatus
import javax.annotation.security.PermitAll

/**
 * An Error view for unkown Errors
 *
 * @author Marcel Pot
 * @since 1.0
 */
@ParentLayout(MainLayout::class)
@PageTitle(ServerErrorView.TITLE)
@PermitAll
open class ServerErrorView : AbstractErrorView(IDENTITY), HasErrorParameter<RuntimeException?> {

    override fun setErrorParameter(
        event: BeforeEnterEvent, errorParameter: ErrorParameter<RuntimeException?>
    ): Int {
        logErrorMessage("An unknown error occurred.")
        logExceptionToScreen(errorParameter.exception)
        return HTTP_STATUS
    }

    companion object {
        const val TITLE = "Unknown Error"
        const val IDENTITY = "unknown-error"
        const val HTTP_STATUS = HttpStatus.SC_INTERNAL_SERVER_ERROR

    }
}