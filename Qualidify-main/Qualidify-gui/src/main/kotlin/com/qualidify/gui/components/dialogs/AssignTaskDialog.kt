package com.qualidify.gui.components.dialogs

import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.TaskDto
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.select.Select

/**
 * Dialog that presents available users and selects a user to assign a task to.
 *
 * @param closeFunction: Optional function to execute after closing the dialog (e.g. fire event or forward)
 *
 * @author Menno Tol
 * @since 1.0
 */
class AssignTaskDialog(
    private val taskService: TaskService,
    private val taskDataConnector: TaskDataConnector,
    private val dto: TaskDto,
    private val users: Set<String>,
    private val currentUser: String,
    private val closeFunction: (() -> Unit)? = null
) : Dialog() {

    lateinit var userName: String

    init {
        add(HorizontalLayout(userField(), confirmButton()))
    }

    private fun userField(): Select<String> {
        val field = Select("User")
        field.placeholder = "Select user"
        field.setItems(users)
        field.addValueChangeListener { event ->
            userName = event.value
        }
        return field
    }

    private fun confirmButton(): Button {
        val button = Button("Confirm") {
            taskService.assignTask(dto, userName, currentUser)
            taskDataConnector.refresh()
            close()
            closeFunction?.invoke()
        }
        return button
    }

}