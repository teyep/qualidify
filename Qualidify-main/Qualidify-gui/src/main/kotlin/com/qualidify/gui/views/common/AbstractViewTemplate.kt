package com.qualidify.gui.views.common

import com.qualidify.metadata.gui.components.DataCrud
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import java.io.Serializable
import javax.annotation.PostConstruct

/**
 * Create an abstract view layout to take care of the basics of the layout
 * which has to be shared with other views.
 *
 * @author Marcel Pot
 */
abstract class AbstractViewTemplate(private val viewId: String) : VerticalLayout() {

    /**
     * Builds the view after creation. Annotated with @PostConstruct
     * to make sure it will be executed by spring framework directly
     * after construction phase
     */
    @PostConstruct
    protected open fun init() {
        setId("view-${viewId}")
        setHeightFull()
        header()
        main()
        footer()
    }

    open fun getTitle() = TITLE

    /**
     * Placeholder for header layout
     */
    protected open fun header() {
        add(H2(getTitle()))
    }

    /**
     * Placeholder for main layout
     */
    protected open fun main() {
        //empty
    }

    /**
     * Placeholder for footer layout
     */
    protected open fun footer() {
        //empty
    }

    companion object {
        const val TITLE = "<<VIEW TEMPLATE>>"
    }
}

/**
 * Create an abstract view layout with a Crud to take care of the basics
 * of the layout which has to be shared with other views.
 * It will extend the @see AbstractView which will take care of the
 * basic layouting without a Crud
 *
 * @author Marcel Pot
 * @since 1.0
 */
abstract class AbstractDataCrudTemplate<T : Serializable> protected constructor(
    viewId: String,
    protected val grid: DataCrud<T>
) :
    AbstractViewTemplate(viewId) {

    override fun main() {
        add(grid)
    }

}