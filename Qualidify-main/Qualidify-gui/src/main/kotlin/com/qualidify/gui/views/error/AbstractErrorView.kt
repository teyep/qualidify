package com.qualidify.gui.views.error

import com.qualidify.core.log.LoggerDelegation
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.H3
import com.vaadin.flow.component.html.Pre
import org.apache.commons.lang3.exception.ExceptionUtils

/**
 * An Abstract view for Errors
 *
 * @author Marcel Pot
 * @since 1.0
 */
abstract class AbstractErrorView(private val viewId: String) : Div() {

    private val logger by LoggerDelegation()

    protected fun logErrorMessage(message: String?) {
        setId("view-${viewId}")
        val div = Div()
        val h3 = H3(message)
        div.add(h3)
        add(div)
    }

    protected fun logExceptionToScreen(exception: Exception?) {
        if (exception?.message.equals("Couldn't find route for 'sw.js'")) {
            return
        }
        logger.error("Gui exception", exception)
        setId("view-${viewId}")
        val div = Div()
        div.add("Error occured: ")
        val pre = Pre()
        val stackTrace = ExceptionUtils.getStackTrace(exception)
        pre.add(stackTrace)
        div.add(pre)
        add(div)
    }
}