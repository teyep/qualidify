package com.qualidify.gui.views.configuration.forms

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.process.FormDefinitionService
import com.qualidify.service.process.dto.FormDefinitionDto
import com.qualidify.service.projects.FormMetadataService
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A Subscribable [DataConnector] taking care of querying and persisting [FormDefinition] data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@VaadinSessionScope
@Service
class FormDataConnector(
    private val formDefinitionService: FormDefinitionService,
    private val formMetadataService: FormMetadataService,
) : AbstractQueryableServiceDataProvider<FormDefinitionDto>(FormDefinitionDto::class, formDefinitionService) {

    override fun create(dto: FormDefinitionDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun update(dto: FormDefinitionDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun delete(dto: FormDefinitionDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    fun loadFormModelMetadataByTaskId(taskId: String): List<ColumnDef> {
        return formMetadataService.loadFormModelMetadataByTaskId(taskId)
    }

    fun getFormVariables(taskId: String): Map<String, Any?> {
        return formMetadataService.getFormVariables(taskId)
    }

    fun saveTaskFormData(taskId: String, data: Map<String, Any?>) {
        formMetadataService.saveTaskFormData(taskId, data)
    }

    fun completeTaskFormData(taskId: String, data: Map<String, Any?>) {
        formMetadataService.completeTaskFormData(taskId, data).block()
    }
}