package com.qualidify.gui.views.configuration.metadata

import com.qualidify.core.identity.HasIdentity
import com.qualidify.gui.providers.AbstractServiceDataProvider
import com.qualidify.gui.providers.QueryUtil
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.metadata.gui.providers.MetadataConnector
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.metadata.service.HasIdentityEntityService
import com.vaadin.flow.data.provider.Query
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import java.io.Serializable
import java.util.stream.Stream

/**
 * A Subscribable DataConnector taking care of querying and persisting Configuration domain data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
@Scope("prototype")
class MetaDataDrivenDataConnector(
    override var mnemonic: String,
    var hasIdentityEntityService: HasIdentityEntityService
) :
    AbstractServiceDataProvider<HasIdentity>(), MetadataConnector<HasIdentity> {

    override fun create(dto: HasIdentity): Outcome<Any> {
        return executeAndWrap { hasIdentityEntityService.create(mnemonic, dto) }
    }

    override fun update(dto: HasIdentity): Outcome<Any> {
        return executeAndWrap { hasIdentityEntityService.update(mnemonic, dto) }
    }

    override fun delete(dto: HasIdentity): Outcome<Any> {
        return executeAndWrap { hasIdentityEntityService.delete(mnemonic, dto) }
    }

    override fun fetchFromBackEnd(query: Query<HasIdentity, GridFilter>): Stream<HasIdentity> {
        return hasIdentityEntityService.findByQuery<HasIdentity, Serializable>(
            mnemonic,
            QueryUtil.mapSearchQuery(mnemonic, query)
        )
            .stream()
    }

    override fun sizeInBackEnd(query: Query<HasIdentity, GridFilter>): Int {
        return hasIdentityEntityService.countByQuery<Serializable>(
            mnemonic,
            QueryUtil.mapCountQuery(mnemonic, query)
        ).count.toInt()
    }


}