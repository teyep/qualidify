package com.qualidify.gui.views.tasks

import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.components.taskpresenter.HistoricTaskTitleBar
import com.qualidify.gui.views.AbstractPresenter
import com.qualidify.gui.views.configuration.cmmn.HistoricTaskDataConnector
import com.qualidify.service.process.dto.HistoricTaskDto
import com.vaadin.flow.component.ComponentEventListener
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class HistoricTaskPresenter(
    private val historicTaskDataConnector: HistoricTaskDataConnector,
    private val historicTaskTitleBar: HistoricTaskTitleBar,
) : AbstractPresenter<HistoricTaskDto>(HistoricTaskDto::class.java, historicTaskDataConnector),
    ComponentEventListener<RefreshEvent<*>> {

    override fun addTitleBar() {
        historicTaskTitleBar.registerListener(this)
        historicTaskTitleBar.setDto(dtoInstance)
        add(historicTaskTitleBar)
    }

    override fun onComponentEvent(event: RefreshEvent<*>) {
        when (event.target) {
            // RefreshEventTarget.### -> ###.refresh(
            else -> refresh()
        }
    }

}