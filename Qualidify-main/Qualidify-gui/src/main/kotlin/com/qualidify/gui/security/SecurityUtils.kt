package com.qualidify.gui.security

import com.qualidify.service.impl.security.RolePermissionEvaluator
import com.qualidify.service.security.CurrentUser
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*
import kotlin.reflect.KClass


/**
 * A Helper class for security checks
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
class SecurityUtils(val permissionEvaluator: RolePermissionEvaluator) {

    /**
     * Get the current name of the user logged in (if any)
     * @return the username (otherwise null)
     */
    val currentUserName: String?
        get() {
            return determineUserDetails()?.username
        }

    val currentUser: CurrentUser
        get() {
            return CurrentUser {
                Optional.ofNullable(determineUserDetails())
            }
        }

    private fun determineUserDetails(): UserDetails? {
        val context = SecurityContextHolder.getContext()
        val principal = context.authentication.principal
        if (principal is UserDetails) {
            val userDetails = context.authentication.principal as UserDetails
            return userDetails
        }
        return null
    }


    /**
     * Check if a user is currently logged in
     * @return a boolean indicating if a user is logged in
     */
    val isUserLoggedIn: Boolean
        get() = isUserLoggedIn(SecurityContextHolder.getContext().authentication)

    /**
     * Check if a user is currently loggedin
     * @param authentication the authentication to check
     * @return a boolean indicating if a user is logged in
     */
    fun isUserLoggedIn(authentication: Authentication?): Boolean {
        return (authentication != null
                && authentication !is AnonymousAuthenticationToken)
    }

    /**
     * Checks if a Kotlin Class is allowed for the current user
     * @param kClass the Class to investigate
     * @return boolean indicating if the class is allowed for the current user
     */
    fun checkClassAccess(kClass: KClass<*>): Boolean {
        val authentication = SecurityContextHolder.getContext().authentication
        return permissionEvaluator.checkClass(authentication, kClass)
    }

    fun checkClassAccess(jClass: Class<*>) = checkClassAccess(jClass.kotlin)

}
