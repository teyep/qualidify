package com.qualidify.gui.views.projects

import com.qualidify.core.util.html.toHtmlCase
import com.qualidify.gui.components.templates.TemplateGrid
import com.qualidify.gui.providers.UserProvider
import com.qualidify.gui.views.configuration.project.ProjectTemplateDataConnector
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component


@Component
@UIScope
class ProjectCreator(
    private val projectTemplateDataConnector: ProjectTemplateDataConnector,
    private val formFieldComponentFactory: FormFieldComponentFactory,
    private val userProvider: UserProvider
) : TemplateGrid<ProjectTemplateDto>(ProjectTemplateDto::class.java, projectTemplateDataConnector) {

    init {
        setId("datagrid-newproject")  // used to identify the grid on the page
    }

    override fun prepareTemplate(): LitRenderer<ProjectTemplateDto> {

        return LitRenderer.of<ProjectTemplateDto>(
                "<div class=project-info id=\${item.id}>" +
                    "<div class=project-detail>Name: \${item.name}</div> " +
                    "<div class=project-detail>CMMN model: \${item.cmmnModel}</div> " +
                    "<div class=project-detail>Version: \${item.cmmnModelVersion}</div> " +
                    "<div class=project-actions> " +
                        "<vaadin-button theme=small id=\${item.id} @click=\${startEvent} " +
                            "?hidden=\${item.startVisible}>Start</vaadin-button> " +
                    "</div>" +
                "</div>"
        )
        .withProperty("id") { it.name.toHtmlCase() }
        .withProperty("name") { it.name }
        .withProperty("cmmnModel") { it.cmmnModel }
        .withProperty("cmmnModelVersion") { it.caseDefinition.version }
        .withProperty("startVisible") { !projectTemplateDataConnector.isActionVisible("Start", it) }

        .withFunction("startEvent") { dto ->
            val wizard = ProjectCreationWizard(
                formFieldComponentFactory, projectTemplateDataConnector, userProvider, dto
            )
            wizard.open()
        }

    }

}