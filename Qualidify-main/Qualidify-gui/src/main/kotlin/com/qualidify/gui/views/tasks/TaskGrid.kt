package com.qualidify.gui.views.tasks

import com.qualidify.gui.components.ActionButton
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.service.projects.dto.TaskDto
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.grid.GridVariant
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

/**
 * Grid for showing tasks on the [TaskGridPanel]
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
@CssImport(value = "./styles/taskgrid.css", themeFor = "vaadin-grid")
class TaskGrid(
    private val taskDataConnector: TaskDataConnector
) : Grid<TaskDto>() {

    val filter = GridFilter()
    private val dataProviderWithFilter = taskDataConnector.withConfigurableFilter()

    init {
        setId("datagrid-tasks")
        addThemeVariants(GridVariant.LUMO_COMPACT)
    }

    /**
     * Returns a key-value map if the constraint key exists or else null
     */
    fun getConstraintByKey(key: String): Map<String, String?>? {
        return if (filter.constraints.containsKey(key)) {
            mapOf(key to filter.constraints[key])
        } else {
            null
        }
    }

    fun addConstraint(key: String, value: String?) {
        filter.constraints[key] = value
        refresh()
    }

    fun clearConstraints() {
        filter.constraints.clear()
        refresh()
    }

    fun removeConstraint(key: String) {
        filter.constraints.remove(key)
        refresh()
    }

    fun refresh() {
        setItems(dataProviderWithFilter)
        dataProviderWithFilter.setFilter(filter)

        removeAllColumns()
        addColumns()

        dataProvider.refreshAll()
        setSelectionMode(SelectionMode.NONE)  // turn off selection, this will prevent one rpc for a non-existent node
        isDetailsVisibleOnClick = false  // should prevent second rpc for non-existent node, but does not work always
    }

    private fun addColumns() {
        addColumn(
            LitRenderer.of<TaskDto>(
                "<div class=task-row id=\${item.id}>" +
                        "<div class=task-info @click=\${handleClick}>" +
                        "<div class=task-detail>Name: \${item.name}</div> " +
                        "<div class=task-detail>Project: \${item.project}</div> " +
                        "<div class=task-detail>Assignee: \${item.assignee}</div> " +
                        "</div>" +
                        "</div>"
            )
                .withProperty("id") { it.id }
                .withProperty("name") { it.name }
                .withProperty("project") { "TODO (QUAL-507): project name from variable" }
                .withProperty("assignee") { it.assignee }
                .withFunction("handleClick") { dto ->
                    UI.getCurrent().page.fetchCurrentURL { taskDataConnector.setRedirect(it) }
                    UI.getCurrent().navigate("tasks/${dto.id}")
                }
        )

        taskDataConnector.innerActions.keys.forEach { action ->
            addComponentColumn { taskDto ->
                return@addComponentColumn ActionButton(action, taskDto, taskDataConnector)
            }.apply {
                flexGrow = 0
                width = "100px"  // only set together with button width (columnwidth = buttonwidth + 16px)
                addThemeName("taskgrid-padding")
            }
        }

    }

}

