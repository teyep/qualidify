package com.qualidify.gui.views.configuration.metadata

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.metadata.annotations.Mnemonic
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.metadata.service.Metadata
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.router.BeforeEvent
import com.vaadin.flow.router.HasDynamicTitle
import com.vaadin.flow.router.HasUrlParameter
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.beans.factory.BeanFactory
import javax.annotation.security.RolesAllowed

/**
 * A View for MetaData driven entities
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Route(value = "metadata", layout = MainLayout::class)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Data.METADATA
)
open class MetaDataDrivenView(private val beanFactory: BeanFactory) : Div(), HasUrlParameter<String>,
    HasDynamicTitle {

    private val logger by LoggerDelegation()
    private lateinit var mnemonic: String
    private lateinit var innerView: InnerMetaDataDrivenView


    override fun getPageTitle(): String {
        return this.innerView.getPageTitle()
    }

    @Suppress("UNCHECKED_CAST")
    override fun setParameter(event: BeforeEvent?, mnemonic: String) {
        removeAll();
        logger.info("Setting mnemonic {}", mnemonic)
        this.mnemonic = mnemonic
        val dataCrud = beanFactory.getBean("mnemonicCrud", mnemonic) as DataCrud<HasIdentity>
        innerView = InnerMetaDataDrivenView(dataCrud, mnemonic)
        innerView.build()
        add(innerView)
    }


}

/**
 * An Inner class because the Crud must be determined based on mnemonic
 */
class InnerMetaDataDrivenView(dataCrud: DataCrud<HasIdentity>, private val mnemonic: String) :
    AbstractDataCrudTemplate<HasIdentity>(mnemonic, dataCrud) {

    fun getPageTitle(): String {
        val name = Metadata.getDtoByMnemonic(mnemonic)
            ?.getAnnotation(Mnemonic::class.java)
            ?.plural ?: ""
        if (name.isBlank()) {
            return this.mnemonic + " | " + QualidifyConstants.QUALIDIFY_NAME
        } else {
            return name + " | " + QualidifyConstants.QUALIDIFY_NAME
        }
    }

    /**
     * The @PostConstruct is not executed on the AbstractView, so make it possible to build up the screen manually
     */
    fun build() {
        super.init()
    }

    override fun getTitle(): String {
        return Metadata.getDtoByMnemonic(mnemonic)
            ?.getAnnotation(Mnemonic::class.java)
            ?.plural ?: this.mnemonic
    }

}