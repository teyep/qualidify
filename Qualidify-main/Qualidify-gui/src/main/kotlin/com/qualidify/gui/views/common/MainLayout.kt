package com.qualidify.gui.views.common

import com.qualidify.core.servicelocator.ServiceLocator
import com.qualidify.gui.components.BrandExpression
import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.security.SecurityUtils
import com.qualidify.gui.views.common.MainLayout.Companion.VIEWPORT
import com.qualidify.gui.views.configuration.ConfigurationView
import com.qualidify.gui.views.home.HomeView
import com.qualidify.gui.views.home.LogoutView
import com.qualidify.gui.views.projects.ProjectViewRouter
import com.qualidify.gui.views.reports.ReportsRouter
import com.qualidify.gui.views.tasks.TaskViewRouter
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.HasComponents
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.applayout.AppLayout
import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.page.AppShellConfigurator
import com.vaadin.flow.component.page.Push
import com.vaadin.flow.component.page.Viewport
import com.vaadin.flow.component.tabs.Tab
import com.vaadin.flow.component.tabs.TabVariant
import com.vaadin.flow.component.tabs.Tabs
import com.vaadin.flow.router.Route
import com.vaadin.flow.router.RouterLink

@Viewport(VIEWPORT)
@Push
@CssImport(value = "./styles/styles.css")
class MainLayout : AppLayout(), AppShellConfigurator {

    private val CLASS_NAME = "main-layout"
    private var menu: Tabs? = null
    private val securityUtils: SecurityUtils

    init {
        securityUtils = ServiceLocator.getServiceByType(SecurityUtils::class.java)!!

        setId(CLASS_NAME)
        this.isDrawerOpened = false

        val logo = BrandExpression(QualidifyConstants.QUALIDIFY_NAME)
        logo.addClassName("hide-on-mobile")
        logo.addClickListener { UI.getCurrent().navigate(HomeView::class.java) }

        menu = createMenuTabs()

        this.addToNavbar(logo)
        this.addToNavbar(true, menu)

    }

    private fun createMenuTabs(): Tabs {
        val tabs = Tabs()
        tabs.orientation = Tabs.Orientation.HORIZONTAL
        tabs.add(*getAvailableTabs())
        return tabs
    }

    private fun getAvailableTabs(): Array<Tab> {
        val tabs: MutableList<Tab?> = mutableListOf()
        tabs += createTab(VaadinIcon.INBOX, TITLE_PROJECTS, ProjectViewRouter::class.java)
        tabs += createTab(VaadinIcon.OPEN_BOOK, TITLE_TASKS, TaskViewRouter::class.java)
        tabs += createTab(VaadinIcon.RECORDS, TITLE_REPORTS, ReportsRouter::class.java)
        tabs += createTab(VaadinIcon.COG_O, TITLE_CONFIG, ConfigurationView::class.java)
        tabs += createTab(VaadinIcon.ARROW_RIGHT, TITLE_LOGOUT, LogoutView::class.java)
        return tabs.filterNotNull().toTypedArray()
    }

    private fun createTab(icon: VaadinIcon, title: String, viewClass: Class<out Component>): Tab? {
        if (securityUtils.checkClassAccess(viewClass)) {
            return createTab(populateLink(RouterLink("", viewClass), icon, title), viewClass)
        } else {
            return null
        }
    }

    private fun createTab(content: Component, viewClass: Class<out Component>): Tab {
        val route = viewClass.getAnnotation(Route::class.java).value

        val tab = Tab()
        tab.addThemeVariants(TabVariant.LUMO_ICON_ON_TOP)
        tab.add(content)
        tab.addAttachListener {
            UI.getCurrent().page.fetchCurrentURL {
                tab.isSelected = it.toString().substringAfterLast('/') == route
            }
        }
        return tab
    }

    private fun <T : HasComponents> populateLink(a: T, icon: VaadinIcon, title: String): T {
        a.add(icon.create())
        a.add(title)
        return a
    }

    companion object {
        const val TITLE_PROJECTS = "Projects"
        const val TITLE_TASKS = "Tasks"
        const val TITLE_REPORTS = "Reports"
        const val TITLE_CONFIG = "Configuration"
        const val TITLE_LOGOUT = "Logout"

        const val VIEWPORT =
            "width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes, viewport-fit=cover"
    }

}
