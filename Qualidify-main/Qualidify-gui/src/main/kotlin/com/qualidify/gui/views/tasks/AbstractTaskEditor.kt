package com.qualidify.gui.views.tasks

import com.qualidify.core.util.html.toHtmlCase
import com.qualidify.gui.views.configuration.forms.FormDataConnector
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.gui.providers.DataConnector
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.service.dto.HasIdAndName
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component
import java.io.Serializable

@Component
@UIScope
abstract class AbstractTaskEditor<D>(
    beanType: Class<D>,
    private val dataConnector: DataConnector<D>,
    private val formDataConnector: FormDataConnector,
    private val formFieldComponentFactory: FormFieldComponentFactory
) : FormLayout() where D : Serializable, D : HasIdAndName {

    lateinit var dtoInstance: D

    fun setDto(dto: D) {
        this.dtoInstance = dto
        refresh()
    }

    fun refresh() {
        children.forEach { remove(it) }
        setId(dtoInstance.name.toHtmlCase())
        setSizeFull()
        prepareFormFields(getFormVariables())
        setResponsiveSteps(ResponsiveStep("0", 2))
    }

    private fun prepareFormFields(formVariables: Map<String, Any?>) {
        val taskFormFields: List<ColumnDef> = formDataConnector.loadFormModelMetadataByTaskId(dtoInstance.id)

        val binder = Binder(ObjectMap::class.java)
        val objectMap = ObjectMap(formVariables)
        binder.bean = objectMap

        taskFormFields.forEach { columnDef ->
            val field = formFieldComponentFactory.createInputField(binder, columnDef)
            this.add(field)
        }

        prepareFormButtons(binder, objectMap)

    }

    // implementation of form variable retrieval in child class
    abstract fun getFormVariables(): Map<String, Any?>

    // only required for saving or completing forms (i.e. editable forms)
    open fun prepareFormButtons(binder: Binder<ObjectMap>, objectMap: ObjectMap) {}

}