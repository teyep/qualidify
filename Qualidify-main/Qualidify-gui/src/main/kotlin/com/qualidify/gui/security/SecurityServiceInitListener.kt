package com.qualidify.gui.security
//
//import com.qualidify.gui.views.home.LoginView
//import com.qualidify.service.impl.security.SecurityUtils
//import com.vaadin.flow.router.BeforeEnterEvent
//import com.vaadin.flow.server.ServiceInitEvent
//import com.vaadin.flow.server.UIInitEvent
//import com.vaadin.flow.server.VaadinServiceInitListener
//import org.springframework.context.annotation.Profile
//import org.springframework.security.core.context.SecurityContextHolder
//import org.springframework.stereotype.Component
//
///**
// * Add Security configuration to Vaadin,
// *
// * @author Marcel Pot
// * @since 1.0
// */
//@Profile("!nosec")
//@Component
//class SecurityServiceInitListener : VaadinServiceInitListener {
//
//    override fun serviceInit(event: ServiceInitEvent) {
//        event.source.addUIInitListener { uiEvent: UIInitEvent ->
//            val ui = uiEvent.ui
//            ui.addBeforeEnterListener { event: BeforeEnterEvent -> beforeEnter(event) }
//        }
//    }
//
//    /**
//     * Reroutes the user if she is not authorized to access the view.
//     *
//     * @param event before navigation event with event details
//     */
//    private fun beforeEnter(event: BeforeEnterEvent) {
//        val accessGranted = isAccessGranted(event.navigationTarget)
//        if (!accessGranted) {
//            if (SecurityUtils.isUserLoggedIn) {
//                event.rerouteToError(AccessDeniedException::class.java)
//            } else {
//                event.rerouteTo(LoginView::class.java)
//            }
//        }
//    }
//
//    companion object {
//        /**
//         * Checks if access is granted for the current user for the given secured view,
//         * defined by the view class.
//         *
//         * @param securedClass View class
//         * @return true if access is granted, false otherwise.
//         */
//        private fun isAccessGranted(securedClass: Class<*>): Boolean {
//            val publicView = LoginView::class.java == securedClass
//
//            // Always allow access to public views
//            if (publicView) {
//                return true
//            }
//            val userAuthentication = SecurityContextHolder.getContext().authentication
//
//            // All other views require authentication
//            if (!SecurityUtils.isUserLoggedIn(userAuthentication)) {
//                return false
//            }
//
//            // Allow if no roles are required.
//            return SecurityUtils.checkClassAccess(securedClass)
//        }
//    }
//}