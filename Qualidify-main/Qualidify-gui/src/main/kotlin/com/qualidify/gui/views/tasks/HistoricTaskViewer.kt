package com.qualidify.gui.views.tasks

import com.qualidify.gui.views.configuration.cmmn.HistoricTaskDataConnector
import com.qualidify.gui.views.configuration.forms.FormDataConnector
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.service.process.dto.HistoricTaskDto
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class HistoricTaskViewer(
    private val formDataConnector: FormDataConnector,
    private val formFieldComponentFactory: FormFieldComponentFactory,
    private val historicTaskDataConnector: HistoricTaskDataConnector
) : AbstractTaskEditor<HistoricTaskDto>
    (HistoricTaskDto::class.java, historicTaskDataConnector, formDataConnector, formFieldComponentFactory) {

    override fun getFormVariables() = historicTaskDataConnector.getHistoricFormVariables(dtoInstance.id)

}