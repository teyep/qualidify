package com.qualidify.gui.views.configuration.security

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.security.SecurityGroupView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.security.dto.SecurityGroupDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A View for Security Groups
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(SecurityGroupView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Security.GROUP
)
open class SecurityGroupView(dataCrud: DataCrud<SecurityGroupDto>) :
    AbstractDataCrudTemplate<SecurityGroupDto>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val TITLE = "Security Groups"
        const val IDENTITY = "securitygroups"
    }
}