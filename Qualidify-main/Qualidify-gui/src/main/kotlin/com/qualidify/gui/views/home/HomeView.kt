package com.qualidify.gui.views.home

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.home.HomeView.Companion.ROUTE
import com.qualidify.gui.views.home.HomeView.Companion.TITLE
import com.qualidify.gui.views.projects.ProjectViewRouter
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.UIScope
import javax.annotation.security.RolesAllowed


/**
 * Welcome home, a view for Home
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = ROUTE, layout = MainLayout::class)
@PageTitle(TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@RolesAllowed(
    Roles.Home.VIEW
)
@UIScope
open class HomeView : VerticalLayout() {

    init {
        setId("view-$IDENTITY")
        mainView()
    }

    private fun mainView() {
        val divStartingProject = Div()
        val projectStarter = Button("Start Project")
        projectStarter.addClickListener { UI.getCurrent().navigate(ProjectViewRouter::class.java, "new") }
        divStartingProject.add(projectStarter)

        add(divStartingProject)
    }

    companion object {
        const val TITLE = "Home"
        const val ROUTE = ""
        const val IDENTITY = "home"
    }
}
