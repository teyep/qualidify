package com.qualidify.gui.components.dialogs

import com.qualidify.core.mail.MailService
import com.qualidify.core.servicelocator.ServiceLocator
import com.qualidify.metadata.gui.components.dialogs.DialogTemplate
import com.qualidify.metadata.gui.components.interfaces.ToClipboardCopyable
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.html.H5
import com.vaadin.flow.component.html.Pre
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout

/**
 * A implementation of a Dialog for displaying an Exception on the screen for investigating the problem
 * The Exception can be
 * # mailed
 * # copied to clipboard
 * # read and close the dialog
 *
 * @author Marcel Pot
 * @since 1.0
 */
class ExceptionDisplayDialog(val title: String, val description: String) : DialogTemplate("exception"),
    ToClipboardCopyable {

    init {
        this.maxWidth = "400px"
        this.maxHeight = "400px"

        val titleHeader = H5(title)
        val descriptionContainer = Pre(description)
        descriptionContainer.maxHeight = "320px"
        descriptionContainer.style["overflow"] = "auto"

        val copyButton =
            Button("Copy to Clipboard", VaadinIcon.COPY.create()) { this.doCopyToClipboard(description) }
        copyButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST)

        val mailButton = Button("Mail to Support", VaadinIcon.ENVELOPE_O.create()) { sendMail(description) }
        mailButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST)
        mailButton.getElement().getStyle().set("margin-left", "auto");

        val ok = Button("Close", VaadinIcon.CHECK.create()) { this.close() }
        ok.addThemeVariants(ButtonVariant.LUMO_PRIMARY)

        val buttonLayout = HorizontalLayout(mailButton, copyButton, ok)
        buttonLayout.setWidthFull()
        buttonLayout.alignItems = FlexComponent.Alignment.END

        add(VerticalLayout(titleHeader, descriptionContainer, buttonLayout))
    }

    private fun sendMail(text: String) {
        val mailService = ServiceLocator.getServiceByType(MailService::class.java)
        mailService?.sendSimpleMessage("bugs@qualidify", "Bugreport", text)
        this.close()
    }

}