package com.qualidify.gui.views.configuration.customfield

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.customfield.CustomFieldView.Companion.IDENTITY
import com.qualidify.gui.views.configuration.domain.ConfigurationDomainView.Companion.TITLE
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed


/**
 * A View for Custom Fields
 *
 * @author Marcel Pot
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Data.CUSTOM_FIELDS
)
open class CustomFieldView(dataCrud: DataCrud<CustomFieldDef>) :
    AbstractDataCrudTemplate<CustomFieldDef>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val TITLE = "Custom Fields"
        const val IDENTITY = "custom-fields"
    }

}