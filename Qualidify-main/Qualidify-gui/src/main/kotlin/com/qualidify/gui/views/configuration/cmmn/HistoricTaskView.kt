package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.cmmn.HistoricTaskView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.process.dto.HistoricTaskDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A View for historic tasks
 *
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(HistoricTaskView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Project.HISTORIC_TASK,
)
open class HistoricTaskView(dataCrud: DataCrud<HistoricTaskDto>) :
    AbstractDataCrudTemplate<HistoricTaskDto>(IDENTITY, dataCrud) {

    override fun getTitle(): String = TITLE

    companion object {
        const val IDENTITY = "historictasks"
        const val TITLE = "Historic Tasks"
    }
}