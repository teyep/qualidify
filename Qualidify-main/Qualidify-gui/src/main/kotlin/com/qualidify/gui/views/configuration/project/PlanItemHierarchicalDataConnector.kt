package com.qualidify.gui.views.configuration.project

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service
import java.util.stream.Stream

/**
 * A subscribable hierarchical [DataConnector] taking care of querying and persisting [PlanItemDto] data
 * from the backend by requesting queries and commanding changes and keeping the connection open for updates.
 *
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
class PlanItemHierarchicalDataConnector(
    private val planItemService: PlanItemService
) : AbstractBackEndHierarchicalDataProvider<PlanItemDto, Void>() {

    private val logger by LoggerDelegation()
    val filter = GridFilter()

    // return the number of immediate child counts based on the query (!=filter)
    override fun getChildCount(query: HierarchicalQuery<PlanItemDto, Void>): Int {
        // logger.info("getChildCount() parent: ${query.parent}")
        val count = if (query.parent == null) {
            planItemService.getStages(filter.constraints["cmmnInstanceId"]).size
        } else {
            planItemService.getPlanItemWithStageCount(query.parent).toInt()
        }

        // logger.info("getChildCount() children: $count")
        return count
    }

    // check if a given item should be expandable
    override fun hasChildren(parent: PlanItemDto): Boolean {
        // logger.info("hasChildren() parent: $parent")
        return planItemService.hasPlanItemsWithStage(parent)
    }

    // returns the immediate child items TODO(): based on offset, limit and order
    override fun fetchChildrenFromBackEnd(query: HierarchicalQuery<PlanItemDto, Void>): Stream<PlanItemDto> {
        // logger.info("fetchChildrenFromBackEnd() parent: ${query.parent}")
        if (query.parent == null)
            return planItemService.getStages(filter.constraints["cmmnInstanceId"]).stream()
        else
            return planItemService.getPlanItemsWithStage(query.parent).stream()
    }

    // returns the usereventlisteners belonging to a stage
    fun getUserEventListeners(dto: PlanItemDto) = planItemService.getUserEventListenersWithStage(dto.id)

    fun startUserEvent(dto: PlanItemDto) = planItemService.startPlanItem(dto)

    fun startPlanItem(dto: PlanItemDto) = planItemService.startPlanItem(dto)

}