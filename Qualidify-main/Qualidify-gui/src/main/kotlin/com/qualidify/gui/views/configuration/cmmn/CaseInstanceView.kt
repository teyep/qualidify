package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.cmmn.CaseInstanceView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A View for CMMN Case Instances
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(CaseInstanceView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Project.CASE_INSTANCE
)
open class CaseInstanceView(dataCrud: DataCrud<CaseInstanceDto>) :
    AbstractDataCrudTemplate<CaseInstanceDto>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val IDENTITY = "caseinstances"
        const val TITLE = "Case Instances"
    }
}