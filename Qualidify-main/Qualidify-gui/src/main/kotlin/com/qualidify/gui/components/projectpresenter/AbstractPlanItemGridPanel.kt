package com.qualidify.gui.components.projectpresenter

import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component
import java.io.Serializable

/**
 * Abstract class for showing a panel with a task grid in the project presenter
 *
 * @param D: the type of the dtoInstance
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
abstract class AbstractPlanItemGridPanel<D : Serializable> : VerticalLayout() {

    lateinit var dtoInstance: D

    abstract val title: H2
    abstract val titleBar: HorizontalLayout

    fun setDto(dto: D) {
        this.dtoInstance = dto
        refresh()
    }

    fun refresh() {
        this.children.forEach { remove(it) }
        setStyle()
        addTitleBar()
        addGrid()
    }

    open fun setStyle() {
        setId("panel-${title.text.lowercase().replace(" ","")}")
        title.className = "gridpanel-title"
        setWidthFull()
        isPadding = false
    }

    open fun addTitleBar() {
        titleBar.add(title)
        add(titleBar)
    }

    open fun addGrid() {}

}