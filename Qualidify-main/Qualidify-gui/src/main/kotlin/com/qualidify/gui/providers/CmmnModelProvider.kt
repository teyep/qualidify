package com.qualidify.gui.providers

import com.qualidify.core.query.SearchQuery
import com.qualidify.metadata.gui.providers.RetrieveCollectionItemProvider
import com.qualidify.service.process.CaseDefinitionService
import com.qualidify.service.process.dto.CaseDefinitionDto
import org.springframework.stereotype.Component


/**
 * A data provider to collect the CmmnModels from the backend.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
open class CmmnModelProvider(private val caseDefinitionService: CaseDefinitionService) :
    RetrieveCollectionItemProvider<String>() {

    override fun retrieve(): Collection<String> {
        return caseDefinitionService.findByQuery(SearchQuery<CaseDefinitionDto>())
            .map { it.key }
            .toList()
    }

}