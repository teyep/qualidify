package com.qualidify.gui.components.templates

import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.metadata.gui.providers.DataConnector
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.data.value.ValueChangeMode
import java.io.Serializable

/**
 * A grid class for filtering data and displaying html templates
 *
 * @author Menno Tol
 * @since 1.0
 */
abstract class TemplateGrid<E : Serializable>(
    beanType: Class<E>, dataConnector: DataConnector<E>,
) : Grid<E>(beanType, false) {

    val searchField: TextField = TextField()

    private val filter = GridFilter()
    private val dataProviderWithFilter = dataConnector.withConfigurableFilter()

    init {
        refresh()  // NB: all child classes need to have prepareTemplate() overridden
    }

    fun addConstraints(key: String, value: String?) {
        filter.constraints[key] = value
        refresh()
    }

    fun clearConstraints() {
        filter.constraints.clear()
        refresh()
    }

    fun refresh() {
        setItems(dataProviderWithFilter)
        dataProviderWithFilter.setFilter(filter)
        removeAllColumns()
        addColumn(prepareTemplate())
        dataProvider.refreshAll()
        setSelectionMode(SelectionMode.NONE)  // turn off selection, this will prevent one rpc for a non-existent node
        isDetailsVisibleOnClick = false  // should prevent second rpc for non-existent node, but does not work always
        configureSearchField()
    }

    private fun configureSearchField() {
        searchField.setId("searchfield-${beanType.simpleName.lowercase()}")
        searchField.placeholder = "Search by name"
        searchField.isClearButtonVisible = true
        searchField.valueChangeMode = ValueChangeMode.LAZY
        searchField.addValueChangeListener {
            addConstraints("name", searchField.value)
        }
    }

    // override specific implementation of template in child class
    abstract fun prepareTemplate(): LitRenderer<E>

}