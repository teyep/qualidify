package com.qualidify.gui.error

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.gui.components.Notifications
import com.vaadin.flow.component.UI
import com.vaadin.flow.server.*
import org.springframework.stereotype.Component


/**
 * Connecting an errorhandler to Vaadin
 *
 * @author Marcel Pot
 */
@Component
class ApplicationErrorHandling : VaadinServiceInitListener, SessionInitListener {
    override fun serviceInit(event: ServiceInitEvent) {
        event.source.addSessionInitListener(this)
    }

    @Throws(ServiceException::class)
    override fun sessionInit(event: SessionInitEvent) {
        event.session.errorHandler = SessionErrorHandler()
    }
}

/**
 * The Error Handler for handling unexpected exceptions and errors
 *
 * @author Marcel Pot
 * @since 1.0
 */
internal class SessionErrorHandler : ErrorHandler {

    private val log by LoggerDelegation()

    override fun error(event: ErrorEvent) {
        log.error("Uncaught UI exception", event.throwable)
        if (UI.getCurrent() != null) {
            UI.getCurrent()
                .access {
                    Notifications.warnNotification(
                        "We are sorry, but an internal error occurred? Please contact support.",
                        event.throwable
                    )
                }
        }
    }
}