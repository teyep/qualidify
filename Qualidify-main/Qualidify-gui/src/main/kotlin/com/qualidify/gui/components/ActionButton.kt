package com.qualidify.gui.components

import com.qualidify.metadata.gui.providers.ActionExecutor
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import java.io.Serializable

class ActionButton<D : Serializable>(
    action: String, dto: D, dataConnector: ActionExecutor<D>
) : Button(action) {

    init {
        isEnabled = dataConnector.isActionEnabled(action, dto)
        isVisible = dataConnector.isActionVisible(action, dto)
        addClickListener { dataConnector.executeAction(action, dto) }
        addThemeVariants(ButtonVariant.LUMO_SMALL)
        width = "84px"  // only set together with column with in grid!
        style.set("padding", "0")
    }

}