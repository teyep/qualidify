package com.qualidify.gui.components.projectpresenter

import com.qualidify.gui.views.configuration.cmmn.HistoricTaskDataConnector
import com.qualidify.gui.views.projects.ProjectPresenter
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.service.process.dto.HistoricTaskDto
import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.grid.GridVariant
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

/**
 * Grid for showing historic tasks on the [TasksCompletedGridPanel] of the [ProjectPresenter]
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
@CssImport(value = "./styles/taskgrid.css", themeFor = "vaadin-grid")
class TasksCompletedGrid(
    private val historicTaskDataConnector: HistoricTaskDataConnector,
) : Grid<HistoricTaskDto>() {

    private val filter = GridFilter()
    private val dataProviderWithFilter = historicTaskDataConnector.withConfigurableFilter()

    init {
        addThemeNames("taskgrid-row-height", "taskgrid-without-objects")  // change of default padding required
        addThemeVariants(GridVariant.LUMO_COMPACT)
        setId("datagrid-taskscompleted")
        refresh()
    }

    fun addConstraints(key: String, value: String?) {
        filter.constraints[key] = value
        refresh()
    }

    fun refresh() {
        setItems(dataProviderWithFilter)
        dataProviderWithFilter.setFilter(filter)
        removeAllColumns()
        addComponentColumn { historicTaskDto ->
            val row = TasksCompletedGridRow()
            row.setDto(historicTaskDto)
            return@addComponentColumn row
        }
        dataProvider.refreshAll()
        setSelectionMode(SelectionMode.NONE)  // turn off selection, this will prevent one rpc for a non-existent node
        isDetailsVisibleOnClick = false  // should prevent second rpc for non-existent node, but does not work always
    }

}