package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.SearchQuery
import com.qualidify.gui.components.CmmnViewer
import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.ActionExecutor
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.process.CaseDefinitionService
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service


/**
 * A Subscribable [DataConnector] taking care of querying and persisting [CaseDefinition] data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
open class CaseDefinitionDataConnector(private val caseDefinitionService: CaseDefinitionService
) : AbstractQueryableServiceDataProvider<CaseDefinitionDto>(CaseDefinitionDto::class, caseDefinitionService),
    ActionExecutor<CaseDefinitionDto> {

    private val logger by LoggerDelegation()

    override fun create(dto: CaseDefinitionDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun update(dto: CaseDefinitionDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun delete(dto: CaseDefinitionDto): Outcome<Any> {
        return Outcome.OK
    }

    fun getCaseDefinitionDtos(): Collection<CaseDefinitionDto> =
        caseDefinitionService.findByQuery(SearchQuery<CaseDefinitionDto>())

    private fun diagram(dto: CaseDefinitionDto) {
        val dialog = Dialog(Div(CmmnViewer(getXml(dto.key))))
        dialog.isResizable = true
        dialog.open()
    }

    fun getXml(caseDefinitionKey: String) =
        caseDefinitionService.getXml(caseDefinitionKey)

    inner class InnerAction(
        val action: (CaseDefinitionDto) -> Unit,
    )

    val innerActions: Map<String, InnerAction> = mapOf(
        "Diagram" to InnerAction { diagram(it) }
    )

    override fun executeAction(action: String, dto: CaseDefinitionDto): Outcome<Any> {
        return try {
            innerActions[action]!!.action.invoke(dto)
            Outcome.OK
        } catch (e: Exception) {
            Outcome.fail(e)
        }
    }

    override fun refresh() {
        this.refreshAll()
    }

}