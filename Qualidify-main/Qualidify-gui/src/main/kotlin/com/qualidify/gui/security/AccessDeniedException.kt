package com.qualidify.gui.security

/**
 * An exception indicating a problem with the authorization of a method or page
 *
 * @author Marcel Pot
 * @since 1.0
 */
class AccessDeniedException(message: String? = null, throwable: Throwable? = null) :
    RuntimeException(message, throwable)
