package com.qualidify.gui.views.projects

import com.qualidify.core.util.html.toHtmlCase
import com.qualidify.gui.components.templates.TemplateGrid
import com.qualidify.gui.views.configuration.project.ProjectInstanceDataConnector
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.vaadin.flow.component.UI
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class ProjectList(
    private val projectInstanceDataConnector: ProjectInstanceDataConnector,
) : TemplateGrid<ProjectInstanceDto>(ProjectInstanceDto::class.java, projectInstanceDataConnector) {

    init {
        setId("datagrid-projectlist")  // used to identify the grid on the page
    }

    override fun prepareTemplate(): LitRenderer<ProjectInstanceDto> = LitRenderer.of<ProjectInstanceDto>(
        "<div class=project-info id=\${item.id} @click=\${handleClick}>" +
            "<div class=project-detail>Name: \${item.name}</div> " +
            "<div class=project-detail>CMMN model: \${item.cmmnModel}</div> " +
            "<div class=project-detail>Version: \${item.cmmnModelVersion}</div> " +
        "</div>"
    )
        .withProperty("id") { it.name.toHtmlCase() }
        .withProperty("name") { it.name }
        .withProperty("cmmnModel") { it.cmmnInstanceDto.caseDefinition.name }
        .withProperty("cmmnModelVersion") { it.cmmnInstanceDto.caseDefinition.version ?: "" }

        .withFunction("handleClick") { dto -> UI.getCurrent().navigate("projects/${dto.id}") }

}