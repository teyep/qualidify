package com.qualidify.gui.views.configuration.user

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.user.UserService
import com.qualidify.service.user.dto.UserDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A Subscribable DataConnector taking care of querying and persisting Userdata
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
@VaadinSessionScope
open class UserDataConnector(val userService: UserService) :
    AbstractQueryableServiceDataProvider<UserDto>(UserDto::class, userService) {

    override fun create(dto: UserDto): Outcome<Any> {
        return executeAndWrap { userService.create(dto) }
    }

    override fun update(dto: UserDto): Outcome<Any> {
        return executeAndWrap { userService.update(dto) }
    }

    override fun delete(dto: UserDto): Outcome<Any> {
        return Outcome.FORBIDDEN
    }


}