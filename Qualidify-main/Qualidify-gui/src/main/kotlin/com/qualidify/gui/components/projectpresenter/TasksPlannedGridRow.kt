package com.qualidify.gui.components.projectpresenter

import com.qualidify.gui.components.AbstractGridRow
import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.components.RefreshEventTarget
import com.qualidify.gui.views.configuration.project.PlanItemHierarchicalDataConnector
import com.qualidify.gui.views.projects.ProjectPresenter
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

/**
 * Component for rendering the [PlanItemHierarchicalGrid] on the [TasksPlannedGridPanel] of the [ProjectPresenter].
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
class TasksPlannedGridRow(
    private val planItemHierarchicalDataConnector: PlanItemHierarchicalDataConnector
) : AbstractGridRow<PlanItemDto>() {

    init {
        className = "gridrow"
    }

    fun registerListener(listener: ProjectPresenter) {
        addListener(RefreshEvent::class.java, listener)
    }

    override fun setupClickListener() {
        if (dtoInstance.type == PlanItemType.HUMAN_TASK && dtoInstance.state == PlanItemState.ENABLED) {
            addClickListener {
                planItemHierarchicalDataConnector.startPlanItem(dtoInstance)
                fireEvent(RefreshEvent(this, target = RefreshEventTarget.PLAN_ITEMS))
                fireEvent(RefreshEvent(this, target = RefreshEventTarget.TASKS))
            }
        }
    }

    override fun prepare() {
        val span = Span(
            "${if (dtoInstance.type == PlanItemType.STAGE) "Stage" else "Task"}: ${dtoInstance.name}"
        )
        span.style.set(
            "color",
            if (dtoInstance.type == PlanItemType.HUMAN_TASK && dtoInstance.state == PlanItemState.ENABLED) {
                "black"
            } else if (dtoInstance.type == PlanItemType.STAGE && dtoInstance.state == PlanItemState.ACTIVE) {
                "black"
            } else {
                "hsla(214, 45%, 20%, 0.52)"  // --lumo-tertiary-text-color: hsla(214, 45%, 20%, 0.52) for disabled text with sufficient contrast
            }
        )
        val nameDiv = Div(span)

        add(nameDiv)
    }

}