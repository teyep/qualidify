package com.qualidify.gui.providers

import com.qualidify.core.query.SearchQuery
import com.qualidify.metadata.gui.providers.RetrieveCollectionItemProvider
import com.qualidify.service.user.UserService
import com.qualidify.service.user.dto.UserDto
import org.springframework.stereotype.Repository


/**
 * A data provider to collect the Users from the backend.
 *
 * @author Menno Tol
 * @since 1.0
 */
@Repository("userProvider")
open class UserProvider(private val userService: UserService) : RetrieveCollectionItemProvider<String>() {

    override fun retrieve(): Collection<String> {
        return userService.findByQuery(SearchQuery<UserDto>()).asSequence().map { it.username }.toList()
    }

}