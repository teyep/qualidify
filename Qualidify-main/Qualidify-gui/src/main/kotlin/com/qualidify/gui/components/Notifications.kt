package com.qualidify.gui.components

import com.qualidify.gui.components.dialogs.Dialogs
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.notification.NotificationVariant
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout

/**
 * Abstract Notification centralizing all functionality and displaying properties at one place
 *
 * @author Marcel Pot
 * @since 1.0
 */
class NotificationTemplate(val identity: String) : AbstractNotificationTemplate() {
    init {
        setId("notification-${identity}")
    }
}

/**
 * Abstract Notification centralizing all functionality and displaying properties at one place
 *
 * @author Marcel Pot
 * @since 1.0
 */
abstract class AbstractNotificationTemplate : Notification()


object Notifications {
    private const val MARGIN_RIGHT = "margin-right"
    private const val MARGIN_RIGHT_VALUE = "0.5rem"

    /**
     * Create a warning Notification on the screen
     * @param warning The warning message
     * @param throwable the Throwable if available
     * @return a Notification (already visible)
     */
    @JvmStatic
    fun warnNotification(warning: String, throwable: Throwable?): Notification {
        val notification = NotificationTemplate("warning")
        notification.addThemeVariants(NotificationVariant.LUMO_PRIMARY)

        val warningLabel = Span(warning)
        warningLabel.style[MARGIN_RIGHT] = MARGIN_RIGHT_VALUE

        if (throwable != null) {
            //Also add profile to this check?
            val investigateButton = Button("Investigate", VaadinIcon.BUG.create()) { e ->
                notification.close()
                Dialogs.errorDialog(warning, throwable)
            }
            investigateButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST)
            notification.add(VerticalLayout(warningLabel, investigateButton))
        } else {
            notification.add(warningLabel)
        }
        notification.position = Notification.Position.TOP_CENTER
        notification.duration = 5000
        notification.open()

        return notification
    }

    /**
     * Create an error Notification on the screen
     * @param error The error Warning
     * @param throwable the Throwable if available
     * @return a Notification (already visible)
     */
    @JvmStatic
    fun errorNotification(error: String, throwable: Throwable?): Notification {
        val notification = NotificationTemplate("error")
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR)

        val errorSpan = Span(error)
        errorSpan.style[MARGIN_RIGHT] = MARGIN_RIGHT_VALUE

        val okButton = Button("OK", VaadinIcon.CHECK.create()) { e -> notification.close() }
        okButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
        okButton.style[MARGIN_RIGHT] = MARGIN_RIGHT_VALUE

        if (throwable != null) {
            //Also add profile to this check?
            val investigateButton = Button("Investigate", VaadinIcon.BUG.create()) { e ->
                notification.close()
                Dialogs.errorDialog(error, throwable)
            }
            investigateButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST)

            val layout = VerticalLayout(errorSpan, HorizontalLayout(investigateButton, okButton))
            notification.add(layout)
        } else {
            val layout = VerticalLayout(errorSpan, okButton)
            notification.add(layout)
        }

        notification.position = Notification.Position.MIDDLE
        notification.open()
        return notification
    }

}