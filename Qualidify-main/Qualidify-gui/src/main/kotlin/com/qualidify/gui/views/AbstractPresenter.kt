package com.qualidify.gui.views

import com.qualidify.metadata.gui.providers.DataConnector
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component
import java.io.Serializable

/**
 *
 */
@Component
@UIScope
abstract class AbstractPresenter<D : Serializable>(
    beanType: Class<D>, private val dataConnector: DataConnector<D>,
) : VerticalLayout() {

    lateinit var dtoInstance: D

    fun setDto(dto: D) {
        this.dtoInstance = dto
        refresh()
    }

    fun refresh() {
        this.children.forEach { remove(it) }
        setSizeFull()

        addTitleBar()
        addPanels()
    }

    abstract fun addTitleBar()

    /**
     * Optional function to add more panels below the titlebar
     */
    open fun addPanels() {}

}