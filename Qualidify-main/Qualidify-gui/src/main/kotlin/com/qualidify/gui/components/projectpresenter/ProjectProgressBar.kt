package com.qualidify.gui.components.projectpresenter

import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
//TODO(): add information about stages and milestones (image of cmmn?)
class ProjectProgressBar : HorizontalLayout() {

    private lateinit var projectInstance: ProjectInstanceDto

    init {
        setId("projectprogress")
        setWidthFull()
        style.set("border", "1px solid var(--lumo-shade-20pct)")
    }

    fun setProject(dto: ProjectInstanceDto) {
        projectInstance = dto
        refresh()
    }

    fun refresh() {
        children.forEach { remove(it) }
        addImages()
    }

    private fun addImages() {
        val pieChart = Icon("vaadin", "pie-chart")
        pieChart.setSize("125px")

        val barChart = Icon("vaadin", "bar-chart")
        barChart.setSize("125px")

        add(Div(pieChart))
        add(Div(barChart))
    }
}