package com.qualidify.gui.views.reports.util

import com.qualidify.gui.providers.DownloadLinkProvider
import com.qualidify.gui.views.reports.BatchJobDataConnector
import com.qualidify.service.report.dto.BatchJobDto
import java.util.*

/**
 * Request handler for handling and creating report download links
 *
 * @author Niels Visscher
 * @since 1.0
 */
class ReportDownloadLinkProvider(
    val connector: BatchJobDataConnector,
) : DownloadLinkProvider<BatchJobDto>() {

    override val downloadFilePrefix: String
        get() = "report"
    override val downloadLocationPrefix: String?
        get() = "/downloadreport"
    override val viewLocationPrefix: String?
        get() = "/viewreport"

    override fun getDto(id: UUID): BatchJobDto? = connector.getById(id)

    override fun getMimeType(dto: BatchJobDto): String = connector.getContent(dto.batchJobId)!!.mimeType

    override fun getIdForDto(dto: BatchJobDto): UUID = dto.batchJobId

    override fun getContent(dto: BatchJobDto): ByteArray {
        return if(connector.hasContent(dto)) { connector.getContent(dto.batchJobId)!!.content } else { "".toByteArray() }
    }

    override fun contentIsPresent(dto: BatchJobDto): Boolean = connector.hasContent(dto)

}
