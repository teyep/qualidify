package com.qualidify.gui.views.reports

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.service.report.SenderSideBatchJobService
import com.qualidify.service.report.dto.BatchJobArtifactDto
import com.qualidify.service.report.dto.BatchJobDto
import com.qualidify.service.report.dto.ReportTemplateDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service
import java.util.*

@Service
@VaadinSessionScope
open class BatchJobDataConnector(private val service: SenderSideBatchJobService) :
    AbstractQueryableServiceDataProvider<BatchJobDto>(BatchJobDto::class, service) {

    override fun create(dto: BatchJobDto): Outcome<Any> {
        service.createBatchJob(dto)
        return Outcome.OK
    }

    override fun update(dto: BatchJobDto): Outcome<Any> {
        return Outcome.FORBIDDEN
    }

    override fun delete(dto: BatchJobDto): Outcome<Any> {
        return Outcome.OK
    }

    fun getById(id: UUID): BatchJobDto? {
        return service.getById(id)
    }

    fun createFromReportTemplateDto(reportTemplate: ReportTemplateDto, parameters: ObjectMap) {
        service.scheduleFromReportTemplateDto(reportTemplate, parameters)
    }

    fun hasContent(dto: BatchJobDto): Boolean {
        return service.hasArtifact(dto.batchJobId)
    }

    fun getContent(id: UUID): BatchJobArtifactDto? {
        return service.getArtifact(id)
    }
}