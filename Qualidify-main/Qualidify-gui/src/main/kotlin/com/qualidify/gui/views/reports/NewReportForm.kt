package com.qualidify.gui.views.reports

import com.qualidify.gui.components.dialogs.Dialogs
import com.qualidify.gui.components.templates.TemplateGrid
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.service.report.dto.ReportTemplateDto
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.function.ValueProvider
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Component

/**
 * Build an interface with a view that lets the user choose a template from which to initiate a new report.
 * When a template is chosen, a dialog is presented for specifying additional parameters.
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Component
@VaadinSessionScope
class NewReportForm(
    val reportTemplateDataConnector: ReportTemplateDataConnector,
    val batchJobDataConnector: BatchJobDataConnector,
    val formFieldComponentFactory: FormFieldComponentFactory
) : VerticalLayout() {

    init {
        val titlebar = HorizontalLayout()
        titlebar.add(H2("New report"))
        add(titlebar)
        add(ReportServiceTemplateTemplateGrid(reportTemplateDataConnector, batchJobDataConnector, formFieldComponentFactory))
    }

    private class ReportServiceTemplateTemplateGrid(
        val connector: ReportTemplateDataConnector,
        val batchJobDataConnector: BatchJobDataConnector,
        val formFieldComponentFactory: FormFieldComponentFactory) :
        TemplateGrid<ReportTemplateDto>(ReportTemplateDto::class.java, connector) {

        init {
            // Note: MUST be called unless addConstraint is called, which calls refresh() by itself.
            refresh()
        }

        // Value provider for report template name
        private class ReportTemplateName : ValueProvider<ReportTemplateDto, String> {
            override fun apply(dto: ReportTemplateDto?): String {
                if ((dto == null) || (dto.name.isEmpty())) {
                    return "Unnamed template"
                } else return dto.name
            }
        }

        override fun prepareTemplate(): LitRenderer<ReportTemplateDto> {

            return LitRenderer.of<ReportTemplateDto>(
                "<div class=reporttemplate-row>" +
                "<div clas=reporttemplate-info>" +
                    "<span class=report-template-name>\${item.name}</span>" +
                "</div>" +
                "<div class=reporttemplate-actions>" +
                    "<vaadin-button theme=small id=reporttemplate-start @click=\${startReportEvent}>Start</vaadin-button>" +
                "</div>" +
            "</div>"
            )
                .withProperty("name") { dto -> ReportTemplateName().apply(dto) }
                .withFunction("startReportEvent") { dto -> Dialogs.startWithTemplateDialog(connector, batchJobDataConnector, formFieldComponentFactory, dto) }

        }
    }

}

