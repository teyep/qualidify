package com.qualidify.gui.components

import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.Image

@CssImport("./styles/brand-expression.css")
class BrandExpression(text: String) : Div() {

    private val clazzName = "brand-expression"
    private val logo: Image

    val IMG_PATH = "/images/"

    init {
        setId("brand-expression")
        setClassName(clazzName)
        logo = Image(IMG_PATH + "Qualidify-logo.png", text)
        logo.setAlt("$text logo")
        logo.className = clazzName + "__logo"
        add(logo)
    }
}