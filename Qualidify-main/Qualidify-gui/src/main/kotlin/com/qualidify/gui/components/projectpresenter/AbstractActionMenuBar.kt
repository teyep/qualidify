package com.qualidify.gui.components.projectpresenter

import com.vaadin.flow.component.contextmenu.MenuItem
import com.vaadin.flow.component.contextmenu.SubMenu
import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.menubar.MenuBar
import com.vaadin.flow.component.menubar.MenuBarVariant

/**
 * Abstract class for building a menubar for actions to be executed on a dto.
 *
 * @author Menno Tol
 * @since 1.0
 */
@CssImport(value = "./styles/menubar.css", themeFor = "vaadin-menu-bar")
abstract class AbstractActionMenuBar : MenuBar() {

    val menuItem: MenuItem = addItem(Icon(VaadinIcon.MENU))
    val subMenu: SubMenu = menuItem.subMenu

    fun prepare() {
        addThemeName("menubar")
        addThemeVariants(MenuBarVariant.LUMO_TERTIARY_INLINE)
        element.setAttribute("class", "gridrow-menubar")

        menuItem.removeThemeNames("menu-bar-item")
        menuItem.addThemeNames("tertiary-inline")
        menuItem.element.setAttribute("class", "gridrow-menuitem")

        prepareSubMenu()
    }

    /**
     * Submenu contains the action items. Implementation requires a list of action items.
     * Actions should be executed via the corresponding dataconnector.
     */
    abstract fun prepareSubMenu()

}