package com.qualidify.gui.components.dialogs

import com.qualidify.gui.views.reports.BatchJobDataConnector
import com.qualidify.gui.views.reports.ReportTemplateDataConnector
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.service.report.dto.ReportTemplateDto

import com.vaadin.flow.component.dialog.Dialog


/**
 * A centralized place for creating Vaadin Dialogs
 * to have the common styling at one place
 *
 * @author Marcel Pot
 */
object Dialogs {
    private const val MARGIN_RIGHT = "margin-right"
    private const val MARGIN_RIGHT_VALUE = "0.5rem"


    /**
     * Create an error Dialog on the screen
     * @param error The error Warning
     * @param throwable the Throwable
     * @return a Dialog (already visible)
     */
    @JvmStatic
    fun errorDialog(error: String, throwable: Throwable): Dialog {
        val dialog = ExceptionDisplayDialog(error, throwable.stackTraceToString())
        dialog.show()
        return dialog
    }

    @JvmStatic
    fun startWithTemplateDialog(templateDataConnector: ReportTemplateDataConnector, batchJobConnector: BatchJobDataConnector, formFieldComponentFactory: FormFieldComponentFactory, dto: ReportTemplateDto): Dialog {
        val dialog = StartWithTemplateDialog(templateDataConnector, batchJobConnector, formFieldComponentFactory, dto)
        dialog.show()
        return dialog
    }
}

