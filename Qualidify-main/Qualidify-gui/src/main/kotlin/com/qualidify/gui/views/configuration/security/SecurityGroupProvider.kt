package com.qualidify.gui.views.configuration.security

import com.qualidify.gui.providers.QueryUtil
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.metadata.gui.providers.ItemProvider
import com.qualidify.service.security.SecurityService
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider
import com.vaadin.flow.data.provider.Query
import org.springframework.stereotype.Repository
import java.util.stream.Stream


/**
 * A Data Provider to collect the Security Groups from the backend.
 * This class also implements the interface ItemLabelGenerator, to
 * describe the Security Group
 *
 * @author Marcel Pot
 */
@Repository("securityGroupProvider")
open class SecurityGroupProvider(val securityService: SecurityService) :
    AbstractBackEndDataProvider<String, GridFilter>(), ItemProvider<String, GridFilter> {

    override fun sizeInBackEnd(query: Query<String, GridFilter>): Int {
        val countQuery = QueryUtil.mapCountQuery(String::class, query)
        return securityService.countByQuery(countQuery).count.toInt()
    }

    override fun fetchFromBackEnd(query: Query<String, GridFilter>): Stream<String> {
        val searchQuery = QueryUtil.mapSearchQuery(String::class, query)
        return securityService.findByQuery(searchQuery)
            .map { it.name }
            .stream()
    }

    override fun apply(item: String) = item
}
