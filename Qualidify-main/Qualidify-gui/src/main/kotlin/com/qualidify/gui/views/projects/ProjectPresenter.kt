package com.qualidify.gui.views.projects

import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.components.RefreshEventTarget
import com.qualidify.gui.components.projectpresenter.*
import com.qualidify.gui.views.AbstractPresenter
import com.qualidify.gui.views.configuration.project.ProjectInstanceDataConnector
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.vaadin.flow.component.ComponentEventListener
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

/**
 * Main view for displaying information about a [ProjectInstance].
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
class ProjectPresenter(
    private val projectInstanceDataConnector: ProjectInstanceDataConnector,
    private val projectTitleBar: ProjectTitleBar,
    private val projectProgressBar: ProjectProgressBar,
    private val tasksPlannedGridPanel: TasksPlannedGridPanel,
    private val tasksOngoingGridPanel: TasksOngoingGridPanel,
    private val tasksCompletedGridPanel: TasksCompletedGridPanel,
    private val planItemHierarchicalGrid: PlanItemHierarchicalGrid,
    private val tasksOngoingGrid: TasksOngoingGrid,
) : AbstractPresenter<ProjectInstanceDto>(ProjectInstanceDto::class.java, projectInstanceDataConnector),
    ComponentEventListener<RefreshEvent<*>> {

    override fun addTitleBar() {
        projectTitleBar.registerListener(this)
        projectTitleBar.setDto(dtoInstance)
        add(projectTitleBar)
    }

    override fun addPanels() {
        addProgressBar()
        addTaskPanels()
    }

    private fun addProgressBar() {
        projectProgressBar.setProject(dtoInstance)
        add(projectProgressBar)
    }

    private fun addTaskPanels() {
        planItemHierarchicalGrid.registerListener(this)
        tasksOngoingGrid.registerListener(this)

        tasksPlannedGridPanel.setDto(dtoInstance)
        tasksOngoingGridPanel.setDto(dtoInstance)
        tasksCompletedGridPanel.setDto(dtoInstance)

        val layout = HorizontalLayout(tasksPlannedGridPanel, tasksOngoingGridPanel, tasksCompletedGridPanel)
        layout.setWidthFull()
        layout.isPadding = false
        add(layout)
    }

    override fun onComponentEvent(event: RefreshEvent<*>) {
        when (event.target) {
            RefreshEventTarget.DETAIL_BAR -> projectTitleBar.refresh()
            RefreshEventTarget.PROGRESS_BAR -> projectProgressBar.refresh()
            RefreshEventTarget.PLAN_ITEMS -> tasksPlannedGridPanel.refresh()
            RefreshEventTarget.TASKS -> tasksOngoingGridPanel.refresh()
            RefreshEventTarget.HISTORIC_TASKS -> tasksCompletedGridPanel.refresh()
            else -> refresh()
        }
    }

}

