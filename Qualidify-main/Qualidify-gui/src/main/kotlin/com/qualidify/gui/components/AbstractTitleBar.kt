package com.qualidify.gui.components

import com.qualidify.core.util.html.toHtmlCase
import com.qualidify.service.dto.HasIdAndName
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

/**
 * Abstract class providing a unified title bar.
 * @param [D]: the dto type
 * @param [P]: the type of the presenter that listens to refresh events
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
abstract class AbstractTitleBar<D: HasIdAndName, P> : VerticalLayout() {

    lateinit var dtoInstance: D

    init {
        style.set("border", "1px solid var(--lumo-shade-20pct)")
        isSpacing = false
    }

    fun setDto(dto: D) {
        this.dtoInstance = dto
        refresh()
    }

    abstract fun registerListener(listener: P)

    fun refresh() {
        this.children.forEach { remove(it) }
        setWidthFull()

        val titleBox = TitleBox()
        val detailBox = DetailBox()
        val buttonBox = ButtonBox()
        prepareDetailBox(detailBox)

        val detailAndButtonsBox = DetailAndButtonBox(detailBox, buttonBox)

        add(titleBox)
        add(detailAndButtonsBox)
    }

    // abstract fun to modify a specific detail box in a child class
    abstract fun prepareDetailBox(detailBox: DetailBox): DetailBox

    abstract fun addButtons(): Array<ActionButton>

    /**
     * Inner class for generalized action buttons
     */
    inner class ActionButton(text:String?): Button(text) {
        init {
            setId("button-${text?.toHtmlCase()}")
        }
    }

    inner class TitleBox: HorizontalLayout() {
        init {
            setId("titlebox")
            width = "100%"
            alignItems = FlexComponent.Alignment.BASELINE
            val title = H2(dtoInstance.name)
            title.style.set("margin-top", "0")
            add(title)
        }
    }

    inner class DetailBox: FormLayout() {
        init {
            setId("detailbox")
            setResponsiveSteps(ResponsiveStep("0", 8))
        }
    }

    /**
     * Buttons are aligned in a grid with max two rows. The grid is a FormLayout with the number of columns
     * calculated from the number of buttons: columns = buttons/2 + buttons%2
     */
    inner class ButtonBox: FormLayout() {
        init {
            setId("buttonbox")
            val columns = (addButtons().size / 2) + (addButtons().size % 2)
            setResponsiveSteps(ResponsiveStep("0", columns))
            add(*addButtons())
        }
    }

    inner class DetailAndButtonBox(detailBox: DetailBox, buttonBox: ButtonBox): HorizontalLayout() {
        init {
            setId("detailsandbuttonbox")
            isPadding = false
            isSpacing = false
            isMargin = false
            width = "100%"
            setVerticalComponentAlignment(FlexComponent.Alignment.END, detailBox)
            add(detailBox, buttonBox)
        }
    }

}