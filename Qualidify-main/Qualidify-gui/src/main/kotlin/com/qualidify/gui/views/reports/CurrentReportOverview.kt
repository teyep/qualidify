package com.qualidify.gui.views.reports

import com.qualidify.gui.components.dialogs.ConfirmDialog
import com.qualidify.gui.components.templates.TemplateGrid
import com.qualidify.gui.providers.DownloadLinkProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.report.dto.BatchJobDto
import com.vaadin.flow.component.AttachEvent
import com.vaadin.flow.component.DetachEvent
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.server.VaadinSession
import com.vaadin.flow.spring.annotation.VaadinSessionScope

/**
 * An overview of currently saved reports
 *
 * @author Niels Visscher
 * @since 1.0
 */
@VaadinSessionScope
class CurrentReportOverview(
    val batchJobDataConnector: BatchJobDataConnector,
    val downloadRequestHandler: DownloadLinkProvider<BatchJobDto>
) : VerticalLayout() {

    init {
        val titlebar = HorizontalLayout()
        titlebar.alignItems = FlexComponent.Alignment.BASELINE
        titlebar.add(H2("Reports"))
        titlebar.add(NewObjectButton())
        add(titlebar)
        val reportTemplateGrid = ReportTemplateGrid(batchJobDataConnector, downloadRequestHandler)
        add(reportTemplateGrid)
        reportTemplateGrid.refresh()
    }

    override fun onAttach(attachEvent: AttachEvent?) {
        super.onAttach(attachEvent)
        VaadinSession.getCurrent().addRequestHandler(downloadRequestHandler)
    }

    override fun onDetach(detachEvent: DetachEvent?) {
        super.onDetach(detachEvent)
        // Note: If we start downloads in the current tab, this will be done before the custom request is handled!
        VaadinSession.getCurrent().removeRequestHandler(downloadRequestHandler)
    }

    /**
     * A button with a click listener that initiates navigating to a specific address
     */
    private class NewObjectButton() : Button("New", Icon(VaadinIcon.PLUS)) {
        init {
            addClickListener { UI.getCurrent().navigate(ReportsRouter.ROUTE + "/new") }
            addThemeVariants(ButtonVariant.LUMO_LARGE)
        }
    }

    private class ReportTemplateGrid(
        val connector: BatchJobDataConnector,
        val downloadRequestHandler: DownloadLinkProvider<BatchJobDto>
    ) : TemplateGrid<BatchJobDto>(BatchJobDto::class.java, connector) {


        init {
        }

        private fun confirmDeletion(dto: BatchJobDto): Outcome<Any> {
            val outcome = connector.delete(dto)
            if (outcome == Outcome.OK) {
                refresh()
            }
            return outcome
        }

        private fun requestDeletion(dto: BatchJobDto) {
            val dialog = ConfirmDialog(
                "Are you sure you want to delete this report?",
                "Yes",
                "Cancel",
                {
                    confirmDeletion(dto)
                },
                {},
                confirmButtonWarning = true
            )
            dialog.open()
        }

        private fun getReportName(dto: BatchJobDto?) : String {
            return if((dto == null) || dto.jobName.isEmpty()) "Empty report" else dto.jobName
        }

        /**
         * Returns the text describing the current status of the report in the current reports table
         */
        private fun getReportStatus(dto: BatchJobDto?) : String {
            return when (dto?.status) {
                "FINISHED" -> "Finished"
                "INPROGRESS" -> "In progress"
                "FAILED" -> "Failed"
                else -> "Unknown status"
            }
        }

        override fun prepareTemplate(): LitRenderer<BatchJobDto> {

            return LitRenderer.of<BatchJobDto>(
                "<div class=report-row>" +
                    "<div class='current-reports-cell report-name'>\${item.name}</div> " +
                    "<div class='current-reports-cell report-status'>\${item.status}</div> " +
                    "<div class='current-reports-cell report-actions'>" +
                        "<a class='button' @disabled=\${item.downloadDisabled} href=\${item.viewLink} target='_blank'>View</a>" +
                        "<a class='button' @disabled=\${item.downloadDisabled} href=\${item.downloadLink} target='_blank'>Download</a>" +
                        "<vaadin-button theme=small id=report-result-delete @click=\${deleteEvent}>" +
                            "<vaadin-icon icon=vaadin:close></vaadin-icon> " +
                            "<span>Delete</span>" +
                        "</vaadin-button>" +
                        "</div>" +
                    "</div>"
            )
                .withProperty("name") { dto -> getReportName(dto) }
                .withProperty("status") { dto -> getReportStatus(dto) }
                .withProperty("downloadDisabled") { dto -> ! connector.hasContent(dto) }
                .withProperty("viewLink") { dto -> downloadRequestHandler.createViewLink(dto.batchJobId) }
                .withProperty("downloadLink") { dto -> downloadRequestHandler.createDownloadLink(dto.batchJobId) }
                .withFunction("deleteEvent") { dto -> requestDeletion(dto) }
        }
    }

}