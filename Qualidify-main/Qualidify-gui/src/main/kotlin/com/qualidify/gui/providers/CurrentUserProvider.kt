package com.qualidify.gui.providers

import com.qualidify.gui.security.SecurityUtils
import com.qualidify.service.security.CurrentUser
import org.springframework.stereotype.Component

/**
 * Returns information about the current logged-in user
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
open class CurrentUserProvider(val securityUtils: SecurityUtils) {

    open fun getUser(): CurrentUser = securityUtils.currentUser

    open fun getUserName(): String = securityUtils.currentUserName ?: "anonymousUser"

}