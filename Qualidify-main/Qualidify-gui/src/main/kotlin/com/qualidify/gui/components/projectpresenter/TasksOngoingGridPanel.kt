package com.qualidify.gui.components.projectpresenter

import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class TasksOngoingGridPanel(private val grid: TasksOngoingGrid) : AbstractPlanItemGridPanel<ProjectInstanceDto>() {

    override val title = H2("Tasks ongoing")
    override val titleBar = HorizontalLayout()

    override fun addGrid() {
        grid.addConstraints("cmmnInstanceId", dtoInstance.cmmnInstanceId)
        add(grid)
    }

}