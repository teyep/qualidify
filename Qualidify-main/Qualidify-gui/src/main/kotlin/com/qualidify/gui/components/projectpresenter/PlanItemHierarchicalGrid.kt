package com.qualidify.gui.components.projectpresenter

import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.views.configuration.project.PlanItemHierarchicalDataConnector
import com.qualidify.gui.views.projects.ProjectPresenter
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.grid.GridVariant
import com.vaadin.flow.component.treegrid.TreeGrid
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component


@Component
@UIScope
@CssImport(value = "./styles/taskgrid.css", themeFor = "vaadin-grid")
class PlanItemHierarchicalGrid(
    private val planItemHierarchicalDataConnector: PlanItemHierarchicalDataConnector,
) : TreeGrid<PlanItemDto>(planItemHierarchicalDataConnector) {

    private lateinit var projectPresenter: ProjectPresenter

    val filter = planItemHierarchicalDataConnector.filter  // implement the filter in the dataconnector

    init {
        addThemeNames("taskgrid-row-height", "taskgrid-with-objects")
        addThemeVariants(GridVariant.LUMO_COMPACT)
        setId("datagrid-planitemtree")
        setClassName("datagrid-planitem")
        setDataProvider(planItemHierarchicalDataConnector)
        refresh()
    }

    fun registerListener(listener: ProjectPresenter) {
        addListener(RefreshEvent::class.java, listener)
        projectPresenter = listener
    }

    fun addConstraints(key: String, value: String?) {
        filter.constraints[key] = value
        refresh()
    }

    fun refresh() {
        removeAllColumns()

        addComponentHierarchyColumn { planItemDto ->
            if (planItemDto.state == PlanItemState.ACTIVE) expand(planItemDto)

            val row = TasksPlannedGridRow(planItemHierarchicalDataConnector)
            row.setDto(planItemDto)
            row.registerListener(projectPresenter)
            return@addComponentHierarchyColumn row
        }

        addComponentColumn { planItemDto ->
            val menuBar = PlanItemActionMenuBar(planItemDto, planItemHierarchicalDataConnector)
            menuBar.prepare()
            return@addComponentColumn menuBar
        }.apply {
            /**
             * The minimal width of the menubar column seems to be 44px. Can only be used together with flexGrow = 0
             * and the menubar.css theme file set in the AbstractActionMenuBar. Required to right align the menubar.
             */
            flexGrow = 0
            width = "44px"
        }

        dataProvider.refreshAll()
        setSelectionMode(SelectionMode.NONE)  // turn off selection, this will prevent one rpc for a non-existent node
        isDetailsVisibleOnClick = false  // should prevent second rpc for non-existent node, but does not work always
    }

}

