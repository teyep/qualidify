package com.qualidify.gui.views.tasks

import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.components.RefreshEventTarget
import com.qualidify.gui.components.taskpresenter.TaskTitleBar
import com.qualidify.gui.views.AbstractPresenter
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.service.projects.dto.TaskDto
import com.vaadin.flow.component.ComponentEventListener
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class TaskPresenter(
    private val taskDataConnector: TaskDataConnector,
    private val taskTitleBar: TaskTitleBar,
) : AbstractPresenter<TaskDto>(TaskDto::class.java, taskDataConnector),
    ComponentEventListener<RefreshEvent<*>> {

    override fun addTitleBar() {
        taskTitleBar.registerListener(this)
        taskTitleBar.setDto(dtoInstance)
        add(taskTitleBar)
    }

    override fun onComponentEvent(event: RefreshEvent<*>) {
        when (event.target) {
            RefreshEventTarget.DETAIL_BAR -> taskTitleBar.refresh()
            else -> refresh()
        }
    }

}

