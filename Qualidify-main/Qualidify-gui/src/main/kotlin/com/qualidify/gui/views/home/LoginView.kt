package com.qualidify.gui.views.home

import com.qualidify.service.configuration.QualidifyConstants
import com.vaadin.flow.component.html.H1
import com.vaadin.flow.component.login.LoginForm
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.BeforeEnterEvent
import com.vaadin.flow.router.BeforeEnterObserver
import com.vaadin.flow.router.Route
import javax.annotation.security.PermitAll

@Route("login")
@PermitAll
open class LoginView : VerticalLayout(),
    BeforeEnterObserver {

    val form: LoginForm = LoginForm()

    init {
        addClassName("login-view")
        setSizeFull()
        alignItems = FlexComponent.Alignment.CENTER
        justifyContentMode = FlexComponent.JustifyContentMode.CENTER
        form.action = "login"
        add(H1(QualidifyConstants.QUALIDIFY_NAME), form)
    }


    override fun beforeEnter(event: BeforeEnterEvent) {
        if (event.location.queryParameters.parameters.containsKey("error")) {
            form.isError = true
        }

    }

}

