package com.qualidify.gui.components.dialogs

import com.qualidify.gui.views.reports.BatchJobDataConnector
import com.qualidify.gui.views.reports.ReportTemplateDataConnector
import com.qualidify.metadata.gui.components.dialogs.DialogTemplate
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.service.report.dto.ReportTemplateDto
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.H5
import com.vaadin.flow.component.html.Paragraph
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.binder.StatusChangeEvent
import com.vaadin.flow.data.binder.StatusChangeListener

/**
 * Dialog guiding the user through generating a new report using a given template
 *
 * @author Niels Visscher
 * @since 1.0
 */
class StartWithTemplateDialog(
    val reportTemplateDataConnector: ReportTemplateDataConnector,
    val batchJobDataConnector: BatchJobDataConnector,
    val formFieldComponentFactory: FormFieldComponentFactory,
    val templateDto: ReportTemplateDto
    )
    : DialogTemplate("startwithtemplate"), StatusChangeListener {

        var okButton: Button

        var paramBinder = Binder<ObjectMap>()

        private fun createParameterFields() : Component {
            val container = Div()
            reportTemplateDataConnector.getParametersForTemplate(templateDto).forEach {
                container.add(
                    Div(
                        formFieldComponentFactory.createInputField(paramBinder, it, true)
                    )
                )
            }
            return container
        }

        init {
            // Button layout
            val cancelButton = Button("Cancel", VaadinIcon.STOP.create()) {
                paramBinder.removeBean()
                close()
            }
            okButton = Button("OK", VaadinIcon.PLAY.create()) {
                finishDialog()

                close()
            }
            val buttonLayout = HorizontalLayout(cancelButton, okButton)

            paramBinder.bean = ObjectMap()
            paramBinder.addStatusChangeListener(this)

            add(
                H5("Start new report: " + templateDto.name),
                Paragraph(templateDto.description),
                createParameterFields(),
                buttonLayout
            )
        }

        private fun finishDialog() {
            batchJobDataConnector.createFromReportTemplateDto(templateDto, paramBinder.bean)
            close()
        }

    /**
     * Implementation of StatusChangeListener. Called when the inputs change.
     * Used to conditionally enable the OK button
     */
    override fun statusChange(changeEvent: StatusChangeEvent?) {
        // A parameter was updated
        if(changeEvent != null) {
            okButton.isEnabled = this.paramBinder.isValid()
        }
    }
}
