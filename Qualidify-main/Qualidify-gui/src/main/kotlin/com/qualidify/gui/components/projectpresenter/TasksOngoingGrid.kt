package com.qualidify.gui.components.projectpresenter

import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.gui.views.projects.ProjectPresenter
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.service.projects.dto.TaskDto
import com.vaadin.flow.component.dependency.CssImport
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.grid.GridVariant
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

/**
 * Grid for showing tasks on the [TasksOngoingGridPanel] of the [ProjectPresenter]
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
@CssImport(value = "./styles/taskgrid.css", themeFor = "vaadin-grid")
class TasksOngoingGrid(
    private val taskDataConnector: TaskDataConnector,
) : Grid<TaskDto>() {

    private lateinit var presenter: ProjectPresenter

    val filter = GridFilter()
    private val dataProviderWithFilter = taskDataConnector.withConfigurableFilter()

    init {
        addThemeNames("taskgrid-row-height", "taskgrid-with-objects")  // change of default padding required
        addThemeVariants(GridVariant.LUMO_COMPACT)
        setId("datagrid-tasksongoing")
        refresh()
    }

    fun registerListener(listener: ProjectPresenter) {
        addListener(RefreshEvent::class.java, listener)
        presenter = listener
    }

    fun addConstraints(key: String, value: String?) {
        filter.constraints[key] = value
        refresh()
    }

    fun refresh() {
        setItems(dataProviderWithFilter)
        dataProviderWithFilter.setFilter(filter)

        removeAllColumns()
        addColumns()

        dataProvider.refreshAll()
        setSelectionMode(SelectionMode.NONE)  // turn off selection, this will prevent one rpc for a non-existent node
        isDetailsVisibleOnClick = false  // should prevent second rpc for non-existent node, but does not work always
    }

    private fun addColumns() {
        addComponentColumn { taskDto ->
            val row = TasksOngoingGridRow(taskDataConnector)
            row.setDto(taskDto)
            row.registerListener(presenter)
            return@addComponentColumn row
        }

        addComponentColumn { taskDto ->
            val menuBar = TasksOngoingActionMenuBar(taskDto, taskDataConnector)
            menuBar.prepare()
            menuBar.registerListener(presenter)
            return@addComponentColumn menuBar
        }.apply {
            /**
             * The minimal width of the menubar column seems to be 44px. Can only be used together with flexGrow = 0
             * and the menubar.css theme file set in the AbstractActionMenuBar. Required to right align the menubar.
             */
            flexGrow = 0
            width = "44px"
        }
    }

}
