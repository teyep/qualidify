package com.qualidify.gui.components.dialogs

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout

/**
 * Simple dialog with confirm and cancel buttons.
 *
 * @param text:                 Description of dialog
 * @param confirmButtonText:    Text on the confirm button (i.e. "Yes" or "Confirm")
 * @param cancelButtonText:     Text on the cancel button (i.e. "No" or "Cancel")
 * @param actionFunction:       Function to execute when clicking yes
 * @param closeFunction:        Function to execute when closing the dialog (i.e. forward to a uri)
 * @param confirmButtonWarning  True if the confirm button requires a red error theme
 *
 * @author Menno Tol
 * @since 1.0
 */
class ConfirmDialog(
    private val text: String,
    private val confirmButtonText: String = "Confirm",
    private val cancelButtonText: String = "Cancel",
    private val actionFunction: () -> Unit,
    private val closeFunction: (() -> Unit)?,
    private val confirmButtonWarning: Boolean = false
) : Dialog() {

    init {
        val buttons = HorizontalLayout(confirmButton(), cancelButton())
        val layout = VerticalLayout(Span(text), buttons)
        layout.setHorizontalComponentAlignment(FlexComponent.Alignment.END, buttons)  // align buttons right

        add(layout)
        isCloseOnEsc
        isCloseOnOutsideClick
    }

    private fun cancelButton(): Button {
        val button = Button(cancelButtonText)
        button.setId(cancelButtonText.lowercase().replace(" ", "-"))
        button.addClickListener {
            close()
        }

        return button
    }

    private fun confirmButton(): Button {
        val button = Button(confirmButtonText)
        button.setId(confirmButtonText.lowercase().replace(" ", "-"))
        button.addClickListener {
            actionFunction.invoke()
            close()
            closeFunction?.invoke()
        }

        if (confirmButtonWarning) {
            button.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR)
        }

        return button
    }

}