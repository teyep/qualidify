package com.qualidify.gui.components

import com.vaadin.flow.component.Component
import com.vaadin.flow.component.ComponentEvent

/**
 * Event that can be fired to refresh a certain target (view, presenter, component). Classes of type component
 * are generalized. Different targets can be defined using the enum class [RefreshEventTarget].
 *
 * @param source the generalized component class
 *
 * @author Menno Tol
 * @since 1.0
 */
class RefreshEvent<T: Component>(
    source: T, val target: RefreshEventTarget? = null, fromClient: Boolean = true
) : ComponentEvent<T>(source, fromClient)

enum class RefreshEventTarget {
    PLAN_ITEMS, TASKS, HISTORIC_TASKS, DETAIL_BAR, PROGRESS_BAR
}