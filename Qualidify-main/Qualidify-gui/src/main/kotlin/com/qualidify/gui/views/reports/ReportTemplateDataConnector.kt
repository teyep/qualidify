package com.qualidify.gui.views.reports

import com.fasterxml.jackson.databind.ObjectMapper
import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.report.ReportTemplateService
import com.qualidify.service.report.dto.ReportTemplateDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service
import java.util.*

/**
 * Data Connector for querying and persisting report templates
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Service
@VaadinSessionScope
open class ReportTemplateDataConnector
    (
    val service: ReportTemplateService,
    val objectMapper: ObjectMapper
) : AbstractQueryableServiceDataProvider<ReportTemplateDto>(ReportTemplateDto::class, service) {

    override fun create(dto: ReportTemplateDto): Outcome<Any> {
        service.create(dto)
        refreshAll()
        return Outcome.OK
    }

    override fun update(dto: ReportTemplateDto): Outcome<Any> {
        return Outcome.FORBIDDEN
    }

    override fun delete(dto: ReportTemplateDto): Outcome<Any> {
        service.deleteById(dto.id)
        return Outcome.OK

    }

    // - Fetching report templates

    fun getTemplateById(id: UUID): ReportTemplateDto {
        return service.getTemplateById(id)
    }

    fun getAllTemplates(): Collection<ReportTemplateDto> {
        return service.getAllTemplates()
    }

    fun getParametersForTemplate(dto: ReportTemplateDto): Array<ColumnDef> {
        return try {
            objectMapper.readValue(dto.parameters, Array<ColumnDef>::class.java) as Array<ColumnDef>
        } catch (e: Exception) {
            arrayOf()
        }
    }
}
