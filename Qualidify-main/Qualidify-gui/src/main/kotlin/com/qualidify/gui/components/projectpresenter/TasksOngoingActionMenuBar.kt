package com.qualidify.gui.components.projectpresenter

import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.gui.views.projects.ProjectPresenter
import com.qualidify.service.projects.dto.TaskDto

class TasksOngoingActionMenuBar(
    private val taskDto: TaskDto,
    private val taskDataConnector: TaskDataConnector
) : AbstractActionMenuBar() {

    fun registerListener(listener: ProjectPresenter) {
        addListener(RefreshEvent::class.java, listener)
    }

    override fun prepareSubMenu() {
        taskDataConnector.innerActions.keys
            .forEach { action ->
                val item = subMenu.addItem(action) {
                    taskDataConnector.executeAction(action, taskDto)
                    fireEvent(RefreshEvent(this))  // all panels will be refreshed
                }
                item.isEnabled = taskDataConnector.isActionEnabled(action, taskDto)
                item.isVisible = taskDataConnector.isActionVisible(action, taskDto)
            }
    }

}