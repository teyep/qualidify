package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.gui.components.dialogs.AssignTaskDialog
import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.gui.providers.CurrentUserProvider
import com.qualidify.gui.providers.UserProvider
import com.qualidify.metadata.gui.providers.ActionExecutor
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.ReviewResponse
import com.qualidify.service.quality.ReviewResponseRouter
import com.vaadin.flow.component.UI
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service
import java.net.URL

/**
 * A subscribable [DataConnector] taking care of querying and persisting CMMN Project Task data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
class TaskDataConnector(
    private val taskService: TaskService,
    private val reviewResponseRouter: ReviewResponseRouter,
    private val currentUserProvider: CurrentUserProvider,
    private val userProvider: UserProvider,
) : AbstractQueryableServiceDataProvider<TaskDto>(TaskDto::class, taskService), ActionExecutor<TaskDto> {

    val currentUser: String
        get() {
            // custom getter for when currentUser is actually called, returns anonymousUser in dev environment
            return currentUserProvider.getUserName()
        }

    private var redirectUrl: URL? = null

    fun setRedirect(url: URL) {
        redirectUrl = url
    }

    override fun create(dto: TaskDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun update(dto: TaskDto): Outcome<Any> {
        return Outcome.OK
    }

    override fun delete(dto: TaskDto): Outcome<Any> {
        return Outcome.OK
    }

    fun complete(dto: TaskDto) {
        redirect()
        taskService.completeOrResolveTask(dto, currentUser).block()
        refresh()
    }

    /**
     * Assign a user to a task.
     *
     * @param closeFunction: Optional function to be executed in the dialog
     */
    fun assign(dto: TaskDto, closeFunction: (() -> Unit)? = null) {
        val users: Set<String> = userProvider.retrieve().toSet() + currentUser

        val dialog = AssignTaskDialog(taskService, this, dto, users, currentUser, closeFunction)
        dialog.open()

        refresh()
    }

    fun claim(dto: TaskDto) {
        taskService.claimTask(dto, currentUser).block()
        refresh()
    }

    fun unclaim(dto: TaskDto) {
        taskService.unclaimTask(dto, currentUser).block()
        refresh()
    }

    fun approve(dto: TaskDto) {
        reviewResponseRouter.routeRequest(dto, currentUser, ReviewResponse.APPROVE).block()
        refresh()
    }

    fun reject(dto: TaskDto) {
        reviewResponseRouter.routeRequest(dto, currentUser, ReviewResponse.REJECT).block()
        refresh()
    }

    inner class InnerAction(
        val action: (TaskDto) -> Unit,
        val isEnabled: (TaskDto) -> Boolean,
        val isVisible: (TaskDto) -> Boolean,
    )

    /**
     *  Matrix to define under which conditions actions (buttons) are enabled or visible.
     *
     *  As a general rule:
     *  - all user actions are visible, actions for which to user has no right should not be visible
     *  - non-enabled actions are greyed-out
     */
    val innerActions: Map<String, InnerAction> = mapOf(
        "Complete" to InnerAction({ complete(it) },
            { it.formKey == null },
            { !it.isQualityShell }),
        "Assign" to InnerAction({ assign(it) },
            { true },
            { true }),
        "Claim" to InnerAction({ claim(it) },
            { it.assignee == null },
            { true }),
        "Unclaim" to InnerAction({ unclaim(it) },
            { it.assignee == currentUser },
            { true }),
        "Approve" to InnerAction({ approve(it) },
            { true },
            { it.isQualityShell }),
        "Reject" to InnerAction({ reject(it) },
            { true },
            { it.isQualityShell })
    )

    override fun isActionEnabled(action: String, dto: TaskDto): Boolean {
        return innerActions[action]?.isEnabled?.invoke(dto) ?: super.isActionEnabled(action, dto)
    }

    override fun isActionVisible(action: String, dto: TaskDto): Boolean {
        return innerActions[action]?.isVisible?.invoke(dto) ?: super.isActionVisible(action, dto)
    }

    override fun executeAction(action: String, dto: TaskDto): Outcome<Any> = try {
        innerActions[action]!!.action.invoke(dto)
        Outcome.OK
    } catch (e: Exception) {
        Outcome.fail(e)
    }

    fun isTaskAvailable(id: String): Boolean = taskService.isTaskAvailable(id)

    fun getTaskDtoById(id: String): TaskDto = taskService.getTaskDtoById(id)

    fun getProjectInstanceDto(projectInstanceId: String): ProjectInstanceDto =
        taskService.getProjectInstanceDto(projectInstanceId)

    override fun refresh() = this.refreshAll()

    /**
     * Function to redirect the user to the page set in the redirectUrl. Only executed when redirectUrl is not null.
     * Resets the redirectUrl to null after the navigation call.
     */
    fun redirect() {
        if (redirectUrl != null) {
            UI.getCurrent().navigate(redirectUrl.toString())
            redirectUrl = null
        }
    }

}
