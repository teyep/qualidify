package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.process.HistoricTaskService
import com.qualidify.service.process.dto.HistoricTaskDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A subscribable DataConnector taking care of querying and persisting CMMN historic tasks
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
class HistoricTaskDataConnector(
    private val historicTaskService: HistoricTaskService
) : AbstractQueryableServiceDataProvider<HistoricTaskDto>(HistoricTaskDto::class, historicTaskService) {

    override fun create(dto: HistoricTaskDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun update(dto: HistoricTaskDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun delete(dto: HistoricTaskDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    fun isHistoricTaskAvailable(id: String): Boolean = historicTaskService.isHistoricTaskAvailable(id)

    fun getHistoricTaskById(id: String): HistoricTaskDto = historicTaskService.getHistoricTaskDtoById(id)

    fun getHistoricFormVariables(id: String): Map<String, Any?> = historicTaskService.getHistoricFormVariables(id)

}