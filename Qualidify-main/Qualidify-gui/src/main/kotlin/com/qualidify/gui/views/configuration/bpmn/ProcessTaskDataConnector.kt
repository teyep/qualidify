package com.qualidify.gui.views.configuration.bpmn

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.process.BpmnTaskService
import com.qualidify.service.process.dto.BusinessProcessTaskDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A subscribable DataConnector taking care of querying and persisting Task data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
open class ProcessTaskDataConnector(val processTaskService: BpmnTaskService) :
        AbstractQueryableServiceDataProvider<BusinessProcessTaskDto>(BusinessProcessTaskDto::class, processTaskService) {

    override fun create(dto: BusinessProcessTaskDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun update(dto: BusinessProcessTaskDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun delete(dto: BusinessProcessTaskDto): Outcome<Any> {
        TODO("Not yet implemented")
    }
}