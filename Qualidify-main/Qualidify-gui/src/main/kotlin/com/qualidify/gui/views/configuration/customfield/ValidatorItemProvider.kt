package com.qualidify.gui.views.configuration.customfield

import com.qualidify.metadata.gui.providers.RetrieveCollectionItemProvider
import org.springframework.stereotype.Component

/**
 * A Item Provider for Validators
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
class ValidatorItemProvider() :
    RetrieveCollectionItemProvider<String>(

    ) {
    override fun retrieve(): Collection<String> {
        return listOf()
    }
}