package com.qualidify.gui.components

import com.vaadin.flow.component.Component
import com.vaadin.flow.component.Tag
import com.vaadin.flow.component.UI

/**
 * Viewer for Cmmn files from BPMN.io (https://github.com/bpmn-io/cmmn-js)
 *
 * NB: Implementation via url or @JsModule, using node_modules is not possible anymore.
 * Vaadin does not allow cross-loading JavaScript modules (to prevent XSS vulnerability?)
 *
 * @author Menno Tol
 * @since 1.0
 */
@Tag("cmmn-js")
class CmmnViewer(private val xml: String) : Component() {

    init {
        setId("canvas")
        prepare()
    }

    private fun prepare() {

        val page = UI.getCurrent().page

        page.addJsModule("https://unpkg.com/cmmn-js@0.20.0/dist/cmmn-viewer.production.min.js")
        page.executeJs(
            """
                var cmmnViewer = new CmmnJS({
                    container: '#canvas'
                })
    
                cmmnViewer.importXML($0, function(err) {                   
                    if (!err) {
                        console.log('success!');
                        viewer.get('canvas').zoom('fit-viewport');
                    } else {
                        console.log('something went wrong:', err);
                    }
                });
            """.trimIndent(), xml  // xml variable inserted via '$0'
        )
    }

}