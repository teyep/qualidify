package com.qualidify.gui.components.projectpresenter

import com.qualidify.gui.components.AbstractGridRow
import com.qualidify.gui.views.projects.ProjectPresenter
import com.qualidify.service.process.dto.HistoricTaskDto
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component
import java.time.format.DateTimeFormatter

/**
 * Component for rendering the [TasksCompletedGrid] on the [TasksCompletedGridPanel] of the [ProjectPresenter].
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
class TasksCompletedGridRow : AbstractGridRow<HistoricTaskDto>() {

    init {
        className = "gridrow"
    }

    override fun setupClickListener() {
        addClickListener {
            UI.getCurrent().navigate("tasks/${dtoInstance.id}")
        }
    }

    override fun prepare() {
        val name = Div()
        name.add(dtoInstance.name)
        name.width = "50%"
        name.style.set("overflow", "clip")

        val byAssignee = Div()
        byAssignee.add("By: ${dtoInstance.assignee}")
        byAssignee.style.set("overflow", "clip")

        val pattern = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm")
        val endTime = Div()
        endTime.add("On: ${dtoInstance.endTime.format(pattern)}")
        endTime.style.set("overflow", "clip")

        add(name, byAssignee, endTime)
    }

}