package com.qualidify.gui.views.configuration.security

import com.qualidify.gui.providers.QueryUtil
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.metadata.gui.providers.ItemProvider
import com.qualidify.service.security.SecurityService
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider
import com.vaadin.flow.data.provider.Query
import org.springframework.stereotype.Repository
import java.util.stream.Stream

/**
 * A Data Provider to collect the Security Roles from the backend.
 * This class also implements the interface ItemLabelGenerator, to
 * describe the Security roles
 *
 * @author Marcel Pot
 */
@Repository("securityRoleProvider")
open class SecurityRoleProvider(val securityService: SecurityService) :
    AbstractBackEndDataProvider<String, GridFilter>(), ItemProvider<String, GridFilter> {

    override fun sizeInBackEnd(query: Query<String, GridFilter>): Int {
        val countQuery = QueryUtil.mapCountQuery(String::class, query)
        return securityService.availableRoles()
            .filter(countQuery.toFilter())
            .count()
    }

    override fun fetchFromBackEnd(query: Query<String, GridFilter>): Stream<String> {
        val searchQuery = QueryUtil.mapSearchQuery(String::class, query)
        return securityService.availableRoles()
            .filter(searchQuery.toFilter())
            .sortedWith(searchQuery.toComparator())
            .stream()
    }

    override fun apply(item: String) = item
}