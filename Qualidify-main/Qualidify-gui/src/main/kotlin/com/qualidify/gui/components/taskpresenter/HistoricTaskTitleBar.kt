package com.qualidify.gui.components.taskpresenter

import com.qualidify.gui.components.AbstractTitleBar
import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.views.configuration.cmmn.HistoricTaskDataConnector
import com.qualidify.gui.views.tasks.HistoricTaskPresenter
import com.qualidify.service.process.dto.HistoricTaskDto
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class HistoricTaskTitleBar(
    private val historicTaskDataConnector: HistoricTaskDataConnector
) : AbstractTitleBar<HistoricTaskDto, HistoricTaskPresenter>() {

    init {
        setId("historictasktitlebar")
    }

    override fun registerListener(listener: HistoricTaskPresenter) {
        addListener(RefreshEvent::class.java, listener)
    }

    override fun prepareDetailBox(detailBox: DetailBox): DetailBox {
        val projectId = Span(dtoInstance.id)
        val assignee = Span(dtoInstance.assignee)

        detailBox.add(Span("Project id:"), projectId)
        detailBox.add(Span("Assignee:"), assignee)

        detailBox.setColspan(projectId, 3)
        detailBox.setColspan(assignee, 3)

        return detailBox
    }

    override fun addButtons(): Array<ActionButton> {
        val buttons: MutableList<ActionButton> = mutableListOf()

        return buttons.toTypedArray()
    }

}