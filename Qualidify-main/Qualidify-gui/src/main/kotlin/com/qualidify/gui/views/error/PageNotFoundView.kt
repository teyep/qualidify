package com.qualidify.gui.views.error

import com.qualidify.gui.views.common.MainLayout
import com.vaadin.flow.router.*
import org.apache.http.HttpStatus
import javax.annotation.security.PermitAll

/**
 * An Error view for Page not found errors
 *
 * @author Marcel Pot
 * @since 1.0
 */
@ParentLayout(MainLayout::class)
@PageTitle(PageNotFoundView.TITLE)
@PermitAll
open class PageNotFoundView : AbstractErrorView(IDENTITY), HasErrorParameter<NotFoundException?> {

    override fun setErrorParameter(
        event: BeforeEnterEvent, parameter: ErrorParameter<NotFoundException?>
    ): Int {
        logErrorMessage("Page '${event.location.path}' does not exist.")
        logExceptionToScreen(parameter.exception)
        return HTTP_STATUS
    }

    companion object {
        const val TITLE = "Page not found"
        const val IDENTITY = "page-not-found"
        const val HTTP_STATUS = HttpStatus.SC_NOT_FOUND
    }
}