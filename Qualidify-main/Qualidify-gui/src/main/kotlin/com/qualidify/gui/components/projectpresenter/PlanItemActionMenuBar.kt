package com.qualidify.gui.components.projectpresenter

import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.views.configuration.project.PlanItemHierarchicalDataConnector
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType

class PlanItemActionMenuBar(
    private val planItemDto: PlanItemDto,
    private val planItemHierarchicalDataConnector: PlanItemHierarchicalDataConnector,
) : AbstractActionMenuBar() {

    private var index = 0
    private val subMenuItemsEnabled = mutableSetOf<Boolean>()

    init {
        menuItem.isVisible = (planItemDto.type == PlanItemType.STAGE)
    }

    override fun prepareSubMenu() {
        planItemHierarchicalDataConnector.getUserEventListeners(planItemDto)
            .forEach { planItemDto ->
                subMenu.addItem(planItemDto.name) {
                    planItemHierarchicalDataConnector.startUserEvent(planItemDto)
                    fireEvent(RefreshEvent(this))
                }
                subMenu.items[index].isEnabled = (planItemDto.state == PlanItemState.AVAILABLE)
                subMenuItemsEnabled += (planItemDto.state == PlanItemState.AVAILABLE)
                index++
            }

        menuItem.isEnabled = subMenuItemsEnabled.contains(true)
    }

}