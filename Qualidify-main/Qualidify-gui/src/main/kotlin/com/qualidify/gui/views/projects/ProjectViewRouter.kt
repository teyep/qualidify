package com.qualidify.gui.views.projects

import com.qualidify.gui.components.ListTitleBar
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.common.isUUID
import com.qualidify.gui.views.configuration.project.ProjectInstanceDataConnector
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.H3
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.*
import com.vaadin.flow.spring.annotation.UIScope
import javax.annotation.security.RolesAllowed


/**
 * An overview of all projects
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = "projects", layout = MainLayout::class)
@RolesAllowed(
    Roles.Project.VIEW
)
@UIScope
open class ProjectViewRouter(
    private val projectPresenter: ProjectPresenter,
    private val projectList: ProjectList,
    private val projectCreator: ProjectCreator,
    private val projectInstanceDataConnector: ProjectInstanceDataConnector,
) : VerticalLayout(), HasDynamicTitle, HasUrlParameter<String> {

    override fun getPageTitle(): String {
        return "Projects"
    }

    override fun setParameter(event: BeforeEvent, @OptionalParameter projectId: String?) {
        children.forEach { remove(it) }
        setSizeFull()

        if (projectId.equals("new", true)) {
            projectCreator()
        } else if (projectId != null && isUUID(projectId)) {
            if (projectInstanceDataConnector.isProjectAvailable(projectId)) {
                projectPresenter(projectId)
            } else {
                add(ProjectNotFoundView(projectId))
            }
        } else
            projectList()
    }

    private fun projectList() {
        UI.getCurrent().navigate("projects")
        add(ListTitleBar("Running projects", arrayOf(newProjectButton()), projectList.searchField))
        projectList.addConstraints("cmmnInstanceState", "active")
        add(projectList)
    }

    private fun projectPresenter(projectId: String) {
        val projectInstance = projectInstanceDataConnector.getById(projectId)!!
        projectPresenter.setDto(projectInstance)
        add(projectPresenter)
    }

    private fun projectCreator() {
        projectCreator.refresh()
        add(ListTitleBar("Project templates", searchField = projectCreator.searchField))
        add(projectCreator)
    }

    private fun newProjectButton(): Button {
        val newProjectButton = Button("New", Icon(VaadinIcon.PLUS))
        newProjectButton.addClickListener { UI.getCurrent().navigate("projects/new") }
        newProjectButton.addThemeVariants(ButtonVariant.LUMO_LARGE)
        return newProjectButton
    }
}

class ProjectNotFoundView(projectId: String) : Div() {
    init {
        add(H3("Project with id '$projectId' could not be found."))
    }
}


