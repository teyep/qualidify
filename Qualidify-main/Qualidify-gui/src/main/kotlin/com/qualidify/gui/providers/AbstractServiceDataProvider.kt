package com.qualidify.gui.providers

import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.metadata.gui.providers.DataConnector
import com.qualidify.metadata.gui.providers.Outcome
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider
import java.io.Serializable

abstract class AbstractServiceDataProvider<T : Serializable> : AbstractBackEndDataProvider<T, GridFilter>(),
    DataConnector<T> {

    /**
     * Execute a function and wrap the result in an [Outcome] object
     *
     * @param function the lambda for retrieving the information
     * @param parameter the parameter to use in the lambda
     * @param T The type of the parameter
     * @param R the type of the result object
     * @return an outcome object (wrapper around the result)
     */
    fun <T, R> executeAndWrap(function: (type: T) -> R, parameter: T): Outcome<R> {
        try {
            val returnValue = function.invoke(parameter)
            return Outcome.success(returnValue)
        } catch (e: Exception) {
            return Outcome.fail(e)
        }
    }

    /**
     * Execute a function and wrap the result in an [Outcome] object
     *
     * @param function the lambda for retrieving the information
     * @param R the type of the result object
     * @return an outcome object (wrapper around the result)
     */
    fun <R> executeAndWrap(function: () -> R): Outcome<R> {
        try {
            val returnValue = function.invoke()
            return Outcome.success(returnValue)
        } catch (e: Exception) {
            return Outcome.fail(e)
        }
    }

    /**
     * Execute a function and return an indicator [Outcome] it the function is executed
     * succesfully
     *
     * @param function the lambda for retrieving the information
     * @return an outcome object (without result)
     */
    fun executeWithoutResultAndWrap(function: () -> Unit): Outcome<Any> {
        try {
            function.invoke()
            return Outcome.OK
        } catch (e: Exception) {
            return Outcome.fail(e)
        }
    }

}