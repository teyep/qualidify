package com.qualidify.gui.components.projectpresenter


import com.qualidify.core.util.html.toHtmlCase
import com.qualidify.gui.components.AbstractTitleBar
import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.components.dialogs.ConfirmDialog
import com.qualidify.gui.views.configuration.cmmn.PlanItemDataConnector
import com.qualidify.gui.views.projects.ProjectPresenter
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class ProjectTitleBar(private val planItemDataConnector: PlanItemDataConnector
) : AbstractTitleBar<ProjectInstanceDto, ProjectPresenter>() {

    init {
        setId("projecttitlebar")
    }

    override fun registerListener(listener: ProjectPresenter) {
        addListener(RefreshEvent::class.java, listener)
    }

    override fun prepareDetailBox(detailBox: DetailBox): DetailBox {
        val description = Span(dtoInstance.description)
        val projectId = Span(dtoInstance.id)
        val caseDefinition = Span(
            "${dtoInstance.cmmnInstanceDto.caseDefinition.key}," +
                    " version ${dtoInstance.cmmnInstanceDto.caseDefinition.version}"
        )
        val owner = Span("TODO: implement owner")  // TODO(): Implement owner
        val users = Span(dtoInstance.users.toString()
            .replace("[", "").replace("]", ""))

        detailBox.add(Span("Description:"), description)
        detailBox.add(Span("Project id:"), projectId)
        detailBox.add(Span("Definition:"), caseDefinition)
        detailBox.add(Span("Owner:"), owner)
        detailBox.add(Span("Users:"), users)

        detailBox.setColspan(description, 7)
        detailBox.setColspan(projectId, 3)
        detailBox.setColspan(caseDefinition, 3)
        detailBox.setColspan(owner, 3)
        detailBox.setColspan(users, 3)

        return detailBox
    }

    override fun addButtons(): Array<ActionButton> {
        val userEvents = planItemDataConnector.getTopLevelUserEvents(dtoInstance.cmmnInstanceId)
        val buttons: MutableList<ActionButton> = mutableListOf()

        userEvents.forEach { planItemDto ->
            if (planItemDto.stage == null) {
                val button = ActionButton(planItemDto.name)
                button.setId("button-${planItemDto.name.toHtmlCase()}")
                button.isEnabled = (planItemDto.state == PlanItemState.AVAILABLE)

                if (planItemDto.name.contains("cancel", true)) {
                    button.addClickListener {
                        val dialog = ConfirmDialog(
                            "Are you sure?",
                            actionFunction = { planItemDataConnector.start(planItemDto) },
                            closeFunction = { fireEvent(RefreshEvent(this)) },
                            confirmButtonWarning = true
                        )
                        dialog.open()
                    }
                } else
                    button.addClickListener {
                        planItemDataConnector.start(planItemDto)
                        fireEvent(RefreshEvent(this))
                    }

                buttons += button
            }
        }

        return buttons.toTypedArray()
    }

}