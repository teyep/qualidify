package com.qualidify.gui.views.reports

import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.reports.util.ReportDownloadLinkProvider
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.*
import com.vaadin.flow.spring.annotation.UIScope
import javax.annotation.security.RolesAllowed

/**
 * An overview of current reports, a form for a new report and a layout for a currently available report.
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Route(value = ReportsRouter.ROUTE, layout = MainLayout::class)
@RolesAllowed(
    Roles.Report.VIEW
)
@UIScope
open class ReportsRouter(
    private val reportResultDataConector: BatchJobDataConnector,
    private val reportTemplateDataConnector: ReportTemplateDataConnector,
    private val batchJobDataConnector: BatchJobDataConnector,
    private val formFieldComponentFactory: FormFieldComponentFactory,
) : VerticalLayout(), HasUrlParameter<String>, HasDynamicTitle {

    companion object {
        const val ROUTE = "reports"
    }

    override fun setParameter(event: BeforeEvent?, @OptionalParameter parameter: String?) {
        children.forEach { remove(it) }
        // Construct page based on (optional) parameter
        when (parameter) {
            "", null -> add(CurrentReportOverview(reportResultDataConector, ReportDownloadLinkProvider(reportResultDataConector)))
            "new" -> add(NewReportForm(reportTemplateDataConnector, batchJobDataConnector, formFieldComponentFactory))
            else -> {
                // This is the ID of the document we want to view
                add(ReportDetail(reportResultDataConector, parameter))
            }
        }
    }

    override fun getPageTitle(): String {
        return "Reports"
    }
}
