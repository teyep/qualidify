package com.qualidify.gui.views.configuration.domain

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.configuration.ConfigurationService
import com.qualidify.service.configuration.dto.ConfigurationCodeDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service


/**
 * A  DataConnector taking care of querying and persisting Configuration domain data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@VaadinSessionScope
@Service
class ConfigurationDomainDataConnector(private val configurationService: ConfigurationService) :
    AbstractQueryableServiceDataProvider<ConfigurationCodeDto>(
        ConfigurationCodeDto::class,
        configurationService
    ) {

    override fun create(dto: ConfigurationCodeDto): Outcome<Any> {
        return executeWithoutResultAndWrap {
            configurationService.createCode(dto)
        }
    }

    override fun update(dto: ConfigurationCodeDto): Outcome<Any> {
        return executeWithoutResultAndWrap {
            configurationService.updateCode(dto)
        }
    }

    override fun delete(dto: ConfigurationCodeDto): Outcome<Any> {
        return Outcome.FORBIDDEN
    }


}