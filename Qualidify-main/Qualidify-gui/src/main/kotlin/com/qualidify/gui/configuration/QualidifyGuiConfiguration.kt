package com.qualidify.gui.configuration

import com.vaadin.flow.spring.annotation.EnableVaadin
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


/**
 * A configuration for extra beans needed for the vaadin gui
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Configuration
@ComponentScan(basePackages = ["com.qualidify.gui"])
@EnableVaadin
open class QualidifyGuiConfiguration(
    val userDetailsService: ReactiveUserDetailsService,
    val passwordEncoder: PasswordEncoder
) {

    @Bean("guiExecutorService")
    open fun guiExecutorService(): ExecutorService {
        return Executors.newCachedThreadPool()
    }

    @Bean
    open fun authenticationManager(): ReactiveAuthenticationManager {
        val authenticationManager = UserDetailsRepositoryReactiveAuthenticationManager(userDetailsService)
        authenticationManager.setPasswordEncoder(passwordEncoder)
        return authenticationManager
    }
}