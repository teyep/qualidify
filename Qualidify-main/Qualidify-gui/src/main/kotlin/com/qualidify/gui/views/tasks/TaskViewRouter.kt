package com.qualidify.gui.views.tasks

import com.qualidify.gui.components.ListTitleBar
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.common.isUUID
import com.qualidify.gui.views.configuration.cmmn.HistoricTaskDataConnector
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.service.process.dto.HistoricTaskDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.H3
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.*
import com.vaadin.flow.spring.annotation.UIScope
import javax.annotation.security.RolesAllowed


/**
 * An overview of all tasks
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = "tasks", layout = MainLayout::class)
@RolesAllowed(
    Roles.Task.VIEW
)
@UIScope
open class TaskViewRouter(
    private val taskGridPanel: TaskGridPanel,
    private val taskPresenter: TaskPresenter,
    private val taskEditor: TaskEditor,
    private val taskDataConnector: TaskDataConnector,
    private val historicTaskPresenter: HistoricTaskPresenter,
    private val historicTaskViewer: HistoricTaskViewer,
    private val historicTaskDataConnector: HistoricTaskDataConnector,
) : VerticalLayout(), HasDynamicTitle, HasUrlParameter<String> {

    override fun getPageTitle(): String {
        return "Tasks"
    }

    override fun setParameter(event: BeforeEvent?, @OptionalParameter taskId: String?) {
        children.forEach { remove(it) }
        setSizeFull()

        if (taskId != null && isUUID(taskId)) {
            if (taskDataConnector.isTaskAvailable(taskId)) {
                val taskInstance = taskDataConnector.getTaskDtoById(taskId)
                taskPresenter(taskInstance)
                if (taskInstance.formKey != null) {
                    taskEditor(taskInstance)
                }
            } else if (historicTaskDataConnector.isHistoricTaskAvailable(taskId)) {
                val historicTaskInstance = historicTaskDataConnector.getHistoricTaskById(taskId)
                historicTaskPresenter(historicTaskInstance)
                if (historicTaskInstance.formKey != null) {
                    historicTaskEditor(historicTaskInstance)
                }
            } else {
                add(TaskNotFoundView(taskId))
            }
        } else {
            taskList()
        }
    }

    private fun taskPresenter(taskInstance: TaskDto) {
        taskPresenter.setDto(taskInstance)
        add(taskPresenter)
    }

    private fun historicTaskPresenter(historicTaskInstance: HistoricTaskDto) {
        historicTaskPresenter.setDto(historicTaskInstance)
        add(historicTaskPresenter)
    }

    private fun taskEditor(taskInstance: TaskDto) {
        add(ListTitleBar("Task editor"))
        taskEditor.setDto(taskInstance)
        add(taskEditor)
    }

    private fun historicTaskEditor(historicTaskInstance: HistoricTaskDto) {
        add(ListTitleBar("Task viewer"))
        historicTaskViewer.setDto(historicTaskInstance)
        add(historicTaskViewer)
    }

    private fun taskList() {
        UI.getCurrent().navigate("tasks")
        taskGridPanel.refresh()
        add(taskGridPanel)
    }

}

class TaskNotFoundView(taskId: String) : Div() {
    init {
        add(H3("Task with id '$taskId' could not be found."))
    }
}

