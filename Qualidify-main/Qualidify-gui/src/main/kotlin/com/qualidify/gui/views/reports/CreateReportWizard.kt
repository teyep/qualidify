package com.qualidify.gui.views.reports

import com.qualidify.metadata.gui.components.dialogs.DialogTemplate
import com.vaadin.flow.component.html.Paragraph

/**
 * Help the user create a new report. At this point, the user has already chosen a report template but not filled out
 * any other parameters yet.
 *
 * @author Niels Visscher
 * @since 1.0
 */
class CreateReportWizard : DialogTemplate("report-generation-wizard") {
    companion object {
        const val REPORT_TEMPLATE = "Report Template"
        const val PROJECT_INSTANCE = "Project Instance"
    }

    init {
        add(Paragraph("New report dialog"))
    }
}