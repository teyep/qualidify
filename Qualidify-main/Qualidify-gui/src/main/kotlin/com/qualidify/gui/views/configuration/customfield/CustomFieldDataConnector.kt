package com.qualidify.gui.views.configuration.customfield


import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.metadata.service.CustomFieldService
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A Subscribable DataConnector taking care of querying and persisting CustomFieldDefinitions data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@VaadinSessionScope
@Service
class CustomFieldDataConnector(val customFieldService: CustomFieldService) :
    AbstractQueryableServiceDataProvider<CustomFieldDef>(CustomFieldDef::class, customFieldService) {

    override fun create(dto: CustomFieldDef): Outcome<Any> {
        return executeAndWrap { customFieldService.create(dto) }
    }

    override fun update(dto: CustomFieldDef): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun delete(dto: CustomFieldDef): Outcome<Any> {
        return executeAndWrap { customFieldService.delete(dto) }
    }

}

