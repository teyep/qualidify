package com.qualidify.gui.views.configuration.bpmn

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.ActionExecutor
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.process.BpmnDefinitionService
import com.qualidify.service.process.dto.BusinessProcessDefinitionDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A subscribable DataConnector taking care of querying and persisting Process(definitions) data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
class ProcessDefinitionDataConnector(
        private val processDefinitionService: BpmnDefinitionService,
) : AbstractQueryableServiceDataProvider<BusinessProcessDefinitionDto>(
        BusinessProcessDefinitionDto::class,
        processDefinitionService
),
        ActionExecutor<BusinessProcessDefinitionDto> {

    private val logger by LoggerDelegation()

    override fun create(dto: BusinessProcessDefinitionDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun update(dto: BusinessProcessDefinitionDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun delete(dto: BusinessProcessDefinitionDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    fun start(dto: BusinessProcessDefinitionDto) {
        processDefinitionService.startProcess(dto)
        logger.info("Started new process definition item ${dto.key}")
        refresh()
    }

    inner class InnerAction(
            val action: (BusinessProcessDefinitionDto) -> Unit,
    )

    val innerActions: Map<String, InnerAction> = mapOf(
        "Start" to InnerAction { start(it) }
    )

    override fun executeAction(action: String, dto: BusinessProcessDefinitionDto): Outcome<Any> {
        return try {
            innerActions[action]!!.action.invoke(dto)
            Outcome.OK
        } catch (e: Exception) {
            Outcome.fail(e)
        }
    }

    override fun refresh() {
        this.refreshAll()
    }


}