package com.qualidify.gui.views.configuration.forms

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.forms.FormView.Companion.IDENTITY
import com.qualidify.gui.views.configuration.forms.FormView.Companion.TITLE
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.process.dto.FormDefinitionDto

import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A View for Forms
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Project.FORM_DEFINITION
)
open class FormView(dataCrud: DataCrud<FormDefinitionDto>) :
    AbstractDataCrudTemplate<FormDefinitionDto>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val TITLE = "Forms"
        const val IDENTITY = "forms"
    }

}