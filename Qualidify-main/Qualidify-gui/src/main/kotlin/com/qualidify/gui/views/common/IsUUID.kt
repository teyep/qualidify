package com.qualidify.gui.views.common

import java.util.*

/**
 * Returns true or false for converting any string to a UUID
 *
 * @author Menno Tol
 * @since 1.0
 */
fun isUUID(id: String): Boolean {
    try {
        UUID.fromString(id)
    } catch (exception: java.lang.IllegalArgumentException) {
        return false
    }
    return true
}