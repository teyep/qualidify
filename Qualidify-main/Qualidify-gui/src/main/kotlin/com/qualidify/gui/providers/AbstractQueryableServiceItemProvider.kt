package com.qualidify.gui.providers

import com.qualidify.core.query.service.Queryable
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.metadata.gui.providers.ItemProvider
import com.vaadin.flow.data.provider.AbstractBackEndDataProvider
import com.vaadin.flow.data.provider.Query
import java.io.Serializable
import java.util.stream.Stream
import kotlin.reflect.KClass

/**
 * Abstract class for [ItemProvider] in combination with the [Queryable] interface
 *
 * @author Marcel Pot
 * @since 1.0
 */
abstract class AbstractQueryableServiceItemProvider<T : Serializable>(
    protected val beanType: KClass<T>,
    protected val queryable: Queryable<T>
) :
    AbstractBackEndDataProvider<T, GridFilter>(), ItemProvider<T, GridFilter> {

    override fun sizeInBackEnd(query: Query<T, GridFilter>): Int {
        val countQuery = QueryUtil.mapCountQuery(beanType = beanType, query = query)
        return queryable.countByQuery(countQuery).count.toInt()
    }

    override fun fetchFromBackEnd(query: Query<T, GridFilter>): Stream<T> {
        val searchQuery = QueryUtil.mapSearchQuery(beanType = beanType, query = query)
        return queryable.findByQuery(searchQuery).stream()
    }


}