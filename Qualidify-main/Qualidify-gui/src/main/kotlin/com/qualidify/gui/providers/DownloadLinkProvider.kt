package com.qualidify.gui.providers

import com.qualidify.gui.configuration.FileDownloadDefinitions
import com.vaadin.flow.server.RequestHandler
import com.vaadin.flow.server.VaadinRequest
import com.vaadin.flow.server.VaadinResponse
import com.vaadin.flow.server.VaadinSession
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import java.util.*

/**
 * Abstract class for handling and creating download links for a dto of type T
 *
 * @author Niels Visscher
 * @since 1.0
 */
@VaadinSessionScope
abstract class DownloadLinkProvider<T>: RequestHandler {

    /**
     * Suggested prefix for the downloaded file
     */
    abstract val downloadFilePrefix: String

    /**
     * Prefix for locations for which this RequestHandler should enable downloading. Should not be slash-terminated.
     */
    abstract val downloadLocationPrefix: String?

    /**
     * Prefix for location for which this RequestHandler should enable viewing. Similar to downloadLocationPrefix,
     * except for this one the Content-Disposition header will not be set.
     */
    abstract val viewLocationPrefix: String?

    /**
     * Fetches the DTO of a T based on a UUID
     */
    abstract fun getDto(id: UUID): T?

    /**
     * Fetches the ID of a dto of type T, used for link creation
     */
    abstract fun getIdForDto(dto: T): UUID

    /**
     * Fetches the mime type for a dto of type T
     */
    abstract fun getMimeType(dto: T): String

    /**
     * Fetches the content of a dto of type T as a byte array
     */
    abstract fun getContent(dto: T): ByteArray

    /**
     * Determines whether the requested content is present
     */
    abstract fun contentIsPresent(dto: T): Boolean

    /**
     * Handles a request that was intended to be handled by this [RequestHandler] instance
     */
    private fun handleOurRequest(request: VaadinRequest, response: VaadinResponse?, downloadFile: Boolean): Boolean {
        val uid = if(downloadFile == true) {
            UUID.fromString(request.pathInfo!!.substring(downloadLocationPrefix!!.length + "/".length))
        } else {
            UUID.fromString(request.pathInfo!!.substring(viewLocationPrefix!!.length + "/".length))
        }

        val dto = getDto(uid)
        return if(dto == null) {
            false
        } else {
            val mimeType = getMimeType(dto)
            response?.setContentType(mimeType)
            if(downloadFile) {
                response?.setHeader(
                    CONTENT_DISPOSITION_HEADER,
                    String.format("attachment; filename=\"%s\"", formatFilename(uid, mimeType)))
            }
            response?.outputStream!!.write(getContent(dto))
            true
        }
    }

    override fun handleRequest(session: VaadinSession?,
                               request: VaadinRequest?,
                               response: VaadinResponse?): Boolean {
        return if(downloadLocationPrefix != null && request?.pathInfo!!.startsWith(downloadLocationPrefix!!)) {
            handleOurRequest(request, response, true)
        }
        else if(viewLocationPrefix != null && request?.pathInfo!!.startsWith(viewLocationPrefix!!)) {
            handleOurRequest(request, response, false)
        }
        else {
            return false
        }
    }

    fun createDownloadLink(id: UUID?) : String {
        return if(id != null) String.format("%s/%s", downloadLocationPrefix, id.toString()) else EMPTY_LINK_VALUE
    }

    fun createViewLink(id: UUID?) : String {
        return if(id != null) String.format("%s/%s", viewLocationPrefix, id.toString()) else EMPTY_LINK_VALUE
    }

    private fun formatFilename(id: UUID, mimeType: String): String {
        return String.format("%s_%s.%s",
            downloadFilePrefix,
            id.toString(),
            extensionForMimeType(mimeType))
    }

    companion object {
        @JvmStatic
        private fun extensionForMimeType(mimeType: String): String {
            return FileDownloadDefinitions.mimeTypesExtensions[mimeType]
                ?: FileDownloadDefinitions.FALLBACK_FILE_EXTENSION
        }

        const val CONTENT_DISPOSITION_HEADER: String = "Content-Disposition"
        const val EMPTY_LINK_VALUE: String = "#"
    }
}
