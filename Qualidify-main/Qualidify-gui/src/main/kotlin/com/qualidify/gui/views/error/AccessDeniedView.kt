package com.qualidify.gui.views.error

import com.qualidify.gui.security.AccessDeniedException
import com.qualidify.gui.views.common.MainLayout
import com.vaadin.flow.router.*
import org.apache.http.HttpStatus
import javax.annotation.security.PermitAll

/**
 * An Error view for Access Denied
 *
 * @author Marcel Pot
 * @since 1.0
 */
@ParentLayout(MainLayout::class)
@PageTitle(AccessDeniedView.TITLE)
@PermitAll
open class AccessDeniedView : AbstractErrorView(IDENTITY), HasErrorParameter<AccessDeniedException?> {

    override fun setErrorParameter(
        event: BeforeEnterEvent, errorParameter: ErrorParameter<AccessDeniedException?>
    ): Int {
        logErrorMessage("Access to '" + event.location.path + "' is denied.")
        logExceptionToScreen(errorParameter.exception)
        return HTTP_STATUS
    }

    companion object {
        const val TITLE = "Access Denied"
        const val IDENTITY = "access-denied"
        const val HTTP_STATUS = HttpStatus.SC_FORBIDDEN
    }
}