package com.qualidify.gui.views.configuration.bpmn

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.bpmn.ProcessTaskView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.process.dto.BusinessProcessTaskDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A view for BPMN Tasks
 *
 * @author Menno Tol
 * @since 1.0
 * */

@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(ProcessTaskView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Project.BPMN_TASK
)
open class ProcessTaskView(dataCrud: DataCrud<BusinessProcessTaskDto>) :
    AbstractDataCrudTemplate<BusinessProcessTaskDto>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val IDENTITY = "processtasks"
        const val TITLE = "Process Tasks"
    }
}