package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.cmmn.CaseDefinitionView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A View for CMMN Case Definitions
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(CaseDefinitionView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Project.CASE_DEFINITION
)
open class CaseDefinitionView(dataCrud: DataCrud<CaseDefinitionDto>) :
    AbstractDataCrudTemplate<CaseDefinitionDto>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val IDENTITY = "casedefinitions"
        const val TITLE = "Case Definitions"
    }

}