package com.qualidify.gui.views.reports
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.html.Paragraph
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import java.util.*


/**
 * Build an interface with a view that describes the chosen document (assuming it is a report)
 *
 * @author Niels Visscher
 * @since 1.0
 */
@VaadinSessionScope
class ReportDetail(val connector: BatchJobDataConnector, val documentId: String) : VerticalLayout() {
    init {
        try {
            val uuid_id = UUID.fromString(documentId)

            //val dto: ReportResultDto = connector.getDocumentById(UUID.fromString(documentId))
            //val content: DocumentContentDto = connector.getDocumentContent(uuid_id)

            add(H2("Report " + documentId))
            add(Paragraph("Under development"))
        } catch (e: Exception) {
            add(H2("Failed to fetch document"))
            add(Paragraph("Could not fetch document with identifier " + documentId + ". The exception is described below"))
            add(Paragraph(e.message))
        }
    }
}
