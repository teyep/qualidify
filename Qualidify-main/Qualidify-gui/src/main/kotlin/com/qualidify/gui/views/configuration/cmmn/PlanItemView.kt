package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.cmmn.PlanItemView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A View for CMMN plan items
 *
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(PlanItemView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Project.PLAN_ITEM
)
open class PlanItemView(dataCrud: DataCrud<PlanItemDto>) : AbstractDataCrudTemplate<PlanItemDto>(IDENTITY, dataCrud) {

    override fun getTitle() = TITLE

    companion object {
        const val IDENTITY = "planitems"
        const val TITLE = "Plan Items"
    }

}