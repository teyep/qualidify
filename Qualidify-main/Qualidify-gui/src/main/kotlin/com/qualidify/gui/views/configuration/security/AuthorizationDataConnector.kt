package com.qualidify.gui.views.configuration.security

import com.qualidify.service.security.UserAuthorizationService
import org.springframework.stereotype.Service

@Service
class AuthorizationDataConnector(private val authorizationService: UserAuthorizationService) {

    fun isUserLoggedIn(): Boolean {
        return authorizationService.isUserLoggedIn()
    }

    fun loadUserByUsername(userName: String) = authorizationService.loadUserByUsername(userName)

}