package com.qualidify.gui.views.reports

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.document.DocumentService
import com.qualidify.service.document.dto.DocumentContentDto
import com.qualidify.service.document.dto.DocumentDto
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service
import java.util.*

/**
 * Data Connector for querying and persisting documents
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Service
@VaadinSessionScope
open class DocumentDataConnector(private val service: DocumentService) :
    AbstractQueryableServiceDataProvider<DocumentDto>(
        DocumentDto::class,
        service
    ) {

    fun createDocument(document: DocumentDto, content: DocumentContentDto) {
        service.createDocument(document, content)
        refreshAll()
    }

    fun getDocumentById(documentId: UUID): DocumentDto {
        return service.getDocumentById(documentId)
    }

    fun getDocumentContent(documentId: UUID): DocumentContentDto {
        return service.getDocumentContent(documentId)
    }


    override fun create(dto: DocumentDto): Outcome<Any> {
        return Outcome.ERROR
    }

    override fun update(dto: DocumentDto): Outcome<Any> {
        return Outcome.FORBIDDEN
    }

    override fun delete(dto: DocumentDto): Outcome<Any> {
        return Outcome.FORBIDDEN
    }
}