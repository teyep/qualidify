package com.qualidify.gui.components.taskpresenter

import com.qualidify.gui.components.AbstractTitleBar
import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.components.RefreshEventTarget
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.gui.views.tasks.TaskPresenter
import com.qualidify.service.projects.dto.TaskDto
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class TaskTitleBar(
    private val taskDataConnector: TaskDataConnector
) : AbstractTitleBar<TaskDto, TaskPresenter>() {

    init {
        setId("tasktitlebar")
    }

    override fun registerListener(listener: TaskPresenter) {
        addListener(RefreshEvent::class.java, listener)
    }

    override fun prepareDetailBox(detailBox: DetailBox): DetailBox {
        val projectId = Span(dtoInstance.id)
        val assignee = Span(taskDataConnector.getTaskDtoById(dtoInstance.id).assignee)

        detailBox.add(Span("Project id:"), projectId)
        detailBox.add(Span("Assignee:"), assignee)

        detailBox.setColspan(projectId, 3)
        detailBox.setColspan(assignee, 3)

        return detailBox
    }

    override fun addButtons(): Array<ActionButton> {
        val actions = taskDataConnector.innerActions.keys
        val buttons: MutableList<ActionButton> = mutableListOf()

        actions.forEach { action ->
            val button = ActionButton(action)
            button.isEnabled =
                (taskDataConnector.isActionEnabled(action, taskDataConnector.getTaskDtoById(dtoInstance.id)))
            button.isVisible =
                (taskDataConnector.isActionVisible(action, taskDataConnector.getTaskDtoById(dtoInstance.id)))

            button.addClickListener {
                when (action) {
                    "Complete" -> {
                        taskDataConnector.executeAction(action, taskDataConnector.getTaskDtoById(dtoInstance.id))
                    }
                    "Assign" -> {
                        taskDataConnector.assign(taskDataConnector.getTaskDtoById(dtoInstance.id))
                            { fireEvent(RefreshEvent(this, target = RefreshEventTarget.DETAIL_BAR)) }
                    }
                    else -> {
                        taskDataConnector.executeAction(action, taskDataConnector.getTaskDtoById(dtoInstance.id))
                        fireEvent(RefreshEvent(this, target = RefreshEventTarget.DETAIL_BAR))
                    }
                }
            }

            buttons += button
        }

        return buttons.toTypedArray()
    }

}