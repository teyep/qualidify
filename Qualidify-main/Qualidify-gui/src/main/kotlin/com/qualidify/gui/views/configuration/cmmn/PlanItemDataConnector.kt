package com.qualidify.gui.views.configuration.cmmn

import com.qualidify.gui.providers.AbstractQueryableServiceDataProvider
import com.qualidify.gui.providers.CurrentUserProvider
import com.qualidify.metadata.gui.providers.ActionExecutor
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import org.springframework.stereotype.Service

/**
 * A subscribable dataconnector taking care of querying and persisting CMMN Plan Item data
 * from the backend by requesting queries and commanding changes
 * and keeping the connection open for updates.
 *
 * @author Menno Tol
 * @since 1.0
 */
@VaadinSessionScope
@Service
class PlanItemDataConnector(
    private val planItemService: PlanItemService,
    private val currentUserProvider: CurrentUserProvider,
    ) : AbstractQueryableServiceDataProvider<PlanItemDto>(PlanItemDto::class, planItemService),
    ActionExecutor<PlanItemDto> {

    val currentUser: String
        get() {
            // custom getter for when currentUser is actually called, returns anonymousUser in dev environment
            return currentUserProvider.getUserName()
        }

    override fun create(dto: PlanItemDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun update(dto: PlanItemDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    override fun delete(dto: PlanItemDto): Outcome<Any> {
        TODO("Not yet implemented")
    }

    fun start(dto: PlanItemDto) {
        planItemService.startPlanItem(dto)
        refresh()
    }

    fun completeStage(dto: PlanItemDto) {
        planItemService.forceCompleteStage(dto, currentUser)
    }

    inner class InnerAction(
        val action: (PlanItemDto) -> Unit,
        val isEnabled: (PlanItemDto) -> Boolean,
        val isVisible: (PlanItemDto) -> Boolean,
    )

    /**
     *  Matrix to define under which conditions actions (buttons) are enabled or visible.
     *
     *  As a general rule:
     *  - all user actions are visible, actions for which to user has no right should not be visible
     *  - non-enabled actions are greyed-out
     */
    val innerActions: Map<String, InnerAction> = mapOf(
        "Start" to InnerAction({ start(it) },
            {
                (it.type == PlanItemType.USER_EVENT_LISTENER && it.state == PlanItemState.AVAILABLE)
                    .or(it.type == PlanItemType.HUMAN_TASK && it.state == PlanItemState.ENABLED)
            },
            { true }),
        "Complete" to InnerAction({ completeStage(it) },
            { it.completable ?: false },
            { true }),
    )

    override fun executeAction(action: String, dto: PlanItemDto): Outcome<Any> {
        return try {
            innerActions[action]!!.action.invoke(dto)
            Outcome.OK
        } catch (e: Exception) {
            Outcome.fail(e)
        }
    }

    override fun isActionEnabled(action: String, dto: PlanItemDto): Boolean {
        return innerActions[action]?.isEnabled?.invoke(dto) ?: super.isActionEnabled(action, dto)
    }

    override fun isActionVisible(action: String, dto: PlanItemDto): Boolean {
        return innerActions[action]?.isVisible?.invoke(dto) ?: super.isActionVisible(action, dto)
    }

    override fun refresh() {
        this.refreshAll()
    }

    fun getTopLevelUserEvents(cmmnInstanceId: String): List<PlanItemDto> =
        planItemService.getTopLevelUserEventListeners(cmmnInstanceId)

}