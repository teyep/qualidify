package com.qualidify.gui.components.projectpresenter

import com.qualidify.gui.components.AbstractGridRow
import com.qualidify.gui.components.RefreshEvent
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.gui.views.projects.ProjectPresenter
import com.qualidify.service.projects.dto.TaskDto
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

/**
 * Component for rendering the [TasksOngoingGrid] on the [TasksOngoingGridPanel] of the [ProjectPresenter].
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
@UIScope
class TasksOngoingGridRow(
    private val taskDataConnector: TaskDataConnector,
) : AbstractGridRow<TaskDto>() {

    init {
        setId("gridrow")
    }

    fun registerListener(listener: ProjectPresenter) {
        addListener(RefreshEvent::class.java, listener)
    }

    override fun setupClickListener() {
        addClickListener {
            UI.getCurrent().page.fetchCurrentURL { taskDataConnector.setRedirect(it) }
            UI.getCurrent().navigate("tasks/${dtoInstance.id}")
        }
    }

    override fun prepare() {
        val nameDiv = Div()
        nameDiv.add(dtoInstance.name)

        val assigneeDiv = Div()
        assigneeDiv.add(
            if (dtoInstance.assignee == null) "Unassigned"
            else "Assignee: ${dtoInstance.assignee}"
        )
        assigneeDiv.addClickListener {
            taskDataConnector.executeAction("Assign", dtoInstance)
        }

        add(nameDiv, assigneeDiv)
    }

}