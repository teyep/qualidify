package com.qualidify.gui.views.projects

import com.qualidify.core.util.html.toHtmlCase
import com.qualidify.gui.providers.UserProvider
import com.qualidify.gui.views.configuration.project.ProjectTemplateDataConnector
import com.qualidify.metadata.gui.components.dialogs.DialogTemplate
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.accordion.Accordion
import com.vaadin.flow.component.accordion.AccordionPanel
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep
import com.vaadin.flow.component.html.Span
import com.vaadin.flow.component.listbox.MultiSelectListBox
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.binder.Binder


class ProjectCreationWizard(
    private val formFieldComponentFactory: FormFieldComponentFactory,
    private val projectTemplateDataConnector: ProjectTemplateDataConnector,
    private val userProvider: UserProvider,
    private var projectTemplateDto: ProjectTemplateDto
) : DialogTemplate("project-creation-wizard") {

    companion object {
        const val PROJECT_INFO = "Project Information"
        const val PROJECT_VARS = "Project Variables"
        const val PROJECT_USERS = "Project Users"
    }

    private val projectInfoPanel: AccordionPanel
    private val projectVariablesPanel: AccordionPanel
    private val projectUsersPanel: AccordionPanel

    private val projectInfoBinder = Binder<ObjectMap>()
    private val projectVariableBinder = Binder<ObjectMap>()
    private val projectUsersBinder = Binder<Set<String>>()

    lateinit var finishButton: Button
    lateinit var cancelButton: Button

    init {
        this.minWidth = "400px"
        val accordion = Accordion()

        val projectInfoFormLayout: FormLayout = createFormLayout()
        projectInfoPanel = accordion.add(PROJECT_INFO, projectInfoFormLayout)

        val projectVariablesFormLayout: FormLayout = createFormLayout()
        projectVariablesPanel = accordion.add(PROJECT_VARS, projectVariablesFormLayout)

        val projectUsersFormLayout: FormLayout = createFormLayout()
        projectUsersPanel = accordion.add(PROJECT_USERS, projectUsersFormLayout)

        fillProjectInfoPanel(projectInfoFormLayout)
        fillProjectVariablesPanel(projectVariablesFormLayout)
        fillProjectUsersPanel(projectUsersFormLayout)

        add(accordion)
        add(HorizontalLayout(cancelButton(), okButton()))
    }

    private fun okButton(): Button {
        finishButton = Button("Done") { finish() }
        finishButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
        finishButton.setId("button-${this.finishButton.text}".toHtmlCase())
        finishButton.isEnabled = false
        projectInfoBinder.addStatusChangeListener {
            finishButton.isEnabled = validForm()
        }
        projectVariableBinder.addStatusChangeListener {
            finishButton.isEnabled = validForm()
        }
        projectUsersBinder.addStatusChangeListener {
            finishButton.isEnabled = validForm()
        }
        return finishButton
    }

    private fun validForm() = projectInfoBinder.isValid && projectVariableBinder.isValid && projectUsersBinder.isValid

    private fun cancelButton(): Button {
        cancelButton = Button("Cancel") {
            projectInfoBinder.removeBean()
            projectVariableBinder.removeBean()
            this.close()
        }
        cancelButton.setId("button-${this.cancelButton.text}".toHtmlCase())
        cancelButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY)
        return cancelButton
    }


    private fun fillProjectInfoPanel(layout: FormLayout) {
        val columnDefs = projectTemplateDataConnector.getColumnMetadataForProjectInformation(dto = projectTemplateDto)

        val projectInfo = ObjectMap(columnDefs.map { it.name to "" }.toMap())
        projectInfoBinder.bean = projectInfo

        columnDefs
            .asSequence()
            .forEach { columnDef ->
                val field = formFieldComponentFactory.createInputField(projectInfoBinder, columnDef)
                if (field is TextField) {
                    field.addValueChangeListener {
                        finishButton.isEnabled = validForm()
                    }
                }
                layout.add(field)
            }

        projectInfoPanel.addOpenedChangeListener {
            if (it.isOpened) {
                projectInfoPanel.summaryText = PROJECT_INFO
            } else if (projectInfoBinder.bean != null) {
                val bean = projectInfoBinder.bean
                projectInfoPanel.summary = createSummary(
                    PROJECT_INFO,
                    bean["name"].toString(),
                    bean["description"].toString()
                )
            }
        }

        val projectInfoButton = Button("Continue") {
            if (doesProjectNeedsVariables()) projectVariablesPanel.isOpened = true else projectUsersPanel.isOpened =
                true
        }
        projectInfoButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
        projectInfoPanel.addContent(projectInfoButton)

        projectInfoBinder.addStatusChangeListener {
            projectInfoButton.isEnabled = projectInfoBinder.isValid
        }
    }

    private fun fillProjectVariablesPanel(layout: FormLayout) {
        if (doesProjectNeedsVariables()) {

            val columnDefs = projectTemplateDataConnector.getColumnMetadataForProjectVariables(dto = projectTemplateDto)
            val objectMap = ObjectMap(projectTemplateDto.variables.map { it.name to "" }.toMap())
            projectVariableBinder.bean = objectMap

            columnDefs
                .asSequence()
                .forEach { columnDef ->
                    val field = formFieldComponentFactory.createInputField(projectVariableBinder, columnDef)
                    layout.add(field)
                }

            projectVariablesPanel.addOpenedChangeListener {
                if (it.isOpened) {
                    projectInfoPanel.summaryText = PROJECT_VARS
                } else if (projectVariableBinder.bean != null) {
                    val bean = projectVariableBinder.bean
                    projectVariablesPanel.summary = createSummary(
                        PROJECT_VARS,
                        *columnDefs.map {
                            "${it.name} -> ${bean[it.name]}"
                        }.toTypedArray()
                    )
                }
            }

            val projectVariablesButton = Button("Continue") { projectUsersPanel.isOpened = true }
            projectVariablesButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
            projectVariablesPanel.addContent(projectVariablesButton)

            projectInfoBinder.addStatusChangeListener {
                projectVariablesButton.isEnabled = projectVariableBinder.isValid
            }

        } else {
            projectVariablesPanel.isEnabled = false
        }

    }


    private fun fillProjectUsersPanel(layout: FormLayout) {

        val users = mutableSetOf<String>()
        projectUsersBinder.bean = users // ObjectMap("users" to users)

        val listBox = MultiSelectListBox<String>()
        listBox.setItems(userProvider.toListDataProvider())
        layout.add(listBox)

        projectUsersBinder.forField(listBox).bind(
            { bean -> bean },
            { _, fieldvalue -> users.clear(); users.addAll(fieldvalue) })

        projectUsersPanel.addOpenedChangeListener {
            if (it.isOpened) {
                projectInfoPanel.summaryText = PROJECT_USERS
            } else if (projectUsersBinder.bean != null) {
                projectUsersPanel.summary = createSummary(
                    PROJECT_USERS,
                    *users.toTypedArray()
                )
            }
        }

    }

    private fun finish() {

        val projectInfo = projectInfoBinder.bean
        val projectVar = projectVariableBinder.bean
        val projectUsers = projectUsersBinder.bean

        this.projectTemplateDto = ProjectTemplateDto(
            id = projectTemplateDto.id,
            name = projectInfo["name"].toString(),
            description = projectInfo["description"].toString(),
            caseDefinition = projectTemplateDataConnector.getCaseDefinitionDtoByKey(projectTemplateDto.cmmnModel),
            cmmnModel = projectTemplateDto.cmmnModel,
            variables = projectTemplateDto.variables.map { it.copy(value = projectVar[it.name].toString()) },
            users = projectUsers.toList(),
            fields = mutableMapOf()

        )
        val projectId = projectTemplateDataConnector.start(projectTemplateDto)
        this.close()
        UI.getCurrent().navigate("projects/$projectId")
    }


    private fun createSummary(title: String, vararg details: String): VerticalLayout {
        val layout = VerticalLayout()
        layout.isSpacing = false
        layout.isPadding = false
        layout.add(title)
        if (details.isNotEmpty()) {
            val detailsLayout = VerticalLayout()
            detailsLayout.isSpacing = false
            detailsLayout.isPadding = false
            detailsLayout.style["font-size"] = "var(--lumo-font-size-s)"
            for (detail in details) {
                if (detail.isNotEmpty()) {
                    detailsLayout.add(Span(detail))
                }
            }
            layout.add(detailsLayout)
        }
        return layout
    }

    private fun createFormLayout(): FormLayout {
        val formLayout = FormLayout()
        formLayout.setResponsiveSteps(
            ResponsiveStep("0", 1),
            ResponsiveStep("20em", 2)
        )
        return formLayout
    }

    private fun doesProjectNeedsVariables(): Boolean {
        return projectTemplateDto.variables.isNotEmpty()
    }
}