package com.qualidify.gui.views.tasks

import com.qualidify.gui.components.Notifications
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.gui.views.configuration.forms.FormDataConnector
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.service.projects.dto.TaskDto
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.spring.annotation.UIScope
import org.springframework.stereotype.Component

@Component
@UIScope
class TaskEditor(
    private val formDataConnector: FormDataConnector,
    private val formFieldComponentFactory: FormFieldComponentFactory,
    private val taskDataConnector: TaskDataConnector,
) : AbstractTaskEditor<TaskDto>(TaskDto::class.java, taskDataConnector, formDataConnector, formFieldComponentFactory) {

    override fun getFormVariables() = formDataConnector.getFormVariables(dtoInstance.id)

    override fun prepareFormButtons(binder: Binder<ObjectMap>, objectMap: ObjectMap) {
        add(HorizontalLayout(
            button("Save", binder, objectMap) {
                taskDataConnector.redirect()
                formDataConnector.saveTaskFormData(dtoInstance.id, objectMap)
            },
            button("Complete", binder, objectMap) {
                taskDataConnector.redirect()
                formDataConnector.completeTaskFormData(dtoInstance.id, objectMap)
            }
        ))
    }

    private fun button(name: String, binder: Binder<ObjectMap>, objectMap: ObjectMap, action: () -> Unit): Button {
        val button = Button(name)
        button.setId(name.lowercase() + "button")
        if (name.lowercase() == "save") {
            button.isEnabled = binder.isValid && binder.hasChanges()
        } else {
            button.isEnabled = binder.isValid
        }

        binder.addValueChangeListener {
            button.isEnabled = binder.isValid
        }

        button.addClickListener {
            binder.validate()
            dtoInstance = taskDataConnector.getTaskDtoById(dtoInstance.id)  // get most recent dto

            when (dtoInstance.assignee) {
                null -> {
                    taskDataConnector.claim(dtoInstance)
                    binder.writeBeanIfValid(objectMap)
                    action.invoke()
                }

                taskDataConnector.currentUser -> {
                    binder.writeBeanIfValid(objectMap)
                    action.invoke()
                }

                else -> {
                    Notifications.warnNotification(
                        "Current user is not assigned to this task, please reassign the task first",
                        throwable = null
                    )
                }
            }
        }

        return button
    }

}