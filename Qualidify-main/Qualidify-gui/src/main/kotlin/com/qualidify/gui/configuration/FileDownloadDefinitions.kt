package com.qualidify.gui.configuration

import java.util.*

/**
 * Reusable definitions for handling file downloads
 *
 * @author Niels Visscher
 * @since 1.0
 */
object FileDownloadDefinitions {
    val mimeTypesExtensions: Map<String, String> = getMimeTypes()

    /**
     * Extension to be used when the mime type of a file is unknown
     */
    const val FALLBACK_FILE_EXTENSION = "bin"

    private fun getMimeTypes(): Map<String, String> {
        return Collections.unmodifiableMap(mapOf(
            "text/plain" to "txt",
            "application/pdf" to "pdf"))
    }
}
