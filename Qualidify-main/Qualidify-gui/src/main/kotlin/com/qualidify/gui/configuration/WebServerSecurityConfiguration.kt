package com.qualidify.gui.configuration

import com.qualidify.configuration.CombinedSecurityUtil
import com.qualidify.gui.views.home.LoginView
import com.vaadin.flow.spring.security.VaadinWebSecurity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer
import org.springframework.security.core.session.SessionRegistryImpl
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.logout.LogoutFilter
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter


@Configuration
@Profile("nosec")
open class NoSecurityConfiguration {

    /**
     * SecurityFilterChain bean that allows access to all endpoints.
     */
    @Bean
    @Throws(Exception::class)
    open fun filterChain(http: HttpSecurity): SecurityFilterChain {
        http
            .authorizeRequests()
            .antMatchers("/")
            .permitAll()
        http
            .csrf()
            .disable()
        http
            .headers()
            .frameOptions()
            .disable()
        return http.build()
    }

    /**
     * WebSecurityCustomizer bean that can be used to customize security for specific endpoints.
     * Note: the "/ignore1" and "/ignore2" are example endpoints that are not used actively (i.e. this
     * webSecurityCustomizer is not doing anything).
     */
    @Bean
    open fun webSecurityCustomizer(): WebSecurityCustomizer {
        return WebSecurityCustomizer { web: WebSecurity ->
            web.ignoring().antMatchers("/ignore1", "/ignore2")
        }
    }

}

@EnableWebSecurity
@Configuration
@Profile("singleserver")
open class SingleServerWebSecurityConfiguration : VaadinWebSecurity() {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        super.configure(http)
        setLoginView(http, LoginView::class.java)
    }

    override fun configure(web: WebSecurity) {
        web.ignoring().antMatchers("/images/**")
        super.configure(web)
    }

}