package com.qualidify.gui.views.configuration.project

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.project.ProjectTemplateView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import javax.annotation.security.RolesAllowed


/**
 * A View for Project Templates
 *
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(ProjectTemplateView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@RolesAllowed(
    Roles.Configuration.Project.TEMPLATE
)
open class ProjectTemplateView(dataCrud: DataCrud<ProjectTemplateDto>) :
    AbstractDataCrudTemplate<ProjectTemplateDto>(IDENTITY, dataCrud) {

    override fun getTitle(): String = TITLE

    companion object {
        const val IDENTITY = "projecttemplates"
        const val TITLE = "Project Templates"
    }
}