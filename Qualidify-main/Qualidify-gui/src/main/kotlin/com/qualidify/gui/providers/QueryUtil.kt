package com.qualidify.gui.providers

import com.qualidify.core.query.*
import com.qualidify.metadata.gui.components.GridFilter
import com.qualidify.metadata.service.Metadata
import com.vaadin.flow.data.provider.Query
import com.vaadin.flow.data.provider.SortDirection
import kotlin.reflect.KClass

/**
 * A helper class for mapping Vaadin Queries to Search and Count Queries
 *
 * @author Marcel Pot
 * @since 1.0
 */
object QueryUtil {

    /**
     * Map the Vaadin Query to a CountQuery
     *
     * @param beanType the Kotlin class
     * @param query the VaadinQuery
     * @result the CountQuery
     */
    @JvmStatic
    fun <T : Any> mapCountQuery(beanType: KClass<T>, query: Query<T, GridFilter>): CountQuery {
        return CountQuery(
            beanType,
            query.filter.map { f -> f.constraints.entries.map { SearchCriterium(it.key, it.value) } }
                .orElse(emptyList())
        )
    }

    /**
     * Map the Vaadin Query to a CountQuery
     *
     * @param mnemonic the Mnemonic
     * @param query the VaadinQuery
     * @result the CountQuery
     */
    @JvmStatic
    fun <T : Any> mapCountQuery(mnemonic: String, query: Query<T, GridFilter>): CountQuery {
        return CountQuery(
            getKClass(mnemonic),
            query.filter.map { f -> f.constraints.entries.map { SearchCriterium(it.key, it.value) } }
                .orElse(emptyList())
        )
    }

    private fun getKClass(mnemonic: String) = Metadata.getDtoByMnemonic(mnemonic)!!.kotlin


    /**
     * Map the Vaadin Query to a SearchQuery
     *
     * @param beanType the Kotlin class
     * @param query the VaadinQuery
     * @result the SearchQuery
     */
    @JvmStatic
    fun <T : Any> mapSearchQuery(beanType: KClass<T>, query: Query<T, GridFilter>): SearchQuery {
        return SearchQuery(
            beanType,
            query.offset,
            query.limit,
            query.filter.map { f -> f.constraints.entries.map { SearchCriterium(it.key, it.value) } }
                .orElse(emptyList()),
            query.sortOrders.asSequence()
                .map { SearchDirection(it.sorted, mapSortOrder(it.direction)) }
                .toList()
        )
    }

    /**
     * Map the Vaadin Query to a SearchQuery
     *
     * @param mnemonic the Mnemonic
     * @param query the VaadinQuery
     * @result the SearchQuery
     */
    @JvmStatic
    fun <T : Any> mapSearchQuery(mnemonic: String, query: Query<T, GridFilter>): SearchQuery {
        return SearchQuery(
            getKClass(mnemonic),
            query.offset,
            query.limit,
            query.filter.map { f -> f.constraints.entries.map { SearchCriterium(it.key, it.value) } }
                .orElse(emptyList()),
            query.sortOrders.asSequence()
                .map { SearchDirection(it.sorted, mapSortOrder(it.direction)) }
                .toList()
        )
    }

    private fun mapSortOrder(direction: SortDirection): Direction? {
        return when (direction) {
            SortDirection.ASCENDING -> Direction.ASC
            SortDirection.DESCENDING -> Direction.DESC
            else -> null
        }
    }
}