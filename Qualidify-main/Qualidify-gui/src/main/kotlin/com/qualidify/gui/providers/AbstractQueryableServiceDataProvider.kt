package com.qualidify.gui.providers

import com.qualidify.core.query.service.Queryable
import com.qualidify.metadata.gui.components.GridFilter
import com.vaadin.flow.data.provider.Query
import java.io.Serializable
import java.util.stream.Stream
import kotlin.reflect.KClass


/**
 * An abstract data provider class with some usefull functions for the
 * dataproviders in the gui
 * @author Marcel Pot
 * @since 1.0
 */
abstract class AbstractQueryableServiceDataProvider<T : Serializable>(
    protected val beanType: KClass<T>,
    protected val queryableService: Queryable<T>
) :
    AbstractServiceDataProvider<T>() {

    override fun sizeInBackEnd(query: Query<T, GridFilter>): Int {
        val countQuery = QueryUtil.mapCountQuery(beanType = beanType, query = query)
        return queryableService.countByQuery(countQuery).count.toInt()
    }

    override fun fetchFromBackEnd(query: Query<T, GridFilter>): Stream<T> {
        val searchQuery = QueryUtil.mapSearchQuery(beanType = beanType, query = query)
        return queryableService.findByQuery(searchQuery).stream()
    }

}