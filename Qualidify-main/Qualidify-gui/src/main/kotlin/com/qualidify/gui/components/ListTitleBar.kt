package com.qualidify.gui.components

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.html.H2
import com.vaadin.flow.component.html.Label
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.component.textfield.TextField

/**
 * Adds a title and eventual buttons
 *
 * @author: Menno Tol
 * @since: 1.0
 */
class ListTitleBar(val title: String, buttons: Array<Button> = emptyArray(), searchField: TextField? = null
) : HorizontalLayout() {

    init {
        alignItems = FlexComponent.Alignment.BASELINE
        setWidthFull()
        add(H2(title))
        add(*buttons)
        if (searchField != null) {
            val spacer = Label()
            add(spacer, searchField)
            setFlexGrow(2.0, spacer)
        }
    }

}