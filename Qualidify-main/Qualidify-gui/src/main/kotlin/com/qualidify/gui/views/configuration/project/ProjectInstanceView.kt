package com.qualidify.gui.views.configuration.project

import com.qualidify.service.configuration.QualidifyConstants
import com.qualidify.gui.views.common.AbstractDataCrudTemplate
import com.qualidify.gui.views.common.MainLayout
import com.qualidify.gui.views.configuration.project.ProjectInstanceView.Companion.IDENTITY
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.qualidify.service.security.roles.Roles
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.spring.annotation.VaadinSessionScope
import javax.annotation.security.RolesAllowed

/**
 * A View for Project Instances
 *
 * @author Menno Tol
 * @since 1.0
 */
@Route(value = IDENTITY, layout = MainLayout::class)
@PageTitle(ProjectInstanceView.TITLE + " | " + QualidifyConstants.QUALIDIFY_NAME)
@VaadinSessionScope
@RolesAllowed(
    Roles.Configuration.Project.INSTANCE
)
open class ProjectInstanceView(dataCrud: DataCrud<ProjectInstanceDto>) :
    AbstractDataCrudTemplate<ProjectInstanceDto>(IDENTITY, dataCrud) {

    override fun getTitle(): String = TITLE

    companion object {
        const val IDENTITY = "projectinstances"
        const val TITLE = "Project Instances"
    }
}