package com.qualidify.gui.dataconnectors

import com.qualidify.gui.providers.CurrentUserProvider
import com.qualidify.service.process.exceptions.WrongImplementationException
import com.qualidify.gui.views.configuration.cmmn.PlanItemDataConnector
import com.qualidify.metadata.annotations.Actions
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.*
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.*

/**
 * A unit test for the [PlanItemDataConnector]
 *
 * @author Menno Tol
 * @since 1.0
 */
class PlanItemDataConnectorTest {

    private val planItemService = mockk<PlanItemService>()
    private val currentUserProvider = mockk<CurrentUserProvider>()

    private val fixture = PlanItemDataConnector(planItemService, currentUserProvider)

    private val planItemDto = DtoFactory.planItemDto()

    @AfterEach
    fun checkTest() {
        confirmVerified(planItemService, currentUserProvider)
        checkUnnecessaryStub(planItemService, currentUserProvider)
    }

    @Test
    fun `Test the start plan item service call`() {
        every { planItemService.startPlanItem(planItemDto) } just Runs

        fixture.start(planItemDto)

        verify(exactly = 1) { planItemService.startPlanItem(planItemDto) }
    }

    @ParameterizedTest
    @MethodSource("getActions")
    fun `Test correct executeAction calls`(action: String) {

        when (action) {
            "Start" -> {
                every { planItemService.startPlanItem(planItemDto) } just Runs

                fixture.executeAction(action, planItemDto)

                verify(exactly = 1) { planItemService.startPlanItem(planItemDto) }


            }
            "Complete" -> {
                every { currentUserProvider.getUserName() } returns "currentUser"

                fixture.executeAction(action, planItemDto)

                verify(exactly = 1) { planItemService.forceCompleteStage(planItemDto, "currentUser") }
                verify(exactly = 1) { currentUserProvider.getUserName() }
            }

            else -> throw WrongImplementationException(
                "Action '$action' not implemented correctly. " +
                        "Check if implementation in Dto and corresponding DataConnector match."
            )
        }
    }

    companion object {

        @JvmStatic
        fun getActions(): Set<String> {
            val set = mutableSetOf<String>()
            set += PlanItemDataConnector(mockk(), mockk()).innerActions.keys

            val dto = PlanItemDto::class.java
            set += if (dto.isAnnotationPresent(Actions::class.java)) {
                dto.getDeclaredAnnotation(Actions::class.java).value
                    .asSequence()
                    .map { it.action }
                    .toList()
            } else {
                dto.getDeclaredAnnotationsByType(Actions::class.java)
                    .asSequence()
                    .map { it.value.toString() }
                    .toList()
            }

            return set
        }
    }
}
