package com.qualidify.gui.dataconnectors

import com.fasterxml.jackson.databind.ObjectMapper
import com.qualidify.gui.views.reports.ReportTemplateDataConnector
import com.qualidify.metadata.DataType
import com.qualidify.service.report.ReportTemplateService
import com.qualidify.service.report.dto.ReportTemplateDto
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*

/**
 * Unit test for [ReportTemplateDataConnector]
 *
 * @author Niels Visscher
 * @since 1.0
 */
internal class ReportTemplateDataConnectorTest {

    private var fixture: ReportTemplateDataConnector? = null

    @BeforeEach
    fun setup() {
        fixture = ReportTemplateDataConnector(mockk<ReportTemplateService>(), ObjectMapper())
    }

    private fun createTestReportDto(parameters: String): ReportTemplateDto {
        return ReportTemplateDto(
            id = UUID.randomUUID(),
            name ="Test template",
            description ="Test template",
            content = "Test".toByteArray(),
            parameters = parameters,
            resources = arrayOf()
        )
    }

    @Test
    fun `ReportTemplateDataConnector test fixture was created`() {
        assertThat(fixture).`as`("Test fixture has been created").isNotEqualTo(null)
    }

    @Test
    fun `Report template parameters are extracted from ReportTemplateDto`() {
        val testDto = createTestReportDto("""
                [
                    {
                        "name": "docVersion",
                        "description": "Document version",
                        "dataType": "STRING",
                        "attributes": {
                            "id": "docVersion",
                            "isRequired": "true"
                        }
                    }
                ]
            """.trimIndent())
        val result = fixture!!.getParametersForTemplate(testDto)
        assertThat(result.size).`as`("A single parameter is extracted").isEqualTo(1)
        assertThat(result.first().name).`as`("Name is extracted correctly").isEqualTo("docVersion")
        assertThat(result.first().description).`as`("Description is extracted correctly").isEqualTo("Document version")
        assertThat(result.first().dataType).`as`("Data type is extracted correctly").isEqualTo(DataType.STRING)
        assertThat(result.first().attributes.keys.size).`as`("Number of attributes is read correctly").isEqualTo(2)
        assertThat(result.first().attributes["isRequired"]).`as`("Required attribute is read correctly").isEqualTo("true")
    }

    @Test
    fun `Multiple report template parameters can be extracted`() {
        val testDto = createTestReportDto("""
                [
                    {
                        "name": "docTitle",
                        "description": "Document title",
                        "dataType": "STRING",
                        "attributes": {
                            "isRequired": "true"
                        }
                    },
                    {
                        "name": "authorBirthday",
                        "description": "Birthday of the author of the report",
                        "dataType": "DATE",
                        "attributes": {
                            "isRequired": "true"
                        }
                    }
                ]
            """.trimIndent())
        val result = fixture!!.getParametersForTemplate(testDto)
        assertThat(result.size).`as`("Two parameters were extracted").isEqualTo(2)
        assertThat(result[1].name).`as`("Second parameter has the correct name").isEqualTo("authorBirthday")
        assertThat(result[1].dataType).`as`("Second parameter has a correctly parsed data type").isEqualTo(DataType.DATE)
    }
}
