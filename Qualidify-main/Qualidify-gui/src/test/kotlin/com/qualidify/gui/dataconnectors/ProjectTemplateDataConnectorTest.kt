package com.qualidify.gui.dataconnectors

import com.qualidify.service.process.exceptions.WrongImplementationException
import com.qualidify.gui.views.configuration.project.ProjectTemplateDataConnector
import com.qualidify.metadata.annotations.Action
import com.qualidify.metadata.annotations.Actions
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.CaseDefinitionService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

/**
 * Unit test for the [ProjectTemplateDataConnector]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class ProjectTemplateDataConnectorTest {

    private val projectTemplateService = mockk<ProjectTemplateService>()
    private val caseDefinitionService = mockk<CaseDefinitionService>()

    private val testSubject = ProjectTemplateDataConnector(projectTemplateService, caseDefinitionService)

    private val mockDto = DtoFactory.projectTemplateDto()

    @BeforeEach
    fun setup() {
        every { projectTemplateService.startProject(mockDto) } returns ""
    }

    @Test
    fun `Test the start project service call`() {
        testSubject.start(mockDto)

        verify(exactly = 1) { projectTemplateService.startProject(mockDto) }
    }

    @ParameterizedTest
    @MethodSource("getActions")
    fun `Test correct executeAction call`(action: String) {
        testSubject.executeAction(action, mockDto)

        when (action) {
            "Start" ->  verify(exactly = 1) { projectTemplateService.startProject(mockDto) }
            else -> throw WrongImplementationException("Action '$action' not implemented correctly. " +
                    "Check if implementation in Dto and corresponding DataConnector match.")
        }
    }

    companion object {

        @JvmStatic
        fun getActions(): Set<String> {
            val actionSet = mutableSetOf<String>()
            actionSet += ProjectTemplateDataConnector(mockk(), mockk()).innerActions.keys

            val dtoType = ProjectTemplateDto::class.java
            actionSet += if (dtoType.isAnnotationPresent(Actions::class.java)) {
                dtoType.getDeclaredAnnotation(Actions::class.java).value
                    .asSequence()
                    .map { it.action}
                    .toList()
            } else {
                dtoType.getDeclaredAnnotationsByType(Action::class.java)
                    .asSequence()
                    .map { it.action }
                    .toList()
            }
            return actionSet
        }
    }

}

