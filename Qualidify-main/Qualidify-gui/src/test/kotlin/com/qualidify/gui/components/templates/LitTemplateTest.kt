package com.qualidify.gui.components.templates

import com.google.common.collect.Sets
import com.qualidify.core.log.LoggerDelegation
import com.vaadin.flow.data.renderer.LitRenderer
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.fail
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.reflections.Reflections
import org.reflections.scanners.Scanners
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.lang.reflect.Method
import java.lang.reflect.Modifier


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class LitTemplateTest {

    private val logger by LoggerDelegation()

    @ParameterizedTest
    @MethodSource("findPrepareTemplateMethods")
    fun testLitTemplates(method: Method) {

        logger.info("Testing ${method.name}() from '${method.declaringClass}'")

        // get the absolute path from the string representation of the Method object
        val directory = method.toString()
            .substringAfterLast(" ") // get the full method name
            .substringBeforeLast(".") // remove the method name to get the class
            .substringBefore("$") // remove all nested classes
            .prependIndent("src/main/kotlin/") // hacky solution to insert a string at the beginning
        val file = File(directory)
        val absolutePath = file.absolutePath.replace(".", "/") + ".kt"

        logger.info("Reading source from: $absolutePath")

        // read the file
        val input = getFileContent(FileInputStream(absolutePath))

        // find properties
        val templateProperties = findElements(input, "item.")
        logger.info("Number of properties found in template: ${templateProperties.size}")
        val functionProperties = findElements(input, "withProperty(\"" )
        logger.info("Number of properties found in function: ${functionProperties.size}")

        if (templateProperties != functionProperties) {
            val difference = Sets.symmetricDifference(templateProperties, functionProperties)
            fail("The following property elements do not match: $difference, \n" +
                    " set 1: $templateProperties \n set 2: $functionProperties")
        }

        // find the function
        val templateFunctions = findElements(input)
        logger.info("Number of functions found in template: ${templateFunctions.size}")
        val functionFunctions = findElements(input, "withFunction(\"")
        logger.info("Number of functions found in functions: ${functionFunctions.size}")

        if (templateFunctions != functionFunctions) {
            val difference = Sets.symmetricDifference(templateFunctions, functionFunctions)
            fail("The following function elements do not match: $difference, \n" +
                " set 1: $templateFunctions \n set 2: $functionFunctions")
        }

    }


    /**
     * Find elements defined in LitRenderer source code
     */
    private fun findElements(source: String, expression: String = ""): MutableSet<String> {
        val set = mutableSetOf<String>()

        // transform source to work with indexesOf() and prevent requiring incomprehensible Regex expressions
        val source = source
            .replace("$", "variable")
            .replace("{", "start")
            .replace("}", "end")

        var prefix = ""
        var suffix = ""
        var pattern = ""

        when (expression) {
            "item." ->             { prefix = expression; suffix = "end"; pattern = "variablestart" }
            "withProperty(\"" ->   { prefix = expression; suffix = "\")"; pattern = "" }
            "withFunction(\"" ->   { prefix = expression; suffix = "\")"; pattern = "" }
            else ->                { pattern = "variablestart"; suffix = "end" }
        }

        val indexes = source.indexesOf(pattern + prefix)
        indexes.forEach { index ->
            val element = source.substring(index).removePrefix(pattern + prefix).substringBefore(suffix)
            if (!element.startsWith("item.")) {
                set += element
            }

            set -= "dto.id"  // remove false positives, such as ${dto.id}
        }

        return set
    }


    /**
     * Extension function of String to that returns a list of indices matching the pattern
     */
    private fun String.indexesOf(pattern: String): List<Int> = Regex.fromLiteral(pattern)
            .findAll(this)
            .map { it.range.first }
            .toList()

    /**
     * Uses reflection to find all prepareTemplate() methods
     */
    private fun findPrepareTemplateMethods(): MutableSet<Method> {
        val reflections = Reflections("com.qualidify.gui", Scanners.MethodsReturn)
        val methods = reflections.getMethodsReturn(LitRenderer::class.java)

        val selection = mutableSetOf<Method>()
        methods.forEach {
            if (!Modifier.isAbstract(it.modifiers)) {
                selection.add(it)
            }
        }

        logger.info("Number of non-abstract methods returning a LitRenderer found: ${selection.size}")

        return selection
    }

    /**
     * Reads the file content and transforms to a string
     */
    private fun getFileContent(fileInputStream: FileInputStream, encoding: String = "utf-8"): String {

        val bufferedReader = BufferedReader(InputStreamReader(fileInputStream, encoding))
        val stringBuilder = StringBuilder()

        bufferedReader.forEachLine { line ->
            stringBuilder.append(line)
            stringBuilder.append('\n')
        }

        return stringBuilder.toString()
    }

}
