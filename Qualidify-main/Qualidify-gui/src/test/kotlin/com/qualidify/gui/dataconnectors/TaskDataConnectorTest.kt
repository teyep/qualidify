package com.qualidify.gui.dataconnectors

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.process.exceptions.WrongImplementationException
import com.qualidify.gui.providers.CurrentUserProvider
import com.qualidify.gui.providers.UserProvider
import com.qualidify.gui.views.configuration.cmmn.TaskDataConnector
import com.qualidify.metadata.annotations.Action
import com.qualidify.metadata.annotations.Actions
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.dto.*
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.ReviewResponse
import com.qualidify.service.quality.ReviewResponseRouter
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.*

/**
 * Unit test the [TaskDataConnector]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class TaskDataConnectorTest {

    private val logger by LoggerDelegation()

    private val taskService = mockk<TaskService>()
    private val reviewResponseRouter = mockk<ReviewResponseRouter>()
    private val currentUserProvider = mockk<CurrentUserProvider>()
    private val userProvider = mockk<UserProvider>()

    private val fixture = TaskDataConnector(taskService, reviewResponseRouter, currentUserProvider, userProvider)

    private val taskDto = DtoFactory.taskDto()
    private val currentUser = "currentUser"

//    @BeforeEach
//    fun setup() {
//        every { currentUserProvider.getUserName() } returns currentUser
//        every { taskService.completeOrResolveTask(taskDto, currentUser) } just Runs
//        every { taskService.claimTask(taskDto, currentUser) } just Runs
//        every { taskService.unclaimTask(taskDto, currentUser) } just Runs
//        every { taskService.assignTask(any(), any(), any()) } just Runs
//    }

    @AfterEach
    fun checkTest() {
        confirmVerified(taskService, reviewResponseRouter, currentUserProvider, userProvider)
        checkUnnecessaryStub(taskService, reviewResponseRouter, currentUserProvider, userProvider)
    }

    @Test
    fun `Test complete TaskService call`() {
        every { currentUserProvider.getUserName() } returns currentUser
        every { taskService.completeOrResolveTask(taskDto, currentUser) } just Runs

        fixture.complete(taskDto)

        verify(exactly = 1) { taskService.completeOrResolveTask(taskDto, currentUser) }
        verify(exactly = 1) { currentUserProvider.getUserName() }
    }

    @Test
    fun `Test claim TaskService call`() {
        every { currentUserProvider.getUserName() } returns currentUser
        every { taskService.claimTask(taskDto, currentUser) } just Runs

        fixture.claim(taskDto)

        verify(exactly = 1) { taskService.claimTask(taskDto, currentUser) }
        verify(exactly = 1) { currentUserProvider.getUserName() }
    }

    @Test
    fun `Test unclaim TaskService call`() {
        every { currentUserProvider.getUserName() } returns currentUser
        every { taskService.unclaimTask(taskDto, currentUser) } just Runs

        fixture.unclaim(taskDto)

        verify(exactly = 1) { taskService.unclaimTask(taskDto, currentUser) }
        verify(exactly = 1) { currentUserProvider.getUserName() }
    }

    @ParameterizedTest
    @MethodSource("getActions")
    fun `Test correct executeAction call`(action: String) {

        when (action) {
            "Complete" -> {
                every { currentUserProvider.getUserName() } returns currentUser
                every { taskService.completeOrResolveTask(taskDto, currentUser) } just Runs

                fixture.executeAction(action, taskDto)

                verify(exactly = 1) { taskService.completeOrResolveTask(taskDto, currentUser) }
                verify(exactly = 1) { currentUserProvider.getUserName() }
            }

            "Claim" -> {
                every { taskService.claimTask(taskDto, currentUser) } just Runs
                every { currentUserProvider.getUserName() } returns currentUser

                fixture.executeAction(action, taskDto)

                verify(exactly = 1) { taskService.claimTask(taskDto, currentUser) }
                verify(exactly = 1) { currentUserProvider.getUserName() }
            }

            "Unclaim" -> {
                every { taskService.unclaimTask(taskDto, currentUser) } just Runs
                every { currentUserProvider.getUserName() } returns currentUser

                fixture.executeAction(action, taskDto)

                verify(exactly = 1) { taskService.unclaimTask(taskDto, currentUser) }
                verify(exactly = 1) { currentUserProvider.getUserName() }
            }

            "Approve" -> {
                every { currentUserProvider.getUserName() } returns currentUser

                fixture.executeAction(action, taskDto)

                verify(exactly = 1) {
                    reviewResponseRouter.routeRequest(
                        taskDto,
                        currentUser,
                        ReviewResponse.APPROVE
                    )
                }
                verify(exactly = 1) { currentUserProvider.getUserName() }

            }

            "Reject" -> {
                every { currentUserProvider.getUserName() } returns currentUser

                fixture.executeAction(action, taskDto)

                verify(exactly = 1) {
                    reviewResponseRouter.routeRequest(
                        taskDto,
                        currentUser,
                        ReviewResponse.REJECT
                    )
                }
                verify(exactly = 1) { currentUserProvider.getUserName() }

            }

            "Assign" -> {
                fixture.executeAction(action, taskDto)

                verify(exactly = 0) { taskService.assignTask(any(), any(), any()) }
                verify(exactly = 1) { userProvider.retrieve() }

            }

            else -> throw WrongImplementationException(
                "Action '$action' not implemented correctly. " +
                        "Check if implementation in Dto and corresponding DataConnector match."
            )
        }
    }

    companion object {

        @JvmStatic
        fun getActions(): Set<String> {
            val actionSet = mutableSetOf<String>()
            actionSet += TaskDataConnector(mockk(), mockk(), mockk(), mockk()).innerActions.keys

            val dtoType = TaskDto::class.java
            actionSet += if (dtoType.isAnnotationPresent(Actions::class.java)) {
                dtoType.getDeclaredAnnotation(Actions::class.java).value
                    .asSequence()
                    .map { it.action }
                    .toList()
            } else {
                dtoType.getDeclaredAnnotationsByType(Action::class.java)
                    .asSequence()
                    .map { it.action }
                    .toList()
            }
            return actionSet
        }

    }
}
