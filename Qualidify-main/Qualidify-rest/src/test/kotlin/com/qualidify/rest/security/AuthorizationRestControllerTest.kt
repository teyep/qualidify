package com.qualidify.rest.security

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.RestQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.security.SecurityService
import com.qualidify.service.security.dto.SecurityGroupDto
import com.qualidify.service.user.UserService
import com.qualidify.service.user.dto.CreateUserCommand
import com.qualidify.service.user.dto.UserDto
import com.qualidify.service.user.exceptions.UserNotFoundException
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient()
@ExtendWith(SpringExtension::class)
@WithMockUser
@DirtiesContext
class AuthorizationRestControllerTest {
    @Autowired
    private lateinit var webClient: WebTestClient

    @MockBean
    private lateinit var securityService: SecurityService

    @MockBean
    private lateinit var userService: UserService

    private val mapper = jacksonObjectMapper()


    @Test
    @WithMockUser
    fun testIsUserLoggedIn() {

        webClient.get().uri("/authorization/isuserloggedin")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody(Boolean::class.java)
            .value({ it }, equalTo(true))

    }

    @WithAnonymousUser
    @Test
    fun testIsUserNotLoggedIn() {
        webClient.get().uri("/authorization/isuserloggedin")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isUnauthorized
    }

    @Test
    fun testSecurityGroupCount() {
        val countQuery = CountQuery<SecurityGroupDto>()
        val countQueryJson = mapper.writeValueAsString(RestQuery())

        `when`(securityService.countByQuery(countQuery))
            .thenReturn(CountResponse(7))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/authorization/securitygroup/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(countQueryJson)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(7)


        verify(securityService).countByQuery(countQuery)
    }

    @Test
    fun testSecurityGroupFind() {
        val searchQuery = SearchQuery<SecurityGroupDto>()
        val searchQueryJson = mapper.writeValueAsString(RestQuery())

        `when`(securityService.findByQuery(searchQuery))
            .thenReturn(
                listOf(
                    securityGroupDto("left"),
                    securityGroupDto("right"),
                )
            )

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/authorization/securitygroup/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(searchQueryJson)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                [
                    {
                        "name":"left",
                        "description":"left",
                        "enabled":true,
                        "roles":["Vegter's"]
                    },{
                        "name":"right",
                        "description":"right",
                        "enabled":true,
                        "roles":["Vegter's"]
                    }
                ]""".trimIndent()
            )

        verify(securityService).findByQuery(searchQuery)
    }

    @Test
    @WithMockUser
    fun testCreateSecurityGroup() {
        val group = securityGroupDto("Upper")

        doNothing().`when`(securityService).createGroup(group)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/authorization/securitygroup/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(group))
            .exchange()
            .expectStatus().isCreated
            .expectBody(String::class.java).isEqualTo("{}")
    }


    @Test
    fun testUpdateSecurityGroup() {
        val group = securityGroupDto("Bottom")

        doNothing().`when`(securityService).update(group)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/authorization/securitygroup/update")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(group))
            .exchange()
            .expectStatus().isAccepted
            .expectBody().isEmpty
    }


    private fun securityGroupDto(name: String) = SecurityGroupDto(
        name = name,
        description = name,
        enabled = true,
        roles = mutableSetOf("Vegter's")
    )


    @Test
    fun testUserCount() {
        val countQuery = CountQuery<UserDto>()
        val countQueryJson = mapper.writeValueAsString(RestQuery())

        `when`(userService.countByQuery(countQuery))
            .thenReturn(CountResponse(3))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/authorization/user/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(countQueryJson)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(3)


        verify(userService).countByQuery(countQuery)
    }

    @Test
    fun testUserFind() {
        val searchQuery = SearchQuery<UserDto>()
        val searchQueryJson = mapper.writeValueAsString(RestQuery())

        `when`(userService.findByQuery(searchQuery))
            .thenReturn(
                listOf(
                    user("Doc"),
                    user("Grumpy"),
                    user("Happy"),
                    user("Sleepy"),
                    user("Bashful"),
                    user("Sneezy"),
                    user("Dopey")
                )
            )

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/authorization/user/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(searchQueryJson)

            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                [
                    {
                        "userId":null,
                        "username":"Doc",
                        "email":"",
                        "fields":{},
                        "securityGroups":[]
                    },{
                        "userId":null,
                        "username":"Grumpy",
                        "email":"",
                        "fields":{},
                        "securityGroups":[]
                    },{
                        "userId":null,
                        "username":"Happy",
                        "email":"",
                        "fields":{},
                        "securityGroups":[]
                    },{
                        "userId":null,
                        "username":"Sleepy",
                        "email":"",
                        "fields":{},
                        "securityGroups":[]
                    },{
                        "userId":null,
                        "username":"Bashful",
                        "email":"",
                        "fields":{},
                        "securityGroups":[]
                    },{
                        "userId":null,
                        "username":"Sneezy",
                        "email":"",
                        "fields":{},
                        "securityGroups":[]
                    },{
                        "userId":null,
                        "username":"Dopey",
                        "email":"",
                        "fields":{},
                        "securityGroups":[]
                    }]
                    """.trimIndent()
            )

        verify(userService).findByQuery(searchQuery)
    }

    @Test
    fun testCreateUser() {
        val createCommand = createUser("Snow White")

        doReturn(user("Snow White")).`when`(userService).create(createCommand)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/authorization/user/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(createCommand))
            .exchange()
            .expectStatus().isCreated
            .expectBody(String::class.java)
    }

    @Test
    fun testUpdateUser() {
        val user = user("Evil Queen")

        doReturn(user).`when`(userService).update(user)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/authorization/user/update")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(user))
            .exchange()
            .expectStatus().isAccepted
            .expectBody(String::class.java)
    }

    @Test
    fun testCreateUserWithIllegalArgumentException() {
        val createCommand = createUser("Prince")

        doThrow(IllegalArgumentException("Prince is missing his white horse"))
            .`when`(userService).create(createCommand)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/authorization/user/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(createCommand))
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(String::class.java).isEqualTo("Prince is missing his white horse")
    }

    @Test
    fun testUpdateUSerWithUserNorFoundException() {
        val user = user("Three pigs")

        doThrow(UserNotFoundException("Three pigs are not part of the tale of Snow White"))
            .`when`(userService).update(user)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/authorization/user/update")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(user))
            .exchange()
            .expectStatus().isNotFound
            .expectBody(String::class.java)
            .isEqualTo("Three pigs are not part of the tale of Snow White")
    }

    private fun user(name: String) = UserDto(username = name)
    private fun createUser(name: String) = CreateUserCommand(userName = name, password = "", email = "")
}