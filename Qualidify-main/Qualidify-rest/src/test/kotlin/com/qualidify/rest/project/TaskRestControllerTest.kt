package com.qualidify.rest.project

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.RestQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.FormDefinitionService
import com.qualidify.service.process.HistoricTaskService
import com.qualidify.service.process.dto.FormDefinitionDto
import com.qualidify.service.process.exceptions.FormNotFoundException
import com.qualidify.service.process.exceptions.TaskServiceException
import com.qualidify.service.projects.FormMetadataService
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.ReviewResponse
import com.qualidify.service.quality.ReviewResponseRouter
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient()
@ExtendWith(SpringExtension::class)
@WithMockUser
@DirtiesContext
class TaskRestControllerTest {

    @Autowired
    private lateinit var webClient: WebTestClient

    @MockBean
    private lateinit var taskService: TaskService

    @MockBean
    private lateinit var historicTaskService: HistoricTaskService

    @MockBean
    private lateinit var formDefinitionService: FormDefinitionService

    @MockBean
    private lateinit var formMetadataService: FormMetadataService

    @MockBean
    private lateinit var reviewResponseRouter: ReviewResponseRouter

    private val objectMapper = jacksonObjectMapper().registerModule(JavaTimeModule())

    @Test
    @WithMockUser
    fun testHistoricTaskcount() {
        val countQuery = CountQuery<TaskDto>()
        val result = (3..14).random().toLong()

        `when`(historicTaskService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/tasks/historic/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(historicTaskService).countByQuery(countQuery)
    }

    @Test
    fun testHistoricTaskfind() {
        val searchQuery = SearchQuery<TaskDto>()

        `when`(historicTaskService.findByQuery(searchQuery))
            .thenReturn(listOf(DtoFactory.historicTaskDto()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/tasks/historic/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("[0].id").isEqualTo("mockHistoricTaskId")


        verify(historicTaskService).findByQuery(searchQuery)
    }

    @Test
    fun getHistoricTaskById() {
        val dto = DtoFactory.historicTaskDto()

        `when`(historicTaskService.getHistoricTaskDtoById("someid"))
            .thenReturn(dto)

        webClient.get().uri("/tasks/historic/find/someid")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                {
                    "id":"mockHistoricTaskId",
                    "name":"historicTaskName",
                    "taskDefinitionKey":"taskDefinitionKey",
                    "caseInstanceId":"caseInstanceId",
                    "caseDefinitionId":"caseDefinitionKey",
                    "planItemInstanceId":"planItemInstanceId",
                    "formKey":"formKey",
                    "owner":"owner",
                    "assignee":"assignee",
                    "type":"CMMN",
                    "stage":"stageId",
                    "isQualityShell":false,
                    "regularCaseId": null,
                    "qualityShellId":"qualityShellId"
                }
                    """.trimIndent()
            )

        verify(historicTaskService).getHistoricTaskDtoById("someid")

    }

    @Test
    fun testHistoricFormVariables() {
        val vars = mapOf<String, Any?>("question" to "answer", "nullable" to null)

        `when`(historicTaskService.getHistoricFormVariables("someid"))
            .thenReturn(vars)

        webClient.get().uri("/tasks/historic/someid/variables")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                {
                    "question":"answer",
                    "nullable":null
                }
                """.trimIndent()
            )

        verify(historicTaskService).getHistoricFormVariables("someid")
    }


    @Test
    fun testTaskcount() {
        val countQuery = CountQuery<TaskDto>()
        val result = (3..8).random().toLong()

        `when`(taskService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/tasks/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(taskService).countByQuery(countQuery)
    }

    @Test
    fun testTaskFind() {
        val searchQuery = SearchQuery<TaskDto>()

        `when`(taskService.findByQuery(searchQuery))
            .thenReturn(listOf(DtoFactory.taskDto()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/tasks/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("[0].id").isEqualTo("mockTaskDto")

        verify(taskService).findByQuery(searchQuery)
    }

    @Test
    @WithMockUser("Papageno")
    fun testTaskAction_complete() {

        val taskDto = DtoFactory.taskDto()
        doNothing().`when`(taskService).completeOrResolveTask(taskDto, "Papageno")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/tasks/action/complete")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(taskDto))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json("{}")

        verify(taskService).completeOrResolveTask(taskDto, "Papageno")
    }

    @Test
    @WithMockUser("Papagena")
    fun testTaskaction_complete_TaskException() {
        val taskDto = DtoFactory.taskDto()


        doThrow(TaskServiceException("cannot complete"))
            .`when`(taskService).completeOrResolveTask(taskDto, "Papagena")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/tasks/action/complete")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(taskDto))
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(String::class.java).isEqualTo("cannot complete")

        verify(taskService).completeOrResolveTask(taskDto, "Papagena")
    }

    @Test
    @WithMockUser("Papageno")
    fun testTaskAction_claim() {
        val taskDto = DtoFactory.taskDto()
        doNothing().`when`(taskService).claimTask(taskDto, "Papageno")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/tasks/action/claim")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(taskDto)
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(taskService).claimTask(taskDto, "Papageno")
    }

    @Test
    @WithMockUser("Papageno")
    fun testTaskAction_unclaim() {
        val taskDto = DtoFactory.taskDto()

        doNothing().`when`(taskService).unclaimTask(taskDto, "Papageno")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/tasks/action/unclaim")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(taskDto)
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(taskService).unclaimTask(taskDto, "Papageno")
    }

    @Test
    fun testTaskAction_unknown() {
        val taskDto = DtoFactory.taskDto()

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/tasks/action/unknown")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(taskDto)
            .exchange()
            .expectStatus().isOk
            .expectBody(String::class.java).isEqualTo("\"Error\"")

        verifyNoInteractions(taskService)
    }

    @Test
    @WithMockUser("Papageno")
    fun testTaskAssign() {
        val taskDto = DtoFactory.taskDto()

        doNothing().`when`(taskService).assignTask(taskDto, "tamino", "Papageno")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/tasks/action/assign/tamino")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(taskDto)
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(taskService).assignTask(taskDto, "tamino", "Papageno")
    }

    @Test
    @WithMockUser("Papageno")
    fun testTaskApproveAction() {
        val taskDto = DtoFactory.taskDto()

        doNothing().`when`(reviewResponseRouter).routeRequest(taskDto, "Papageno", ReviewResponse.APPROVE)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/tasks/action/approve/")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(taskDto)
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(reviewResponseRouter).routeRequest(taskDto, "Papageno", ReviewResponse.APPROVE)

    }

    @Test
    @WithMockUser("Papagena")
    fun testTaskRejectAction() {
        val taskDto = DtoFactory.taskDto()

        doNothing().`when`(reviewResponseRouter).routeRequest(taskDto, "Papagena", ReviewResponse.REJECT)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/tasks/action/reject/")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(taskDto)
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(reviewResponseRouter).routeRequest(taskDto, "Papagena", ReviewResponse.REJECT)
    }

    @Test
    fun testTaskById() {
        val taskDto = DtoFactory.taskDto()
        `when`(taskService.getTaskDtoById(taskDto.id)).thenReturn(taskDto)


        webClient
            .get().uri("/tasks/find/${taskDto.id}")
            .exchange()
            .expectStatus().isOk
            .expectBody().jsonPath("$.name").isEqualTo("mockTaskName")

        verify(taskService).getTaskDtoById(taskDto.id)
    }

    @Test
    fun testFormcount() {
        val countQuery = CountQuery<FormDefinitionDto>()
        val result = (0..15).random().toLong()

        `when`(formDefinitionService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/tasks/form/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(formDefinitionService).countByQuery(countQuery)
    }

    @Test
    fun testFormFind() {
        val searchQuery = SearchQuery<FormDefinitionDto>()

        val formDefinitionDto = DtoFactory.formDefinitionDto()

        `when`(formDefinitionService.findByQuery(searchQuery))
            .thenReturn(listOf(formDefinitionDto))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/tasks/form/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                [
                    {   
                        "id":"formDefinitionId",
                        "name":"formDefinitionName",    
                        "key":"formDefinitionKey",
                        "version":1,
                        "form":{}
                    }
                ]
                """.trimIndent()
            )

        verify(formDefinitionService).findByQuery(searchQuery)
    }

    @Test
    fun testColumnMetadataByTaskId() {
        `when`(formMetadataService.loadFormModelMetadataByTaskId("some-unique-task-id"))
            .thenReturn(listOf(DtoFactory.columnDef("A"), DtoFactory.columnDef("B")))

        webClient
            .get().uri("/tasks/form/some-unique-task-id/metadata")
            .exchange()
            .expectStatus().isOk
            .expectBody().json(
                """
                    [
                      {
                        "name":"A",
                        "description":"A",
                        "dataType":"STRING",
                        "attributes":{},
                        "hidden":false,
                        "viewable":true,
                        "editable":true,
                        "searchable":true,
                        "sortable":true,
                        "fieldAccess":"DEFAULT"
                      },
                      {
                        "name":"B",
                        "description":"B",
                        "dataType":"STRING",
                        "attributes":{},
                        "hidden":false,
                        "viewable":true,
                        "editable":true,
                        "searchable":true,
                        "sortable":true,
                        "fieldAccess":"DEFAULT"
                      }
                    ]
                """.trimIndent()
            )

        verify(formMetadataService).loadFormModelMetadataByTaskId("some-unique-task-id")
    }

    @Test
    fun testColumnMetadataByTaskId_FormNotFoundException() {
        `when`(formMetadataService.loadFormModelMetadataByTaskId("some-unique-task-id"))
            .thenThrow(FormNotFoundException("can not find your unique task"))

        webClient
            .get().uri("/tasks/form/some-unique-task-id/metadata")
            .exchange()
            .expectStatus().isNotFound
            .expectBody(String::class.java).isEqualTo("can not find your unique task")

        verify(formMetadataService).loadFormModelMetadataByTaskId("some-unique-task-id")
    }


    @Test
    fun testFormVariables() {
        `when`(formMetadataService.getFormVariables("some-unique-task-id"))
            .thenReturn(
                mapOf(
                    "question" to "Answer to the ultimate question of life, the universe, and everything",
                    "answer" to 42
                )
            )


        webClient
            .get().uri("/tasks/some-unique-task-id/form/variables")
            .exchange()
            .expectStatus().isOk
            .expectBody().json(
                """
                {
                  "question":"Answer to the ultimate question of life, the universe, and everything",
                  "answer":42
                }
            """.trimIndent()
            )

        verify(formMetadataService).getFormVariables("some-unique-task-id")
    }

    @Test
    fun testFormVariables_FormNotFoundException() {
        `when`(formMetadataService.getFormVariables("some-unique-task-id"))
            .thenThrow(FormNotFoundException("can not find your unique task"))


        webClient
            .get().uri("/tasks/some-unique-task-id/form/variables")
            .exchange()
            .expectStatus().isNotFound
            .expectBody(String::class.java).isEqualTo("can not find your unique task")

        verify(formMetadataService).getFormVariables("some-unique-task-id")
    }

    @Test
    fun testSaveFormData() {
        val data = mapOf("question" to "6 times 7", "answer" to 42)
        doNothing().`when`(formMetadataService).saveTaskFormData("some-unique-task-id", data)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/tasks/some-unique-task-id/form/save")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(data))
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(formMetadataService).saveTaskFormData("some-unique-task-id", data)

    }

    @Test
    fun testCompleteTaskFormData() {
        val data = mapOf("question" to "6 times 7", "answer" to 42)
        doNothing().`when`(formMetadataService).completeTaskFormData("some-unique-task-id", data)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/tasks/some-unique-task-id/form/complete")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(data))
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")


        verify(formMetadataService).completeTaskFormData("some-unique-task-id", data)
    }

}