package com.qualidify.rest.config

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.RestQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.DtoFactory.configurationCodeDto
import com.qualidify.service.configuration.ConfigurationDomainNotFoundException
import com.qualidify.service.configuration.ConfigurationService
import com.qualidify.service.configuration.dto.ConfigurationCodeDto
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient()
@ExtendWith(SpringExtension::class)
@WithMockUser
@DirtiesContext
internal class ConfigurationRestControllerTest() {

    @Autowired
    private lateinit var webClient: WebTestClient

    @MockBean
    lateinit var configurationService: ConfigurationService

    @Test
    fun count() {
        val countQuery = CountQuery<ConfigurationCodeDto>()
        val result = (300..500).random().toLong()

        `when`(configurationService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/config/domain/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(RestQuery())
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(configurationService).countByQuery(countQuery)
    }

    @Test
    fun find() {
        val searchQuery = SearchQuery<ConfigurationCodeDto>()

        `when`(configurationService.findByQuery(searchQuery))
            .thenReturn(listOf(configurationCodeDto("Automatic", "igore")))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/config/domain/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(RestQuery())
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                [
                  {
                    "domain": "Automatic",
                    "code": "igore",
                    "description": "A testing configurationcode",
                    "dataType": "BOOLEAN",
                    "value": "TRUE",
                    "active": true
                  }
                ]
            """.trimIndent()
            )

        verify(configurationService).findByQuery(searchQuery)
    }

    @Test
    fun create() {
        val code = configurationCodeDto("Register", "never")
        doNothing()
            .`when`(configurationService).createCode(code)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/config/domain/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(code)
            .exchange()
            .expectStatus().isAccepted
            .expectBody().isEmpty

        verify(configurationService).createCode(code)
    }

    @Test
    fun create_IllegalArgumentException() {
        val code = configurationCodeDto("Register", "never")
        doThrow(IllegalArgumentException("Cannot register Register"))
            .`when`(configurationService).createCode(code)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/config/domain/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(code)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(String::class.java).isEqualTo("Cannot register Register")

        verify(configurationService).createCode(code)
    }

    @Test
    fun create_ConfigurationDomainNotFoundException() {
        val code = configurationCodeDto("Register", "never")
        doThrow(ConfigurationDomainNotFoundException("Register is not registered yet"))
            .`when`(configurationService).createCode(code)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/config/domain/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(code)
            .exchange()
            .expectStatus().isNotFound
            .expectBody(String::class.java).isEqualTo("Register is not registered yet")


        verify(configurationService).createCode(code)
    }

    @Test
    fun update() {
        val code = configurationCodeDto("Register", "never")
        doNothing()
            .`when`(configurationService).updateCode(code)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/config/domain/update")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(code)
            .exchange()
            .expectStatus().isAccepted
            .expectBody().isEmpty

        verify(configurationService).updateCode(code)
    }
}