package com.qualidify.rest

import org.flowable.engine.ProcessEngineConfiguration
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.security.Principal
import java.util.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient()
@ExtendWith(SpringExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext
internal class GlobalMethodSecurityTest {

    @Autowired
    private lateinit var webClient: WebTestClient

    @Autowired
    private lateinit var processEngineConfiguration: ProcessEngineConfiguration


    @WithMockUser(value = "user", roles = ["{all.web.read}"])
    @Test
    fun allowedRequestShouldSucceedWith200() {

        webClient.mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/user/me")
            .exchange()
            .expectStatus().isOk
            .expectBody(String::class.java).isEqualTo("user")
    }

    @WithAnonymousUser
    @Test
    fun notAllowedRequestShouldRedirectToLogin() {

        webClient.mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/user/me")
            .exchange()
            .expectStatus().isUnauthorized
        //.expectHeader().location("/login") TODO: QUAL-530
    }


    @WithMockUser(value = "user", roles = ["read"])
    @Test
    fun requestWithIncorrectRoleShouldNotSucceed_PreAuthorize() {

        webClient.mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/user/16")
            .exchange()
            .expectStatus().isForbidden
    }

    @WithMockUser(value = "user", roles = ["write"])
    @Test
    fun requestWithCorrectRoleShouldSucceed_PreAuthorize() {

        webClient.mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/user/16")
            .exchange()
            .expectStatus().isOk
            .expectBody(String::class.java).isEqualTo("User 16")
    }

    /**
     * The Flowable engine needs to be properly shut down after test execution, otherwise console logging will
     * run over other tests.
     */
    @AfterAll
    fun tearDown() {
        processEngineConfiguration.asyncExecutor.shutdown()
    }

}

@RestController
@RequestMapping(value = ["user"], produces = [MediaType.APPLICATION_JSON_VALUE])
open class TestRestController {

    @GetMapping("/me")
    open fun currentUser(principal: Principal?): Mono<String> {
        return principal?.name.toMono()
    }

    @PreAuthorize("hasRole('write') or hasRole('admin')")
    @GetMapping("/{userId}")
    open fun userByUserId(@PathVariable("userId") userId: Int): Mono<String> {
        return "User $userId".toMono()
    }
}