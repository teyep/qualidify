package com.qualidify.rest.project

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.RestQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.*
import com.qualidify.service.process.dto.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient()
@ExtendWith(SpringExtension::class)
@WithMockUser
@DirtiesContext
class ProcessRestControllerTest() {

    @Autowired
    private lateinit var webClient: WebTestClient

    @MockBean
    private lateinit var processDefinitionService: BpmnDefinitionService

    @MockBean
    private lateinit var caseDefinitionService: CaseDefinitionService

    @MockBean
    private lateinit var caseInstanceService: CaseInstanceService

    @MockBean
    private lateinit var planItemService: PlanItemService

    private val objectMapper = jacksonObjectMapper()

    @Test
    fun testProcessCount() {
        val countQuery = CountQuery<BusinessProcessDefinitionDto>()
        val result = (5..150).random().toLong()

        `when`(processDefinitionService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/process/processdef/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(processDefinitionService).countByQuery(countQuery)
    }

    @Test
    fun testProcessFind() {
        val searchQuery = SearchQuery<BusinessProcessDefinitionDto>()

        `when`(processDefinitionService.findByQuery(searchQuery))
            .thenReturn(listOf(DtoFactory.businessProcessDefinitionDto()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/process/processdef/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("[0].id").isEqualTo("mockProcessDefId")

        verify(processDefinitionService).findByQuery(searchQuery)
    }

    @Test
    fun testCaseDefinitionCount() {
        val countQuery = CountQuery<CaseDefinitionDto>()
        val result = (1..4).random().toLong()

        `when`(caseDefinitionService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/process/casedef/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(caseDefinitionService).countByQuery(countQuery)
    }

    @Test
    fun testCaseDefinitionFind() {
        val searchQuery = SearchQuery<CaseDefinitionDto>()

        `when`(caseDefinitionService.findByQuery(searchQuery))
            .thenReturn(listOf(DtoFactory.caseDefinitionDto()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/process/casedef/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("[0].id").isEqualTo("caseDefinitionId")

        verify(caseDefinitionService).findByQuery(searchQuery)
    }


    @Test
    fun testCaseDefinitionByKey() {

        `when`(caseDefinitionService.getCaseDefinitionDtoByKey("keesId"))
            .thenReturn(DtoFactory.caseDefinitionDto())

        webClient
            .get().uri("/process/casedef/keesId")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                {
                  "id":"caseDefinitionId",
                  "name":"caseDefinitionName",
                  "key":"caseDefinitionKey",
                  "version":1,
                  "deploymentResourceId":null,
                  "graphicalNotation":null
                }
            """.trimIndent()
            )

        verify(caseDefinitionService).getCaseDefinitionDtoByKey("keesId")
    }


    @Test
    fun testCaseInstanceCount() {
        val countQuery = CountQuery<CaseInstanceDto>()
        val result = (7..11).random().toLong()

        `when`(caseInstanceService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/process/caseinst/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(caseInstanceService).countByQuery(countQuery)
    }

    @Test
    fun testCaseInstanceFind() {
        val searchQuery = SearchQuery<CaseInstanceDto>()

        `when`(caseInstanceService.findByQuery(searchQuery))
            .thenReturn(listOf(DtoFactory.caseInstanceDto()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/process/caseinst/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("[0].id").isEqualTo("caseInstanceId")

        verify(caseInstanceService).findByQuery(searchQuery)
    }


    @Test
    @WithMockUser("Papageno")
    fun testCaseInstanceCompleteAction() {
        val dto = DtoFactory.caseInstanceDto()

        doNothing().`when`(caseInstanceService).complete(dto, "Papageno")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/process/caseinst/action/complete")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(dto)
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(caseInstanceService).complete(dto, "Papageno")

    }

    @Test
    @WithMockUser("Papagena")
    fun testCaseInstanceCancelAction() {
        val dto = DtoFactory.caseInstanceDto()

        doNothing().`when`(caseInstanceService).cancel(dto, "Papagena")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/process/caseinst/action/cancel")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(dto)
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(caseInstanceService).cancel(dto, "Papagena")
    }

    @Test
    fun testCaseInstanceById() {
        val dto = DtoFactory.caseInstanceDto()

        `when`(caseInstanceService.getCaseInstanceDtoById(dto.id))
            .thenReturn(dto)

        webClient
            .get().uri("/process/caseinst/find/${dto.id}")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.id").isEqualTo("caseInstanceId")

        verify(caseInstanceService).getCaseInstanceDtoById(dto.id)

    }

    @Test
    fun testPlanItemCount() {
        val countQuery = CountQuery<PlanItemDto>()
        val result = (2..5).random().toLong()

        `when`(planItemService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/process/planitem/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(planItemService).countByQuery(countQuery)
    }

    @Test
    fun testPlanItemFind() {
        val searchQuery = SearchQuery<PlanItemDto>()

        `when`(planItemService.findByQuery(searchQuery))
            .thenReturn(listOf(DtoFactory.planItemDto()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/process/planitem/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(objectMapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("[0].id").isEqualTo("mockPlanItemId")

        verify(planItemService).findByQuery(searchQuery)
    }

    @Test
    fun testPlanItemStartAction() {
        val dto = DtoFactory.planItemDto()

        doNothing().`when`(planItemService).startPlanItem(dto)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/process/planitem/action/start")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(dto)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json("{}")

        verify(planItemService).startPlanItem(dto)
    }

    @Test
    @WithMockUser("Papageno")
    fun testPlanItemCompleteStageAction() {
        val dto = DtoFactory.planItemDto(type = PlanItemType.STAGE)

        doNothing().`when`(planItemService).forceCompleteStage(dto, "Papageno")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/process/planitem/action/complete")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(dto)
            .exchange()
            .expectStatus().isOk
            .expectBody().json("{}")

        verify(planItemService).forceCompleteStage(dto, "Papageno")
    }

    @Test
    fun testPlanItemUnknownAction() {
        val dto = DtoFactory.planItemDto()

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/process/planitem/action/unknown")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(dto)
            .exchange()
            .expectStatus().isOk
            .expectBody(String::class.java).isEqualTo("\"Error\"")

        verifyNoMoreInteractions(planItemService)
    }

    @Test
    fun testPlanItemTopLevelUserEvents() {
        val dto = DtoFactory.planItemDto()

        `when`(planItemService.getTopLevelUserEventListeners("someCmmnInstanceId"))
            .thenReturn(listOf(dto))

        webClient

            .get().uri("/process/planitem/userevents/someCmmnInstanceId")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("[0].id").isEqualTo("mockPlanItemId")

        verify(planItemService).getTopLevelUserEventListeners("someCmmnInstanceId")
    }

}