package com.qualidify.rest.project

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.RestQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.DtoFactory
import com.qualidify.service.DtoFactory.projectInstanceDto
import com.qualidify.service.process.CaseDefinitionService
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.exceptions.ProjectTemplateNotFoundException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import java.util.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient()
@ExtendWith(SpringExtension::class)
@WithMockUser
@DirtiesContext
internal class ProjectRestControllerTest() {

    @Autowired
    private lateinit var webClient: WebTestClient

    @MockBean
    lateinit var projectInstanceService: ProjectInstanceService

    @MockBean
    private lateinit var projectTemplateService: ProjectTemplateService

    @MockBean
    private lateinit var caseDefinitionService: CaseDefinitionService

    private val mapper = jacksonObjectMapper()

    @Test
    fun testProjectInstanceCount() {
        val countQuery = CountQuery<ProjectInstanceDto>()
        val result = (0..15).random().toLong()

        `when`(projectInstanceService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/project/instance/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(projectInstanceService).countByQuery(countQuery)
    }

    @Test
    fun testProjectInstanceFind() {
        val searchQuery = SearchQuery<ProjectInstanceDto>()

        val projectInstanceDto = projectInstanceDto()
        `when`(projectInstanceService.findByQuery(searchQuery))
            .thenReturn(listOf(projectInstanceDto))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/project/instance/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("[0].id").isEqualTo(projectInstanceDto.id)

        verify(projectInstanceService).findByQuery(searchQuery)
    }

    @Test
    fun testProjectInstanceById() {

        val projectInstanceDto = projectInstanceDto()
        `when`(projectInstanceService.getProjectInstanceById(projectInstanceDto.id))
            .thenReturn(projectInstanceDto)

        webClient
            .get().uri("/project/instance/find/${projectInstanceDto.id}")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.id").isEqualTo(projectInstanceDto.id)

        verify(projectInstanceService).getProjectInstanceById(projectInstanceDto.id)
    }

    @Test
    fun testProjectTemplateCount() {
        val countQuery = CountQuery<ProjectTemplateDto>()

        `when`(projectTemplateService.countByQuery(countQuery))
            .thenReturn(CountResponse(11))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/project/template/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(11)

        verify(projectTemplateService).countByQuery(countQuery)
    }

    @Test
    fun testProjectTemplateFind() {
        val searchQuery = SearchQuery<ProjectTemplateDto>()
        val projectTemplateDto = DtoFactory.projectTemplateDto()

        `when`(projectTemplateService.findByQuery(searchQuery))
            .thenReturn(listOf(projectTemplateDto))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/project/template/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(RestQuery()))
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                [
                  { 
                    "id":"${projectTemplateDto.id}",
                    "name":"",
                    "description":"",
                    "caseDefinition":
                      {
                        "id":"caseDefinitionId",
                        "name":"caseDefinitionName",
                        "key":"caseDefinitionKey",
                        "version":1,
                        "deploymentResourceId":null,
                        "graphicalNotation":null
                      },
                    "cmmnModel":"mockCmmnModel",
                    "variables":[],
                    "users":[],
                    "fields":{}
                  }
                ]
            """.trimIndent()
            )

        verify(projectTemplateService).findByQuery(searchQuery)
    }

    @Test
    fun testProjectTemplateStartAction() {
        val dto = DtoFactory.projectTemplateDto()

        `when`(projectTemplateService.startProject(dto))
            .thenReturn("No problem")

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/project/template/action/start")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(dto))
            .exchange()
            .expectStatus().isOk
            .expectBody(String::class.java).isEqualTo("No problem")

        verify(projectTemplateService).startProject(dto)
    }

    @Test
    fun testProjectTemplateBlowUpAction() {
        val dto = DtoFactory.projectTemplateDto()

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/project/template/action/blow_up")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(mapper.writeValueAsString(dto))
            .exchange()
            .expectStatus().isOk
            .expectBody(String::class.java).isEqualTo("Error")

        verifyNoInteractions(projectTemplateService)
    }

    @Test
    fun testColumnMetadataForProjectVariables() {
        val dto = DtoFactory.projectTemplateDto()
        val id = dto.id

        `when`(projectTemplateService.getProjectTemplateByIdentity(UUID.fromString(id)))
            .thenReturn(dto)
        `when`(projectTemplateService.getColumnDefinitionsForProjectVariables(dto))
            .thenReturn(listOf(DtoFactory.columnDef("First"), DtoFactory.columnDef("Second")))

        webClient
            .get().uri("/project/template/metadata/variables/${id}")
            .exchange()
            .expectStatus().isOk
            .expectBody().json(
                """
                [
                  {
                    "name":"First",
                    "description":"First",
                    "dataType":"STRING",
                    "attributes":{},
                    "hidden":false,
                    "viewable":true,
                    "editable":true,
                    "searchable":true,
                    "sortable":true,
                    "fieldAccess":"DEFAULT"
                  },
                  {
                    "name":"Second",
                    "description":"Second",
                    "dataType":"STRING",
                    "attributes":{},
                    "hidden":false,
                    "viewable":true,
                    "editable":true,
                    "searchable":true,
                    "sortable":true,
                    "fieldAccess":"DEFAULT"
                  }
                ]
            """.trimIndent()
            )

        verify(projectTemplateService).getProjectTemplateByIdentity(UUID.fromString(id))
        verify(projectTemplateService).getColumnDefinitionsForProjectVariables(dto)

        verifyNoInteractions(caseDefinitionService)
    }

    @Test
    fun testColumnMetadataForProjectInformation() {
        val dto = DtoFactory.projectTemplateDto()
        val id = dto.id

        `when`(projectTemplateService.getProjectTemplateByIdentity(UUID.fromString(id)))
            .thenReturn(dto)
        `when`(projectTemplateService.getColumnDefinitionsForProjectInfo(dto))
            .thenReturn(listOf(DtoFactory.columnDef("Alfa"), DtoFactory.columnDef("Beta")))

        webClient
            .get().uri("/project/template/metadata/info/${id}")
            .exchange()
            .expectStatus().isOk
            .expectBody().json(
                """
                [
                  {
                    "name":"Alfa",
                    "description":"Alfa",
                    "dataType":"STRING",
                    "attributes":{},
                    "hidden":false,
                    "viewable":true,
                    "editable":true,
                    "searchable":true,
                    "sortable":true,
                    "fieldAccess":"DEFAULT"
                  },
                  {
                    "name":"Beta",
                    "description":"Beta",
                    "dataType":"STRING",
                    "attributes":{},
                    "hidden":false,
                    "viewable":true,
                    "editable":true,
                    "searchable":true,
                    "sortable":true,
                    "fieldAccess":"DEFAULT"
                  }
                ]
            """.trimIndent()
            )

        verify(projectTemplateService).getProjectTemplateByIdentity(UUID.fromString(id))
        verify(projectTemplateService).getColumnDefinitionsForProjectInfo(dto)

        verifyNoInteractions(caseDefinitionService)
    }

    @Test
    fun testHandlingNotFoundException() {
        val randomId = UUID.randomUUID()

        `when`(projectTemplateService.getProjectTemplateByIdentity(randomId))
            .thenThrow(ProjectTemplateNotFoundException("Cannot find template with random id"))

        webClient
            .get().uri("/project/template/metadata/info/${randomId}")
            .exchange()
            .expectStatus().isNotFound
            .expectBody(String::class.java).isEqualTo("Cannot find template with random id")

        verify(projectTemplateService).getProjectTemplateByIdentity(randomId)
        verifyNoMoreInteractions(projectTemplateService)

        verifyNoInteractions(caseDefinitionService)

    }


}