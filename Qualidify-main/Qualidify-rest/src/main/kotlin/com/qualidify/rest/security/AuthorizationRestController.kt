package com.qualidify.rest.security

import com.qualidify.core.query.RestQuery
import com.qualidify.service.security.SecurityService
import com.qualidify.service.security.dto.SecurityGroupDto
import com.qualidify.service.user.UserService
import com.qualidify.service.user.dto.CreateUserCommand
import com.qualidify.service.user.dto.UserDto
import com.qualidify.service.user.exceptions.UserNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono


@RestController
@RequestMapping("/authorization")
open class AuthorizationRestController(
    //  val authorizationService: UserAuthorizationService,
    val securityService: SecurityService,
    val userService: UserService
) {

    @GetMapping("/isuserloggedin")
    fun isUserLoggedIn(): Mono<Boolean> {
        return ReactiveSecurityContextHolder.getContext()
            .map { it.authentication !is AnonymousAuthenticationToken }
    }

    @PostMapping("/securitygroup/count")
    fun groupCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            securityService.countByQuery(query = query.toCountQuery<SecurityGroupDto>())
        }

    @PostMapping("/securitygroup/find")
    fun groupFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            securityService.findByQuery(query = query.toSearchQuery<SecurityGroupDto>())
        }

    @PostMapping("/securitygroup/create")
    @ResponseStatus(HttpStatus.CREATED)
    fun groupCreate(@RequestBody group: SecurityGroupDto): Mono<Unit> {
        return securityService.createGroup(group).toMono()
    }

    @PutMapping("/securitygroup/update")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun groupUpdate(@RequestBody group: SecurityGroupDto) {
        return securityService.update(group)
    }

    @PostMapping("/user/count")
    fun userCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            userService.countByQuery(query = query.toCountQuery<UserDto>())
        }

    @PostMapping("/user/find")
    fun userFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            userService.findByQuery(query = query.toSearchQuery<UserDto>())
        }

    @PostMapping("/user/create")
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody command: CreateUserCommand) {
        userService.create(command)
    }

    @PutMapping("/user/update")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun update(@RequestBody user: UserDto) {
        userService.update(user)
    }

    @ExceptionHandler(value = [UserNotFoundException::class])
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleUserNotFoundException(exception: Exception) = exception.message

    @ExceptionHandler(value = [IllegalArgumentException::class])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleIllegalArgumentException(exception: Exception) = exception.message

}