package com.qualidify.rest.project

import com.qualidify.core.query.RestQuery
import com.qualidify.service.process.*
import com.qualidify.service.process.dto.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.security.Principal

@RestController
@RequestMapping("/process")
class ProcessRestController(
    private val processDefinitionService: BpmnDefinitionService,
    private val caseDefinitionService: CaseDefinitionService,
    private val caseInstanceService: CaseInstanceService,
    private val planItemService: PlanItemService,
) {

    @PostMapping("/casedef/count")
    fun caseDefinitionCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            caseDefinitionService.countByQuery(query = query.toCountQuery<CaseDefinitionDto>())
        }

    @PostMapping("/casedef/find")
    fun caseDefinitionFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            caseDefinitionService.findByQuery(query = query.toSearchQuery<CaseDefinitionDto>())
        }

    @GetMapping("/casedef/{key}")
    fun caseDefinitionByKey(@PathVariable key: String) =
        Mono.fromCallable {
            caseDefinitionService.getCaseDefinitionDtoByKey(key)
        }

    @PostMapping("/caseinst/count")
    fun caseInstanceCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            caseInstanceService.countByQuery(query = query.toCountQuery<CaseInstanceDto>())
        }

    @PostMapping("/caseinst/find")
    fun caseInstanceFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            caseInstanceService.findByQuery(query = query.toSearchQuery<CaseInstanceDto>())
        }

    @GetMapping("/caseinst/find/{id}")
    fun caseInstanceById(@PathVariable("id") id: String) =
        Mono.fromCallable {
            caseInstanceService.getCaseInstanceDtoById(id)
        }

    @PutMapping("/caseinst/action/{action}")
    fun caseInstanceAction(
        @PathVariable("action") action: String,
        @RequestBody dto: CaseInstanceDto,
        principal: Principal?,
    ): Mono<Any> {
        return Mono.fromCallable {
            val currentUser = currentUser(principal)
            when (action.uppercase()) {
                "COMPLETE" -> caseInstanceService.complete(dto, currentUser)
                "CANCEL" -> caseInstanceService.cancel(dto, currentUser)
                else -> "Error"
            }
        }
    }

    @PostMapping("/planitem/count")
    fun planItemCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            planItemService.countByQuery(query = query.toCountQuery<PlanItemDto>())
        }

    @PostMapping("/planitem/find")
    fun planItemFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            planItemService.findByQuery(query = query.toSearchQuery<PlanItemDto>())
        }

    @PutMapping("/planitem/action/{action}")
    fun planItemAction(
        @PathVariable("action") action: String,
        @RequestBody dto: PlanItemDto,
        principal: Principal?,
    ): Mono<Any> {
        return Mono.fromCallable {
            val currentUser = currentUser(principal)
            when (action.uppercase()) {
                "START" -> planItemService.startPlanItem(dto)
                "COMPLETE" -> planItemService.forceCompleteStage(dto, currentUser)
                else -> "Error"
            }
        }
    }

    @GetMapping("/planitem/{type}/{cmmnid}")
    fun getTopLevelUserEvents(
        @PathVariable("type") type: String,
        @PathVariable("cmmnid") cmmnId: String,
    ): Flux<PlanItemDto> {

        return Mono.fromCallable {
            if (type.equals("userevents", true)) {
                planItemService.getTopLevelUserEventListeners(cmmnId)
            } else {
                emptyList()
            }
        }.flatMapIterable { it }
    }

    @PostMapping("/processdef/count")
    fun processDefinitionCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            processDefinitionService.countByQuery(query = query.toCountQuery<BusinessProcessDefinitionDto>())
        }

    @PostMapping("/processdef/find")
    fun processDefinitionFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            processDefinitionService.findByQuery(query = query.toSearchQuery<BusinessProcessDefinitionDto>())
        }

    @ExceptionHandler(value = [NotImplementedError::class])
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    fun notImplemented(exception: Exception) = exception.message

    private fun currentUser(principal: Principal?): String {
        val currentUser = principal?.name ?: "anonymousUser"
        return currentUser
    }

}