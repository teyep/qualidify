package com.qualidify.rest

import com.qualidify.core.jpa.annotations.EnableSoftDeleteJpaRepositories
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.*
import org.springframework.core.convert.converter.Converter
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.client.registration.*
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator
import org.springframework.security.oauth2.jwt.*
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter
import org.springframework.security.oauth2.server.resource.web.server.BearerTokenServerAuthenticationEntryPoint
import org.springframework.security.web.server.DefaultServerRedirectStrategy
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationSuccessHandler
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import java.util.*
import java.util.stream.Collectors


@SpringBootApplication(
    exclude = [
        //org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration::class,
        org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration::class,
    ]
)
@ComponentScan("com.qualidify")
@EnableWebFlux
@EnableSoftDeleteJpaRepositories
open class QualidifyRestApplication

fun main(args: Array<String>) {
    runApplication<QualidifyRestApplication>(*args)
}

open class QualidifyContextFilter() : WebFilter {
    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        TODO("Not yet implemented")
    }
}

@Profile("localuser", "!nouser")
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@Configuration
open class LocalSecurityWebFluxConfiguration {

    @Bean
    open fun localSecurityWebFilterChain(
        authenticationManager: ReactiveAuthenticationManager,
        http: ServerHttpSecurity,
    ): SecurityWebFilterChain {
        return http.authorizeExchange()
            .pathMatchers("/api/session/**").permitAll()
            .anyExchange().authenticated()
            .and().httpBasic()
            .and().authenticationManager(authenticationManager)
            .csrf().disable()
            .build()
    }
}


@Profile("nouser", "!localuser", "!azure")  // removes user level security (not active by default)
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@Configuration
open class NoUserWebFluxConfiguration {

    @Bean
    open fun securityWebFilterChain(
        authenticationManager: ReactiveAuthenticationManager,
        http: ServerHttpSecurity,
    ): SecurityWebFilterChain {
        return http.authorizeExchange()
            .pathMatchers("/api/session/**").permitAll()
            .anyExchange().permitAll()
            .and().httpBasic()
            .and().authenticationManager(authenticationManager)
            .csrf().disable()
            .build()
    }
}


@Profile("azure")
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@Configuration
open class RemoteSecurityWebFluxConfiguration {

    @Bean
    open fun authenticationSuccessHandler(): ServerAuthenticationSuccessHandler {
        val handler = RedirectServerAuthenticationSuccessHandler()
        val strategy = DefaultServerRedirectStrategy()
        strategy.setContextRelative(false)
        handler.setRedirectStrategy(strategy)
        return handler
    }

    @Bean
    open fun jwtDecoder(
        @Value("\${spring.security.oauth2.resourceserver.jwt.issuer-uri}") issuerUri: String,
        @Value("\${spring.security.oauth2.resourceserver.jwt.client-id}") clientId: String,
    ): ReactiveJwtDecoder {
        val jwtDecoder = ReactiveJwtDecoders.fromIssuerLocation(issuerUri) as NimbusReactiveJwtDecoder
        val tokenValidator = DelegatingOAuth2TokenValidator<Jwt>(
            JwtValidators.createDefaultWithIssuer(issuerUri),
            JwtClaimValidator<List<String>>(
                "aud"
            ) {
                it != null && it.contains(clientId)
            })
        jwtDecoder.setJwtValidator(tokenValidator)
        return jwtDecoder
    }

    @Bean
    open fun grantedAuthoritiesExtractor(): Converter<Jwt, Mono<AbstractAuthenticationToken>> {
        val jwtAuthenticationConverter = JwtAuthenticationConverter()

        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter {
            val authorities: Collection<SimpleGrantedAuthority> = it.claims.getOrDefault(
                "roles",
                Collections.emptyList<SimpleGrantedAuthority>()
            ) as Collection<SimpleGrantedAuthority>

            authorities.stream()
                .map { it.toString() }
                .map { "ROLE_".plus(it) }
                .map { SimpleGrantedAuthority(it) }
                .collect(Collectors.toList()) as Collection<GrantedAuthority>?
        }
        return ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter)
    }

    @Bean
    open fun oauth2SecurityWebFilterChain(
        http: ServerHttpSecurity,
        jwtDecoder: ReactiveJwtDecoder,
        grantedAuthoritiesExtractor: Converter<Jwt, Mono<AbstractAuthenticationToken>>,
        successHandler: ServerAuthenticationSuccessHandler,
    ): SecurityWebFilterChain {
        http
            .authorizeExchange()
            .pathMatchers(
                "/session/**",
                "/login/**"
            ).permitAll()
            .anyExchange().authenticated()
            .and()
            .exceptionHandling()
            .authenticationEntryPoint(BearerTokenServerAuthenticationEntryPoint())
            .and()
            .oauth2Login()
            .authenticationSuccessHandler(successHandler)
            .and()
            .oauth2ResourceServer()
            .jwt()
            .jwtDecoder(jwtDecoder)
            .jwtAuthenticationConverter(grantedAuthoritiesExtractor)
        return http.build()
    }
}



