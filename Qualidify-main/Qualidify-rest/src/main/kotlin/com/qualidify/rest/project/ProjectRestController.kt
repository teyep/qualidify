package com.qualidify.rest.project

import com.qualidify.core.query.RestQuery
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.exceptions.ProjectTemplateNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*


@RestController
@RequestMapping("/project")
class ProjectRestController(
    private val projectTemplateService: ProjectTemplateService,
    private val projectInstanceService: ProjectInstanceService,
) {

    @PostMapping("/template/count")
    fun templateCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            projectTemplateService.countByQuery(query = query.toCountQuery<ProjectTemplateDto>())
        }

    @PostMapping("/template/find")
    fun templateFind(@RequestBody query: RestQuery): Mono<Collection<ProjectTemplateDto>> =
        Mono.fromCallable {
            projectTemplateService.findByQuery(query = query.toSearchQuery<ProjectTemplateDto>())
        }

    @PutMapping("/template/action/{action}")
    fun action(@PathVariable("action") action: String, @RequestBody dto: ProjectTemplateDto): Mono<String> {
        return Mono.fromCallable {
            if ("START".equals(action, ignoreCase = true)) {
                projectTemplateService.startProject(dto)
            } else {
                "Error"
            }
        }
    }

    @PostMapping("/instance/count")
    fun instanceCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            projectInstanceService.countByQuery(query = query.toCountQuery<ProjectInstanceDto>())
        }

    @PostMapping("/instance/find")
    fun instanceFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            projectInstanceService.findByQuery(query = query.toSearchQuery<ProjectInstanceDto>())
        }

    @GetMapping("/instance/find/{id}")
    fun instanceById(@PathVariable("id") id: String): Mono<ProjectInstanceDto> =
        Mono.fromCallable {
            projectInstanceService.getProjectInstanceById(id)
        }

    @GetMapping("/template/metadata/{part}/{id}")
    fun columnMetadataForProjectVariables(
        @PathVariable("part") part: String,
        @PathVariable("id") id: UUID
    ): Flux<ColumnDef> {
        return Mono.fromCallable {
            val dto = projectTemplateService.getProjectTemplateByIdentity(id)
            when (part) {
                "variables" -> projectTemplateService.getColumnDefinitionsForProjectVariables(dto)
                "info" -> projectTemplateService.getColumnDefinitionsForProjectInfo(dto)
                else -> emptyList()
            }
        }.flatMapIterable { it }
    }

    @ExceptionHandler(value = [ProjectTemplateNotFoundException::class])
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleNotFoundException(exception: Exception) = exception.message
}