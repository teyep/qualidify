package com.qualidify.rest.project

import com.qualidify.core.query.RestQuery
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.process.FormDefinitionService
import com.qualidify.service.process.HistoricTaskService
import com.qualidify.service.process.dto.FormDefinitionDto
import com.qualidify.service.process.exceptions.FormNotFoundException
import com.qualidify.service.process.exceptions.TaskServiceException
import com.qualidify.service.projects.FormMetadataService
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.ReviewResponse
import com.qualidify.service.quality.ReviewResponseRouter
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.security.Principal

@RestController
@RequestMapping("/tasks")
class TaskRestController(
    private val taskService: TaskService,
    private val reviewResponseRouter: ReviewResponseRouter,
    private val historicTaskService: HistoricTaskService,
    private val formDefinitionService: FormDefinitionService,
    private val formMetadataService: FormMetadataService,
) {

    @PostMapping("/count")
    fun taskCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            taskService.countByQuery(query = query.toCountQuery<TaskDto>())
        }

    @PostMapping("/find")
    fun taskFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            taskService.findByQuery(query = query.toSearchQuery<TaskDto>())
        }

    @GetMapping("/find/{id}")
    fun taskById(@PathVariable("id") id: String) =
        Mono.fromCallable {
            taskService.getTaskDtoById(id)
        }

    @PutMapping("/action/{action}")
    fun action(@PathVariable("action") action: String, @RequestBody dto: TaskDto, principal: Principal?): Mono<Any> {
        return Mono.fromCallable {
            val currentUser = currentUser(principal)
            when (action.uppercase()) {
                "COMPLETE" -> taskService.completeOrResolveTask(dto, currentUser)
                "CLAIM" -> taskService.claimTask(dto, currentUser)
                "UNCLAIM" -> taskService.unclaimTask(dto, currentUser)
                "APPROVE" -> reviewResponseRouter.routeRequest(dto, currentUser, ReviewResponse.APPROVE)
                "REJECT" -> reviewResponseRouter.routeRequest(dto, currentUser, ReviewResponse.REJECT)
                else -> "Error"
            }
        }
    }

    @PutMapping("/action/assign/{user}")
    fun assign(@PathVariable("user") user: String, @RequestBody dto: TaskDto, principal: Principal?): Mono<Any> {
        return Mono.fromCallable {
            val currentUser = currentUser(principal)
            taskService.assignTask(dto, user, currentUser)
        }
    }

    @PostMapping("/form/count")
    fun formCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            formDefinitionService.countByQuery(query = query.toCountQuery<FormDefinitionDto>())
        }

    @PostMapping("/form/find")
    fun formFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            formDefinitionService.findByQuery(query = query.toSearchQuery<FormDefinitionDto>())
        }

    @GetMapping("/form/{taskid}/metadata")
    fun columnMetadataByTaskId(@PathVariable("taskid") taskId: String): Flux<ColumnDef> {
        return Mono.fromCallable {
            formMetadataService.loadFormModelMetadataByTaskId(taskId)
        }.flatMapIterable { it }
    }

    @GetMapping("/{taskid}/form/variables")
    fun formVariables(@PathVariable("taskid") taskId: String): Mono<Map<String, Any?>> {
        return Mono.fromCallable {
            formMetadataService.getFormVariables(taskId)
        }
    }

    @PostMapping("/{taskid}/form/save")
    fun saveFormData(@PathVariable("taskid") taskId: String, @RequestBody data: Map<String, Any?>): Mono<Unit> {
        return Mono.fromCallable {
            formMetadataService.saveTaskFormData(taskId, data)
        }
    }

    @PostMapping("/{taskid}/form/complete")
    fun completeTaskFormData(
        @PathVariable("taskid") taskId: String,
        @RequestBody data: Map<String, Any?>,
    ): Mono<Unit> {
        return Mono.fromCallable {
            formMetadataService.completeTaskFormData(taskId, data)
        }
    }

    @PostMapping("/historic/count")
    fun historicTaskCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            historicTaskService.countByQuery(query = query.toCountQuery<TaskDto>())
        }

    @PostMapping("/historic/find")
    fun historicTaskFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            historicTaskService.findByQuery(query = query.toSearchQuery<TaskDto>())
        }

    @GetMapping("/historic/find/{id}")
    fun historicTaskById(@PathVariable("id") id: String): Mono<TaskDto> {
        return Mono.fromCallable {
            historicTaskService.getHistoricTaskDtoById(id)
        }
    }

    @GetMapping("/historic/{id}/variables")
    fun historicFormVariablesById(@PathVariable("id") id: String): Mono<Map<String, Any?>> {
        return Mono.fromCallable {
            historicTaskService.getHistoricFormVariables(id)
        }
    }

    @ExceptionHandler(value = [TaskServiceException::class])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun badRequest(exception: Exception) = exception.message

    @ExceptionHandler(value = [FormNotFoundException::class])
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun notFoundException(exception: Exception) = exception.message

    private fun currentUser(principal: Principal?): String {
        return principal?.name ?: "anonymousUser"
    }


}