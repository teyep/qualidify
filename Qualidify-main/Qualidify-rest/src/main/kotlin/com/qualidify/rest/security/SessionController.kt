package com.qualidify.rest.security

import com.qualidify.service.security.SessionService
import com.qualidify.service.security.dto.LoginAttemptDto
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

/**
 * Controller for session-related requests. This was separated from
 * AuthorizationRestController because we want to control access to these functions separately.
 *
 * @author Niels Visscher
 * @since 1.0
 */
@RestController
@RequestMapping("/session")
open class SessionController(
    val sessionService: SessionService
) {

    @GetMapping("/isuserloggedin")
    fun isUserLoggedIn(): Mono<Boolean> {
        return sessionService.isLoggedIn()
    }

    @PostMapping("/login")
    fun login(@RequestBody loginDto: LoginAttemptDto): Mono<Boolean> {
        return sessionService.login(loginDto.userName, loginDto.password)
    }

    @GetMapping("/logout")
    fun logout(): Mono<Boolean> {
        return sessionService.logout()
    }

    @ExceptionHandler(value = [BadCredentialsException::class])
    @ResponseStatus(HttpStatus.FORBIDDEN)
    fun handleIllegalArgumentException(exception: Exception) = exception.message
}