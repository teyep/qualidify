package com.qualidify.rest.config

import com.qualidify.core.query.RestQuery
import com.qualidify.service.configuration.ConfigurationDomainNotFoundException
import com.qualidify.service.configuration.ConfigurationService
import com.qualidify.service.configuration.dto.ConfigurationCodeDto
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/config")
class ConfigurationRestController(
    private val configurationService: ConfigurationService
) {

    @PostMapping("/domain/count")
    fun domainCount(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            configurationService.countByQuery(query = query.toCountQuery<ConfigurationCodeDto>())
        }

    @PostMapping("/domain/find")
    fun domainFind(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            configurationService.findByQuery(query = query.toSearchQuery<ConfigurationCodeDto>())
        }

    @PostMapping("/domain/create")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun create(@RequestBody dto: ConfigurationCodeDto) {
        configurationService.createCode(dto)
    }

    @PutMapping("/domain/update")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun update(@RequestBody dto: ConfigurationCodeDto) {
        configurationService.updateCode(dto)
    }

    @ExceptionHandler(value = [IllegalArgumentException::class])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun badRequest(exception: Exception) = exception.message

    @ExceptionHandler(value = [ConfigurationDomainNotFoundException::class])
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun notFound(exception: Exception) = exception.message

}