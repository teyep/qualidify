package com.qualidify.service.process

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.process.dto.BusinessProcessDefinitionDto

/**
 * A Service for BPMN Processes
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface BpmnDefinitionService : Queryable<BusinessProcessDefinitionDto> {
    fun startProcess(dto: BusinessProcessDefinitionDto)
}