package com.qualidify.service.document

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.document.dto.DocumentContentDto
import com.qualidify.service.document.dto.DocumentDto
import java.util.*

/**
 * Service for Documents
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface DocumentService : Queryable<DocumentDto> {

    /**
     * Create a document
     * @param path the path to create the document in
     * @param name the name of the file
     * @param type the type of the file
     * @param content the content of the file
     * @return a DocumentDto containing all information
     */
    fun createDocument(path: String, name: String, type: String, content: ByteArray): DocumentDto

    /**
     * Create a document
     * @param path the path to create the document in
     * @param name the name of the file
     * @param type the type of the file
     * @param content the content of the file
     * @return a DocumentDto containing all information
     */
    fun createDocument(
        path: String,
        name: String,
        type: String,
        attributes: Map<String, String?>,
        content: ByteArray,
    ): DocumentDto


    /**
     * Create a document
     * @param document the document dto containing all necessary information
     * @param content the document content
     * @return a DocumentDto containing all information
     */
    fun createDocument(document: DocumentDto, content: DocumentContentDto): DocumentDto

    /**
     * Update a document
     * @param path the path to create the document in
     * @param name the name of the file
     * @param type the type of the file
     * @param content the content of the file
     * @return a DocumentDto containing all information
     */
    fun updateDocument(path: String, name: String, type: String, content: ByteArray): DocumentDto

    /**
     * Update a document
     * @param document the document dto containing all necessary information
     * @param content the document content
     * @return a DocumentDto containing all information
     */
    fun updateDocument(document: DocumentDto, content: DocumentContentDto): DocumentDto

    /**
     * Get a document
     * @param path the path to get the document from
     * @param name the name of the file
     * @param type the type of the file
     * @return a DocumentDto containing all information
     */
    fun getDocument(path: String, name: String, type: String): DocumentDto

    /**
     * Get the document by its DocumentId
     * @return the DocumentDto with metadata.
     */
    fun getDocumentById(documentId: UUID): DocumentDto

    /**
     * Get the content of a document
     * @param documentId the document identifier
     * @return the documentContent
     */
    fun getDocumentContent(documentId: UUID): DocumentContentDto

    /**
     * Get the content of a document
     * @param documentDto the document information
     * @return the documentContent
     */
    fun getDocumentContent(documentDto: DocumentDto): DocumentContentDto

    /**
     * Get the content of a document
     * @param path the path to get the document from
     * @param name the name of the file
     * @param type the type of the file
     * @return the documentContent
     */
    fun getDocumentContent(path: String, name: String, type: String): DocumentContentDto

    /**
     * Get all documents in path
     * @param path the Path to search in
     * @return an array of DocumentDto's
     */
    fun getAllDocumentsInPath(path: String): Array<DocumentDto>

    /*
     * Retrieves all documents with a given file type
     *
     * @param type The file extension
     */
    fun getAllDocumentsByType(type: String): Array<DocumentDto>

}