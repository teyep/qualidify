package com.qualidify.service.report.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.Hidden
import java.io.Serializable
import java.util.*


@DtoMetadata(singular = "Batch job artifact", plural = "Batch job artifacts", enableAddition = false)
data class BatchJobArtifactDto(
    @Hidden
    val batchJobId: UUID,
    val version: Int,
    val mimeType: String,
    val content: ByteArray,
    val finishedDate: Date?

) : Serializable, HasIdentity {
    override fun identity(): Array<Any> = arrayOf(batchJobId)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BatchJobArtifactDto

        if (batchJobId != other.batchJobId) return false
        if (version != other.version) return false
        if (mimeType != other.mimeType) return false
        if (!content.contentEquals(other.content)) return false
        if (finishedDate != other.finishedDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = batchJobId.hashCode()
        result = 31 * result + mimeType.hashCode()
        result = 31 * result + content.contentHashCode()
        result = 31 * result + (finishedDate?.hashCode() ?: 0)
        return result
    }

}
