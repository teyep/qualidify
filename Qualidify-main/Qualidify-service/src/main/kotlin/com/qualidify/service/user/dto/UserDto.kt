package com.qualidify.service.user.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.types.*
import com.qualidify.metadata.dto.CustomizableDtoHelper
import java.util.*

@Mnemonic("Users", singular = "User", plural = "Users")
@Segments(
    [
        SegmentDefinition("DEFAULT", "Default"),
        SegmentDefinition("SECURITY")
    ]
)
data class UserDto(

    @Hidden
    val userId: UUID? = null,

    @TextType(clearButtonVisible = true, minLength = 4)
    @DtoColumn(description = "User name")
    var username: String = "",

    @EmailType
    @DtoColumn(description = "Email")
    var email: String = "",

    @CustomType(Reference.USER)
    @SearchQueryHint("fields[*]")
    override val fields: MutableMap<String, String?> = mutableMapOf(),

    @DtoColumn(description = "Security Groups", viewable = false, segments = [Segment("SECURITY")])
    @CheckBoxGroupType(itemProvider = "securityGroupProvider")
    val securityGroups: Set<String> = mutableSetOf()

) : CustomizableDtoHelper(), HasIdentity {

    override fun identity(): Array<Any> {
        return arrayOf(username);
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserDto

        if (userId != other.userId) return false

        return true
    }

    override fun hashCode(): Int {
        return userId.hashCode()
    }
}