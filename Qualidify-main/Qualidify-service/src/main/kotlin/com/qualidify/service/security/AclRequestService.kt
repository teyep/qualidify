package com.qualidify.service.security

import com.qualidify.service.security.dto.Permission
import java.util.*

interface AclRequestService {
    fun create(request: AclRequest)
    fun update(request: AclRequest)
    fun delete(request: AclRequest)
}

data class AclRequest(
    val clazz: Class<*>,
    val id: UUID,
    val permissions: List<Permission>,
    val roles: Set<String>,
    val users: Set<String>,
    val isOwner: Boolean,
)