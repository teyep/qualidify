package com.qualidify.service.report

import com.qualidify.core.query.service.Queryable
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.service.report.dto.BatchJobArtifactDto
import com.qualidify.service.report.dto.BatchJobDto
import com.qualidify.service.report.dto.ReportTemplateDto
import java.util.*

/**
 *
 * Interface to a batch job service as used by the sending side of the application. The Qualidify-reports
 * module uses its own interface as it has a different implementation of BatchJobRepository.
 */
interface SenderSideBatchJobService : Queryable<BatchJobDto> {

    fun getById(id: UUID): BatchJobDto?

    fun createBatchJob(dto: BatchJobDto)

    fun scheduleFromReportTemplateDto(dto: ReportTemplateDto, parameters: ObjectMap)

    fun getArtifact(id: UUID): BatchJobArtifactDto?

    /**
     * Returns true when a job with a given job ID is linked to an artifact, false otherwise
     */
    fun hasArtifact(jobId: UUID): Boolean
}
