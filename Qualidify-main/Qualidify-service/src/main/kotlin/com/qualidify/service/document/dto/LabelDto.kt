package com.qualidify.service.document.dto

import com.qualidify.core.identity.HasIdentity

data class LabelDto(
    val name: String,
): HasIdentity {
    override fun identity(): Array<Any> = arrayOf(name)
}