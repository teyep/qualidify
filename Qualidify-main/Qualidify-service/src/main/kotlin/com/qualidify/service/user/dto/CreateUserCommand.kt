package com.qualidify.service.user.dto

import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.DtoCommand
import com.qualidify.metadata.annotations.SegmentDefinition
import com.qualidify.metadata.annotations.Segments
import com.qualidify.metadata.annotations.types.CheckBoxGroupType
import com.qualidify.metadata.annotations.types.CustomType
import com.qualidify.metadata.annotations.types.EmailType
import com.qualidify.metadata.annotations.types.PasswordType
import com.qualidify.metadata.dto.CustomizableDtoHelper

@Segments(
    [
        SegmentDefinition("DEFAULT", "Default"),
        SegmentDefinition("SECURITY")
    ]
)
@DtoCommand("createUser")
class CreateUserCommand(
    val userName: String,
    @EmailType
    val email: String,
    @PasswordType
    val password: String,

    @CustomType(Reference.USER)
    override val fields: MutableMap<String, String?> = mutableMapOf(),

    @CheckBoxGroupType(itemProvider = "securityGroupProvider")
    val securityGroups: Set<String> = mutableSetOf()

) : CustomizableDtoHelper() {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CreateUserCommand) return false

        if (userName != other.userName) return false
        if (email != other.email) return false
        if (password != other.password) return false
        if (fields != other.fields) return false
        if (securityGroups != other.securityGroups) return false

        return true
    }

    override fun hashCode(): Int {
        var result = userName.hashCode()
        result = 31 * result + email.hashCode()
        result = 31 * result + password.hashCode()
        result = 31 * result + fields.hashCode()
        result = 31 * result + securityGroups.hashCode()
        return result
    }
}
