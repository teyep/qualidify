package com.qualidify.service.projects.dto

import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.DtoColumn
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.Hidden
import com.qualidify.metadata.annotations.types.CustomType
import com.qualidify.metadata.annotations.types.GridType
import com.qualidify.metadata.dto.CustomizableDtoHelper
import com.qualidify.service.dto.HasIdAndName
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.dto.CaseInstanceState
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*

/**
 * A Dto for Project Instances. The [ProjectTemplate] wraps the corresponding Flowable [CaseDefinition] and the
 * [ProjectInstance] wraps the [CaseInstance]. The wrapper is created to handle the flowable variables.
 *
 * @author Menno Tol
 * @author Marcel Pot
 * @since 1.0
 */
@DtoMetadata(singular = "Project Instance", plural = "Project Instances", enableUpdate = false, enableAddition = false)
data class ProjectInstanceDto(

    override val id: String,

    override val name: String,

    val description: String,

    @DtoColumn(editable = false)
    val qualityShellId: String,

    @Hidden
    @DtoColumn(searchable = false, editable = false, sortable = false)
    val qualityShellInstanceDto: CaseInstanceDto,

    @DtoColumn(editable = false)
    val qualityShellInstanceState: CaseInstanceState,

    @DtoColumn(editable = false)
    val regularCaseId: String,

    @Hidden
    @DtoColumn(searchable = false, editable = false, sortable = false)
    val regularCaseInstanceDto: CaseInstanceDto,

    @DtoColumn(editable = false)
    val regularCaseInstanceState: CaseInstanceState,

    val startTime: LocalDateTime,

    @CustomType(Reference.PROJECT_INSTANCE)
    @SearchQueryHint("customFields[*]")
    override val fields: MutableMap<String, String?>,

    @DtoColumn(viewable = false)
    @GridType(mnemonic = "ProjectInstanceVariable")
    val variables: List<ProjectInstanceVariableDto> = emptyList(),

    @DtoColumn(viewable = false)
    val users: List<String>,

    val state: String,

    ) : CustomizableDtoHelper(), Serializable, HasIdAndName {

    data class ProjectInstanceVariableDto(
        val name: String,
        val datatype: DataType,
        val value: String? = null,
    ) : Serializable

    override fun identity(): Array<Any> = arrayOf(id)

}