package com.qualidify.service.projects

import com.qualidify.metadata.service.dto.ColumnDef


/**
 * A wrapping interface to hide the logic of the form implementation
 * This interface is meant to be used for retrieving form data, saving form data
 * and collecting filled-in form data from the past
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
interface FormMetadataService {

    /**
     * Load the model data in columndefinitions
     * @param formDefinitionKey the key of the form
     * @return a list of columnMetadata
     */
    fun loadFormModelMetadataByFormKey(formDefinitionKey: String): List<ColumnDef>

    /**
     * Load the model data in columndefinitions
     * @param taskId the id of the task
     * @return a list of columnMetadata
     */
    fun loadFormModelMetadataByTaskId(taskId: String): List<ColumnDef>

    /**
     * Save the filled in values for a form
     * Completes the task
     * @param taskId the taskid connected to the form
     * @param data the filled in values
     */
    fun completeTaskFormData(taskId: String, data: Map<String, Any?>)

    /**
     * Save the filled in values for a form
     * Marks the task as resolved at the owner (i.e. the assignee has resolved the task)
     * @param taskId the taskid connected to the form
     * @param data the filled in values
     */
    fun saveTaskFormData(taskId: String, data: Map<String, Any?>)

    /**
     * Get the filled in (and saved) variables for a form
     * @param taskId the taskid connected to the form
     * @return a key value map with as keys the formfield names and as value the filled in value
     */
    fun getFormVariables(taskId: String): Map<String, Any?>

    /**
     * Get the empty variables for a form
     * @param taskId the taskid connected to the form
     * @return a key value map with as keys the formfield names and as value null
     */
    fun emptyFormVariables(taskId: String): Map<String, Any?>

}