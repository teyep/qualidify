package com.qualidify.service.security.roles

object Roles {

    object Home {
        const val VIEW = "home.view"
    }

    object Project {
        const val VIEW = "project.view"

        object Action {
            const val NEW = "project.action.new"
        }
    }

    object Task {
        const val VIEW = "task.view"
    }

    object Report {
        const val VIEW = "report.view"
    }

    object Configuration {
        const val VIEW = "config.view"

        object Project {
            const val TEMPLATE = "config.project.projecttemplate.view"
            const val INSTANCE = "config.project.projectinstance.view"
            const val CASE_DEFINITION = "config.project.casedefinition.view"
            const val CASE_INSTANCE = "config.project.caseinstance.view"
            const val PLAN_ITEM = "config.project.planitem.view"
            const val CMMN_TASK = "config.project.cmmntask.view"
            const val FORM_DEFINITION = "config.project.formdefinition.view"
            const val PROCESS_DEFINITION = "config.project.processdefinition.view"
            const val BPMN_TASK = "config.project.bpmntask.view"
            const val HISTORIC_TASK = "config.project.historictask.view"
        }

        object Security {
            const val USER = "config.security.user.view"
            const val GROUP = "config.security.group.view"
        }

        object Data {
            const val CUSTOM_FIELDS = "config.data.customfields.view"
            const val CONFIGURATION = "config.data.configuration.view"
            const val METADATA = "config.data.metadata.view"

        }
    }
}