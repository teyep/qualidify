package com.qualidify.service.process

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.process.dto.PlanItemDto
import org.flowable.variable.api.history.HistoricVariableInstance

/**
 * This interface is meant to be used for retrieving plan item info
 * related to the [PlanItemDto] from flowable
 *
 * @author Menno Tol
 * @since 1.0
 */
interface PlanItemService : Queryable<PlanItemDto> {

    /**
     * Obtains the [PlanItemDto] from the database
     * @param planItemId the id of the [PlanItem]
     * @return the [PlanItemDto]
     */
    fun getPlanItemDtoOrNull(planItemId: String): PlanItemDto?

    /**
     * Obtains the plan items of type 'generic event listeners' by case instance id
     * @param caseInstanceId the id of the corresponding case instance
     * @return a list of [PlanItemDto] containing the generic event listeners
     */
    fun getEventListenersByCaseInstanceId(caseInstanceId: String): List<PlanItemDto>

    /**
     * Obtain the plan items with definition type 'human task' with the same stage id (i.e. siblings plan item under the
     * parent stage)
     * @param planItemDto the [PlanItemDto]
     * @return a list of [PlanItemDto] that share the same stage id
     */
    fun getPlanItemsByStageAndDefinition(planItemDto: PlanItemDto): List<PlanItemDto>

    /**
     * Obtains the plan items with type 'user event listener' belonging to a stage
     * @param stageId the id of the stage to search for
     * @return a list of [PlanItemDto] of type user event listeners that have the stage id
     */
    fun getUserEventListenersWithStage(stageId: String): List<PlanItemDto>

    /**
     * Obtains the plan item with type 'user event listener' belonging to a case instance
     * @param cmmnInstanceId the id of the case instance
     * @return a list of [PlanItemDto] of type user event listeners that have the cmmn instance id
     */
    fun getTopLevelUserEventListeners(cmmnInstanceId: String): List<PlanItemDto>

    /**
     * Starts a plan item of type human task or completes a plan item of 'user event listener'
     * @param dto the [PlanItemDto] to start/complete
     *
     * @throws NotImplementedError if the action is not implemented for the plan item type
     */
    fun startPlanItem(dto: PlanItemDto)

    /**
     * Get the stages of the case instance that the plan item belong to
     * @param caseInstanceId the id of the case instance
     * @return the stages as a collection of [PlanItemDto]s
     */
    fun getStages(caseInstanceId: String?): Collection<PlanItemDto>

    /**
     * Gets the count of the child plan items belonging to a stage
     * @param dto the dto of the parent stage
     * @return the number of child plan items
     */
    fun getPlanItemWithStageCount(dto: PlanItemDto): Long

    /**
     * Checks if a stage has actually child plan items
     * @param dto the dto of the parent stage
     * @return a boolean
     */
    fun hasPlanItemsWithStage(dto: PlanItemDto): Boolean

    /**
     * Gets all child plan items belonging to a stage
     * @param dto the dto of the parent stage
     * @return the plan items as a collection
     */
    fun getPlanItemsWithStage(dto: PlanItemDto): Collection<PlanItemDto>

    /**
     * Completes a plan item of type 'generic event listener'
     * @param dto the [PlanItemDto] to complete
     *
     * @throws PlanItemServiceException if the dto is not of type 'generic event listener'
     */
    fun completeGenericEventListener(dto: PlanItemDto)

    /**
     * Forces a stage to complete
     * NB: this can lead to one or more plan items be in state 'terminated' (i.e. incorrect completion)
     * @param dto the [PlanItemDto] of the stage to be completed
     */
    fun forceCompleteStage(dto: PlanItemDto, currentUser: String)

    /**
     * Obtains a list of planItemIds that belong to a certain case.
     * @param caseInstanceId the id of the case instance
     * @return a list of caseInstanceId's
     */
    fun getPlanItemIdsByCase(caseInstanceId: String): List<String>

    /**
     * Obtains the historic plan item from the historic tables of the database.
     * @param planItemId the id of the plan item
     * @return the [PlanItemDto] of the historic plan item
     *
     * @throws [PlanItemServiceException] when no plan item can be found
     */
    fun getHistoricPlanItemDto(planItemId: String): PlanItemDto

    /**
     * Obtains all local variables of the plan item from the database.
     * @param planItemId the id of the plan item
     * @return a map with key-value pairs containing all local variables
     */
    fun getPlanItemVariables(planItemId: String): Map<String, Any>

    /**
     * Obtain a local variable of the plan item
     * @param planItemId the id of the plan item
     * @param variableName the name of the variable to look for
     * @return any non-null String stored as local variable, or null if empty or not of type String
     */
    fun getPlanItemVariable(planItemId: String, variableName: String): String?

    /**
     * Store a value in the local variable of a plan item
     * NB: any existing value will be overwritten!
     * @param planItemId the id of the plan item
     * @param variableName the name of the variable
     * @param variableValue the value to be stored in the variable
     */
    fun setPlanItemVariable(planItemId: String, variableName: String, variableValue: String)

    /**
     * Obtain a local variable of the plan item from the historic database tables
     * @param planItemId the id of the plan item
     * @param variableName the name of the variable to look for
     * @return any non-null String stored as local variable, or null if empty or not of type String
     */
    fun getHistoricPlanItemVariable(planItemId: String, variableName: String): HistoricVariableInstance

    /**
     * Update a variable in the historic database tables
     * @param historicVariable the historic variable instance
     * @param variableValue the name of the variable to look for
     */
    fun updateHistoricVariable(historicVariable: HistoricVariableInstance, variableValue: String)
}