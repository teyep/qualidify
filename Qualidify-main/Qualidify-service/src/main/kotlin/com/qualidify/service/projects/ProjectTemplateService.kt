package com.qualidify.service.projects

import com.qualidify.core.query.service.Queryable
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.exceptions.ProjectTemplateNotFoundException
import java.util.*

/**
 * Interface for ProjectTemplateService defining all possible methods
 *
 * @author Marcel Pot
 * @since 1.0
 */

interface ProjectTemplateService : Queryable<ProjectTemplateDto> {
    /**
     * Start a project
     * @param dto the template to build a project on
     * @return a projectId
     */
    fun startProject(dto: ProjectTemplateDto): String?

    /**
     * Generate the columnDefinitions for ProjectInformation
     * @param dto the ProjectTemplate
     * @return a list with ColumnDefinitions
     */
    fun getColumnDefinitionsForProjectInfo(dto: ProjectTemplateDto): List<ColumnDef>

    /**
     * Generate the columnDefinitions for ProjectVariables
     * @param dto the ProjectTemplate
     * @return a list with ColumnDefinitions
     */
    fun getColumnDefinitionsForProjectVariables(dto: ProjectTemplateDto): List<ColumnDef>

    /**
     * Get a ProjectTemplate by its identity
     * @param[id] the identity
     * @return a ProjectTemplateId
     * @throws a [ProjectTemplateNotFoundException] when not able to find the correct template
     */
    @Throws(ProjectTemplateNotFoundException::class)
    fun getProjectTemplateByIdentity(id: UUID): ProjectTemplateDto
}