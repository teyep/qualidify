package com.qualidify.service.security

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.security.dto.SecurityGroupDto
import java.util.*

/**
 * A service for handling security actions
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface SecurityService : Queryable<SecurityGroupDto> {
    /**
     * Find a Security group by its name
     * @param groupName the name of the group
     * @return Optional of SecurityGroupDto
     */
    fun findSecurityGroupByName(groupName: String): Optional<SecurityGroupDto>

    /**
     * Find SecurityGroup by their names
     * @param groupNames a collection of groupNames
     * @return a collection of SecurityGroupDto's
     */
    fun findSecurityGroupsByNames(groupNames: Collection<String>): Collection<SecurityGroupDto>

    /**
     * Create a security group
     * @param group the securitygroup to create
     */
    fun createGroup(group: SecurityGroupDto)

    /**
     * Update a security group
     * @param group the group to update
     */
    fun update(group: SecurityGroupDto)


    /**
     * Get all available roles in the system
     * @return a collection of available roles
     */
    fun availableRoles(): Collection<String>


}
