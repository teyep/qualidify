package com.qualidify.service.quality

/**
 * Enum class indicating which keys can be used to link review task plan items with regular plan items. The keys are
 * used to store the id of the linked item (task, stage, case) in the local variable of the plan item of that item
 * (task, stage, case).
 */
enum class QualityLinkKey(val key: String) {
    UNDEFINED("undefined"),
    REVIEW_TASK_PLAN_ITEM_ID("reviewTaskPlanItemId"),
    REGULAR_TASK_PLAN_ITEM_ID("regularTaskPlanItemId"),
    REGULAR_STAGE_PLAN_ITEM_ID("regularStagePlanItemId"),
    REGULAR_CASE_ID("regularCaseInstanceId"),
    ;

    override fun toString(): String = key

    companion object {

        fun getByName(name: String?) =
            if (name == null) UNDEFINED
            else
                try {
                    QualityLinkKey.values().single { it.key == name }
                } catch (exception: Exception) {
                    throw QualityLinkKeyException("Quality key '$name' is not implemented in QualityLinkKeys enum")
                }

        fun getKeySet() = QualityLinkKey.values().map { QualityLinkKey.getByName(it.key).toString() }.toSet()

    }
}

class QualityLinkKeyException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)
