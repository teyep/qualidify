package com.qualidify.service.process.exceptions

class ConstraintKeyNotFoundException(message: String? = null, throwable: Throwable? = null) :
    RuntimeException(message, throwable)