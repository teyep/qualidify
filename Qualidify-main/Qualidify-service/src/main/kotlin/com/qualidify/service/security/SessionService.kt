package com.qualidify.service.security

import reactor.core.publisher.Mono

interface SessionService {
    /**
     * Attempts a login for the requested combination of username and password
     */
    fun login(userName: String, password: String): Mono<Boolean>

    /**
     * Removes the session for the currently logged-in user
     */
    fun logout(): Mono<Boolean>

    /**
     * Returns true if a user is currently logged-in, false otherwise
     */
    fun isLoggedIn(): Mono<Boolean>
}