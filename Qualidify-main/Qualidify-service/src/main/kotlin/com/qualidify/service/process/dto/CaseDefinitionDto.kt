package com.qualidify.service.process.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.renderer.PopupImageRenderer
import java.io.Serializable

/**
 * A Dto for CMMN case definitions
 *
 * @author Menno Tol
 * @author Marcel Pot
 * @since 1.0
 */
@Action("Diagram", "Diagram")
@DtoMetadata(singular = "Case Definition", plural = "Case Definitions", enableUpdate = false)
@CreationStrategy(CreationStrategyPattern.FACTORY, creationClass = CaseDefinitionUploadDto::class)
data class CaseDefinitionDto(
    @DtoColumn(hidden = true)
    val id: String,

    val name: String,

    val key: String,

    @DtoColumn(searchable = false)
    val version: Int?,

    @Hidden
    @DtoColumn(searchable = false, sortable = false)
    val deploymentResourceId: String? = null,

    @PopupImageRenderer
    @DtoColumn(searchable = false, sortable = false)
    val graphicalNotation: ByteArray? = null,

) : Serializable, HasIdentity {
    override fun identity(): Array<Any> = arrayOf(id)
}

