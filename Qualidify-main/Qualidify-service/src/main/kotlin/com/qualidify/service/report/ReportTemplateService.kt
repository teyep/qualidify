package com.qualidify.service.report

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.report.dto.ReportTemplateDto
import java.util.*

interface ReportTemplateService : Queryable<ReportTemplateDto> {
    fun getTemplateById(id: UUID): ReportTemplateDto

    fun getAllTemplates(): Collection<ReportTemplateDto>

    fun create(dto: ReportTemplateDto)

    fun deleteById(id: UUID)
}
