package com.qualidify.service.projects

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.projects.dto.ProjectInstanceDto

/**
 * Interface for ProjectInstanceService
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface ProjectInstanceService : Queryable<ProjectInstanceDto> {

    /**
     * Get a Project Instance by its id
     * @param id the identity
     * @return a ProjectInstanceDto containing the project information
     */
    fun getProjectInstanceById(id: String): ProjectInstanceDto?

    /**
     * Get a Project Instance by the corresponding cmmnInstanceId
     * @param cmmnInstanceId the cmmnInstanceId
     * @return a ProjectInstanceDto containing the project information
     */
    fun getProjectInstanceByCmmnId(cmmnInstanceId: String): ProjectInstanceDto?

    /**
     * Checks if a project instance is available
     * @param id the identity
     * @return a boolean indicating if the project instance is available
     */
    fun isProjectAvailable(id: String): Boolean

    /**
     * Get a Project Instance by the corresponding cmmnInstanceId or the qualityShellId
     * @param id the cmmnInstanceId or qualityShellId
     * @return the [ProjectInstanceDto]
     */
    fun getProjectInstanceDtoByCmmnIdOrQualityShellId(id: String): ProjectInstanceDto
}