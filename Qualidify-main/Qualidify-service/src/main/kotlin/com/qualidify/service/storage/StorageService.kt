package com.qualidify.service.storage

import org.springframework.core.io.Resource
import org.springframework.web.multipart.MultipartFile
import java.io.InputStream

/**
 * Interface for different Storage of files implemenentations
 *
 * @author Marcel Pot
 * @since 1.0
 *
 */
interface StorageService {

    /**
     * Store a file
     * @param file the file to store
     */
    fun store(file: MultipartFile): String

    /**
     * Load a file as a resource
     * @param identifier the filename or hash
     * @return the inputStream
     */
    fun load(identifier: String): InputStream

    /**
     * Load a file as a resource
     * @param filename the filename or hash
     * @return the Resource
     */
    fun loadAsResource(identifier: String): Resource
}