package com.qualidify.service.process.dto

import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.types.EnumerationType
import com.qualidify.service.quality.HasQuality
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import java.io.Serializable
import java.time.LocalDateTime

typealias PlanItemId = String

/**
 * A Dto for CMMN plan item information
 *
 * @author Menno Tol
 * @since 1.0
 */
@Actions([
    Action("Start", "Start"),
    Action("Complete", "Complete")
])
@DtoMetadata(singular = "Plan Item", plural = "Plan Items", enableUpdate = false, enableAddition = false)
data class PlanItemDto(

    override val id: PlanItemId,

    override val name: String,

    @SearchQueryHint("caseInstanceId")
    val caseInstanceId: String,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val projectInstanceId: String,

    val stage: String?,

    val reference: String?,

    @EnumerationType(itemProvider = "planItemTypeEnumItemProvider")
    @SearchQueryHint("definitionType")
    val type: PlanItemType,

    @EnumerationType(itemProvider = "planItemStateEnumItemProvider")
    val state: PlanItemState,

    @Hidden
    val parent: PlanItemId? = stage,  // for use with query of type HierarchicalQuery

    val completable: Boolean?,

    val timeCreated: LocalDateTime,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    val historic: Boolean,

    @Hidden
    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val isQualityShell: Boolean,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val regularCaseId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityShellId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityLinkKey: QualityLinkKey,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityLinkId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val reviewState: ReviewState,

    ) : Serializable, HasQuality {
    override fun identity(): Array<Any> = arrayOf(id)
}


/**
 * Refer to flowable interface PlanItemDefinitionType
 */
enum class PlanItemType(val type: String) {
    STAGE("stage"),
    PLAN_FRAGMENT("planfragment"),
    MILESTONE("milestone"),
    TIMER_EVENT_LISTENER("timereventlistener"),
    USER_EVENT_LISTENER("usereventlistener"),
    SIGNAL_EVENT_LISTENER("signaleventlistener"),
    VARIABLE_EVENT_LISTENER("variableeventlistener"),
    GENERIC_EVENT_LISTENER("genericeventlistener"),
    HUMAN_TASK("humantask"),
    CASE_TASK("casetask"),
    CASE_PAGE_TASK("casepagetask"),
    EXTERNAL_WORKER_TASK("externalworkertask"),
    PROCESS_TASK("processtask"),
    DECISION_TASK("decisiontask"),
    SERVICE_TASK("servicetask"),
    HTTP_SERVICE_TASK("httpservicetask");

    override fun toString() = type

    companion object {
        fun getByName(name: String) = try {
            values().single { it.type == name }
        } catch (exception: Exception) {
            throw PlanItemDtoException("Flowable type '$name' is not implemented in PlanItemType enum")
        }
    }
}


/**
 * Refer to flowable interface PlanItemInstanceState
 */
enum class PlanItemState(val state: String) {
    ACTIVE("active"),
    AVAILABLE("available"),
    UNAVAILABLE("unavailable"),  // state used by flowable for conditional activation
    ENABLED("enabled"),
    DISABLED("disabled"),
    COMPLETED("completed"),
    FAILED("failed"),
    SUSPENDED("suspended"),
    TERMINATED("terminated"),
    WAITING_FOR_REPETITION("wait_repetition"),;

    override fun toString() = state

    companion object {
        fun getByName(name: String) = try {
            values().single { it.state == name }
        } catch (exception: Exception) {
            throw PlanItemDtoException("Flowable state '$name' is not implemented in PlanItemState enum")
        }
    }
}

class PlanItemDtoException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)