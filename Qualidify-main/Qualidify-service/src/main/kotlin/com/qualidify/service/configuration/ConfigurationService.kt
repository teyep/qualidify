package com.qualidify.service.configuration

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.configuration.dto.ConfigurationCodeDto
import com.qualidify.service.configuration.dto.ConfigurationDomainDto

/**
 * A Service for Configurations (Configuration domains and configurationcodes)
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface ConfigurationService : Queryable<ConfigurationCodeDto> {
    /**
     * Creates a configuration domain
     * @param domain the configurationDomain
     */
    fun create(domain: ConfigurationDomainDto)

    /**
     * Updates a configuration domain
     * @param domain the configurationDomain
     */
    fun update(domain: ConfigurationDomainDto)

    /**
     * Deletes a configurationDomain
     * @param domain the configurationDomain
     */
    fun delete(domain: ConfigurationDomainDto)

    /**
     * Create a configurationcode
     * @param code the ConfigurationCode
     */
    fun createCode(code: ConfigurationCodeDto)

    /**
     * Update a configurationcode
     * @param code the ConfigurationCode
     */
    fun updateCode(code: ConfigurationCodeDto)
}