package com.qualidify.service.quality

/**
 * The review state of the underlying plan item instance.
 *
 * @property UNDEFINED State has not been evaluated or local variable is null
 * @property IN_REVIEW Review is ongoing
 * @property REVIEW_NOT_REQUIRED Review is not needed for this item
 * @property REVIEW_REQUIRED Review is required for this item, but not started (the item is not completed/terminated)
 * @property COMPLETED Review concluded successfully
 * @property REJECTED Review did not conclude successfully
 */
enum class ReviewState(val state: String) {
    UNDEFINED("undefined"),
    IN_REVIEW("in_review"),
    REVIEW_NOT_REQUIRED("not_required"),
    REVIEW_REQUIRED("required"),
    APPROVED("approved"),
    REJECTED("rejected"),
    CORRECTED("corrected"),
    ;

    override fun toString(): String = state

    companion object {
        fun getByName(name: String?): ReviewState =
            if (name == null) UNDEFINED
            else
                try {
                    values().single { it.state == name }
                } catch (e: Exception) {
                    throw ReviewStateException("Review state key '$name' is not implemented in ReviewState enum")
                }
    }


}

class ReviewStateException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)