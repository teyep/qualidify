package com.qualidify.service.quality

import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.projects.dto.TaskDto
import org.flowable.cmmn.api.runtime.PlanItemInstance


/**
 * An interface for the [TaskReviewService]
 *
 * @author Menno Tol
 * @since 1.0
 */
interface TaskReviewService : ReviewService<PlanItemDto> {

    /**
     * Starting point for incoming plan item instances.
     *
     * @param planItemInstance the delegate plan item instance obtained from the [TaskCompletedListener]
     *
     * @throws ReviewServiceException when the plan item cannot be found.
     */
    fun startReview(planItemInstance: PlanItemInstance)

    /**
     * Update the task review state (disable edit to the regular task and enables edits to the review task).
     * @param regularTaskDto the [TaskDto] of the regular task
     * @param principal the user correcting the regular task
     */
    fun taskCorrected(regularTaskDto: TaskDto, principal: String)

}
