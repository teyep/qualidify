package com.qualidify.service.report

import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.service.report.dto.BatchJobDto
import com.qualidify.service.report.dto.ReportTemplateDto

/**
 * Interface for a service that represents a Report Template as a more generic batch job object.
 *
 * @author Niels Visscher
 * @since 1.0
 */
interface ReportTemplateBatchJobConversionService {
    fun convert(reportTemplate: ReportTemplateDto, parameters: ObjectMap): BatchJobDto
}
