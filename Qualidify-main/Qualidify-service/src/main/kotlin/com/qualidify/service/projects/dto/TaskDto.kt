package com.qualidify.service.projects.dto

import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.types.EnumerationType
import com.qualidify.service.quality.HasQuality
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import java.io.Serializable

/**
 * A Dto for CMMN task information
 *
 * @author Menno Tol
 * @since 1.0
 */
@Actions(
    [
        Action("Claim", "Claim"),
        Action("Unclaim", "Unclaim"),
        Action("Assign", "Assign"),
        Action("Complete", "Complete"),
        Action("Approve", "Approve"),
        Action("Reject", "Reject"),
    ]
)
@DtoMetadata(singular = "Task", plural = "Tasks", enableUpdate = false, enableAddition = false)
data class TaskDto(
    override val id: String,

    override val name: String,

    val taskDefinitionKey: String,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val projectInstanceId: String,

    @SearchQueryHint("caseInstanceId")
    val caseInstanceId: String?,

    val caseDefinitionId: String?,

    @SearchQueryHint("planItemInstanceId")
    val planItemInstanceId: String,

    val formKey: String?,

    @EnumerationType(itemProvider = "userProvider")
    val owner: String?,  // owner vs assignee -> owner is project owner, assignee executes tasks

    @EnumerationType(itemProvider = "userProvider")
    val assignee: String?,

    @Hidden
    val type: TaskType,

    val stage: String?,

    val historic: Boolean?,

    @Hidden
    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val isQualityShell: Boolean,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val regularCaseId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityShellId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityLinkKey: QualityLinkKey,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityLinkId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val reviewState: ReviewState,

    ) : Serializable, HasQuality {
    override fun identity(): Array<Any> = arrayOf(id)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TaskDto

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

}

/**
 * Identify from which kind of process this task belongs
 */
enum class TaskType(val type: String) {
    CMMN("cmmn"),
    BPMN("bpmn");

    override fun toString() = type

    companion object {
        fun getByName(name: String) = try {
            values().first { it.type == name }
        } catch (exception: Exception) {
            throw TaskDtoException("Flowable type '$name' is not implemented in TaskType enum")
        }
    }
}

class TaskDtoException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)
