package com.qualidify.service.process.exceptions

class ProjectInstanceNotFoundException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause) {
}