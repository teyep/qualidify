package com.qualidify.service.security.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.renderer.ActiveRenderer
import com.qualidify.metadata.annotations.types.BooleanType
import com.qualidify.metadata.annotations.types.CheckBoxGroupType
import com.qualidify.metadata.annotations.types.TextType
import java.io.Serializable

@DtoMetadata(singular = "Security Group", plural = "Security Groups")
@CreationStrategy(CreationStrategyPattern.OBJECT_CONSTRUCTOR, creationClass = SecurityGroupCreateCommand::class)
data class SecurityGroupDto(

    @DtoColumn(searchable = false, sortable = false)
    @SearchQueryHint("id")
    @TextType(isRequired = true)
    val name: String,

    @TextType(isRequired = true)
    val description: String,

    @DtoColumn(searchable = false, sortable = false)
    @ActiveRenderer
    @BooleanType
    val enabled: Boolean = true,

    @DtoColumn(viewable = false)
    @CheckBoxGroupType(itemProvider = "securityRoleProvider")
    val roles: MutableSet<String> = mutableSetOf(),

) : Serializable, HasIdentity {

    constructor(command: SecurityGroupCreateCommand) : this(name = command.name)

    constructor(name: String) : this(name = name, description = "")

    constructor(name: String, description: String, roles: Set<String>) : this(
        name,
        description,
        true,
        roles.toMutableSet()
    )

    override fun identity(): Array<Any> = arrayOf(name)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SecurityGroupDto

        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        return description.hashCode()
    }
}

@DtoCommand("SecurityGroupCreationCommand")
data class SecurityGroupCreateCommand(
    val name: String
)