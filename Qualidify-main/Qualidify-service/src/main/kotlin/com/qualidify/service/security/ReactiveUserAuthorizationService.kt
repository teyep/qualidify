package com.qualidify.service.security

import org.springframework.security.core.userdetails.ReactiveUserDetailsService


/**
 * Reactive interface for retrieving authorizationsrequests
 * @author Marcel Pot
 * @since 1.0 - QUAL-501
 */
interface ReactiveUserAuthorizationService : ReactiveUserDetailsService {
}