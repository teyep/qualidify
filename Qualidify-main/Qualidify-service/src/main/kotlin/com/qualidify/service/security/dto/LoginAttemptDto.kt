package com.qualidify.service.security.dto

import com.qualidify.metadata.annotations.DtoCommand

@DtoCommand("LoginAttempCommand")
data class LoginAttemptDto(
    val userName: String,
    val password: String
)
