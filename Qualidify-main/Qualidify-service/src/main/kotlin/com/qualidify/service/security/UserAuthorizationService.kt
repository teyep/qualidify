package com.qualidify.service.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import java.util.*

/**
 * Service for authorization questions
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface UserAuthorizationService : UserDetailsService {

    /**
     * Get the current name of the user logged in (if any)
     * @return the username (otherwise null)
     */
    val currentUserName: String?

    /**
     * Load a user by its name
     * @param username the name to look for
     * @return Userdetails
     */
    override fun loadUserByUsername(username: String): UserDetails

    /**
     * Load a user by its Id
     * @param id the userid
     * @return Userdetails
     */
    fun loadUserById(id: UUID): UserDetails

    /**
     * Load a collection of users by their ids
     * @param ids a collection of userids
     * @return a List with userdetails
     */
    fun loadUsersByIds(ids: Collection<UUID>): List<UserDetails>

    /**
     * Load a security group by its Id (name)
     * @param id the groupname
     * @return a GrantedAuthority
     */
    fun loadGroupByName(id: String): GrantedAuthority?

    /**
     * Load a collection of security groups by their Ids
     * @param ids a collection of id's (names)
     * @return a collection of GrantedAuthorities
     */
    fun loadGroupsByNames(ids: Collection<String>): Collection<GrantedAuthority>

    /**
     * Load a collection of security groups linked to a user
     * @param[userName] the name of the user
     * @return a collection of GrantedAuthorities
     */
    fun loadGroupsByUserName(userName: String): Collection<GrantedAuthority>

    /**
     * Load a collection of security groups linked to a user
     * @param[userNames] a collection of usernames
     * @return a collection of GrantedAuthorities
     */
    fun loadGroupsByUserNames(userNames: Collection<String>): Collection<GrantedAuthority>

    /**
     * Checks if a user is logged in
     * @return a boolean indicating if a user is logged in
     */
    fun isUserLoggedIn(): Boolean
}