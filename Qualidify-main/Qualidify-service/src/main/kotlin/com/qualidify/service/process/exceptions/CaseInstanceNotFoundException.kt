package com.qualidify.service.process.exceptions

/**
 * Exception to indicate a Caseinstance cannot be found in the repository
 *
 * @author Marcel Pot
 * @since 1.0
 */
class CaseInstanceNotFoundException(message: String? = null, throwable: Throwable? = null) :
    RuntimeException(message, throwable)