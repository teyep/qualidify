package com.qualidify.service.report.dto


import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.annotations.DtoColumn
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.Hidden
import java.io.Serializable
import java.util.*
import javax.persistence.Embeddable

/**
 * Data transfer object for ReportServiceTemplate entities
 *
 * @author Niels Visscher
 * @since 1.0
 */
@DtoMetadata(singular = "Report Service Template", plural = "Report Service Templates", enableAddition = false)
data class ReportTemplateDto(
    @Hidden
    val id: UUID,

    @DtoColumn
    @SearchQueryHint("templateName")
    val name: String,

    @DtoColumn
    @SearchQueryHint("templateDescription")
    val description: String,

    @Hidden
    val content: ByteArray,

    @DtoColumn
    @SearchQueryHint("templateParameters")
    val parameters: String,

    @DtoColumn(searchable = false)
    val resources: Array<ReportTemplateResourceDto>

): Serializable, HasIdentity {
    override fun identity(): Array<Any> = arrayOf(id)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ReportTemplateDto

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (!content.contentEquals(other.content)) return false
        if (parameters != other.parameters) return false
        if (!resources.contentEquals(other.resources)) return false
        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + content.contentHashCode()
        result = 31 * result + parameters.hashCode()
        result = 31 * result + resources.contentHashCode()
        return result
    }
}

/**
 * Data transfer object for ReportTemplateResource
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Embeddable
data class ReportTemplateResourceDto(

    /**
     * File name of the content. The resource type (subreport/non-subreport) is deduced from the extension
     */
    val resourceName: String,

    /**
     * Binary content of the resource
     */
    val resourceData: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ReportTemplateResourceDto

        if (resourceName != other.resourceName) return false
        if (!resourceData.contentEquals(other.resourceData)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = resourceName.hashCode()
        result = 31 * result + resourceData.contentHashCode()
        return result
    }
}
