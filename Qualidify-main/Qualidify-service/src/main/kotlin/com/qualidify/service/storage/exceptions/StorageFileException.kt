package com.qualidify.service.storage.exceptions

/**
 * An exception indicating a problem in the [StorageService]
 *
 * @author Marcel Pot
 */
class StorageFileException(message: String?, cause: Throwable?) : RuntimeException(message, cause) {

    constructor(message: String?) : this(message, null)
    constructor(cause: Throwable?) : this("", cause)

}