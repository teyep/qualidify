package com.qualidify.service.projects.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.Action
import com.qualidify.metadata.annotations.DtoColumn
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.Hidden
import com.qualidify.metadata.annotations.types.CustomType
import com.qualidify.metadata.annotations.types.GridType
import com.qualidify.metadata.annotations.types.SelectType
import com.qualidify.metadata.dto.CustomizableDtoHelper
import com.qualidify.service.process.dto.CaseDefinitionDto
import java.io.Serializable

@Action("Start", "Start")
@DtoMetadata(singular = "Project Template", plural = "Project Templates")
data class ProjectTemplateDto(
    val id: String,

    val name: String,

    val description: String,

    @Hidden
    val caseDefinition: CaseDefinitionDto,

    @SelectType(itemProvider = "cmmnModelProvider")
    val cmmnModel: String = caseDefinition.key,

    @DtoColumn(viewable = false)
    @GridType(mnemonic = "ProjectTemplateVariable")
    val variables: List<ProjectTemplateVariableDto> = emptyList(),

    @Hidden
    val users: List<String> = emptyList(),

    @CustomType(Reference.PROJECT_TEMPLATE)
    @SearchQueryHint("customFields[*]")
    override val fields: MutableMap<String, String?>

) : CustomizableDtoHelper(),
    Serializable, HasIdentity {

    @DtoMetadata(singular = "Project Template Variable", plural = "Project Template Variables")
    data class ProjectTemplateVariableDto(
        val name: String,
        val description: String,
        val datatype: DataType,
        val value: String? = null,
        val required: Boolean = true,
        val userInput: Boolean = true,
    ) : Serializable

    override fun identity(): Array<Any> = arrayOf(id)

}