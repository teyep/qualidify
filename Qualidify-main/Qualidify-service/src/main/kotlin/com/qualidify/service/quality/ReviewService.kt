package com.qualidify.service.quality

import com.qualidify.service.projects.dto.TaskDto
import java.io.Serializable

/**
 * An interface for defining the requirements for any ReviewService implementation.
 *
 * @author Menno Tol
 * @since 1.0
 */
interface ReviewService<D> where D : Serializable, D : HasQuality {

    /**
     * Starts the review cascade for a regular item dto.
     * @param dto the dto if the item for which a review needs to be started.
     *
     * @throws ReviewServiceException when dto is part of a quality shell (i.e. is not a regular item)
     */
    fun startReview(dto: D)

    /**
     * Performs basic evaluation of the incoming item, such as if it is part of a quality shell instance,
     * and if its [ReviewState] is not already 'approved'.
     * @param dto the dto on which the evaluation is performed
     * @return [true] if it is a regular item, [false] otherwise
     */
    fun evaluate(dto: D): Boolean

    /**
     * Updates the [ReviewState] variable of the regular item and the review task.
     * @param reviewTaskPlanItemDto the dto of the review task plan item
     * @param reviewState the new [ReviewState]
     */
//    fun updateReviewState(reviewTaskPlanItemDto: PlanItemDto, reviewState: ReviewState)

    /**
     * Updates the [ReviewState] variable of the regular item and the review task.
     * @param reviewTaskDto the dto of the review task
     * @param reviewState the new [ReviewState]
     */
//    fun updateReviewState(reviewTaskDto: TaskDto, reviewState: ReviewState)

    /**
     * Updates the [ReviewState] local variable of the review task (runtime) and regular item (historic).
     * @param reviewTaskDto the dto of the review task
     * @param reviewState the new [ReviewState]
     */
//    fun updateReviewStateInHistory(reviewTaskDto: TaskDto, reviewState: ReviewState)

    /**
     * Performs the required actions upon approval of the review task:
     * @param reviewTaskDto the [TaskDto] of the review task
     * @param principal the user rejecting the review task
     */
    fun approveReview(reviewTaskDto: TaskDto, principal: String)

    /**
     * Performs required actions upon rejecting the review task:
     * @param reviewTaskDto the [TaskDto] of the review task
     * @param principal the user rejecting the review task
     */
    fun rejectReview(reviewTaskDto: TaskDto, principal: String)
}
