package com.qualidify.service.process.exceptions

class WrongImplementationException(message: String? = null, throwable: Throwable? = null) :
    RuntimeException(message, throwable)