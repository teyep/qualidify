package com.qualidify.service.user.exceptions

class UserNotFoundException(message: String? = null, throwable: Throwable? = null) :
    RuntimeException(message, throwable)