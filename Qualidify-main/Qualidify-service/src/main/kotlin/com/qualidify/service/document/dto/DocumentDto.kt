package com.qualidify.service.document.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.annotations.Hidden
import java.io.Serializable
import java.util.*

data class DocumentDto(

    @Hidden
    val documentId: UUID,
    val path: String,
    val name: String,
    val type: String,
    val size: Long,
    val version: Long,
    @Hidden
    val attributes: Map<String, String?> = emptyMap(),
    @Hidden
    val labels: Array<LabelDto> = emptyArray(),

) : Serializable, HasIdentity {

    override fun identity(): Array<Any> = arrayOf(documentId)

}

