package com.qualidify.service

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.service.configuration.dto.ConfigurationCodeDto
import com.qualidify.service.process.dto.*
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.projects.dto.TaskType
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import java.time.LocalDateTime
import java.util.*

/**
 * Central location for test dto's, sorted alphabetically.
 * Part of main folder so that it can be shared with other modules.
 *
 * The default arguments match with the corresponding test entities, see [com.qualidify.model.entities.TestEntities]).
 * Please use Kotlin's named argument function with defaults for introducing variables.
 */
object DtoFactory {
    fun businessProcessDefinitionDto() = BusinessProcessDefinitionDto(
        id = "mockProcessDefId",
        key = "mockProcessDefKey",
        name = "mockProcessDefName",
        version = 1,
        graphicalNotation = true
    )

    fun businessProcessTaskDto() = BusinessProcessTaskDto(
        id = "mockDto",
        name = "mockName",
        owner = "",
        assignee = "",
        type = "bpmn"
    )

    fun caseDefinitionDto(
        caseDefinitionKey: String = "caseDefinitionKey",
        deploymentResourceId: String? = null,  // "deploymentId"
        graphicalNotation: ByteArray? = null,  // "TestBytes".toByteArray()
    ) = CaseDefinitionDto(
        id = "caseDefinitionId",
        name = "caseDefinitionName",
        key = caseDefinitionKey,
        version = 1,
        deploymentResourceId = deploymentResourceId,
        graphicalNotation = graphicalNotation
    )

    fun caseInstanceDto(
        id: String = "caseInstanceId",
        projectInstanceId: String = "projectInstanceId",
        completable: Boolean = false,
        isQualityShell: Boolean = false,
        caseInstanceId: String? = null,
        qualityShellId: String? = "qualityShellId",
        qualityLinkKey: QualityLinkKey = QualityLinkKey.UNDEFINED,
        qualityLinkId: String? = null,
        reviewState: ReviewState = ReviewState.UNDEFINED,
    ) = CaseInstanceDto(
        id = id,
        name = "caseDefinitionName",
        caseDefinition = caseDefinitionDto(
            caseDefinitionKey = if (isQualityShell) "cmmnQualityShell" else "caseDefinitionKey"
        ),
        caseDefinitionKey = if (isQualityShell) "cmmnQualityShell" else "caseDefinitionKey",
        projectInstanceId = projectInstanceId,
        state = CaseInstanceState.NEW,
        completable = completable,
        isQualityShell = isQualityShell,
        regularCaseId = caseInstanceId,
        qualityShellId = if (isQualityShell) null else qualityShellId,
        qualityLinkKey = qualityLinkKey,
        qualityLinkId = qualityLinkId,
        reviewState = reviewState,
    )

    fun columnDef(name: String) = ColumnDef(name = name, dataType = DataType.STRING)

    fun configurationCodeDto(domain: String, name: String) = ConfigurationCodeDto(
        domain,
        name,
        "A testing configurationcode",
        DataType.BOOLEAN,
        "TRUE",
        true
    )

    fun customFieldDef() =
        CustomFieldDef(
            id = UUID.randomUUID(),
            reference = Reference.USER,
            field = "EMAIL",
            description = "Email of the user",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )

    fun formDefinitionDto() = FormDefinitionDto(
        id = "formDefinitionId",
        name = "formDefinitionName",
        key = "formDefinitionKey",
        version = 1,
        form = emptyMap()
    )

    fun historicTaskDto() = TaskDto(
        id = "mockHistoricTaskId",
        name = "historicTaskName",
        taskDefinitionKey = "taskDefinitionKey",
        projectInstanceId = "projectInstanceId",
        caseInstanceId = "caseInstanceId",
        caseDefinitionId = "caseDefinitionKey",
        planItemInstanceId = "planItemInstanceId",
        formKey = "formKey",
        owner = "owner",
        assignee = "assignee",
        type = TaskType.CMMN,
        stage = "stageId",
        historic = true,
        isQualityShell = false,
        regularCaseId = null,
        qualityShellId = "qualityShellId",
        qualityLinkKey = QualityLinkKey.UNDEFINED,
        qualityLinkId = null,
        reviewState = ReviewState.UNDEFINED,
    )

    fun planItemDto(
        id: String = "mockPlanItemId",
        name: String = "mockPlanItemName",
        type: PlanItemType = PlanItemType.HUMAN_TASK,
        state: PlanItemState = PlanItemState.ACTIVE,
        isQualityShell: Boolean = false,
        qualityLinkKey: QualityLinkKey = QualityLinkKey.UNDEFINED,
        qualityLinkId: String? = null,
        reviewState: ReviewState = ReviewState.UNDEFINED,
    ) = PlanItemDto(
        id = id,
        name = name,
        caseInstanceId = "caseInstanceId",
        projectInstanceId = "projectInstanceId",
        stage = "mockStage",
        reference = "mockReference",
        type = type,
        state = state,
        completable = true,
        timeCreated = LocalDateTime.of(2023, 2, 9, 9, 39, 39),
        historic = false,
        isQualityShell = isQualityShell,
        regularCaseId = null,
        qualityShellId = "qualityShellId",
        qualityLinkKey = qualityLinkKey,
        qualityLinkId = qualityLinkId,
        reviewState = reviewState,
    )

    fun projectInstanceDto(id: String = "projectInstanceId") = ProjectInstanceDto(
        id = id,
        name = "projectDtoName",
        description = "ProjectDtoDescription",
        qualityShellId = "cmmnQualityShellId",
        qualityShellInstanceDto = caseInstanceDto(isQualityShell = true),
        qualityShellInstanceState = CaseInstanceState.ACTIVE,
        regularCaseId = "cmmnId",
        regularCaseInstanceState = CaseInstanceState.ACTIVE,
        regularCaseInstanceDto = caseInstanceDto(isQualityShell = false),
        startTime = LocalDateTime.of(2022, 9, 22, 11, 46, 0),
        fields = mutableMapOf<String, String?>(),
        variables = listOf(
            ProjectInstanceDto.ProjectInstanceVariableDto(
                name = "projectInstanceVariable",
                datatype = DataType.BINARY,
                value = "value"
            ),
            ProjectInstanceDto.ProjectInstanceVariableDto(
                name = "projectInstanceVariable",
                datatype = DataType.BINARY,
                value = "value"
            ),
            ProjectInstanceDto.ProjectInstanceVariableDto(
                name = "projectInstanceVariable",
                datatype = DataType.BINARY,
                value = "value"
            )
        ),
        users = listOf(
            "user",
            "admin",
            "viewer"
        ),
        state = "active",
    )

    fun projectTemplateDto(
        id: String = UUID.randomUUID().toString(),
        name: String = "",
        cmmnModel: String = "mockCmmnModel",
    ) = ProjectTemplateDto(
        id = id,
        name = name,
        description = "",
        caseDefinition = caseDefinitionDto(),
        cmmnModel = cmmnModel,
        variables = emptyList(),
        fields = HashMap()
    )

    fun taskDto(
        id: String = "mockTaskDto",
        planItemInstanceId: String = "planItemInstanceId",
        assignee: String? = "taskAssignee",
        formKey: String? = "formKey",
        isQualityShell: Boolean = false,
        qualityLinkKey: QualityLinkKey = QualityLinkKey.UNDEFINED,
        qualityLinkId: String? = null,
        reviewState: ReviewState = ReviewState.UNDEFINED,
    ) = TaskDto(
        id,
        name = "mockTaskName",
        taskDefinitionKey = "taskDefinitionKey",
        projectInstanceId = "projectInstanceId",
        caseInstanceId = "cmmnInstanceId",
        caseDefinitionId = "caseDefinitionId",
        planItemInstanceId = planItemInstanceId,
        formKey = formKey,
        owner = "mockOwner",
        assignee = assignee,
        type = TaskType.CMMN,
        stage = "stageInstanceId",
        historic = false,
        isQualityShell = isQualityShell,
        regularCaseId = null,
        qualityShellId = "qualityShellId",
        qualityLinkKey = qualityLinkKey,
        qualityLinkId = qualityLinkId,
        reviewState = reviewState,
    )

}