package com.qualidify.service.dto

import com.qualidify.core.identity.HasIdentity

/**
 * Interface ensuring a dto has certain properties (e.g. id and name)
 *
 * @author Menno Tol
 * @since 1.0
 */
interface HasIdAndName : HasIdentity {
    val id: String
    val name: String
}