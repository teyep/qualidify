package com.qualidify.service.process

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.projects.dto.TaskDto

/**
 * This interface is meant to be used for retrieving historic task info
 * related to the [HistoricTaskDto] from flowable
 *
 * @author Menno Tol
 * @since 1.0
 */
interface HistoricTaskService : Queryable<TaskDto> {

    /**
     * Retrieves the historic task dto
     * @param historicTaskId the id of the historic task
     * @return the historicTaskDto
     */
    fun getHistoricTaskDtoById(historicTaskId: String): TaskDto

    /**
     * Check if the historic task exists in the database
     * @param historicTaskId the id of the historic task
     * @return true if the taskId could be found
     */
    fun isHistoricTaskAvailable(historicTaskId: String): Boolean

    /**
     * Retrieves form variables from a historic task
     * @param historicTaskId the id of the historic task
     * @return a map with the corresponding form variables
     */
    fun getHistoricFormVariables(historicTaskId: String): Map<String, Any?>

}