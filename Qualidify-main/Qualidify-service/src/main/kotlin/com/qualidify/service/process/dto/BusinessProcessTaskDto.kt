package com.qualidify.service.process.dto

import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.Hidden
import com.qualidify.metadata.annotations.types.EnumerationType
import java.io.Serializable

/**
 * A Dto for Task information
 *
 * @author Marcel Pot
 * @since 1.0
 */
@DtoMetadata(singular = "Task", plural = "Tasks", enableUpdate = false, enableAddition = false)
data class BusinessProcessTaskDto(
        @Hidden
        val id: String,
        val name: String?,
        @EnumerationType(itemProvider = "userProvider")
        val owner: String?,
        @EnumerationType(itemProvider = "userProvider")
        val assignee: String?,
//    @Hidden
        val type: String
) : Serializable


