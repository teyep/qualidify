package com.qualidify.service.user.dto

import com.qualidify.metadata.annotations.DtoCommand
import java.util.*

@DtoCommand("updatePassword")
class ChangePasswordCommand(
    val userId: UUID,
    val password: String
)
