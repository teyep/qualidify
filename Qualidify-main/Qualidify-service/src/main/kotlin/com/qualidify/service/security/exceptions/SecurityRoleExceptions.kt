package com.qualidify.service.security.exceptions

class SecurityRoleNotFound : RuntimeException();