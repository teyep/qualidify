package com.qualidify.service.document.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.annotations.Hidden
import java.util.*

data class DocumentContentDto(
    @Hidden
    val documentId: UUID,
    val content: ByteArray,
) : HasIdentity {
    override fun identity(): Array<Any> = arrayOf(documentId)
}