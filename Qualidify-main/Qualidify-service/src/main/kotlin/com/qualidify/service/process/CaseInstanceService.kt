package com.qualidify.service.process

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.process.dto.CaseInstanceDto
import org.flowable.variable.api.history.HistoricVariableInstance

/**
 * A Service for CMMN Case Instances
 *
 * @author Menno Tol
 * @since 1.0
 */
interface CaseInstanceService : Queryable<CaseInstanceDto> {

    /**
     * Obtains the [CaseInstanceDto] from the database
     * @param caseInstanceId the id of the [CaseInstance]
     * @return the [CaseInstanceDto]
     */
    fun getCaseInstanceDtoById(caseInstanceId: String): CaseInstanceDto

    /**
     * Gets the variables visible from the case instance scope (i.e. non-local variables).
     */
    fun getCaseVariables(caseInstanceId: String): Map<String, Any?>

    /**
     * Gets the variables visible from the case instance scope (i.e. non-local variables) from the historic
     * database tables. When a case has been completed (including autocompletion), the variables can only be found in
     * the historic database tables.
     */
    fun getHistoricCaseVariables(caseInstanceId: String): Map<String, Any?>

    /**
     * Completes the case instance
     * @param dto the [CaseInstanceDto]
     * @param currentUser the name of the current user
     */
    fun complete(dto: CaseInstanceDto, currentUser: String)

    /**
     * Terminates the case instance
     * @param dto the [CaseInstanceDto]
     * @param currentUser the name of the current user
     */
    fun cancel(dto: CaseInstanceDto, currentUser: String)

    /**
     * Obtain a global variable of the case instance
     * @param caseInstanceId the id of the case instance
     * @param variableName the name of the variable to look for
     * @return any non-null String stored as global variable, or null if empty or not of type String
     */
    fun getCaseVariable(caseInstanceId: String, variableName: String): String?

    /**
     * Store a value in the (global) variable of a case instance
     * NB: any existing value will be overwritten!
     * @param planItemId the id of the case instance
     * @param variableName the name of the variable
     * @param variableValue the value to be stored in the variable
     */
    fun setCaseVariable(caseInstanceId: String, variableName: String, variableValue: String): Unit

    /**
     * Obtains the historic case instance from the database.
     * @param caseInstanceId the id of the case instance
     * @return the [CaseInstanceDto] of the historic plan item
     *
     * @throws [CaseInstanceServiceException] when no plan item can be found
     */
    fun getHistoricCaseInstanceDtoById(caseInstanceId: String): CaseInstanceDto

    /**
     * Obtain a variable of the case instance from the historic database tables.
     * NB: do not include local variables for tasks / plan items
     * @param caseInstanceId the id of the case instance
     * @param variableName the name of the variable to look for
     * @return any non-null String stored as local variable, or null if empty or not of type String
     */
    fun getHistoricCaseVariable(caseInstanceId: String, variableName: String): HistoricVariableInstance

    /**
     * Update a variable in the historic database tables
     * @param historicVariable the historic variable instance
     * @param variableValue the name of the variable to look for
     */
    fun updateHistoricVariable(historicVariable: HistoricVariableInstance, variableValue: String)
}