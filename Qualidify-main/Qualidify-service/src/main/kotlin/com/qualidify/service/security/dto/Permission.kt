package com.qualidify.service.security.dto

enum class Permission {
    READ, WRITE, CREATE, DELETE, ADMIN
}