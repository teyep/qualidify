package com.qualidify.service.process

import com.qualidify.core.query.service.Queryable
import com.qualidify.metadata.service.ObjectFactory
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.qualidify.service.process.dto.CaseDefinitionUploadDto
import com.qualidify.service.process.dto.CaseInstanceDto

/**
 * A Service for CMMN Case Definitions
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
interface CaseDefinitionService : Queryable<CaseDefinitionDto>,
    ObjectFactory<CaseDefinitionDto, CaseDefinitionUploadDto> {

    fun startProject(dto: CaseDefinitionDto): CaseInstanceDto

    fun upload(upload: CaseDefinitionUploadDto)

    fun getXml(caseDefinitionKey: String): String

    fun getCaseDefinitionDtoByKey(key: String): CaseDefinitionDto

}