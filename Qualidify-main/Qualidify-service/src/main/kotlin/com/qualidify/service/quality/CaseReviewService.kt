package com.qualidify.service.quality

import com.qualidify.service.process.dto.CaseInstanceDto
import org.flowable.cmmn.api.runtime.CaseInstance


/**
 * An interface for the [CaseReviewService]
 *
 * @author Menno Tol
 * @since 1.0
 */
interface CaseReviewService: ReviewService<CaseInstanceDto> {

    /**
     * Starting point for incoming case items.
     *
     * @param caseInstance the delegate case instance obtained from the [CaseCompletedListener]
     *
     * @throws ReviewServiceException when the case instance cannot be found.
     */
    fun startReview(caseInstance: CaseInstance)

}