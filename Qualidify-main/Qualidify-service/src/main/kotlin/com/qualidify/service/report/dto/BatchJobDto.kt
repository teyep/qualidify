package com.qualidify.service.report.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.Hidden
import com.qualidify.model.entities.BatchJobType
import java.io.Serializable
import java.util.*

/**
 * Data transfer object for the [BatchJob] entity
 *
 * @author Niels Visscher
 * @since 1.0
 */
@DtoMetadata(singular = "Report job", plural = "Report jobs", enableAddition = false)
data class BatchJobDto(
    @Hidden
    val batchJobId: UUID,

    val jobType: BatchJobType,

    val jobName: String,

    val parameters: String,

    val status: String,

    val finishedDate: Date?

) : Serializable, HasIdentity {
    override fun identity(): Array<Any> = arrayOf(batchJobId)
}
