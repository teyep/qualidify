package com.qualidify.service.process.dto

import com.qualidify.metadata.UploadFile
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.types.UploadType
import java.io.Serializable

/**
 * A Dto for Form definitions
 *
 * @author Marcel Pot
 * @since 1.0
 */
@DtoMetadata(
    singular = "Form Definition",
    plural = "Form Definitions",
    enableUpdate = false)
@CreationStrategy(
    CreationStrategyPattern.FACTORY,
    creationClass = FormDefinitionUpload::class)
data class FormDefinitionDto(
    @Hidden
    val id: String,
    val name: String,
    val key: String,
    @DtoColumn(searchable = false)
    val version: Int?,
    @DtoColumn(viewable = false)
    val form: Map<String, String> = emptyMap(),
) : Serializable


data class FormDefinitionUpload(
    @UploadType(acceptedFileTypes = [".json"])
    val file: UploadFile,
)