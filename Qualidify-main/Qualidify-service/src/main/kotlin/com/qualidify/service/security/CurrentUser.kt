package com.qualidify.service.security

import org.springframework.security.core.userdetails.UserDetails
import java.util.*

fun interface CurrentUser {
    fun getUser(): Optional<UserDetails>
}