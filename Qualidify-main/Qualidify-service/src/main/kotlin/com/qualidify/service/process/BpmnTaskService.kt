package com.qualidify.service.process

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.process.dto.BusinessProcessTaskDto

/**
 * A Service for handling BMPN Tasks, to retrieve the [BusinessProcessTaskDto] from Flowable
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface BpmnTaskService : Queryable<BusinessProcessTaskDto> {
}