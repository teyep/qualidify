package com.qualidify.service.process

import com.qualidify.core.query.service.Queryable
import com.qualidify.metadata.service.ObjectFactory
import com.qualidify.service.process.dto.FormDefinitionDto
import com.qualidify.service.process.dto.FormDefinitionUpload

/**
 * A service for FormDefinitions
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface FormDefinitionService
    : Queryable<FormDefinitionDto>,
    ObjectFactory<FormDefinitionDto, FormDefinitionUpload>