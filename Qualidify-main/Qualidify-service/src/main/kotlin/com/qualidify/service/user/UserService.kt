package com.qualidify.service.user

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.user.dto.CreateUserCommand
import com.qualidify.service.user.dto.UserDto
import java.util.*

/**
 * A service for managing user actions
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface UserService : Queryable<UserDto> {

    /**
     * Create a new user
     * @param user the UserDto containing the userdata
     * @return userDto
     */
    fun create(command: CreateUserCommand): UserDto

    /**
     * Update an existing user
     * @param user the UserDto containing the userdata
     * @return userDto
     * @throws UserNotFoundException
     */
    fun update(user: UserDto): UserDto

    /**
     * link a number of securitygroups to a user
     * @param userId the userId to link to
     * @param groups a collection of groupnames
     */
    fun linkGroupsToUser(userId: UUID, groups: Collection<String>)

    /**
     * Find a user by its name
     * @param name the name of the user to find
     * @return UserDto
     * @throws UserNotFoundException
     */
    fun findUserByName(name: String): UserDto

    /**
     * Find a user by its Identifier
     * @param userId the identifier of the user
     * @return the UserDto
     * @throws UserNotFoundException
     */
    fun findUserByUserId(userId: UUID): UserDto

}