package com.qualidify.service.configuration

class ConfigurationDomainNotFoundException(message: String? = null, throwable: Throwable? = null) :
    RuntimeException(message, throwable)
