package com.qualidify.service.process.dto

import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.annotations.Action
import com.qualidify.metadata.annotations.DtoColumn
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.Hidden
import java.io.Serializable

/**
 * A Dto for BPMN model information
 *
 * @author Menno Tol
 * @since 1.0
 */
@Action("Start", "Start")
@DtoMetadata(singular = "Process Definition", plural = "Process Definitions")
data class BusinessProcessDefinitionDto(
    @Hidden
    val id: String,

    val key: String,

    val name: String,
    @DtoColumn(searchable = false)
    val version: Int,

    @SearchQueryHint("hasGraphicalNotation")
    @DtoColumn(description = "diagram")
    val graphicalNotation: Boolean,
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BusinessProcessDefinitionDto) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}