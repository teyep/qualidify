package com.qualidify.service.report

import com.qualidify.metadata.service.dto.ObjectMap
import java.util.*

/**
 * Structure of the report parameters
 *
 * @author Niels Visscher
 */
data class ReportBatchJobParameters(
    val reportTemplate: UUID,
    val reportParameters: ObjectMap
)
