package com.qualidify.service.process.exceptions

class TaskServiceException(msg: String? = null, cause: Throwable? = null) : RuntimeException(msg, cause)