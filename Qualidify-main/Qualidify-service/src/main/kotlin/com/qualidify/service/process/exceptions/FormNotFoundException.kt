package com.qualidify.service.process.exceptions

/**
 * Exception indicating a problem with finding a form
 *
 * @author Marcel Pot
 * @since 1.0
 */
class FormNotFoundException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)