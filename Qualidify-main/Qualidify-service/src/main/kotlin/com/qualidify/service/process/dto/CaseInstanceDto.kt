package com.qualidify.service.process.dto

import com.qualidify.metadata.annotations.*
import com.qualidify.service.quality.HasQuality
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import java.io.Serializable

/**
 * A Dto for CMMN case instances
 *
 * @author Menno Tol
 * @since 1.0
 */
@Actions([
    Action("Complete case", "Complete"),
    Action("Cancel case", "Cancel"),
])
@DtoMetadata(singular = "Case Instance", plural = "Case Instances", enableUpdate = false, enableAddition = false)
data class CaseInstanceDto(

    override val id: String,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val name: String,

    @Hidden
    @DtoColumn(searchable = false, editable = false, sortable = false)
    val caseDefinition: CaseDefinitionDto,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    val caseDefinitionKey: String,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val projectInstanceId: String,

    val state: CaseInstanceState,

    val completable: Boolean,

    @Hidden
    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val isQualityShell: Boolean,

    @Hidden
    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val regularCaseId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityShellId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityLinkKey: QualityLinkKey,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val qualityLinkId: String?,

    @DtoColumn(searchable = false, editable = false, sortable = false)
    override val reviewState: ReviewState,


    ) : Serializable, HasQuality {
    override fun identity(): Array<Any> = arrayOf(id)
}

/**
 * Refer to flowable interface [CaseInstanceState]. 'NEW' is added for testing purposes.
 */
enum class CaseInstanceState(val state: String) {
    ACTIVE("active"),
    COMPLETED("completed"),
    TERMINATED("terminated"),
    FAILED("failed"),
    SUSPENDED("suspended"),
    NEW("new"),
    CLOSED("closed");

    override fun toString() = state

    companion object {
        fun getByName(name: String) = try {
            values().first { it.state == name }
        } catch (exception: Exception) {
            throw CaseInstanceDtoException("Flowable state '$name' is not implemented in CaseInstanceState enum")
        }
    }
}

class CaseInstanceDtoException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)
