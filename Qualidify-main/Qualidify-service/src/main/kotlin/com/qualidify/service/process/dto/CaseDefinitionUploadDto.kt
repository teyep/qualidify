package com.qualidify.service.process.dto

import com.qualidify.metadata.UploadFile
import com.qualidify.metadata.annotations.types.UploadType
import java.io.Serializable

data class CaseDefinitionUploadDto(
    @UploadType(
        label = "Upload CMMN file",
        acceptedFileTypes = [".xml"],
        dropAllowed = true
    )
    val file: UploadFile,
) : Serializable