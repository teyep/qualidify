package com.qualidify.service.quality

import com.qualidify.service.process.dto.PlanItemDto
import org.flowable.cmmn.api.runtime.PlanItemInstance

/**
 * An interface for the [StageReviewService]
 *
 * @author Menno Tol
 * @since 1.0
 */
interface StageReviewService: ReviewService<PlanItemDto> {

    /**
     * Starting point for incoming plan item instances.
     *
     * @param planItemInstance the delegate plan item instance obtained from the [StageCompletedListener]
     *
     * @throws ReviewServiceException when the plan item cannot be found.
     */
    fun startReview(planItemInstance: PlanItemInstance)

}