package com.qualidify.service.configuration.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoColumn
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.types.BooleanType
import com.qualidify.metadata.annotations.types.EditorType
import com.qualidify.metadata.annotations.types.SelectType
import java.io.Serializable

@DtoMetadata(singular = "Configuration Code", plural = "Configuration Codes", enableAddition = false)
data class ConfigurationCodeDto(
    @SearchQueryHint("id")
    val domain: String,

    @DtoColumn(sortable = false)
    @SearchQueryHint("codes.configurationCode")
    var code: String,

    @EditorType
    @SearchQueryHint("codes.~")
    var description: String,


    @SearchQueryHint("codes.~")
    @SelectType(itemProvider = "dataTypeEnumItemProvider")
    var dataType: DataType,

    @DtoColumn(sortable = false)
    @SearchQueryHint("codes.~")
    var value: String,

    @DtoColumn(sortable = false)
    @BooleanType
    @SearchQueryHint("codes.~")
    var active: Boolean,

) : Serializable, HasIdentity {

    override fun identity(): Array<Any> = arrayOf(domain, code)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ConfigurationCodeDto

        if (domain != other.domain) return false
        if (code != other.code) return false

        return true
    }

    override fun hashCode(): Int {
        var result = domain.hashCode()
        result = 31 * result + code.hashCode()
        return result
    }
}

