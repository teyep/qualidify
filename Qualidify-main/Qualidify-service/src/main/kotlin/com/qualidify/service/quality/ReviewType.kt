package com.qualidify.service.quality

/**
 * The type of relationship between regular and review items.
 */
enum class ReviewType(val type: String) {
    CASE("case"),
    STAGE("stage"),
    TASK("task"),
    ;

    override fun toString(): String = type

    companion object {
        fun getByName(type: String): ReviewType =
            try {
                ReviewType.values().single { it.type == type }
            } catch (e: Exception) {
                throw ReviewTypeException("Review type '$type' is not implemented in ReviewType enum")
            }
    }
}

class ReviewTypeException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)