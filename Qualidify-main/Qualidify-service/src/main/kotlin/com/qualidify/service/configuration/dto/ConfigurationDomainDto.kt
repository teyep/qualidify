package com.qualidify.service.configuration.dto

data class ConfigurationDomainDto(
    val domain: String,
    val description: String,
)