package com.qualidify.service.projects

import com.qualidify.core.query.service.Queryable
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.qualidify.service.projects.dto.TaskDto

/**
 * A wrapping interface for hiding the logic of the task implementation
 * This interface is meant to be used for retrieving task info
 * and handling task actions through the [TaskDto]
 *
 * @author Menno Tol
 * @since 1.0
 */
interface TaskService : Queryable<TaskDto> {

    /**
     * Marks the task as resolved (removes the assigned user)
     * NB: when it is unclear if the task requires review, use [TaskService.completeOrResolveTask]
     *
     * @param dto the TaskDto to resolve
     */
    fun resolveTask(dto: TaskDto)

    /**
     * Completes the task
     * NB: when it is unclear if the task requires review, use [TaskService.completeOrResolveTask]
     * @param dto the TaskDto to complete
     */
    fun completeTask(dto: TaskDto)

    /**
     * Determines if a task needs to be resolved or completed (based on its [ReviewState]). Performs the determined
     * action as a user
     * @param dto the TaskDto to resolve or complete
     * @param currentUser the current user who starts the task, if available, else null
     */
    fun completeOrResolveTask(dto: TaskDto, currentUser: String?)

    /**
     * Assign a task to another user
     * @param dto the TaskDto to start
     * @param user the User to assign the task to
     * @param currentUser the current user who starts the task
     */
    fun assignTask(dto: TaskDto, user: String, currentUser: String)

    /**
     * Claim a task
     * @param dto the TaskDto to claim
     * @param currentUser the current user who claims the task
     */
    fun claimTask(dto: TaskDto, currentUser: String)

    /**
     * Unclaim a task
     * @param dto the TaskDto to unclaim
     * @param currentUser the current user who claims the task
     */
    fun unclaimTask(dto: TaskDto, currentUser: String)

    /**
     * Retrieves the task dto
     * @param taskId the id of the task
     * @return the [TaskDto]
     */
    fun getTaskDtoById(taskId: String): TaskDto

    /**
     * Check if the task exists in the database
     * @param taskId the id of the task
     * @return true if the taskId could be found
     */
    fun isTaskAvailable(taskId: String): Boolean

    /**
     * Returns the [TaskDto] by the id of the corresponding plan item
     * @param planItemId the id of the [PlanItem]
     * @return the [TaskDto]
     */
    fun getTaskDtoByPlanItemId(planItemId: String): TaskDto

    /**
     * Returns the [ProjectInstanceDto] of the corresponding task
     * @param projectInstanceId the id of the project instance
     * @return the [ProjectInstanceDto]
     */
    fun getProjectInstanceDto(projectInstanceId: String): ProjectInstanceDto

}