package com.qualidify.service.quality

import com.qualidify.service.dto.HasIdAndName

/**
 * Interface ensuring that a dto has a quality shell mark that can be used to distinguish
 * review tasks from regular tasks.
 *
 * The [qualityShellId] is the cmmnInstanceId if the dto belongs directly to a quality shell (e.g. the parent
 * cmmnInstance is the quality shell).
 * If the cmmnInstanceId is not a qualityShell (e.g. it is started as a child cmmn in a quality shell) than the
 * qualityShellId refers to the id of the (parent) cmmn quality instance id.
 *
 * @author Menno Tol
 * @since 1.0
 */
interface HasQuality : HasIdAndName {
    val projectInstanceId: String
    val isQualityShell: Boolean?
    val regularCaseId: String?
    val qualityShellId: String?
    val qualityLinkKey: QualityLinkKey
    val qualityLinkId: String?
    val reviewState: ReviewState
}