package com.qualidify.service.report.exceptions

class ReportTemplateNotFoundException(msg: String? = null, cause: Throwable? = null) : RuntimeException(msg, cause)