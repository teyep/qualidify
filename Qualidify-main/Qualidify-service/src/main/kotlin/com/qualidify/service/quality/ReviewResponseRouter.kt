package com.qualidify.service.quality

import com.qualidify.service.projects.dto.TaskDto

/**
 * Interface for routing task actions (complete, approve, reject) to the correct service.
 * All task complete actions should pass via this service, to prevent inadvertently bypassing the quality cascade that
 * should be started.
 *
 * @author Menno Tol
 * @since 1.0
 */
interface ReviewResponseRouter {

    /**
     * Routes the review response to the correct service.
     *
     * @param qualityTaskDto the review task dto
     * @param principal the user committing the response
     * @param response the enumerized [ReviewResponse]
     *
     * @throws [ReviewResponseServiceException] when the correct service cannot be determined
     */
    fun routeRequest(qualityTaskDto: TaskDto, principal: String, response: ReviewResponse)

}

/**
 * Response for which the router should find the correct service call.
 */
enum class ReviewResponse {
    APPROVE,
    REJECT
}