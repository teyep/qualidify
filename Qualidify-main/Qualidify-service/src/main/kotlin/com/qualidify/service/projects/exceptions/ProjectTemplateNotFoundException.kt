package com.qualidify.service.projects.exceptions

/**
 * Exception indicating a problem with finding a project template
 *
 * @author Marcel Pot
 * @since 1.0
 */
class ProjectTemplateNotFoundException(msg: String? = null, cause: Throwable? = null) :
    RuntimeException(msg, cause)