package com.qualidify.service

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.annotations.Mnemonic
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.type.filter.AnnotationTypeFilter

/**
 * A DTO Model validation test, it will search the classpath for DTO classes and
 * validate if for each dto the correct configuration is used.
 *
 * @author Marcel Pot
 */
internal class ValidateDtoClassesTest {

    companion object {
        private const val PACKAGE = "com.qualidify"
    }


    @Test
    @DisplayName("Check if all dto-mnemonic classed implements the MetaDataDrivenDto interface ")
    fun doesMnemonicImplementsCorrectInterface() {
        val softAssertions = SoftAssertions()
        val provider = ClassPathScanningCandidateComponentProvider(false)
        provider.addIncludeFilter(AnnotationTypeFilter(Mnemonic::class.java))
        val dtos = provider.findCandidateComponents(PACKAGE)

        softAssertions.assertThat(dtos).`as`("No Dto classed found in classpath.").isNotEmpty

        dtos.stream()
            .map { bd: BeanDefinition -> getClassFromBeanDefinition(bd) }
            .filter { c -> !HasIdentity::class.java.isAssignableFrom(c) }
            .forEach { c -> softAssertions.fail("Class %s doesn't implement MetaDataDrivenDto", c) }

        softAssertions.assertAll()
    }

    private fun getClassFromBeanDefinition(bd: BeanDefinition): Class<*> {
        return try {
            Class.forName(bd.beanClassName)
        } catch (e: ClassNotFoundException) {
            throw RuntimeException("No class found with name " + bd.beanClassName, e)
        }
    }


}
