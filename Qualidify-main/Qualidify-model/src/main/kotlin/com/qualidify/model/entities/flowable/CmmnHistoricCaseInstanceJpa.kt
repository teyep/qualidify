package com.qualidify.model.entities.flowable

import com.qualidify.model.entities.flowable.definition.CmmnCaseInstanceJpaDef
import org.hibernate.annotations.Immutable
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "cmmn_historic_case_instance_view")
@Immutable
class CmmnHistoricCaseInstanceJpa(
    @Id
    @Column(name = "ID_")
    override val id: String,

    @Column(name = "project_instance_id")
    override val projectInstanceId: String?,

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CASE_DEF_ID_", referencedColumnName = "ID_")
    override val cmmnCaseDefinition: CmmnCaseDefinitionJpa,

    @Column(name = "STATE_")
    override val state: String,

    @Column(name = "completable")
    override val completable: Boolean,

    @Column(name = "historic")
    override val historic: Boolean,

    @Column(name = "is_quality_shell")
    override val isQualityShell: Boolean,

    @Column(name = "regular_case_id")
    override val regularCaseId: String?,

    @Column(name = "quality_shell_id")
    override val qualityShellId: String?,

    @Column(name = "quality_link_key")
    override val qualityLinkKey: String?,

    @Column(name = "quality_link_id")
    override val qualityLinkId: String?,

    @Column(name = "review_state")
    override val reviewState: String,

    ) : Serializable, CmmnCaseInstanceJpaDef