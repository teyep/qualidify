package com.qualidify.model.entities.flowable

import org.hibernate.annotations.Immutable
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Immutable
@Entity
@Table(name = "ACT_RE_PROCDEF")
class BpmnDefinitionJpa(
        @Id
        @Column(name = "ID_")
        val id: String,
        @Column(name = "NAME_")
        val name: String,

        @Column(name = "KEY_")
        val key: String,

        @Column(name = "VERSION_")
        val version: Int,

        @Column(name = "HAS_GRAPHICAL_NOTATION_")
        val hasGraphicalNotation: Boolean = false
) : Serializable