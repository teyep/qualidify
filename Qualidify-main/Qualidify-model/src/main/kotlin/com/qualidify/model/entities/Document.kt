package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "document")
class Document(
    id: DocumentId,
    var name: String,
    var type: String,
    var path: String,
    var size: Long,
    var owner: String? = null,
    @Column(name = "is_private")
    var private: Boolean = false,
    @Column(name = "is_deleted")
    var deleted: Boolean = false,
    var version: Long = 1,

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "document_attributes", joinColumns = [JoinColumn(name = "document_id")])
    var attributes: Set<DocumentAttribute> = emptySet(),

    @ManyToMany
    @JoinTable(
        name = "document_label",
        joinColumns = [JoinColumn(name = "document_id")],
        inverseJoinColumns = [JoinColumn(name = "label_id")]
    )
    var labels: List<Label> = emptyList(),

    ) : AbstractAuditableBaseEntity<DocumentId>(id)

@Embeddable
class DocumentAttribute(
    @Column(name = "attribute_key")
    var key: String = "",
    @Column(name = "attribute_value")
    var value: String? = null,
) : Serializable


@Entity
@Table(name = "label")
class Label(
    @Id
    val id: LabelId,

    @ManyToMany(mappedBy = "labels", fetch = FetchType.LAZY)
    var documents: List<Document> = emptyList(),
) {
    constructor(name: String) : this(LabelId(name))

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Label) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "Label(id=$id)"
    }


}

@Entity
@Table(name = "document_content")
@EntityListeners(AuditingEntityListener::class)
class DocumentContent(
    @Id
    val id: DocumentId,

    @Lob
    var content: ByteArray,

    @Column(name = "created_on", updatable = false)
    @CreatedDate
    var createDate: Date? = null,

    @Column(name = "created_by")
    @CreatedBy
    var createdBy: String? = null,

    @Version
    var version: Int? = 0
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DocumentContent) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}

@Embeddable
class DocumentId(id: UUID) : AbstractEntityId<UUID>(id)

@Embeddable
class LabelId(id: String) : AbstractEntityId<String>(id)