package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import java.io.Serializable
import java.util.*
import javax.persistence.*

/**
 * An entity representing a template for the reporting service
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Entity
@Table(name = "report_template")
class ReportTemplate(

    id: ReportTemplateId,

    @Column(name = "template_name")
    var templateName: String,

    @Column(name = "template_description")
    var templateDescription: String,

    @Column(name = "template_data")
    @Lob
    var templateData: ByteArray,

    @Column(name = "template_parameters")
    var templateParameters: String,

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
        name = "report_service_resource",
        joinColumns = [JoinColumn(name = "template_id")]
    )
    var resources: Set<ReportServiceResource> = mutableSetOf()

) : AbstractAuditableAndVersionableBaseEntity<ReportTemplateId>(id) {
    constructor(
        id: UUID,
        templateName: String,
        templateDescription: String,
        data: ByteArray,
        parameters: String,
        resources: Set<ReportServiceResource>
    ) : this(ReportTemplateId(id), templateName, templateDescription, data, parameters, resources)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ReportTemplate

        if (id != other.id) return false
        if (templateName != other.templateName) return false
        if (templateDescription != other.templateDescription) return false
        if (!templateData.contentEquals(other.templateData)) return false
        if (templateParameters != other.templateParameters) return false
        if (resources != other.resources) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + templateName.hashCode()
        result = 31 * result + templateDescription.hashCode()
        result = 31 * result + templateData.contentHashCode()
        result = 31 * result + templateParameters.hashCode()
        result = 31 * result + resources.hashCode()
        return result
    }
}

@Embeddable
class ReportTemplateId (id: UUID) : AbstractEntityId<UUID>(id)

/**
 * A resource associated with a ReportServiceTemplate instance
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Embeddable
class ReportServiceResource(
    @Column(name = "resource_name")
    var resourceName: String,

    @Column(name = "resource_data")
    var resourceData: ByteArray
) : Serializable {
    override fun toString(): String {
        return "ReportServiceResource(resourceName='$resourceName', resourceData=${resourceData.contentToString()})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ReportServiceResource

        if (resourceName != other.resourceName) return false
        if (!resourceData.contentEquals(other.resourceData)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = resourceName.hashCode()
        result = 31 * result + resourceData.contentHashCode()
        return result
    }
}
