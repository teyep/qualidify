package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractSimpleAuditableBaseEntity
import com.qualidify.metadata.DataType
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "configurationcode")
@IdClass(ConfigurationCodePk::class)
class ConfigurationCode(

    @Id
    @Column(name = "configurationdomain_id")
    val configurationDomainId: String,

    @Id
    @Column(name = "code")
    val configurationCode: String,

    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("configurationdomain_id")
    @JoinColumn(name = "configurationdomain_id")
    var configurationDomain: ConfigurationDomain? = null,

    var description: String,

    @Column(name = "datatype")
    @Enumerated(EnumType.STRING)
    var dataType: DataType,

    @Column(name = "configurationvalue")
    var value: String,

    var active: Boolean,
) : AbstractSimpleAuditableBaseEntity(), Serializable {


    @JvmOverloads
    constructor(
        domain: ConfigurationDomain,
        code: String,
        description: String,
        dataType: DataType,
        value: String,
        active: Boolean = true
    ) : this(
        configurationDomainId = domain.id.unbox(),
        configurationCode = code,
        configurationDomain = domain,
        description = description,
        dataType = dataType,
        value = value,
        active = active
    )

    @JvmOverloads
    constructor(
        domain: String,
        code: String,
        description: String,
        dataType: DataType,
        value: String,
        active: Boolean = true
    ) : this(
        configurationDomainId = domain,
        configurationCode = code,
        description = description,
        dataType = dataType,
        value = value,
        active = active
    )


    val code: String
        get() = this.configurationCode

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ConfigurationCode) return false

        if (configurationDomainId != other.configurationDomainId) return false
        if (configurationCode != other.configurationCode) return false

        return true
    }

    override fun hashCode(): Int {
        var result = configurationDomainId.hashCode()
        result = 31 * result + configurationCode.hashCode()
        return result
    }

    override fun toString(): String {
        return "ConfigurationCode(configurationDomainId='$configurationDomainId', " +
                "configurationCode='$configurationCode', " +
                "description='$description', " +
                "dataType=$dataType, " +
                "value='$value', " +
                "active=$active, " +
                "code='$code')"
    }


}


@Embeddable
class ConfigurationCodePk(
    @Column(name = "configurationdomain_id")
    val configurationDomainId: String,
    @Column(name = "code")
    val configurationCode: String
) : Serializable {

    constructor(configurationDomainId: ConfigurationDomainId, configurationCode: String) :
            this(configurationDomainId.unbox(), configurationCode)

    constructor(configurationDomain: ConfigurationDomain, configurationCode: String) :
            this(configurationDomain.id.unbox(), configurationCode)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ConfigurationCodePk) return false

        if (configurationDomainId != other.configurationDomainId) return false
        if (configurationCode != other.configurationCode) return false

        return true
    }

    override fun hashCode(): Int {
        var result = configurationDomainId.hashCode()
        result = 31 * result + configurationCode.hashCode()
        return result
    }


}
