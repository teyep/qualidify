package com.qualidify.model.entities.flowable

import org.hibernate.annotations.Immutable
import org.hibernate.annotations.Where
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Immutable
@Entity
@Table(name = "ACT_RU_TASK")
@Where(clause = "SCOPE_TYPE_ is null")
class BpmnTaskJpa(
        @Id
        @Column(name = "ID_")
        val id: String,

        @Column(name = "NAME_")
        val name: String,

        @Column(name = "OWNER_")
        val owner: String? = null,

        @Column(name = "ASSIGNEE_")
        val assignee: String? = null,

        @Column(name = "SCOPE_TYPE_")
        val type: String?,

        @Column(name = "FORM_KEY_")
        val formKey: String?,

        @Column(name = "SCOPE_ID_")
        val scopeId: String?,

        @Column(name = "SUB_SCOPE_ID_")
        val subScopeId: String?
) : Serializable