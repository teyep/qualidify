package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractSimpleAuditableBaseEntity
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * A Case File identity (can be linked to other identities?)
 * @author Marcel Pot
 * @since 1.0
 */
@Entity
@Table(name = "case_file")
class CaseFile(
    @Id
    val id: UUID,
    @Column(name = "NAME")
    var name: String,
    @Column(name = "DESCRIPTION")
    var description: String,
)

    : AbstractSimpleAuditableBaseEntity() {

    fun setValue() {
        //empty, needs to be used later?
    }
}