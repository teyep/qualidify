package com.qualidify.model.entities.metadata

import com.qualidify.core.jpa.converter.CommaSeperatedKeyValuePairToMapConverter
import com.qualidify.core.jpa.converter.CommaSeperatedStringToArrayConverter
import com.qualidify.metadata.DataType
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "metadata_column")
@IdClass(MetadataColumnPk::class)
data class MetadataColumn(

    @Id
    val mnemonic: String,
    @Id
    val name: String,

    val description: String,

    @Column(name = "datatype")
    @Enumerated(EnumType.STRING)
    val dataType: DataType,

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(
        name = "metadata_attributes",
        joinColumns = [
            JoinColumn(name = "mnemonic", referencedColumnName = "mnemonic"),
            JoinColumn(name = "name", referencedColumnName = "name")
        ]
    )
    @MapKeyColumn(name = "attribute_key")
    @Column(name = "attribute_value")
    val attributes: Map<String, String> = mapOf(),


    val sortorder: Int,
    val hidden: Boolean = false,
    val viewable: Boolean = true,
    val editable: Boolean = true,
    val searchable: Boolean = true,
    val sortable: Boolean = true,
    @Convert(converter = CommaSeperatedStringToArrayConverter::class)
    val segments: Array<String> = emptyArray(),
    val renderer: String? = null,
    @Convert(converter = CommaSeperatedKeyValuePairToMapConverter::class)
    val rendererAttributes: Map<String, String> = emptyMap()

) {

    override fun toString(): String {
        return "MetadataColumn(" +
                "name='$name', " +
                "description='$description', " +
                "dataType=$dataType, " +
                "attributes=$attributes, " +
                "sortorder=$sortorder, " +
                "hidden=$hidden, " +
                "viewable=$viewable, " +
                "editable=$editable, " +
                "searchable=$searchable, " +
                "sortable=$sortable)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MetadataColumn) return false

        if (mnemonic != other.mnemonic) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = mnemonic.hashCode()
        result = 31 * result + name.hashCode()
        return result
    }
}

@Embeddable
data class MetadataColumnPk(
    @Column(name = "mnemonic")
    val mnemonic: String,

    @Column(name = "name")
    val name: String,
) : Serializable