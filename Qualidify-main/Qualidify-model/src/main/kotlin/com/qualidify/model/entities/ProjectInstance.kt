package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import com.qualidify.metadata.DataType
import org.hibernate.annotations.Where
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

/**
 * A wrapper entity for holding Project information. The [ProjectInstance] contains references to the quality shell
 * and the regular cmmn instance (both are Flowable cmmn instances). It also holds variables required for Flowable.
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Entity
@Table(name = "project_instance")
class ProjectInstance(
    id: ProjectInstanceId,

    @Column(name = "project_template_id")
    var templateId: UUID,

    var name: String,

    var description: String,

    @Column(name = "quality_shell_id")
    var qualityShellId: String,

    @Column(name = "quality_shell_instance_state")
    var qualityShellInstanceState: String,  // TODO: implement in QUAL-461

    @Column(name = "regular_case_id")
    var regularCaseId: String,

    @Column(name = "regular_case_instance_state")
    var regularCaseState: String,

    @Column(name = "start_time")
    var startTime: LocalDateTime,

    @OneToMany(fetch = FetchType.EAGER)
    @Where(clause = "reference_type = 'PROJECT_INSTANCE'")
    @JoinColumn(name = "reference_id", referencedColumnName = "id")
    var customFields: MutableSet<CustomFieldData> = mutableSetOf(),

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "project_instance_variables", joinColumns = [JoinColumn(name = "project_instance_id")])
    var variables: List<ProjectInstanceVariable> = listOf(),

    @ManyToMany
    @JoinTable(
        name = "project_instance_users",
        joinColumns = [JoinColumn(name = "project_instance_id")],
        inverseJoinColumns = [JoinColumn(name = "user_id")]
    )
    var users: List<User> = listOf(),

    var state: String,

    ) : AbstractAuditableAndVersionableBaseEntity<ProjectInstanceId>(id)

@Embeddable
class ProjectInstanceVariable(
    var name: String,
    @Enumerated(EnumType.STRING)
    var datatype: DataType,
    @Column(name = "val")
    var value: String?,
) : Serializable

@Embeddable
class ProjectInstanceId(id: UUID) : AbstractEntityId<UUID>(id)