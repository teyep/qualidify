package com.qualidify.model.entities.metadata

import com.qualidify.core.jpa.entities.AbstractSimpleAuditableAndVersionableBaseEntity
import javax.persistence.*

@Entity
@Table(name = "metadata_object")
data class MetadataObject(

    @Id
    val mnemonic: String?,
    val singlename: String,
    val pluralname: String,
    val addable: Boolean = true,
    val updatable: Boolean = true,
    val removable: Boolean = true,
    val searchable: Boolean = true,

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "mnemonic", unique = true)
    @OrderBy("sortorder ASC")
    val columns: List<MetadataColumn> = listOf(),

    ) : AbstractSimpleAuditableAndVersionableBaseEntity() {

    override fun toString(): String {
        return "MetadataObject(" +
                "mnemonic=$mnemonic, " +
                "singlename='$singlename', " +
                "pluralname='$pluralname', " +
                "addable=$addable, " +
                "updatable=$updatable, " +
                "removable=$removable, " +
                "searchable=$searchable, " +
                "columns=${columns.forEach { it.name }})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MetadataObject) return false

        if (mnemonic != other.mnemonic) return false

        return true
    }

    override fun hashCode(): Int {
        return mnemonic.hashCode()
    }


}



