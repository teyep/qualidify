package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "configurationdomain")
class ConfigurationDomain(

    domain: ConfigurationDomainId,

    var description: String,

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "configurationdomain_id")
    var codes: MutableList<ConfigurationCode> = mutableListOf(),

    ) : AbstractAuditableBaseEntity<ConfigurationDomainId>(domain) {
    @JvmOverloads
    constructor(domain: String, description: String, codes: MutableList<ConfigurationCode> = mutableListOf())
            : this(ConfigurationDomainId(domain), description, codes)

    fun getByCode(code: String) =
        Optional.ofNullable(codes.asSequence().filter { it.configurationCode.equals(code) }.firstOrNull())


}

@Embeddable
class ConfigurationDomainId(id: String) : AbstractEntityId<String>(id)