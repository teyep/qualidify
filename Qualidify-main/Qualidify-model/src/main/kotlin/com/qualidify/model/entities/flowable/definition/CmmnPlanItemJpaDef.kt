package com.qualidify.model.entities.flowable.definition

import java.io.Serializable
import java.time.LocalDateTime

interface CmmnPlanItemJpaDef : Serializable {
    val id: String
    val name: String
    val localizedName: String?
    val projectInstanceId: String?
    val caseInstanceId: String
    val reference: String?
    val stage: String
    val state: String
    val definitionType: String
    val completable: Boolean
    val historic: Boolean
    val timeCreated: LocalDateTime
    val isQualityShell: Boolean
    val regularCaseId: String?
    val qualityShellId: String?
    val qualityLinkKey: String?
    val qualityLinkId: String?
    val reviewState: String
}
