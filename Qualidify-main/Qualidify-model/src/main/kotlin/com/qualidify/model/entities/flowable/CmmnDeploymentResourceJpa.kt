package com.qualidify.model.entities.flowable

import org.hibernate.annotations.Immutable
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "ACT_CMMN_DEPLOYMENT_RESOURCE")
@Immutable
class CmmnDeploymentResourceJpa(
    @Id
    @Column(name = "ID_")
    var id: String,

    @Column(name = "DEPLOYMENT_ID_")
    val deploymentId: String,

    @Column(name = "NAME_")
    val name: String?,

    @Column(name = "RESOURCE_BYTES_")
    val graphicalNotation: ByteArray?

) : Serializable