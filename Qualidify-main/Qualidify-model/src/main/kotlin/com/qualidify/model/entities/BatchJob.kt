package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import java.util.*
import javax.persistence.*


@Entity
@Table(name = "batch_jobs")
class BatchJob(
    id: BatchJobId,

    @Column(name = "job_type")
    @Enumerated(EnumType.STRING)
    var jobType: BatchJobType,

    @Column(name = "job_name")
    var jobName: String,

    @Column
    var parameters: String,

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    var status: BatchJobStatus,

    @Column(name = "finished_on")
    var finishedDate: Date? = null
) : AbstractAuditableAndVersionableBaseEntity<BatchJobId>(id)

@Embeddable
class BatchJobId(id: UUID) : AbstractEntityId<UUID>(id)

enum class BatchJobType {
    JasperReportJob
}

enum class BatchJobStatus {
    NEW,
    RUN,
    DONE,
    ERROR
}
