package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users")
class User(
    id: UserId,

    @Column
    var username: String,

    @Column
    var password: String,

    @Column
    var email: String,

    @Column
    var enabled: Boolean = false,

    @Column(name = "non_expired")
    var nonExpired: Boolean = false,

    @Column(name = "credentials_non_expired")
    var credentialsNonExpired: Boolean = false,

    @Column(name = "account_non_locked")
    var accountNonLocked: Boolean = false,

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_fields", joinColumns = [JoinColumn(name = "user_id")])
    var fields: Set<UserField> = mutableSetOf(),

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_securitygroup",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "securitygroup_id")]
    )
    var groups: Set<SecurityGroup> = setOf(),

    ) : AbstractAuditableAndVersionableBaseEntity<UserId>(id) {


    constructor(id: UserId) : this(id, username = "", password = "", email = "")
    constructor(uuid: UUID) : this(UserId(uuid))

    constructor(
        uuid: UUID,
        username: String,
        password: String,
        email: String,
        enabled: Boolean,
        nonExpired: Boolean,
        credentialsNonExpired: Boolean,
        accountNonLocked: Boolean,
        fields: Set<UserField>,
        groups: Set<SecurityGroup>,
    ) :
            this(
                UserId(uuid),
                username,
                password,
                email,
                enabled,
                nonExpired,
                credentialsNonExpired,
                accountNonLocked,
                fields,
                groups
            )

    val userId: UUID
        get() = id.unbox()

    val roles: Set<String>
        get() = groups.asSequence()
            // .filter {it.isEnabled }
            .flatMap { it.roles.asSequence() }
            .map { it.name }
            .toSet()

    fun toBuilder(): UserBuilder {
        return UserBuilder(this)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is User) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    companion object {
        @JvmStatic
        fun builder(): UserBuilder {
            return UserBuilder.anUser()
        }
    }


}

@Embeddable
class UserId(id: UUID) : AbstractEntityId<UUID>(id)

@Embeddable
class UserField(

    var field: String = "",

    @Column(name = "val")
    var value: String? = null,
) : Serializable {
    constructor() : this("", null)
}


class UserBuilder {
    private var username: String = ""
    private var password: String = ""
    private var email: String = ""
    private var enabled = false
    private var nonExpired = false
    private var credentialsNonExpired = false
    private var accountNonLocked = false
    private var groups: Set<SecurityGroup> = HashSet()
    private var fields: MutableSet<UserField> = HashSet()
    private var id: UserId? = null

    private constructor() {}

    internal constructor(user: User) {
        id = user.id
        username = user.username
        password = user.password
        email = user.email
        enabled = user.enabled
        nonExpired = user.nonExpired
        credentialsNonExpired = user.credentialsNonExpired
        accountNonLocked = user.accountNonLocked
        groups = HashSet(user.groups)
        fields = HashSet(user.fields)
    }

    fun withUsername(username: String): UserBuilder {
        this.username = username
        return this
    }

    fun withPassword(password: String): UserBuilder {
        this.password = password
        return this
    }

    fun withEmail(email: String): UserBuilder {
        this.email = email
        return this
    }

    fun withEnabled(enabled: Boolean): UserBuilder {
        this.enabled = enabled
        return this
    }

    fun withNonExpired(nonExpired: Boolean): UserBuilder {
        this.nonExpired = nonExpired
        return this
    }

    fun withCredentialsNonExpired(credentialsNonExpired: Boolean): UserBuilder {
        this.credentialsNonExpired = credentialsNonExpired
        return this
    }

    fun withAccountNonLocked(accountNonLocked: Boolean): UserBuilder {
        this.accountNonLocked = accountNonLocked
        return this
    }

    fun withGroups(groups: Set<SecurityGroup>): UserBuilder {
        this.groups = groups
        return this
    }

    fun withField(field: UserField): UserBuilder {
        fields.add(field)
        return this
    }

    fun withField(key: String, value: String?): UserBuilder {
        return this.withField(UserField(key, value))
    }

    fun withFields(fields: Set<UserField>): UserBuilder {
        this.fields = fields.toMutableSet()
        return this
    }

    fun withId(id: UserId?): UserBuilder {
        this.id = id
        return this
    }

    fun withUserId(uuid: UUID): UserBuilder {
        id = UserId(uuid)
        return this
    }

    fun build(): User {
        val user = User(
            id = id!!,
            username = username,
            password = password,
            email = email,
            enabled = enabled,
            nonExpired = nonExpired,
            credentialsNonExpired = credentialsNonExpired,
            accountNonLocked = accountNonLocked,
            fields = fields,
            groups = groups.toMutableSet()
        )
        return user
    }

    companion object {
        fun anUser(): UserBuilder {
            return UserBuilder()
        }
    }
}