package com.qualidify.model.entities.flowable

import com.qualidify.model.entities.flowable.definition.CmmnTaskJpaDef
import org.hibernate.annotations.Immutable
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Immutable
@Entity
@Table(name = "cmmn_historic_task_view")
class CmmnHistoricTaskJpa(
    @Id
    @Column(name = "ID_")
    override val id: String,

    @Column(name = "NAME_")
    override val name: String,

    @Column(name = "localized_name")
    override val localizedName: String?,

    @Column(name = "project_instance_id")
    override val projectInstanceId: String?,

    @Column(name = "TASK_DEF_KEY_")
    override val taskDefinitionKey: String,

    @Column(name = "OWNER_")
    override val owner: String? = null,

    @Column(name = "ASSIGNEE_")
    override val assignee: String? = null,

    @Column(name = "SCOPE_TYPE_")
    override val type: String,

    @Column(name = "FORM_KEY_")
    override val formKey: String?,

    @Column(name = "SCOPE_ID_")
    override val caseInstanceId: String,

    @Column(name = "SCOPE_DEFINITION_ID_")
    override val caseDefinitionId: String,

    @Column(name = "SUB_SCOPE_ID_")
    override val planItemInstanceId: String,

    @Column(name = "PROPAGATED_STAGE_INST_ID_")
    override val stage: String?,

    @Column(name = "historic")
    override val historic: Boolean,

    @Column(name = "is_quality_shell")
    override val isQualityShell: Boolean,

    @Column(name = "regular_case_id")
    override val regularCaseId: String?,

    @Column(name = "quality_shell_id")
    override val qualityShellId: String?,

    @Column(name = "quality_link_key")
    override val qualityLinkKey: String?,

    @Column(name = "quality_link_id")
    override val qualityLinkId: String?,

    @Column(name = "review_state")
    override val reviewState: String,

    ) : Serializable, CmmnTaskJpaDef





