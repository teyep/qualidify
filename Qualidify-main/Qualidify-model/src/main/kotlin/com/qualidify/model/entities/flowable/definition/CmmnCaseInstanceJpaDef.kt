package com.qualidify.model.entities.flowable.definition

import com.qualidify.model.entities.flowable.CmmnCaseDefinitionJpa
import java.io.Serializable

interface CmmnCaseInstanceJpaDef : Serializable {
    val id: String
    val projectInstanceId: String?
    val cmmnCaseDefinition: CmmnCaseDefinitionJpa
    val state: String
    val completable: Boolean
    val historic: Boolean
    val isQualityShell: Boolean
    val regularCaseId: String?
    val qualityShellId: String?
    val qualityLinkKey: String?
    val qualityLinkId: String?
    val reviewState: String
}