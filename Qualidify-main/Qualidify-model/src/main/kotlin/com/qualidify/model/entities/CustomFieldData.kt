package com.qualidify.model.entities

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "customfield_data")
@IdClass(CustomFieldDataPk::class)

class CustomFieldData(
    @Id
    val referenceType: String,
    @Id
    val referenceId: String,

    var field: String = "",

    @Column(name = "val")
    var value: String? = null,
) : Serializable

@Embeddable
class CustomFieldDataPk(
    @Column(name = "reference_type")
    val referenceType: String,
    @Column(name = "reference_id")
    val referenceId: String
) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CustomFieldDataPk) return false

        if (referenceType != other.referenceType) return false
        if (referenceId != other.referenceId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = referenceType.hashCode()
        result = 31 * result + referenceId.hashCode()
        return result
    }
}