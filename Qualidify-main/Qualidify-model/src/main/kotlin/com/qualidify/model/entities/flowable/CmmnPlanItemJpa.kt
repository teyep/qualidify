package com.qualidify.model.entities.flowable

import com.qualidify.model.entities.flowable.definition.CmmnPlanItemJpaDef
import org.hibernate.annotations.Immutable
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Immutable
@Entity
@Table(name = "cmmn_plan_item_view")
class CmmnPlanItemJpa(
    @Id
    @Column(name = "ID_")
    override val id: String,

    @Column(name = "NAME_")
    override val name: String,

    @Column(name = "localized_name")
    override val localizedName: String?,

    @Column(name = "project_instance_id")
    override val projectInstanceId: String?,

    @Column(name = "CASE_INST_ID_")
    override val caseInstanceId: String,

    @Column(name = "REFERENCE_ID_")
    override val reference: String? = null,

    @Column(name = "STAGE_INST_ID_")
    override val stage: String,

    @Column(name = "STATE_")
    override val state: String,

    @Column(name = "ITEM_DEFINITION_TYPE_")
    override val definitionType: String,

    @Column(name = "IS_COMPLETEABLE_")
    override val completable: Boolean,

    @Column(name = "CREATE_TIME_")
    override val timeCreated: LocalDateTime,

    @Column(name = "historic")
    override val historic: Boolean,

    @Column(name = "is_quality_shell")
    override val isQualityShell: Boolean,

    @Column(name = "regular_case_id")
    override val regularCaseId: String?,

    @Column(name = "quality_shell_id")
    override val qualityShellId: String?,

    @Column(name = "quality_link_key")
    override val qualityLinkKey: String?,

    @Column(name = "quality_link_id")
    override val qualityLinkId: String?,

    @Column(name = "review_state")
    override val reviewState: String,

    ) : Serializable, CmmnPlanItemJpaDef


