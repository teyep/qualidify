package com.qualidify.model.entities.metadata

import com.qualidify.core.jpa.entities.AbstractEntityId
import com.qualidify.core.jpa.entities.AbstractSimpleAuditableBaseEntity
import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import org.hibernate.annotations.*
import java.util.*
import javax.persistence.*
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "customfield")
@SQLDelete(sql = "UPDATE customfield SET deleted = true WHERE id=?")
@FilterDef(
    name = "deletedCustomFieldsFilter",
    parameters = [ParamDef(name = "isDeleted", type = "boolean")]
)
@Filter(name = "deletedCustomFieldsFilter", condition = "deleted = :isDeleted")
data class CustomField(
    @Id
    val id: CustomFieldId,
    @Enumerated(EnumType.STRING)
    val reference: Reference,
    val field: String,
    var description: String = "",

    @Column(name = "datatype")
    @Enumerated(EnumType.STRING)
    var dataType: DataType,
    var validator: String? = null,
    var mandatory: Boolean = false,
    var active: Boolean = true,
) : AbstractSimpleAuditableBaseEntity() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CustomField) return false

        if (reference != other.reference) return false
        if (field != other.field) return false

        return true
    }

    override fun hashCode(): Int {
        var result = reference.hashCode()
        result = 31 * result + field.hashCode()
        return result
    }

    override fun toString(): String {
        return "CustomField(" +
                "id=$id, " +
                "reference=$reference, " +
                "field='$field', " +
                "description='$description', " +
                "datatype=$dataType, " +
                "validator=$validator, " +
                "mandatory=$mandatory, " +
                "active=$active)"
    }


}

@Embeddable
class CustomFieldId(id: UUID) : AbstractEntityId<UUID>(id)


