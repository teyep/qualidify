package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDate
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "filestorage")
class FileStorage(
    id: FileStorageId,

    var path: String,
    var filename: String,
    var filetype: String,

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "id")
    var content: FileContent

) : AbstractAuditableAndVersionableBaseEntity<FileStorageId>(id) {
}

@Entity
@Table(name = "tempfilestorage")
@EntityListeners(AuditingEntityListener::class)
class TempFileStorage(

    @Id
    val hash: TempFileStorageId,
    var content: ByteArray,

    @Column(name = "created_on", nullable = false, updatable = false)
    @CreatedDate
    var createDate: LocalDate = LocalDate.now(),

    @Column(name = "created_by")
    @CreatedBy
    var createdBy: String? = null
) {
    constructor(hash: String, content: ByteArray) : this(hash = TempFileStorageId(hash), content = content)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TempFileStorage) return false

        if (hash != other.hash) return false

        return true
    }

    override fun hashCode(): Int {
        return hash.hashCode()
    }


}

@Embeddable
class FileStorageId(id: UUID) : AbstractEntityId<UUID>(id)

@Embeddable
class TempFileStorageId(id: String) : AbstractEntityId<String>(id)

@Entity
@Table(name = "filecontent")
class FileContent(
    @Id
    val id: FileStorageId,

    var hash: String,
    @Lob
    var content: ByteArray,

    )