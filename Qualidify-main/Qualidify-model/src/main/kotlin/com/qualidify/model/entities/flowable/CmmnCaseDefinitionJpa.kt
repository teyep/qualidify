package com.qualidify.model.entities.flowable

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import org.hibernate.annotations.Immutable
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "ACT_CMMN_CASEDEF")
@Immutable
class CmmnCaseDefinitionJpa(
    @Id
    @Column(name = "ID_")
    val id: String,

    @Column(name = "NAME_")
    val name: String,

    @Column(name = "KEY_")
    val key: String,

    @Column(name = "VERSION_")
    val version: Int,

    @OneToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumns(
        JoinColumn(name = "DEPLOYMENT_ID_", referencedColumnName = "DEPLOYMENT_ID_"),
        JoinColumn(name = "DGRM_RESOURCE_NAME_", referencedColumnName = "NAME_")
    )
    @Fetch(FetchMode.JOIN)
    val deploymentResource: CmmnDeploymentResourceJpa? = null


) : Serializable

