package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import com.qualidify.metadata.DataType
import org.hibernate.annotations.Where
import java.io.Serializable
import java.util.*
import javax.persistence.*


@Entity
@Table(name = "project_template")
class ProjectTemplate(
    id: ProjectTemplateId,

    var name: String,
    var description: String,
    @Column(name = "cmmn_model")
    var cmmnModel: String,

    @OneToMany(fetch = FetchType.EAGER)
    @Where(clause = "reference_type = 'PROJECT_TEMPLATE'")
    @JoinColumn(name = "reference_id", referencedColumnName = "id")
    var customFields: MutableSet<CustomFieldData> = mutableSetOf(),

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "project_template_variables", joinColumns = [JoinColumn(name = "project_template_id")])
    var variables: List<ProjectTemplateVariable> = listOf()

) : AbstractAuditableAndVersionableBaseEntity<ProjectTemplateId>(id)

@Embeddable
class ProjectTemplateVariable(
    var name: String,
    var description: String,
    @Enumerated(EnumType.STRING)
    var datatype: DataType,
    @Column(name = "default_value")
    var defaultValue: String,
    var required: Boolean = true,
    @Column(name = "user_input")
    var userInput: Boolean = true,
) : Serializable

@Embeddable
class ProjectTemplateId(id: UUID) : AbstractEntityId<UUID>(id)



