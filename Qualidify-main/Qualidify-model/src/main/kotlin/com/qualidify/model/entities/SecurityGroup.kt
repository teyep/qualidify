package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractEntityId
import javax.persistence.*

@Entity
@Table(name = "securitygroup")
class SecurityGroup(
    id: SecurityGroupId,

    @Column
    var description: String,

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "securitygroup_roles", joinColumns = [JoinColumn(name = "securitygroup_id")])
    var roles: Set<SecurityRole> = setOf(),

    @Column
    var enabled: Boolean = true,
) : AbstractAuditableAndVersionableBaseEntity<SecurityGroupId>(id) {

    constructor(id: String, description: String, roles: Set<SecurityRole> = setOf(), enabled: Boolean = true) :
            this(SecurityGroupId(id), description, roles, enabled)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is SecurityGroup) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


}

@Embeddable
class SecurityGroupId(id: String) : AbstractEntityId<String>(id)

@Embeddable
class SecurityRole(
    @Column(name = "role")
    val name: String
)

