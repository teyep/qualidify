package com.qualidify.model.entities

import com.qualidify.metadata.DataType
import com.qualidify.model.entities.flowable.*
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.api.runtime.PlanItemInstance
import org.flowable.cmmn.engine.impl.persistence.entity.CaseInstanceEntityImpl
import org.flowable.cmmn.engine.impl.persistence.entity.PlanItemInstanceEntityImpl
import org.flowable.task.api.Task
import org.flowable.task.service.impl.persistence.entity.TaskEntityImpl
import java.time.LocalDateTime
import java.util.*

/**
 * Central location for test entities, sorted alphabetically.
 * Part of main folder so that it can be shared with other modules.
 *
 * The default arguments match with the corresponding dto's, see [com.qualidify.service.DtoFactory]).
 * Please use Kotlin's named argument function with defaults for introducing variables.
 */
object TestEntities {

    fun batchJob(): BatchJob {
        return BatchJob(
            id = BatchJobId(UUID.randomUUID()),
            jobName = "Test Job",
            jobType = BatchJobType.JasperReportJob,
            parameters = "Parameters",
            status = BatchJobStatus.NEW,
            finishedDate = null
        )
    }

    fun bpmnTaskJpa() = BpmnTaskJpa(
        id = "mockDto",
        name = "mockName",
        owner = "",
        assignee = "",
        type = "bpmn",
        formKey = "Form",
        scopeId = "",
        subScopeId = ""
    )

    fun caseInstance(
        id: String = "caseInstance",
        parentId: String? = null,
    ): CaseInstance = CaseInstanceEntityImpl()
        .also {
            it.id = id
            it.caseDefinitionKey = "caseDefKey"
            it.caseDefinitionVersion = 1
            it.state = "NEW"
            it.parentId = parentId
        }

    fun cmmnCaseInstanceJpa(
        id: String = "caseInstanceId",
        projectInstanceId: String = UUID.randomUUID().toString(),
        caseDefinitionId: String = "definitionId",
        caseDefinitionKey: String = "definitionKey",
        isQualityShell: Boolean = false,
        regularCaseId: String? = null,
        qualityShellId: String? = "qualityShellId",
        qualityLinkKey: String? = null,
        qualityLinkId: String? = null,
        reviewState: String = "undefined",
    ) = CmmnCaseInstanceJpa(
        id = id,
        projectInstanceId = projectInstanceId,
        cmmnCaseDefinition = CmmnCaseDefinitionJpa(
            id = caseDefinitionId,
            name = "definitionName",
            key = caseDefinitionKey,
            version = 1
        ),
        state = "new",
        completable = false,
        historic = false,
        isQualityShell = isQualityShell,
        regularCaseId = regularCaseId,
        qualityShellId = qualityShellId,
        qualityLinkKey = qualityLinkKey,
        qualityLinkId = qualityLinkId,
        reviewState = reviewState,
    )

    fun cmmnHistoricCaseInstanceJpa(
        id: String = "historicCaseInstanceId",
        projectInstanceId: String = UUID.randomUUID().toString(),
        caseDefinitionId: String = "definitionId",
        caseDefinitionKey: String = "definitionKey",
        isQualityShell: Boolean = false,
        regularCaseId: String? = null,
        qualityShellId: String? = "cmmnQualityShellId",
        qualityLinkKey: String? = null,
        qualityLinkId: String? = null,
        reviewState: String = "undefined",
    ) = CmmnHistoricCaseInstanceJpa(
        id = id,
        projectInstanceId = projectInstanceId,
        cmmnCaseDefinition = CmmnCaseDefinitionJpa(
            id = caseDefinitionId,
            name = "definitionName",
            key = caseDefinitionKey,
            version = 1
        ),
        state = "new",
        completable = false,
        historic = true,
        isQualityShell = isQualityShell,
        regularCaseId = regularCaseId,
        qualityShellId = qualityShellId,
        qualityLinkKey = qualityLinkKey,
        qualityLinkId = qualityLinkId,
        reviewState = reviewState,
    )

    fun cmmnCaseDefinitionJpa(
        id: String = "caseDefinitionId",
        caseDefinitionKey: String = "caseDefinitionKey",
        deploymentResource: CmmnDeploymentResourceJpa? = cmmnDeploymentResourceJpa(),
    ): CmmnCaseDefinitionJpa = CmmnCaseDefinitionJpa(
        id = id,
        name = "caseDefinitionName",
        key = caseDefinitionKey,
        version = 1,
        deploymentResource = deploymentResource
    )

    fun cmmnDeploymentResourceJpa(
        graphicalNotation: ByteArray? = "TestBytes".toByteArray(),
    ) = CmmnDeploymentResourceJpa(
        id = "testId",
        deploymentId = "deploymentId",
        name = "TestBytes",
        graphicalNotation = graphicalNotation
    )

    fun cmmnPlanItemJpa(
        id: String = "mockPlanItemId",
        caseInstanceId: String = "caseInstanceId",
        name: String = "mockPlanItemName",
        state: String = "active",
        type: String,
    ) = CmmnPlanItemJpa(
        id = id,
        caseInstanceId = caseInstanceId,
        name = name,
        localizedName = null,
        projectInstanceId = "projectInstanceId",
        definitionType = type,
        stage = "mockStage",
        state = state,
        reference = "mockReference",
        completable = true,
        timeCreated = LocalDateTime.of(2023, 2, 9, 9, 39, 39),
        historic = false,
        isQualityShell = false,
        regularCaseId = null,
        qualityShellId = "qualityShellId",
        qualityLinkKey = null,
        qualityLinkId = null,
        reviewState = "undefined"
    )

    fun cmmnTaskJpa(assignee: String, form: String) = CmmnTaskJpa(
        id = "mockTaskDto",
        name = "mockTaskName",
        localizedName = null,
        projectInstanceId = "projectInstanceId",
        taskDefinitionKey = "taskDefinitionKey",
        owner = "mockOwner",
        type = "cmmn",
        assignee = assignee,
        formKey = form,
        caseInstanceId = "cmmnInstanceId",
        caseDefinitionId = "caseDefinitionId",
        planItemInstanceId = "planItemInstanceId",
        stage = "stageInstanceId",
        historic = false,
        isQualityShell = false,
        regularCaseId = null,
        qualityShellId = "qualityShellId",
        qualityLinkKey = null,
        qualityLinkId = null,
        reviewState = "undefined"
    )

    fun cmmnHistoricTaskJpa() = CmmnHistoricTaskJpa(
        id = "historicTaskId",
        name = "historicTaskName",
        localizedName = null,
        projectInstanceId = "projectInstanceId",
        taskDefinitionKey = "taskDefinitionKey",
        owner = "owner",
        assignee = "assignee",
        type = "cmmn",
        formKey = "formKey",
        caseInstanceId = "caseInstanceId",
        caseDefinitionId = "caseDefinitionId",
        planItemInstanceId = "planItemInstanceId",
        stage = "stageInstanceId",
        historic = true,
        isQualityShell = false,
        regularCaseId = null,
        qualityShellId = "qualityShellId",
        qualityLinkKey = null,
        qualityLinkId = null,
        reviewState = "undefined"
    )

    fun document(): Document {
        return Document(
            id = DocumentId(UUID.randomUUID()),
            name = "Test document",
            type = "pdf",
            path = "/test-document",
            size = 5,
            owner = null,
            private = false,
            deleted = false,
            attributes = emptySet(),
            labels = emptyList()
        )
    }

    fun documentContent(id: DocumentId): DocumentContent {
        return DocumentContent(
            id,
            byteArrayOf(84, 101, 115, 116, 0),
            Date(),
            "Test user"
        )
    }

    fun projectInstance(
        id: UUID = UUID.randomUUID(),
        templateId: ProjectTemplateId = ProjectTemplateId(UUID.randomUUID()),
        cmmnInstanceId: String = "cmmnCaseInstanceId",
        cmmnQualityShellId: String = "cmmnQualityShellId",
    ) = ProjectInstance(
        id = ProjectInstanceId(id),
        templateId = templateId.unbox(),
        name = "ProjectInstance",
        description = "ProjectInstanceDescription",
        qualityShellId = cmmnQualityShellId,
        qualityShellInstanceState = "active",
        regularCaseId = cmmnInstanceId,
        regularCaseState = "active",
        startTime = LocalDateTime.now(),
        variables = listOf(
            ProjectInstanceVariable(
                name = "First Input",
                datatype = DataType.STRING,
                value = "START"
            ),
            ProjectInstanceVariable(
                name = "Second Input",
                datatype = DataType.DATETIME,
                value = LocalDateTime.now().toString()
            ),
        ),
        state = "active",
    )

    fun projectTemplate() = ProjectTemplate(
        id = ProjectTemplateId(UUID.randomUUID()),
        name = "ProjectTemplateName",
        description = "ProjectTemplateDescription",
        cmmnModel = UUID.randomUUID().toString(),
        variables = listOf(
            ProjectTemplateVariable(
                name = "First Input",
                description = "First Input",
                datatype = DataType.STRING,
                defaultValue = ""
            ),
            ProjectTemplateVariable(
                name = "Second Input",
                description = "First Input",
                datatype = DataType.DATETIME,
                defaultValue = ""
            )
        )
    )

    fun reportTemplate(): ReportTemplate {
        return ReportTemplate(
            UUID.randomUUID(),
            "TestTemplate",
            "Test template description",
            ByteArray(3),
            "",
            mutableSetOf<ReportServiceResource>()
        )
    }

    fun reportTemplateWithResource(): ReportTemplate {
        val resourceSet: MutableSet<ReportServiceResource> = HashSet<ReportServiceResource>()
        resourceSet.add(ReportServiceResource("TestResource", ByteArray(3)))
        return ReportTemplate(
            UUID.randomUUID(),
            "TestTemplate",
            "Test template with one resource",
            ByteArray(3),
            "",
            resourceSet
        )
    }

    fun taskEntity(): Task = TaskEntityImpl().also {
        it.id = "mockTaskId"
        it.scopeId = "mockTaskScopeId"
        it.name = "mockTaskName"
        it.assignee = null
    }

    fun planItemInstanceEntity(): PlanItemInstance = PlanItemInstanceEntityImpl().also {
        it.id = "mockPlanItemInstanceId"
        it.name = "mockPlanItemInstanceName"
    }

    fun stageInstanceEntity(): PlanItemInstance = PlanItemInstanceEntityImpl().also {
        it.id = "mockStageId"
        it.name = "mockStageName"
    }

}
