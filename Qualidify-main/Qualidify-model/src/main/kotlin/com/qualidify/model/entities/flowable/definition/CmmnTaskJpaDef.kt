package com.qualidify.model.entities.flowable.definition

interface CmmnTaskJpaDef {
    val id: String
    val name: String
    val localizedName: String?
    val projectInstanceId: String?
    val taskDefinitionKey: String
    val owner: String?
    val assignee: String?
    val type: String
    val formKey: String?
    val caseInstanceId: String
    val caseDefinitionId: String?
    val planItemInstanceId: String
    val stage: String?
    val historic: Boolean
    val isQualityShell: Boolean
    val regularCaseId: String?
    val qualityShellId: String?
    val qualityLinkKey: String?
    val qualityLinkId: String?
    val reviewState: String
}