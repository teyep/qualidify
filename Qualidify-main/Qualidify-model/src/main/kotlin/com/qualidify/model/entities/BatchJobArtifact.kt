package com.qualidify.model.entities

import com.qualidify.core.jpa.entities.AbstractAuditableBaseEntity
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Lob
import javax.persistence.Table

/**
 * Entity containing the artifact produced by a finished batch job (such as a report).
 *
 * @author Niels Visscher
 * @since 1.0
 */

// Similar to: DocumentContent
@Entity
@Table(name = "batch_job_artifacts")
class BatchJobArtifact(
    id: BatchJobId,

    @Column(name = "version")
    var version: Int,

    @Column(name = "mime_type")
    var mimeType: String,

    @Lob
    var content: ByteArray,

    @Column(name = "finished_on", updatable = false)
    var finishedDate: Date? = null,

    ) : AbstractAuditableBaseEntity<BatchJobId>(id) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BatchJobArtifact

        if (id != other.id) return false
        if (mimeType != other.mimeType) return false
        if (!content.contentEquals(other.content)) return false
        if (finishedDate != other.finishedDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + mimeType.hashCode()
        result = 31 * result + content.contentHashCode()
        result = 31 * result + (finishedDate?.hashCode() ?: 0)
        return result
    }
}
