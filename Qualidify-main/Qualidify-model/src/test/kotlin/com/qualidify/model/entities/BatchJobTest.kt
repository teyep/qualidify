package com.qualidify.model.entities

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

/**
 * Test suite for BatchJob entity
 *
 * @author Niels Visscher
 * @since 1.0
 */
internal class BatchJobTest : AbstractEntityTest() {

    /**
     * Verifies that a batch job can be retrieved after having been saved
     */
    @Test
    fun `BatchJob can be retrieved`() {
        val batchJob = TestEntities.batchJob()
        session.persist(batchJob)
        val foundJob = session.find(BatchJob::class.java, batchJob.id)
        assertThat(foundJob).`as`("Batch job was found").isEqualTo(batchJob)
    }
}
