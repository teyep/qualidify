package com.qualidify.model.entities

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class ProjectInstanceTest : AbstractEntityTest() {
    @Test
    fun `ProjectInstance can be saved`() {
        val projectTemplate = TestEntities.projectTemplate()
        val projectInstance = TestEntities.projectInstance(templateId = projectTemplate.id)

        val identity = projectInstance.id

        session.persist(projectTemplate)
        session.persist(projectInstance)
        session.flush()

        val loadedProjectInstance: ProjectInstance = session.find(ProjectInstance::class.java, identity)
        Assertions.assertThat(loadedProjectInstance)
            .`as`("Saved ProjectInstance should be matching found ProjectInstance")
            .isEqualTo(projectInstance)
    }


}