package com.qualidify.model.entities

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class ProjectTemplateTest : AbstractEntityTest() {
    @Test
    fun `ProjectTemplate can be saved`() {
        val projectTemplate = TestEntities.projectTemplate()
        val identity = projectTemplate.id

        session.persist(projectTemplate)
        session.flush()

        val loadedProjectTemplate: ProjectTemplate = session.find(ProjectTemplate::class.java, identity)
        Assertions.assertThat(loadedProjectTemplate)
            .`as`("Saved ProjectTemplate should be matching found ProjectTemplate").isEqualTo(projectTemplate)
    }
}