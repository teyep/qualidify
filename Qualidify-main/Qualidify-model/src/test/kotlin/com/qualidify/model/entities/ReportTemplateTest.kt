package com.qualidify.model.entities

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class ReportTemplateTest : AbstractEntityTest() {
    @Test
    fun `ReportServiceTemplate can be saved`()
    {
        val template: ReportTemplate = TestEntities.reportTemplate()
        session.persist(template)
        // Fetch the new template again
        session.flush()
        val templateTest: ReportTemplate = session.find(ReportTemplate::class.java, template.id)
        Assertions.assertThat(templateTest).`as`("Simple report template can be retrieved").isEqualTo(template)
    }

    @Test
    fun `ReportServiceTemplate with resource can be saved`() {
        val template: ReportTemplate = TestEntities.reportTemplateWithResource()
        session.persist(template)
        val templateTest: ReportTemplate = session.find(ReportTemplate::class.java, template.id)

        Assertions.assertThat(templateTest).`as`("Template can be retrieved").isEqualTo(template)
        Assertions.assertThat(templateTest.resources).`as`("Retrieved has one resource").hasSize(1)
        Assertions.assertThat(templateTest.resources).`as`("").isEqualTo(template.resources);
    }
}
