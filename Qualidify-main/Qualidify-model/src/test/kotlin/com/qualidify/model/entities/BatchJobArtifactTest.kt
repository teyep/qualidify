package com.qualidify.model.entities

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import java.util.*

/**
 * Test suite for BatchJobArtifact entity
 *
 * @author Niels Visscher
 * @since 1.0
 */
@SpringBootTest(classes = [JpaTestConfiguration::class])
class BatchJobArtifactTest : AbstractEntityTest() {

    @Test
    fun `Saved report artifact can be retrieved` () {
        val reportTemplate = TestEntities.reportTemplate()
        session.persist(reportTemplate)
        val artifact = BatchJobArtifact(BatchJobId(UUID.randomUUID()), 0, "text/plain", "Test".toByteArray(), Date())
        session.persist(artifact)

        val foundResult = session.find(BatchJobArtifact::class.java, artifact.id)
        assertThat(foundResult).`as`("Batch job artifact was found").isEqualTo(artifact)
    }
}
