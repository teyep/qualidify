package com.qualidify.model.entities.metadata

import com.qualidify.model.entities.AbstractEntityTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class MetadataObjectTest : AbstractEntityTest() {

    @Test
    fun `Metadata_dto without columns is saveable and deletable`() {
        val mnemonic = "Mnemonic"
        val metadataObject = MetadataObject(
            mnemonic = mnemonic,
            singlename = "Test",
            pluralname = "Tests"
        )

        session.persist(metadataObject)
        session.flush()

        val metadataObjectFound = session.find(MetadataObject::class.java, mnemonic)
        assertThat(metadataObjectFound).`as`("Saved MetadataObject should be matching found metadataObject")
            .isEqualTo(metadataObject)

        session.delete(metadataObjectFound)

        val noMetadataObjectFound = session.find(MetadataObject::class.java, mnemonic)
        assertThat(noMetadataObjectFound).`as`("After a deletion the object is not present anymore").isNull()

    }

}