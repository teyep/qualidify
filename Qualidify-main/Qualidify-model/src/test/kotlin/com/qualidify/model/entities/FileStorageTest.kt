package com.qualidify.model.entities

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.security.MessageDigest
import java.util.*
import javax.xml.bind.DatatypeConverter


open class FileStorageTest : AbstractEntityTest() {

    @Test
    open fun `given a File, when saved it should be found`() {
        val bytes = "test.txt".toByteArray()
        val fileId = FileStorageId(UUID.randomUUID())
        val fileContent = FileContent(fileId, "", bytes)
        val fileToSave = FileStorage(fileId, "/path/", " file", "type", fileContent)

        session.persist(fileToSave)
        session.flush()
        val fileFound: FileStorage = session.find(FileStorage::class.java, fileToSave.id)

        assertThat(fileFound).`as`("Saved file should be matching found file").isEqualTo(fileToSave)
    }

    @Test
    open fun `given a TempFile, when saved it should be found`() {
        val bytes = "test.txt".toByteArray()
        val hash = hash(bytes)
        val tempFileToSave = TempFileStorage(hash, bytes)

        session.persist(tempFileToSave)
        session.flush()

        val fileFound: TempFileStorage = session.find(TempFileStorage::class.java, TempFileStorageId(hash))
        assertThat(fileFound).`as`("Saved file should be matching found file").isEqualTo(tempFileToSave)

    }

    fun hash(bytes: ByteArray): String {
        val md: MessageDigest = MessageDigest.getInstance("MD5")
        md.update(bytes)
        val digest: ByteArray = md.digest()
        return DatatypeConverter.printHexBinary(digest).uppercase()
    }


}