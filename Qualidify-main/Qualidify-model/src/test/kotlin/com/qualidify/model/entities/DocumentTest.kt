package com.qualidify.model.entities

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Instant
import java.util.*

class DocumentTest : AbstractEntityTest() {

    @Test
    fun `given a Document, when saved it should be found`() {

        val labels = listOf(
            getLabel("TEST"),
            getLabel("DOC")
        )

        val document1 = getDocument(labels)
        val document2 = getDocument(labels)

        session.persist(labels[0])
        session.persist(labels[1])

        session.persist(document1)
        session.persist(document2)
        session.flush()

        val documentFound = session.find(Document::class.java, document1.id)

        assertThat(documentFound).`as`("Saved document should be matching found document")
            .isEqualTo(document1)
    }

    @Test
    fun `given a Document with attributes, when saved it should be found`() {

        val labels = listOf(
            getLabel("Important"),
            getLabel("Letter")
        )

        val document = getDocument(
            labels, setOf(
                DocumentAttribute("author", "Paulus"),
                DocumentAttribute("addressee", "Efeze")
            )
        )

        session.persist(labels[0])
        session.persist(labels[1])

        session.persist(document)
        session.flush()

        val documentFound = session.find(Document::class.java, document.id)

        assertThat(documentFound).`as`("Saved document with attributes should be matching found document")
            .isEqualTo(document)
    }

    @Test
    fun `given a Label, when saved it should be found`() {
        val label = getLabel("TEST")

        val document1 = getDocument(listOf(label))
        val document2 = getDocument(listOf(label))

        session.persist(label)
        session.persist(document1)
        session.persist(document2)

        session.flush()
        session.clear()

        val labelFound = session.find(Label::class.java, label.id)

        assertThat(labelFound).`as`("Saved label should be matching found label")
            .isEqualTo(label)

        assertThat(labelFound.documents).`as`("Two documents should be linked to this label")
            .hasSize(2)
    }

    @Test
    fun `test with document content`() {
        val label = getLabel("TEST")
        val document = getDocument(listOf(label))

        val content = getDocumentContent(document.id)


        session.persist(label)
        session.persist(document)
        session.persist(content)

        session.flush()
        session.clear()

        val contentFound = session.find(DocumentContent::class.java, content.id)

        assertThat(contentFound).`as`("Saved Content should be matching found content")
            .isEqualTo(content)

    }

    companion object {
        fun getLabel(name: String) = Label(name)

        fun getDocument(labels: List<Label>) = Document(
            id = DocumentId(UUID.randomUUID()),
            name = "TestDocument",
            type = "docx",
            path = "/",
            size = 1,
            labels = labels
        )

        fun getDocument(labels: List<Label>, attributes: Set<DocumentAttribute>) = Document(
            id = DocumentId(UUID.randomUUID()),
            name = "TestDocument",
            type = "docx",
            path = "/",
            size = 1,
            labels = labels,
            attributes = attributes
        )


        fun getDocumentContent(id: DocumentId) = DocumentContent(
            id = id,
            content = getByteArray(),
            createDate = Date.from(Instant.now()),
            createdBy = "DocumentTest"

        )

        fun getByteArray() = "TestByteArray".toByteArray()
    }
}