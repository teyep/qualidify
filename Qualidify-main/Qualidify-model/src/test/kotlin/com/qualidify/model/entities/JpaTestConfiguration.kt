package com.qualidify.model.entities

import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.util.*


@EnableJpaAuditing
@EnableTransactionManagement
@TestConfiguration
open class JpaTestConfiguration {

//    @Bean
//    open fun dataSource(): DataSource {
//        val dataSourceBuilder = DataSourceBuilder.create()
//        dataSourceBuilder.driverClassName("org.h2.Driver")
//        dataSourceBuilder.url("jdbc:h2:mem:unittestdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE")
//        dataSourceBuilder.username("4m")
//        dataSourceBuilder.password("")
//        return dataSourceBuilder.build()
//    }

//    @Bean
//    open fun liquibase(): SpringLiquibase {
//        val liquibase = SpringLiquibase()
//        liquibase.changeLog = "classpath:db/liquibase-changelog.xml"
//        liquibase.dataSource = dataSource()
//        return liquibase
//    }

//    @Bean
//    open fun sessionFactory(dataSource: DataSource): LocalSessionFactoryBean? {
//        val sessionFactory = LocalSessionFactoryBean()
//        sessionFactory.setDataSource(dataSource)
//        sessionFactory.setPackagesToScan("com.qualidify.model.entities")
//        sessionFactory.hibernateProperties = hibernateProperties()
//        return sessionFactory
//    }
//
//    @Bean
//    open fun hibernateTransactionManager(dataSource: DataSource): PlatformTransactionManager? {
//        val transactionManager = HibernateTransactionManager()
//        transactionManager.sessionFactory = sessionFactory(dataSource)!!.getObject()
//        return transactionManager
//    }
//
//    private fun hibernateProperties(): Properties {
//        val hibernateProperties = Properties()
//        hibernateProperties["hibernate.dialect"] = "org.hibernate.dialect.H2Dialect"
//        hibernateProperties["hibernate.show_sql"] = "true"
//        hibernateProperties["hibernate.format_sql"] = "true"
//        return hibernateProperties
//    }

    @Bean
    open fun auditorAware(): AuditorAware<String> {
        return AuditorAware { Optional.of("UnitTester") }
    }


}




