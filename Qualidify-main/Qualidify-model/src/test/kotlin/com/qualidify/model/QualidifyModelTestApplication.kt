package com.qualidify.model

import org.springframework.boot.SpringApplication

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan


@SpringBootApplication
@EntityScan(basePackages = ["com.qualidify.model.entities"])
open class QualidifyModelTestApplication {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(QualidifyModelTestApplication::class.java, *args)
        }
    }

}