package com.qualidify.model.entities

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*

class CaseFileTest : AbstractEntityTest() {

    @Test
    fun `save a casefile`() {

        val caseFile = CaseFile(UUID.randomUUID(), "Entity", UUID.randomUUID().toString())

        session.persist(caseFile)
        session.flush()

        val caseFileFound = session.find(CaseFile::class.java, caseFile.id)

        Assertions.assertThat(caseFileFound).`as`("Saved casefile should be matching found casefile")
            .isEqualTo(caseFile)
    }
}