package com.qualidify.model.entities

import org.hibernate.Session
import org.hibernate.SessionFactory
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [JpaTestConfiguration::class])
abstract class AbstractEntityTest {

    @Autowired
    val sessionFactory: SessionFactory? = null

    lateinit var session: Session

    @BeforeEach
    fun setup() {
        session = sessionFactory!!.openSession()
        session.beginTransaction()
    }

    @AfterEach
    fun afterTest() {
        session.transaction.rollback()
        session.close()
    }

}