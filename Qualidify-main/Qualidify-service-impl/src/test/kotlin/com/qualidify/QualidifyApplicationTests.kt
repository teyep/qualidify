package com.qualidify

import org.flowable.cmmn.engine.CmmnEngineConfiguration
import org.flowable.engine.ProcessEngineConfiguration
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ActiveProfiles("singleserver")
class QualidifyApplicationTests {

    @Autowired
    private lateinit var processEngineConfiguration: ProcessEngineConfiguration

    @Autowired
    private lateinit var cmmnEngineConfiguration: CmmnEngineConfiguration

    @Test
    fun contextLoads() {
    }

    /**
     * The Flowable engine needs to be properly shut down after test execution, otherwise console logging will
     * run over other tests.
     */
    @AfterAll
    fun tearDown() {
        cmmnEngineConfiguration.asyncExecutor.shutdown()
        processEngineConfiguration.asyncExecutor.shutdown()
    }

}


