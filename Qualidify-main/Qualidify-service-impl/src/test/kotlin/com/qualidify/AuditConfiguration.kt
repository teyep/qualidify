package com.qualidify

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import java.util.*

@EnableJpaAuditing

@EntityScan(basePackages = ["com.qualidify.model", "com.qualidify.metadata.service.impl.domain"])
@TestConfiguration
open class AuditConfiguration {

    @Bean
    @Primary
    open fun auditorAware(): AuditorAware<String> {
        return AuditorAware { Optional.of("UT-Qualidify") }
    }
}