package com.qualidify.configuration

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.MessageSource
import org.springframework.test.context.ContextConfiguration
import java.util.*

/**
 * Integration test for MessageSource
 *
 * @author Marcel Pot
 */
@SpringBootTest
@ContextConfiguration(classes = [I18nConfiguration::class])
internal class I18nMessageSourceIntegrationTest {
    @Autowired
    var i18nMessageSource: MessageSource? = null

    @Test
    fun testMessageSource() {
        Assertions.assertThat(i18nMessageSource!!.getMessage("test.hello", null, Locale.ENGLISH))
            .`as`("English Test.Hello translation based on message bundle")
            .isEqualTo("Hello!")

        Assertions.assertThat(i18nMessageSource!!.getMessage("test.hello", null, LOCALE_NL))
            .`as`("Dutch Test.Hello translation based on message bundle")
            .isEqualTo("Hallo!")
    }

    companion object {
        private val LOCALE_NL = Locale("nl_NL")
    }
}