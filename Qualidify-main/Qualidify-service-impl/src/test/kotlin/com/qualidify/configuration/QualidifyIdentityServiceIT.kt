package com.qualidify.configuration

import org.assertj.core.api.Assertions.assertThat
import org.flowable.idm.api.IdmIdentityService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
//(classes = [QualidifyApplication::class])
//    PersistanceConfiguration::class,
//    FlowableConfiguration::class])

class QualidifyIdentityServiceIT {

    @Autowired
    lateinit var identityService: IdmIdentityService

    @Test
    fun identityServiceTest() {
        assertThat(identityService.createUserQuery()).`as`("IdentityService should be configured to be QualidifyIdentityService and returns a QualidifyUserQuery")
            .isInstanceOf(QualidifyUserQuery::class.java)
        assertThat(identityService.createGroupQuery()).`as`("IdentityService should be configured to be QualidifyIdentityService and returns a QualidifyGroupQuery")
            .isInstanceOf(QualidifyGroupQuery::class.java)
    }


}