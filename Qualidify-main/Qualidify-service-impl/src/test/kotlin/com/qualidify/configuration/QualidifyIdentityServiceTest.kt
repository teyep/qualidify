package com.qualidify.configuration

import com.qualidify.service.security.UserAuthorizationService
import com.qualidify.service.user.exceptions.UserNotFoundException
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.flowable.idm.engine.IdmEngineConfiguration
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class QualidifyIdentityServiceTest {

    @Mock
    lateinit var authorizationService: UserAuthorizationService

    @Mock
    lateinit var config: IdmEngineConfiguration

    @InjectMocks
    lateinit var fixture: QualidifyIdentityService


    @Test
    fun testCreateUserQuery() {
        val userId = UUID.randomUUID()
        val user = createUser("Albus Dumbledore")

        `when`(authorizationService.loadUserById(userId)).thenReturn(user)

        val list = fixture.createUserQuery().userId(userId.toString()).list()

        assertThat(list).`as`("Only one user should be in the list").hasSize(1).contains(QFUser("Albus Dumbledore"))

        verifyNoMoreInteractions(authorizationService)
    }

    @Test
    fun testCreateUserQueryNotExisting() {
        val userId = UUID.randomUUID()

        `when`(authorizationService.loadUserById(userId)).thenThrow(UserNotFoundException::class.java)

        Assertions.assertThatThrownBy {
            fixture.createUserQuery().userId(userId.toString()).list()
        }.isInstanceOf(UserNotFoundException::class.java)

        verifyNoMoreInteractions(authorizationService)
    }

    @Test
    fun testCreateUserQueryWithMultipleUsers() {
        val userId_1 = UUID.randomUUID()
        val userId_2 = UUID.randomUUID()

        `when`(authorizationService.loadUsersByIds(listOf(userId_1, userId_2))).thenReturn(
            listOf(
                createUser("Fred Weasly"),
                createUser("George Weasly")
            )
        )


        val list = fixture.createUserQuery().userIds(listOf(userId_1.toString(), userId_2.toString())).list()
        assertThat(list).`as`("Two wizards should be in the list")
            .hasSize(2)
            .contains(QFUser("Fred Weasly"), QFUser("George Weasly"))

        verifyNoMoreInteractions(authorizationService)
    }

    @Test
    fun testCreateUserQueryWithMultipleUsersWithNoResult() {
        val userId_1 = UUID.randomUUID()
        val userId_2 = UUID.randomUUID()

        `when`(authorizationService.loadUsersByIds(listOf(userId_1, userId_2))).thenReturn(listOf())


        val list = fixture.createUserQuery().userIds(listOf(userId_1.toString(), userId_2.toString())).list()
        assertThat(list).`as`("An empty list is expected")
            .hasSize(0)

        verifyNoMoreInteractions(authorizationService)
    }


    @Test
    fun testCreateGroupQuery() {
        `when`(authorizationService.loadGroupByName("Accio")).thenReturn(SimpleGrantedAuthority("Accio"))

        val list = fixture.createGroupQuery().groupId("Accio").list()

        assertThat(list).`as`("Only one group should be in the list")
            .hasSize(1)
            .contains(QFGroup("Accio"))

        verifyNoMoreInteractions(authorizationService)
    }

    @Test
    fun testCreateGroupQueryNotExisting() {
        `when`(authorizationService.loadGroupByName("Wingardium Leviosa")).thenReturn(null)

        val list = fixture.createGroupQuery().groupId("Wingardium Leviosa").list()

        assertThat(list).`as`("No group should be in the list")
            .hasSize(0)

        verifyNoMoreInteractions(authorizationService)
    }

    @Test
    fun testCreateGroupQueryWithMultipleGroups() {
        `when`(
            authorizationService.loadGroupsByNames(
                listOf(
                    "MUGGLE",
                    "WIZARD"
                )
            )
        ).thenReturn(listOf(SimpleGrantedAuthority("MUGGLE"), SimpleGrantedAuthority("WIZARD")))

        val list = fixture.createGroupQuery().groupIds(listOf("MUGGLE", "WIZARD")).list()

        assertThat(list).`as`("Only two groups should be in the list")
            .hasSize(2)
            .contains(QFGroup("MUGGLE"), QFGroup("WIZARD"))

        verifyNoMoreInteractions(authorizationService)
    }

    @Test
    fun testCreateGroupQueryWithMultipleGroupsNotExisting() {
        `when`(authorizationService.loadGroupsByNames(listOf("MUGGLE", "WIZARD"))).thenReturn(setOf())

        val list = fixture.createGroupQuery().groupIds(listOf("MUGGLE", "WIZARD")).list()

        assertThat(list).`as`("No groups should be in the list")
            .hasSize(0)

        verifyNoMoreInteractions(authorizationService)
    }

    private fun createUser(username: String): UserDetails {
        return org.springframework.security.core.userdetails.User.withUsername(username)
            .password(username)
            .roles("Expelliarmus")
            .build()
    }

}