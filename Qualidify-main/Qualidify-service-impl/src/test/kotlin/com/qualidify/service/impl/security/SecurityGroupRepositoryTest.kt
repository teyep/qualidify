package com.qualidify.service.impl.security

import com.qualidify.model.entities.SecurityGroup
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@DataJpaTest
open class SecurityGroupRepositoryTest(
    @Autowired val securityGroupRepository: SecurityGroupRepository
) {

    @Sql("/sql/SecurityGroupRepositoryTest.sql")
    @Test
    fun findAllByUserName() {
        val groupsOfNoOne = securityGroupRepository.findAllByUserName("NoOne")
        Assertions.assertThat(groupsOfNoOne).`as`("Only one group is available").isEmpty()

        val groupsOfKassim = securityGroupRepository.findAllByUserName("Kasim Baba")
        Assertions.assertThat(groupsOfKassim).`as`("Only one group is available").hasSize(1)
        Assertions.assertThat(groupsOfKassim[0].id.unbox()).isEqualTo("DESERT")

        val groupsOfAli = securityGroupRepository.findAllByUserName("Ali Baba")
        Assertions.assertThat(groupsOfAli).`as`("Three groups are available").hasSize(3)
        Assertions.assertThat(groupsOfAli).contains(
            SecurityGroup("CAVE", "Access to Cave"),
            SecurityGroup("DESERT", "Access to Desert"),
            SecurityGroup("HOUSE", "Access to House")
        )

    }

    @Sql("/sql/SecurityGroupRepositoryTest.sql")
    @Test
    fun findAllByUserNames() {
        val groupsOfNoOneAndKasim = securityGroupRepository.findAllByUserNames(setOf("NoOne", "Kasim Baba"))
        Assertions.assertThat(groupsOfNoOneAndKasim).`as`("Only one group is available").hasSize(1)

        val groupsOfAli = securityGroupRepository.findAllByUserNames(setOf("Ali Baba", "Kasim Baba"))
        Assertions.assertThat(groupsOfAli).`as`("Three groups are available").hasSize(3)
        Assertions.assertThat(groupsOfAli).contains(
            SecurityGroup("CAVE", "Access to Cave"),
            SecurityGroup("DESERT", "Access to Desert"),
            SecurityGroup("HOUSE", "Access to House")
        )

    }

}