package com.qualidify.service.impl.security

import com.qualidify.model.entities.SecurityGroup
import com.qualidify.model.entities.SecurityRole

object SecurityEntityFactory {

    const val GROUP = "Group"
    const val GROUP_DESCRIPTION = "Group description"
    const val GROUP_1 = "Group 1"
    const val GROUP_2 = "Group 2"
    const val GROUP_3 = "Group 3"
    const val ROLE = "Role"
    const val ROLE_DESCRIPTION = "Role Description"
    const val ROLE_1 = "Role 1"
    const val ROLE_2 = "Role 2"
    const val ROLE_3 = "Role 3"

    val securityGroup: SecurityGroup
        get() = SecurityGroup(GROUP, GROUP_DESCRIPTION)

    fun securityGroupWithRoles(vararg roleIdentiers: Int): SecurityGroup {
        return securityGroup.apply { roles = getSecurityRoles(*roleIdentiers) }
    }

    fun getSecurityGroup(i: Int): SecurityGroup {
        return when (i % 3) {
            0 -> SecurityGroup(GROUP_1, GROUP_1)
            1 -> SecurityGroup(GROUP_1, GROUP_1)
            2 -> SecurityGroup(GROUP_1, GROUP_1)
            else -> securityGroup
        }
    }

    val securityRole: SecurityRole
        get() = SecurityRole(ROLE)

    fun getSecurityRole(i: Int): SecurityRole {
        return when ((i - 1) % 3) {
            0 -> SecurityRole(ROLE_1)
            1 -> SecurityRole(ROLE_2)
            2 -> SecurityRole(ROLE_3)
            else -> securityRole
        }
    }

    fun getSecurityRoles(vararg args: Int): Set<SecurityRole> {
        return args.asSequence().map { getSecurityRole(it) }.toSet()

    }


}