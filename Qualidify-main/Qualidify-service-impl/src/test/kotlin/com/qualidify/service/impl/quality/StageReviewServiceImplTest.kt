package com.qualidify.service.impl.quality

import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.projects.TaskService
import com.qualidify.service.quality.ReviewState
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

/**
 * Unit test for the implementation of the [StageReviewService].
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class StageReviewServiceImplTest {

    private val caseInstanceService = mockk<CaseInstanceService>()
    private val planItemService = mockk<PlanItemService>()
    private val taskService = mockk<TaskService>()

    private val fixture = StageReviewServiceImpl(caseInstanceService, planItemService, taskService)

    @AfterEach
    fun checkTest() {
        confirmVerified(caseInstanceService, planItemService, taskService)
        checkUnnecessaryStub(caseInstanceService, planItemService, taskService)
    }

    @Test
    fun `Test start review cascade with stage`() {
        prepareReviewCascadeStubbing()

        val stageInstance = TestEntities.stageInstanceEntity()

        fixture.startReview(stageInstance)

        verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
        verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }
        verify(exactly = 1) { planItemService.getPlanItemsByStageAndDefinition(any()) }
        verify(exactly = 1) { planItemService.completeGenericEventListener(any()) }
        verify(exactly = 7) { planItemService.setPlanItemVariable(any(), any(), any()) }
        verify { taskService wasNot called }

    }

    @ParameterizedTest
    @MethodSource("getPlanItemTypes")
    fun `Test with different PlanItemTypes`(type: String) {
        val planItem = DtoFactory.planItemDto(type = PlanItemType.getByName(type))

        when (planItem.type) {
            PlanItemType.GENERIC_EVENT_LISTENER, PlanItemType.SIGNAL_EVENT_LISTENER, PlanItemType.TIMER_EVENT_LISTENER,
            PlanItemType.USER_EVENT_LISTENER, PlanItemType.VARIABLE_EVENT_LISTENER,
            -> {
                val exception = assertFailsWith<StageReviewServiceException> { fixture.startReview(planItem) }
                assertThat(exception.message).isEqualTo(
                    "Dto with id '${planItem.id}', type '${planItem.type}' is not a stage but an event listener"
                )
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            PlanItemType.HUMAN_TASK, PlanItemType.SERVICE_TASK, PlanItemType.HTTP_SERVICE_TASK, PlanItemType.DECISION_TASK,
            PlanItemType.EXTERNAL_WORKER_TASK, PlanItemType.CASE_TASK, PlanItemType.CASE_PAGE_TASK, PlanItemType.PROCESS_TASK,
            -> {
                val exception = assertFailsWith<StageReviewServiceException> { fixture.startReview(planItem) }
                assertThat(exception.message).isEqualTo(
                    "Dto with id '${planItem.id}', type '${planItem.type}' is not a stage but a task"
                )
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            PlanItemType.MILESTONE, PlanItemType.PLAN_FRAGMENT,
            -> {
                val exception = assertFailsWith<StageReviewServiceException> { fixture.startReview(planItem) }
                assertThat(exception.message).isEqualTo(
                    "Dto with id '${planItem.id}', type '${planItem.type}' is not a stage but a milestone or plan fragment"
                )
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            PlanItemType.STAGE,
            -> {
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            else -> {
                fail("Plan item type '$type' is not implemented in the TaskReviewService and/or its tests.")
            }
        }
    }

    @ParameterizedTest
    @MethodSource("getReviewStates")
    fun `Test review cascade with different review states`(state: String?) {
        val planItemInstance = TestEntities.planItemInstanceEntity()

        when (state) {
            ReviewState.REVIEW_NOT_REQUIRED.state, ReviewState.APPROVED.state, ReviewState.REJECTED.state,
            ReviewState.IN_REVIEW.state, ReviewState.CORRECTED.state,
            -> {
                // minimal stubbing required to reach code that is tested
                every {
                    planItemService.getPlanItemDtoOrNull(any())
                } returns DtoFactory.planItemDto(
                    type = PlanItemType.STAGE,
                    reviewState = ReviewState.getByName(state)
                )

                fixture.startReview(planItemInstance)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            ReviewState.REVIEW_REQUIRED.state, ReviewState.UNDEFINED.state, null,
            -> {
                // complete stubbing required for happy path
                prepareReviewCascadeStubbing()

                fixture.startReview(planItemInstance)

                // basically follow the happy path verifications of the basic test above
                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
                verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
                verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }
                verify(exactly = 1) { planItemService.getPlanItemsByStageAndDefinition(any()) }
                verify(exactly = 1) { planItemService.completeGenericEventListener(any()) }
                verify(exactly = 7) { planItemService.setPlanItemVariable(any(), any(), any()) }
                verify { taskService wasNot called }

            }

            else -> {
                fail("Review state '$state' is not implemented in the StageReviewService and/or its tests.")
            }
        }

    }

    @ParameterizedTest
    @MethodSource("getReviewStatesNonNull")
    fun `Test approve review with different review states`(state: String) {
        val reviewTaskDto = DtoFactory.taskDto(
            id = "reviewTaskId",
            planItemInstanceId = "reviewTaskPlanItemId",
            reviewState = ReviewState.getByName(state),
            qualityLinkId = "regularStagePlanItemId"
        )
        val principal = "testUser"

        every {
            planItemService.getHistoricPlanItemDto(reviewTaskDto.qualityLinkId!!)
        } returns DtoFactory.planItemDto(
            id = "regularTaskPlanItemId",
            isQualityShell = false,
            reviewState = ReviewState.getByName(state),
        )

        when (state) {
            ReviewState.IN_REVIEW.state,  // happy path
            -> {

                every {
                    planItemService.getPlanItemDtoOrNull("regularStagePlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "regularStagePlanItemId",
                    qualityLinkId = "reviewTaskPlanItemId",
                    type = PlanItemType.STAGE
                )

                val regularStageReviewState = slot<String>()
                every {
                    planItemService.setPlanItemVariable(
                        "regularStagePlanItemId",
                        "reviewState",
                        capture(regularStageReviewState)
                    )
                } just Runs

                val reviewTaskReviewState = slot<String>()
                every {
                    planItemService.setPlanItemVariable(
                        "reviewTaskPlanItemId",
                        "reviewState",
                        capture(reviewTaskReviewState)
                    )
                } just Runs

                every { taskService.completeTask(any()) } just Runs

                fixture.approveReview(reviewTaskDto, principal)

                assertEquals(ReviewState.APPROVED, ReviewState.getByName(regularStageReviewState.captured))
                assertEquals(ReviewState.APPROVED, ReviewState.getByName(reviewTaskReviewState.captured))

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
                verify(exactly = 1) { planItemService.getHistoricPlanItemDto(any()) }
                verify(exactly = 2) { planItemService.setPlanItemVariable(any(), any(), any()) }
                verify(exactly = 1) { taskService.completeTask(any()) }
                verify { caseInstanceService wasNot called }

            }

            else -> {  // all other states are not allowed
                val exception =
                    assertFailsWith<StageReviewServiceException> { fixture.approveReview(reviewTaskDto, principal) }

                assertThat(exception.message).isEqualTo(
                    "Unsupported review state '${reviewTaskDto.reviewState.state}' for stage review approval, " +
                            "the only supported state is '${ReviewState.IN_REVIEW.state}'."
                )

                verify(exactly = 1) { planItemService.getHistoricPlanItemDto(any()) }
                verify { taskService wasNot called }
                verify { caseInstanceService wasNot called }
            }
        }
    }

    private fun prepareReviewCascadeStubbing() {

        every {
            planItemService.getPlanItemDtoOrNull(any())
        } returns DtoFactory.planItemDto(type = PlanItemType.STAGE)


        every {
            caseInstanceService.getCaseInstanceDtoById("qualityShellId")
        } returns DtoFactory.caseInstanceDto(
            id = "qualityShellId",
            isQualityShell = true,
            qualityShellId = "qualityShellId",
        )

        every {
            planItemService.getEventListenersByCaseInstanceId("qualityShellId")
        } returns mutableListOf(
            DtoFactory.planItemDto(
                name = "Start stage review",
                type = PlanItemType.GENERIC_EVENT_LISTENER,
                state = PlanItemState.AVAILABLE,
                isQualityShell = true,
            )
        )

        every {
            planItemService.getPlanItemsByStageAndDefinition(any())
        } returns mutableListOf(
            DtoFactory.planItemDto(
                name = "Review stage",
                state = PlanItemState.AVAILABLE,
                isQualityShell = true,
            )
        )

        every {
            planItemService.completeGenericEventListener(any())
        } just Runs

        every {
            planItemService.setPlanItemVariable(any(), any(), any())
        } just Runs

    }

    companion object {

        @JvmStatic
        private fun getPlanItemTypes(): Set<String> = PlanItemType.values().map { it.type }.toSet()

        @JvmStatic
        private fun getReviewStatesNonNull(): Set<String> = ReviewState.values().map { it.state }.toSet()

        @JvmStatic
        private fun getReviewStates(): Set<String?> = getReviewStatesNonNull().plus(null)

    }

}