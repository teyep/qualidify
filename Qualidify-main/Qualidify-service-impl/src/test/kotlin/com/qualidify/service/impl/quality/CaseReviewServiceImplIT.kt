package com.qualidify.service.impl.quality

import com.qualidify.core.query.SearchQuery
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.CaseReviewService
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.StageReviewService
import com.qualidify.service.quality.TaskReviewService
import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional
open class CaseReviewServiceImplIT(
    @Autowired private val caseInstanceService: CaseInstanceService,
    @Autowired private val planItemService: PlanItemService,
    @Autowired private val projectTemplateService: ProjectTemplateService,
    @Autowired private val projectInstanceService: ProjectInstanceService,
    @Autowired private val taskService: TaskService,
    @Autowired private val taskReviewService: TaskReviewService,
    @Autowired private val caseReviewService: CaseReviewService,
    @Autowired private val stageReviewService: StageReviewService,
    private val flowableTaskService: CmmnTaskService,
    private val cmmnRuntimeService: CmmnRuntimeService,
) {

    @Test
    @CmmnDeployment(
        resources =
        ["dev-resources/cases/employeeOnboarding.cmmn.xml", "dev-resources/cases/cmmnQualityShell.cmmn.xml"]
    )
    fun `Test starting of case review tasks when completing regular cases`() {

        // start employee onboarding via qualidify service
        val employeeOnboardingTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(constraints = mapOf("cmmnModel" to "employeeOnboarding"))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(employeeOnboardingTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))

        assertAll(
            "Start of project",
            { assertEquals(2, findCaseInstances().size, "All stages") },
            { assertEquals(1, findCaseInstances(quality = false).size, "Regular cases") },
            { assertEquals(1, findCaseInstances(quality = true).size, "Quality cases") },
            { assertEquals(0, findTasks(quality = true, onlyCaseReview = true).size, "Quality case review tasks") },
        )

        // complete all regular tasks
        var regularTasks = findTasks(quality = false, planItemState = PlanItemState.ACTIVE)
        while (regularTasks.size > 0) {
            regularTasks.forEach { task ->
                flowableTaskService.complete(task.id)
            }
            regularTasks = findTasks(quality = false)
        }

        // finish the regular case
        val case = findCaseInstances(quality = false).single { it.name == "Employee Onboarding" }
        cmmnRuntimeService.completeCaseInstance(case.id)

        assertAll(
            "After finishing the all active regular tasks",
            { assertEquals(1, findCaseInstances().size, "All cases") },
            { assertEquals(0, findCaseInstances(quality = false).size, "Regular cases") },
            { assertEquals(1, findCaseInstances(quality = true).size, "Quality cases") },
            { assertEquals(1, findTasks(quality = true, onlyCaseReview = true).size, "Quality case review tasks") },
        )

        val caseReviewTaskDto = findTasks(quality = true).single { it.name.startsWith("Review for case") }
        caseReviewService.approveReview(caseReviewTaskDto, "testUser")

        assertAll(
            "After finishing the case review tasks",
            { assertEquals(1, findCaseInstances().size, "All cases") },
            { assertEquals(0, findCaseInstances(quality = false).size, "Regular cases") },
            { assertEquals(1, findCaseInstances(quality = true).size, "Quality cases") },
            { assertEquals(0, findTasks(quality = true, onlyCaseReview = true).size, "Quality case review tasks") },
        )

    }

    @Disabled("Rebuild test after QUAL-507")
    @Test
    @CmmnDeployment(
        resources =
        ["dev-resources/cases/smallRequirement.cmmn.xml", "dev-resources/cases/cmmnQualityShell.cmmn.xml"]
    )
    fun `Test updating local variable of regular cases in history`() {

        // start small requirement via qualidify service
        val employeeOnboardingTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(constraints = mapOf("cmmnModel" to "smallRequirement"))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(employeeOnboardingTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))

        // finish task and approve review task
        val regularTaskDto = findTasks().single { it.name.equals("Input requirement", ignoreCase = true) }
        taskService.completeOrResolveTask(regularTaskDto, "testUser")

        val reviewTaskDto =
            findTasks().single { it.name.equals("Review for task: '${regularTaskDto.name}'", ignoreCase = true) }
        taskReviewService.approveReview(reviewTaskDto, "testUser")

        // assert case is closed and variables are only historic (not runtime)
        val caseReviewTaskDto = findTasks().single { it.name.startsWith("Review for case") }
        val exception = assertFailsWith<FlowableObjectNotFoundException> {
//            stageReviewService.updateReviewState(caseReviewTaskDto, ReviewState.APPROVED)
        }
        assertThat(exception.message).isEqualTo(
            "No case instance found for id ${caseReviewTaskDto.qualityLinkId}"
        )
        assertEquals(
            ReviewState.APPROVED.state,
            planItemService.getPlanItemVariable(caseReviewTaskDto.planItemInstanceId, "reviewState"),
            "The reviewState of the review task plan item is updated to 'approved'."
        )
        assertEquals(
            null,
            caseInstanceService.getCaseVariable(caseReviewTaskDto.qualityLinkId!!, "reviewState"),
            "The reviewState of the regular plan item cannot be found in the runtime tables (because the case is completed)."
        )
        assertEquals(
            ReviewState.IN_REVIEW.state,
            caseInstanceService.getHistoricCaseVariable(
                caseReviewTaskDto.qualityLinkId!!,
                "reviewState"
            )
                .value as? String,
            "It can be found in the historic tables, but is not updated to 'approved'."
        )

        // approve stage review task
        caseReviewService.approveReview(caseReviewTaskDto, "testUser")

        // assert historic variable is updated
        assertEquals(
            ReviewState.APPROVED.state,
            caseInstanceService.getHistoricCaseVariable(
                caseReviewTaskDto.qualityLinkId!!,
                "reviewState"
            )
                .value as? String,
            "After approving the review task, the historic variable is also updated to 'approved'."
        )

    }

    private fun findCaseInstances(
        quality: Boolean? = null,
    ): Collection<CaseInstanceDto> {
        val caseInstances = caseInstanceService.findByQuery(SearchQuery<CaseInstanceDto>()).toMutableList()
        if (quality != null) caseInstances.removeIf { it.isQualityShell != quality }
        return caseInstances
    }

    private fun findPlanItems(
        quality: Boolean? = null,
        state: PlanItemState? = null,
        type: PlanItemType? = null,
    ): Collection<PlanItemDto> {
        val planItems = planItemService.findByQuery(SearchQuery<PlanItemDto>()).toMutableList()
        if (type != null) planItems.removeIf { it.type != type }
        if (quality != null) planItems.removeIf { it.isQualityShell != quality }
        if (state != null) planItems.removeIf { it.state != state }
        return planItems
    }

    private fun findTasks(
        quality: Boolean? = null,
        planItemState: PlanItemState? = null,
        onlyCaseReview: Boolean? = null,
        stageId: String? = null,
    ): MutableList<TaskDto> {
        val planItems = findPlanItems(quality, planItemState).toMutableList()
        planItems.removeIf { it.type != PlanItemType.HUMAN_TASK }

        val tasks = taskService.findByQuery(SearchQuery<TaskDto>()).toMutableList()
        tasks.removeIf { taskDto ->
            taskDto.planItemInstanceId !in planItems.map { it.id }.toHashSet()
        }

        if (onlyCaseReview == true) tasks.removeIf { !it.name.lowercase().contains("review for case: ") }
        if (stageId != null) tasks.removeIf { it.stage != stageId }

        return tasks
    }

}