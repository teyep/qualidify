package com.qualidify.service.impl.quality

import com.qualidify.core.query.SearchQuery
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.StageReviewService
import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional
open class StageReviewServiceImplIT(
    @Autowired private val planItemService: PlanItemService,
    @Autowired private val projectTemplateService: ProjectTemplateService,
    @Autowired private val projectInstanceService: ProjectInstanceService,
    @Autowired private val taskService: TaskService,
    @Autowired private val stageReviewService: StageReviewService,
    private val flowableTaskService: CmmnTaskService,
) {

    @Test
    @CmmnDeployment(
        resources =
        ["dev-resources/cases/employeeOnboarding.cmmn.xml", "dev-resources/cases/cmmnQualityShell.cmmn.xml"]
    )
    fun `Test starting of stage review tasks when completing regular stages`() {

        // start employee onboarding via qualidify service
        val employeeOnboardingTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(constraints = mapOf("cmmnModel" to "employeeOnboarding"))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(employeeOnboardingTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))

        assertAll(
            "Start of project",
            { assertEquals(4, findPlanItems(type = PlanItemType.STAGE).size, "All stages") },
            { assertEquals(2, findPlanItems(quality = false, type = PlanItemType.STAGE).size, "Regular stages") },
            { assertEquals(2, findPlanItems(quality = true, type = PlanItemType.STAGE).size, "Quality stages") },
            { assertEquals(0, findTasks(quality = true, excludeTaskReview = true).size, "Quality stage review tasks") },
        )

        val firstStageId =
            findPlanItems(quality = false, type = PlanItemType.STAGE).single { it.name == "Prior to starting" }.id
        val secondStageId =
            findPlanItems(quality = false, type = PlanItemType.STAGE).single { it.name == "After starting" }.id

        // finish all tasks from the first stage (4 tasks)
        var firstStageTasks = findTasks(quality = false, planItemState = PlanItemState.ACTIVE, stageId = firstStageId)
        while (firstStageTasks.size > 0) {
            firstStageTasks.forEach { task ->
                if (task.stage == firstStageId) {
                    flowableTaskService.complete(task.id)
                }
            }
            firstStageTasks = findTasks(quality = false, stageId = firstStageId)
        }

        assertAll(
            "After finishing the first stage tasks",
            { assertEquals(4, findPlanItems(type = PlanItemType.STAGE).size, "All stages") },
            { assertEquals(2, findPlanItems(quality = false, type = PlanItemType.STAGE).size, "Regular stages") },
            { assertEquals(2, findPlanItems(quality = true, type = PlanItemType.STAGE).size, "Quality stages") },
            { assertEquals(1, findTasks(quality = true, excludeTaskReview = true).size, "Quality stage review tasks") },
        )

        // finish all tasks from the second stage (2 tasks)
        var secondStageTasks = findTasks(quality = false, planItemState = PlanItemState.ACTIVE, stageId = secondStageId)
        while (secondStageTasks.size > 0) {
            secondStageTasks.forEach { task ->
                if (task.stage == secondStageId) {
                    flowableTaskService.complete(task.id)
                }
            }
            secondStageTasks = findTasks(quality = false, stageId = secondStageId)
        }

        assertAll(
            "After finishing the second stage tasks",
            { assertEquals(4, findPlanItems(type = PlanItemType.STAGE).size, "All stages") },
            { assertEquals(2, findPlanItems(quality = false, type = PlanItemType.STAGE).size, "Regular stages") },
            { assertEquals(2, findPlanItems(quality = true, type = PlanItemType.STAGE).size, "Quality stages") },
            { assertEquals(2, findTasks(quality = true, excludeTaskReview = true).size, "Quality stage review tasks") },
        )

        // finish the stage review tasks
        var qualityReviewTasks = findTasks(quality = true).size
        val stageReviewTasks = findTasks(quality = true, excludeTaskReview = true)
        stageReviewTasks.forEach { flowableTaskService.complete(it.id).also { qualityReviewTasks-- } }

        assertEquals(
            qualityReviewTasks,
            findTasks(quality = true).size,
            "No addition quality review tasks should have been created"
        )

    }

    @Disabled("Rebuild test after QUAL-507")
    @Test
    @CmmnDeployment(
        resources =
        ["dev-resources/cases/smallRequirement.cmmn.xml", "dev-resources/cases/cmmnQualityShell.cmmn.xml"]
    )
    fun `Test updating local variable of regular stages in history`() {

        // start small requirement via qualidify service
        val employeeOnboardingTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(constraints = mapOf("cmmnModel" to "smallRequirement"))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(employeeOnboardingTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))

        // finish task and approve review task
        val regularTaskDto = findTasks().single { it.name.equals("Input requirement", ignoreCase = true) }
        flowableTaskService.complete(regularTaskDto.id)

        val reviewTaskDto =
            findTasks().single { it.name.equals("Review for task: '${regularTaskDto.name}'", ignoreCase = true) }
        stageReviewService.approveReview(reviewTaskDto, "testUser")

        // assert stage is closed and variables are only historic (not runtime)
        val stageReviewTaskDto = findTasks().single { it.name == "Review for stage: 'Small Requirement Stage'" }
        val exception = assertFailsWith<FlowableObjectNotFoundException> {
//            stageReviewService.updateReviewState(stageReviewTaskDto, ReviewState.APPROVED)
        }
        assertThat(exception.message).isEqualTo(
            "No plan item instance found for id ${stageReviewTaskDto.qualityLinkId}"
        )
        assertEquals(
            ReviewState.APPROVED.state,
            planItemService.getPlanItemVariable(stageReviewTaskDto.planItemInstanceId, "reviewState"),
            "The reviewState of the review task plan item is updated to 'approved'."
        )
        assertEquals(
            null,
            planItemService.getPlanItemVariable(stageReviewTaskDto.qualityLinkId!!, "reviewState"),
            "The reviewState of the regular plan item cannot be found in the runtime tables (because the case is completed)."
        )
        assertEquals(
            ReviewState.IN_REVIEW.state,
            planItemService.getHistoricPlanItemVariable(
                stageReviewTaskDto.qualityLinkId!!,
                "reviewState"
            ).value as? String,
            "It can be found in the historic tables, but is not updated to 'approved'."
        )

        // approve stage review task
        stageReviewService.approveReview(stageReviewTaskDto, "testUser")

        // assert historic variable is updated
        assertEquals(
            ReviewState.APPROVED.state,
            planItemService.getHistoricPlanItemVariable(
                stageReviewTaskDto.qualityLinkId!!,
                "reviewState"
            ).value as? String,
            "After approving the review task, the historic variable is also updated to 'approved'."
        )

    }

    private fun findPlanItems(
        quality: Boolean? = null,
        state: PlanItemState? = null,
        type: PlanItemType? = null,
    ): Collection<PlanItemDto> {
        val planItems = planItemService.findByQuery(SearchQuery<PlanItemDto>()).toMutableList()
        if (type != null) planItems.removeIf { it.type != type }
        if (quality != null) planItems.removeIf { it.isQualityShell != quality }
        if (state != null) planItems.removeIf { it.state != state }
        return planItems
    }

    private fun findTasks(
        quality: Boolean? = null,
        planItemState: PlanItemState? = null,
        excludeTaskReview: Boolean? = null,
        stageId: String? = null,
    ): MutableList<TaskDto> {
        val planItems = findPlanItems(quality, planItemState).toMutableList()
        planItems.removeIf { it.type != PlanItemType.HUMAN_TASK }

        val tasks = taskService.findByQuery(SearchQuery<TaskDto>()).toMutableList()
        tasks.removeIf { taskDto ->
            taskDto.planItemInstanceId !in planItems.map { it.id }.toHashSet()
        }

        if (excludeTaskReview == true) tasks.removeIf { !it.name.lowercase().contains("review for stage: ") }
        if (stageId != null) tasks.removeIf { it.stage != stageId }

        return tasks
    }

}