package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.impl.process.mappers.TaskDtoMapper
import com.qualidify.service.impl.process.repositories.BpmnTaskRepository
import com.qualidify.service.impl.projects.CmmnTaskRepository
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.TaskReviewService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.engine.RuntimeService
import org.flowable.engine.TaskService
import org.flowable.engine.test.Deployment
import org.flowable.spring.impl.test.FlowableSpringExtension
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 * Integration test for the [BpmnTaskServiceImpl] to test counting and filtering of dto properties that
 * are not implemented in flowable queries
 *
 * @author Menno Tol
 * @since 1.0
 */
@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(FlowableSpringExtension::class),
    ExtendWith(SpringExtension::class),
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional
@Disabled(
    "Disabled because 1, quality shell is not implemented for bpmn tasks and " +
            "2, the case task view only works with tasks of type 'cmmn'"
)  // TODO: QUAL-556
open class BpmnTaskServiceImplIT(
    private val runtimeService: RuntimeService,
    private val flowableBpmnTaskService: TaskService,
    private val flowableCmmnTaskService: CmmnTaskService,
    @Autowired private val bpmnTaskRepository: BpmnTaskRepository,
    @Autowired private val cmmnTaskRepository: CmmnTaskRepository,
    @Autowired private val taskDtoMapper: TaskDtoMapper,
    @Autowired private val projectTemplateService: ProjectTemplateService,
    @Autowired private val projectInstanceService: ProjectInstanceService,
    @Autowired private val taskReviewService: TaskReviewService,
    @Autowired private val planItemService: PlanItemService,
) {

    private val qualidifyCmmnTaskService = TaskServiceImpl(
        projectInstanceService,
        cmmnTaskRepository,
        flowableCmmnTaskService,
        taskDtoMapper,
        taskReviewService,
        planItemService,
    )
    private val qualidifyProcessTaskService = BpmnTaskServiceImpl(bpmnTaskRepository)

    /**
     * Flowable makes no distinction between cmmn and bpmn tasks. The ProcessTaskServiceImpl manually adds the bpmn
     * type to the dto and filters based on the type tag. Correct filtering is tested here.
     */
    @Test
    @Deployment(resources = ["dev-resources/processes/simpleTask.bpmn20.xml"])
    @CmmnDeployment(
        resources = [
            "dev-resources/cases/employeeOnboarding.cmmn.xml",
            "dev-resources/cases/cmmnQualityShell.cmmn.xml",
        ]
    )
    fun `Test count and search query of bpmn tasks by filtering`() {

        // start employee onboarding via qualidify service (required for projectInstance wrapper to be available)
        val employeeOnboardingTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(constraints = mapOf("cmmnModel" to "employeeOnboarding"))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(employeeOnboardingTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))

        // start runtime task via flowable service
        runtimeService.startProcessInstanceByKey("simpleTask")

        // bypassing qualidify service
        var flowTasks = flowableBpmnTaskService.createTaskQuery().list()
        assertEquals(4, flowTasks.size)  // flowable makes no distinction between tasks
        flowTasks = flowableBpmnTaskService.createTaskQuery().scopeType("cmmn").list()
        assertEquals(3, flowTasks.size)  // flowable can filter based on scope

        // answer should be 1 instead of 5 if process tasks have "bpmn" scope
        // however, this does not work for "bpmn" (since there is no scope applied)
        flowTasks = flowableBpmnTaskService.createTaskQuery().scopeType("bpmn").list()
        assertEquals(0, flowTasks.size)

        // using cmmn tasks
        var cmmnFindQuery = qualidifyCmmnTaskService.findByQuery(SearchQuery<TaskDto>())
        var cmmnCountQuery = qualidifyCmmnTaskService.countByQuery(CountQuery<TaskDto>())
        assertEquals(cmmnFindQuery.size, cmmnCountQuery.count.toInt())  // 4

        // using cmmn tasks setting the type filter
        cmmnFindQuery = qualidifyCmmnTaskService
            .findByQuery(SearchQuery<TaskDto>(constraints = mapOf("type" to "bpmn")))
        cmmnCountQuery = qualidifyCmmnTaskService
            .countByQuery(CountQuery<TaskDto>(constraints = mapOf("type" to "bpmn")))
        assertEquals(cmmnFindQuery.size, cmmnCountQuery.count.toInt())  // 0

        //using bpmn tasks
        var bpmnFindQuery = qualidifyProcessTaskService.findByQuery(SearchQuery<TaskDto>())
        var bpmnCountQuery = qualidifyProcessTaskService.countByQuery(CountQuery<TaskDto>())
        assertEquals(bpmnFindQuery.size, bpmnCountQuery.count.toInt())  // 1

        //using bpmn tasks setting the type filter
        bpmnFindQuery = qualidifyProcessTaskService
            .findByQuery(SearchQuery<TaskDto>(constraints = mapOf("type" to "cmmn")))
        bpmnCountQuery = qualidifyProcessTaskService
            .countByQuery(CountQuery<TaskDto>(constraints = mapOf("type" to "cmmn")))
        assertEquals(bpmnFindQuery.size, bpmnCountQuery.count.toInt())  // 1
    }

}