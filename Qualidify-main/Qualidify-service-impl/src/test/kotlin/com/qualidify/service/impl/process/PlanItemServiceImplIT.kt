package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import java.util.*
import kotlin.test.assertTrue

/**
 * Integration test for the [PlanItemServiceImpl]
 *
 * @author Menno Tol
 * @since 1.0
 */
@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class),
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional
open class PlanItemServiceImplIT(
    private val cmmnRuntimeService: CmmnRuntimeService,
    @Autowired private val projectTemplateService: ProjectTemplateService,
    @Autowired private val projectInstanceService: ProjectInstanceService,
    @Autowired private val planItemService: PlanItemService
) {

    @Test
    @CmmnDeployment(resources = [
        "dev-resources/cases/basicRequirement.cmmn.xml",
        "dev-resources/cases/cmmnQualityShell.cmmn.xml",
    ])
    fun `Find and start Plan Item by name`() {

        // start employee onboarding via qualidify service (required for projectInstance wrapper to be available)
        val basicRequirementTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(constraints = mapOf("cmmnModel" to "basicRequirement"))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(basicRequirementTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))
        assertThat(planItemService.countByQuery(CountQuery<PlanItemDto>()).count)
            .`as`("The number of plan items found is equal to 16").isEqualTo(16)
        // 7 for basicRequirement, 9 for the qualityShell

        // find the plan item
        val planItemName = "Add User Requirement"
        assertThat(planItemService.findByQuery(SearchQuery<PlanItemDto>(constraints = mapOf("name" to planItemName))).size)
            .`as`("One plan item with name '$planItemName' should be present").isEqualTo(1)

        val planItemDto =
            planItemService.findByQuery(SearchQuery<PlanItemDto>(constraints = mapOf("name" to planItemName))).single()
        assertThat(planItemDto.name).`as`("Plan item with name $planItemName should be present")
            .isEqualTo(planItemName)
        assertThat(planItemDto.type).`as`("The type should be a 'humantask'")
            .isEqualTo(PlanItemType.HUMAN_TASK)
        assertThat(
            cmmnRuntimeService.createPlanItemInstanceQuery().planItemInstanceName(planItemName)
                .singleResult().state
        ).isEqualTo("enabled")
            .`as`("The plan item state with name '$planItemName' should be 'enabled")

        // start the plan item
        planItemService.startPlanItem(planItemDto)
        assertThat(
            cmmnRuntimeService.createPlanItemInstanceQuery().planItemInstanceName(planItemName)
                .singleResult().state
        ).isEqualTo("active")
            .`as`("The plan item state with name '$planItemName' should be 'active")

        // load a planItemDto and change the id
        val planItemDto2 = planItemService.findByQuery(
            SearchQuery<PlanItemDto>(constraints = mapOf("name" to planItemName))
        ).single().copy(id = UUID.randomUUID().toString())

        // start the plan item
        assertThatThrownBy { planItemService.startPlanItem(planItemDto2) }
            .isInstanceOf(FlowableObjectNotFoundException::class.java)
            .hasMessage("Cannot find plan item instance for id ${planItemDto2.id}")

    }

}