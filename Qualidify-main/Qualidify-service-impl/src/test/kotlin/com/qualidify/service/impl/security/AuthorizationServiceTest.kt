package com.qualidify.service.impl.security

import com.google.common.collect.Sets
import com.qualidify.model.entities.SecurityGroup
import com.qualidify.model.entities.SecurityRole
import com.qualidify.model.entities.User
import com.qualidify.model.entities.User.Companion.builder
import com.qualidify.model.entities.UserId
import com.qualidify.service.impl.user.UserDataRepository
import com.qualidify.service.user.exceptions.UserNotFoundException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(SpringExtension::class)
class AuthorizationServiceTest {

    @Mock
    lateinit var userRepository: UserDataRepository

    @Mock
    lateinit var groupRepository: SecurityGroupRepository

    @InjectMocks
    lateinit var fixture: UserAuthorizationServiceImpl

    @Test
    fun testFindUserSuccessfully() {
        val user = createUser("mister-x").apply {
            groups = Sets.newHashSet(createGroupWithRoles())
        }
        `when`(userRepository.findByUsernameIgnoreCase("mister-x")).thenReturn(Optional.of(user))

        val userDetails = fixture.loadUserByUsername(user.username)

        assertThat(userDetails.username).`as`("Username must be {}", user.username).isEqualTo(user.username)
        assertThat(userDetails.password).`as`("Password must be {}", user.password).isEqualTo(user.password)
        assertThat(userDetails.authorities).`as`("Authorities must be available").isNotEmpty
        assertThat(userDetails.authorities).`as`("Roles 1 and 2 must be in authorities").hasSize(2)

        verify(userRepository, times(1)).findByUsernameIgnoreCase("mister-x")

        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository)
    }

    @Test
    fun testFindUserUnSuccessfully() {
        `when`(userRepository.findByUsernameIgnoreCase("UNKNOWN")).thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.loadUserByUsername("UNKNOWN")
        }.isInstanceOf(UserNotFoundException::class.java)
            .hasMessage("User with UNKNOWN not found!")

        verify(userRepository, times(1)).findByUsernameIgnoreCase("UNKNOWN")

        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository)
    }

    @Test
    fun testFindUserByIdSuccessfully() {
        val user = createUser("mister-x").apply {
            groups = Sets.newHashSet(createGroupWithRoles())
        }
        val userId = user.userId

        `when`(userRepository.findById(UserId(userId))).thenReturn(Optional.of(user))

        val userDetails = fixture.loadUserById(userId)

        assertThat(userDetails.username).`as`("Username must be {}", user.username).isEqualTo(user.username)
        assertThat(userDetails.password).`as`("Password must be {}", user.password).isEqualTo(user.password)
        assertThat(userDetails.authorities).`as`("Authorities must be available").isNotEmpty
        assertThat(userDetails.authorities).`as`("Roles 1 and 2 must be in authorities").hasSize(2)

        verify(userRepository, times(1)).findById(UserId(userId))

        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository)
    }

    @Test
    fun testFindUserByIdUnSuccessfully() {
        val userId = UserId(UUID.randomUUID())
        `when`(userRepository.findById(userId)).thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.loadUserById(userId.unbox())
        }.isInstanceOf(UserNotFoundException::class.java)
            .hasMessage("User with id ${userId.unbox()} not found!")

        verify(userRepository, times(1)).findById(userId)
        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository)
    }

    @Test
    fun testFindUsersByIdsSuccessfully() {
        val userX = createUser("mister-x").apply {
            groups = Sets.newHashSet(createGroupWithRoles())
        }
        val userY = createUser("mistress-y").apply {
            groups = Sets.newHashSet(createGroupWithRoles())
        }
        val userZ = createUser("zzz").apply {
            groups = Sets.newHashSet(createGroupWithRoles())
        }
        val userIds = listOf(userX.userId, userY.userId, userZ.userId, UUID.randomUUID())

        `when`(userRepository.findAllById(userIds.map { UserId(it) }.toSet()))
            .thenReturn(listOf(userX, userY, userZ))

        val userDetails = fixture.loadUsersByIds(userIds)

        assertThat(userDetails).`as`("Userdetails should contain three users").hasSize(3)
            .usingElementComparator { t1, t2 -> (t1.username.equals(t2.username)).compareTo(true) }

        verify(userRepository, times(1)).findAllById(userIds.map { UserId(it) }.toSet())
        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository)

    }

    private fun createUser(username: String): User {
        return builder().withUsername(username)
            .withPassword("secret")
            .withEnabled(true)
            .withUserId(UUID.randomUUID())
            .build()
    }

    private fun createGroupWithRoles(): SecurityGroup {
        val roles = setOf(SecurityRole("Role 1"), SecurityRole("Role 2"))
        return SecurityGroup("SecurityGroup", "securitygroup description", roles)
    }
}