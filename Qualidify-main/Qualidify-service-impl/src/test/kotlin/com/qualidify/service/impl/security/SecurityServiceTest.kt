package com.qualidify.service.impl.security


import com.qualidify.model.entities.SecurityGroupId

import com.qualidify.service.impl.security.SecurityEntityFactory.GROUP
import com.qualidify.service.impl.security.SecurityEntityFactory.GROUP_DESCRIPTION
import com.qualidify.service.impl.security.SecurityEntityFactory.ROLE_1
import com.qualidify.service.impl.security.SecurityEntityFactory.ROLE_2
import com.qualidify.service.security.dto.SecurityGroupDto
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*


@ExtendWith(MockitoExtension::class)
class SecurityServiceTest {

    @Mock
    lateinit var groupRepository: SecurityGroupRepository

    @Mock
    lateinit var roleRepository: SecurityRoleRepository

    @Mock
    lateinit var groupMapper: SecurityGroupMapper

    @InjectMocks
    lateinit var fixture: SecurityServiceImpl

    @Test
    fun `Create a new SecurityGroup`() {
        val groupEntity = SecurityEntityFactory.securityGroup
        val groupDto = SecurityGroupDto(groupEntity.id.unbox(), groupEntity.description)

        `when`(groupRepository.findById(groupEntity.id)).thenReturn(Optional.empty())
        `when`(groupMapper.convertToModel(groupDto)).thenReturn(groupEntity)
        `when`(groupRepository.save(groupEntity)).thenReturn(groupEntity)

        fixture.createGroup(groupDto)

        verify(groupRepository, times(1)).findById(groupEntity.id)
        verify(groupMapper, times(1)).convertToModel(groupDto)
        verify(groupRepository, times(1)).save(groupEntity)
        verifyNoMoreInteractions(groupRepository, groupMapper)
        verifyNoInteractions(roleRepository)
    }

    @Test
    fun `Create an already existing SecurityGroup`() {
        val groupEntity = SecurityEntityFactory.securityGroup
        val groupDto = SecurityGroupDto(groupEntity.id.unbox(), groupEntity.description)

        `when`(groupRepository.findById(groupEntity.id)).thenReturn(Optional.of(groupEntity))

        assertThatThrownBy {
            fixture.createGroup(groupDto)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Securitygroup ${groupDto.name} already exists!")


        verify(groupRepository, times(1)).findById(groupEntity.id)
        verifyNoMoreInteractions(groupRepository)
        verifyNoInteractions(roleRepository, groupMapper)
    }

    @Test
    fun `Create a new SecurityGroup without name`() {
        val groupDto = SecurityGroupDto("", GROUP_DESCRIPTION)

        assertThatThrownBy {
            fixture.createGroup(groupDto)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Security Group Name should be filled.")

        verifyNoInteractions(groupRepository, roleRepository, groupMapper)
    }

    @Test
    fun `Create a new SecurityGroup without description`() {
        val groupDto = SecurityGroupDto(GROUP, "")

        assertThatThrownBy {
            fixture.createGroup(groupDto)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Security Group description should be filled.")

        verifyNoInteractions(groupRepository, roleRepository, groupMapper)
    }

    @Test
    fun `Update a SecurityGroup`() {
        val groupDto = SecurityGroupDto(GROUP, GROUP_DESCRIPTION, setOf(ROLE_1, ROLE_2))
        val groupEntity_beforeUpdate = SecurityEntityFactory.securityGroup
        val groupEntity_afterUpdate = SecurityEntityFactory.securityGroupWithRoles(1, 2)

        `when`(groupRepository.findById(SecurityGroupId(GROUP))).thenReturn(Optional.of(groupEntity_beforeUpdate))
        `when`(roleRepository.availableRoles())
            .thenReturn(
                listOf(ROLE_1, ROLE_2)
            )
        `when`(groupRepository.save(groupEntity_afterUpdate)).thenReturn(groupEntity_afterUpdate)

        fixture.update(groupDto)

        verify(groupRepository, times(1)).findById(SecurityGroupId(GROUP))
        verify(roleRepository, times(1)).availableRoles()
        verify(groupRepository, times(1)).save(groupEntity_afterUpdate)

        verifyNoMoreInteractions(groupRepository, roleRepository)
        verifyNoInteractions(groupMapper)
    }

    @Test
    fun `Update a not existing SecurityGroup`() {
        val groupDto = SecurityGroupDto(GROUP, GROUP_DESCRIPTION)

        `when`(groupRepository.findById(SecurityGroupId(GROUP))).thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.update(groupDto)
        }.isInstanceOf(SecurityGroupNotFoundException::class.java)
            .hasMessage("Cannot find the SecurityGroup $GROUP")

        verify(groupRepository, times(1)).findById(SecurityGroupId(GROUP))
        verifyNoMoreInteractions(groupRepository)
        verifyNoInteractions(roleRepository, groupMapper)
    }


}

