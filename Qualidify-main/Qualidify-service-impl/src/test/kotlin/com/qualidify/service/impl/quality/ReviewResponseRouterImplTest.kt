package com.qualidify.service.impl.quality

import com.qualidify.service.DtoFactory
import com.qualidify.service.quality.*
import io.mockk.*
import org.flowable.cmmn.api.CmmnRuntimeService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

internal class ReviewResponseRouterImplTest {

    private val taskReviewService = mockk<TaskReviewService>()
    private val stageReviewService = mockk<StageReviewService>()
    private val caseReviewService = mockk<CaseReviewService>()

    val fixture =
        ReviewResponseRouterImpl(taskReviewService, stageReviewService, caseReviewService)

    @AfterEach
    fun checkTest() {
        confirmVerified(taskReviewService, stageReviewService, caseReviewService)
        checkUnnecessaryStub(taskReviewService, stageReviewService, caseReviewService)
    }

    @ParameterizedTest
    @MethodSource("getPairs")
    fun `Test correct review response routing`(pair: Pair<ReviewResponse, QualityLinkKey>) {
        val taskDto = DtoFactory.taskDto(qualityLinkKey = pair.second)
        val principal = "principal"

        when (pair) {
            Pair(ReviewResponse.APPROVE, QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID) -> {
                every { taskReviewService.approveReview(any(), any()) } just Runs
                fixture.routeRequest(taskDto, principal, pair.first)
                verify(exactly = 1) { taskReviewService.approveReview(any(), any()) }
            }

            Pair(ReviewResponse.REJECT, QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID) -> {
                every { taskReviewService.rejectReview(any(), any()) } just Runs
                fixture.routeRequest(taskDto, principal, pair.first)
                verify(exactly = 1) { taskReviewService.rejectReview(any(), any()) }
            }

            Pair(ReviewResponse.APPROVE, QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID) -> {
                every { stageReviewService.approveReview(any(), any()) } just Runs
                fixture.routeRequest(taskDto, principal, pair.first)
                verify(exactly = 1) { stageReviewService.approveReview(any(), any()) }
            }

            Pair(ReviewResponse.REJECT, QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID) -> {
                every { stageReviewService.rejectReview(any(), any()) } just Runs
                fixture.routeRequest(taskDto, principal, pair.first)
                verify(exactly = 1) { stageReviewService.rejectReview(any(), any()) }
            }

            Pair(ReviewResponse.APPROVE, QualityLinkKey.REGULAR_CASE_ID) -> {
                every { caseReviewService.approveReview(any(), any()) } just Runs
                fixture.routeRequest(taskDto, principal, pair.first)
                verify(exactly = 1) { caseReviewService.approveReview(any(), any()) }
            }

            Pair(ReviewResponse.REJECT, QualityLinkKey.REGULAR_CASE_ID) -> {
                every { caseReviewService.rejectReview(any(), any()) } just Runs
                fixture.routeRequest(taskDto, principal, pair.first)
                verify(exactly = 1) { caseReviewService.rejectReview(any(), any()) }
            }

            else -> {
                assertThrows<ReviewResponseRouterException> { fixture.routeRequest(taskDto, principal, pair.first) }
            }
        }
    }

    companion object {

        @JvmStatic
        fun getPairs(): Set<Pair<ReviewResponse, QualityLinkKey>> {
            val set = mutableSetOf<Pair<ReviewResponse, QualityLinkKey>>()

            QualityLinkKey.values().forEach { key ->
                ReviewResponse.values().forEach { response ->
                    set += Pair(response, key)
                }
            }

            return set
        }
    }
}