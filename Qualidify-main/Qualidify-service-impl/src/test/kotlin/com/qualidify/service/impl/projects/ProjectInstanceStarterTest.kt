package com.qualidify.service.impl.projects

import com.qualidify.model.entities.ProjectInstance
import com.qualidify.model.entities.ProjectInstanceId
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.quality.ReviewStateFiller
import com.qualidify.service.impl.user.UserDataRepository
import io.mockk.*
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.runtime.CaseInstanceBuilder
import org.flowable.cmmn.engine.impl.persistence.entity.CaseInstanceEntityImpl
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import kotlin.test.assertEquals

internal class ProjectInstanceStarterTest {

    private val cmmnRuntimeService = mockk<CmmnRuntimeService>()
    private val projectInstanceRepository = mockk<ProjectInstanceRepository>()
    private val userDataRepository = mockk<UserDataRepository>()
    private val reviewStateFiller = mockk<ReviewStateFiller>()

    private val builder = spyk<CaseInstanceBuilder>()

    private val fixture = ProjectInstanceStarter(
        cmmnRuntimeService, projectInstanceRepository, userDataRepository, reviewStateFiller
    )

    private val projectTemplateDto = DtoFactory.projectTemplateDto(name = "mockProject")
    private val qualityShell = CaseInstanceEntityImpl().also {
        it.id = "qualityShellId"
        it.name = "testCmmnQualityShell"
    }
    private val regularInstance = CaseInstanceEntityImpl().also {
        it.id = "regularCaseId"
        it.name = "testCmmnRegularInstance"
    }

    @AfterEach
    fun checkTest() {
        confirmVerified(cmmnRuntimeService, projectInstanceRepository, userDataRepository, reviewStateFiller)
        checkUnnecessaryStub(cmmnRuntimeService, projectInstanceRepository, userDataRepository, reviewStateFiller)
    }

    @Test
    fun `Start project`() {

        every { cmmnRuntimeService.createCaseInstanceBuilder().caseDefinitionKey(any()) } returns builder

        every { builder.start() } returns qualityShell

        every {
            cmmnRuntimeService.createCaseInstanceQuery()
                .caseDefinitionKey(any())
                .caseInstanceParentId(any())
                .singleResult()
        } returns regularInstance

        every { cmmnRuntimeService.setVariables(any(), any()) } just Runs
        every { cmmnRuntimeService.setVariable(any(), any(), any()) } just Runs
        every { reviewStateFiller.fill(any(), any()) } just Runs

        val projectInstanceSlot = slot<ProjectInstance>()
        every {
            projectInstanceRepository.save(capture(projectInstanceSlot))
        } answers {
            it.invocation.args[0] as ProjectInstance
        }

        fixture.start(projectTemplateDto)

        assertAll(
            { assertEquals(ProjectInstanceId::class.java, projectInstanceSlot.captured.id.javaClass) },
            { assertEquals("mockProject", projectInstanceSlot.captured.name) },
            { assertEquals("qualityShellId", projectInstanceSlot.captured.qualityShellId) },
            { assertEquals("regularCaseId", projectInstanceSlot.captured.regularCaseId) },
        )

        verify(exactly = 1) { builder.start() }
        verify(exactly = 1) { cmmnRuntimeService.createCaseInstanceBuilder() }
        verify(exactly = 1) { cmmnRuntimeService.createCaseInstanceQuery() }
        verify(exactly = 1) { cmmnRuntimeService.setVariables(any(), any()) }
        verify(exactly = 1) { cmmnRuntimeService.setVariable(any(), any(), any()) }
        verify(exactly = 1) { projectInstanceRepository.save(any()) }
        verify(exactly = 1) { reviewStateFiller.fill(any(), any()) }
        verify { userDataRepository wasNot called }

    }

}