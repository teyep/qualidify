package com.qualidify.service.impl.quality

import com.qualidify.core.query.SearchQuery
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.QualityLinkKey
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional(propagation = Propagation.SUPPORTS)
@DirtiesContext
internal open class TaskReviewServiceImplIT(
    @Autowired private val planItemService: PlanItemService,
    @Autowired private val projectTemplateService: ProjectTemplateService,
    @Autowired private val projectInstanceService: ProjectInstanceService,
    @Autowired private val taskService: TaskService,
    private val flowableTaskService: CmmnTaskService,
    private val cmmnRuntimeService: CmmnRuntimeService,
) {

    @Test
    @CmmnDeployment(
        resources =
        ["dev-resources/cases/employeeOnboarding.cmmn.xml", "dev-resources/cases/cmmnQualityShell.cmmn.xml"]
    )
    fun `Test starting of review tasks when completing regular tasks`() {

        var regularTasksCompletedCounter = 0
        var qualityTasksCompletedCounter = 0

        // start employee onboarding via qualidify service
        val employeeOnboardingTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(constraints = mapOf("cmmnModel" to "employeeOnboarding"))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(employeeOnboardingTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))

        assertAll(
            "Start of project",
            { assertEquals(16, findPlanItems().size, "All plan items") },
            { assertEquals(7, findPlanItems(quality = false).size, "Regular plan items") },
            { assertEquals(9, findPlanItems(quality = true).size, "Quality plan items") },
            { assertEquals(regularTasksCompletedCounter, findTasks(quality = true).size, "Quality review tasks") },
        )

        // finish 1 task
        val regularTaskName = "Agree start date"
        val agreeStartDateTask = findTasks(quality = false, planItemState = PlanItemState.ACTIVE)
            .single { it.name == regularTaskName }
        val nextReviewTaskPlanItem = findNextReviewTaskPlanItem()

        // manually complete the task using the flowable task service
        flowableTaskService.complete(agreeStartDateTask.id).also { regularTasksCompletedCounter++ }

        assertAll(
            "After finishing 1 regular task",
            { assertEquals(18, findPlanItems().size, "All plan items") },
            { assertEquals(7, findPlanItems(quality = false).size, "Regular plan items") },
            { assertEquals(11, findPlanItems(quality = true).size, "Quality plan items") },
            { assertEquals(regularTasksCompletedCounter, findTasks(quality = true).size, "Quality review tasks") },
        )

        // find the review task and regular task id's in the local variables and check they are correct
        val agreeStartDateReviewTaskPlanItemIdVariable =
            planItemService.getPlanItemVariable(
                agreeStartDateTask.planItemInstanceId,
                QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID.key
            )
        val regularTaskPlanItemIdVariableStoredInReviewTaskPlanItem =
            planItemService.getPlanItemVariable(nextReviewTaskPlanItem.id, QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID.key)
        assertEquals(
            nextReviewTaskPlanItem.id,
            agreeStartDateReviewTaskPlanItemIdVariable,
            "The id of the review task plan item should be stored in variable 'reviewTaskPlanItemId' of the completed task"
        )
        assertEquals(
            agreeStartDateTask.planItemInstanceId,
            regularTaskPlanItemIdVariableStoredInReviewTaskPlanItem,
            "The id of the regular task plan item should be stored in variable 'regularTaskPlanItemId' of the review task"
        )

        // check if the task name has been updated correctly
        val reviewTaskPlanItemDto = planItemService.getPlanItemDtoOrNull(nextReviewTaskPlanItem.id)!!
        assertEquals("Review for task: '$regularTaskName'", reviewTaskPlanItemDto.name)

        // complete the remaining regular tasks
        var regularTasks = findTasks(quality = false, planItemState = PlanItemState.ACTIVE)
        while (regularTasks.size > 0) {
            regularTasks.forEach { task ->
                flowableTaskService.complete(task.id).also { regularTasksCompletedCounter++ }
            }
            regularTasks = findTasks(quality = false)
        }

        assertAll(
            "After finishing all regular tasks",
            { assertEquals(34, findPlanItems().size, "All plan items") },
            { assertEquals(9, findPlanItems(quality = false).size, "Regular plan items") },
            { assertEquals(25, findPlanItems(quality = true).size, "Quality plan items") },
            {
                assertEquals(
                    regularTasksCompletedCounter,
                    findTasks(quality = true, includeStageReview = false).size,
                    "Quality review tasks"
                )
            },
        )

        // complete all quality tasks
        var qualityTasks = findTasks(quality = true, includeStageReview = false)
        while (qualityTasks.size > 0) {
            qualityTasks.forEach { qualityTask ->
                flowableTaskService.complete(qualityTask.id).also { qualityTasksCompletedCounter++ }
            }
            qualityTasks = findTasks(quality = true, includeStageReview = false)
        }

        assertAll(
            "After finishing all quality tasks",
            { assertEquals(34, findPlanItems().size, "All plan items") },
            { assertEquals(9, findPlanItems(quality = false).size, "Regular plan items") },
            { assertEquals(25, findPlanItems(quality = true).size, "Quality plan items") },
            { assertEquals(0, findTasks(quality = true, includeStageReview = false).size, "Quality review tasks") },
        )

        assertEquals(
            regularTasksCompletedCounter,
            qualityTasksCompletedCounter,
            "The number of completed regular and quality tasks should be the same"
        )

        // find all completed regular tasks
        val completedRegularTaskPlanItems = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(projectInstanceService.getProjectInstanceById(projectInstanceId)!!.regularCaseId)
            .planItemDefinitionType("humantask")
            .planItemInstanceStateCompleted()
            .includeEnded()
            .list()

        // find all completed quality tasks
        val completedQualityTaskPlanItems = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(projectInstanceService.getProjectInstanceById(projectInstanceId)!!.qualityShellId)
            .planItemDefinitionType("humantask")
            .planItemInstanceStateCompleted()
            .includeEnded()
            .list()

        // build maps of regular id's and quality id's
        val reviewTaskLinksFromRegularTasks = mutableMapOf<String, String>()  // map of regular to review
        completedRegularTaskPlanItems.forEach { regularPlanItem ->
            val reviewTaskPlanItemId = planItemService.getPlanItemVariable(
                regularPlanItem.id,
                QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID.key
            ) as String
            reviewTaskLinksFromRegularTasks[regularPlanItem.id] = reviewTaskPlanItemId
        }
        val reviewTaskLinksFromQualityTasks = mutableMapOf<String, String>()  // map of regular to review
        completedQualityTaskPlanItems.forEach { qualityPlanItem ->
            val regularTaskPlanItemId = planItemService.getPlanItemVariable(
                qualityPlanItem.id,
                QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID.key
            ) as String
            reviewTaskLinksFromQualityTasks[regularTaskPlanItemId] = qualityPlanItem.id
        }
        assertEquals(
            reviewTaskLinksFromRegularTasks,
            reviewTaskLinksFromQualityTasks,
            "The maps with review task links should be equal"
        )

        reviewTaskLinksFromRegularTasks.forEach { (regularId, qualityId) ->
            val regularPlanItem = planItemService.getPlanItemDtoOrNull(regularId)!!
            val qualityPlanItem = planItemService.getPlanItemDtoOrNull(qualityId)!!
            assertEquals(
                "Review for task: '${regularPlanItem.name}'", qualityPlanItem.name,
                "The name of the quality item should contain the name of the regular item with a prefix"
            )
        }

        reviewTaskLinksFromQualityTasks.forEach { (regularId, qualityId) ->
            val regularPlanItem = planItemService.getPlanItemDtoOrNull(regularId)!!
            val qualityPlanItem = planItemService.getPlanItemDtoOrNull(qualityId)!!
            assertEquals(
                "Review for task: '${regularPlanItem.name}'", qualityPlanItem.name,
                "The name of the quality item should contain the name of the regular item with a prefix"
            )
        }


    }

    private fun findPlanItems(quality: Boolean? = null, state: PlanItemState? = null): Collection<PlanItemDto> {
        val planItems = planItemService.findByQuery(SearchQuery<PlanItemDto>()).toMutableList()
        if (quality != null) planItems.removeIf { it.isQualityShell != quality }
        if (state != null) planItems.removeIf { it.state != state }
        return planItems
    }

    private fun findTasks(
        quality: Boolean? = null,
        planItemState: PlanItemState? = null,
        includeStageReview: Boolean? = null,
    ): MutableList<TaskDto> {
        val planItems = findPlanItems(quality, planItemState).toMutableList()
        planItems.removeIf { it.type != PlanItemType.HUMAN_TASK }

        val tasks = taskService.findByQuery(SearchQuery<TaskDto>()).toMutableList()
        tasks.removeIf { taskDto ->
            taskDto.planItemInstanceId !in planItems.map { it.id }.toHashSet()
        }

        if (includeStageReview == false) tasks.removeIf { it.name.lowercase().contains("review for stage: ") }

        return tasks

    }

    private fun findNextReviewTaskPlanItem(): PlanItemDto {
        val reviewTaskPlanItems = planItemService.findByQuery(SearchQuery<PlanItemDto>()).toMutableList()

        reviewTaskPlanItems.removeIf { !it.isQualityShell }
        reviewTaskPlanItems.removeIf { it.name.lowercase() != "review task" }
        reviewTaskPlanItems.removeIf { it.state == PlanItemState.ACTIVE }

        reviewTaskPlanItems.sortBy { it.timeCreated }

        return reviewTaskPlanItems.last()
    }

}