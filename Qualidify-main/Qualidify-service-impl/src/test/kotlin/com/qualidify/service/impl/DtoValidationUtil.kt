package com.qualidify.service.impl

import com.qualidify.metadata.annotations.CreationStrategy
import com.qualidify.metadata.annotations.DtoMetadata
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.annotation.AnnotatedElementUtils
import org.springframework.core.type.filter.RegexPatternTypeFilter
import java.util.regex.Pattern
import kotlin.reflect.KClass

object DtoValidationUtil {
    private const val PACKAGE = "com.qualidify"

    fun getBeanDefinitionsOfDtos(): Set<BeanDefinition> {
        val provider = ClassPathScanningCandidateComponentProvider(false)
        provider.addIncludeFilter(RegexPatternTypeFilter(Pattern.compile(".*Dto")))
        val dtos = provider.findCandidateComponents(PACKAGE)
        return dtos
    }

    fun isAnnotatedWithCreationStrategy(dtoClass: KClass<*>): Boolean {
        return getCreationStrategy(dtoClass) != null
    }

    fun getCreationStrategy(dtoClass: KClass<*>) =
        AnnotatedElementUtils.getMergedAnnotation(dtoClass.java, CreationStrategy::class.java)


    fun isAdditionEnabled(dtoClass: KClass<*>): Boolean {
        val metaData = dtoClass.java.getAnnotation(DtoMetadata::class.java)
        return metaData == null || metaData.enableAddition
    }

    fun getClassFromBeanDefinition(bd: BeanDefinition): KClass<*> {
        return try {
            Class.forName(bd.beanClassName).kotlin
        } catch (e: ClassNotFoundException) {
            throw RuntimeException("No class found with name " + bd.beanClassName, e)
        }
    }

    fun getTypeOfField(dtoClass: KClass<*>, fieldName: String): KClass<out Any> {
        return dtoClass.java.getDeclaredField(fieldName).type?.kotlin!!
    }

}