package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchCriterium
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.impl.process.mappers.TaskDtoMapper
import com.qualidify.service.impl.projects.CmmnTaskRepository
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.TaskReviewService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.engine.RuntimeService
import org.flowable.engine.test.Deployment
import org.flowable.spring.impl.test.FlowableSpringExtension
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 * Integration test for the [TaskServiceImpl] to test counting and filtering of dto properties that
 * are not implemented in flowable queries
 *
 * @author Menno Tol
 * @since 1.0
 */
@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(FlowableSpringExtension::class),
    ExtendWith(SpringExtension::class),
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional
@Disabled(
    "Disabled because 1, quality shell is not implemented for bpmn tasks and " +
            "2, the case task view only works with tasks of type 'cmmn'"
)  // TODO: QUAL-556
internal open class TaskServiceImplIT(
    private val runtimeService: RuntimeService,
    private val cmmnTaskService: CmmnTaskService,
    @Autowired private val cmmnTaskRepository: CmmnTaskRepository,
    @Autowired private val taskDtoMapper: TaskDtoMapper,
    @Autowired private val projectTemplateService: ProjectTemplateService,
    @Autowired private val projectInstanceService: ProjectInstanceService,
    @Autowired private val taskReviewService: TaskReviewService,
    @Autowired private val planItemService: PlanItemService,
) {

    private val taskService = TaskServiceImpl(
        projectInstanceService, cmmnTaskRepository, cmmnTaskService, taskDtoMapper, taskReviewService, planItemService
    )

    @Test
    @Deployment(resources = ["dev-resources/processes/simpleTask.bpmn20.xml"])
    @CmmnDeployment(
        resources = [
            "dev-resources/cases/employeeOnboarding.cmmn.xml",
            "dev-resources/cases/cmmnQualityShell.cmmn.xml",
        ]
    )
    fun `Test count and search query of cmmn tasks by filtering`() {

        // start employee onboarding via qualidify service (required for projectInstance wrapper to be available)
        val employeeOnboardingTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(searchCriteria = listOf(SearchCriterium("cmmnModel", "employeeOnboarding")))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(employeeOnboardingTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))

        // start runtime task via flowable service
        runtimeService.startProcessInstanceByKey("simpleTask")

        // bypassing qualidify service
        val flowAllTasks = cmmnTaskService.createTaskQuery().list()
        assertEquals(4, flowAllTasks.size)  // flowable can filter based on scope
        val flowCmmnTasks = cmmnTaskService.createTaskQuery().scopeType("cmmn").list()
        assertEquals(3, flowCmmnTasks.size)  // flowable can filter based on scope

        // without constraints
        var countQuery = taskService.countByQuery(CountQuery<TaskDto>())
        var searchQuery = taskService.findByQuery(SearchQuery<TaskDto>())
        assertEquals(countQuery.count.toInt(), searchQuery.size)  // 4

        // with constraint 'type' = 'cmmn'
        countQuery =
            taskService.countByQuery(
                CountQuery<TaskDto>(
                    searchCriteria = listOf(
                        SearchCriterium(
                            "type",
                            "cmmn"
                        )
                    )
                )
            )
        searchQuery =
            taskService.findByQuery(
                SearchQuery<TaskDto>(
                    searchCriteria = listOf(
                        SearchCriterium(
                            "type",
                            "cmmn"
                        )
                    )
                )
            )
        assertEquals(countQuery.count.toInt(), searchQuery.size)  // 4

        // with constraint 'type' = 'bpmn'
        countQuery =
            taskService.countByQuery(CountQuery<TaskDto>(searchCriteria = listOf(SearchCriterium("type", "bpmn"))))
        searchQuery =
            taskService.findByQuery(SearchQuery<TaskDto>(searchCriteria = listOf(SearchCriterium("type", "bpmn"))))
        assertEquals(countQuery.count.toInt(), searchQuery.size)  // 0

    }

}