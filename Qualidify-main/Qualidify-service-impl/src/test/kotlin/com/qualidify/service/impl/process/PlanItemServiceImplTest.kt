package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.mappers.PlanItemDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnHistoricPlanItemRepository
import com.qualidify.service.impl.process.repositories.CmmnPlanItemRepository
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemType
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnHistoryService
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.CmmnEngine
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import java.util.*
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

/**
 * A unit test for the [PlanItemServiceImpl]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class PlanItemServiceImplTest {

    private val cmmnPlanItemRepository = mockk<CmmnPlanItemRepository>()
    private val cmmnHistoricPlanItemRepository = mockk<CmmnHistoricPlanItemRepository>()
    private val cmmnRuntimeService = mockk<CmmnRuntimeService>()
    private val cmmnHistoryService = mockk<CmmnHistoryService>()
    private val cmmnEngine = mockk<CmmnEngine>()

    private val planItemDtoMapper =
        spyk(PlanItemDtoMapper())

    private val fixture = PlanItemServiceImpl(
        cmmnPlanItemRepository,
        cmmnHistoricPlanItemRepository,
        planItemDtoMapper,
        cmmnEngine,
        cmmnRuntimeService,
        cmmnHistoryService,
    )

    private val dto = DtoFactory.planItemDto(type = PlanItemType.HUMAN_TASK)
    private val entityHumanTask = TestEntities.cmmnPlanItemJpa(type = "humantask")
    private val entityUsereventlistener = TestEntities.cmmnPlanItemJpa(type = "usereventlistener")

    @AfterEach
    fun checkTest() {
        checkUnnecessaryStub(
            cmmnPlanItemRepository,
            cmmnHistoricPlanItemRepository,
            planItemDtoMapper,
            cmmnEngine,
            cmmnRuntimeService,
            cmmnHistoryService,
        )
        confirmVerified(
            cmmnPlanItemRepository,
            cmmnHistoricPlanItemRepository,
            planItemDtoMapper,
            cmmnEngine,
            cmmnRuntimeService,
            cmmnHistoryService,
        )
    }

    @Test
    fun `Test countByQuery`() {
        mockkStatic(CmmnPlanItemRepository::countResponse)

        every {
            cmmnPlanItemRepository.countResponse(CountQuery<PlanItemDto>())
        } returns CountResponse(3)

        val result = fixture.countByQuery(CountQuery<PlanItemDto>())

        assertEquals(3, result.count)

        verify { cmmnRuntimeService wasNot called }
        verify(exactly = 1) { cmmnPlanItemRepository.countResponse(CountQuery<PlanItemDto>()) }
    }

    @Test
    fun `Test findByQuery is empty`() {
        mockkStatic(CmmnPlanItemRepository::findAllByQuery)

        every {
            cmmnPlanItemRepository.findAllByQuery(SearchQuery<PlanItemDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<PlanItemDto>())

        assertContentEquals(emptyList(), result)

        verify(exactly = 1) { cmmnPlanItemRepository.findAllByQuery(SearchQuery<PlanItemDto>()) }
        verify { cmmnRuntimeService wasNot called }

    }

    @Test
    fun `Test get plan items with stage`() {
        val planItemDto = DtoFactory.planItemDto(id = "mockPlanItemId")

        every {
            cmmnPlanItemRepository.countByStageEquals(planItemDto.id)
        } returns 2

        every {
            cmmnPlanItemRepository.findAllByStageEquals(planItemDto.id)
        } returns listOf(
            TestEntities.cmmnPlanItemJpa(type = PlanItemType.HUMAN_TASK.type),
            TestEntities.cmmnPlanItemJpa(type = PlanItemType.HUMAN_TASK.type)
        )

        val hasPlans = fixture.hasPlanItemsWithStage(planItemDto)
        val countPlans = fixture.getPlanItemWithStageCount(planItemDto)
        val plans = fixture.getPlanItemsWithStage(planItemDto)

        assertThat(hasPlans).isTrue
        assertThat(countPlans).isEqualTo(2)
        assertThat(plans).isEqualTo(listOf(DtoFactory.planItemDto(), DtoFactory.planItemDto()))

        verify(exactly = 2) { cmmnPlanItemRepository.countByStageEquals(planItemDto.id) }
        verify(exactly = 1) { cmmnPlanItemRepository.findAllByStageEquals(planItemDto.id) }
        verify(exactly = 2) { planItemDtoMapper.convertToDto(any()) }

    }

    @Test
    fun `Test get stages with case instance`() {
        val caseInstanceId = "caseInstanceId"

        every {
            cmmnPlanItemRepository.findAllByCaseInstanceIdAndDefinitionType(caseInstanceId, "stage")
        } returns listOf(TestEntities.cmmnPlanItemJpa(type = PlanItemType.HUMAN_TASK.type))

        val result = fixture.getStages(caseInstanceId)

        assertThat(result.size).isOne
        assertThat(result).isEqualTo(listOf(DtoFactory.planItemDto()))

        verify(exactly = 1) { cmmnPlanItemRepository.findAllByCaseInstanceIdAndDefinitionType(caseInstanceId, "stage") }
        verify(exactly = 1) { planItemDtoMapper.convertToDto(any()) }

    }

    @ParameterizedTest
    @MethodSource("getDtoProperties")
    fun `Test query constraints mapping`(property: String) {
        mockkStatic(CmmnPlanItemRepository::findAllByQuery)

        val searchQuery = SearchQuery<PlanItemDto>(constraints = mapOf(property to "randomValue"))
        every {
            cmmnPlanItemRepository.findAllByQuery(searchQuery)
        } returns PageImpl(listOf(entityHumanTask, entityUsereventlistener))

        val results = fixture.findByQuery(searchQuery)

        assertThat(results).contains(
            DtoFactory.planItemDto(type = PlanItemType.HUMAN_TASK),
            DtoFactory.planItemDto(type = PlanItemType.USER_EVENT_LISTENER)
        )

        verify(exactly = 1) { cmmnPlanItemRepository.findAllByQuery(searchQuery) }
        verify(exactly = 2) { planItemDtoMapper.convertToDto(any()) }

    }

    @Test
    fun `Test startPlanItem`() {
        every {
            cmmnRuntimeService.startPlanItemInstance(dto.id)
        } just Runs

        fixture.startPlanItem(dto)

        verify { cmmnPlanItemRepository wasNot called }
        verify(exactly = 1) {
            cmmnRuntimeService.startPlanItemInstance(dto.id)
        }
    }

    companion object {
        @JvmStatic
        private fun getDtoProperties(): Set<String> {
            return PlanItemDto::class.declaredMemberProperties
                .map { it.name }
                .toSet()
        }
    }

}

