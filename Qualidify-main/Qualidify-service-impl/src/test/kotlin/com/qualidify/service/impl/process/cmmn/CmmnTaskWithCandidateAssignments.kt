package com.qualidify.service.impl.process.cmmn

import com.qualidify.service.security.SecurityService
import com.qualidify.service.security.dto.SecurityGroupDto
import com.qualidify.service.user.UserService
import com.qualidify.service.user.dto.CreateUserCommand
import com.qualidify.service.user.dto.UserDto
import org.assertj.core.api.Assertions
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*
import kotlin.test.assertNotNull

@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Disabled(
    "Disabled because it is not testing Qualidify functionality, " +
            "but it is a showcase for CMMN (disable this line, to walk through)"
)
open class CmmnTaskWithCandidateAssignments(
    val cmmnRuntimeService: CmmnRuntimeService,
    val cmmnTaskService: CmmnTaskService,
    @Autowired val securityService: SecurityService,
    @Autowired val userService: UserService
) {

    val diningRoom: SecurityGroupDto
    val restrictedRoom: SecurityGroupDto
    val bluebeard: UserDto
    val mrsBluebeard: UserDto
    val anne: UserDto

    init {
        diningRoom = SecurityGroupDto("DINING_ROOM", "The Dining room")
        restrictedRoom = SecurityGroupDto("RESTRICTED_ROOM", "Restricted room")
        bluebeard = UserDto(
            userId = UUID.randomUUID(),
            username = "Bluebeard",

            email = "bluebeard@yahoo.com",
            securityGroups = setOf(diningRoom.name, restrictedRoom.name)
        )


        mrsBluebeard = UserDto(
            UUID.randomUUID(),
            "Mrs Bluebeard",
            email = "mrsBluebeard@fairytale.tv",
            securityGroups = setOf(diningRoom.name)
        )

        anne = UserDto(
            UUID.randomUUID(),
            "Anne",
            "anne@fairytale.tv",
        )

        if (!securityService.findSecurityGroupByName("DINING_ROOM").isPresent) {
            securityService.createGroup(diningRoom)
            securityService.createGroup(restrictedRoom)
            userService.create(
                CreateUserCommand(
                    userName = "Mrs Bluebeard",
                    email = "mrsBluebeard@fairytale.tv",
                    password = "1234",
                    securityGroups = setOf(diningRoom.name)
                )
            )
            userService.create(
                CreateUserCommand(
                    "Mrs Bluebeard",
                    password = "password",
                    email = "mrsBluebeard@fairytale.tv",
                    securityGroups = setOf(diningRoom.name)
                )
            )
        }
    }

    @Test
    @CmmnDeployment(
        resources = ["cases/TaskWithGroup.cmmn.xml"]
    )
    fun `test cmmn file with use of security group`() {

        val caseInstance: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("CaseTaskWithGroup")
            .start()
        assertNotNull(caseInstance, "The parentCase case instance should be found")

        val planItemInstances = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateActive()
            .list()
        Assertions.assertThat(planItemInstances)
            .`as`("There must be one task available at startup.")
            .hasSize(1)

        val allAvailableTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()
        Assertions.assertThat(allAvailableTasks).`as`("There must be one task available at startup.")
            .hasSize(1)


        val tasksForAnne = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(anne.username)
            .list()
        Assertions.assertThat(tasksForAnne).`as`("There must be no task available for Anne")
            .isEmpty()

        val tasksForBluebeard = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(bluebeard.username)
            .list()
        Assertions.assertThat(tasksForBluebeard).`as`("There must be one task available for Bluebeard")
            .hasSize(1)

        //Everyone can claim task
        cmmnTaskService.claim(tasksForBluebeard[0].id, "Anne")

    }

    @Test
    @CmmnDeployment(
        resources = ["cases/TaskWithGroups.cmmn.xml"]
    )
    fun `test cmmn file with use of security groups`() {

        val caseInstance: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("CaseTaskWithGroups")
            .start()
        assertNotNull(caseInstance, "The parentCase case instance should be found")

        val planItemInstances = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateActive()
            .list()
        Assertions.assertThat(planItemInstances)
            .`as`("There must be one task available at startup.")
            .hasSize(1)

        val allAvailableTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()
        Assertions.assertThat(allAvailableTasks).`as`("There must be one task available at startup.")
            .hasSize(1)


        val tasksForAnne = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(anne.username)
            .list()
        Assertions.assertThat(tasksForAnne).`as`("There must be no task available for Anne")
            .isEmpty()

        val tasksForMrsBluebeard = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(mrsBluebeard.username)
            .list()
        Assertions.assertThat(tasksForMrsBluebeard).`as`("With DINING_ROOM access, Mrs Bluebeard has access ")
            .hasSize(1)


        val tasksForBluebeard = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(bluebeard.username)
            .list()
        Assertions.assertThat(tasksForBluebeard).`as`("There must be one task available for Bluebeard")
            .hasSize(1)

        //Everyone can claim task
        cmmnTaskService.claim(tasksForBluebeard[0].id, "Anne")

    }

    @Test
    @CmmnDeployment(
        resources = ["cases/TaskWithUser.cmmn.xml"]
    )
    fun `test cmmn file with use of user`() {

        val caseInstance: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("CaseTaskWithUser")
            .start()
        assertNotNull(caseInstance, "The parentCase case instance should be found")

        val planItemInstances = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateActive()
            .list()
        Assertions.assertThat(planItemInstances)
            .`as`("There must be one task available at startup.")
            .hasSize(1)

        val allAvailableTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()
        Assertions.assertThat(allAvailableTasks).`as`("There must be one task available at startup.")
            .hasSize(1)


        val tasksForAnne = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(anne.username)
            .list()
        Assertions.assertThat(tasksForAnne).`as`("There must be no task available for Anne")
            .isEmpty()

        val tasksForMrsBluebeard = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(mrsBluebeard.username)
            .list()
        Assertions.assertThat(tasksForMrsBluebeard).`as`("There must be no task available for Mrs Bluebeard")
            .hasSize(0)


        val tasksForBluebeard = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(bluebeard.username)
            .list()
        Assertions.assertThat(tasksForBluebeard).`as`("There must be one task available for Bluebeard")
            .hasSize(1)

        //Everyone can claim task
        cmmnTaskService.claim(tasksForBluebeard[0].id, "Anne")

    }

    @Test
    @CmmnDeployment(
        resources = ["cases/TaskWithUsers.cmmn.xml"]
    )
    fun `test cmmn file with use of users`() {

        val caseInstance: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("CaseTaskWithUsers")
            .start()
        assertNotNull(caseInstance, "The parentCase case instance should be found")

        val planItemInstances = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateActive()
            .list()
        Assertions.assertThat(planItemInstances)
            .`as`("There must be one task available at startup.")
            .hasSize(1)

        val allAvailableTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()
        Assertions.assertThat(allAvailableTasks).`as`("There must be one task available at startup.")
            .hasSize(1)


        val tasksForAnne = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(anne.username)
            .list()
        Assertions.assertThat(tasksForAnne).`as`("There must be no task available for Anne")
            .isEmpty()

        val tasksForMrsBluebeard = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(mrsBluebeard.username)
            .list()
        Assertions.assertThat(tasksForMrsBluebeard).`as`("There must be one task available for Mrs Bluebeard")
            .hasSize(1)


        val tasksForBluebeard = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(bluebeard.username)
            .list()
        Assertions.assertThat(tasksForBluebeard).`as`("There must be one task available for Bluebeard")
            .hasSize(1)

        //Everyone can claim task
        cmmnTaskService.claim(tasksForBluebeard[0].id, "Anne")

        val allAvailableTasksAfterClaim = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()
        Assertions.assertThat(allAvailableTasksAfterClaim)
            .`as`("There is still one task available after all is claimed.")
            .hasSize(1)

        val tasksForBluebeardAfterClaim = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .taskCandidateUser(bluebeard.username)
            .list()
        Assertions.assertThat(tasksForBluebeardAfterClaim).`as`("There is no task available for Bluebeard")
            .hasSize(0)

    }

}