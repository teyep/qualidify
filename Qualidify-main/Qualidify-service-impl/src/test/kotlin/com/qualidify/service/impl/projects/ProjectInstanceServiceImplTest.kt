package com.qualidify.service.impl.projects

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.ProjectInstance
import com.qualidify.model.entities.ProjectInstanceId
import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.events.CompletedCmmnEvent
import com.qualidify.service.impl.process.events.TerminatedCmmnEvent
import com.qualidify.service.impl.process.mappers.CaseInstanceDtoMapper
import com.qualidify.service.impl.process.mappers.ProjectInstanceDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.impl.process.repositories.CmmnHistoricCaseInstanceRepository
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.projects.dto.ProjectInstanceDto
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.data.domain.Page
import java.util.*
import kotlin.test.*

internal class ProjectInstanceServiceImplTest {

    private val projectInstanceRepository = mockk<ProjectInstanceRepository>()
    private val cmmnCaseInstanceRepository = mockk<CmmnCaseInstanceRepository>()
    private val cmmnCaseDefinitionRepository = mockk<CmmnCaseDefinitionRepository>()
    private val cmmnHistoricCaseInstanceRepository = mockk<CmmnHistoricCaseInstanceRepository>()
    private val caseInstanceService = mockk<CaseInstanceService>()

    private val caseInstanceDtoMapper =
        spyk(CaseInstanceDtoMapper(cmmnCaseDefinitionRepository))
    private val projectInstanceDtoMapper = spyk(
        ProjectInstanceDtoMapper(
            cmmnCaseInstanceRepository,
            caseInstanceDtoMapper,
            cmmnHistoricCaseInstanceRepository,
        )
    )

    private val fixture = ProjectInstanceServiceImpl(projectInstanceRepository, projectInstanceDtoMapper)

    private val cmmnCaseInstanceJpa = TestEntities.cmmnCaseInstanceJpa()
    private val historicCmmnCaseInstanceJpa = TestEntities.cmmnHistoricCaseInstanceJpa()
    private val id = UUID.randomUUID()

    @AfterEach
    fun checkTest() {
        checkUnnecessaryStub(
            projectInstanceRepository,
            cmmnCaseInstanceRepository,
            cmmnCaseDefinitionRepository,
            cmmnHistoricCaseInstanceRepository,
            caseInstanceService,
        )
        confirmVerified(
            cmmnCaseInstanceRepository,
            cmmnCaseDefinitionRepository,
            projectInstanceRepository,
            cmmnHistoricCaseInstanceRepository,
            caseInstanceService,
        )
    }

    @Test
    fun `Test countByQuery`() {
        mockkStatic(ProjectInstanceRepository::countResponse)

        every {
            projectInstanceRepository.countResponse(CountQuery<ProjectInstanceDto>())
        } returns CountResponse(2)

        val result = fixture.countByQuery(CountQuery<ProjectInstanceDto>())

        assertEquals(2, result.count, "The list of project instances should be empty")

        verify(exactly = 1) { projectInstanceRepository.countResponse(CountQuery<ProjectInstanceDto>()) }
    }

    @Test
    fun `Test findByQuery is empty`() {
        mockkStatic(ProjectInstanceRepository::findAllByQuery)

        every {
            projectInstanceRepository.findAllByQuery(SearchQuery<ProjectInstanceDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<ProjectInstanceDto>())

        assertContentEquals(emptyList(), result, "The list of project instances should be empty")

        verify(exactly = 1) { projectInstanceRepository.findAllByQuery(SearchQuery<ProjectInstanceDto>()) }

    }

    @Test
    fun `Test get a projectInstance by its Id`() {
        every {
            cmmnCaseInstanceRepository.findById(any())
        } returns Optional.of(cmmnCaseInstanceJpa)

        every {
            cmmnHistoricCaseInstanceRepository.findById(any())
        } returns Optional.of(historicCmmnCaseInstanceJpa)

        every {
            cmmnCaseDefinitionRepository.findById(any()).get()
        } returns cmmnCaseInstanceJpa.cmmnCaseDefinition

        every {
            projectInstanceRepository.findById(ProjectInstanceId(id))
        } returns Optional.of(TestEntities.projectInstance(id = id))

        val projectInstance = fixture.getProjectInstanceById(id.toString())

        assertNotNull(projectInstance, "A projectInstance can be found")
        assertEquals(id.toString(), projectInstance.id, "The found projectInstance has a correct id")

        verify(exactly = 1) { projectInstanceRepository.findById(ProjectInstanceId(id)) }
        verify(exactly = 2) { cmmnCaseInstanceRepository.findById(any()) }
        verify(exactly = 2) { cmmnHistoricCaseInstanceRepository.findById(any()) }
        verify(exactly = 2) { cmmnCaseDefinitionRepository.findById(any()) }

    }

    @Test
    fun `Test try to get a projectInstance by its Id where it doesn't exist `() {
        every {
            projectInstanceRepository.findById(ProjectInstanceId(id))
        } returns Optional.empty()

        val projectInstance = fixture.getProjectInstanceById(id.toString())

        assertNull(projectInstance, "A projectInstance can be found")

        verify(exactly = 1) { projectInstanceRepository.findById(ProjectInstanceId(id)) }

    }

    @Test
    fun `Test availability of a projectInstance`() {
        every {
            projectInstanceRepository.existsById(ProjectInstanceId(id))
        } returns true

        val availability = fixture.isProjectAvailable(id.toString())

        assertTrue(availability, "Project should be available")

        verify(exactly = 1) { projectInstanceRepository.existsById(ProjectInstanceId(id)) }

    }

    @Test
    fun `Test non-availability of a projectInstance`() {
        every {
            projectInstanceRepository.existsById(ProjectInstanceId(id))
        } returns false

        val availability = fixture.isProjectAvailable(id.toString())

        assertFalse(availability, "Project should not be available")

        verify(exactly = 1) { projectInstanceRepository.existsById(ProjectInstanceId(id)) }

    }

    @Test
    fun `Test with a cmmn ending (terminating) event`() {
        val cmmnId = UUID.randomUUID().toString()
        val event = TerminatedCmmnEvent(DtoFactory.caseInstanceDto(cmmnId))

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(cmmnId)
        } returns Optional.of(TestEntities.projectInstance(cmmnInstanceId = cmmnId))

        val projectInstanceSlot = slot<ProjectInstance>()
        every {
            projectInstanceRepository.save(capture(projectInstanceSlot))
        } answers {
            it.invocation.args[0] as ProjectInstance
        }

        fixture.handleCmmnEndingEvent(event)

        assertEquals(cmmnId, projectInstanceSlot.captured.regularCaseId)
        assertEquals("terminated", projectInstanceSlot.captured.regularCaseState)

        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(cmmnId) }
        verify(exactly = 1) { projectInstanceRepository.save(any()) }

    }

    @Test
    fun `Test with a cmmn ending (completing) event`() {
        val cmmnId = UUID.randomUUID().toString()
        val event = CompletedCmmnEvent(DtoFactory.caseInstanceDto(cmmnId))

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(cmmnId)
        } returns Optional.of(TestEntities.projectInstance(cmmnInstanceId = cmmnId))

        val projectInstanceSlot = slot<ProjectInstance>()
        every {
            projectInstanceRepository.save(capture(projectInstanceSlot))
        } answers {
            it.invocation.args[0] as ProjectInstance
        }

        fixture.handleCmmnEndingEvent(event)

        assertEquals(cmmnId, projectInstanceSlot.captured.regularCaseId)
        assertEquals("completed", projectInstanceSlot.captured.regularCaseState)

        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(cmmnId) }
        verify(exactly = 1) { projectInstanceRepository.save(any()) }

    }

    @Test
    fun `Test with a cmmn ending (completing) event, while projectinstance does not exists`() {
        val cmmnId = UUID.randomUUID().toString()
        val event = CompletedCmmnEvent(DtoFactory.caseInstanceDto(cmmnId))

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(cmmnId)
        } returns Optional.of(TestEntities.projectInstance(cmmnInstanceId = cmmnId))
        every {
            projectInstanceRepository.save(any())
        } returns TestEntities.projectInstance(cmmnInstanceId = cmmnId)

        fixture.handleCmmnEndingEvent(event)

        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(cmmnId) }
        verify(exactly = 1) { projectInstanceRepository.save(any()) }

    }

}
