package com.qualidify.service.impl.document

import com.qualidify.model.entities.*
import com.qualidify.service.document.dto.DocumentContentDto
import com.qualidify.service.document.dto.DocumentDto
import com.qualidify.service.document.dto.LabelDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

class DocumentMapperTest {

    val documentMapper = DocumentMapper()
    val labelMapper = LabelMapper()
    val documentContentMapper = DocumentContentMapper()

    @Test
    fun `convert Document to DocumentDto`() {
        val label_1 = getLabel("TEST")
        val label_2 = getLabel("LABEL")
        val document = getDocument(listOf(label_1, label_2))

        val dto = documentMapper.convertToDto(document)

        assertThat(dto).`as`("Dto should have the correct class")
            .isInstanceOf(DocumentDto::class.java)
        assertThat(dto.documentId).`as`("DocumentId should contain same uid as model")
            .isEqualTo(document.id.unbox())
        assertThat(dto.path).`as`("Path should contain same path as model")
            .isEqualTo(document.path)
        assertThat(dto.name).`as`("Name should contain same name as model")
            .isEqualTo(document.name)
        assertThat(dto.type).`as`("Type should contain same type as model")
            .isEqualTo(document.type)
        assertThat(dto.size).`as`("Size should contain same size as model")
            .isEqualTo(document.size)
        assertThat(dto.version).`as`("Version should contain same version as model")
            .isEqualTo(document.size)
        assertThat(dto.labels).`as`("Labels should contain same labels as model")
            .hasSize(2)
            .contains(LabelDto("TEST"), LabelDto("LABEL"))
    }

    @Test
    fun `convert DocumentDto to Document`() {
        val label_1 = LabelDto("TEST")
        val label_2 = LabelDto("LABEL")
        val documentDto = getDocumentDto(listOf(label_1, label_2))

        val model = documentMapper.convertToModel(documentDto)

        assertThat(model).`as`("Model should have the correct class")
            .isInstanceOf(Document::class.java)
        assertThat(model.id).`as`("DocumentId should contain same uid as dto")
            .isEqualTo(DocumentId(documentDto.documentId))
        assertThat(model.path).`as`("Path should contain same path as dto")
            .isEqualTo(documentDto.path)
        assertThat(model.name).`as`("Name should contain same name as dto")
            .isEqualTo(documentDto.name)
        assertThat(model.type).`as`("Type should contain same type as dto")
            .isEqualTo(documentDto.type)
        assertThat(model.size).`as`("Size should contain same size as dto")
            .isEqualTo(documentDto.size)
        assertThat(model.version).`as`("Version should contain same version as dto")
            .isEqualTo(documentDto.size)
        assertThat(model.labels).`as`("Labels should contain same labels as dto")
            .hasSize(2)
            .contains(Label("TEST"), Label("LABEL"))
    }

    @Test
    fun `convert Label to LabelDto`() {
        val label = Label("TESTLABEL")
        val dto = labelMapper.convertToDto(label)

        assertThat(dto).`as`("Model should have the correct class")
            .isInstanceOf(LabelDto::class.java)
        assertThat(dto.name).`as`("Name should contain the same name as the model")
            .isEqualTo(label.id.unbox())
    }

    @Test
    fun `convert LabelDto to Label`() {
        val labeldto = LabelDto("TESTLABEL")
        val model = labelMapper.convertToModel(labeldto)

        assertThat(model).`as`("Dto should have the correct class")
            .isInstanceOf(Label::class.java)
        assertThat(model.id).`as`("Name should contain the same name as the model")
            .isEqualTo(LabelId(labeldto.name))
        assertThat(model.documents).`as`("Linked documents is empty").isEmpty()
    }

    @Test
    fun `convert DocumentContent to DocumentContentDto`() {
        val documentContent = getDocumentContent()
        val dto = documentContentMapper.convertToDto(documentContent)

        assertThat(dto).`as`("Dto should have the correct class")
            .isInstanceOf(DocumentContentDto::class.java)
        assertThat(dto.documentId).`as`("documentId should contain the same name as the model")
            .isEqualTo(documentContent.id.unbox())
        assertThat(dto.content).`as`("content should contain the same name as the model")
            .isEqualTo(documentContent.content)
        assertThat(String(dto.content)).`as`("Bytearray can be converted back to original")
            .isEqualTo("BYTEARRAY")
    }

    @Test
    fun `convert DocumentContentDto to DocumentContent`() {
        val documentContentDto = getDocumentContentDto()
        val model = documentContentMapper.convertToModel(documentContentDto)

        assertThat(model).`as`("Model should have the correct class")
            .isInstanceOf(DocumentContent::class.java)
        assertThat(model.id).`as`("Id should contain the same name as the dto")
            .isEqualTo(DocumentId(documentContentDto.documentId))
        assertThat(model.content).`as`("documentId should contain the same name as the model")
            .isEqualTo(documentContentDto.content)
        assertThat(String(model.content)).`as`("Bytearray can be converted back to original")
            .isEqualTo("BYTEARRAY")
    }

    private fun getDocument(labels: List<Label>) = Document(
        id = DocumentId(UUID.randomUUID()),
        name = "TestDocument",
        type = "docx",
        path = "/",
        size = 1,
        labels = labels
    )

    private fun getLabel(name: String) = Label(name)

    private fun getDocumentContent() = DocumentContent(
        id = DocumentId(UUID.randomUUID()),
        content = getByteArray()
    )

    private fun getDocumentContentDto() = DocumentContentDto(
        documentId = UUID.randomUUID(),
        content = getByteArray()
    )

    private fun getByteArray() = "BYTEARRAY".toByteArray()

    private fun getDocumentDto(labels: List<LabelDto>) = DocumentDto(
        documentId = UUID.randomUUID(),
        name = "TestDocument",
        type = "docx",
        path = "/",
        size = 1,
        version = 1,
        labels = labels.toTypedArray()
    )


}

