package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.repositories.BpmnTaskRepository
import com.qualidify.service.process.dto.BusinessProcessTaskDto
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

/**
 * A unit test for the [BpmnTaskServiceImpl]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class BpmnTaskServiceImplTest {

    private val mockBpmnTaskRepository = mockk<BpmnTaskRepository>()

    private val fixture = BpmnTaskServiceImpl(mockBpmnTaskRepository)

    private val dto = DtoFactory.businessProcessTaskDto()

    private val entity = TestEntities.bpmnTaskJpa()

    @Test
    fun `Test countByQuery`() {
        mockkStatic(BpmnTaskRepository::countResponse)

        every {
            mockBpmnTaskRepository.countResponse(CountQuery<BusinessProcessTaskDto>())
        } returns CountResponse(2)

        val result = fixture.countByQuery(CountQuery<BusinessProcessTaskDto>())

        assertEquals(2, result.count)

        verify(exactly = 1) { mockBpmnTaskRepository.countResponse(CountQuery<BusinessProcessTaskDto>()) }
    }

    @Test
    fun `Test findQuery is empty`() {
        mockkStatic(BpmnTaskRepository::findAllByQuery)

        every {
            mockBpmnTaskRepository.findAllByQuery(SearchQuery<BusinessProcessTaskDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<BusinessProcessTaskDto>())

        assertEquals(emptyList(), result)

        verify(exactly = 1) { mockBpmnTaskRepository.findAllByQuery(SearchQuery<BusinessProcessTaskDto>()) }
    }

    @ParameterizedTest
    @MethodSource("getDtoProperties")
    fun `Test query constraints mapping`(property: String) {
        mockkStatic(BpmnTaskRepository::findAllByQuery)

        val searchQuery = SearchQuery<BusinessProcessTaskDto>(constraints = mapOf(property to "RandomValue"))

        every {
            mockBpmnTaskRepository.findAllByQuery(searchQuery)
        } returns PageImpl(listOf(entity))

        val result = fixture.findByQuery(searchQuery)

        assertContentEquals(result, listOf(dto))

        verify(exactly = 1) { mockBpmnTaskRepository.findAllByQuery(searchQuery) }
    }

    companion object {

        @JvmStatic
        private fun getDtoProperties(): Set<String> {
            return BusinessProcessTaskDto::class.declaredMemberProperties
                    .map { it.name }
                    .toSet()
        }

    }

}