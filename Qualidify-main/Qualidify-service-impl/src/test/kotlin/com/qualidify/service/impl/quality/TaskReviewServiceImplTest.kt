package com.qualidify.service.impl.quality

import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.projects.TaskService
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.TaskReviewService
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

/**
 * Unit test for the implementation of the [TaskReviewService].
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class TaskReviewServiceImplTest {

    private val caseInstanceService = mockk<CaseInstanceService>()
    private val planItemService = mockk<PlanItemService>()
    private val taskService = mockk<TaskService>()

    private val fixture = TaskReviewServiceImpl(caseInstanceService, planItemService, taskService)

    @AfterEach
    fun checkTest() {
        confirmVerified(caseInstanceService, planItemService, taskService)
        checkUnnecessaryStub(caseInstanceService, planItemService, taskService)
    }

    @Test
    fun `Test start review cascade with human task`() {
        prepareReviewCascadeStubbing()

        every {
            caseInstanceService.getCaseInstanceDtoById("qualityShellId")
        } returns DtoFactory.caseInstanceDto(
            id = "qualityShellId",
            isQualityShell = true,
            qualityShellId = "cmmnQualityShellId",
        )

        val planItemInstance = TestEntities.planItemInstanceEntity()

        fixture.startReview(planItemInstance)

        verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
        verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }
        verify(exactly = 1) { planItemService.getPlanItemsByStageAndDefinition(any()) }
        verify(exactly = 1) { planItemService.completeGenericEventListener(any()) }
        verify(exactly = 7) { planItemService.setPlanItemVariable(any(), any(), any()) }
        verify { taskService wasNot called }

    }

    @ParameterizedTest
    @MethodSource("getPlanItemTypes")
    fun `Test with different PlanItemTypes`(type: String) {
        val planItem = DtoFactory.planItemDto(type = PlanItemType.getByName(type))

        when (planItem.type) {
            PlanItemType.GENERIC_EVENT_LISTENER, PlanItemType.SIGNAL_EVENT_LISTENER, PlanItemType.TIMER_EVENT_LISTENER,
            PlanItemType.USER_EVENT_LISTENER, PlanItemType.VARIABLE_EVENT_LISTENER,
            -> {
                val exception = assertFailsWith<TaskReviewServiceException> { fixture.startReview(planItem) }
                assertThat(exception.message).isEqualTo(
                    "Dto with id '${planItem.id}', type '${planItem.type}' is not a task but an event listener"
                )
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            PlanItemType.STAGE, PlanItemType.MILESTONE, PlanItemType.PLAN_FRAGMENT,
            -> {
                val exception = assertFailsWith<TaskReviewServiceException> { fixture.startReview(planItem) }
                assertThat(exception.message).isEqualTo(
                    "Dto with id 'mockPlanItemId', type '${planItem.type}' is not a task but a stage, milestone or plan fragment"
                )
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            PlanItemType.HUMAN_TASK, PlanItemType.SERVICE_TASK, PlanItemType.HTTP_SERVICE_TASK, PlanItemType.DECISION_TASK,
            PlanItemType.EXTERNAL_WORKER_TASK, PlanItemType.CASE_TASK, PlanItemType.CASE_PAGE_TASK, PlanItemType.PROCESS_TASK,
            -> {
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            else -> {
                fail("Plan item type '$type' is not implemented in the TaskReviewService and/or its tests.")
            }
        }
    }

    @ParameterizedTest
    @MethodSource("getReviewStates")
    fun `Test review cascade with different review states`(state: String?) {
        val planItemInstance = TestEntities.planItemInstanceEntity()

        when (state) {
            ReviewState.REVIEW_REQUIRED.state, ReviewState.UNDEFINED.state, null,
            -> {
                // complete stubbing required for happy path
                prepareReviewCascadeStubbing()

                every {
                    caseInstanceService.getCaseInstanceDtoById("qualityShellId")
                } returns DtoFactory.caseInstanceDto(
                    id = "qualityShellId",
                    isQualityShell = true,
                    qualityShellId = "cmmnQualityShellId",
                )

                fixture.startReview(planItemInstance)

                // follow the happy path verifications of the basic test above
                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
                verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
                verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }
                verify(exactly = 1) { planItemService.getPlanItemsByStageAndDefinition(any()) }
                verify(exactly = 1) { planItemService.completeGenericEventListener(any()) }
                verify(exactly = 7) { planItemService.setPlanItemVariable(any(), any(), any()) }
                verify { taskService wasNot called }

            }

            ReviewState.REVIEW_NOT_REQUIRED.state, ReviewState.APPROVED.state, ReviewState.REJECTED.state,
            ReviewState.IN_REVIEW.state, ReviewState.CORRECTED.state,
            -> {
                // minimal stubbing required to reach code that is tested
                every {
                    planItemService.getPlanItemDtoOrNull(any())
                } returns DtoFactory.planItemDto(
                    reviewState = ReviewState.getByName(state)
                )

                fixture.startReview(planItemInstance)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
                verify { caseInstanceService wasNot called }
                verify { taskService wasNot called }
            }

            else -> {
                fail("Review state '$state' is not implemented in the TaskReviewService and/or its tests.")
            }
        }

    }

    @ParameterizedTest
    @MethodSource("getReviewStatesNonNull")
    fun `Test approve review with different review states`(state: String) {
        val reviewTaskDto = DtoFactory.taskDto(
            id = "reviewTaskId",
            planItemInstanceId = "reviewTaskPlanItemId",
            reviewState = ReviewState.getByName(state),
            qualityLinkId = "regularTaskPlanItemId"
        )
        val principal = "testUser"

        every {
            taskService.getTaskDtoByPlanItemId("regularTaskPlanItemId")
        } returns DtoFactory.taskDto("regularTaskId", reviewState = ReviewState.getByName(state))

        when (state) {
            ReviewState.REJECTED.state,  /// unhappy path
            -> {
                fixture.approveReview(reviewTaskDto, principal)

                verify(exactly = 1) { taskService.getTaskDtoByPlanItemId("regularTaskPlanItemId") }
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }
            }

            ReviewState.IN_REVIEW.state, ReviewState.CORRECTED.state,  // happy path
            -> {

                every {
                    planItemService.getPlanItemDtoOrNull("regularTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "regularTaskPlanItemId",
                    qualityLinkId = "reviewTaskPlanItemId",
                    reviewState = ReviewState.REJECTED,
                )

                val regularTaskReviewState = slot<String>()
                every {
                    planItemService.setPlanItemVariable(
                        "regularTaskPlanItemId",
                        "reviewState",
                        capture(regularTaskReviewState)
                    )
                } just Runs

                val reviewTaskReviewState = slot<String>()
                every {
                    planItemService.setPlanItemVariable(
                        "reviewTaskPlanItemId",
                        "reviewState",
                        capture(reviewTaskReviewState)
                    )
                } just Runs

                every { taskService.completeTask(any()) } just Runs

                fixture.approveReview(reviewTaskDto, principal)

                assertEquals(ReviewState.APPROVED, ReviewState.getByName(regularTaskReviewState.captured))
                assertEquals(ReviewState.APPROVED, ReviewState.getByName(reviewTaskReviewState.captured))

                verify(exactly = 2) { planItemService.setPlanItemVariable(any(), any(), any()) }
                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
                verify(exactly = 1) { taskService.getTaskDtoByPlanItemId(any()) }
                verify(exactly = 2) { taskService.completeTask(any()) }
                verify { caseInstanceService wasNot called }

            }

            else -> {  // all other states are not allowed
                val exception =
                    assertFailsWith<TaskReviewServiceException> { fixture.approveReview(reviewTaskDto, principal) }

                assertThat(exception.message).isEqualTo(
                    "Unsupported review state '${reviewTaskDto.reviewState.state}' for task review approval, " +
                            "supported states are '${ReviewState.IN_REVIEW.state}' and '${ReviewState.CORRECTED.state}'."
                )

                verify(exactly = 1) { taskService.getTaskDtoByPlanItemId(any()) }
                verify { planItemService wasNot called }
                verify { caseInstanceService wasNot called }

            }
        }
    }

    @Test
    fun `Test reject review`() {
        val reviewTaskDto = DtoFactory.taskDto(
            id = "reviewTaskId",
            planItemInstanceId = "reviewTaskPlanItemId",
            qualityLinkId = "regularTaskPlanItemId"
        )
        val principal = "testUser"

        every {
            planItemService.getPlanItemDtoOrNull("regularTaskPlanItemId")
        } returns DtoFactory.planItemDto(
            id = "regularTaskPlanItemId",
            qualityLinkId = "reviewTaskPlanItemId",
            reviewState = ReviewState.REJECTED,
        )

        val regularTaskReviewState = slot<String>()
        every {
            planItemService.setPlanItemVariable(
                "regularTaskPlanItemId",
                "reviewState",
                capture(regularTaskReviewState)
            )
        } just Runs

        val reviewTaskReviewState = slot<String>()
        every {
            planItemService.setPlanItemVariable(
                "reviewTaskPlanItemId",
                "reviewState",
                capture(reviewTaskReviewState)
            )
        } just Runs

        every { taskService.resolveTask(any()) } just Runs

        fixture.rejectReview(reviewTaskDto, principal)

        assertEquals(ReviewState.REJECTED, ReviewState.getByName(regularTaskReviewState.captured))
        assertEquals(ReviewState.REJECTED, ReviewState.getByName(reviewTaskReviewState.captured))

        verify(exactly = 2) { planItemService.setPlanItemVariable(any(), any(), any()) }
        verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
        verify(exactly = 1) { taskService.resolveTask(any()) }
        verify { caseInstanceService wasNot called }

    }

    @Test
    fun `Test correct task`() {
        val regularTaskDto = DtoFactory.taskDto(
            id = "regularTaskId",
            planItemInstanceId = "regularTaskPlanItemId",
            qualityLinkId = "reviewTaskPlanItemId",
            reviewState = ReviewState.REJECTED,
        )
        val principal = "testUser"

        every {
            planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
        } returns DtoFactory.planItemDto(
            id = "reviewTaskPlanItemId",
            qualityLinkKey = QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID,
            qualityLinkId = "regularTaskPlanItemId"
        )

        val regularTaskReviewState = slot<String>()
        every {
            planItemService.setPlanItemVariable(
                "regularTaskPlanItemId",
                "reviewState",
                capture(regularTaskReviewState)
            )
        } just Runs

        val reviewTaskReviewState = slot<String>()
        every {
            planItemService.setPlanItemVariable(
                "reviewTaskPlanItemId",
                "reviewState",
                capture(reviewTaskReviewState)
            )
        } just Runs

        every { taskService.resolveTask(regularTaskDto) } just Runs

        fixture.taskCorrected(regularTaskDto, principal)

        assertEquals(ReviewState.CORRECTED, ReviewState.getByName(regularTaskReviewState.captured))
        assertEquals(ReviewState.CORRECTED, ReviewState.getByName(reviewTaskReviewState.captured))

        verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
        verify(exactly = 2) { planItemService.setPlanItemVariable(any(), any(), any()) }
        verify(exactly = 1) { taskService.resolveTask(any()) }
        verify { caseInstanceService wasNot called }

    }

    private fun prepareReviewCascadeStubbing() {

        every {
            planItemService.getPlanItemDtoOrNull(any())
        } returns DtoFactory.planItemDto()

        every {
            planItemService.getEventListenersByCaseInstanceId("qualityShellId")
        } returns mutableListOf(
            DtoFactory.planItemDto(
                name = "Start task review",
                type = PlanItemType.GENERIC_EVENT_LISTENER,
                state = PlanItemState.AVAILABLE,
                isQualityShell = true,
            )
        )

        every {
            planItemService.getPlanItemsByStageAndDefinition(any())
        } returns mutableListOf(
            DtoFactory.planItemDto(
                name = "Review task",
                state = PlanItemState.AVAILABLE,
                isQualityShell = true,
            )
        )

        every {
            planItemService.completeGenericEventListener(any())
        } just Runs

        every {
            planItemService.setPlanItemVariable(any(), any(), any())
        } just Runs

    }

    companion object {

        @JvmStatic
        private fun getPlanItemTypes(): Set<String> = PlanItemType.values().map { it.type }.toSet()

        @JvmStatic
        private fun getReviewStatesNonNull(): Set<String> = ReviewState.values().map { it.state }.toSet()

        @JvmStatic
        private fun getReviewStates(): Set<String?> = getReviewStatesNonNull().plus(null)

    }

}