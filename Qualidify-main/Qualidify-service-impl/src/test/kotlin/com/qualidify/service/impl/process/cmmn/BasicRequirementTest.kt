package com.qualidify.service.impl.process.cmmn

import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.api.runtime.PlanItemInstance
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension


@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Disabled("Disabled because it is not testing Qualidify functionality, " +
        "but it is a showcase for CMMN (disable this line, to walk through")
class BasicRequirementTest(
    var cmmnRuntimeService: CmmnRuntimeService,
    var cmmnTaskService: CmmnTaskService,
) {

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/basicRequirement.cmmn.xml"])
    fun basicRequirementFlow() {
        val caseInstance: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("basicRequirement")
            .start()

        assertThat(caseInstance)
            .`as`("The CMMN instance basicRequirementFlow cannot be found.")
            .isNotNull

        //0 active tasks, all must be manually started
        var tasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()

        assertThat(tasks)
            .`as`("There must be 0 active tasks available at startup.")
            .isEmpty()

        var enabledPlanItems = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateEnabled()
            .list()

        assertThat(enabledPlanItems)
            .`as`("There must be 2 enabled planitems available at startup.")
            .hasSize(2)

        val addUserRequirementPlanInstance = filterPlanItemInstance(enabledPlanItems, "Add User Requirement")
        cmmnRuntimeService.startPlanItemInstance(addUserRequirementPlanInstance.id)

        tasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()

        assertThat(tasks)
            .`as`("There is a task enabled")
            .hasSize(1)
        assertThat((tasks[0].name))
            .`as`("The enabled task is 'Add User Requirement'")
            .isEqualTo("Add User Requirement")

        //complete the task
        cmmnTaskService.complete(tasks[0].id)

        val activeTasksCount = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .count()
        assertThat(activeTasksCount)
            .`as`("No tasks active anymore")
            .isZero
        val activePlanItemInstanceCount = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateEnabled()
            .count()
        assertThat(activePlanItemInstanceCount)
            .`as`("Still 2 planitems available (because of repetition rule)")
            .isEqualTo(2)

        //try to approve the stage
        val sendForApproval = cmmnRuntimeService.createPlanItemInstanceQuery()
            .planItemInstanceName("Send for approval").singleResult()
        cmmnRuntimeService.completeUserEventListenerInstance(sendForApproval.id)

        var activeTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()

        activeTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()
        assertThat(activeTasks)
            .`as`("There is a task enabled")
            .hasSize(1)
        assertThat(activeTasks[0].name)
            .`as`("The enabled task is 'Approve user requirements'")
            .isEqualTo("Approve user requirements")
        cmmnTaskService.complete(activeTasks[0].id)

        //we should be in the next stage at this moment, lets check
        enabledPlanItems = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateEnabled()
            .list()

        assertThat(enabledPlanItems)
            .`as`("There must be 2 enabled planitems available in Functional requirement phase.")
            .hasSize(2)
    }

    private fun filterPlanItemInstance(enabledPlanItems: List<PlanItemInstance>, name: String) =
        enabledPlanItems.asSequence().filter { it.name.equals(name) }
            .first()

}