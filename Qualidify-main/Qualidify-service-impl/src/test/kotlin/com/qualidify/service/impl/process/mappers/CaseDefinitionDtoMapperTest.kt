package com.qualidify.service.impl.process.mappers

import com.qualidify.model.entities.flowable.CmmnCaseDefinitionJpa
import com.qualidify.model.entities.flowable.CmmnDeploymentResourceJpa
import io.mockk.every
import io.mockk.mockk
import org.flowable.cmmn.api.CmmnRepositoryService
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

/**
 * A unit test for the [CaseDefinitionDtoMapper]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class CaseDefinitionDtoMapperTest {

    private val cmmnRepositoryService = mockk<CmmnRepositoryService>()

    private val fixture = CaseDefinitionDtoMapper(
        cmmnRepositoryService
    )

    @Test
    fun `Test convert CaseDefinitionEntity to CaseDefinitionDto`() {
        val result = fixture.convertToDto(caseDefinition())

        assertNotNull(result.id, "The dto id should not be empty")
        assertNotEquals("", result.id, "The dto id should not be empty")
        assertEquals("caseDefKey", result.key, "The dto key should be equal to: caseDefKey")
        assertNull(result.deploymentResourceId,"The dto graphical notation should not be filled")
    }

    @Test
    fun `Test convert CaseDefinitionEntity with Image to CaseDefinitionDto`() {

        every {
            cmmnRepositoryService.getCaseDiagram(any())
        } returns "graphicalNotation".byteInputStream()

        val deploymentResource = CmmnDeploymentResourceJpa(
            id = "id",
            name = "deploymentName",
            deploymentId = "deploymentId",
            graphicalNotation = "graphicalNotation".byteInputStream().readAllBytes()
        )

        val result = fixture.convertToDto(caseDefinition(deploymentResource))

        assertNotNull(result.id, "The dto id should not be empty")
        assertNotEquals("", result.id, "The dto id should not be empty")
        assertEquals("caseDefKey", result.key, "The dto key should be equal to: caseDefKey")
        assertNotNull(result.deploymentResourceId,"The dto graphical notation should be filled")
        assertEquals("graphicalNotation", String(result.graphicalNotation!!),
            "The dto graphical notation should be filled")

    }

    private fun caseDefinition(deploymentResource: CmmnDeploymentResourceJpa? = null) = CmmnCaseDefinitionJpa(
        id = UUID.randomUUID().toString(),
        name = "caseDefName",
        key = "caseDefKey",
        version = 1,
        deploymentResource = deploymentResource
    )

}