package com.qualidify.service.impl.process.mappers

import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.projects.ProjectInstanceService
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import java.util.*
import kotlin.test.assertEquals

/**
 * A unit test for the [CaseInstanceDtoMapper]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class CaseInstanceDtoMapperTest {

    private val caseDefinitionRepository = mockk<CmmnCaseDefinitionRepository>()
    private val projectInstanceService = mockk<ProjectInstanceService>()

    private val fixture =
        CaseInstanceDtoMapper(caseDefinitionRepository)

    private val projectInstanceId = UUID.randomUUID()
    private val regularCaseDefinition = TestEntities.cmmnCaseDefinitionJpa()
    private val qualityCaseDefinition = TestEntities.cmmnCaseDefinitionJpa(caseDefinitionKey = "cmmnQualityShell")
    private val regularCaseInstanceJpa = TestEntities.cmmnCaseInstanceJpa(
        id = "regularCaseInstanceId",
        projectInstanceId = projectInstanceId.toString()
    )
    private val regularCaseInstanceDto = DtoFactory.caseInstanceDto(
        "regularCaseInstanceId",
        projectInstanceId = projectInstanceId.toString(),
        isQualityShell = false,
        qualityShellId = "qualityShellId"
    )
    private val qualityShellCaseInstanceJpa = TestEntities.cmmnCaseInstanceJpa(
        id = "cmmnQualityShellId",
        projectInstanceId = projectInstanceId.toString(),
        isQualityShell = true,
        qualityShellId = null
    )
    private val qualityShellCaseInstanceDto = DtoFactory.caseInstanceDto(
        id = "cmmnQualityShellId",
        projectInstanceId = projectInstanceId.toString(),
        qualityLinkId = null,
        isQualityShell = true,
    )

    @AfterEach
    fun checkTest() {
        confirmVerified(caseDefinitionRepository, projectInstanceService)
        checkUnnecessaryStub(caseDefinitionRepository, projectInstanceService)
    }

    @Test
    fun `Test convert entity to dto with regular CmmnCaseInstance`() {
        every {
            caseDefinitionRepository.findById(any())
        } returns Optional.of(regularCaseDefinition)

        val result = fixture.convertToDto(regularCaseInstanceJpa)

        assertAll(
            { assertEquals("regularCaseInstanceId", result.id, "The id should be equal to 'regularCaseInstanceId'") },
            {
                assertEquals(
                    null,
                    result.qualityLinkId,
                    "The qualityLinkId should be 'cmmnQualityShellId' for regular case instances"
                )
            },
            {
                assertEquals(
                    "qualityShellId",
                    result.qualityShellId,
                    "The cmmnQualityShellId should be equal to 'cmmnQualityShellId'"
                )
            },
            {
                assertEquals(
                    regularCaseInstanceDto.caseDefinitionKey,
                    result.caseDefinitionKey,
                    "The caseDefinitionKey of the case instance should be 'caseDefinitionKey'"
                )
            },
            { assertThat(result).isEqualTo(regularCaseInstanceDto) }
        )

        verify(exactly = 1) { caseDefinitionRepository.findById(any()) }

    }

    @Test
    fun `Test convert entity to dto with quality shell CmmnCaseInstance`() {
        every {
            caseDefinitionRepository.findById(any())
        } returns Optional.of(qualityCaseDefinition)

        val result = fixture.convertToDto(qualityShellCaseInstanceJpa)

        assertAll(
            { assertEquals("cmmnQualityShellId", result.id, "The id should be equal to 'qualityShellId'") },
            {
                assertEquals(
                    null,
                    result.regularCaseId,
                    "The cmmnCaseInstanceId should be equal to 'null'"
                )
            },
            {
                assertEquals(
                    null,
                    result.qualityShellId,
                    "The cmmnQualityShellId should be 'null' for quality shell instances"
                )
            },
            {
                assertEquals(
                    qualityShellCaseInstanceDto.caseDefinitionKey,
                    result.caseDefinitionKey,
                    "The caseDefinitionKey of the quality shell should be 'cmmnQualityShell'"
                )
            },
            { assertThat(result).isEqualTo(qualityShellCaseInstanceDto) }
        )

        verify(exactly = 1) { caseDefinitionRepository.findById(any()) }

    }

}