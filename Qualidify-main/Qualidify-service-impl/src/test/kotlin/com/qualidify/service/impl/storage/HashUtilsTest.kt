package com.qualidify.service.impl.storage

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class HashUtilsTest {

    @Test
    fun `given a Byte Array, return the correct hash`() {
        val bytes = TestObject.getFileBytes()
        val hash = HashUtils.md5(bytes)
        assertThat(hash).`as`("Hash should be calculated correctly.").isEqualTo("A0C2BE846197F2267220B77BCBE98941")
    }
}