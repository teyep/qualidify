package com.qualidify.service.impl.security

import com.qualidify.service.security.AclRequest
import com.qualidify.service.security.dto.Permission
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.springframework.security.acls.domain.*
import org.springframework.security.acls.model.MutableAcl
import org.springframework.security.acls.model.MutableAclService
import org.springframework.security.acls.model.NotFoundException
import org.springframework.security.acls.model.ObjectIdentity
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(SpringExtension::class)
internal class AclTest {
    @Mock
    lateinit var aclService: MutableAclService

    @InjectMocks
    lateinit var fixture: AclRequestServiceImpl

    @Test
    @WithMockUser(value = "user", authorities = ["ROLE_3"])
    fun testAclCreated() {
        val acl = acl
        val aclArgument = ArgumentCaptor.forClass(MutableAcl::class.java)

        `when`(aclService.readAclById(OBJECT_IDENTITY)).thenThrow(NotFoundException(""))
        `when`(aclService.createAcl(OBJECT_IDENTITY)).thenReturn(acl)
        `when`(aclService.updateAcl(ArgumentMatchers.any(MutableAcl::class.java))).thenReturn(acl)

        fixture.create(aclRequest())

        verify(aclService, times(1)).readAclById(OBJECT_IDENTITY)
        verify(aclService, times(1)).createAcl(OBJECT_IDENTITY)
        verify(aclService, times(1)).updateAcl(aclArgument.capture())

        assertThat(aclArgument.value.entries.size).`as`("Acl must have 4 Aces").isEqualTo(4)

    }

    @Test
    @WithAnonymousUser
    fun testAclCreateWhenNotAuthorized() {

        `when`(aclService.readAclById(OBJECT_IDENTITY)).thenThrow(NotFoundException(""))
        `when`(aclService.createAcl(OBJECT_IDENTITY))
            .thenThrow(IllegalArgumentException("Authentication required"))

        assertThatThrownBy({
            fixture.create(aclRequest())
        }).isInstanceOf(IllegalArgumentException::class.java)
    }

    @Test
    @WithMockUser(value = "user", authorities = ["ROLE_3"])
    fun testAclUpdate() {
        val acl = acl
        acl.insertAce(0, BasePermission.CREATE, PrincipalSid("user"), true)
        val aclArgument = ArgumentCaptor.forClass(MutableAcl::class.java)

        `when`(aclService.readAclById(OBJECT_IDENTITY)).thenReturn(acl)
        `when`(aclService.updateAcl(ArgumentMatchers.any(MutableAcl::class.java)))
            .thenReturn(acl)

        fixture.update(aclRequest())

        verify(aclService, times(1)).readAclById(OBJECT_IDENTITY)
        verify(aclService, times(1)).updateAcl(aclArgument.capture())

        assertThat(aclArgument.value.entries.size).`as`("Updated Acl must have 4 Aces").isEqualTo(4)
    }

    @Test
    @WithMockUser(value = "user", authorities = ["ROLE_3"])
    fun testAclDeleted() {

        fixture.delete(aclRequest())

        verify(aclService, times(1)).deleteAcl(OBJECT_IDENTITY, true)
    }

    val acl: MutableAcl
        get() = AclImpl(
            OBJECT_IDENTITY, OBJECT_IDENTITY.identifier,
            { _, _: Int -> },
            ConsoleAuditLogger()
        )

    fun aclRequest(): AclRequest {
        return AclRequest(
            AclTest::class.java,
            OBJECT_IDENTITY.identifier as UUID,
            listOf(Permission.READ, Permission.CREATE),
            setOf("ROLE 1", "ROLE 2"),
            emptySet(),
            true
        )
    }


    companion object {
        private val OBJECT_IDENTITY: ObjectIdentity = ObjectIdentityImpl(AclTest::class.java, UUID.randomUUID())
    }
}