package com.qualidify.service.impl.process.cmmn

import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.form.api.FormRepositoryService
import org.flowable.form.api.FormService
import org.flowable.form.engine.test.FormDeploymentAnnotation
import org.flowable.form.spring.test.FlowableFormSpringExtension
import org.flowable.spring.impl.test.FlowableSpringExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@Extensions(
    ExtendWith(FlowableSpringExtension::class),
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(FlowableFormSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
class FormTest(
    var cmmnRuntimeService: CmmnRuntimeService,
    var cmmnTaskService: CmmnTaskService,
    var formService: FormService,
    var formRepository: FormRepositoryService,
) {

    @Test
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun simpleFormTest() {

        val formModel = formRepository.getFormModelByKey("form1")

        assertThat(formModel).`as`("Form model can be retrieved from the database").isNotNull
        assertThat(formModel.name).`as`("Form model is equal with form definition in testform.form")
            .isEqualTo("My first form")

        val formInstance = formService.getFormInstanceModelByKey(formModel.key, "taskId", null, null)

        assertThat(formInstance).`as`("Form instance is created").isNotNull
        assertThat(formInstance.id).`as`("Form instance id is equal to model")
            .isEqualTo(formModel.id)
        assertThat(formInstance.name).`as`("Form instance name is equal to definition in testform.form")
            .isEqualTo("My first form")
    }

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/smallRequirement.cmmn.xml"])
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun cmmnFormConnectionTest() {
        val caseInstance: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("smallRequirement")
            .parentId("anyIdButNullShouldBeFine")
            .start()
        assertThat(caseInstance).`as`("The CMMN instance can be found.").isNotNull
        assertThat(caseInstance.state).`as`("The started cmmn is active").isEqualTo("active")

        val tasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()
        assertThat(tasks).`as`("There must be one tasks available at startup.").hasSize(1)

        val inputRequirementTask = tasks[0]
        val taskFormModel = cmmnTaskService.getTaskFormModel(inputRequirementTask.id)

        assertThat(taskFormModel).`as`("Form model can be retrieved from the database").isNotNull
        assertThat(taskFormModel.name).`as`("Form model is equal with form definition in testform.form")
            .isEqualTo("My first form")

        val taskFormInstance =
            formService.getFormInstanceModelByKey(taskFormModel.key, inputRequirementTask.id, null, null)
        assertThat(taskFormInstance).`as`("Form instance is created").isNotNull
        assertThat(taskFormInstance.id).`as`("Form instance id is equal to model")
            .isEqualTo(taskFormModel.id)
        assertThat(taskFormInstance.name).`as`("Form instance name is equal to definition in testform.form")
            .isEqualTo("My first form")

    }

    @Test
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun `SimpleFormTest with datastorage`() {

        val formInstance = formService.getFormInstanceModelByKey("form1", "taskId", null, null)

        assertThat(formInstance).`as`("Form instance is created").isNotNull
        assertThat(formInstance.name).`as`("Form instance name is equal to definition in testform.form")
            .isEqualTo("My first form")

        val data = mapOf("input1" to "TESTDATA")

        formService.saveFormInstanceByFormDefinitionId(data, formInstance.id, "taskid", null, null, null, null)

        formService.getFormInstanceModelByKey("form1", "taskId", null, null)

    }
}