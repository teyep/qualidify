package com.qualidify.service.impl.projects

import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.mappers.ProjectTemplateDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.projects.dto.ProjectTemplateDto
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnRuntimeService
import org.junit.jupiter.api.Test
import org.springframework.data.domain.Page
import java.util.*
import kotlin.test.assertContains

/**
 * A unit test for the [ProjectTemplateServiceImpl]
 *
 * @author Menno Tol
 * @author Marcel Pot
 * @since 1.0
 */
internal class ProjectTemplateServiceImplTest {

    private val cmmnCaseDefinitionRepository = mockk<CmmnCaseDefinitionRepository>()
    private val cmmnRuntimeService = mockk<CmmnRuntimeService>()
    private val projectTemplateRepository = mockk<ProjectTemplateRepository>()
    private val projectInstanceStarter = mockk<ProjectInstanceStarter>()

    private val projectTemplateDtoMapper =
        spyk(ProjectTemplateDtoMapper(cmmnCaseDefinitionRepository))

    private val fixture = ProjectTemplateServiceImpl(
        projectTemplateRepository, projectTemplateDtoMapper, projectInstanceStarter
    )

    @Test
    fun `Test findByQuery is empty`() {
        mockkStatic(ProjectTemplateRepository::findAllByQuery)

        every {
            projectTemplateRepository.findAllByQuery(SearchQuery<ProjectTemplateDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<ProjectTemplateDto>())

        assertThat(result).`as`("The list of projects should be empty").isEmpty()

        verify { cmmnRuntimeService wasNot called }

    }

    @Test
    fun `Test startProject service call`() {
        val id = UUID.randomUUID()
        val projectTemplateDto = DtoFactory.projectTemplateDto(id.toString())

        every {
            projectInstanceStarter.start(any())
        } returns TestEntities.projectInstance(id = id)

        val result = fixture.startProject(projectTemplateDto)

        assertContains(result.toString(), id.toString())

        verify { projectTemplateRepository wasNot called }

    }

}