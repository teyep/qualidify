package com.qualidify.service.impl.reports

import com.qualidify.model.entities.BatchJobArtifact
import com.qualidify.model.entities.BatchJobId
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

/**
 * Unit test suite for SenderSideBatchJobServiceImpl
 */
@ExtendWith(MockitoExtension::class)
internal class SenderSideBatchJobServiceImplTest {

    @Mock
    lateinit var batchJobRepository: BatchJobRepository
    @Mock
    lateinit var batchJobArtifactRepository: BatchJobArtifactRepository
    @Mock
    lateinit var batchJobDtoMapper: BatchJobDtoMapper
    @Mock
    lateinit var batchJobArtifactDtoMapper: BatchJobArtifactDtoMapper
    @Mock
    lateinit var reportTemplateBatchJobConversionService: ReportTemplateBatchJobConversionServiceImpl

    lateinit var fixture: SenderSideBatchJobServiceImpl

    @BeforeEach
    fun setUp() {
        fixture = SenderSideBatchJobServiceImpl(batchJobRepository, batchJobArtifactRepository, batchJobDtoMapper, batchJobArtifactDtoMapper, reportTemplateBatchJobConversionService)
    }

    @Test
    fun `getById returns null when batch job is not found`() {
        `when`(batchJobRepository.findById(any(BatchJobId::class.java))).thenReturn(Optional.empty())
        assertThat(fixture.getById(UUID.randomUUID())).isEqualTo(null)
    }

    @Test
    fun `hasArtifact returns true when artifact has been found`() {
        val testId = UUID.randomUUID()
        val entity = BatchJobArtifact(BatchJobId(testId), 0, "test/plain", "test".toByteArray(), Date())
        `when`(batchJobArtifactRepository.findById(BatchJobId(testId))).thenReturn(Optional.of(entity))
        assertThat(fixture.hasArtifact(testId)).isTrue;
    }

    @Test
    fun `hasArtifact returns false when artifact has not been found`() {
        `when`(batchJobArtifactRepository.findById(any(BatchJobId::class.java))).thenReturn(Optional.empty())
        assertThat(fixture.hasArtifact(UUID.randomUUID())).isFalse
    }

}