package com.qualidify.service.impl.storage

import com.qualidify.model.entities.TempFileStorageId
import com.qualidify.service.storage.exceptions.StorageFileException
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.core.io.Resource
import org.springframework.mock.web.MockMultipartFile
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.*


@ExtendWith(MockitoExtension::class)
internal class TemporaryFileStorageServiceTest {

    @Mock
    lateinit var repository: TempFileStorageRepository

    @InjectMocks
    lateinit var fixture: TemporaryFileStorageServiceImpl

    @Test
    fun `given a File, when saved it return the hash`() {
        val bytes = TestObject.getFileBytes()

        val hash = HashUtils.md5(bytes)
        val file = MockMultipartFile("test.txt", "test.txt", "text/plain", bytes)
        val tempFileStorage = TestObject.getTempFileStorage()

        `when`(repository.save(tempFileStorage)).thenReturn(tempFileStorage)

        val returnedHash = fixture.store(file)

        assertThat(returnedHash).`as`("Returned hash must be equal to calculated hash").isEqualTo(hash)

        verify(repository, times(1))!!.save(tempFileStorage)
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `given a hash, when asked it return the file`() {
        val bytes = TestObject.getFileBytes()

        val hash = HashUtils.md5(bytes)
        val tempFileStorage = TestObject.getTempFileStorage()

        `when`(repository.findById(TempFileStorageId(hash))).thenReturn(Optional.of(tempFileStorage))

        val inputStream = fixture.load(hash)

        assertThat(String(inputStream.readAllBytes(), StandardCharsets.UTF_8))
            .`as`("Returned inputstream must be equal to bytes")
            .isEqualTo(String(bytes, StandardCharsets.UTF_8))

        verify(repository, times(1))!!.findById(TempFileStorageId(hash))
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `given a nonexsting hash, when asked it returns an exception`() {
        val bytes = TestObject.getFileBytes()
        val hash = HashUtils.md5(bytes)

        `when`(repository.findById(TempFileStorageId(hash))).thenReturn(Optional.empty())

        Assertions.assertThatThrownBy {
            fixture.load(hash)
        }.isInstanceOf(
            StorageFileException::class.java
        ).hasMessage("Could not find file with hash $hash.")

        verify(repository, times(1))!!.findById(TempFileStorageId(hash))
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `given a hash, when asked it return the file as Resource`() {
        val bytes = TestObject.getFileBytes()

        val hash = HashUtils.md5(bytes)
        val tempFileStorage = TestObject.getTempFileStorage()

        `when`(repository.findById(TempFileStorageId(hash))).thenReturn(Optional.of(tempFileStorage))

        val resource = fixture.loadAsResource(hash)

        assertThat(resourceToString(resource))
            .`as`("Returned Resource must be equal to classpath resource")
            .isEqualTo(resourceToString(TestObject.getFileResource()))

        verify(repository, times(1))!!.findById(TempFileStorageId(hash))
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `given a nonexsting hash, when asked for a resource it returns an exception`() {
        val bytes = TestObject.getFileBytes()
        val hash = HashUtils.md5(bytes)

        `when`(repository.findById(TempFileStorageId(hash))).thenReturn(Optional.empty())

        Assertions.assertThatThrownBy {
            fixture.loadAsResource(hash)
        }.isInstanceOf(
            StorageFileException::class.java
        ).hasMessage("Could not find file with hash $hash.")

        verify(repository, times(1))!!.findById(TempFileStorageId(hash))
        verifyNoMoreInteractions(repository)
    }

    fun inputStreamToString(input: InputStream): String {
        return String(input.readAllBytes(), StandardCharsets.UTF_8);
    }

    fun resourceToString(input: Resource): String {
        return inputStreamToString(input.inputStream)
    }

}