package com.qualidify.service.impl.configuration

import com.qualidify.metadata.DataType
import com.qualidify.model.entities.ConfigurationCode
import com.qualidify.model.entities.ConfigurationDomain
import com.qualidify.model.entities.ConfigurationDomainId
import com.qualidify.service.configuration.ConfigurationDomainNotFoundException
import com.qualidify.service.configuration.dto.ConfigurationCodeDto
import com.qualidify.service.configuration.dto.ConfigurationDomainDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentCaptor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
class ConfigurationServiceTest {

    @Mock
    lateinit var repository: ConfigurationDomainRepository

    @Mock
    lateinit var mapper: ConfigurationCodeDtoMapper

    @InjectMocks
    lateinit var fixture: ConfigurationServiceImpl

    @Test
    fun `Create a new domain`() {
        val domain = ConfigurationDomainDto("TestDomain", "Description of TestDomain")
        val domainEntity = ConfigurationDomain("TestDomain", "Description of TestDomain")
        `when`(repository.save(domainEntity))
            .thenReturn(domainEntity)

        fixture.create(domain)

        verify(repository, times(1)).save(domainEntity)
        verifyNoMoreInteractions(repository)
        verifyNoInteractions(mapper)
    }

    @Test
    fun `Create a new domain with a empty domainname`() {
        val domain = ConfigurationDomainDto("", "Description of TestDomain")

        assertThatThrownBy {
            fixture.create(domain)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Configuration Domain must have a name.")

        verifyNoInteractions(repository, mapper)
    }

    @Test
    fun `Create a new domain with a empty description`() {
        val domain = ConfigurationDomainDto("TestDomain", "")

        assertThatThrownBy {
            fixture.create(domain)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Configuration Domain must have a description.")

        verifyNoInteractions(repository, mapper)
    }

    @Test
    fun `Update an existing domain`() {


        val domain = ConfigurationDomainDto("TestDomain", "A much better description of TestDomain")
        val domainEntity = ConfigurationDomain("TestDomain", "A much better description of TestDomain")


        `when`(repository.save(domainEntity))
            .thenReturn(domainEntity)

        fixture.update(domain)

        verify(repository, times(1)).save(domainEntity)

        verifyNoMoreInteractions(repository)
        verifyNoInteractions(mapper)
    }

    @Test
    fun `Update an existing domain with empty descriptin`() {
        val domain = ConfigurationDomainDto("TestDomain", "")

        assertThatThrownBy {
            fixture.update(domain)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Configuration Domain must have a description.")

        verifyNoInteractions(repository, mapper)
    }

    @Test
    fun `Delete a domain`() {
        val domain = ConfigurationDomainDto("TestDomain", "")
        val entityId = ConfigurationDomainId(domain.domain)

        fixture.delete(domain)

        verify(repository).deleteById(entityId)
        verifyNoMoreInteractions(repository)
        verifyNoInteractions(mapper)
    }

    @Test
    fun `Create a configurationCode`() {
        val domain = ConfigurationDomainDto("TestDomain", "Description of TestDomain")
        val domainEntity = ConfigurationDomain("TestDomain", "Description of TestDomain")
        val code = ConfigurationCodeDto(
            domain.domain,
            "TestCode",
            "A code for testing",
            DataType.BOOLEAN,
            "TRUE",
            true
        )
        val codeEntity = ConfigurationCode(
            "TestDomain",
            "TestCode",
            "A code for testing",
            DataType.BOOLEAN,
            "TRUE"
        )

        doReturn(domainEntity).`when`(repository).save(domainEntity)

        `when`(repository.findById(ConfigurationDomainId(domain.domain)))
            .thenReturn(Optional.of(domainEntity))
        `when`(mapper.convertToModel(code))
            .thenReturn(codeEntity)

        val configurationDomainCapture = ArgumentCaptor.forClass(ConfigurationDomain::class.java)



        fixture.create(domain)
        fixture.createCode(code)



        verify(repository, times(2)).save(configurationDomainCapture.capture())
        verify(repository, times(1)).findById(ConfigurationDomainId(domain.domain))
        verify(mapper, times(1)).convertToModel(code)

        verifyNoMoreInteractions(repository, mapper)

        assertThat(configurationDomainCapture.value.codes).contains(codeEntity)
    }

    @Test
    fun `Update a configurationCode`() {
        val domain = ConfigurationDomainDto("TestDomain", "Description of TestDomain")
        val domainEntity = ConfigurationDomain(
            "TestDomain", "Description of TestDomain",
            mutableListOf(
                ConfigurationCode(
                    "TestDomain",
                    "TestCode",
                    "A code for testing",
                    DataType.BOOLEAN,
                    "TRUE"
                )
            )
        )
        val codeEntity = ConfigurationCode(
            "TestDomain",
            "TestCode",
            "A code for testing",
            DataType.BOOLEAN,
            "FALSE"
        )
        val updatedDomainEntity = domainEntity.also {
            it.codes = mutableListOf(codeEntity)
        }
        val code = ConfigurationCodeDto(
            domain.domain,
            "TestCode",
            "A code for testing",
            DataType.BOOLEAN,
            "FALSE",
            true
        )

        doReturn(Optional.of(domainEntity))
            .`when`(repository).findById(ConfigurationDomainId(domain.domain))
        doReturn(updatedDomainEntity)
            .`when`(repository).save(updatedDomainEntity)

        fixture.updateCode(code)

        verify(repository, times(1)).save(domainEntity)
        verify(repository, times(1)).findById(ConfigurationDomainId(domain.domain))
        verify(repository, times(1)).save(updatedDomainEntity)

        verifyNoMoreInteractions(repository)
        verifyNoInteractions(mapper)
    }

    @Test
    fun `Create a configurationCode without domain`() {
        val code = ConfigurationCodeDto(
            "",
            "TestCode",
            "A code for testing",
            DataType.BOOLEAN,
            "TRUE",
            true
        )
        assertThatThrownBy {
            fixture.createCode(code)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Configuration code must have a configurationDomain.")
    }

    @Test
    fun `Create a configurationCode without code`() {
        val code = ConfigurationCodeDto(
            "TestDomain",
            "",
            "A code for testing",
            DataType.BOOLEAN,
            "TRUE",
            true
        )
        assertThatThrownBy {
            fixture.createCode(code)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Configuration code must have a value.")
    }

    @Test
    fun `Create a configurationCode without description`() {
        val code = ConfigurationCodeDto(
            "TestDomain",
            "TestCode",
            "",
            DataType.BOOLEAN,
            "TRUE",
            true
        )
        assertThatThrownBy {
            fixture.createCode(code)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Configuration code must have a description.")
    }

    @Test
    fun `Create a configurationCode without existing domain`() {
        val code = ConfigurationCodeDto(
            "NotExisting",
            "TestCode",
            "A code for testing",
            DataType.BOOLEAN,
            "TRUE",
            true
        )

        `when`(repository.findById(ConfigurationDomainId("NotExisting"))).thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.createCode(code)
        }.isInstanceOf(ConfigurationDomainNotFoundException::class.java)
            .hasMessage("The configurationDomain NotExisting cannot be found!")
    }
}