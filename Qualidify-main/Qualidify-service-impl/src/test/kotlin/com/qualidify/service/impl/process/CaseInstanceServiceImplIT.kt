package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.service.impl.process.mappers.CaseInstanceDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.impl.process.repositories.CmmnHistoricCaseInstanceRepository
import com.qualidify.service.process.dto.CaseInstanceDto
import org.flowable.cmmn.api.CmmnHistoryService
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.CmmnEngine
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.spring.impl.test.FlowableSpringExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.test.assertEquals

/**
 * Integration test for the [CaseInstanceServiceImpl] to test counting and filtering of dto properties that
 * are not implemented in flowable queries
 *
 * @author Menno Tol
 * @since 1.0
 */
@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(FlowableSpringExtension::class),
    ExtendWith(SpringExtension::class),
)
@SpringBootTest
internal class CaseInstanceServiceImplIT(
    @Autowired private val cmmnCaseInstanceRepository: CmmnCaseInstanceRepository,
    @Autowired private val cmmnHistoricCaseInstanceRepository: CmmnHistoricCaseInstanceRepository,
    @Autowired private val caseInstanceDtoMapper: CaseInstanceDtoMapper,
    private val cmmnEngine: CmmnEngine,
    private val cmmnHistoryService: CmmnHistoryService,
    private val cmmnRuntimeService: CmmnRuntimeService,
) {

    private val fixture = CaseInstanceServiceImpl(
        cmmnCaseInstanceRepository,
        cmmnHistoricCaseInstanceRepository,
        caseInstanceDtoMapper,
        cmmnEngine,
        cmmnRuntimeService,
        cmmnHistoryService,
    )

    @Test
    @CmmnDeployment(
        resources = [
            "dev-resources/cases/cmmnQualityShell.cmmn.xml",
            "dev-resources/cases/employeeOnboarding.cmmn.xml",
            "dev-resources/cases/basicRequirement.cmmn.xml",
            "dev-resources/cases/smallRequirement.cmmn.xml",
        ]
    )
    fun `Test count query of case instances tasks by filtering`() {
        cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("cmmnQualityShell")
            .variable("regularCaseDefinitionKey", "employeeOnboarding")
            .variable("potentialEmployee", "test.employee")
            .start()
        cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("cmmnQualityShell")
            .variable("regularCaseDefinitionKey", "smallRequirement")
            .start()
        cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("cmmnQualityShell")
            .variable("regularCaseDefinitionKey", "basicRequirement")
            .start()

        // bypassing qualidify service
        val flowableCaseInstances = cmmnRuntimeService.createCaseInstanceQuery().list()
        assertEquals(6, flowableCaseInstances.size)  // 3 regular + 3 quality shell instances

        // without constraints
        var countQuery = fixture.countByQuery(CountQuery<CaseInstanceDto>())
//        var searchQuery = fixture.findByQuery(SearchQuery<CaseInstanceDto>())
        assertEquals(6, countQuery.count.toInt())  //, searchQuery.size)

        // with constraint 'state' = 'active'
        countQuery =
            fixture.countByQuery(CountQuery<CaseInstanceDto>(constraints = mapOf("state" to "active")))
//        searchQuery =
//            fixture.findByQuery(SearchQuery<CaseInstanceDto>(constraints = mapOf("state" to "active")))
        assertEquals(6, countQuery.count.toInt())  //, searchQuery.size)

        // with constraint 'state' = 'terminated'
        countQuery =
            fixture.countByQuery(CountQuery<CaseInstanceDto>(constraints = mapOf("state" to "terminated")))
//        searchQuery =
//            fixture.findByQuery(SearchQuery<CaseInstanceDto>(constraints = mapOf("state" to "terminated")))
        assertEquals(0, countQuery.count.toInt())  //, searchQuery.size)

    }

}