package com.qualidify.service.impl.process

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.flowable.BpmnDefinitionJpa
import com.qualidify.service.impl.process.repositories.BpmnDefinitionRepository
import com.qualidify.service.process.dto.BusinessProcessDefinitionDto
import io.mockk.*
import org.assertj.core.api.Assertions
import org.flowable.engine.ProcessEngine
import org.flowable.engine.runtime.ProcessInstance
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

/**
 * A unit test for the [BpmnDefinitionServiceImpl]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class BpmnDefinitionServiceImplTest {
    private val logger by LoggerDelegation()
    private val mockBpmnDefinitionRepository = mockk<BpmnDefinitionRepository>()
    private val mockProcessEngine = mockk<ProcessEngine>()

    private val fixture = BpmnDefinitionServiceImpl(mockBpmnDefinitionRepository, mockProcessEngine)

    private val dto = createDto()
    private val entity = createEntity()

    @Test
    fun `Test countByQuery`() {
        mockkStatic(BpmnDefinitionRepository::countResponse)

        every {
            mockBpmnDefinitionRepository.countResponse(CountQuery<BusinessProcessDefinitionDto>())
        } returns CountResponse(3)

        val result = fixture.countByQuery(CountQuery<BusinessProcessDefinitionDto>())

        assertEquals(3, result.count)

        verify { mockProcessEngine wasNot called }
        verify(exactly = 1) { mockBpmnDefinitionRepository.countResponse(CountQuery<BusinessProcessDefinitionDto>()) }
    }

    @Test
    fun `Test findByQuery is empty`() {
        mockkStatic(BpmnDefinitionRepository::findAllByQuery)

        every {
            mockBpmnDefinitionRepository.findAllByQuery(SearchQuery<BusinessProcessDefinitionDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<BusinessProcessDefinitionDto>())

        assertContentEquals(emptyList(), result)

        verify { mockProcessEngine wasNot called }

        verify(exactly = 1) {
            mockBpmnDefinitionRepository.findAllByQuery(SearchQuery<BusinessProcessDefinitionDto>())
        }
    }

    @ParameterizedTest
    @MethodSource("getDtoProperties")
    fun `Test query constraints mapping`(property: String) {
        mockkStatic(BpmnDefinitionRepository::findAllByQuery)

        val searchQuery = SearchQuery<BusinessProcessDefinitionDto>(constraints = mapOf(property to "randomValue"))
        every {
            mockBpmnDefinitionRepository.findAllByQuery(searchQuery)
        } returns PageImpl(listOf(entity))

        val results = fixture.findByQuery(searchQuery)

        Assertions.assertThat(results.size).isEqualTo(1)
        assertContentEquals(results, listOf(dto))

        verify { mockProcessEngine wasNot called }
        verify(exactly = 1) {
            mockBpmnDefinitionRepository.findAllByQuery(searchQuery)
        }
    }

    @Test
    fun `Test of starting a new Bpmn Process`() {
        every {
            mockProcessEngine.runtimeService.startProcessInstanceById(dto.id)
        } returns mockk<ProcessInstance>()

        fixture.startProcess(dto)

        verify { mockBpmnDefinitionRepository wasNot called }
        verify(exactly = 1) { mockProcessEngine.runtimeService.startProcessInstanceById(dto.id) }
    }

    private fun createDto(): BusinessProcessDefinitionDto {
        return BusinessProcessDefinitionDto(
                id = "mockProcessDefId",
                key = "mockProcessDefKey",
                name = "mockProcessDefName",
                version = 1,
                graphicalNotation = true
        )
    }

    private fun createEntity() = BpmnDefinitionJpa(
            id = "mockProcessDefId",
            key = "mockProcessDefKey",
            name = "mockProcessDefName",
            version = 1,
            hasGraphicalNotation = true

    )

    companion object {

        @JvmStatic
        private fun getDtoProperties(): Set<String> {
            return BusinessProcessDefinitionDto::class.declaredMemberProperties
                    .map { it.name }
                    .toSet()
        }

    }

}