package com.qualidify.service.impl.storage

import com.qualidify.model.entities.TempFileStorage
import com.qualidify.model.entities.TempFileStorageId
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource

object TestObject {

    fun getFileBytes(): ByteArray {
        val classPathResource = getFileResource()
        return classPathResource.inputStream.readAllBytes()
    }

    fun getFileResource(): Resource {
        return ClassPathResource("/test.txt")
    }

    fun getTempFileStorage(): TempFileStorage {
        val bytes = getFileBytes()
        val hash = HashUtils.md5(bytes)

        return TempFileStorage(TempFileStorageId(hash), bytes)
    }
}