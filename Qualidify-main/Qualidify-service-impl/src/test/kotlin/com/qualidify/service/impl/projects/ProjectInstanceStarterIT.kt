package com.qualidify.service.impl.projects

import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.impl.quality.ReviewStateFiller
import com.qualidify.service.impl.user.UserDataRepository
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional
internal open class ProjectInstanceStarterIT(
    private val cmmnRuntimeService: CmmnRuntimeService,
    @Autowired private val projectInstanceRepository: ProjectInstanceRepository,
    @Autowired private val userDataRepository: UserDataRepository,
    @Autowired private val cmmnCaseInstanceRepository: CmmnCaseInstanceRepository,
    @Autowired private val reviewStateFiller: ReviewStateFiller,
) {

    private val projectInstanceStarter =
        ProjectInstanceStarter(cmmnRuntimeService, projectInstanceRepository, userDataRepository, reviewStateFiller)
    private val projectTemplateDto = DtoFactory.projectTemplateDto(cmmnModel = "employeeOnboarding")

    @Test
    @CmmnDeployment(
        resources = [
            "dev-resources/cases/employeeOnboarding.cmmn.xml",
            "dev-resources/cases/cmmnQualityShell.cmmn.xml",
        ]
    )
    fun `Test projectInstanceStarter`() {
        val project = projectInstanceStarter.start(projectTemplateDto)

        val qualityShell = cmmnCaseInstanceRepository.findById(project.qualityShellId).get()
        val caseInstance = cmmnCaseInstanceRepository.findById(project.regularCaseId).get()
        assertAll(
            { assertEquals("cmmnQualityShell", qualityShell.cmmnCaseDefinition.key) },
            { assertEquals("employeeOnboarding", caseInstance.cmmnCaseDefinition.key) },
        )

        val qualityShellVariables = cmmnRuntimeService.createCaseInstanceQuery()
            .caseInstanceId(project.qualityShellId)
            .includeCaseVariables()
            .singleResult()
            .caseVariables
        val caseInstanceVariables = cmmnRuntimeService.createCaseInstanceQuery()
            .caseInstanceId(project.regularCaseId)
            .includeCaseVariables()
            .singleResult()
            .caseVariables
        assertAll(
            { assertEquals(project.id.id.toString(), qualityShellVariables["projectInstanceId"]) },
            { assertEquals(true, qualityShellVariables["isQualityShell"]) },
            { assertEquals(caseInstance.id, qualityShellVariables["cmmnCaseInstanceId"]) },
            { assertEquals(project.id.id.toString(), caseInstanceVariables["projectInstanceId"]) },
            { assertEquals(false, caseInstanceVariables["isQualityShell"]) },
            { assertEquals(qualityShell.id, caseInstanceVariables["cmmnQualityShellId"]) },
        )

    }

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/employeeOnboarding.cmmn.xml"])
    fun `Test projectInstanceStarter without qualityShell`() {
        val exception = assertFailsWith<QualityShellNotFoundException> {
            projectInstanceStarter.start(projectTemplateDto)
        }
        assertEquals(
            "The cmmn quality shell case definition (key: 'cmmnQualityShell') could not be found",
            exception.message
        )
    }

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/cmmnQualityShell.cmmn.xml"])
    fun `Test projectInstanceStarter without regular caseModel`() {
        val exception = assertFailsWith<CaseDefinitionNotFoundException> {
            projectInstanceStarter.start(projectTemplateDto)
        }
        assertEquals("The case definition for key 'employeeOnboarding' could not be found.", exception.message)
    }

    @Test
    @CmmnDeployment(
        resources = [
            "dev-resources/cases/employeeOnboarding.cmmn.xml",
            "dev-resources/cases/cmmnQualityShell.cmmn.xml",
        ]
    )
    fun `Test projectInstanceStarter without regularCaseDefinitionKey provided`() {
        val wrongProjectTemplateDto = projectTemplateDto.copy(cmmnModel = "")
        val exception = assertFailsWith<NoCaseDefinitionProvidedException> {
            projectInstanceStarter.start(wrongProjectTemplateDto)
        }
        assertEquals(
            "The cmmnModel attribute of dto '$wrongProjectTemplateDto' is empty," +
                    " please provide a valid cmmnModel", exception.message
        )
    }

}