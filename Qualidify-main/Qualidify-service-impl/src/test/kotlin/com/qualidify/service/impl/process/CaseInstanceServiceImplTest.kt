package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.mappers.CaseInstanceDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.impl.process.repositories.CmmnHistoricCaseInstanceRepository
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.exceptions.CaseInstanceNotFoundException
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.flowable.cmmn.api.CmmnHistoryService
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.CmmnEngine
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import java.util.*
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

/**
 * A unit test for the [CaseInstanceServiceImpl]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class CaseInstanceServiceImplTest {

    private val cmmnCaseInstanceRepository = mockk<CmmnCaseInstanceRepository>()
    private val cmmnCaseDefinitionRepository = mockk<CmmnCaseDefinitionRepository>()
    private val cmmnHistoricCaseInstanceRepository = mockk<CmmnHistoricCaseInstanceRepository>()
    private val cmmnRuntimeService = mockk<CmmnRuntimeService>()
    private val cmmnHistoryService = mockk<CmmnHistoryService>()
    private val cmmnEngine = mockk<CmmnEngine>()

    private val caseInstanceDtoMapper = spyk(CaseInstanceDtoMapper(cmmnCaseDefinitionRepository))

    private val fixture = CaseInstanceServiceImpl(
        cmmnCaseInstanceRepository,
        cmmnHistoricCaseInstanceRepository,
        caseInstanceDtoMapper,
        cmmnEngine,
        cmmnRuntimeService,
        cmmnHistoryService,
    )

    private val projectInstanceId = UUID.randomUUID()
    private val dto = DtoFactory.caseInstanceDto(projectInstanceId = projectInstanceId.toString())
    private val cmmnCaseInstanceJpa = TestEntities.cmmnCaseInstanceJpa(
        projectInstanceId = projectInstanceId.toString(),
    )
    private val cmmnCaseDefinitionJpa = TestEntities.cmmnCaseDefinitionJpa()

    @AfterEach
    fun checkTest() {
        confirmVerified(
            cmmnCaseInstanceRepository,
            cmmnCaseDefinitionRepository,
            cmmnHistoricCaseInstanceRepository,
            cmmnEngine,
            cmmnRuntimeService,
            cmmnHistoryService,
        )
        checkUnnecessaryStub(
            cmmnCaseInstanceRepository,
            cmmnCaseDefinitionRepository,
            cmmnHistoricCaseInstanceRepository,
            cmmnEngine,
            cmmnRuntimeService,
            cmmnHistoryService,
        )
    }

    @Test
    fun `Test countByQuery`() {
        mockkStatic(CmmnCaseInstanceRepository::countResponse)

        every {
            cmmnCaseInstanceRepository.countResponse(CountQuery<CaseInstanceDto>())
        } returns CountResponse(2)

        val result = fixture.countByQuery(CountQuery<CaseInstanceDto>())

        assertEquals(2, result.count)

        verify(exactly = 1) { cmmnCaseInstanceRepository.countResponse(CountQuery<CaseInstanceDto>()) }

    }

    @Test
    fun `Test findQuery is empty`() {
        mockkStatic(CmmnCaseInstanceRepository::findAllByQuery)

        every {
            cmmnCaseInstanceRepository.findAllByQuery(SearchQuery<CaseInstanceDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<CaseInstanceDto>())

        assertContentEquals(emptyList(), result)

        verify(exactly = 1) { cmmnCaseInstanceRepository.findAllByQuery(SearchQuery<CaseInstanceDto>()) }

    }

    @ParameterizedTest
    @MethodSource("getDtoProperties")
    fun `Test query constraints mapping`(property: String) {
        mockkStatic(CmmnCaseInstanceRepository::findAllByQuery)

        val searchQuery = SearchQuery<CaseInstanceDto>(constraints = mapOf(property to "randomValue"))
        every {
            cmmnCaseInstanceRepository.findAllByQuery(searchQuery)
        } returns PageImpl(mutableListOf(cmmnCaseInstanceJpa))

        every {
            cmmnCaseDefinitionRepository.findById(any())
        } returns Optional.of(cmmnCaseDefinitionJpa)

        val results = fixture.findByQuery(searchQuery)

        assertThat(results.size).isOne

        verify(exactly = 1) { cmmnCaseInstanceRepository.findAllByQuery(searchQuery) }
        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }

    }

    @Test
    fun `Test find caseInstance by Id`() {
        every {
            cmmnCaseInstanceRepository.findById("TESTID")
        } returns Optional.of(cmmnCaseInstanceJpa)

        every {
            cmmnCaseInstanceRepository.findById("NOT_EXISTING")
        } returns Optional.empty()

        every {
            cmmnCaseDefinitionRepository.findById(any())
        } returns Optional.of(cmmnCaseDefinitionJpa)

        val firstResult = fixture.getCaseInstanceDtoById("TESTID")
        assertThat(firstResult).isEqualTo(dto)

        assertThatThrownBy {
            fixture.getCaseInstanceDtoById("NOT_EXISTING")
        }.isExactlyInstanceOf(CaseInstanceNotFoundException::class.java)
            .hasMessage("Cannot find a case instance with id=NOT_EXISTING.")

        verify(exactly = 2) { cmmnCaseInstanceRepository.findById(any()) }
        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }

    }

    companion object {

        @JvmStatic
        private fun getDtoProperties(): Set<String> {
            return CaseInstanceDto::class.declaredMemberProperties
                .map { it.name }
                .toSet()
        }

    }

}