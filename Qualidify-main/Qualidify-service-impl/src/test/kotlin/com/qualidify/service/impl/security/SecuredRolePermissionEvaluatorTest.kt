package com.qualidify.service.impl.security

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.security.authentication.TestingAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import javax.annotation.security.RolesAllowed

internal class SecuredRolePermissionEvaluatorTest {

    @Test
    fun checkClassWithAuthentication() {

        val evaluator = SecuredRolePermissionEvaluator()
        val principal = User("User", "password", setOf(SimpleGrantedAuthority("ACCESS")))

        val authentication = TestingAuthenticationToken(principal, null)
        val access = evaluator.checkClass(authentication, TestPermissionClass::class)
        assertThat(access).isTrue

        val noAccess = evaluator.checkClass(authentication, NoAccessClass::class)
        assertThat(noAccess).isFalse
    }

    @Test
    fun checkClassWithoutAuthentication() {

        val evaluator = SecuredRolePermissionEvaluator()
        val access = evaluator.checkClass(null, TestPermissionClass::class)
        assertThat(access).isFalse

        val noAccess = evaluator.checkClass(null, NoAccessClass::class)
        assertThat(noAccess).isFalse
    }

    @Test
    fun checkNoAccessWithAuthentication() {

        val evaluator = SecuredRolePermissionEvaluator()
        val principal = User("User", "password", setOf<GrantedAuthority>())

        val authentication = TestingAuthenticationToken(principal, null)
        val access = evaluator.checkClass(authentication, TestPermissionClass::class)
        assertThat(access).isFalse

        val noAccess = evaluator.checkClass(authentication, NoAccessClass::class)
        assertThat(noAccess).isFalse
    }

    @Test
    fun checkClasWithRoleWithAuthentication() {

        val evaluator = SecuredRolePermissionEvaluator()
        val principal = User("User", "password", setOf(SimpleGrantedAuthority("ROLE_ACCESS")))

        val authentication = TestingAuthenticationToken(principal, null)
        val access = evaluator.checkClass(authentication, TestPermissionClass::class)
        assertThat(access).isTrue

        val noAccess = evaluator.checkClass(authentication, NoAccessClass::class)
        assertThat(noAccess).isFalse
    }

    @Test
    fun hasPermissionCheck() {
        val evaluator = SecuredRolePermissionEvaluator()
        val principal = User("User", "password", setOf(SimpleGrantedAuthority("ROLE_ACCESS")))

        val authentication = TestingAuthenticationToken(principal, null)
        val hasPermission1 = evaluator.hasPermission(authentication, TestPermissionClass::class, "ACCESS")
        assertThat(hasPermission1).isTrue

        val hasPermission2 = evaluator.hasPermission(
            authentication,
            null,
            "com.qualidify.service.impl.security.TestPermissionClass",
            "ACCESS"
        )
        assertThat(hasPermission2).isTrue

        val noPermission1 = evaluator.hasPermission(authentication, TestPermissionClass::class, "NO-ACCESS")
        assertThat(noPermission1).isFalse

        val noPermission2 = evaluator.hasPermission(
            authentication,
            null,
            "com.qualidify.service.impl.security.TestPermissionClass",
            "NO-ACCESS"
        )
        assertThat(noPermission2).isFalse

    }
}

@RolesAllowed("ACCESS")
class TestPermissionClass {
}

@RolesAllowed("NOACCESS")
class NoAccessClass {

}