package com.qualidify.service.impl.reports

import com.qualidify.model.entities.ReportTemplate
import com.qualidify.service.report.dto.ReportTemplateDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

/**
 * @author Niels Visscher
 * @since 1.0
 *
 * Unit test suite for ReportTemplateServiceImpl
 */

@ExtendWith(MockitoExtension::class)
internal class ReportTemplateServiceImplTest {

    @Mock
    lateinit var reportServiceTemplateRepository: ReportServiceTemplateRepository

    @Mock
    lateinit var reportServiceTemplateDtoMapper: ReportServiceTemplateDtoMapper

    /**
     * Helper function for mockito
     */
    private fun <T> any(type: Class<T>): T = Mockito.any<T>(type)

    @Test
    fun `ReportServiceTemplateServiceImpl fetches report templates from repository`() {
        // Set up the interface so it returns a report template
        `when`(reportServiceTemplateRepository.findAll()).thenReturn(
            listOf<ReportTemplate>(
                ReportTemplate(UUID.randomUUID(), "name", "description", "Niels".toByteArray(), "[]", mutableSetOf())
            )
        )
        // Mock the relevant part of the mapper
        `when`(reportServiceTemplateDtoMapper.convertToDto(any(ReportTemplate::class.java))).thenReturn(null)
        // Create the service
        val service = ReportTemplateServiceImpl(reportServiceTemplateRepository, reportServiceTemplateDtoMapper)

        assertThat(service.getAllTemplates()).`as`("Service returns a single template").hasSize(1)
    }

    @Test
    fun `ReportServiceTemplateServiceImpl can save a new template`() {
        var saveableTemplate = ReportTemplate(UUID.randomUUID(), "Template name", "Template description", "Niels".toByteArray(), "[]", mutableSetOf())
        var saveableDto = ReportTemplateDto(UUID.randomUUID(), "Template name", "Template description", "Niels".toByteArray(), "[]", arrayOf())

        `when`(reportServiceTemplateRepository.save(null)).thenReturn(null);
        `when`(reportServiceTemplateRepository.save(any(ReportTemplate::class.java))).thenReturn(saveableTemplate)
        `when`(reportServiceTemplateDtoMapper.convertToModel(any(ReportTemplateDto::class.java))).thenReturn(saveableTemplate)

        val service = ReportTemplateServiceImpl(reportServiceTemplateRepository, reportServiceTemplateDtoMapper)

        service.create(saveableDto)

        // Mapper was called once to convert to a model
        verify(reportServiceTemplateDtoMapper, times(1)).convertToModel(any(ReportTemplateDto::class.java))
        verify(reportServiceTemplateDtoMapper, never()).convertToDto(any(ReportTemplate::class.java))
        // Save function in repo was called once
        verify(reportServiceTemplateRepository, times(1)).save(any(ReportTemplate::class.java))

    }
}