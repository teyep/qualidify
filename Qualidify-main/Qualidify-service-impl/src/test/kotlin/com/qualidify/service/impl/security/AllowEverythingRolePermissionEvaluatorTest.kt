package com.qualidify.service.impl.security

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.security.authentication.TestingAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User

internal class AllowEverythingRolePermissionEvaluatorTest {

    @Test
    fun checkClassWithAuthentication() {

        val evaluator = AllowEverythingRolePermissionEvaluator()
        val principal = User("User", "password", setOf(SimpleGrantedAuthority("ACCESS")))

        val authentication = TestingAuthenticationToken(principal, null)
        val access = evaluator.checkClass(authentication, TestPermissionClass::class)
        Assertions.assertThat(access).isTrue

        val noAccess = evaluator.checkClass(authentication, NoAccessClass::class)
        Assertions.assertThat(noAccess).isTrue
    }

    @Test
    fun checkClassWithoutAuthentication() {

        val evaluator = AllowEverythingRolePermissionEvaluator()
        val access = evaluator.checkClass(null, TestPermissionClass::class)
        Assertions.assertThat(access).isTrue

        val noAccess = evaluator.checkClass(null, NoAccessClass::class)
        Assertions.assertThat(noAccess).isTrue
    }

    @Test
    fun hasPermissionCheck() {
        val evaluator = AllowEverythingRolePermissionEvaluator()
        val principal = User("User", "password", setOf(SimpleGrantedAuthority("ROLE_ACCESS")))

        val authentication = TestingAuthenticationToken(principal, null)
        val hasPermission1 = evaluator.hasPermission(authentication, TestPermissionClass::class, "ACCESS")
        Assertions.assertThat(hasPermission1).isTrue

        val hasPermission2 = evaluator.hasPermission(
            authentication,
            null,
            "com.qualidify.service.impl.security.TestPermissionClass",
            "ACCESS"
        )
        Assertions.assertThat(hasPermission2).isTrue

        val noPermission1 = evaluator.hasPermission(authentication, TestPermissionClass::class, "NO-ACCESS")
        Assertions.assertThat(noPermission1).isTrue

        val noPermission2 = evaluator.hasPermission(
            authentication,
            null,
            "com.qualidify.service.impl.security.TestPermissionClass",
            "NO-ACCESS"
        )
        Assertions.assertThat(noPermission2).isTrue

    }
}