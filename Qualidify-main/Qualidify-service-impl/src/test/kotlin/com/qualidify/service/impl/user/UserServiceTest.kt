package com.qualidify.service.impl.user

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countByQuery
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.MetadataValidationService
import com.qualidify.metadata.service.exceptions.MetadataValidationException
import com.qualidify.model.entities.SecurityGroup
import com.qualidify.model.entities.SecurityGroupId
import com.qualidify.model.entities.User
import com.qualidify.model.entities.UserField
import com.qualidify.service.impl.security.SecurityGroupRepository
import com.qualidify.service.user.dto.CreateUserCommand
import com.qualidify.service.user.dto.UserDto
import com.qualidify.service.user.exceptions.UserNotFoundException
import io.mockk.every
import io.mockk.mockkStatic
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class UserServiceTest {
    @Mock
    lateinit var userRepository: UserDataRepository

    @Mock
    lateinit var groupRepository: SecurityGroupRepository

    @Mock
    lateinit var validator: MetadataValidationService

    @Mock
    lateinit var mapper: UserMapper

    @InjectMocks
    lateinit var fixture: UserServiceImpl

    val PRISON_ACCESS_SECURITYGROUP = SecurityGroup("PRISON_ACCESS", "access to jail")
    val DEPUTY_SECURITYGROUP = SecurityGroup("DEPUTY", "assistent of the sherrif")

    @Test
    fun `Count the users`() {
        mockkStatic(UserDataRepository::countByQuery)
        every {
            userRepository.countByQuery(CountQuery<UserDto>())
        } returns 42

        val countResponse = fixture.countByQuery(CountQuery<UserDto>())

        assertThat(countResponse.count).`as`("Expecting 42 users in the repository").isEqualTo(42)

        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository, validator, mapper)
    }

    @Suppress("UNCHECKED_CAST")
    @Test
    fun `Find a couple of users`() {
        val users = listOf<User>(
            luckyLukeEntity(),
            jollyJumperEntity()
        )

        `when`(userRepository.findAll(any(), any(Pageable::class.java))).thenReturn(PageImpl<User>(users))
        `when`(mapper.convertToDto(users[0])).thenReturn(
            UserDto(
                users[0].userId,
                users[0].username,
                users[0].email
            )
        )
        `when`(mapper.convertToDto(users[1])).thenReturn(
            UserDto(
                users[1].userId,
                users[1].username,
                users[1].email
            )
        )

        val userResult = fixture.findByQuery(SearchQuery<UserDto>())

        assertThat(userResult).`as`("Expecting 2 users in the repository").hasSize(2).contains(
            UserDto(users[0].userId, users[0].username, users[0].email),
            UserDto(users[1].userId, users[1].username, users[1].email)
        )

        verify(userRepository, times(1)).findAll(
            any(),
            any(Pageable::class.java)
        )
        verify(mapper, times(2)).convertToDto(any(User::class.java))
        verifyNoMoreInteractions(userRepository, mapper)
        verifyNoInteractions(groupRepository, validator)
    }


    @Test
    fun `Create a new user`() {
        val luckyLuke = luckyLukeEntity()
        val luckyLukeDto = toDto(luckyLuke)

        `when`(userRepository.save(any(User::class.java))).thenReturn(luckyLuke)
        `when`(groupRepository.findAllById(emptySet())).thenReturn(emptySet())
        `when`(mapper.convertToDto(luckyLuke)).thenReturn(luckyLukeDto)

        val userCaptor = ArgumentCaptor.forClass(User::class.java)

        fixture.create(
            CreateUserCommand(
                userName = luckyLukeDto.username,
                email = luckyLukeDto.email,
                password = "Sigaret",
                fields = luckyLukeDto.fields
            )
        )

        verify(validator, times(1)).checkAllCustomFieldsAreAllowed(Reference.USER, mapOf<String, String?>())
        verify(validator, times(1)).checkAllMandatoryCustomFieldsArePresent(Reference.USER, mapOf<String, String?>())
        verify(userRepository, times(1)).save(userCaptor.capture())
        verify(groupRepository, times(1)).findAllById(emptySet())
        verify(mapper, times(1)).convertToDto(luckyLuke)

        assertThat(userCaptor.value.email).isEqualTo(luckyLukeDto.email)
        assertThat(userCaptor.value.username).isEqualTo(luckyLukeDto.username)
        assertThat(userCaptor.value.password).isEqualTo("Sigaret")

        verifyNoMoreInteractions(userRepository, groupRepository, validator)

    }

    @Test
    fun `Create a new user with fields`() {
        val fields = mutableSetOf(UserField("EMAIL", "luckyluke@wildwest.com"))
        val luckyLuke = luckyLukeEntity(fields)
        val luckyLukeDto = toDto(luckyLuke)

        val userCaptor = ArgumentCaptor.forClass(User::class.java)

        `when`(userRepository.save(any(User::class.java))).thenReturn(luckyLuke)
        `when`(groupRepository.findAllById(emptySet())).thenReturn(emptySet())
        `when`(mapper.convertToDto(luckyLuke)).thenReturn(luckyLukeDto)

        fixture.create(
            CreateUserCommand(
                userName = luckyLukeDto.username,
                email = luckyLukeDto.email,
                password = "Sigaret",
                fields = luckyLukeDto.fields
            )
        )

        verify(validator, times(1)).checkAllCustomFieldsAreAllowed(Reference.USER, luckyLukeDto.fields)
        verify(validator, times(1)).checkAllMandatoryCustomFieldsArePresent(Reference.USER, luckyLukeDto.fields)
        verify(userRepository, times(1)).save(userCaptor.capture())
        verify(groupRepository, times(1)).findAllById(emptySet())
        verify(mapper, times(1)).convertToDto(luckyLuke)

        assertThat(userCaptor.value.email).isEqualTo(luckyLukeDto.email)
        assertThat(userCaptor.value.username).isEqualTo(luckyLukeDto.username)
        assertThat(userCaptor.value.password).isEqualTo("Sigaret")

        verifyNoMoreInteractions(userRepository, groupRepository, validator)
    }

    @Test
    fun `Create a new user with securityGroups`() {
        val securityGroups = setOf("PRISON_ACCESS", "DEPUTY")
        val securityGroupIds = securityGroups.map { SecurityGroupId(it) }.toSet()
        val luckyLuke = luckyLukeEntity().apply {
            groups = setOf(PRISON_ACCESS_SECURITYGROUP, DEPUTY_SECURITYGROUP)
        }
        val luckyLukeDto = toDto(luckyLuke).copy(
            securityGroups = securityGroups
        )
        val userCaptor = ArgumentCaptor.forClass(User::class.java)

        `when`(userRepository.save(any(User::class.java))).thenReturn(luckyLuke)
        `when`(groupRepository.findAllById(securityGroupIds))
            .thenReturn(setOf(PRISON_ACCESS_SECURITYGROUP, DEPUTY_SECURITYGROUP))
        `when`(mapper.convertToDto(luckyLuke)).thenReturn(luckyLukeDto)

        fixture.create(
            CreateUserCommand(
                userName = luckyLukeDto.username,
                email = luckyLukeDto.email,
                password = "Sigaret",
                fields = luckyLukeDto.fields,
                securityGroups = securityGroups
            )
        )

        verify(validator, times(1)).checkAllCustomFieldsAreAllowed(Reference.USER, luckyLukeDto.fields)
        verify(validator, times(1)).checkAllMandatoryCustomFieldsArePresent(Reference.USER, luckyLukeDto.fields)
        verify(userRepository, times(1)).save(userCaptor.capture())
        verify(groupRepository, times(1)).findAllById(securityGroupIds)
        verify(mapper, times(1)).convertToDto(luckyLuke)


        assertThat(userCaptor.value.email).isEqualTo(luckyLukeDto.email)
        assertThat(userCaptor.value.username).isEqualTo(luckyLukeDto.username)
        assertThat(userCaptor.value.password).isEqualTo("Sigaret")

        verifyNoMoreInteractions(userRepository, groupRepository, validator, mapper)

    }

    @Test
    fun `Create a new user without name`() {
        assertThatThrownBy {
            fixture.create(
                CreateUserCommand(
                    userName = "",
                    email = "dev@null",
                    password = "secret"
                )
            )
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("User name not filled in.")
        verifyNoInteractions(userRepository, validator, groupRepository, mapper)
    }

    @Test
    fun `Create a new user without password`() {

        assertThatThrownBy {
            fixture.create(
                CreateUserCommand(
                    userName = "Rataplan",
                    email = "rataplan@wildewest.com",
                    password = ""
                )
            )
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Password not filled in.")

        verifyNoInteractions(userRepository, validator, groupRepository, mapper)
    }

    @Test
    fun `Create a new user without email`() {
        assertThatThrownBy {
            fixture.create(
                CreateUserCommand(
                    userName = "Rataplan",
                    email = "",
                    password = "bone"
                )
            )
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Email not filled in.")

        verifyNoInteractions(userRepository, validator, groupRepository, mapper)
    }

    @Test
    fun `Create a new user without not expected field`() {
        val fields = mutableMapOf<String, String?>("INJAIL" to "TRUE")
        `when`(
            validator.checkAllCustomFieldsAreAllowed(
                Reference.USER,
                fields
            )
        ).thenThrow(MetadataValidationException("Field not allowed"))

        assertThatThrownBy {
            fixture.create(
                CreateUserCommand(
                    userName = "Averell Dalton",
                    email = "averell@wildwest.com",
                    password = "Ma Dalton",
                    fields = fields

                )
            )
        }.isInstanceOf(MetadataValidationException::class.java)
            .hasMessage("Field not allowed")

        verifyNoMoreInteractions(validator)
        verifyNoInteractions(userRepository, groupRepository, mapper)
    }

    @Test
    fun `Update an existing user`() {
        val luckyLuke = luckyLukeEntity()
        val luckyLukeDto = toDto(luckyLuke)

        `when`(userRepository.findById(luckyLuke.id)).thenReturn(Optional.of(luckyLuke))
        `when`(userRepository.save(luckyLuke)).thenReturn(luckyLuke)
        `when`(mapper.convertToDto(luckyLuke)).thenReturn(luckyLukeDto)
        `when`(groupRepository.findAllById(anySet())).thenReturn(emptySet())

        fixture.update(luckyLukeDto)

        verify(validator, times(1)).checkAllCustomFieldsAreAllowed(Reference.USER, mapOf<String, String?>())
        verify(validator, times(1)).checkAllMandatoryCustomFieldsArePresent(Reference.USER, mapOf<String, String?>())
        verify(userRepository, times(1)).findById(luckyLuke.id)
        verify(userRepository, times(1)).save(luckyLuke)
        verify(mapper, times(1)).convertToDto(luckyLuke)

        verifyNoMoreInteractions(userRepository, validator)
    }

    @Test
    fun `Update an existing user with fields`() {
        val fields = mutableSetOf(UserField("EMAIL", "luckyluke@wildwest.com"))
        val luckyLuke = luckyLukeEntity(fields)
        val luckyLukeDto = toDto(luckyLuke)

        `when`(userRepository.findById(luckyLuke.id)).thenReturn(Optional.of(luckyLuke))
        `when`(userRepository.save(luckyLuke)).thenReturn(luckyLuke)
        `when`(groupRepository.findAllById(emptySet())).thenReturn(emptySet())
        `when`(mapper.convertToDto(luckyLuke)).thenReturn(luckyLukeDto)

        fixture.update(luckyLukeDto)

        verify(validator, times(1)).checkAllCustomFieldsAreAllowed(Reference.USER, luckyLukeDto.fields)
        verify(validator, times(1)).checkAllMandatoryCustomFieldsArePresent(Reference.USER, luckyLukeDto.fields)
        verify(userRepository, times(1)).findById(luckyLuke.id)
        verify(userRepository, times(1)).save(luckyLuke)
        verify(groupRepository, times(1)).findAllById(emptySet())
        verify(mapper, times(1)).convertToDto(luckyLuke)

        verifyNoMoreInteractions(userRepository, groupRepository, validator)
    }

    @Test
    fun `Update a not existing user`() {
        val luckyLuke = luckyLukeEntity()
        val luckyLukeDto = toDto(luckyLuke)

        `when`(userRepository.findById(luckyLuke.id)).thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.update(luckyLukeDto)
        }.isInstanceOf(UserNotFoundException::class.java)
            .hasMessage("User with id ${luckyLuke.userId} cannot be found.")

        verify(validator, times(1))
            .checkAllCustomFieldsAreAllowed(Reference.USER, mutableMapOf<String, String?>())
        verify(validator, times(1))
            .checkAllMandatoryCustomFieldsArePresent(Reference.USER, mutableMapOf<String, String?>())
        verify(userRepository, times(1)).findById(luckyLuke.id)

        verifyNoMoreInteractions(userRepository, validator)
        verifyNoInteractions(groupRepository, mapper)
    }

    @Test
    fun `Update an existing user without name`() {
        val dto = UserDto(
            userId = UUID.randomUUID(),
            username = "",
            email = "bones@wildwest.com"
        )

        assertThatThrownBy {
            fixture.update(dto)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("User name not filled in.")
        verifyNoInteractions(userRepository, validator, groupRepository, mapper)
    }


    @Test
    fun `Update an existing user without email`() {
        val dto = UserDto(
            userId = UUID.randomUUID(),
            username = "Joe",
            email = "",
        )

        assertThatThrownBy {
            fixture.update(dto)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Email not filled in.")
        verifyNoInteractions(userRepository, validator, groupRepository, mapper)
    }


    @Test
    fun `Link securityGroups to User`() {
        val luckyLuke = luckyLukeEntity()
        val securityGroups = setOf("PRISON_ACCES", "DEPUTY")

        val luckyLukeWithGroups = luckyLuke.apply {
            groups = setOf(PRISON_ACCESS_SECURITYGROUP, DEPUTY_SECURITYGROUP)
        }

        `when`(userRepository.findById(luckyLuke.id)).thenReturn(Optional.of(luckyLuke))
        `when`(groupRepository.findAllById(securityGroups.asSequence().map { SecurityGroupId(it) }.toSet()))
            .thenReturn(
                setOf(
                    PRISON_ACCESS_SECURITYGROUP,
                    DEPUTY_SECURITYGROUP
                )
            )
        `when`(userRepository.save(luckyLukeWithGroups)).thenReturn(luckyLukeWithGroups)

        fixture.linkGroupsToUser(luckyLuke.userId, securityGroups)

        verify(userRepository, times(1)).findById(luckyLuke.id)
        verify(groupRepository, times(1)).findAllById(securityGroups.asSequence().map { SecurityGroupId(it) }.toSet())
        verify(userRepository, times(1)).save(luckyLukeWithGroups)

        verifyNoMoreInteractions(userRepository, groupRepository)
        verifyNoInteractions(validator, mapper)
    }

    @Test
    fun `Link securityGroups to non-existing User`() {
        val luckyLuke = luckyLukeEntity()
        val securityGroups = setOf("PRISON_ACCES", "DEPUTY")

        `when`(userRepository.findById(luckyLuke.id)).thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.linkGroupsToUser(luckyLuke.userId, securityGroups)
        }.isInstanceOf(UserNotFoundException::class.java)
            .hasMessage("User with id ${luckyLuke.userId} cannot be found.")

        verify(userRepository, times(1)).findById(luckyLuke.id)
        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository, validator, mapper)
    }

    @Test
    fun `Find a user by its name`() {
        val luckyLuke = luckyLukeEntity()
        val luckyLukeDto = toDto(luckyLuke)

        `when`(userRepository.findByUsernameIgnoreCase(luckyLuke.username)).thenReturn(Optional.of(luckyLuke))
        `when`(mapper.convertToDto(luckyLuke)).thenReturn(luckyLukeDto)

        val user = fixture.findUserByName(luckyLuke.username)

        assertThat(user.username).`as`("Found user has name Lucky Luke").isEqualTo(luckyLuke.username)

        verify(userRepository, times(1)).findByUsernameIgnoreCase(luckyLuke.username)
        verify(mapper, times(1)).convertToDto(luckyLuke)

        verifyNoMoreInteractions(userRepository, mapper)
        verifyNoInteractions(groupRepository, validator)
    }

    @Test
    fun `Find a non existing user by its name `() {
        val luckyLuke = luckyLukeEntity()

        `when`(userRepository.findByUsernameIgnoreCase(luckyLuke.username)).thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.findUserByName(luckyLuke.username)
        }.isInstanceOf(UserNotFoundException::class.java)
            .hasMessage("User ${luckyLuke.username} cannot be found.")

        verify(userRepository, times(1)).findByUsernameIgnoreCase(luckyLuke.username)
        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository, validator, mapper)
    }

    @Test
    fun `Find a user by its id`() {
        val luckyLuke = luckyLukeEntity()
        val luckyLukeDto = toDto(luckyLuke)

        `when`(userRepository.findById(luckyLuke.id)).thenReturn(Optional.of(luckyLuke))
        `when`(mapper.convertToDto(luckyLuke)).thenReturn(luckyLukeDto)

        val user = fixture.findUserByUserId(luckyLuke.userId)

        assertThat(user.username).`as`("Found user has name Lucky Luke").isEqualTo(luckyLuke.username)

        verify(userRepository, times(1)).findById(luckyLuke.id)
        verify(mapper, times(1)).convertToDto(luckyLuke)
        verifyNoMoreInteractions(userRepository, mapper)
        verifyNoInteractions(groupRepository, validator)
    }

    @Test
    fun `Find a non existing user by its id `() {
        val luckyLuke = luckyLukeEntity()

        `when`(userRepository.findById(luckyLuke.id)).thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.findUserByUserId(luckyLuke.userId)
        }.isInstanceOf(UserNotFoundException::class.java)
            .hasMessage("User with id ${luckyLuke.userId} cannot be found.")

        verify(userRepository, times(1)).findById(luckyLuke.id)
        verifyNoMoreInteractions(userRepository)
        verifyNoInteractions(groupRepository, validator, mapper)
    }

    /**
     * Helper function for mockito
     */
    private fun <T> any(type: Class<T>): T = Mockito.any<T>(type)

    private fun toDto(user: User) = UserDto(
        userId = user.userId,
        username = user.username,
        email = user.email,
        fields = UserFieldMapper().convertToDto(user.fields)
    )

    private fun jollyJumperEntity(fields: MutableSet<UserField> = mutableSetOf()) = User.builder()
        .withUserId(UUID.randomUUID())
        .withUsername("Jolly Jumper")
        .withPassword("Rataplan")
        .withEmail("jolly.jumper@wildwest.com")
        .withFields(fields)
        .withEnabled(true)
        .withNonExpired(true)
        .withCredentialsNonExpired(true)
        .withAccountNonLocked(true)
        .build()

    private fun luckyLukeEntity(fields: MutableSet<UserField> = mutableSetOf()) = User.builder()
        .withUserId(UUID.randomUUID())
        .withUsername("Lucky Luke")
        .withPassword("Sigaret")
        .withEmail("lucky.luke@wildwest.com")
        .withFields(fields)
        .withEnabled(true)
        .withNonExpired(true)
        .withCredentialsNonExpired(true)
        .withAccountNonLocked(true)
        .build()

}