package com.qualidify.service.impl.process.events

import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.impl.projects.ProjectInstanceRepository
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.projects.ProjectInstanceService
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.context.ApplicationEventPublisher
import java.util.*

internal class FlowableSpringEventRepublisherTest {

    private val cmmnCaseDefinitionRepository = mockk<CmmnCaseDefinitionRepository>()
    private val projectInstanceRepository = mockk<ProjectInstanceRepository>()
    private val projectInstanceService = mockk<ProjectInstanceService>()
    private val caseInstanceService = mockk<CaseInstanceService>()

    private val publisher = spyk<ApplicationEventPublisher>()

    private val fixture =
        FlowableSpringEventRepublisher(
            publisher,
            cmmnCaseDefinitionRepository,
            projectInstanceRepository,
            caseInstanceService
        )

    private val regularCaseInstanceId = UUID.randomUUID().toString()
    private val qualityCaseInstanceId = UUID.randomUUID().toString()
    private val projectInstanceId: UUID = UUID.randomUUID()
    private val regularCaseInstance = TestEntities.caseInstance(regularCaseInstanceId)
    private val regularCaseInstanceDto = DtoFactory.caseInstanceDto(
        id = regularCaseInstanceId,
        projectInstanceId = projectInstanceId.toString(),
        caseInstanceId = null,
        qualityShellId = qualityCaseInstanceId
    )
    private val regularCaseDefinition = TestEntities.cmmnCaseDefinitionJpa(deploymentResource = null)

    @AfterEach
    fun checkTest() {
        confirmVerified(
            cmmnCaseDefinitionRepository,
            projectInstanceRepository,
            projectInstanceService,
            caseInstanceService
        )
        checkUnnecessaryStub(
            cmmnCaseDefinitionRepository,
            projectInstanceRepository,
            projectInstanceService,
            caseInstanceService
        )
    }

    @Test
    fun `simple republishing base-event test with regular case instance`() {
        every {
            cmmnCaseDefinitionRepository.findById(any())
        } returns Optional.of(regularCaseDefinition)

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(any())
        } returns Optional.of(
            TestEntities.projectInstance(
                id = projectInstanceId,
                cmmnInstanceId = regularCaseInstanceId,
                cmmnQualityShellId = qualityCaseInstanceId,
            )
        )

        every {
            caseInstanceService.getCaseVariable(any(), any())
        } returns null

        val cmmnLifecycleEventSlot = slot<CmmnLifecycleEvent>()
        every {
            publisher.publishEvent(capture(cmmnLifecycleEventSlot))
        } just Runs

        fixture.stateChanged(regularCaseInstance, "", "active")

        assertThat(cmmnLifecycleEventSlot.captured.caseInstance).isEqualTo(regularCaseInstanceDto)
        assertThat(cmmnLifecycleEventSlot.captured.oldState).isEqualTo("")
        assertThat(cmmnLifecycleEventSlot.captured.newState).isEqualTo("active")

        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }
        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(any()) }
        verify(exactly = 1) { publisher.publishEvent(ofType(CmmnLifecycleEvent::class)) }
        verify(exactly = 6) { caseInstanceService.getCaseVariable(any(), any()) }

    }

    @Test
    fun `test with regular case instance and no projectInstance`() {
        val caseInstanceDtoWithoutProjectInstance = DtoFactory.caseInstanceDto(
            id = regularCaseInstanceId,
            projectInstanceId = "PROJECT_INSTANCE_NOT_AVAILABLE",
            qualityShellId = "QUALITY_SHELL_ID_NOT_KNOWN"
        )

        every {
            cmmnCaseDefinitionRepository.findById(any())
        } returns Optional.of(regularCaseDefinition)

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(any())
        } returns Optional.empty()

        every {
            caseInstanceService.getCaseVariable(any(), any())
        } returns null

        val cmmnLifecycleEventSlot = slot<CmmnLifecycleEvent>()
        every {
            publisher.publishEvent(capture(cmmnLifecycleEventSlot))
        } just Runs

        fixture.stateChanged(regularCaseInstance, "", "active")

        assertThat(cmmnLifecycleEventSlot.captured.caseInstance).isEqualTo(caseInstanceDtoWithoutProjectInstance)
        assertThat(cmmnLifecycleEventSlot.captured.oldState).isEqualTo("")
        assertThat(cmmnLifecycleEventSlot.captured.newState).isEqualTo("active")

        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }
        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(any()) }
        verify(exactly = 1) { publisher.publishEvent(ofType(CmmnLifecycleEvent::class)) }
        verify(exactly = 6) { caseInstanceService.getCaseVariable(any(), any()) }

    }

    @Test
    fun `test with quality shell case instance`() {
        val qualityCaseInstance = TestEntities.caseInstance(qualityCaseInstanceId)
        val qualityCaseInstanceDto = DtoFactory.caseInstanceDto(
            id = qualityCaseInstanceId,
            projectInstanceId = projectInstanceId.toString(),
            isQualityShell = true,
            caseInstanceId = regularCaseInstanceId,
            qualityShellId = null
        )
        val qualityCaseDefinition =
            TestEntities.cmmnCaseDefinitionJpa(caseDefinitionKey = "cmmnQualityShell", deploymentResource = null)

        every {
            cmmnCaseDefinitionRepository.findById(any())
        } returns Optional.of(qualityCaseDefinition)

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(any())
        } returns Optional.of(
            TestEntities.projectInstance(
                id = projectInstanceId,
                cmmnInstanceId = regularCaseInstanceId,
                cmmnQualityShellId = qualityCaseInstanceId
            )
        )

        every {
            caseInstanceService.getCaseVariable(any(), any())
        } returns null

        val cmmnLifecycleEventSlot = slot<CmmnLifecycleEvent>()
        every {
            publisher.publishEvent(capture(cmmnLifecycleEventSlot))
        } just Runs

        fixture.stateChanged(qualityCaseInstance, "", "active")

        assertThat(cmmnLifecycleEventSlot.captured.caseInstance).isEqualTo(qualityCaseInstanceDto)
        assertThat(cmmnLifecycleEventSlot.captured.oldState).isEqualTo("")
        assertThat(cmmnLifecycleEventSlot.captured.newState).isEqualTo("active")

        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }
        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(any()) }
        verify(exactly = 1) { publisher.publishEvent(ofType(CmmnLifecycleEvent::class)) }
        verify(exactly = 6) { caseInstanceService.getCaseVariable(any(), any()) }

    }

    @Test
    fun `republishing Activated Cmmn test`() {
        every {
            cmmnCaseDefinitionRepository.findById(any())
        } returns Optional.of(regularCaseDefinition)

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(any())
        } returns Optional.of(
            TestEntities.projectInstance(
                id = projectInstanceId,
                cmmnInstanceId = regularCaseInstanceId,
                cmmnQualityShellId = qualityCaseInstanceId,
            )
        )

        every {
            caseInstanceService.getCaseVariable(any(), any())
        } returns null

        val activatedCmmnEventSlot = slot<ActivatedCmmnEvent>()
        every {
            publisher.publishEvent(capture(activatedCmmnEventSlot))
        } just Runs

        fixture.stateChanged(regularCaseInstance, "", "activated")

        assertThat(activatedCmmnEventSlot.captured.caseInstance).isEqualTo(regularCaseInstanceDto)
        assertThat(activatedCmmnEventSlot.captured.oldState).isEqualTo("")
        assertThat(activatedCmmnEventSlot.captured.newState).isEqualTo("active")

        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }
        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(any()) }
        verify(exactly = 1) { publisher.publishEvent(ofType(ActivatedCmmnEvent::class)) }
        verify(exactly = 6) { caseInstanceService.getCaseVariable(any(), any()) }

    }

    @Test
    fun `republishing Terminate Cmmn test`() {
        every {
            cmmnCaseDefinitionRepository.findById(any())
        } returns Optional.of(regularCaseDefinition)

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(any())
        } returns Optional.of(
            TestEntities.projectInstance(
                id = projectInstanceId,
                cmmnInstanceId = regularCaseInstanceId,
                cmmnQualityShellId = qualityCaseInstanceId,
            )
        )

        every {
            caseInstanceService.getCaseVariable(any(), any())
        } returns null

        val terminatedCmmnEventSlot = slot<TerminatedCmmnEvent>()
        every {
            publisher.publishEvent(capture(terminatedCmmnEventSlot))
        } just Runs

        fixture.stateChanged(regularCaseInstance, "active", "terminated")

        assertThat(terminatedCmmnEventSlot.captured.caseInstance).isEqualTo(regularCaseInstanceDto)
        assertThat(terminatedCmmnEventSlot.captured.oldState).isEqualTo("active")
        assertThat(terminatedCmmnEventSlot.captured.newState).isEqualTo("terminated")

        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }
        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(any()) }
        verify(exactly = 1) { publisher.publishEvent(ofType(TerminatedCmmnEvent::class)) }
        verify(exactly = 6) { caseInstanceService.getCaseVariable(any(), any()) }

    }

    @Test
    fun `republishing Completed Cmmn test`() {
        every {
            cmmnCaseDefinitionRepository.findById(any())
        } returns Optional.of(regularCaseDefinition)

        every {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(any())
        } returns Optional.of(
            TestEntities.projectInstance(
                id = projectInstanceId,
                cmmnInstanceId = regularCaseInstanceId,
                cmmnQualityShellId = qualityCaseInstanceId,
            )
        )

        every {
            caseInstanceService.getCaseVariable(any(), any())
        } returns null

        val completedCmmnEventSlot = slot<CompletedCmmnEvent>()
        every {
            publisher.publishEvent(capture(completedCmmnEventSlot))
        } just Runs

        fixture.stateChanged(regularCaseInstance, "active", "completed")

        assertThat(completedCmmnEventSlot.captured.caseInstance).isEqualTo(regularCaseInstanceDto)
        assertThat(completedCmmnEventSlot.captured.oldState).isEqualTo("active")
        assertThat(completedCmmnEventSlot.captured.newState).isEqualTo("completed")

        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }
        verify(exactly = 1) { projectInstanceRepository.findProjectInstanceByRegularCaseId(any()) }
        verify(exactly = 1) { publisher.publishEvent(ofType(CompletedCmmnEvent::class)) }
        verify(exactly = 6) { caseInstanceService.getCaseVariable(any(), any()) }

    }

}