package com.qualidify.service.impl.process.cmmn

import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.engine.CmmnEngine
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension


@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Disabled("Disabled because it is not testing Qualidify functionality, " +
        "but it is a showcase for CMMN (disable this line, to walk through)")
class EmployeeOnboardingTest(
    var cmmnEngine: CmmnEngine,
    var cmmnRuntimeService: CmmnRuntimeService,
    var cmmnTaskService: CmmnTaskService,
) {

    /**
     * This is a demo test case for understanding cmmn, it will follow the path
     * described in @see [link] (https://flowable.com/open-source/docs/cmmn/ch06-cmmn/)
     * See also for the flow as image ![](../../../../resources/cases/employeeOnboarding.png)
     */
    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/employeeOnboarding.cmmn.xml"])
    fun employeeOnboarding() {
        val caseInstance: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("employeeOnboarding")
            .variable("potentialEmployee", "John Doe")
            .start()
        assertThat(caseInstance).`as`("The CMMN instance cannot be found.").isNotNull

        val planItemInstances = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateActive()
            .list()
        assertThat(planItemInstances).`as`("There must be four tasks available at startup.").hasSize(4)

        //One of the planitem is a stage, we are more interested in tasks
        //  there should be 4 tasks directly available (because they are not
        //  annotated with manualy starting)
        val tasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .list()
        assertThat(tasks).`as`("There must be three tasks available at startup.").hasSize(3)

        //complete Create Emailadress
        val createEmailadress =
            tasks.asSequence().filter { it.taskDefinitionKey.equals("sid-787CBEC5-1C6A-4582-A9FA-90D2F09E7BEA") }
                .first()
        cmmnTaskService.complete(createEmailadress.id)
        //Complete Allocate office
        val allocateOffice =
            tasks.asSequence().filter { it.taskDefinitionKey.equals("sid-8CBED033-5F3A-41C5-9E90-653F8565D2D2") }
                .first()
        cmmnTaskService.complete(allocateOffice.id)

        //Two tasks completed, but still on to go, to activate "Send joining letter to candidate"
        var activeTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .active()
            .list()

        assertThat(activeTasks)
            .`as`("There must be one tasks available after completing Emailadress and Allocate Office.")
            .hasSize(1)

        val agreeStartdate =
            tasks.asSequence().filter { it.taskDefinitionKey.equals("sid-98DE117A-1010-4D49-8139-6076DEE81403") }
                .first()
        cmmnTaskService.complete(agreeStartdate.id)

        activeTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .active()
            .list()

        //Send joining letter should be activated
        assertThat(activeTasks)
            .`as`("There must be one tasks available, the Send joining letter is new.")
            .hasSize(1)

        val sendJoiningLetter =
            activeTasks.asSequence().filter { it.taskDefinitionKey.equals("sid-72266CE6-229F-4D6F-B55A-007A088A42BD") }
                .first()
        cmmnTaskService.complete(sendJoiningLetter.id)

        activeTasks = cmmnTaskService.createTaskQuery().caseInstanceId(caseInstance.id)
            .active()
            .list()

        assertThat(activeTasks)
            .`as`("All tasks in this stage a completed, there must be 0 tasks available.")
            .hasSize(0)

        //complete the stage
        val complete = cmmnRuntimeService.createUserEventListenerInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .name("Complete")
            .singleResult()
        cmmnRuntimeService.completeUserEventListenerInstance(complete.id)

        activeTasks = cmmnTaskService.createTaskQuery().caseInstanceId(caseInstance.id)
            .active()
            .list()

        assertThat(activeTasks)
            .`as`("There must be two tasks available.")
            .hasSize(2)

        //when canceling ("Reject Job") nothing should be active anymore
        val reject = cmmnRuntimeService.createUserEventListenerInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .name("Job rejected")
            .singleResult()
        cmmnRuntimeService.completeUserEventListenerInstance(reject.id)

        activeTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(caseInstance.id)
            .active()
            .list()
        assertThat(activeTasks).`as`("There are no tasks available.").isEmpty()

        val activePlanInstances = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(caseInstance.id)
            .planItemInstanceStateActive()
            .count()
        assertThat(activePlanInstances).`as`("There are no plan instances available.").isZero

        // check items available with the historyservice
        val cmmnHistoryService = cmmnEngine.cmmnHistoryService
        val historicCaseInstances = cmmnHistoryService.createHistoricCaseInstanceQuery()
            .list()
        assertThat(historicCaseInstances.size)
            .`as`("One case instance should have been executed")
            .isEqualTo(1)
        assertThat(historicCaseInstances[0].caseDefinitionId)
            .`as`("Historic case definition ID should be equal to case instance ID used")
            .isEqualTo(caseInstance.caseDefinitionId)
        assertThat(historicCaseInstances)

        // for case definitions query the repositoryservice
        val caseDefinitionQuery = cmmnEngine.cmmnRepositoryService.createCaseDefinitionQuery()
            .caseDefinitionId(historicCaseInstances[0].caseDefinitionId)  // use caseInstance.caseDefinitionId to access via created case instance
            .list()
        assertThat(caseDefinitionQuery[0].key)
            .`as`("The case definition key should be employeeOnboarding")
            .isEqualTo("employeeOnboarding")
        assertThat(caseDefinitionQuery[0].name)
            .`as`("The case definition name should be Employee Onboarding")
            .isEqualTo("Employee Onboarding")
        assertThat(caseDefinitionQuery[0].version)
            .`as`("The case definition version should be 1")
            .isEqualTo(1)

        val historicPlanItemInstances = cmmnHistoryService.createHistoricPlanItemInstanceQuery()
            .list()
        assertThat(historicPlanItemInstances.size)
            .`as`("The total number of plan items available in history is 13")
            .isEqualTo(13)
        assertThat(historicPlanItemInstances.filter { it.state == "completed" }).size()
            .`as`("The number of completed plan item instances in history is 7")
            .isEqualTo(7)
        assertThat(historicPlanItemInstances.filter { it.state == "terminated" }).size()
            .`as`("The number of terminated plan item instances in history is 6")
            .isEqualTo(6)

        val historicTaskInstances = cmmnHistoryService.createHistoricTaskInstanceQuery()
            .list()
        assertThat(historicTaskInstances.filter { it.assignee == "John Doe" }).size()
            .`as`("The number of tasks that have been assigned to John Doe should be 2")
            .isEqualTo(2)
        assertThat(historicTaskInstances.filter { it.deleteReason == "cmmn-state-transition-terminate-case" }).size()
            .`as`("The number of outstanding tasks that were terminated by executing by the reject action should be 2")
            .isEqualTo(2)

        val historicVariableInstance = cmmnHistoryService.createHistoricVariableInstanceQuery()
            .list()
        assertThat(historicVariableInstance.size)
            .`as`("The total number of variables should be 2")
            .isEqualTo(2)
        // first variable is initiator - added by default, second variable should be potentialEmployee
        assertThat(historicVariableInstance[1].variableName)
            .`as`("The variable name should be: potentialEmployee")
            .isEqualTo("potentialEmployee")
        assertThat(historicVariableInstance.asSequence().map{it.variableName to it.value}.toMap())
            .`as`("The variable name and value should be equal to potentialEmployee, John Doe")
            .isEqualTo(mapOf("initiator" to null, "potentialEmployee" to "John Doe"))

    }

}