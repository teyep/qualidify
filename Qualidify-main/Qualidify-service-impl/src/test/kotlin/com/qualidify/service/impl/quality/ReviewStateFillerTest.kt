package com.qualidify.service.impl.quality

import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class ReviewStateFillerTest {

    private val planItemService = mockk<PlanItemService>()
    private val caseInstanceService = mockk<CaseInstanceService>()

    private val fixture = ReviewStateFiller(planItemService, caseInstanceService)

    @AfterEach
    fun checkTest() {
        confirmVerified(planItemService, caseInstanceService)
        checkUnnecessaryStub(planItemService, caseInstanceService)
    }

    @Test
    fun `Check filling of review states`() {
        val qualityShell = TestEntities.caseInstance(id = "qualityShellId")
        val regularCase = TestEntities.caseInstance(id = "regularCaseInstanceId")

        every {
            caseInstanceService.getCaseInstanceDtoById("qualityShellId")
        } returns DtoFactory.caseInstanceDto(id = "qualityShellId", isQualityShell = true)

        every {
            caseInstanceService.getCaseInstanceDtoById("regularCaseInstanceId")
        } returns DtoFactory.caseInstanceDto(id = "regularCaseInstanceId", isQualityShell = false)

        every {
            planItemService.getPlanItemIdsByCase("qualityShellId")
        } returns listOf(
            "firstPlanItem",
            "secondPlanItem",
            "thirdPlanItem"
        )

        every {
            planItemService.getPlanItemIdsByCase("regularCaseInstanceId")
        } returns listOf(
            "fourthPlanItem",
            "fifthPlanItem"
        )

        val idList = mutableListOf<String>()
        val reviewStateList = mutableListOf<String>()
        every {
            planItemService.setPlanItemVariable(
                capture(idList), "reviewState",
                capture(reviewStateList)
            )
        } just Runs

        fixture.fill(qualityShell, regularCase)

        assertEquals(
            listOf(
                Pair("firstPlanItem", "not_required"),
                Pair("secondPlanItem", "not_required"),
                Pair("thirdPlanItem", "not_required"),
                Pair("fourthPlanItem", "required"),
                Pair("fifthPlanItem", "required")
            ),
            idList zip reviewStateList
        )

        verify(exactly = 2) { planItemService.getPlanItemIdsByCase(any()) }
        verify(exactly = 5) { planItemService.setPlanItemVariable(any(), any(), any()) }
        verify(exactly = 2) { caseInstanceService.getCaseInstanceDtoById(any()) }

    }

}