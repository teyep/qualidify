package com.qualidify.service.impl.process.cmmn

import com.qualidify.model.entities.CaseFile
import com.qualidify.service.impl.projects.CaseFileRepository
import com.qualidify.service.quality.CaseReviewService
import com.qualidify.service.quality.StageReviewService
import com.qualidify.service.quality.TaskReviewService
import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Disabled("Disabled because it is not testing Qualidify functionality, " +
        "but it is a showcase for CMMN! (disable this line to walk through")
class FlowableJpaEntityTest(
    val cmmnRuntimeService: CmmnRuntimeService,
    val cmmnTaskService: CmmnTaskService,
    @Autowired val caseFileRepository: CaseFileRepository,
) {

    // mock beans that overwrite the reviewService with null object, preventing startup of the review cascade
    @MockBean
    private var taskReviewService: TaskReviewService? = null
    @MockBean
    private var stageReviewService: StageReviewService? = null
    @MockBean
    private var caseReviewService: CaseReviewService? = null

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/jpaEntities.cmmn.xml"])
    fun `test Flowable with Document JPA entity`() {

        val caseFile = CaseFile(
            UUID.randomUUID(),
            "First",
            "First Try of casefile"
        )
        //first save the entity to the database
        caseFileRepository.save(caseFile)

        //Start the caseinstance
        val caseInstance: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("jpaEntities")
            .variable("link", caseFile)
            .start()
        assertNotNull(caseInstance, "The CMMN instance cannot be found")

        //Change variable in another process
        val changedCaseFile = caseFile.also {
            it.name = "second"
            it.description = "Second try"
        }
        caseFileRepository.save(changedCaseFile)

        //Try to get the variable from the database using the flowable framework
        val caseFileFromDatabase = cmmnRuntimeService.getVariable(caseInstance.id, "link") as CaseFile
        assertThat(caseFileFromDatabase.name).`as`("Entity should be changed by jpa repository but should be accessable in new state by flowable")
            .isEqualTo("second")

        //Start Planitem
        val planItem = cmmnRuntimeService.createPlanItemInstanceQuery().caseInstanceId(caseInstance.id).singleResult()
        assertEquals("Update document", planItem.name)
        cmmnRuntimeService.startPlanItemInstance(planItem.id)

        val caseFileChangedByFlowable = caseFileRepository.findById(caseFile.id).get()
        assertThat(caseFileChangedByFlowable.description).`as`("Entity should be chande by flowable")
            .isEqualTo("third try")

        val linkCount = caseFileRepository.count()
        assertThat(linkCount).isEqualTo(1)


    }



}