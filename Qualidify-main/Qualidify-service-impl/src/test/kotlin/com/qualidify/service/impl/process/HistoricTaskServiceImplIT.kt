package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.service.process.dto.BusinessProcessTaskDto
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.dto.TaskDto
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.engine.RuntimeService
import org.flowable.engine.TaskService
import org.flowable.engine.test.Deployment
import org.flowable.spring.impl.test.FlowableSpringExtension
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertTrue


/**
 * Integration test for the [HistoricTaskServiceImpl] to test counting and filtering of dto properties that
 * are not implemented in flowable queries
 *
 * @author Menno Tol
 * @author Marcel Pot
 * @since 1.0
 */
@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(FlowableSpringExtension::class),
    ExtendWith(SpringExtension::class),
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional
@Disabled(
    "Disabled because 1, quality shell is not implemented for bpmn tasks and " +
            "2, the case task view only works with tasks of type 'cmmn'"
)  // TODO: QUAL-556
internal open class HistoricTaskServiceImplIT(
    private val runtimeService: RuntimeService,
    private val taskService: TaskService,
    @Autowired private val projectTemplateService: ProjectTemplateService,
    @Autowired private val projectInstanceService: ProjectInstanceService,
    @Autowired private val planItemService: PlanItemServiceImpl,
    @Autowired private val cmmnTaskService: TaskServiceImpl,
    @Autowired private val processTaskService: BpmnTaskServiceImpl,
    @Autowired private val historicTaskService: HistoricTaskServiceImpl,
) {

    @Test
    @Deployment(resources = ["dev-resources/processes/simpleTask.bpmn20.xml"])
    @CmmnDeployment(
        resources =
        ["dev-resources/cases/basicRequirement.cmmn.xml", "dev-resources/cases/cmmnQualityShell.cmmn.xml"]
    )
    fun `Test taskflow`() {

        // start basic requirement via qualidify service (required for projectInstance wrapper to be available)
        val basicRequirementTemplate = projectTemplateService.findByQuery(
            SearchQuery<ProjectTemplateDto>(constraints = mapOf("cmmnModel" to "basicRequirement"))
        ).single()
        val projectInstanceId = projectTemplateService.startProject(basicRequirementTemplate)
        assertTrue(projectInstanceService.isProjectAvailable(projectInstanceId!!))

        // start runtime task via flowable service
        runtimeService.startProcessInstanceByKey("simpleTask")

        assertAll(
            { assertEquals(16, planItemService.countByQuery(CountQuery<PlanItemDto>()).count) },
            // 7 for the basicRequirement, 9 for the qualityShell

            { assertEquals(0, cmmnTaskService.countByQuery(CountQuery<TaskDto>()).count) },
            { assertEquals(1, processTaskService.countByQuery(CountQuery<BusinessProcessTaskDto>()).count) },
            { assertEquals(0, historicTaskService.countByQuery(CountQuery<TaskDto>()).count) },
        )

        // start the add ur task
        val planItemName = "Add User Requirement"
        val planItemDto =
            planItemService.findByQuery(SearchQuery<PlanItemDto>(constraints = mapOf("name" to planItemName)))
                .single()
        planItemService.startPlanItem(planItemDto)

        assertAll(
            { assertEquals(16, planItemService.countByQuery(CountQuery<PlanItemDto>()).count) },
            // 7 for the basicRequirement, 9 for the qualityShell

            { assertEquals(1, cmmnTaskService.countByQuery(CountQuery<TaskDto>()).count) },
            { assertEquals(1, processTaskService.countByQuery(CountQuery<BusinessProcessTaskDto>()).count) },
            { assertEquals(0, historicTaskService.countByQuery(CountQuery<TaskDto>()).count) },
        )

        // complete the add ur task
        val taskName = "Add User Requirement"
        val taskId = taskService.createTaskQuery().taskName(taskName).singleResult().id
        val taskDto = cmmnTaskService.getTaskDtoById(taskId)
        cmmnTaskService.completeOrResolveTask(taskDto, "testuser")

        assertAll(
            { assertEquals(18, planItemService.countByQuery(CountQuery<PlanItemDto>()).count) },
            // 7 for the basicRequirement, 9 for the qualityShell, 2 additional for completed task and new listener, -1 for the task that is in review

            {
                assertEquals(
                    2,
                    cmmnTaskService.countByQuery(CountQuery<TaskDto>()).count
                )
            }, // the review task and the still open regular task
            { assertEquals(1, processTaskService.countByQuery(CountQuery<BusinessProcessTaskDto>()).count) },
            { assertEquals(0, historicTaskService.countByQuery(CountQuery<TaskDto>()).count) },
        )
    }

}