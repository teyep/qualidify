package com.qualidify.service.impl.projects

import com.qualidify.metadata.DataType
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.exceptions.FormNotFoundException
import com.qualidify.service.projects.TaskService
import com.qualidify.service.quality.TaskReviewService
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.flowable.form.api.FormRepositoryService
import org.flowable.form.engine.test.FormDeploymentAnnotation
import org.flowable.form.spring.test.FlowableFormSpringExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension


/**
 * Integration test for the FormMetadataServiceImpl
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Extensions(
    ExtendWith(FlowableFormSpringExtension::class),
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@DirtiesContext
internal class FormMetadataServiceImplIT(
    private val formRepositoryService: FormRepositoryService,
    private val cmmnTaskService: CmmnTaskService,
    private val cmmnRuntimeService: CmmnRuntimeService,
    @Autowired private val taskService: TaskService,
) {

    // mock bean will overwrite the taskReviewService with a null object, preventing startup of the review cascade
    @MockBean
    private var taskReviewService: TaskReviewService? = null

    private var formMetadataService = FormMetadataServiceImpl(formRepositoryService, cmmnTaskService, taskService)

    @Test
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun `load FormMetadata by key`() {
        val key = "form1"
        val metadata = formMetadataService.loadFormModelMetadataByFormKey(key)

        assertThat(metadata).`as`("Formmetadata has five fields").hasSize(5)
            .contains(
                ColumnDef(
                    "textInput1",
                    "NameTextInput1",
                    DataType.STRING,
                    mapOf("label" to "NameTextInput1", "placeholder" to "emptyInput1", "isRequired" to "true"),
                ),
                ColumnDef(
                    "multiTextInput2",
                    "NameMultiTextInput2",
                    DataType.TEXT,
                    mapOf("label" to "NameMultiTextInput2", "placeholder" to "emptyInput2", "isRequired" to "false"),
                ),
                ColumnDef(
                    "integerInput3",
                    "NameIntegerInput3",
                    DataType.INTEGER,
                    mapOf("label" to "NameIntegerInput3", "placeholder" to "123", "isRequired" to "false"),
                ),
                ColumnDef(
                    "booleanInput4",
                    "NameBooleanInput4",
                    DataType.BOOLEAN,
                    mapOf("label" to "NameBooleanInput4", "placeholder" to "true", "isRequired" to "true"),
                ),
                ColumnDef(
                    "dateInput5",
                    "NameDateInput5",
                    DataType.DATE,
                    mapOf("label" to "NameDateInput5", "placeholder" to "emptyInput5", "isRequired" to "false"),
                ),
            )
    }

    @Test
    fun `load FormMetadata by unkown key`() {
        val key = "unkown"
        assertThatThrownBy {
            formMetadataService.loadFormModelMetadataByFormKey(key)
        }.isInstanceOf(FormNotFoundException::class.java)
            .hasMessage("Cannot find form with key $key")
            .hasCauseInstanceOf(FlowableObjectNotFoundException::class.java)
    }

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/smallRequirement.cmmn.xml"])
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun `load FormMetadata by cmmn taskid`() {
        val caseInstance = startCmmn("smallRequirement")
        val tasks = loadTasks(caseInstance.id)

        assertThat(tasks).`as`("Check if task data is correctly loaded!").hasSize(1)

        val metadata = formMetadataService.loadFormModelMetadataByTaskId(tasks[0].id)

        assertThat(metadata).`as`("Formmetadata has five fields field").hasSize(5)
            .contains(
                ColumnDef(
                    "textInput1",
                    "NameTextInput1",
                    DataType.STRING,
                    mapOf("label" to "NameTextInput1", "placeholder" to "emptyInput1", "isRequired" to "true"),
                ),
                ColumnDef(
                    "multiTextInput2",
                    "NameMultiTextInput2",
                    DataType.TEXT,
                    mapOf("label" to "NameMultiTextInput2", "placeholder" to "emptyInput2", "isRequired" to "false"),
                ),
                ColumnDef(
                    "integerInput3",
                    "NameIntegerInput3",
                    DataType.INTEGER,
                    mapOf("label" to "NameIntegerInput3", "placeholder" to "123", "isRequired" to "false"),
                ),
                ColumnDef(
                    "booleanInput4",
                    "NameBooleanInput4",
                    DataType.BOOLEAN,
                    mapOf("label" to "NameBooleanInput4", "placeholder" to "true", "isRequired" to "true"),
                ),
                ColumnDef(
                    "dateInput5",
                    "NameDateInput5",
                    DataType.DATE,
                    mapOf("label" to "NameDateInput5", "placeholder" to "emptyInput5", "isRequired" to "false"),
                ),
            )
    }

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/smallRequirement.cmmn.xml"])
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun `load FormMetadata by unknown cmmn taskid`() {
        val taskId = "UnknownTaskId"
        assertThatThrownBy {
            formMetadataService.loadFormModelMetadataByTaskId(taskId)
        }.isInstanceOf(FormNotFoundException::class.java)
            .hasMessage("Cannot find form for task with $taskId")
            .hasCauseInstanceOf(FlowableObjectNotFoundException::class.java)
    }

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/smallRequirement.cmmn.xml"])
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun `save and get Formdata by task`() {
        val caseInstance = startCmmn("smallRequirement")
        val tasks = loadTasks(caseInstance.id)
        val data = mapOf<String, Any?>("Input1" to "Qualidify")

        formMetadataService.saveTaskFormData(tasks[0].id, data)

        val formVariables = formMetadataService.getFormVariables(tasks[0].id)

        assertThat(formVariables).`as`("Data should be retrieved unchanged")
            .containsAllEntriesOf(data)
    }

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/twoEqualForms.cmmn.xml"])
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun `save and get Formdata by two tasks`() {
        val caseInstance = startCmmn("twoEqualForms")
        val tasks = loadTasks(caseInstance.id)
        val data1 = mapOf<String, Any?>("Input1" to "Qualidify")
        val data2 = mapOf<String, Any?>("Input2" to "Qualidify2")

        formMetadataService.saveTaskFormData(tasks[0].id, data1)
        formMetadataService.saveTaskFormData(tasks[1].id, data2)

        val formVariablesTask1 = formMetadataService.getFormVariables(tasks[0].id)
        val formVariablesTask2 = formMetadataService.getFormVariables(tasks[1].id)

        assertThat(formVariablesTask1).`as`("Data of Task1 should be retrieved unchanged")
            .containsAllEntriesOf(data1)
        assertThat(formVariablesTask2).`as`("Data of Task2 should be retrieved unchanged")
            .containsAllEntriesOf(data2)
    }

    @Test
    @CmmnDeployment(resources = ["dev-resources/cases/smallRequirement.cmmn.xml"])
    @FormDeploymentAnnotation(resources = ["dev-resources/forms/testform.form"])
    fun `save and get Formdata by unknown task`() {
        val unknownTask = DtoFactory.taskDto(id = "UnknownTaskId")
        val data = mapOf<String, Any?>("Input1" to "Qualidify")

        assertThatThrownBy {
            formMetadataService.saveTaskFormData(unknownTask.id, data)
        }.isInstanceOf(FormNotFoundException::class.java)
            .hasMessage("Cannot find form for task with taskId ${unknownTask.id}, or the task has been completed")
            .hasCauseInstanceOf(FlowableObjectNotFoundException::class.java)
    }

    private fun startCmmn(key: String): CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
        .caseDefinitionKey(key)
        .parentId("anyIdButNullShouldBeFine")
        .start()

    private fun loadTasks(caseInstanceId: String) =
        cmmnTaskService.createTaskQuery().caseInstanceId(caseInstanceId).list()

}
