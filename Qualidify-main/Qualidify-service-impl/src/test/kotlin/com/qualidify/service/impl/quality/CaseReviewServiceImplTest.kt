package com.qualidify.service.impl.quality

import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.process.exceptions.CaseInstanceNotFoundException
import com.qualidify.service.projects.TaskService
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.flowable.variable.service.impl.persistence.entity.HistoricVariableInstanceEntityImpl
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.fail

/**
 * Unit test for the implementation of the [CaseReviewService]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class CaseReviewServiceImplTest {

    private val caseInstanceService = mockk<CaseInstanceService>()
    private val planItemService = mockk<PlanItemService>()
    private val taskService = mockk<TaskService>()

    private val fixture = CaseReviewServiceImpl(caseInstanceService, planItemService, taskService)

    @AfterEach
    fun checkTest() {
        confirmVerified(caseInstanceService, planItemService, taskService)
        checkUnnecessaryStub(caseInstanceService, planItemService, taskService)
    }

    @Test
    fun `Test start review cascade with case`() {
        prepareReviewCascadeStubbing()

        val caseInstance = TestEntities.caseInstance()

        fixture.startReview(caseInstance)

        verify(exactly = 2) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }
        verify(exactly = 1) { planItemService.getPlanItemsByStageAndDefinition(any()) }
        verify(exactly = 1) { planItemService.completeGenericEventListener(any()) }
        verify(exactly = 4) { planItemService.setPlanItemVariable(any(), any(), any()) }
        verify(exactly = 3) { caseInstanceService.setCaseVariable(any(), any(), any()) }
        verify { taskService wasNot called }

    }

    @ParameterizedTest
    @MethodSource("getReviewStates")
    fun `Test review cascade with different review states`(state: String?) {
        val caseInstance = TestEntities.caseInstance()

        when (state) {
            ReviewState.REVIEW_NOT_REQUIRED.state, ReviewState.APPROVED.state, ReviewState.REJECTED.state,
            ReviewState.IN_REVIEW.state, ReviewState.CORRECTED.state,
            -> {
                // minimal stubbing required to reach code that is tested
                every {
                    caseInstanceService.getCaseInstanceDtoById(any())
                } returns DtoFactory.caseInstanceDto(reviewState = ReviewState.getByName(state))

                fixture.startReview(caseInstance)

                verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
                verify { planItemService wasNot called }
                verify { taskService wasNot called }
            }

            ReviewState.REVIEW_REQUIRED.state, ReviewState.UNDEFINED.state, null,
            -> {
                // complete stubbing required for happy path
                prepareReviewCascadeStubbing()

                fixture.startReview(caseInstance)

                // basically follow the happy path verifications of the basic test above
                verify(exactly = 2) { caseInstanceService.getCaseInstanceDtoById(any()) }
                verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }
                verify(exactly = 1) { planItemService.getPlanItemsByStageAndDefinition(any()) }
                verify(exactly = 1) { planItemService.completeGenericEventListener(any()) }
                verify(exactly = 4) { planItemService.setPlanItemVariable(any(), any(), any()) }
                verify(exactly = 3) { caseInstanceService.setCaseVariable(any(), any(), any()) }
                verify { taskService wasNot called }

            }

            else -> {
                fail("Review state '$state' is not implemented in the CaseReviewService and/or its tests.")
            }
        }

    }

    @ParameterizedTest
    @MethodSource("getReviewStatesNonNull")
    fun `Test approve review with different review states`(state: String) {
        val reviewTaskDto = DtoFactory.taskDto(
            id = "reviewTaskId",
            planItemInstanceId = "reviewTaskPlanItemId",
            isQualityShell = true,
            qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
            qualityLinkId = "regularCaseInstanceId",
            reviewState = ReviewState.getByName(state)
        )
        val principal = "testUser"

        every {
            caseInstanceService.getHistoricCaseInstanceDtoById(reviewTaskDto.qualityLinkId!!)
        } returns DtoFactory.caseInstanceDto(
            id = "regularCaseInstanceId",
            isQualityShell = false,
            reviewState = ReviewState.getByName(state),
        )

        when (state) {
            ReviewState.IN_REVIEW.state,  // happy path
            -> {

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    qualityLinkId = "regularCaseInstanceId",
                    type = PlanItemType.HUMAN_TASK
                )

                every {
                    caseInstanceService.getCaseInstanceDtoById("regularCaseInstanceId")
                } returns DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                val regularCaseReviewState = slot<String>()
                every {
                    caseInstanceService.setCaseVariable(
                        "regularCaseInstanceId",
                        "reviewState",
                        capture(regularCaseReviewState)
                    )
                } just Runs

                val reviewTaskReviewState = slot<String>()
                every {
                    planItemService.setPlanItemVariable(
                        "reviewTaskPlanItemId",
                        "reviewState",
                        capture(reviewTaskReviewState)
                    )
                } just Runs

                every { taskService.completeTask(any()) } just Runs

                fixture.approveReview(reviewTaskDto, principal)

                assertEquals(ReviewState.APPROVED, ReviewState.getByName(regularCaseReviewState.captured))
                assertEquals(ReviewState.APPROVED, ReviewState.getByName(reviewTaskReviewState.captured))

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
                verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
                verify(exactly = 1) { caseInstanceService.setCaseVariable(any(), any(), any()) }
                verify(exactly = 1) { caseInstanceService.getHistoricCaseInstanceDtoById(any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable(any(), any(), any()) }
                verify(exactly = 1) { taskService.completeTask(any()) }

            }

            else -> {  // all other states are not allowed
                val exception =
                    assertFailsWith<CaseReviewServiceException> { fixture.approveReview(reviewTaskDto, principal) }

                assertThat(exception.message).isEqualTo(
                    "Unsupported review state '${reviewTaskDto.reviewState.state}' for case review approval, " +
                            "the only supported state is '${ReviewState.IN_REVIEW.state}'."
                )

                verify(exactly = 1) { caseInstanceService.getHistoricCaseInstanceDtoById(any()) }
                verify { planItemService wasNot called }
                verify { taskService wasNot called }
            }
        }
    }

    @Test
    fun `Test approve review with update historic review state`() {
        val reviewTaskDto = DtoFactory.taskDto(
            id = "reviewTaskId",
            planItemInstanceId = "reviewTaskPlanItemId",
            isQualityShell = true,
            qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
            qualityLinkId = "regularCaseInstanceId",
            reviewState = ReviewState.getByName(ReviewState.IN_REVIEW.state)
        )
        val principal = "testUser"

        every {
            planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
        } returns DtoFactory.planItemDto(
            id = "reviewTaskPlanItemId",
            isQualityShell = true,
            qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
            qualityLinkId = "regularCaseInstanceId",
            reviewState = ReviewState.getByName(ReviewState.IN_REVIEW.state)
        )

        every {
            caseInstanceService.getCaseInstanceDtoById(reviewTaskDto.qualityLinkId!!)
        } throws CaseInstanceNotFoundException("On purpose")

        every {
            caseInstanceService.getHistoricCaseInstanceDtoById(reviewTaskDto.qualityLinkId!!)
        } returns DtoFactory.caseInstanceDto(
            id = "regularCaseInstanceId",
            isQualityShell = false,
            reviewState = ReviewState.getByName(ReviewState.IN_REVIEW.state),
        )

        every {
            caseInstanceService.setCaseVariable(
                "regularCaseInstanceId",
                "reviewState",
                ReviewState.APPROVED.state
            )
        } throws FlowableObjectNotFoundException("On purpose")

        every { planItemService.setPlanItemVariable(any(), any(), any()) } just Runs

        every {
            caseInstanceService.getHistoricCaseVariable("regularCaseInstanceId", "reviewState")
        } returns HistoricVariableInstanceEntityImpl()

        every { caseInstanceService.updateHistoricVariable(any(), "approved") } just Runs

        every { taskService.completeTask(any()) } just Runs

        fixture.approveReview(reviewTaskDto, principal)

        verify(exactly = 2) { caseInstanceService.getHistoricCaseInstanceDtoById(any()) }
        verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify(exactly = 1) { caseInstanceService.setCaseVariable(any(), any(), any()) }
        verify(exactly = 1) { caseInstanceService.getHistoricCaseVariable(any(), any()) }
        verify(exactly = 1) { caseInstanceService.updateHistoricVariable(any(), any()) }
        verify(exactly = 1) { planItemService.getPlanItemDtoOrNull(any()) }
        verify(exactly = 2) { planItemService.setPlanItemVariable(any(), any(), any()) }
        verify(exactly = 1) { taskService.completeTask(any()) }

    }

    private fun prepareReviewCascadeStubbing() {

        every {
            caseInstanceService.getCaseInstanceDtoById("caseInstance")
        } returns DtoFactory.caseInstanceDto("caseInstanceId")

        every {
            caseInstanceService.getCaseInstanceDtoById("qualityShellId")
        } returns DtoFactory.caseInstanceDto(
            id = "qualityShellId",
            isQualityShell = true,
            qualityShellId = "cmmnQualityShellId",
        )

        every {
            planItemService.getEventListenersByCaseInstanceId("qualityShellId")
        } returns mutableListOf(
            DtoFactory.planItemDto(
                name = "Start case review",
                type = PlanItemType.GENERIC_EVENT_LISTENER,
                state = PlanItemState.AVAILABLE,
                isQualityShell = true,
            )
        )

        every {
            planItemService.getPlanItemsByStageAndDefinition(any())
        } returns mutableListOf(
            DtoFactory.planItemDto(
                name = "Review case",
                state = PlanItemState.AVAILABLE,
                isQualityShell = true,
            )
        )

        every {
            planItemService.completeGenericEventListener(any())
        } just Runs

        every {
            planItemService.setPlanItemVariable(any(), any(), any())
        } just Runs

        every {
            caseInstanceService.setCaseVariable(any(), any(), any())
        } just Runs

    }

    companion object {

        @JvmStatic
        private fun getReviewStatesNonNull(): Set<String> = ReviewState.values().map { it.state }.toSet()

        @JvmStatic
        private fun getReviewStates(): Set<String?> = getReviewStatesNonNull().plus(null)

    }

}