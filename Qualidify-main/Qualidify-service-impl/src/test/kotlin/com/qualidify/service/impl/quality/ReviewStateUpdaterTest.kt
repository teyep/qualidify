package com.qualidify.service.impl.quality

import com.qualidify.service.DtoFactory
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.ReviewType
import io.mockk.*
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.flowable.variable.api.history.HistoricVariableInstance
import org.flowable.variable.service.impl.persistence.entity.HistoricVariableInstanceEntityImpl
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

/**
 * Unit test for the [ReviewStateUpdater].
 * @author Menno Tol
 * @since 1.0
 */
internal class ReviewStateUpdaterTest {

    private val planItemService = mockk<PlanItemService>()
    private val caseInstanceService = mockk<CaseInstanceService>()

    @AfterEach
    fun checkTest() {
        confirmVerified(planItemService, caseInstanceService)
        checkUnnecessaryStub(planItemService, caseInstanceService)
    }

    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Update review states with regular item dto`(reviewType: ReviewType) {

        when (reviewType) {
            ReviewType.CASE -> {

                val dto = DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
                    qualityLinkId = "regularCaseInstanceId"
                )

                every {
                    caseInstanceService.setCaseVariable("regularCaseInstanceId", any(), any())
                } just Runs

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                val fixture = ReviewStateUpdater<CaseInstanceDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { caseInstanceService.setCaseVariable("regularCaseInstanceId", any(), any()) }

            }

            ReviewType.STAGE -> {

                val dto = DtoFactory.planItemDto(
                    id = "regularStageId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID,
                    qualityLinkId = "regularStageId"
                )

                every {
                    planItemService.setPlanItemVariable("regularStageId", any(), any())
                } just Runs

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                val fixture = ReviewStateUpdater<PlanItemDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("regularStageId", any(), any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }

            }

            ReviewType.TASK -> {

                val dto = DtoFactory.taskDto(
                    id = "regularTaskId",
                    planItemInstanceId = "regularTaskPlanItemId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "regularTaskPlanItemId"
                )

                every {
                    planItemService.setPlanItemVariable("regularTaskPlanItemId", any(), any())
                } just Runs

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                val fixture = ReviewStateUpdater<TaskDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("regularTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }

            }

        }

    }

    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Update review states with review item dto`(reviewType: ReviewType) {

        when (reviewType) {
            ReviewType.CASE -> {

                val dto = DtoFactory.taskDto(
                    id = "reviewTaskId",
                    planItemInstanceId = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
                    qualityLinkId = "regularCaseId",
                )

                every {
                    caseInstanceService.getCaseInstanceDtoById("regularCaseId")
                } returns DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
                    qualityLinkId = "regularCaseId",
                )

                every {
                    caseInstanceService.setCaseVariable("regularCaseInstanceId", any(), any())
                } just Runs

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                val fixture = ReviewStateUpdater<CaseInstanceDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById("regularCaseId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { caseInstanceService.setCaseVariable("regularCaseInstanceId", any(), any()) }

            }

            ReviewType.STAGE -> {

                val dto = DtoFactory.taskDto(
                    id = "reviewTaskPlanItemId",
                    planItemInstanceId = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID,
                    qualityLinkId = "regularStageId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("regularStageId")
                } returns DtoFactory.planItemDto(
                    id = "regularStageId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID,
                    qualityLinkId = "regularStageId",
                )

                every {
                    planItemService.setPlanItemVariable("regularStageId", any(), any())
                } just Runs

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                val fixture = ReviewStateUpdater<PlanItemDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("regularStageId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("regularStageId", any(), any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }

            }

            ReviewType.TASK -> {

                val dto = DtoFactory.taskDto(
                    id = "reviewTaskId",
                    planItemInstanceId = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "regularTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("regularTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "regularTaskPlanItemId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId"
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "regularTaskPlanItemId",
                )

                every {
                    planItemService.setPlanItemVariable("regularTaskPlanItemId", any(), any())
                } just Runs

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                val fixture = ReviewStateUpdater<TaskDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("regularTaskPlanItemId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("regularTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }

            }

        }

    }

    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Update review states with historic regular item dto`(reviewType: ReviewType) {
        when (reviewType) {

            ReviewType.CASE -> {

                val dto = DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
                    qualityLinkId = "regularCaseInstanceId"
                )

                every {
                    caseInstanceService.setCaseVariable("regularCaseInstanceId", any(), any())
                } throws FlowableObjectNotFoundException("On purpose")

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                every {
                    caseInstanceService.getHistoricCaseVariable("regularCaseInstanceId", any())
                } returns HistoricVariableInstanceEntityImpl() as HistoricVariableInstance

                every { caseInstanceService.updateHistoricVariable(any(), any()) } just Runs

                val fixture = ReviewStateUpdater<CaseInstanceDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { caseInstanceService.setCaseVariable("regularCaseInstanceId", any(), any()) }
                verify(exactly = 1) { caseInstanceService.getHistoricCaseVariable("regularCaseInstanceId", any()) }
                verify(exactly = 1) { caseInstanceService.updateHistoricVariable(any(), any()) }

            }

            ReviewType.STAGE -> {

                val dto = DtoFactory.planItemDto(
                    id = "regularStageId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID,
                    qualityLinkId = "regularStageId"
                )

                every {
                    planItemService.setPlanItemVariable("regularStageId", any(), any())
                } throws FlowableObjectNotFoundException("On purpose")

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                every {
                    planItemService.getHistoricPlanItemVariable("regularStageId", any())
                } returns HistoricVariableInstanceEntityImpl() as HistoricVariableInstance

                every { planItemService.updateHistoricVariable(any(), any()) } just Runs

                val fixture = ReviewStateUpdater<PlanItemDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("regularStageId", any(), any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { planItemService.getHistoricPlanItemVariable("regularStageId", any()) }
                verify(exactly = 1) { planItemService.updateHistoricVariable(any(), any()) }

            }

            ReviewType.TASK -> {

                val dto = DtoFactory.taskDto(
                    id = "regularTaskId",
                    planItemInstanceId = "regularTaskPlanItemId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "regularTaskPlanItemId"
                )

                every {
                    planItemService.setPlanItemVariable("regularTaskPlanItemId", any(), any())
                } throws FlowableObjectNotFoundException("On purpose")

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                every {
                    planItemService.getHistoricPlanItemVariable("regularTaskPlanItemId", any())
                } returns HistoricVariableInstanceEntityImpl() as HistoricVariableInstance

                every { planItemService.updateHistoricVariable(any(), any()) } just Runs

                val fixture = ReviewStateUpdater<TaskDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("regularTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { planItemService.getHistoricPlanItemVariable("regularTaskPlanItemId", any()) }
                verify(exactly = 1) { planItemService.updateHistoricVariable(any(), any()) }

            }

        }
    }

    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Update review states with historic review item dto`(reviewType: ReviewType) {

        when (reviewType) {
            ReviewType.CASE -> {

                val dto = DtoFactory.taskDto(
                    id = "reviewTaskId",
                    planItemInstanceId = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
                    qualityLinkId = "regularCaseId",
                )

                every {
                    caseInstanceService.getCaseInstanceDtoById("regularCaseId")
                } returns DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_CASE_ID,
                    qualityLinkId = "regularCaseId",
                )

                every {
                    caseInstanceService.setCaseVariable("regularCaseInstanceId", any(), any())
                } throws FlowableObjectNotFoundException("On purpose")

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                every {
                    caseInstanceService.getHistoricCaseVariable("regularCaseInstanceId", any())
                } returns HistoricVariableInstanceEntityImpl() as HistoricVariableInstance

                every { caseInstanceService.updateHistoricVariable(any(), any()) } just Runs

                val fixture = ReviewStateUpdater<CaseInstanceDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById("regularCaseId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { caseInstanceService.setCaseVariable("regularCaseInstanceId", any(), any()) }
                verify(exactly = 1) { caseInstanceService.getHistoricCaseVariable("regularCaseInstanceId", any()) }
                verify(exactly = 1) { caseInstanceService.updateHistoricVariable(any(), any()) }

            }

            ReviewType.STAGE -> {

                val dto = DtoFactory.taskDto(
                    id = "reviewTaskPlanItemId",
                    planItemInstanceId = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID,
                    qualityLinkId = "regularStageId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("regularStageId")
                } returns DtoFactory.planItemDto(
                    id = "regularStageId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID,
                    qualityLinkId = "regularStageId",
                )

                every {
                    planItemService.setPlanItemVariable("regularStageId", any(), any())
                } throws FlowableObjectNotFoundException("On purpose")

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                every {
                    planItemService.getHistoricPlanItemVariable("regularStageId", any())
                } returns HistoricVariableInstanceEntityImpl() as HistoricVariableInstance

                every { planItemService.updateHistoricVariable(any(), any()) } just Runs

                val fixture = ReviewStateUpdater<PlanItemDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("regularStageId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("regularStageId", any(), any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { planItemService.getHistoricPlanItemVariable("regularStageId", any()) }
                verify(exactly = 1) { planItemService.updateHistoricVariable(any(), any()) }

            }

            ReviewType.TASK -> {

                val dto = DtoFactory.taskDto(
                    id = "reviewTaskId",
                    planItemInstanceId = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "regularTaskPlanItemId",
                )

                every {
                    planItemService.getPlanItemDtoOrNull("regularTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "regularTaskPlanItemId",
                    isQualityShell = false,
                    qualityLinkKey = QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "reviewTaskPlanItemId"
                )

                every {
                    planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId")
                } returns DtoFactory.planItemDto(
                    id = "reviewTaskPlanItemId",
                    isQualityShell = true,
                    qualityLinkKey = QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID,
                    qualityLinkId = "regularTaskPlanItemId",
                )

                every {
                    planItemService.setPlanItemVariable("regularTaskPlanItemId", any(), any())
                } throws FlowableObjectNotFoundException("On purpose")

                every {
                    planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any())
                } just Runs

                every {
                    planItemService.getHistoricPlanItemVariable("regularTaskPlanItemId", any())
                } returns HistoricVariableInstanceEntityImpl() as HistoricVariableInstance

                every { planItemService.updateHistoricVariable(any(), any()) } just Runs

                val fixture = ReviewStateUpdater<TaskDto>(planItemService, caseInstanceService)
                fixture.start(dto, ReviewState.APPROVED)

                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("reviewTaskPlanItemId") }
                verify(exactly = 1) { planItemService.getPlanItemDtoOrNull("regularTaskPlanItemId") }
                verify(exactly = 1) { planItemService.setPlanItemVariable("regularTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { planItemService.setPlanItemVariable("reviewTaskPlanItemId", any(), any()) }
                verify(exactly = 1) { planItemService.getHistoricPlanItemVariable("regularTaskPlanItemId", any()) }
                verify(exactly = 1) { planItemService.updateHistoricVariable(any(), any()) }

            }

        }

    }

    companion object {
        @JvmStatic
        private fun getReviewTypes() = ReviewType.values()
    }

}