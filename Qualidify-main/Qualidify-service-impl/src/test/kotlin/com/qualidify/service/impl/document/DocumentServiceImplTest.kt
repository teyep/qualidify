package com.qualidify.service.impl.document

import com.qualidify.model.entities.Document
import com.qualidify.model.entities.DocumentAttribute
import com.qualidify.model.entities.DocumentContent
import com.qualidify.model.entities.DocumentId
import com.qualidify.service.document.DocumentService
import com.qualidify.service.document.dto.DocumentContentDto
import com.qualidify.service.document.dto.DocumentDto
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.time.LocalDateTime
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class DocumentServiceImplTest {

    @Mock
    lateinit var documentRepository: DocumentRepository

    @Mock
    lateinit var labelRepository: LabelRepository

    @Mock
    lateinit var documentContentRepository: DocumentContentRepository

    val documentMapper = DocumentMapper()
    val labelMapper = LabelMapper()
    val documentContentMapper = DocumentContentMapper()

    lateinit var fixture: DocumentService

    @BeforeEach
    fun setup() {
        fixture = DocumentServiceImpl(
            documentRepository,
            labelRepository,
            documentContentRepository,
            documentMapper,
            labelMapper,
            documentContentMapper
        )
    }

    @Test
    fun `get All Documents In Path`() {
        val path = "\\"
        val doc_1 = getDocument(path, "Document_1")
        val doc_2 = getDocument(path, "Document_2")
        val doc_3 = getDocument(path, "Document_3")

        `when`(documentRepository.findAllByPath(path)).thenReturn(listOf(doc_1, doc_2, doc_3))

        val documents = fixture.getAllDocumentsInPath(path)

        assertThat(documents).`as`("Retrieved 3 documents").hasSize(3)
        assertThat(documents[1].path).isEqualTo(path)
        assertThat(documents[1].name).isEqualTo("Document_2")
        assertThat(documents[2].attributes.size).isEqualTo(2)

        verify(documentRepository, times(1)).findAllByPath(path)
        verifyNoMoreInteractions(documentRepository)
        verifyNoInteractions(labelRepository, documentContentRepository)

    }

    @Test
    fun `Create a new Document`() {
        val path = "\\"

        val document = fixture.createDocument(path, "newDocument", "doc", "Content".toByteArray())

        assertThat(document).`as`("Document has class DocumentDto").isInstanceOf(DocumentDto::class.java)
        assertThat(document.path).`as`("Document path is \\").isEqualTo(path)
        assertThat(document.name).`as`("Document name is 'newDocument'").isEqualTo("newDocument")
        assertThat(document.type).`as`("Document type is 'doc'").isEqualTo("doc")
        assertThat(document.version).`as`("Document version is 1").isEqualTo(1)
        assertThat(document.size).isEqualTo("Content".toByteArray().size.toLong())
        assertThat(document.attributes).isEmpty()

        verify(documentRepository, times(1)).save(any(Document::class.java))
        verify(documentContentRepository, times(1)).save(any(DocumentContent::class.java))

        verifyNoMoreInteractions(documentRepository, documentContentRepository)
        verifyNoInteractions(labelRepository)
    }

    @Test
    fun `Create a new document with Dto information`() {
        val path = "\\"
        val documentDto = getDocumentDto(path, "newDocument", "doc")
        val contentDto = DocumentContentDto(
            documentId = documentDto.documentId,
            content = "Content".toByteArray()
        )
        val document = fixture.createDocument(documentDto, contentDto)

        assertThat(document).`as`("Document has class DocumentDto").isInstanceOf(DocumentDto::class.java)
        assertThat(document.path).`as`("Document path is \\").isEqualTo(path)
        assertThat(document.name).`as`("Document name is 'newDocument'").isEqualTo("newDocument")
        assertThat(document.type).`as`("Document type is 'doc'").isEqualTo("doc")
        assertThat(document.version).`as`("Document version is 1").isEqualTo(1)
        assertThat(document.size).isEqualTo("Content".toByteArray().size.toLong())
        assertThat(document.attributes).hasSize(2)

        verify(documentRepository, times(1)).save(any(Document::class.java))
        verify(documentContentRepository, times(1)).save(any(DocumentContent::class.java))

        verifyNoMoreInteractions(documentRepository, documentContentRepository)
        verifyNoInteractions(labelRepository)
    }

    @Test
    fun `Update a document`() {
        val path = "\\"
        val name = "updatableDocument"
        val type = "doc"

        val previousDocument = getDocument(path, name, type)

        `when`(documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type))
            .thenReturn(Optional.of(previousDocument))

        val updatedDocument = fixture.updateDocument(path, name, type, "Content".toByteArray())

        assertThat(updatedDocument).`as`("Document has class DocumentDto").isInstanceOf(DocumentDto::class.java)
        assertThat(updatedDocument.documentId).`as`("Document path is not equal to old documentid")
            .isNotEqualTo(previousDocument.id.unbox())
        assertThat(updatedDocument.path).`as`("Document path is \\").isEqualTo(path)
        assertThat(updatedDocument.name).`as`("Document name is '$name'").isEqualTo(name)
        assertThat(updatedDocument.type).`as`("Document type is 'doc'").isEqualTo(type)
        assertThat(updatedDocument.version).`as`("Document version is 2").isEqualTo(2)
        assertThat(updatedDocument.size).isEqualTo("Content".toByteArray().size.toLong())
        assertThat(updatedDocument.attributes).hasSize(2)

        verify(documentRepository, times(1)).findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type)
        verify(documentRepository, times(2)).save(any(Document::class.java))
        verify(documentContentRepository, times(1)).save(any(DocumentContent::class.java))

        verifyNoMoreInteractions(documentRepository, documentContentRepository)
        verifyNoInteractions(labelRepository)
    }

    @Test
    fun `Update a not existing document`() {
        val path = "\\"
        val name = "updatableDocument"
        val type = "doc"

        `when`(documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type))
            .thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.updateDocument(path, name, type, "Content".toByteArray())
        }.isInstanceOf(DocumentNotFoundException::class.java)
            .hasMessage("The document $path$name.$type cannot be found")

        verify(documentRepository, times(1)).findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type)
        verifyNoMoreInteractions(documentRepository)
        verifyNoInteractions(labelRepository, documentContentRepository)
    }

    @Test
    fun `Update a document with dto's`() {
        val path = "\\"
        val name = "updatableDocument"
        val type = "doc"

        val previousDocument = getDocument(path, name, type)
        val previousDocumentDto = documentMapper.convertToDto(previousDocument)


        val contentDto = DocumentContentDto(
            documentId = previousDocumentDto.documentId,
            content = "Content".toByteArray()
        )

        `when`(documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type))
            .thenReturn(Optional.of(previousDocument))

        val updatedDocument = fixture.updateDocument(previousDocumentDto, contentDto)

        assertThat(updatedDocument).`as`("Document has class DocumentDto").isInstanceOf(DocumentDto::class.java)
        assertThat(updatedDocument.documentId).`as`("Document path is not equal to old documentid")
            .isNotEqualTo(previousDocument.id.unbox())
        assertThat(updatedDocument.path).`as`("Document path is \\").isEqualTo(path)
        assertThat(updatedDocument.name).`as`("Document name is '$name'").isEqualTo(name)
        assertThat(updatedDocument.type).`as`("Document type is 'doc'").isEqualTo(type)
        assertThat(updatedDocument.version).`as`("Document version is 2").isEqualTo(2)
        assertThat(updatedDocument.size).isEqualTo("Content".toByteArray().size.toLong())
        assertThat(updatedDocument.attributes).hasSize(2)

        verify(documentRepository, times(1)).findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type)
        verify(documentRepository, times(2)).save(any(Document::class.java))
        verify(documentContentRepository, times(1)).save(any(DocumentContent::class.java))

        verifyNoMoreInteractions(documentRepository, documentContentRepository)
        verifyNoInteractions(labelRepository)
    }

    @Test
    fun `find a document with path, name and type`() {
        val path = "\\"

        `when`(documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, "document", "doc"))
            .thenReturn(Optional.of(getDocument(path, "document", "doc")))

        val document = fixture.getDocument(path, "document", "doc")

        assertThat(document).`as`("Document has class DocumentDto").isInstanceOf(DocumentDto::class.java)
        assertThat(document.path).`as`("Document path is \\").isEqualTo(path)
        assertThat(document.name).`as`("Document name is 'document'").isEqualTo("document")
        assertThat(document.type).`as`("Document type is 'doc'").isEqualTo("doc")
        assertThat(document.version).`as`("Document version is 1").isEqualTo(1)
        assertThat(document.size).`as`("Document size is 64").isEqualTo(64L)
        assertThat(document.attributes).hasSize(2)

        verify(documentRepository, times(1)).findDocumentByPathAndNameAndTypeAndDeletedIsFalse(
            path,
            "document",
            "doc"
        )
        verifyNoMoreInteractions(documentRepository)
        verifyNoInteractions(documentContentRepository, labelRepository)
    }

    @Test
    fun `find a not existing document with path, name and type`() {
        val path = "\\"

        `when`(documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, "document", "doc"))
            .thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.getDocument(path, "document", "doc")
        }.isInstanceOf(DocumentNotFoundException::class.java)
            .hasMessage("The document \\document.doc cannot be found")

        verify(documentRepository, times(1)).findDocumentByPathAndNameAndTypeAndDeletedIsFalse(
            path,
            "document",
            "doc"
        )
        verifyNoMoreInteractions(documentRepository)
        verifyNoInteractions(documentContentRepository, labelRepository)
    }

    @Test
    fun `retrieve Document Content based on path, name and type`() {
        val path = "\\"
        val name = "updatableDocument"
        val type = "doc"

        val document = getDocument(path, "document", "doc")
        val content = getDocumentContent(document.id, "Content")

        `when`(documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type))
            .thenReturn(Optional.of(document))
        `when`(documentContentRepository.findById(document.id)).thenReturn(Optional.of(content))

        val contentDto = fixture.getDocumentContent(path, name, type)

        assertThat(contentDto).`as`("contentDto has class DocumentContentDto")
            .isInstanceOf(DocumentContentDto::class.java)
        assertThat(contentDto.documentId).`as`("Document Id is equal to document \\").isEqualTo(document.id.unbox())
        assertThat(String(contentDto.content)).`as`("Document content is 'Content'").isEqualTo("Content")

        verify(documentRepository, times(1)).findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type)
        verify(documentContentRepository, times(1)).findById(document.id)

        verifyNoMoreInteractions(documentRepository, documentContentRepository)
        verifyNoInteractions(labelRepository)
    }

    @Test
    fun `retrieve Document Content based on path, name and type while not existing`() {
        val path = "\\"
        val name = "updatableDocument"
        val type = "doc"

        val document = getDocument(path, "document", "doc")
        val content = getDocumentContent(document.id, "Content")

        `when`(documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type))
            .thenReturn(Optional.empty())

        assertThatThrownBy {
            fixture.getDocumentContent(path, name, type)
        }.isInstanceOf(DocumentNotFoundException::class.java)
            .hasMessage("The document $path$name.$type cannot be found")

        verify(documentRepository, times(1)).findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type)

        verifyNoMoreInteractions(documentRepository)
        verifyNoInteractions(documentContentRepository, labelRepository)
    }

    @Test
    fun `retrieve Document Content based on DocumentDto`() {
        val path = "\\"
        val name = "updatableDocument"
        val type = "doc"

        val document = getDocument(path, name, type)
        val content = getDocumentContent(document.id, "Content")
        val documentDto = documentMapper.convertToDto(document)

        `when`(documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type))
            .thenReturn(Optional.of(document))
        `when`(documentContentRepository.findById(document.id)).thenReturn(Optional.of(content))

        val contentDto = fixture.getDocumentContent(documentDto)

        assertThat(contentDto).`as`("contentDto has class DocumentContentDto")
            .isInstanceOf(DocumentContentDto::class.java)
        assertThat(contentDto.documentId).`as`("Document Id is equal to document \\").isEqualTo(document.id.unbox())
        assertThat(String(contentDto.content)).`as`("Document content is 'Content'").isEqualTo("Content")

        verify(documentRepository, times(1)).findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type)
        verify(documentContentRepository, times(1)).findById(document.id)

        verifyNoMoreInteractions(documentRepository, documentContentRepository)
        verifyNoInteractions(labelRepository)
    }

    @Test
    fun `retrieve Document Content based on Id`() {
        val documentId = DocumentId(UUID.randomUUID())
        val content = getDocumentContent(documentId, "Content")

        `when`(documentContentRepository.findById(documentId)).thenReturn(Optional.of(content))

        val contentDto = fixture.getDocumentContent(documentId.unbox())

        assertThat(contentDto).`as`("contentDto has class DocumentContentDto")
            .isInstanceOf(DocumentContentDto::class.java)
        assertThat(contentDto.documentId).`as`("Document Id is equal to document \\").isEqualTo(documentId.unbox())
        assertThat(String(contentDto.content)).`as`("Document content is 'Content'").isEqualTo("Content")

        verify(documentContentRepository, times(1)).findById(documentId)

        verifyNoMoreInteractions(documentContentRepository)
        verifyNoInteractions(documentRepository, labelRepository)
    }

    @Test
    fun `retrieve Document Content based on Not existing Id`() {
        val documentId = DocumentId(UUID.randomUUID())

        `when`(documentContentRepository.findById(documentId)).thenReturn(Optional.empty())

        assertThatThrownBy {

            fixture.getDocumentContent(documentId.unbox())

        }.isInstanceOf(DocumentNotFoundException::class.java)
            .hasMessage("The Document Content for ${documentId.unbox()} cannot be found")

        verify(documentContentRepository, times(1)).findById(documentId)

        verifyNoMoreInteractions(documentContentRepository)
        verifyNoInteractions(documentRepository, labelRepository)
    }

    fun getDocument(path: String, name: String) = getDocument(path, name, "testD")


    fun getDocument(path: String, name: String, type: String) = Document(
        id = DocumentId(UUID.randomUUID()),
        path = path,
        name = name,
        type = type,
        size = 64,
        version = 1,
        attributes = setOf(
            DocumentAttribute("code", UUID.randomUUID().toString()),
            DocumentAttribute("timestamp", LocalDateTime.now().toString())
        )
    )

    fun getDocumentDto(path: String, name: String, type: String) = DocumentDto(
        documentId = UUID.randomUUID(),
        path = path,
        name = name,
        type = type,
        size = 64,
        version = 1,
        attributes = mapOf("code" to UUID.randomUUID().toString(), "timestamp" to LocalDateTime.now().toString())
    )

    fun getDocumentContent(id: DocumentId, content: String) = DocumentContent(id, content.toByteArray())
}