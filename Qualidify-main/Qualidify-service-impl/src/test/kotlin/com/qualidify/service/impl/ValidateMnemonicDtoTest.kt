package com.qualidify.service.impl

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.metadata.annotations.Mnemonic
import com.qualidify.metadata.service.dto.MetadataObjectDto
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.type.ClassMetadata
import org.springframework.core.type.filter.AbstractClassTestingTypeFilter
import org.springframework.core.type.filter.AnnotationTypeFilter
import java.util.*


class ValidateMnemonicDtoTest {

    companion object {
        private const val PACKAGE = "com.qualidify"
    }

    val logger by LoggerDelegation()

    internal class ExclusionListFilter(private val exclusionList: Collection<String?>) :
        AbstractClassTestingTypeFilter() {
        constructor(vararg classes: String?) : this(Arrays.asList<String?>(*classes)) {}

        override fun match(classMetadata: ClassMetadata): Boolean {
            return exclusionList.contains(classMetadata.className)
        }
    }

    @Test
    @DisplayName("Check if all Dto-mnemonic classes do have a mapper")
    fun doesMapperExistForMnemonicDto() {
        val softAssertions = SoftAssertions()
        val provider = ClassPathScanningCandidateComponentProvider(false)
        provider.addIncludeFilter(AnnotationTypeFilter(Mnemonic::class.java))
        provider.addExcludeFilter(
            ExclusionListFilter(
                MetadataObjectDto::class.java.canonicalName,
            )
        )
        val dtos = provider.findCandidateComponents(PACKAGE)


        val dtosUsedInMappers = allDtoMappers.asSequence()
            .map { bd: BeanDefinition -> getClassFromBeanDefinition(bd) }
            .map { getDtoFromMapper(it) to it }
            .toMap()


        softAssertions.assertThat(dtos).`as`("No Dto classes found in classpath.").isNotEmpty
        softAssertions.assertThat(dtosUsedInMappers).`as`("No Dto Mappers found in classpath.").isNotEmpty

        logger.info("***" + dtosUsedInMappers.toString())

        dtos.stream()
            .map { bd: BeanDefinition -> getClassFromBeanDefinition(bd) }
            .filter { c -> !dtosUsedInMappers.containsKey(c) }
            .forEach { c -> softAssertions.fail("Class %s doesn't have a mapper", c) }

        softAssertions.assertAll()
    }

    private fun getClassFromBeanDefinition(bd: BeanDefinition): Class<*> {
        return try {
            Class.forName(bd.beanClassName)
        } catch (e: ClassNotFoundException) {
            throw RuntimeException("No class found with name " + bd.beanClassName, e)
        }
    }

    private val allDtoMappers: Set<BeanDefinition>
        get() {
            val provider = ClassPathScanningCandidateComponentProvider(false)
            provider.addIncludeFilter(AnnotationTypeFilter(DtoMapper::class.java))
            return provider.findCandidateComponents(PACKAGE)
        }

    private fun getDtoFromMapper(mapper: Class<*>): Class<*> {
        val dtoType = mapper.methods.asSequence()
            .filter { it.name == "convertToModel" }
            .filter {
                it.parameterTypes[0].canonicalName != java.lang.Object::class.qualifiedName
                        && it.parameterTypes[0].canonicalName != HasIdentity::class.qualifiedName
            }
            .map { it.parameterTypes[0] as Class<*> }
            .firstOrNull()

        return dtoType ?: throw RuntimeException("No Dto found on mapper $mapper")
    }
}