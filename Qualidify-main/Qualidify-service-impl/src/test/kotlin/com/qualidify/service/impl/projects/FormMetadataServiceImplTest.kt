package com.qualidify.service.impl.projects

import com.qualidify.metadata.DataType
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.DtoFactory
import com.qualidify.service.process.exceptions.FormNotFoundException
import com.qualidify.service.projects.TaskService
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.flowable.form.api.FormInfo
import org.flowable.form.api.FormModel
import org.flowable.form.api.FormRepositoryService
import org.flowable.form.model.FormField
import org.flowable.form.model.SimpleFormModel
import org.flowable.variable.api.persistence.entity.VariableInstance
import org.flowable.variable.service.impl.persistence.entity.TransientVariableInstance
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension

/**
 * Unit test for the FormMetadataServiceImpl
 *
 * @author Marcel Pot
 * @since 1.0
 */
@ExtendWith(MockitoExtension::class)
internal class FormMetadataServiceImplTest {
    @Mock
    lateinit var formRepositoryService: FormRepositoryService

    @Mock
    lateinit var cmmnTaskService: CmmnTaskService

    @Mock
    lateinit var taskService: TaskService

    @InjectMocks
    lateinit var fixture: FormMetadataServiceImpl

    @Test
    fun `load FormMetadata by key`() {
        val key = "Form_1"
        val formInfoMock = getFormInfoMock()

        `when`(formInfoMock.formModel).thenReturn(getSimpleFormModel())
        `when`(formRepositoryService.getFormModelByKey(key)).thenReturn(formInfoMock)

        val columnMetadata = fixture.loadFormModelMetadataByFormKey(key)

        assertThat(columnMetadata).`as`("Expected three columnMetadata objects")
            .containsAnyOf(
                ColumnDef("id1", "first", DataType.STRING),
                ColumnDef("id2", "second", DataType.INTEGER)
            )

        verify(formRepositoryService, times(1)).getFormModelByKey(key)
        verify(formInfoMock, times(1)).formModel

        verifyNoMoreInteractions(formRepositoryService)
        verifyNoMoreInteractions(formInfoMock)
        verifyNoInteractions(cmmnTaskService)
    }

    @Test
    fun `try to load non-existing FormMetadata by key`() {
        val key = "Unknown Form"
        `when`(formRepositoryService.getFormModelByKey(key)).thenThrow(FlowableObjectNotFoundException("$key not found!"))

        assertThatThrownBy {
            fixture.loadFormModelMetadataByFormKey(key)
        }.isInstanceOf(FormNotFoundException::class.java)
            .hasMessage("Cannot find form with key $key")
            .hasCauseInstanceOf(FlowableObjectNotFoundException::class.java)

        verify(formRepositoryService, times(1)).getFormModelByKey(key)
        verifyNoInteractions(cmmnTaskService)
    }

    @Test
    fun `load FormMetadata by key when Form is not SimpleFormModel`() {
        val key = "Form_1"
        val formInfoMock = getFormInfoMock()
        `when`(formInfoMock.formModel).thenReturn(NotSimpleForm())
        `when`(formRepositoryService.getFormModelByKey(key)).thenReturn(formInfoMock)

        val columnMetadata = fixture.loadFormModelMetadataByFormKey(key)

        assertThat(columnMetadata)
            .`as`("Expected empty list of columnMetadata because converting is not possible ")
            .isEmpty()

        verify(formRepositoryService, times(1)).getFormModelByKey(key)
        verify(formInfoMock, times(1)).formModel

        verifyNoMoreInteractions(formRepositoryService)
        verifyNoMoreInteractions(formInfoMock)
        verifyNoInteractions(cmmnTaskService)
    }

    @Test
    fun `save data to Form with task`() {
        val taskId = "Task_1"
        val data = mapOf<String, Any>("First" to "Alphabet", "Second" to 8)

        fixture.saveTaskFormData(taskId, data)

        verify(cmmnTaskService, times(1)).getVariableInstancesLocal(taskId)
        verify(cmmnTaskService, times(1)).setVariablesLocal(taskId, data)
        verifyNoInteractions(formRepositoryService)
    }

    @Test
    fun `complete task with Form data`() {
        val taskId = "Task_1"
        val taskDto = DtoFactory.taskDto(id = "Task_1")
        val data = mapOf<String, Any>("First" to "Alphabet", "Second" to 8)
        `when`(taskService.getTaskDtoById("Task_1")).thenReturn(taskDto)

        fixture.completeTaskFormData(taskId, data)

        verify(cmmnTaskService, times(1)).getVariableInstancesLocal(taskId)
        verify(cmmnTaskService, times(1)).setVariablesLocal(taskId, data)
        verify(taskService, times(1)).completeOrResolveTask(taskDto, null)
        verifyNoInteractions(formRepositoryService)
    }


    @Test
    fun `get data from Form by taskId`() {
        val taskId = "Task_1"
        val variableInstanceData = mapOf<String, VariableInstance>(
            "First" to TransientVariableInstance("First", "Alphabet"),
            "Second" to TransientVariableInstance("Second", 8)
        )

        `when`(cmmnTaskService.getVariableInstancesLocal(taskId)).thenReturn(variableInstanceData)

        val variables = fixture.getFormVariables(taskId)

        assertThat(variables).hasSize(2)
            .containsEntry("First", "Alphabet")
            .containsEntry("Second", 8)

        verify(cmmnTaskService, times(1)).getVariableInstancesLocal(taskId)
        verifyNoMoreInteractions(cmmnTaskService)
        verifyNoInteractions(formRepositoryService)
    }


    private fun getFormInfoMock() = mock(FormInfo::class.java)

    private fun getSimpleFormModel(): SimpleFormModel {
        val formModel = SimpleFormModel()
        formModel.fields = listOf(
            getFormField("id1", "first", "text"),
            getFormField("id2", "second", "integer")
        )
        return formModel
    }

    private fun getFormField(id: String, name: String, type: String): FormField {
        val field = FormField()
        field.id = id
        field.name = name
        field.type = type
        return field
    }

    class NotSimpleForm : FormModel
}