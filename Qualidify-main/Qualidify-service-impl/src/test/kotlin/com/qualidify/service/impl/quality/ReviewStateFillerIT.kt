package com.qualidify.service.impl.quality

import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.projects.ProjectInstanceRepository
import com.qualidify.service.impl.projects.ProjectInstanceStarter
import com.qualidify.service.impl.user.UserDataRepository
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import kotlin.test.assertNotNull

@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Sql("/sql/ProjectTestData.sql")
@Transactional
internal open class ReviewStateFillerIT(
    private val cmmnRuntimeService: CmmnRuntimeService,
    @Autowired private val projectInstanceRepository: ProjectInstanceRepository,
    @Autowired private val userDataRepository: UserDataRepository,
    @Autowired private val reviewStateFiller: ReviewStateFiller,
) {

    private val projectInstanceStarter =
        ProjectInstanceStarter(cmmnRuntimeService, projectInstanceRepository, userDataRepository, reviewStateFiller)
    private val projectTemplateDto = DtoFactory.projectTemplateDto(cmmnModel = "employeeOnboarding")

    @Test
    @CmmnDeployment(
        resources = [
            "dev-resources/cases/employeeOnboarding.cmmn.xml",
            "dev-resources/cases/cmmnQualityShell.cmmn.xml",
        ]
    )
    fun `Test filling of review states upon project start`() {
        projectInstanceStarter.start(projectTemplateDto)

        val planItems = cmmnRuntimeService.createPlanItemInstanceQuery().list()

        planItems.forEach { planItem ->
            val reviewState = cmmnRuntimeService.getLocalVariable(planItem.id, "reviewState")
            assertNotNull(reviewState)
        }

    }

}