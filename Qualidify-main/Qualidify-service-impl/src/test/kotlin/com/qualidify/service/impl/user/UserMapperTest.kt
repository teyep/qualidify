package com.qualidify.service.impl.user

import com.qualidify.model.entities.User
import com.qualidify.service.user.dto.UserDto
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import java.util.*

class UserMapperTest {
    val userId: UUID = UUID.randomUUID()
    val mapper: UserMapper = UserMapper()

    @Test
    fun convertToDtoTest() {
        val validator = getModel()

        val dto = mapper.convertToDto(validator)

        Assertions.assertThat(dto).`as`("Dto should have the same data as the entity").isEqualTo(
            getDto()
        )
    }

    @Test
    fun convertToModelTest() {
        val dto = getDto()
        val model = mapper.convertToModel(dto)

        Assertions.assertThat(model).`as`("Entity should have the same data as the dto").isEqualTo(
            getModel()
        )
    }

    private fun getModel(): User {
        return User.builder().withUserId(userId)
            .withUsername("Mickey Mouse")
            .withPassword("I3Minney")
            .withEmail("Mickey@disney.com")
            .withField("BIRTHYEAR", "1928")
            .build()
    }

    private fun getDto() =
        UserDto(
            userId,
            "Mickey Mouse",
            "Mickey@disney.com",
            mutableMapOf("BIRTHYEAR" to "1928")
        )
}