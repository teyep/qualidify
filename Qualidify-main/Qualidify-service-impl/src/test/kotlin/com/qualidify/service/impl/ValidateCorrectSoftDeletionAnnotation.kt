package com.qualidify.service.impl

import com.qualidify.core.jpa.annotations.SoftDelete
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.type.filter.AnnotationTypeFilter
import org.springframework.data.repository.support.Repositories
import org.springframework.test.context.junit.jupiter.SpringExtension
import javax.persistence.Entity

@ExtendWith(SpringExtension::class)
@SpringBootTest
class ValidateCorrectSoftDeletionAnnotation(
    applicationContext: ApplicationContext
) {

    companion object {
        private const val PACKAGE = "com.qualidify"
    }

    val repositories = Repositories(applicationContext)

    @Test
    fun testCorrectUsageOfSoftDeletionAnnotationOnRepositories() {

        val entitiesToCheck = getEntities().asSequence()
            .map { Class.forName(it.beanClassName) }
            .filter { hasSoftDeleteAnnotationOnRepository(it) }

        val softAssertions = SoftAssertions()

        for (entityClass in entitiesToCheck) {
            val softDeletionAnnotation = getSoftDeleteAnnotationOnInterface(entityClass)
            val column = softDeletionAnnotation?.column!!

            val foundField = entityClass.kotlin.members.asSequence()
                .any { it.name.equals(column, true) }

            softAssertions.assertThat(foundField)
                .`as`(
                    "Cannot find the annotated field 'SoftDelete.column=$column' " +
                            "on entity ${entityClass.simpleName} ($entityClass)."
                ).isTrue

        }
        softAssertions.assertAll()
    }

    @Test
    fun testCorrectUsageOfSoftDeletionAnnotationOnEntities() {

        val entitiesToCheck = getEntities().asSequence()
            .map { Class.forName(it.beanClassName) }
            .filter { it.isAnnotationPresent(SoftDelete::class.java) }
            .toList()

        val softAssertions = SoftAssertions()

        for (entityClass in entitiesToCheck) {
            val softDeletionAnnotation = entityClass.getAnnotation(SoftDelete::class.java)

            val column = softDeletionAnnotation?.column!!

            val foundField = entityClass.kotlin.members.asSequence()
                .any { it.name.equals(column, true) }

            softAssertions.assertThat(foundField)
                .`as`(
                    "Cannot find the annotated field 'SoftDelete.column=$column' " +
                            "on entity ${entityClass.simpleName} ($entityClass)."
                ).isTrue

        }
        softAssertions.assertAll()
    }


    private fun hasSoftDeleteAnnotationOnRepository(clazz: Class<*>): Boolean {
        if (repositories.hasRepositoryFor(clazz)) {
            return repositories.getRepositoryInformationFor(clazz)
                .map { it.repositoryInterface }
                .map { it.isAnnotationPresent(SoftDelete::class.java) }
                .orElse(false)
        }
        return false
    }

    private fun getSoftDeleteAnnotationOnInterface(clazz: Class<*>): SoftDelete? {
        return repositories.getRepositoryInformationFor(clazz)
            .map { it.repositoryInterface }
            .map { it.getDeclaredAnnotation(SoftDelete::class.java) }
            .orElseGet { null }
    }

    private fun getEntities(): Set<BeanDefinition> {
        val provider = ClassPathScanningCandidateComponentProvider(false)
        provider.addIncludeFilter(AnnotationTypeFilter(Entity::class.java))
        return provider.findCandidateComponents(PACKAGE)
    }

}