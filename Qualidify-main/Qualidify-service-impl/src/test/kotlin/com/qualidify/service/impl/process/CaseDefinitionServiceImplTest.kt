package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.mappers.CaseDefinitionDtoMapper
import com.qualidify.service.impl.process.mappers.CaseInstanceDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.qualidify.service.projects.ProjectInstanceService
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.engine.CmmnEngine
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertEquals

/**
 * A unit test for the [CaseDefinitionServiceImpl]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class CaseDefinitionServiceImplTest {

    private val engine = mockk<CmmnEngine>()
    private val caseDefinitionRepository = mockk<CmmnCaseDefinitionRepository>()
    private val caseInstanceRepository = mockk<CmmnCaseInstanceRepository>()
    private val caseDefinitionDtoMapper = mockk<CaseDefinitionDtoMapper>()
    private val projectInstanceService = mockk<ProjectInstanceService>()

    private val caseInstanceDtoMapper = spyk(CaseInstanceDtoMapper(caseDefinitionRepository))

    private val fixture = CaseDefinitionServiceImpl(
        engine,
        caseDefinitionRepository,
        caseDefinitionDtoMapper,
        caseInstanceRepository,
        caseInstanceDtoMapper
    )

    private val entity = TestEntities.cmmnCaseDefinitionJpa()

    @AfterEach
    fun checkTest() {
        confirmVerified(
            engine,
            caseDefinitionRepository,
            caseInstanceRepository,
            caseDefinitionDtoMapper,
            projectInstanceService
        )
        checkUnnecessaryStub(
            engine,
            caseDefinitionRepository,
            caseInstanceRepository,
            caseDefinitionDtoMapper,
            projectInstanceService
        )
    }

    @Test
    fun `Test countByQuery`() {
        mockkStatic(CmmnCaseDefinitionRepository::countResponse)

        every {
            caseDefinitionRepository.countResponse(CountQuery<CaseDefinitionDto>())
        } returns CountResponse(3)

        val result = fixture.countByQuery(CountQuery<CaseDefinitionDto>())

        assertEquals(3, result.count)

        verify { caseDefinitionDtoMapper wasNot called }
        verify { caseInstanceDtoMapper wasNot called }
        verify(exactly = 1) { caseDefinitionRepository.countResponse(CountQuery<CaseDefinitionDto>()) }
    }

    @Test
    fun `Test findByQuery is empty`() {
        mockkStatic(CmmnCaseDefinitionRepository::findAllByQuery)

        every {
            caseDefinitionRepository.findAllByQuery(SearchQuery<CaseDefinitionDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<CaseDefinitionDto>())

        assertEquals(emptyList(), result)

        verify { caseDefinitionDtoMapper wasNot called }
        verify { caseInstanceDtoMapper wasNot called }
        verify(exactly = 1) { caseDefinitionRepository.findAllByQuery(SearchQuery<CaseDefinitionDto>()) }

    }

    @ParameterizedTest
    @MethodSource("getDtoProperties")
    fun `Test query constraints mapping`(property: String) {
        mockkStatic(CmmnCaseDefinitionRepository::findAllByQuery)

        every {
            caseDefinitionDtoMapper.convertToDto(any())
        } returns DtoFactory.caseDefinitionDto()

        val searchQuery = SearchQuery<CaseDefinitionDto>(constraints = mapOf(property to "randomValue"))
        every {
            caseDefinitionRepository.findAllByQuery(searchQuery)
        } returns PageImpl(mutableListOf(entity))

        val results = fixture.findByQuery(searchQuery)

        assertThat(results.size).isOne

        verify(exactly = 1) { caseDefinitionRepository.findAllByQuery(searchQuery) }
        verify(exactly = 1) { caseDefinitionDtoMapper.convertToDto(any()) }

    }

    companion object {

        @JvmStatic
        private fun getDtoProperties(): Set<String> {
            return CaseDefinitionDto::class.declaredMemberProperties
                .map { it.name }
                .toSet()
        }


    }

}