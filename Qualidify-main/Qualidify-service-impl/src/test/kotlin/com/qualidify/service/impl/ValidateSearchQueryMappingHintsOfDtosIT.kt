package com.qualidify.service.impl

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.SearchCriterium
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.core.query.service.Queryable
import com.qualidify.metadata.service.impl.harvest.AnnotationHarvester
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.fail
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.reflect.KClass
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.jvm.jvmErasure


/**
 * Test if all dto's are mapped correctly onto the database.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest
@DirtiesContext
class ValidateSearchQueryHintsOfDtosIT(
    private val applicationContext: ApplicationContext,
) {
    private val logger by LoggerDelegation()
    private val excludingServices = setOf("")
    private val excludingDtos = setOf(
        Class.forName("com.qualidify.service.configuration.dto.ConfigurationDomainDto"),
        Class.forName("com.qualidify.service.document.dto.DocumentContentDto"),
        Class.forName("com.qualidify.service.document.dto.LabelDto"),
        Class.forName("com.qualidify.service.process.dto.CaseDefinitionUploadDto"),
        Class.forName("com.qualidify.service.projects.dto.ProjectInstanceDto\$ProjectInstanceVariableDto"),
        Class.forName("com.qualidify.service.projects.dto.ProjectTemplateDto\$ProjectTemplateVariableDto"),
        Class.forName("com.qualidify.service.report.dto.BatchJobArtifactDto"),
        Class.forName("com.qualidify.service.report.dto.ReportTemplateResourceDto"),
        Class.forName("com.qualidify.metadata.service.dto.MetadataObjectDto\$MetadataColumnDto"),
        Class.forName("com.qualidify.metadata.service.dto.MetadataObjectDto"),
        Class.forName("com.qualidify.metadata.dto.CustomFieldDto"),
        Class.forName("com.qualidify.service.security.dto.LoginAttemptDto")
    ).map { it.kotlin }


    private fun determineDtoToFindByQueryMapping(
        name: String,
        service: Queryable<*>
    ): Pair<KClass<*>, (SearchQuery) -> Collection<Any>>? {
        try {
            //remove CGLib Spring class part
            val serviceClassName = service::class.qualifiedName!!.substringBefore("$$")
            val serviceClass = Class.forName(serviceClassName).kotlin

            val dtoClass = if (AbstractQueryableService::class.java.isAssignableFrom(service::class.java)) {
                serviceClass.declaredMemberFunctions
                    .firstOrNull { it.name == "mapEntityToDto" }
                    ?.returnType
                    ?.jvmErasure
            } else {
                serviceClass.declaredMemberFunctions
                    .first { it.name == "findByQuery" }
                    .returnType.arguments[0]
                    .type
                    ?.jvmErasure
            }
            val lambda = { searchQuery: SearchQuery -> service.findByQuery(searchQuery) }
            return Pair(dtoClass!!, lambda)
        } catch (e: Exception) {
            logger.warn("No method findByQuery found for implementation Queryable : $name.")
        }
        return null
    }

    @Test
    fun `test all FindByQuery Methods on Queryable Service with Specification Mapping`() {
        val dtos = DtoValidationUtil.getBeanDefinitionsOfDtos()
        val annotationHarvester = AnnotationHarvester()

        val services =
            applicationContext.getBeansOfType(Queryable::class.java).filter { !excludingServices.contains(it.key) }
        val classToServiceMap = services.entries.asSequence()
            .map { determineDtoToFindByQueryMapping(it.key, it.value) }
            .filterNotNull()
            .toMap()

        for (beanDefinition in dtos) {
            logger.info("Validating ${beanDefinition.beanClassName}")
            val dtoClass = DtoValidationUtil.getClassFromBeanDefinition(beanDefinition)
            val metadata = annotationHarvester.harvest(dtoClass)
            val columns = metadata.columns
                .filter { !!it.hidden && it.viewable }
                .filter { it.searchable }

            if (classToServiceMap.containsKey(dtoClass)) {
                for (column in columns) {

                    try {
                        val searchQuery = SearchQuery(
                            beanType = dtoClass,
                            searchCriteria = listOf(SearchCriterium(column.name to "RandomValue"))
                        )

                        val lambda = classToServiceMap[dtoClass]!!
                        lambda.invoke(searchQuery)
                    } catch (e: Exception) {
                        fail(
                            "Add a correct @SearchQueryHint on dto ${dtoClass} field ${column.name}",
                            e
                        )
                    }
                }
            } else {
                if (!excludingDtos.contains(dtoClass)) {
                    fail("Cannot find lambda for FindByQuery for dto $dtoClass")
                }
            }

        }

    }


}