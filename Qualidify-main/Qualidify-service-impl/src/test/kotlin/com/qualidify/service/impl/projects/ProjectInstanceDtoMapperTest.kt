package com.qualidify.service.impl.projects

import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.mappers.CaseInstanceDtoMapper
import com.qualidify.service.impl.process.mappers.ProjectInstanceDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.impl.process.repositories.CmmnHistoricCaseInstanceRepository
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.exceptions.CaseInstanceNotFoundException
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class ProjectInstanceDtoMapperTest {

    private val cmmnCaseInstanceRepository = mockk<CmmnCaseInstanceRepository>()
    private val cmmnHistoricCaseInstanceRepository = mockk<CmmnHistoricCaseInstanceRepository>()
    private val caseInstanceService = mockk<CaseInstanceService>()
    private val cmmnCaseDefinitionRepository = mockk<CmmnCaseDefinitionRepository>()
    private val projectInstanceRepository = mockk<ProjectInstanceRepository>()

    private val caseInstanceDtoMapper =
        spyk(CaseInstanceDtoMapper(cmmnCaseDefinitionRepository))

    private val fixture = ProjectInstanceDtoMapper(
        cmmnCaseInstanceRepository,
        caseInstanceDtoMapper,
        cmmnHistoricCaseInstanceRepository,
    )

    private val projectInstanceId = UUID.randomUUID()
    private val qualityShellInstanceId = "cmmnQualityShellId"
    private val regularCaseInstanceId = "regularCaseInstanceId"
    private val projectInstance =
        TestEntities.projectInstance(id = projectInstanceId, cmmnInstanceId = regularCaseInstanceId)
    private val qualityShellInstanceJpa =
        TestEntities.cmmnCaseInstanceJpa(id = qualityShellInstanceId, caseDefinitionId = "qualityDefinitionId")
    private val regularCaseInstanceJpa =
        TestEntities.cmmnCaseInstanceJpa(id = regularCaseInstanceId, caseDefinitionId = "definitionId")
    private val historicQualityShellInstanceJpa =
        TestEntities.cmmnHistoricCaseInstanceJpa(id = qualityShellInstanceId, caseDefinitionId = "qualityDefinitionId")
    private val historicRegularCaseInstanceJpa =
        TestEntities.cmmnHistoricCaseInstanceJpa(id = regularCaseInstanceId, caseDefinitionId = "definitionId")
    private val qualityShellInstanceDto = DtoFactory.caseInstanceDto(
        id = qualityShellInstanceId,
        projectInstanceId = projectInstanceId.toString(),
        isQualityShell = true,
    )
    private val regularCaseInstanceDto =
        DtoFactory.caseInstanceDto(id = regularCaseInstanceId, projectInstanceId = projectInstanceId.toString())

    //TODO(): QUAL-470
    private val historicRegularCaseInstanceDto =
        DtoFactory.caseInstanceDto(id = regularCaseInstanceId, projectInstanceId = projectInstanceId.toString())
    private val historicQualityCaseInstanceDto =
        DtoFactory.caseInstanceDto(
            id = qualityShellInstanceId,
            projectInstanceId = projectInstanceId.toString(),
            isQualityShell = true,
        )

    @AfterEach
    fun checkTest() {
        checkUnnecessaryStub(
            cmmnCaseDefinitionRepository,
            cmmnCaseInstanceRepository,
            cmmnHistoricCaseInstanceRepository,
            caseInstanceDtoMapper,
            projectInstanceRepository,
            caseInstanceService
        )
        confirmVerified(
            cmmnCaseDefinitionRepository,
            cmmnCaseInstanceRepository,
            cmmnHistoricCaseInstanceRepository,
            caseInstanceDtoMapper,
            projectInstanceRepository,
            caseInstanceService
        )
    }

    @Test
    fun `Convert projectInstanceEntity to dto`() {

        every {
            cmmnCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.of(regularCaseInstanceJpa)

        every {
            caseInstanceDtoMapper.convertToDto(regularCaseInstanceJpa)
        } returns regularCaseInstanceDto

        every {
            cmmnCaseInstanceRepository.findById(qualityShellInstanceId)
        } returns Optional.of(qualityShellInstanceJpa)

        every {
            caseInstanceDtoMapper.convertToDto(qualityShellInstanceJpa)
        } returns qualityShellInstanceDto

        every {
            cmmnHistoricCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.of(historicRegularCaseInstanceJpa)

        every {
            cmmnHistoricCaseInstanceRepository.findById(qualityShellInstanceId)
        } returns Optional.of(historicQualityShellInstanceJpa)

        val result = fixture.convertToDto(projectInstance)

        assertEquals(
            regularCaseInstanceId, result.regularCaseId,
            "The regular case instance id should be equal to '$regularCaseInstanceId'"
        )
        assertEquals(
            qualityShellInstanceId, result.qualityShellId,
            "The quality shell case instance id should be equal to '$qualityShellInstanceId'"
        )
        assertEquals(
            "caseDefinitionKey", result.regularCaseInstanceDto.caseDefinitionKey,
            "The caseDefinitionKey of the regular case instance should be 'caseDefinitionKey'"
        )
        assertEquals(
            "cmmnQualityShell", result.qualityShellInstanceDto.caseDefinitionKey,
            "The caseDefinitionKey of the quality shell instance should be 'cmmnQualityShell'"
        )
        assertThat(result.qualityShellInstanceDto).isEqualTo(qualityShellInstanceDto)
        assertThat(result.regularCaseInstanceDto).isEqualTo(regularCaseInstanceDto)

        verify(exactly = 1) { caseInstanceDtoMapper.convertToDto(regularCaseInstanceJpa) }
        verify(exactly = 1) { caseInstanceDtoMapper.convertToDto(qualityShellInstanceJpa) }
        verify(exactly = 2) { cmmnCaseInstanceRepository.findById(any()) }
        verify(exactly = 2) { cmmnHistoricCaseInstanceRepository.findById(any()) }

    }

    @Test
    fun `Convert projectInstanceEntity to dto with empty CmmnCaseInstanceJpa`() {

        every {
            cmmnHistoricCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.of(historicRegularCaseInstanceJpa)

        every {
            caseInstanceDtoMapper.convertToDto(historicRegularCaseInstanceJpa)
        } returns historicRegularCaseInstanceDto

        every {
            caseInstanceDtoMapper.convertToDto(qualityShellInstanceJpa)
        } returns qualityShellInstanceDto

        every {
            cmmnCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.empty()

        every {
            cmmnCaseInstanceRepository.findById(qualityShellInstanceId)
        } returns Optional.of(qualityShellInstanceJpa)

        every {
            cmmnHistoricCaseInstanceRepository.findById(qualityShellInstanceId)
        } returns Optional.of(historicQualityShellInstanceJpa)

        val result = fixture.convertToDto(projectInstance)

        assertEquals(
            regularCaseInstanceId, result.regularCaseId,
            "The regular case instance id should be equal to '$regularCaseInstanceId'"
        )
        assertEquals(
            qualityShellInstanceId, result.qualityShellId,
            "The quality shell case instance id should be equal to '$qualityShellInstanceId'"
        )
        assertEquals(
            "caseDefinitionKey", result.regularCaseInstanceDto.caseDefinitionKey,
            "The caseDefinitionKey of the regular case instance should be 'caseDefinitionKey'"
        )
        assertEquals(
            "cmmnQualityShell", result.qualityShellInstanceDto.caseDefinitionKey,
            "The caseDefinitionKey of the quality shell instance should be 'cmmnQualityShell'"
        )
        // assertThat(result.cmmnQualityShellInstanceDto).isEqualTo(historicQualityShellInstanceDto) TODO(QUAL-470)
        // assertThat(result.cmmnInstanceDto).isEqualTo(historicRegularCaseInstanceDto) TODO(QUAL-470)

        verify(exactly = 1) { caseInstanceDtoMapper.convertToDto(qualityShellInstanceJpa) }
        verify(exactly = 1) { caseInstanceDtoMapper.convertToDto(historicRegularCaseInstanceJpa) }
        verify(exactly = 2) { cmmnCaseInstanceRepository.findById(any()) }
        verify(exactly = 2) { cmmnHistoricCaseInstanceRepository.findById(any()) }

    }

    @Test
    fun `Convert projectInstanceEntity to dto with empty CmmnQualityShellInstanceJpa`() {

        every {
            caseInstanceDtoMapper.convertToDto(historicQualityShellInstanceJpa)
        } returns historicQualityCaseInstanceDto

        every {
            cmmnHistoricCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.of(historicRegularCaseInstanceJpa)

        every {
            cmmnHistoricCaseInstanceRepository.findById(qualityShellInstanceId)
        } returns Optional.of(historicQualityShellInstanceJpa)

        every {
            cmmnCaseDefinitionRepository.findById("definitionId")
        } returns Optional.of(TestEntities.cmmnCaseDefinitionJpa())

        every {
            cmmnCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.of(regularCaseInstanceJpa)

        every {
            cmmnCaseInstanceRepository.findById(qualityShellInstanceId)
        } returns Optional.empty()

        val result = fixture.convertToDto(projectInstance)

        assertEquals(
            regularCaseInstanceId, result.regularCaseId,
            "The regular case instance id should be equal to '$regularCaseInstanceId'"
        )
        assertEquals(
            qualityShellInstanceId, result.qualityShellId,
            "The regular case instance id should be equal to '$qualityShellInstanceId'"
        )
        assertEquals(
            "caseDefinitionKey", result.regularCaseInstanceDto.caseDefinitionKey,
            "The caseDefinitionKey of the regular case instance should be 'caseDefinitionKey'"
        )
        assertEquals(
            "cmmnQualityShell", result.qualityShellInstanceDto.caseDefinitionKey,
            "The caseDefinitionKey of the quality shell instance should be 'cmmnQualityShell'"
        )
        // assertThat(result.cmmnQualityShellInstanceDto).isEqualTo(historicQualityShellInstanceDto) TODO(): QUAL-470
        // assertThat(result.cmmnInstanceDto).isEqualTo(historicRegularCaseInstanceDto) TODO(): QUAL-470

        verify(exactly = 1) { caseInstanceDtoMapper.convertToDto(regularCaseInstanceJpa) }
        verify(exactly = 1) { caseInstanceDtoMapper.convertToDto(historicQualityShellInstanceJpa) }
        verify(exactly = 2) { cmmnCaseInstanceRepository.findById(any()) }
        verify(exactly = 1) { cmmnCaseDefinitionRepository.findById(any()) }
        verify(exactly = 2) { cmmnHistoricCaseInstanceRepository.findById(any()) }

    }

    @Test
    fun `Convert projectInstanceEntity to dto with empty historic CmmnQualityShellInstanceJpa`() {

        every {
            caseInstanceDtoMapper.convertToDto(regularCaseInstanceJpa)
        } returns regularCaseInstanceDto

        every {
            cmmnCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.of(regularCaseInstanceJpa)

        every {
            cmmnHistoricCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.of(historicRegularCaseInstanceJpa)

        every {
            cmmnCaseInstanceRepository.findById(qualityShellInstanceId)
        } returns Optional.empty()

        every {
            cmmnHistoricCaseInstanceRepository.findById(qualityShellInstanceId)
        } returns Optional.empty()

        val exception = assertFailsWith<QualityShellNotFoundException> { fixture.convertToDto(projectInstance) }
        assertThat(exception.message).isEqualTo("Could not find a (historic) quality shell instance with id '${qualityShellInstanceId}'")

        verify(exactly = 1) { caseInstanceDtoMapper.convertToDto(regularCaseInstanceJpa) }
        verify(exactly = 2) { cmmnCaseInstanceRepository.findById(any()) }
        verify(exactly = 2) { cmmnHistoricCaseInstanceRepository.findById(any()) }

    }

    @Test
    fun `Convert projectInstanceEntity to dto with empty historic CmmnCaseInstanceJpa`() {

        every {
            cmmnCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.empty()

        every {
            cmmnHistoricCaseInstanceRepository.findById(regularCaseInstanceId)
        } returns Optional.empty()

        val exception = assertFailsWith<CaseInstanceNotFoundException> { fixture.convertToDto(projectInstance) }
        assertThat(exception.message).isEqualTo("Could not find a (historic) case instance with id '${regularCaseInstanceId}'")

        verify(exactly = 1) { cmmnCaseInstanceRepository.findById(any()) }
        verify(exactly = 1) { cmmnHistoricCaseInstanceRepository.findById(any()) }

    }
}
