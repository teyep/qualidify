package com.qualidify.service.impl.process

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.metadata.UploadFile
import com.qualidify.model.entities.flowable.FormDefinitionJpa
import com.qualidify.service.impl.process.repositories.FormDefinitionRepository
import com.qualidify.service.process.dto.FormDefinitionDto
import com.qualidify.service.process.dto.FormDefinitionUpload
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.flowable.form.api.FormDeployment
import org.flowable.form.api.FormDeploymentBuilder
import org.flowable.form.api.FormRepositoryService
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertEquals

internal class FormDefinitionServiceImplTest {
    private val logger by LoggerDelegation()

    private val mockFormDefinitionRepository = mockk<FormDefinitionRepository>()
    private val mockFormRepositoryService = mockk<FormRepositoryService>()


    private val fixture = FormDefinitionServiceImpl(mockFormDefinitionRepository, mockFormRepositoryService)

    private val dto = createMockDto()
    private val entity = createJpaEntity()

    @Test
    fun `Test countByQuery`() {
        mockkStatic(FormDefinitionRepository::countResponse)

        every {
            mockFormDefinitionRepository.countResponse(CountQuery<FormDefinitionDto>())
        } returns CountResponse(3)

        val result = fixture.countByQuery(CountQuery<FormDefinitionDto>())

        assertEquals(3, result.count)

        verify { mockFormRepositoryService wasNot called }
        verify(exactly = 1) { mockFormDefinitionRepository.countResponse(CountQuery<FormDefinitionDto>()) }
    }

    @Test
    fun `Test findByQuery is empty`() {
        mockkStatic(FormDefinitionRepository::findAllByQuery)

        every {
            mockFormDefinitionRepository.findAllByQuery(SearchQuery<FormDefinitionDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<FormDefinitionDto>())

        assertEquals(emptyList(), result)

        verify { mockFormRepositoryService wasNot called }
        verify(exactly = 1) { mockFormDefinitionRepository.findAllByQuery(SearchQuery<FormDefinitionDto>()) }
    }

    @ParameterizedTest
    @MethodSource("getDtoProperties")
    fun `Test query constraints mapping`(property: String) {
        mockkStatic(FormDefinitionRepository::findAllByQuery)

        val searchQuery = SearchQuery<FormDefinitionDto>(constraints = mapOf(property to "randomValue"))

        every {
            mockFormDefinitionRepository.findAllByQuery(searchQuery)
        } returns PageImpl(mutableListOf(entity))

        val results = fixture.findByQuery(searchQuery)

        assertThat(results.size).isOne()

        verify(exactly = 1) { mockFormDefinitionRepository.findAllByQuery(searchQuery) }
        verify { mockFormRepositoryService wasNot called }
    }

    @Test
    fun `Test creation of a FormDefinition`() {
        val formDefinitionUpload = FormDefinitionUpload(file = UploadFile("test", "bytes".toByteArray()))

        val mockFormDeploymentBuilder = mockk<FormDeploymentBuilder>()
        val mockFormDeployment = mockk<FormDeployment>()

        every {
            mockFormRepositoryService.createDeployment()
        } returns mockFormDeploymentBuilder
        every {
            mockFormDeploymentBuilder.addFormBytes("test", "bytes".toByteArray())
        } returns mockFormDeploymentBuilder
        every {
            mockFormDeploymentBuilder.deploy()
        } returns mockFormDeployment

        every {
            mockFormDeployment.id
        } returns "CreatedDeploymentID"

        every {
            mockFormDefinitionRepository.findByDeploymentId("CreatedDeploymentID")
        } returns listOf(entity)

        val result = fixture.create(formDefinitionUpload)

        assertThat(result).isEqualTo(dto)

        verify(exactly = 1) { mockFormRepositoryService.createDeployment() }
        verify(exactly = 1) { mockFormDefinitionRepository.findByDeploymentId("CreatedDeploymentID") }
        verify(exactly = 1) { mockFormDeploymentBuilder.addFormBytes("test", "bytes".toByteArray()) }
        verify(exactly = 1) { mockFormDeploymentBuilder.deploy() }
        verify(exactly = 1) { mockFormDeployment.id }
    }

    private fun createMockDto(): FormDefinitionDto {
        return FormDefinitionDto(
            id = "formDefinitionId",
            name = "formDefinitionName",
            key = "formDefinitionKey",
            version = 1,
            form = emptyMap()
        )
    }

    private fun createJpaEntity() = FormDefinitionJpa(
        id = "formDefinitionId",
        name = "formDefinitionName",
        key = "formDefinitionKey",
        version = 1,
        deploymentId = "CreatedDeploymentID"
    )


    companion object {

        @JvmStatic
        private fun getDtoProperties(): Set<String> {
            return FormDefinitionDto::class.declaredMemberProperties
                .map { it.name }
                .toSet()
        }

    }

}