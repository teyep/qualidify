package com.qualidify.service.impl.process.mappers

import com.qualidify.core.query.CountQuery
import com.qualidify.service.impl.process.PlanItemServiceImpl
import com.qualidify.service.impl.process.repositories.CmmnHistoricPlanItemRepository
import com.qualidify.service.impl.process.repositories.CmmnPlanItemRepository
import com.qualidify.service.process.dto.PlanItemDto
import org.flowable.cmmn.api.CmmnHistoryService
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.CmmnEngine
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.test.assertEquals

/**
 * Integration test for the mapping of Plan Items, Stages and Task Plans
 *
 * @author Menno Tol
 * @since 1.0
 */
@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class),
)
@SpringBootTest
internal class PlanItemJpaMappingIT(
    private val cmmnRuntimeService: CmmnRuntimeService,
    private val cmmnHistoryService: CmmnHistoryService,
    private val cmmnEngine: CmmnEngine,
    @Autowired private val cmmnPlanItemRepository: CmmnPlanItemRepository,
    @Autowired private val cmmnHistoricPlanItemRepository: CmmnHistoricPlanItemRepository,
    @Autowired private val planItemDtoMapper: PlanItemDtoMapper,
) {

    private val planItemService = PlanItemServiceImpl(
        cmmnPlanItemRepository,
        cmmnHistoricPlanItemRepository,
        planItemDtoMapper,
        cmmnEngine,
        cmmnRuntimeService,
        cmmnHistoryService,
    )

    @Test
    @CmmnDeployment(
        resources = [
            "dev-resources/cases/basicRequirement.cmmn.xml",
            "dev-resources/cases/employeeOnboarding.cmmn.xml",
            "dev-resources/cases/smallRequirement.cmmn.xml",
            "dev-resources/cases/twoEqualForms.cmmn.xml",
        ]
    )
    fun `Test count plant items`() {

        // start the cmmn processes
        cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("basicRequirement")
            .parentId("anyIdButNullShouldBeFine")
            .start()
        cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("employeeOnboarding")
            .parentId("anyIdButNullShouldBeFine")
            .variable("potentialEmployee", "John Doe")
            .start()
        cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("smallRequirement")
            .parentId("anyIdButNullShouldBeFine")
            .start()
        cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("twoEqualForms")
            .parentId("anyIdButNullShouldBeFine")
            .start()

        assertEquals(19, planItemService.countByQuery(CountQuery<PlanItemDto>()).count)

    }

}