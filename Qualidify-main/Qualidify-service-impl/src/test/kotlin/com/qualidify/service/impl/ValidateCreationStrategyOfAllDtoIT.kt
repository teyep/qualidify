package com.qualidify.service.impl

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.UploadFile
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.service.*
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.exceptions.DtoCreationException
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import kotlin.reflect.KClass


/**
 * Test the static MetaData supplier when setup is correct.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest
@DirtiesContext
class ValidateCreationStrategyOfAllDtoIT() {

    private val logger by LoggerDelegation()

    @Autowired
    lateinit var creator: ObjectCreator

    @Autowired
    lateinit var converter: ObjectConverter

    @Autowired
    lateinit var mnemonicRegistry: MnemonicRegistry


    @Test
    fun `Check if all Dto's can be created with the creationstrategypattern as configured`() {
        val dtos = DtoValidationUtil.getBeanDefinitionsOfDtos()
        val softAssertions = SoftAssertions()

        for (beanDefinition in dtos) {

            val dtoClass = DtoValidationUtil.getClassFromBeanDefinition(beanDefinition)
            if (DtoValidationUtil.isAdditionEnabled(dtoClass) && DtoValidationUtil.isAnnotatedWithCreationStrategy(
                    dtoClass
                )
            ) {

                val creationStrategy = DtoValidationUtil.getCreationStrategy(dtoClass)

                logger.info("Validating Dto $dtoClass with creationStrategy $creationStrategy.")

                when (creationStrategy?.value) {
                    CreationStrategyPattern.MAP_DTO -> testMapDtoCreation(dtoClass, softAssertions)
                    CreationStrategyPattern.FACTORY -> testFactoryCreation(dtoClass, softAssertions)
                    CreationStrategyPattern.EMPTY_CONSTRUCTOR -> testEmptyConstructorCreation(dtoClass, softAssertions)
                    CreationStrategyPattern.OBJECT_CONSTRUCTOR -> testObjectConstructorCreation(dtoClass,
                        softAssertions)
                    else -> {}
                }
            }

        }
        softAssertions.assertAll()
    }

    private fun testMapDtoCreation(dtoClass: KClass<*>, softAssertions: SoftAssertions) {
        try {
            creator.create(dtoClass)
        } catch (exception: DtoCreationException) {
            softAssertions.fail("Map Dto Creation of class ${dtoClass} failed", exception)
        }
    }

    private fun testFactoryCreation(dtoClass: KClass<*>, softAssertions: SoftAssertions) {
        try {
            val creationCommandType = DtoValidationUtil.getCreationStrategy(dtoClass)?.creationClass
            softAssertions.assertThat(creationCommandType)
                .`as`("The bean $dtoClass with creation strategy Factory, should have a creation command")
                .isNotEqualTo(Nothing::class)

            val factory = creator.getObjectFactory(beanType = dtoClass, command = creationCommandType!!)
            //only test if factory exists

            softAssertions.assertThat(factory)
                .`as`("Factory for $dtoClass with $creationCommandType should exist!").isNotNull
        } catch (exception: DtoCreationException) {
            softAssertions.fail("Factory constructor creation of class $dtoClass failed", exception)
        }
    }

    private fun testEmptyConstructorCreation(dtoClass: KClass<*>, softAssertions: SoftAssertions) {
        try {
            creator.create(dtoClass)
        } catch (exception: DtoCreationException) {
            softAssertions.fail("Empty constructor creation of class $dtoClass failed", exception)
        }
    }

    private fun testObjectConstructorCreation(dtoClass: KClass<*>, softAssertions: SoftAssertions) {
        try {
            val creationCommandType = DtoValidationUtil.getCreationStrategy(dtoClass)?.creationClass
            softAssertions.assertThat(creationCommandType)
                .`as`("The bean $dtoClass with creation strategy ObjectParameter, should have a creation command")
                .isNotEqualTo(Nothing::class)

            val dummyData = creataDummyDataForCommand(creationCommandType!!)
            val command = converter.convertToObject(dummyData, creationCommandType)

            creator.create(dtoClass, command)
        } catch (exception: DtoCreationException) {
            softAssertions.fail("Object constructor creation of class $dtoClass failed", exception)
        }
    }

    private fun creataDummyDataForCommand(creationCommandType: KClass<*>): Map<String, Any?> {
        val mnemonic = mnemonicRegistry.mnemonicOf(creationCommandType)!!
        val columnDefinitions = Metadata.getColumns(mnemonic)
        return columnDefinitions.asSequence()
            .map { it.name to getDummyDataForType(creationCommandType, it) }
            .toMap()
    }

    private fun getDummyDataForType(type: KClass<*>, columnDef: ColumnDef): Any? {
        val fieldType = DtoValidationUtil.getTypeOfField(type, columnDef.name)
        return getDummyDataForClass(fieldType)
    }

    private fun getDummyDataForClass(type: KClass<*>): Any? {
        return when (type) {
            Double::class -> 3.14
            Float::class -> 3.14
            Int::class -> 1
            LocalDate::class -> LocalDate.now()
            LocalDateTime::class -> LocalDateTime.now()
            Long::class -> 1
            String::class -> "Hello"
            UploadFile::class -> UploadFile("testfile", "content".toByteArray())
            UUID::class -> UUID.randomUUID()
            else -> throw RuntimeException("Dummydata not provided for type $type")
        }
    }


}