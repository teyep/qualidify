package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchCriterium
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.TestEntities
import com.qualidify.service.DtoFactory
import com.qualidify.service.impl.process.mappers.TaskDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnPlanItemRepository
import com.qualidify.service.impl.projects.CmmnTaskRepository
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.exceptions.TaskServiceException
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.TaskReviewService
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.flowable.cmmn.api.CmmnTaskService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import java.util.*
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

/**
 * A unit test for the [TaskServiceImpl]
 *
 * @author Menno Tol
 * @author Marcel Pot
 * @since 1.0
 */
internal class TaskServiceImplTest {

    private val cmmnTaskRepository = mockk<CmmnTaskRepository>()
    private val cmmnTaskService = mockk<CmmnTaskService>()
    private val planItemService = mockk<PlanItemService>()
    private val taskReviewService = mockk<TaskReviewService>()
    private val projectInstanceService = mockk<ProjectInstanceService>()

    private val taskDtoMapper = spyk(TaskDtoMapper())

    private val fixture =
        TaskServiceImpl(
            projectInstanceService,
            cmmnTaskRepository,
            cmmnTaskService,
            taskDtoMapper,
            taskReviewService,
            planItemService
        )

    private val currentUser = "currentUser"
    private val dto1 = DtoFactory.taskDto(assignee = "assignee", formKey = "mockForm")
    private val dto2 = DtoFactory.taskDto(assignee = currentUser, formKey = "mockForm")
    private val dto3 = DtoFactory.taskDto(assignee = null, formKey = "mockForm")

    @AfterEach
    fun checkTest() {
        confirmVerified(
            cmmnTaskRepository,
            cmmnTaskService,
            planItemService,
            taskReviewService,
            projectInstanceService
        )
        checkUnnecessaryStub(
            cmmnTaskRepository,
            cmmnTaskService,
            planItemService,
            taskReviewService,
            projectInstanceService
        )
    }

    @Test
    fun `Test countByQuery`() {
        mockkStatic(CmmnTaskRepository::countResponse)

        every {
            cmmnTaskRepository.countResponse(CountQuery<TaskDto>())
        } returns CountResponse(3)

        val result = fixture.countByQuery(CountQuery<TaskDto>())

        assertEquals(3, result.count)

        verify { cmmnTaskService wasNot called }
        verify(exactly = 1) { cmmnTaskRepository.countResponse(CountQuery<TaskDto>()) }
    }

    @Test
    fun `Test findQuery is empty`() {
        mockkStatic(CmmnTaskRepository::findAllByQuery)

        every {
            cmmnTaskRepository.findAllByQuery(SearchQuery<TaskDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<TaskDto>())

        assertContentEquals(emptyList(), result)

        verify { cmmnTaskService wasNot called }
        verify(exactly = 1) { cmmnTaskRepository.findAllByQuery(SearchQuery<TaskDto>()) }
    }

    @Test
    fun `Test assign TaskService call`() {
        every { cmmnTaskService.setAssignee(any(), any()) } just Runs
        fixture.assignTask(dto1, "user", currentUser)
        verify(exactly = 1) { cmmnTaskService.setAssignee(any(), "user") }
    }

    @Test
    fun `Test claim TaskService call`() {
        every { cmmnTaskService.claim(any(), any()) } just Runs

        fixture.claimTask(dto3, currentUser)

        verify(exactly = 1) { cmmnTaskService.claim(dto3.id, currentUser) }
    }

    @Test
    fun `Test try to claim an already claimed task`() {
        assertThrows<TaskServiceException> { fixture.claimTask(dto2, currentUser) }
        verify(exactly = 0) { cmmnTaskService.claim(dto2.id, currentUser) }
    }

    @Test
    fun `Test unclaim TaskService call as owner`() {
        every { cmmnTaskService.unclaim(dto2.id) } just Runs
        fixture.unclaimTask(dto2, currentUser)
        verify(exactly = 1) { cmmnTaskService.unclaim(dto2.id) }
    }

    @Test
    fun `Test unclaim TaskService call as non-owner`() {
        assertThrows<TaskServiceException> { fixture.unclaimTask(dto1, currentUser) }
        verify(exactly = 0) { cmmnTaskService.unclaim(dto1.id) }
    }

    @ParameterizedTest
    @MethodSource("getDtoProperties")
    fun `Test query constraints mapping`(property: String) {
        mockkStatic(CmmnPlanItemRepository::findAllByQuery)

        val searchQuery = SearchQuery<PlanItemDto>(searchCriteria = listOf(SearchCriterium(property to "randomValue")))
        every {
            cmmnTaskRepository.findAllByQuery(searchQuery)
        } returns PageImpl(listOf(TestEntities.cmmnTaskJpa("assignee", "mockForm")))

        val results = fixture.findByQuery(searchQuery)

        assertThat(results.size).isEqualTo(1)
        assertContentEquals(results, listOf(dto1))

        verify { cmmnTaskService wasNot called }
        verify(exactly = 1) { cmmnTaskRepository.findAllByQuery(searchQuery) }

    }

    companion object {

        @JvmStatic
        private fun getDtoProperties(): Set<String> {
            return TaskDto::class.declaredMemberProperties
                .map { it.name }
                .toSet()
        }
    }

}