package com.qualidify.service.impl.process

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.TestEntities
import com.qualidify.service.impl.process.mappers.TaskDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnHistoricTaskRepository
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.dto.TaskDto
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.flowable.engine.HistoryService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import java.util.*
import kotlin.reflect.full.declaredMemberProperties
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

/**
 * A unit test for the [HistoricTaskServiceImpl]
 *
 * @author Menno Tol
 * @since 1.0
 */
internal class HistoricTaskServiceImplTest {

    private val cmmnHistoricTaskRepository = mockk<CmmnHistoricTaskRepository>()
    private val flowableHistoryService = mockk<HistoryService>()
    private val caseInstanceService = mockk<CaseInstanceService>()
    private val projectInstanceService = mockk<ProjectInstanceService>()

    private val historicTaskDtoMapper = spyk(TaskDtoMapper())

    private val fixture =
        HistoricTaskServiceImpl(cmmnHistoricTaskRepository, historicTaskDtoMapper, flowableHistoryService)

    private val entity = TestEntities.cmmnHistoricTaskJpa()

    @AfterEach
    fun checkTest() {
        confirmVerified(cmmnHistoricTaskRepository, flowableHistoryService, caseInstanceService, projectInstanceService)
        checkUnnecessaryStub(cmmnHistoricTaskRepository, flowableHistoryService, caseInstanceService, projectInstanceService)
    }

    @Test
    fun `Test countByQuery`() {
        mockkStatic(CmmnHistoricTaskRepository::countResponse)

        every {
            cmmnHistoricTaskRepository.countResponse(CountQuery<TaskDto>())
        } returns CountResponse(3)

        val result = fixture.countByQuery(CountQuery<TaskDto>())

        assertEquals(3, result.count)

        verify(exactly = 1) { cmmnHistoricTaskRepository.countResponse(CountQuery<TaskDto>()) }
    }

    @Test
    fun `Test findByQuery is empty`() {
        mockkStatic(CmmnHistoricTaskRepository::findAllByQuery)

        every {
            cmmnHistoricTaskRepository.findAllByQuery(SearchQuery<TaskDto>())
        } returns Page.empty()

        val result = fixture.findByQuery(SearchQuery<TaskDto>())

        assertContentEquals(emptyList(), result)

        verify(exactly = 1) { cmmnHistoricTaskRepository.findAllByQuery(SearchQuery<TaskDto>()) }
    }

    @ParameterizedTest
    @MethodSource("getDtoProperties")
    fun `Test query constraints mapping`(property: String) {
        mockkStatic(CmmnHistoricTaskRepository::findAllByQuery)

        val searchQuery = SearchQuery<TaskDto>(constraints = mapOf(property to "randomValue"))
        every {
            cmmnHistoricTaskRepository.findAllByQuery(searchQuery)
        } returns PageImpl(mutableListOf(entity))

        val results = fixture.findByQuery(searchQuery)

        assertThat(results.size).isOne

        verify(exactly = 1) { cmmnHistoricTaskRepository.findAllByQuery(searchQuery) }
    }

    companion object {
        @JvmStatic
        private fun getDtoProperties(): Set<String> {
            return TaskDto::class.declaredMemberProperties
                .map { it.name }
                .toSet()
        }
    }

}