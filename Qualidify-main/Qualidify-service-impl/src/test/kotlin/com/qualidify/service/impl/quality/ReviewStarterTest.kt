package com.qualidify.service.impl.quality

import com.qualidify.service.DtoFactory
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.process.exceptions.CaseInstanceNotFoundException
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.ReviewType
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.fail
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import kotlin.test.assertEquals

/**
 * Unit test for the [ReviewStarter].
 * @author Menno Tol
 * @since 1.0
 */
internal class ReviewStarterTest {

    private val planItemService = mockk<PlanItemService>()
    private val caseInstanceService = mockk<CaseInstanceService>()

    @AfterEach
    fun checkTest() {
        confirmVerified(caseInstanceService, planItemService)
        checkUnnecessaryStub(caseInstanceService, planItemService)
    }

    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Test start review cascade with qualityShell item`(reviewType: ReviewType) {

        every {
            caseInstanceService.getCaseInstanceDtoById("qualityShellId")
        } returns DtoFactory.caseInstanceDto(
            id = "qualityShellInstanceId",
            isQualityShell = true
        )

        val eventListenerDto = DtoFactory.planItemDto(
            id = "qualityEventListenerId",
            state = PlanItemState.AVAILABLE,
            name = "Start ${reviewType.type} review",
            isQualityShell = true,
            type = PlanItemType.GENERIC_EVENT_LISTENER,
        )
        every {
            planItemService.getEventListenersByCaseInstanceId("qualityShellInstanceId")
        } returns listOf(
            eventListenerDto
        )

        val nextReviewTaskPlanItemDto = DtoFactory.planItemDto(
            id = "nextReviewTaskPlanItemId",
            name = "Review ${reviewType.type}",
            state = PlanItemState.WAITING_FOR_REPETITION,
        )
        every {
            planItemService.getPlanItemsByStageAndDefinition(eventListenerDto)
        } returns listOf(
            nextReviewTaskPlanItemDto
        )

        val reviewTaskQualityLinkIdSlot = slot<String>()
        every {
            planItemService.setPlanItemVariable(
                "nextReviewTaskPlanItemId",
                capture(reviewTaskQualityLinkIdSlot),
                any()
            )
        } just Runs

        val localizedNameSlot = slot<String>()
        every {
            planItemService.setPlanItemVariable(
                "nextReviewTaskPlanItemId",
                "localizedName",
                capture(localizedNameSlot)
            )
        } just Runs

        every {
            planItemService.completeGenericEventListener(any())
        } just Runs

        every {
            planItemService.setPlanItemVariable(
                "nextReviewTaskPlanItemId",
                "reviewState",
                ReviewState.IN_REVIEW.state
            )
        } just Runs

        when (reviewType) {
            ReviewType.TASK -> {
                val regularItem = DtoFactory.planItemDto(
                    id = "regularTaskPlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.HUMAN_TASK
                )

                val regularTaskQualityLinkIdSlot = slot<String>()
                every {
                    planItemService.setPlanItemVariable(
                        regularItem.id,
                        capture(regularTaskQualityLinkIdSlot),
                        any()
                    )
                } just Runs

                every {
                    planItemService.setPlanItemVariable(
                        "regularTaskPlanItemId",
                        "reviewState",
                        ReviewState.IN_REVIEW.state
                    )
                } just Runs

                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                fixture.start(regularItem)

                assertEquals("regularTaskPlanItemId", reviewTaskQualityLinkIdSlot.captured)
                assertEquals("reviewTaskPlanItemId", regularTaskQualityLinkIdSlot.captured)
                assertEquals("Review for ${reviewType.type}: '${regularItem.name}'", localizedNameSlot.captured)

                verify(exactly = 7) { planItemService.setPlanItemVariable(any(), any(), any()) }

            }

            ReviewType.STAGE -> {
                val regularItem = DtoFactory.planItemDto(
                    id = "regularStagePlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.STAGE
                )

                val regularStageQualityLinkIdSlot = slot<String>()
                every {
                    planItemService.setPlanItemVariable(
                        regularItem.id,
                        capture(regularStageQualityLinkIdSlot),
                        any()
                    )
                } just Runs

                every {
                    planItemService.setPlanItemVariable(
                        "regularStagePlanItemId",
                        "reviewState",
                        ReviewState.IN_REVIEW.state
                    )
                } just Runs

                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                fixture.start(regularItem)

                assertEquals("regularStagePlanItemId", reviewTaskQualityLinkIdSlot.captured)
                assertEquals("reviewTaskPlanItemId", regularStageQualityLinkIdSlot.captured)
                assertEquals("Review for ${reviewType.type}: '${regularItem.name}'", localizedNameSlot.captured)

                verify(exactly = 7) { planItemService.setPlanItemVariable(any(), any(), any()) }

            }

            ReviewType.CASE -> {
                val regularItem = DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,  // this is the regular item, so isQualityShell is false
                    qualityShellId = "qualityShellId"
                )

                val regularCaseQualityLinkIdSlot = slot<String>()
                every {
                    caseInstanceService.setCaseVariable(
                        regularItem.id,
                        capture(regularCaseQualityLinkIdSlot),
                        any()
                    )
                } just Runs

                every {
                    caseInstanceService.setCaseVariable(
                        "regularCaseInstanceId",
                        "reviewState",
                        ReviewState.IN_REVIEW.state
                    )
                } just Runs

                val fixture = ReviewStarter<CaseInstanceDto>(caseInstanceService, planItemService)
                fixture.start(regularItem)

                assertEquals("regularCaseInstanceId", reviewTaskQualityLinkIdSlot.captured)
                assertEquals("reviewTaskPlanItemId", regularCaseQualityLinkIdSlot.captured)
                assertEquals("Review for ${reviewType.type}: '${regularItem.name}'", localizedNameSlot.captured)

                verify(exactly = 3) { caseInstanceService.setCaseVariable(any(), any(), any()) }
                verify(exactly = 4) { planItemService.setPlanItemVariable(any(), any(), any()) }

            }

            else -> {
                fail("Type $reviewType not implemented.")
            }

        }

        verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }
        verify(exactly = 1) { planItemService.getPlanItemsByStageAndDefinition(any()) }
        verify(exactly = 1) { planItemService.completeGenericEventListener(any()) }

    }

    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Test find non-existing quality shell case instance`(type: ReviewType) {

        every { caseInstanceService.getCaseInstanceDtoById(any()) } throws CaseInstanceNotFoundException()

        when (type) {
            ReviewType.TASK -> {
                val dto = DtoFactory.planItemDto(
                    id = "regularTaskPlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.HUMAN_TASK
                )
                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                val exception = assertThrows<ReviewStarterException> { fixture.start(dto) }
                assertEquals(
                    "The quality shell instance with id '${dto.qualityShellId}' could not be found.",
                    exception.message
                )
            }

            ReviewType.STAGE -> {
                val dto = DtoFactory.planItemDto(
                    id = "regularStagePlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.STAGE
                )
                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                val exception = assertThrows<ReviewStarterException> { fixture.start(dto) }
                assertEquals(
                    "The quality shell instance with id '${dto.qualityShellId}' could not be found.",
                    exception.message
                )
            }

            ReviewType.CASE -> {
                val dto = DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,
                    qualityShellId = "some_id"
                )
                val fixture = ReviewStarter<CaseInstanceDto>(caseInstanceService, planItemService)
                val exception = assertThrows<ReviewStarterException> { fixture.start(dto) }
                assertEquals(
                    "The quality shell instance with id '${dto.qualityShellId}' could not be found.",
                    exception.message
                )
            }

            else -> {
                fail("Type $type not implemented in AbstractReviewServiceImpl and/or its tests.")
            }
        }

        verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify { planItemService wasNot called }

    }

    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Test find quality shell case instance without id`(type: ReviewType) {

        when (type) {
            ReviewType.TASK -> {
                val dto = DtoFactory.planItemDto(
                    id = "regularTaskPlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.HUMAN_TASK
                )
                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                val exception = assertThrows<ReviewStarterException> { fixture.start(dto.copy(qualityShellId = null)) }
                assertEquals(
                    "The 'cmmnQualityShellId' property of dto '${dto.name}', id '${dto.id}' is 'null'",
                    exception.message
                )
            }

            ReviewType.STAGE -> {
                val dto = DtoFactory.planItemDto(
                    id = "regularStagePlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.STAGE
                )
                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                val exception = assertThrows<ReviewStarterException> { fixture.start(dto.copy(qualityShellId = null)) }
                assertEquals(
                    "The 'cmmnQualityShellId' property of dto '${dto.name}', id '${dto.id}' is 'null'",
                    exception.message
                )
            }

            ReviewType.CASE -> {
                val dto = DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,
                    qualityShellId = "some_id"
                )
                val fixture = ReviewStarter<CaseInstanceDto>(caseInstanceService, planItemService)
                val exception = assertThrows<ReviewStarterException> { fixture.start(dto.copy(qualityShellId = null)) }
                assertEquals(
                    "The 'cmmnQualityShellId' property of dto '${dto.name}', id '${dto.id}' is 'null'",
                    exception.message
                )
            }

            else -> {
                fail("Type $type not implemented in AbstractReviewServiceImpl and/or its tests.")
            }
        }

        verify { caseInstanceService wasNot called }
        verify { planItemService wasNot called }

    }

    @Test
    fun `Test with non-exising review type`() {

        every {
            caseInstanceService.getCaseInstanceDtoById("qualityShellId")
        } returns DtoFactory.caseInstanceDto(
            id = "qualityShellInstanceId",
            isQualityShell = true
        )

        val dto = DtoFactory.planItemDto(
            id = "regularTaskPlanItemId",
            isQualityShell = true,
            type = PlanItemType.MILESTONE  // milestone should do it for now
        )

        val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
        val exception = assertThrows<ReviewStarterException> { fixture.start(dto) }
        assertEquals(
            "Review for plan item type '${dto.type.type}' not implemented.",
            exception.message
        )

        verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify { planItemService wasNot called }

    }


    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Test find review starter when no starter are present`(type: ReviewType) {

        every {
            caseInstanceService.getCaseInstanceDtoById("qualityShellId")
        } returns DtoFactory.caseInstanceDto(
            id = "qualityShellInstanceId",
            isQualityShell = true
        )

        every {
            planItemService.getEventListenersByCaseInstanceId(any())
        } returns mutableListOf()

        when (type) {
            ReviewType.TASK -> {
                val regularItemDto = DtoFactory.planItemDto(
                    id = "regularTaskPlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.HUMAN_TASK
                )
                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                assertThrows<ReviewStarterException> { fixture.start(regularItemDto) }
            }

            ReviewType.STAGE -> {
                val regularItemDto = DtoFactory.planItemDto(
                    id = "regularStagePlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.STAGE
                )
                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                assertThrows<ReviewStarterException> { fixture.start(regularItemDto) }
            }

            ReviewType.CASE -> {
                val regularItemDto = DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,
                    qualityShellId = "qualityShellId"
                )
                val fixture = ReviewStarter<CaseInstanceDto>(caseInstanceService, planItemService)
                assertThrows<ReviewStarterException> { fixture.start(regularItemDto) }
            }

            else -> {
                fail("Type $type not implemented in AbstractReviewServiceImpl and/or its tests.")
            }
        }

        verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }

    }

    @ParameterizedTest
    @MethodSource("getReviewTypes")
    fun `Test find review starter with multiple starters`(type: ReviewType) {

        every {
            caseInstanceService.getCaseInstanceDtoById("qualityShellId")
        } returns DtoFactory.caseInstanceDto(
            id = "qualityShellInstanceId",
            isQualityShell = true
        )

        every { planItemService.getEventListenersByCaseInstanceId(any()) } returns mutableListOf(
            DtoFactory.planItemDto(state = PlanItemState.AVAILABLE, name = "Start $type review"),
            DtoFactory.planItemDto(state = PlanItemState.AVAILABLE, name = "Start $type review"),
            DtoFactory.planItemDto(state = PlanItemState.AVAILABLE, name = "Start $type review"),
        )

        when (type) {
            ReviewType.TASK -> {
                val regularItemDto = DtoFactory.planItemDto(
                    id = "regularTaskPlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.HUMAN_TASK
                )
                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                assertThrows<ReviewStarterException> { fixture.start(regularItemDto) }
            }

            ReviewType.STAGE -> {
                val regularItemDto = DtoFactory.planItemDto(
                    id = "regularStagePlanItemId",
                    isQualityShell = false,
                    type = PlanItemType.STAGE
                )
                val fixture = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
                assertThrows<ReviewStarterException> { fixture.start(regularItemDto) }
            }

            ReviewType.CASE -> {
                val regularItemDto = DtoFactory.caseInstanceDto(
                    id = "regularCaseInstanceId",
                    isQualityShell = false,
                    qualityShellId = "qualityShellId"
                )
                val fixture = ReviewStarter<CaseInstanceDto>(caseInstanceService, planItemService)
                assertThrows<ReviewStarterException> { fixture.start(regularItemDto) }
            }

            else -> {
                fail("Type $type not implemented in AbstractReviewServiceImpl and/or its tests.")
            }
        }

        verify(exactly = 1) { caseInstanceService.getCaseInstanceDtoById(any()) }
        verify(exactly = 1) { planItemService.getEventListenersByCaseInstanceId(any()) }

    }

    companion object {
        @JvmStatic
        private fun getReviewTypes() = ReviewType.values()
    }

}