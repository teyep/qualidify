package com.qualidify.service.impl.process.cmmn

import com.qualidify.service.quality.TaskReviewService
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.engine.test.CmmnDeployment
import org.flowable.cmmn.spring.impl.test.FlowableCmmnSpringExtension
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

/**
 * Showcase for starting a case task from a cmmn file (cmmn-in-cmmn).
 * Information gathered from the following links:
 * [link] https://www.flowable.com/open-source/docs/cmmn/ch06-cmmn#case-task
 * [link] https://github.com/flowable/flowable-engine/blob/main/modules/flowable-cmmn-engine/src/test/resources/org/flowable/cmmn/test/runtime/CaseTaskTest.testCaseReferenceExpression.cmmn
 *
 * For mapping a single variable (e.g. document/file?) it is better to use IO mapping with extensionElements:
 * <extensionElements>
 *   <flowable:in source="someVariableInMainProcess" target="nameOfVariableInSubProcess" />
 *   <flowable:out source="someVariableInSubProcess" target="nameOfVariableInMainProcess" />
 * </extensionElements>
 * NB: The extensionElements of a caseTask must be placed above the caseRefExpression tag!
 *
 * @author Menno Tol
 * @since 1.0
 */
@Extensions(
    ExtendWith(FlowableCmmnSpringExtension::class),
    ExtendWith(SpringExtension::class)
)
@SpringBootTest
@Disabled("Disabled because it is not testing Qualidify functionality, " +
    "but it is a showcase for CMMN (disable this line to walk through)"
)
class CmmnInCmmnTest(
    val cmmnRuntimeService: CmmnRuntimeService,
    val cmmnTaskService: CmmnTaskService
) {

    // mock bean will overwrite the taskReviewService with a null object, preventing startup of the review cascade
    @MockBean
    private var taskReviewService: TaskReviewService? = null

    @Test
    @CmmnDeployment(
        resources = ["dev-resources/cases/cmmnQualityShell.cmmn.xml", "dev-resources/cases/employeeOnboarding.cmmn.xml"]
    )
    fun `test running cmmn file from cmmn task`() {
        // start the parent case
        val parentCase: CaseInstance = cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionKey("cmmnQualityShell")
            .variable("regularCaseDefinitionKey", "employeeOnboarding")
            .variable("potentialEmployee", "TEST-EMPLOYEE-from-main-process")
            .start()
        assertNotNull(parentCase, "The parentCase case instance should be found")

        val parentCasePlanItems = cmmnRuntimeService.createPlanItemInstanceQuery()
            .caseInstanceId(parentCase.id)
            .includeEnded()
            .list()
        assertEquals(8, parentCasePlanItems.size, "There should be 8 plan item instance in the parent case")

        // the childCasePlanItem should be in state active since it is an automatic task
        val childCasePlanItem = cmmnRuntimeService.createPlanItemInstanceQuery().planItemInstanceName("Start child case").singleResult()
        assertEquals(
            "active",
            childCasePlanItem.state,
            "The 'Start child case' plan item instance should be in state 'active' (because it is a automatic task)"
        )
        assertEquals(
            2, cmmnRuntimeService.createCaseInstanceQuery().list().size,
            "There should be 2 case instances"
        )

        val childCase =
            cmmnRuntimeService.createCaseInstanceQuery()
                .caseDefinitionKey(parentCase.caseVariables["regularCaseDefinitionKey"].toString())
                .singleResult()

        // make all variables of the parent available to the child
        cmmnRuntimeService.setVariables(childCase.id, parentCase.caseVariables)

        assertEquals(childCase.parentId, parentCase.id, "The child case should have the parentCase as parent")
        assertEquals(
            childCase.callbackType,
            "cmmn-1.1-to-cmmn-1.1-child-case",
            "The callbacktype should be like 'cmmn-1.1-to-cmmn-1.1-child-case'"
        )

        var childTasks = cmmnTaskService.createTaskQuery().caseInstanceId(childCase.id).list()
        assertEquals(3, childTasks.size, "There should be 3 tasks active")

        // fast complete the child case to see if the review task is started
        while (childTasks.size > 0) {
            childTasks.forEach { task ->
                // complete the task
                cmmnTaskService.complete(task.id)

                // trigger the task review event listener
                val taskReviewEventListener = cmmnRuntimeService.createGenericEventListenerInstanceQuery()
                    .planItemDefinitionId("EventListener_0c8gdhe")
                    .singleResult()

                cmmnRuntimeService.triggerPlanItemInstance(taskReviewEventListener.id)
            }
            childTasks = cmmnTaskService.createTaskQuery().caseInstanceId(childCase.id).list()
        }

        // complete the child case
        cmmnRuntimeService.completeCaseInstance(childCase.id)

        val parentPlanItemsAfterChildCompletion = cmmnRuntimeService
            .createPlanItemInstanceQuery().caseInstanceId(parentCase.id).includeEnded().list()

        val parentReviewChildTaskTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(parentCase.id)
            .taskName("Review child task")
            .list()
        assertEquals(6, parentReviewChildTaskTasks.size, "There should be 6 review tasks")

        // complete all review tasks
        parentReviewChildTaskTasks.forEach { reviewTask ->
            cmmnTaskService.complete(reviewTask.id)
        }

        val parentReviewChildCaseTasks = cmmnTaskService.createTaskQuery()
            .caseInstanceId(parentCase.id)
            .taskName("Review child case")
            .list()
        assertEquals(1, parentReviewChildCaseTasks.size, "There should be 1 case review task")


    }

}