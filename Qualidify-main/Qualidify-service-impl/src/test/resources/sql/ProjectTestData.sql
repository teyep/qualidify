INSERT INTO PROJECT_TEMPLATE (ID, NAME, DESCRIPTION, CMMN_MODEL, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
VALUES ('155f999b-24a4-fde2-4f5b-0110efbbbf31', 'Small Requirement', 'Small Requirement Case', 'smallRequirement',
        'devscript', CURRENT_TIMESTAMP(), 'devscript',
        CURRENT_TIMESTAMP());
INSERT INTO PROJECT_TEMPLATE (ID, NAME, DESCRIPTION, CMMN_MODEL, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
VALUES ('125b959b-24a4-fde2-4f5b-0110efbbbf31', 'Basic Requirement', 'Basic Requirement Case', 'basicRequirement',
        'devscript', CURRENT_TIMESTAMP(), 'devscript',
        CURRENT_TIMESTAMP());
INSERT INTO PROJECT_TEMPLATE (ID, NAME, DESCRIPTION, CMMN_MODEL, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
VALUES ('134f939c-24a4-fde3-4f66-0110cfbbef31', 'Employee Onboarding', 'Employee Onboarding Case', 'employeeOnboarding',
        'devscript', CURRENT_TIMESTAMP(), 'devscript',
        CURRENT_TIMESTAMP());
INSERT INTO PROJECT_TEMPLATE (ID, NAME, DESCRIPTION, CMMN_MODEL, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
VALUES ('167f939c-24a4-fde3-4f66-0110cfbbef31', 'Two Equal Forms', 'Two Equal Forms Case', 'twoEqualForms',
        'devscript', CURRENT_TIMESTAMP(), 'devscript',
        CURRENT_TIMESTAMP());
INSERT INTO PROJECT_TEMPLATE (ID, NAME, DESCRIPTION, CMMN_MODEL, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
VALUES ('524f346c-24a4-fde3-4f66-0110cfbbef31', 'Jpa Entities', 'Jpa Entities Case', 'jpaEntities',
        'devscript', CURRENT_TIMESTAMP(), 'devscript',
        CURRENT_TIMESTAMP());

-- Project Template variables
INSERT INTO PROJECT_TEMPLATE_VARIABLES (PROJECT_TEMPLATE_ID, NAME, DESCRIPTION, DATATYPE, DEFAULT_VALUE)
VALUES ('134f939c-24a4-fde3-4f66-0110cfbbef31', 'potentialEmployee', 'potentialEmployee', 'STRING', null);