package com.qualidify.service.impl.process

import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.flowable.CmmnHistoricTaskJpa
import com.qualidify.service.impl.process.mappers.TaskDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnHistoricTaskRepository
import com.qualidify.service.process.HistoricTaskService
import com.qualidify.service.projects.dto.TaskDto
import org.flowable.engine.HistoryService
import org.springframework.stereotype.Service

/**
 * An implementation of the [HistoricTaskService], to retrieve the [HistoricTaskDto] from flowable
 *
 * @author Menno Tol
 * @author Marcel Pot
 * @since 1.0
 */
@Service
open class HistoricTaskServiceImpl(
    private val cmmnHistoricTaskRepository: CmmnHistoricTaskRepository,
    private val mapper: TaskDtoMapper,
    private val flowableHistoryService: HistoryService,
) : AbstractQueryableService<CmmnHistoricTaskJpa, TaskDto>(cmmnHistoricTaskRepository),
    HistoricTaskService {

    override fun mapEntityToDto(entity: CmmnHistoricTaskJpa) = mapper.convertToDto(entity)

    override fun isHistoricTaskAvailable(historicTaskId: String): Boolean =
        cmmnHistoricTaskRepository.existsById(historicTaskId)

    override fun getHistoricTaskDtoById(historicTaskId: String): TaskDto =
        cmmnHistoricTaskRepository.findById(historicTaskId).map {
            mapEntityToDto(it)
        }.get()

    override fun getHistoricFormVariables(historicTaskId: String): Map<String, Any?> =
        flowableHistoryService.createHistoricVariableInstanceQuery().taskId(historicTaskId)
            .list()
            .associate { it.variableName to it.value }

}