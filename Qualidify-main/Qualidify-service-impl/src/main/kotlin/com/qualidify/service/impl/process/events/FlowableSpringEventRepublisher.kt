package com.qualidify.service.impl.process.events

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.impl.projects.ProjectInstanceRepository
import com.qualidify.service.impl.quality.getQualityLinkId
import com.qualidify.service.impl.quality.getReviewState
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.dto.CaseInstanceState
import com.qualidify.service.quality.QualityLinkKey
import org.flowable.cmmn.api.listener.CaseInstanceLifecycleListener
import org.flowable.cmmn.api.runtime.CaseInstance
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component
import kotlin.jvm.optionals.getOrElse

/**
 * A publisher for adapting flowable cmmn lifecycle events to spring events
 *
 * NB: the publisher/listener is added to the flowable engine at an early stage where instances are not available
 * (e.g. project instances, case instances, etc.).
 * This leads to the following work-around:
 * if the caseInstanceDto below is NOT a qualityShell, then it's cmmnQualityShellId cannot be set (since the project
 * instance is not available). Instead, the reference is marked as 'QUALITY_SHELL_ID_NOT_KNOWN'
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Component
class FlowableSpringEventRepublisher(
    private val publisher: ApplicationEventPublisher,
    private val cmmnCaseDefinitionRepository: CmmnCaseDefinitionRepository,
    private val projectInstanceRepository: ProjectInstanceRepository,
    @Lazy private val caseInstanceService: CaseInstanceService,
) : CaseInstanceLifecycleListener {

    private val logger by LoggerDelegation()

    override fun getSourceState() = ""

    override fun getTargetState() = ""

    override fun stateChanged(caseInstance: CaseInstance, oldState: String, newState: String) {
        logger.trace(
            "Publishing cmmn lifecycle event for case instance '${caseInstance.id}', " +
                    "from '$oldState' to '$newState'."
        )
        val cmmnCaseDefinitionJpa = cmmnCaseDefinitionRepository.findById(caseInstance.caseDefinitionId).get()
        val projectInstance = projectInstanceRepository.findProjectInstanceByRegularCaseId(caseInstance.id)
        val isQualityShell = (cmmnCaseDefinitionJpa.key == "cmmnQualityShell")

        val caseInstanceDto = CaseInstanceDto(
            id = caseInstance.id,
            name = cmmnCaseDefinitionJpa.name,
            projectInstanceId = projectInstance.map { it.id.id.toString() }
                .getOrElse { "PROJECT_INSTANCE_NOT_AVAILABLE" },
            caseDefinitionKey = cmmnCaseDefinitionJpa.key,
            caseDefinition = CaseDefinitionDto(
                id = cmmnCaseDefinitionJpa.id,
                name = cmmnCaseDefinitionJpa.name,
                key = cmmnCaseDefinitionJpa.key,
                version = cmmnCaseDefinitionJpa.version,
                deploymentResourceId = cmmnCaseDefinitionJpa.deploymentResource?.deploymentId,
                graphicalNotation = cmmnCaseDefinitionJpa.deploymentResource?.graphicalNotation
            ),
            state = CaseInstanceState.getByName(caseInstance.state.lowercase()),
            completable = caseInstance.isCompletable,
            isQualityShell = isQualityShell,
            regularCaseId = if (cmmnCaseDefinitionJpa.key != "cmmnQualityShell") null
            else projectInstance.map { it.regularCaseId }.getOrElse { "REGULAR_CASE_ID_NOT_KNOWN" },
            qualityShellId = if (cmmnCaseDefinitionJpa.key == "cmmnQualityShell") null
            else projectInstance.map { it.qualityShellId }.getOrElse { "QUALITY_SHELL_ID_NOT_KNOWN" },
//            qualityLinkKey = getQualityLinkKey(caseInstanceService, caseInstance),
            qualityLinkKey = QualityLinkKey.UNDEFINED,
            qualityLinkId = getQualityLinkId(caseInstanceService, caseInstance, isQualityShell),
            reviewState = getReviewState(caseInstanceService, caseInstance)
        )
        publisher.publishEvent(buildEvent(caseInstanceDto, oldState, newState))
    }

    private fun buildEvent(
        caseInstance: CaseInstanceDto,
        oldState: String,
        newState: String,
    ): CmmnLifecycleEvent {
        return when (newState.uppercase()) {
            "ACTIVATED" -> ActivatedCmmnEvent(caseInstance)
            "COMPLETED" -> CompletedCmmnEvent(caseInstance)
            "TERMINATED" -> TerminatedCmmnEvent(caseInstance)
            else -> CmmnLifecycleEvent(caseInstance, oldState, newState)
        }
    }
}