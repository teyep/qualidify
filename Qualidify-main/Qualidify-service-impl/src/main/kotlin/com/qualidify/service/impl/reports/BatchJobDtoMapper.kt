package com.qualidify.service.impl.reports

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.BatchJob
import com.qualidify.model.entities.BatchJobId
import com.qualidify.model.entities.BatchJobStatus
import com.qualidify.service.report.dto.BatchJobDto

@DtoMapper
class BatchJobDtoMapper : EntityDtoMapper<BatchJob, BatchJobDto> {

    override fun convertToModel(dto: BatchJobDto): BatchJob {
        return BatchJob(
            id = BatchJobId(dto.batchJobId),
            jobType = dto.jobType,
            jobName = dto.jobName,
            parameters = dto.parameters,
            status = BatchJobStatus.valueOf(dto.status),
            finishedDate = dto.finishedDate
        )
    }

    override fun convertToDto(entity: BatchJob): BatchJobDto {
        return BatchJobDto(
            batchJobId = entity.id.unbox(),
            jobType = entity.jobType,
            jobName = entity.jobName,
            parameters = entity.parameters,
            status = entity.status.toString(),
            finishedDate = entity.finishedDate
        )
    }
}