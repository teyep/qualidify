package com.qualidify.service.impl.projects


import com.qualidify.core.jpa.annotations.SoftDelete
import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.ProjectInstance
import com.qualidify.model.entities.ProjectInstanceId
import java.util.*

/**
 * Repository for persisting and retrieving projectInstances from the datastorage
 *
 * @author Marcel Pot
 * @since 1.0
 */
@SoftDelete
interface ProjectInstanceRepository : QueryablePagingAndSortingRepository<ProjectInstance, ProjectInstanceId> {
    /**
     * Find a project instance based on its coupled regular case instance id
     * @param regularCaseId    the id of the regular case instance
     * @return                  an optional of a project instance
     */
    fun findProjectInstanceByRegularCaseId(regularCaseId: String): Optional<ProjectInstance>

    /**
     * Find a project instance based on its coupled quality shell instance id
     * @param qualityShellId    the id of the quality shell instance
     * @return                      an optional of a project instance
     */
    fun findProjectInstanceByQualityShellId(qualityShellId: String): Optional<ProjectInstance>

}

