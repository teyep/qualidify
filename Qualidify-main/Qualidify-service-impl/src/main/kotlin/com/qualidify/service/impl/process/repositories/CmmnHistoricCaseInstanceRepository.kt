package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.CmmnHistoricCaseInstanceJpa

/**
 * Read only repository for the [CmmnHistoricCaseInstanceJpa]
 *
 * @author Menno Tol
 * @since 1.0
 */
interface CmmnHistoricCaseInstanceRepository :
    ReadOnlyQueryablePagingAndSortingRepository<CmmnHistoricCaseInstanceJpa, String> {
}