package com.qualidify.service.impl.quality.exceptions

class QualityLinkKeyNotFoundException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)
