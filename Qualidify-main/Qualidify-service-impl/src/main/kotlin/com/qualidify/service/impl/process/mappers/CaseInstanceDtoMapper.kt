package com.qualidify.service.impl.process.mappers

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.flowable.CmmnCaseInstanceJpa
import com.qualidify.model.entities.flowable.definition.CmmnCaseInstanceJpaDef
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.dto.CaseInstanceState
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState

@DtoMapper
class CaseInstanceDtoMapper(
    private val cmmnCaseDefinitionRepository: CmmnCaseDefinitionRepository,
) : EntityDtoMapper<CmmnCaseInstanceJpaDef, CaseInstanceDto> {

    override fun convertToModel(dto: CaseInstanceDto): CmmnCaseInstanceJpa {
        TODO("Not yet implemented")
    }

    /**
     * Manual mapping of the embedded caseDefinitionDto's graphical notation to null
     * to prevent sending an image with all parent CaseInstanceDto's
     * (which are included in PlanItems, Tasks etc.).
     * If required the graphics can be obtained via the deploymentResourceId
     */
    override fun convertToDto(entity: CmmnCaseInstanceJpaDef): CaseInstanceDto {
        val cmmnCaseDefinitionJpa = cmmnCaseDefinitionRepository.findById(entity.cmmnCaseDefinition.id).get()

        return CaseInstanceDto(
            id = entity.id,
            name = cmmnCaseDefinitionJpa.name,
            projectInstanceId = entity.projectInstanceId ?: "PROJECT_INSTANCE_NOT_AVAILABLE",
            caseDefinitionKey = cmmnCaseDefinitionJpa.key,
            caseDefinition = CaseDefinitionDto(
                id = cmmnCaseDefinitionJpa.id,
                name = cmmnCaseDefinitionJpa.name,
                key = cmmnCaseDefinitionJpa.key,
                version = cmmnCaseDefinitionJpa.version,
                deploymentResourceId = entity.cmmnCaseDefinition.deploymentResource?.deploymentId,
                graphicalNotation = null
            ),
            state = CaseInstanceState.getByName(entity.state.lowercase()),
            completable = entity.completable,
            isQualityShell = entity.isQualityShell,
            regularCaseId = entity.regularCaseId,
            qualityShellId = entity.qualityShellId,
            qualityLinkKey =  QualityLinkKey.getByName(entity.qualityLinkKey),
            qualityLinkId = entity.qualityLinkId,
            reviewState = ReviewState.getByName(entity.reviewState),
        )
    }

}