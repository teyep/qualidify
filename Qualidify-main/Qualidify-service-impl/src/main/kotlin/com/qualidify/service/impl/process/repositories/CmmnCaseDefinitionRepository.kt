package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.CmmnCaseDefinitionJpa

/**
 * Read only repository fo [CmmnCaseDefinitionJpa]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface CmmnCaseDefinitionRepository : ReadOnlyQueryablePagingAndSortingRepository<CmmnCaseDefinitionJpa, String> {

    fun findByKey(key: String): CmmnCaseDefinitionJpa

}