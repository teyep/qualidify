package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.impl.quality.exceptions.UnsupportedReviewStateException
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.TaskReviewService
import org.flowable.cmmn.api.runtime.PlanItemInstance
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

/**
 * Starts the task review cascade based on a regular plan item instance obtained from a listener,
 * see [TaskCompletedListener].
 *
 * @author Menno Tol
 * @since 1.0
 */
@Service
class TaskReviewServiceImpl(
    private val caseInstanceService: CaseInstanceService,
    @Lazy private val planItemService: PlanItemService,
    @Lazy private val taskService: TaskService,
) : TaskReviewService {

    private val logger by LoggerDelegation()

    override fun startReview(planItemInstance: PlanItemInstance) {
        val planItemDto = planItemService.getPlanItemDtoOrNull(planItemInstance.id)!!
        startReview(planItemDto)
    }

    override fun startReview(dto: PlanItemDto) {
        if (evaluate(dto)) {
            val reviewStarter = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
            reviewStarter.start(dto)
            logger.info("Review cascade for task '${dto.name}', id '${dto.id}' was started.")
        }
    }

    override fun evaluate(dto: PlanItemDto): Boolean {
        when (dto.type) {
            PlanItemType.GENERIC_EVENT_LISTENER, PlanItemType.SIGNAL_EVENT_LISTENER, PlanItemType.TIMER_EVENT_LISTENER,
            PlanItemType.USER_EVENT_LISTENER, PlanItemType.VARIABLE_EVENT_LISTENER,
            ->
                throw TaskReviewServiceException(
                    "Dto with id '${dto.id}', type '${dto.type}' is not a task but an event listener"
                )

            PlanItemType.STAGE, PlanItemType.MILESTONE, PlanItemType.PLAN_FRAGMENT ->
                throw TaskReviewServiceException(
                    "Dto with id '${dto.id}', type '${dto.type}' is not a task but a stage, milestone or plan fragment"
                )

            else -> {
                // quality tasks do not need start the review cascade
                if (dto.isQualityShell) return false

                // these review states do not need start the review cascade
                return dto.reviewState !in setOf(
                    ReviewState.REVIEW_NOT_REQUIRED,
                    ReviewState.APPROVED,  // review cascade concluded
                    ReviewState.REJECTED,  // has already a review cascade
                    ReviewState.IN_REVIEW,  // has already a review cascade
                    ReviewState.CORRECTED,  // has already a review cascade
                )
            }
        }
    }

    override fun approveReview(reviewTaskDto: TaskDto, principal: String) {

        // get the corresponding regular task dto
        val regularTaskDto = taskService.getTaskDtoByPlanItemId(reviewTaskDto.qualityLinkId!!)

        // the review state of the review task should be equal to the review state of the regular task
        if (reviewTaskDto.reviewState != regularTaskDto.reviewState) {
            throw TaskReviewServiceException("Review state of regular task and corresponding review task are not the same")
        }

        when (reviewTaskDto.reviewState) {
            ReviewState.REJECTED,
            -> {
                logger.warn("The ReviewState is 'rejected', the regular task '${regularTaskDto.name}' should first be corrected")
                return
            }

            ReviewState.IN_REVIEW, ReviewState.CORRECTED,
            -> {
                // update the review state of the regular and review task
                // (otherwise a new review task might be created or the complete function is not executed)
                val reviewStateUpdater = ReviewStateUpdater<TaskDto>(planItemService, caseInstanceService)
                reviewStateUpdater.start(reviewTaskDto, ReviewState.APPROVED)

                // complete the review task (copy the new review state)
                taskService.completeTask(reviewTaskDto.copy(reviewState = ReviewState.APPROVED))
                logger.info("Review task [${reviewTaskDto.name}] was completed by user $principal")

                // complete the regular task (copy the new review state)
                taskService.completeTask(regularTaskDto.copy(reviewState = ReviewState.APPROVED))
                logger.info("Regular task [${regularTaskDto.name}] was approved by user '$principal' and is completed")
            }

            else -> {
                throw TaskReviewServiceException(
                    "Unsupported review state '${reviewTaskDto.reviewState.state}' for task review approval, " +
                            "supported states are '${ReviewState.IN_REVIEW.state}' and '${ReviewState.CORRECTED.state}'."
                )
            }

        }
    }

    override fun rejectReview(reviewTaskDto: TaskDto, principal: String) {
        return if (reviewTaskDto.reviewState == ReviewState.REJECTED) {
            logger.warn("ReviewState is already 'rejected', the regular task must first be corrected")
        } else {
            // update the task review state
            val reviewStateUpdater = ReviewStateUpdater<TaskDto>(planItemService, caseInstanceService)
            reviewStateUpdater.start(reviewTaskDto, ReviewState.REJECTED)

            // marks the review task as resolved (so that it can be completed later) (copy the new review state)
            taskService.resolveTask(reviewTaskDto.copy(reviewState = ReviewState.REJECTED))
            logger.info("Review task [${reviewTaskDto.name}] was rejected by user '$principal'")
        }
    }

    override fun taskCorrected(regularTaskDto: TaskDto, principal: String) {
        if (regularTaskDto.reviewState != ReviewState.REJECTED) {
            throw UnsupportedReviewStateException("Review state '${regularTaskDto.reviewState.state}' is not allowed for task correction.")
        } else {
            // update the task review state
            val reviewStateUpdater = ReviewStateUpdater<TaskDto>(planItemService, caseInstanceService)
            reviewStateUpdater.start(regularTaskDto, ReviewState.CORRECTED)

            // marks the regular task as resolved (copy the new review state)
            taskService.resolveTask(regularTaskDto.copy(reviewState = ReviewState.CORRECTED))
            logger.info("Regular task [${regularTaskDto.name}] was corrected by user $principal")
        }
    }

}

class TaskReviewServiceException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)
