package com.qualidify.service.impl.process.mappers

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.flowable.definition.CmmnTaskJpaDef
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.projects.dto.TaskType
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState

@DtoMapper
class TaskDtoMapper : EntityDtoMapper<CmmnTaskJpaDef, TaskDto> {

    override fun convertToModel(dto: TaskDto): CmmnTaskJpaDef {
        TODO("Not yet implemented")
    }

    override fun convertToDto(entity: CmmnTaskJpaDef): TaskDto =
        TaskDto(
            id = entity.id,
            name = entity.localizedName ?: entity.name,
            taskDefinitionKey = entity.taskDefinitionKey,
            projectInstanceId = entity.projectInstanceId ?: "PROJECT_INSTANCE_NOT_AVAILABLE",
            caseInstanceId = entity.caseInstanceId,
            caseDefinitionId = entity.caseDefinitionId,
            planItemInstanceId = entity.planItemInstanceId,
            formKey = entity.formKey,
            owner = entity.owner,
            assignee = entity.assignee,
            type = TaskType.getByName(entity.type),
            stage = entity.stage,
            historic = entity.historic,
            isQualityShell = entity.isQualityShell,
            regularCaseId = entity.regularCaseId,
            qualityShellId = entity.qualityShellId,
            qualityLinkKey = QualityLinkKey.getByName(entity.qualityLinkKey),
            qualityLinkId = entity.qualityLinkId,
            reviewState = ReviewState.getByName(entity.reviewState),
        )

}