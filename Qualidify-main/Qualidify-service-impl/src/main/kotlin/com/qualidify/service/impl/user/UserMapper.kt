package com.qualidify.service.impl.user

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.User
import com.qualidify.model.entities.UserField
import com.qualidify.service.user.dto.UserDto
import java.util.stream.Collectors

/**
 * Mapper for mapping [User] model entities to and from [UserDto]
 *
 * @author Marcel Pot
 * @since 1.0
 */

@DtoMapper
open class UserMapper : EntityDtoMapper<User, UserDto> {

    private val fieldMapper = UserFieldMapper()

    companion object {
        @JvmStatic
        val INSTANCE: UserMapper by lazy {
            UserMapper()
        }
    }

    override fun convertToModel(dto: UserDto): User {
        return User.builder().withUserId(dto.userId!!)
            .withUsername(dto.username)
            .withEmail(dto.email)
            .withFields(fieldMapper.convertToModel(dto.fields))
            .build()
    }

    override fun convertToDto(entity: User): UserDto {
        return UserDto(
            userId = entity.userId,
            username = entity.username,
            email = entity.email,
            fields = fieldMapper.convertToDto(entity.fields),
            securityGroups = entity.groups.asSequence()
                .map { it.id.unbox() }
                .toSet()
        )
    }

}

open class UserFieldMapper {

    companion object {
        @JvmStatic
        val INSTANCE: UserFieldMapper by lazy {
            UserFieldMapper()
        }
    }

    fun convertToModel(fields: Map<String, String?>): Set<UserField> {
        return fields.entries.stream()
            .map { e -> UserField(e.key, e.value) }
            .collect(Collectors.toSet())
    }

    fun convertToDto(fields: Set<UserField>): MutableMap<String, String?> {
        return fields.asSequence()
            .map { Pair(it.field, it.value ?: "") }
            .toMap().toMutableMap()
    }
}