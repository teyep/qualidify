package com.qualidify.service.impl.process.mappers

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.flowable.CmmnCaseDefinitionJpa
import com.qualidify.service.process.dto.CaseDefinitionDto
import org.flowable.cmmn.api.CmmnRepositoryService

@DtoMapper
class CaseDefinitionDtoMapper(
    private val cmmnRepositoryService: CmmnRepositoryService
) : EntityDtoMapper<CmmnCaseDefinitionJpa, CaseDefinitionDto> {

    override fun convertToModel(dto: CaseDefinitionDto): CmmnCaseDefinitionJpa {
        TODO("Not yet implemented")
    }

    override fun convertToDto(entity: CmmnCaseDefinitionJpa): CaseDefinitionDto {
        return CaseDefinitionDto(
            id = entity.id,
            name = entity.name,
            key = entity.key,
            version = entity.version,
            deploymentResourceId = entity.deploymentResource?.deploymentId,
            graphicalNotation = loadImage(entity)
        )
    }

    private fun loadImage(entity: CmmnCaseDefinitionJpa): ByteArray? {
        return if (entity.deploymentResource != null) cmmnRepositoryService.getCaseDiagram(entity.id)
            .readAllBytes() else null
    }

}