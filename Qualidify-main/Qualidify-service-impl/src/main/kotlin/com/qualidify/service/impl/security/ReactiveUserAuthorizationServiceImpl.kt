package com.qualidify.service.impl.security

import com.qualidify.model.entities.User
import com.qualidify.service.impl.user.UserDataRepository
import com.qualidify.service.security.ReactiveUserAuthorizationService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
class ReactiveUserAuthorizationServiceImpl(
    val userRepository: UserDataRepository,
) : ReactiveUserAuthorizationService {

    override fun findByUsername(username: String): Mono<UserDetails> {
        return userRepository.findByUsernameIgnoreCase(username)
            .map { createUserDetails(it) }
            .toMono()
            .flatMap { Mono.justOrEmpty(it) }
    }

    private fun createUserDetails(user: User): UserDetails {
        val roles = user.roles.asSequence().toList().toTypedArray()
        return org.springframework.security.core.userdetails.User.withUsername(user.username)
            .password(user.password)
            .roles(*roles)
            .build()
    }


}