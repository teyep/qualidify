package com.qualidify.service.impl.reports

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.BatchJobArtifact
import com.qualidify.model.entities.BatchJobId
import com.qualidify.service.report.dto.BatchJobArtifactDto

@DtoMapper
class BatchJobArtifactDtoMapper : EntityDtoMapper<BatchJobArtifact, BatchJobArtifactDto>
{
    override fun convertToModel(dto: BatchJobArtifactDto): BatchJobArtifact {
        return BatchJobArtifact(
            BatchJobId(dto.batchJobId),
            dto.version,
            dto.mimeType,
            dto.content,
            dto.finishedDate
        )
    }

    override fun convertToDto(entity: BatchJobArtifact): BatchJobArtifactDto {
        return BatchJobArtifactDto(
            entity.id.unbox(),
            entity.version,
            entity.mimeType,
            entity.content,
            entity.finishedDate
        )
    }
}
