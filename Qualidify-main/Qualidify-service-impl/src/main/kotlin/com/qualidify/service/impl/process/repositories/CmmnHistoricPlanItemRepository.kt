package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.CmmnHistoricPlanItemJpa

/**
 * Read only repository for [CmmnHistoricPlanItemJpa]
 *
 * @author Menno Tol
 * @since 1.0
 */
interface CmmnHistoricPlanItemRepository : ReadOnlyQueryablePagingAndSortingRepository<CmmnHistoricPlanItemJpa, String>