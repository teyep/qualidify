package com.qualidify.service.impl.process

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.flowable.CmmnPlanItemJpa
import com.qualidify.service.impl.process.mappers.PlanItemDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnHistoricPlanItemRepository
import com.qualidify.service.impl.process.repositories.CmmnPlanItemRepository
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemType
import org.flowable.cmmn.api.CmmnHistoryService
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.CmmnEngine
import org.flowable.cmmn.engine.impl.util.CommandContextUtil
import org.flowable.variable.api.history.HistoricVariableInstance
import org.flowable.variable.service.impl.persistence.entity.HistoricVariableInstanceEntityImpl
import org.flowable.variable.service.impl.persistence.entity.VariableInstanceEntityImpl
import org.springframework.stereotype.Service
import java.util.*

/**
 * An implementation of the [PlanItemService], to retrieve the [PlanItemDto] from flowable
 *
 * @author Menno Tol
 * @since 1.0
 */
@Service
open class PlanItemServiceImpl(
    private val cmmnPlanItemRepository: CmmnPlanItemRepository,
    private val cmmnHistoricPlanItemRepository: CmmnHistoricPlanItemRepository,
    private val mapper: PlanItemDtoMapper,
    private val cmmnEngine: CmmnEngine,
    private val cmmnRuntimeService: CmmnRuntimeService,
    private val cmmnHistoryService: CmmnHistoryService,
) : AbstractQueryableService<CmmnPlanItemJpa, PlanItemDto>(cmmnPlanItemRepository), PlanItemService {

    private val logger by LoggerDelegation()

    override fun mapEntityToDto(entity: CmmnPlanItemJpa) = mapper.convertToDto(entity)

    override fun getPlanItemDtoOrNull(planItemId: String): PlanItemDto? =
        cmmnPlanItemRepository.findById(planItemId).map { mapper.convertToDto(it) }.orElse(null)

    override fun getHistoricPlanItemDto(planItemId: String): PlanItemDto =
        cmmnHistoricPlanItemRepository.findById(planItemId)
            .map {
                mapper.convertToDto(it)
            }.orElseThrow {
                PlanItemServiceException("Could not find a historic plan item with id '$planItemId'.")
            }

    override fun getEventListenersByCaseInstanceId(caseInstanceId: String) = cmmnPlanItemRepository
        .findEventListenersByCaseInstanceId(caseInstanceId)
        .map { mapEntityToDto(it) }
        .toMutableList()


    override fun getPlanItemsByStageAndDefinition(planItemDto: PlanItemDto) =
        cmmnPlanItemRepository
            .findAllByStageEqualsAndDefinitionType(planItemDto.stage, PlanItemType.HUMAN_TASK.type)
            .map { mapEntityToDto(it) }
            .toList()

    override fun getUserEventListenersWithStage(stageId: String) = cmmnPlanItemRepository
        .findAllByStageEqualsAndDefinitionType(stageId, PlanItemType.USER_EVENT_LISTENER.type)
        .map { mapEntityToDto(it) }

    override fun getTopLevelUserEventListeners(cmmnInstanceId: String): List<PlanItemDto> = cmmnPlanItemRepository
        .findAllByCaseInstanceIdEqualsAndStageEqualsAndDefinitionType(
            cmmnInstanceId,
            type = PlanItemType.USER_EVENT_LISTENER.type
        )
        .map { mapEntityToDto(it) }

    override fun startPlanItem(dto: PlanItemDto) {
        if (dto.type == PlanItemType.HUMAN_TASK) {
            logger.info("Plan item ${dto.name} was started")
            return cmmnRuntimeService.startPlanItemInstance(dto.id)
        }
        if (dto.type == PlanItemType.USER_EVENT_LISTENER) {
            logger.info("User event ${dto.name} was completed")
            return cmmnRuntimeService.completeUserEventListenerInstance(dto.id)
        } else throw NotImplementedError("Action not implemented on ${dto.type}")
    }

    override fun getStages(caseInstanceId: String?): Collection<PlanItemDto> {
        return if (caseInstanceId == null) {
            cmmnPlanItemRepository.findAllByDefinitionType("stage")
                .map { mapEntityToDto(it) }
        } else {
            cmmnPlanItemRepository.findAllByCaseInstanceIdAndDefinitionType(caseInstanceId, "stage")
                .map { mapEntityToDto(it) }
        }
    }

    override fun hasPlanItemsWithStage(dto: PlanItemDto) = getPlanItemWithStageCount(dto) > 0

    override fun getPlanItemWithStageCount(dto: PlanItemDto) = cmmnPlanItemRepository.countByStageEquals(dto.id)

    override fun getPlanItemsWithStage(dto: PlanItemDto) = cmmnPlanItemRepository.findAllByStageEquals(dto.id)
        .map { mapEntityToDto(it) }

    override fun completeGenericEventListener(dto: PlanItemDto) {
        if (dto.type != PlanItemType.GENERIC_EVENT_LISTENER) {
            throw PlanItemServiceException("Dto [$dto] is not of type '${PlanItemType.GENERIC_EVENT_LISTENER}'.")
        } else cmmnRuntimeService.completeGenericEventListenerInstance(dto.id)
    }

    override fun forceCompleteStage(dto: PlanItemDto, currentUser: String) {
        if (dto.type != PlanItemType.STAGE) {
            throw PlanItemServiceException("Dto [$dto] is not of type '${PlanItemType.STAGE}'.")
        }
        cmmnRuntimeService.completeStagePlanItemInstance(dto.id, true)
        logger.info("Stage ${dto.name} of project ${dto.caseInstanceId} was completed by user '$currentUser'")
    }

    override fun getPlanItemIdsByCase(caseInstanceId: String): List<String> {
        val cmmnPlanItemJpaList = cmmnPlanItemRepository.findAllByCaseInstanceIdEquals(caseInstanceId)
        return cmmnPlanItemJpaList.map { it.id }
    }

    override fun getPlanItemVariables(planItemId: String): Map<String, Any> {
        return cmmnRuntimeService.getLocalVariables(planItemId)
    }

    override fun getPlanItemVariable(planItemId: String, variableName: String): String? =
        cmmnRuntimeService.getLocalVariable(planItemId, variableName) as? String

    override fun getHistoricPlanItemVariable(planItemId: String, variableName: String): HistoricVariableInstance =
        cmmnHistoryService.createHistoricVariableInstanceQuery()
            .planItemInstanceId(planItemId)
            .variableName(variableName)
            .singleResult()

    override fun setPlanItemVariable(planItemId: String, variableName: String, variableValue: String) =
        cmmnRuntimeService.setLocalVariable(planItemId, variableName, variableValue)

    override fun updateHistoricVariable(historicVariable: HistoricVariableInstance, variableValue: String) {

        // cast historic variable as entity impl (so it can be changed)
        historicVariable as HistoricVariableInstanceEntityImpl

        // convert to actual variable instance entity impl and update the review state
        val updatedVariable = VariableInstanceEntityImpl().also {
            it.id = historicVariable.id
            it.name = historicVariable.name
            it.scopeId = historicVariable.scopeId
            it.subScopeId = historicVariable.subScopeId
            it.scopeType = historicVariable.scopeType
            it.textValue = if (historicVariable.textValue != null) variableValue else null
            it.textValue2 = historicVariable.textValue2
            it.doubleValue = historicVariable.doubleValue
            it.longValue = historicVariable.longValue
            it.type = historicVariable.variableType
            it.originalPersistentState = historicVariable.originalPersistentState
        }

        // update the historic variable by executing a recordVariableUpdate command using the command executor
        cmmnEngine.cmmnEngineConfiguration.commandExecutor.execute { commandContext ->
            CommandContextUtil.getHistoricVariableService(commandContext)
                .recordVariableUpdate(updatedVariable, Date())
        }

    }

}

class PlanItemServiceException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)