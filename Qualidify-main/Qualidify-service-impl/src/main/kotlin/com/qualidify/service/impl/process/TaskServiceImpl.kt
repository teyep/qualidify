package com.qualidify.service.impl.process

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.flowable.CmmnTaskJpa
import com.qualidify.service.impl.process.mappers.TaskDtoMapper
import com.qualidify.service.impl.projects.CmmnTaskRepository
import com.qualidify.service.impl.quality.exceptions.UnsupportedReviewStateException
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.exceptions.TaskServiceException
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.ProjectInstanceDto
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.TaskReviewService
import org.flowable.cmmn.api.CmmnTaskService
import org.springframework.stereotype.Service

/**
 * An implementation of the [TaskService], to retrieve the [TaskDto] from Flowable
 *
 * @author Menno Tol
 * @since 1.0
 */
@Service
open class TaskServiceImpl(
    private val projectInstanceService: ProjectInstanceService,
    private val cmmnTaskRepository: CmmnTaskRepository,
    private val flowableTaskService: CmmnTaskService,
    private val mapper: TaskDtoMapper,
    private val taskReviewService: TaskReviewService,
    private val planItemService: PlanItemService,
) : AbstractQueryableService<CmmnTaskJpa, TaskDto>(cmmnTaskRepository), TaskService {

    private val logger by LoggerDelegation()

    override fun mapEntityToDto(entity: CmmnTaskJpa) = mapper.convertToDto(entity)

    override fun isTaskAvailable(taskId: String) = cmmnTaskRepository.existsById(taskId)

    override fun getTaskDtoById(taskId: String) = cmmnTaskRepository.findById(taskId)
        .map { mapEntityToDto(it) }
        .get()

    override fun getTaskDtoByPlanItemId(planItemId: String): TaskDto = cmmnTaskRepository
        .findByPlanItemInstanceId(planItemId)
        .map { mapEntityToDto(it) }
        .get()

    override fun getProjectInstanceDto(projectInstanceId: String): ProjectInstanceDto =
        projectInstanceService.getProjectInstanceById(projectInstanceId)!!

    override fun resolveTask(dto: TaskDto) {
        if (dto.reviewState == ReviewState.UNDEFINED || dto.reviewState == ReviewState.REJECTED || dto.reviewState == ReviewState.CORRECTED) {
            flowableTaskService.resolveTask(dto.id)
        } else {
            throw UnsupportedReviewStateException("Resolve task with review state '${dto.reviewState.state}' is not supported. Allowed review state is '${ReviewState.REJECTED.state}'.")
        }
    }

    override fun completeTask(dto: TaskDto) {
        if (dto.reviewState == ReviewState.APPROVED || dto.reviewState == ReviewState.REVIEW_NOT_REQUIRED) {
            flowableTaskService.complete(dto.id)
        } else {
            throw UnsupportedReviewStateException("Complete task with review state '${dto.reviewState.state}' is not supported. Allowed review states are '${ReviewState.REVIEW_NOT_REQUIRED.state}' and '${ReviewState.APPROVED.state}'.")
        }
    }

    override fun completeOrResolveTask(dto: TaskDto, currentUser: String?) {
        // TODO("Make currentUser non-nullable, see QUAL-483")
        when (dto.reviewState) {
            ReviewState.APPROVED, ReviewState.REVIEW_NOT_REQUIRED -> {
                // these tasks can be completed
                flowableTaskService.complete(dto.id)
                logger.info("Task [${dto.name}], id '${dto.id}' was completed by user $currentUser")
            }

            ReviewState.UNDEFINED, ReviewState.REVIEW_REQUIRED -> {
                // these tasks need to be reviewed, so will be resolved until review is done
                flowableTaskService.resolveTask(dto.id)
                logger.info("Task [${dto.name}], id '${dto.id}' was resolved by user $currentUser and is waiting for review")
                val planItemDto = planItemService.getPlanItemDtoOrNull(dto.planItemInstanceId)!!
                taskReviewService.startReview(planItemDto)
            }

            ReviewState.IN_REVIEW, ReviewState.CORRECTED -> {
                // these tasks should not be changed, under normal conditions completeTask() should not be called
                logger.warn("Task [${dto.name}], id '${dto.id}' is in state '${dto.reviewState}' and should not be editable/completable until review is finished")
            }

            ReviewState.REJECTED -> {
                // these tasks have a corresponding review task, and corrections were made
                taskReviewService.taskCorrected(dto, currentUser!!)
                logger.info("Task [${dto.name}], id '${dto.id}' has been corrected by user '$currentUser' and is ready for re-evaluation\"")
            }
        }


    }

    override fun assignTask(dto: TaskDto, user: String, currentUser: String) {
        when (dto.reviewState) {
            ReviewState.IN_REVIEW -> {
                logger.warn("Task [${dto.name}] with id ${dto.id} is in state '${ReviewState.IN_REVIEW}' and should not be editable/completable until review is finished")
            }

            else -> {
                flowableTaskService.setAssignee(dto.id, user)
                logger.info("Task [${dto.name}] with id ${dto.id} was assigned to $user by user $currentUser")
            }
        }
    }

    /**
     * Claim is intended to be performed on unassigned tasks, by the user that will take responsibility for the task
     */
    override fun claimTask(dto: TaskDto, currentUser: String) {
        if (dto.assignee != null) {
            throw TaskServiceException("Cannot claim task, it was already assigned to '${dto.assignee}'")
        } else {
            when (dto.reviewState) {
                ReviewState.IN_REVIEW -> {
                    logger.warn("Task [${dto.name}] with id ${dto.id} is in state '${ReviewState.IN_REVIEW}' and should not be editable/completable until review is finished")
                }

                else -> {
                    flowableTaskService.claim(dto.id, currentUser)
                    logger.info("User $currentUser claimed task ${dto.name} with id ${dto.id}")
                }
            }
        }
    }

    override fun unclaimTask(dto: TaskDto, currentUser: String) {
        if (dto.assignee != currentUser) {
            throw TaskServiceException("Cannot claim task, it was already assigned to '${dto.assignee}'")
        } else {
            when (dto.reviewState) {
                ReviewState.IN_REVIEW -> {
                    logger.warn("Task [${dto.name}] with id ${dto.id} is in state '${ReviewState.IN_REVIEW}' and should not be editable/completable until review is finished")
                }

                else -> {
                    flowableTaskService.unclaim(dto.id)
                    logger.info("User $currentUser unclaimed task ${dto.name} with id ${dto.id}")
                }
            }
        }
    }

}

