package com.qualidify.service.impl.security

import org.springframework.core.annotation.AnnotationUtils
import org.springframework.security.access.PermissionEvaluator
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import java.io.Serializable
import javax.annotation.security.PermitAll
import javax.annotation.security.RolesAllowed
import kotlin.reflect.KClass

/**
 * A custom role permission Evaluator
 * @author Marcel Pot
 * @since 1.0 [QUAL-313]
 */
interface RolePermissionEvaluator : PermissionEvaluator {
    /**
     * Checks if a class has [RolesAllowed] annotations
     * @param[authentication] the Authentication
     * @param[kClass] the Kotlin class to check
     */
    fun checkClass(authentication: Authentication?, kClass: KClass<*>): Boolean
}


class SecuredRolePermissionEvaluator() : RolePermissionEvaluator {

    override fun checkClass(authentication: Authentication?, kClass: KClass<*>): Boolean {
        if (authentication == null || kClass == null) {
            return false
        }
        return checkPermitAll(kClass.java) || checkRolesAllowed(authentication, kClass.java)
    }

    override fun hasPermission(authentication: Authentication?, targetDomainObject: Any?, permission: Any): Boolean {
        if (authentication == null || targetDomainObject == null || (permission !is String)) {
            return false
        }
        val targetType = targetDomainObject::class.java
        return check(authentication, targetType, permission.toString().uppercase())
    }

    override fun hasPermission(
        authentication: Authentication?,
        targetId: Serializable?,
        targetType: String?,
        permission: Any?
    ): Boolean {
        if (authentication == null || targetType == null || permission !is String) {
            return false
        }
        val targetTypeClass = Class.forName(targetType)
        return check(authentication, targetTypeClass, permission.toString().uppercase())
    }

    private fun check(authentication: Authentication, targetType: Class<*>, permission: String): Boolean {
        return hasPrincipalPermission(authentication, permission)
    }

    private fun checkPermitAll(targetType: Class<*>): Boolean {
        val permitAll = AnnotationUtils.findAnnotation(targetType, PermitAll::class.java)
        return permitAll != null
    }

    private fun checkRolesAllowed(authentication: Authentication, targetType: Class<*>): Boolean {
        val rolesAllowed = AnnotationUtils.findAnnotation(targetType, RolesAllowed::class.java) ?: return true
        return rolesAllowed.value.asSequence()
            .any { hasPrincipalPermission(authentication, it) }
    }

    private fun hasPrincipalPermission(authentication: Authentication, permission: String): Boolean {
        val principal = authentication.principal
        if (principal is UserDetails) {
            val userDetails = principal
            return userDetails.authorities.asSequence()
                .map { it.authority.replace("ROLE_", "") }
                .any { permissionRoleCheck(permission, it) }
        }
        return false
    }

    private fun permissionRoleCheck(permission: String, role: String) = permission.equals(role, true)

}

class AllowEverythingRolePermissionEvaluator() : RolePermissionEvaluator {

    override fun checkClass(authentication: Authentication?, kClass: KClass<*>) = true

    override fun hasPermission(authentication: Authentication?, targetDomainObject: Any?, permission: Any?) = true

    override fun hasPermission(
        authentication: Authentication?,
        targetId: Serializable?,
        targetType: String?,
        permission: Any?
    ) = true

}

