package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.StageReviewService
import org.flowable.cmmn.api.runtime.PlanItemInstance
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

/**
 * Starts the stage review cascade based on a plan item instance obtained from a listener,
 * see [StageCompletedListener].
 *
 * @author Menno Tol
 * @since 1.0
 */
@Service
class StageReviewServiceImpl(
    private val caseInstanceService: CaseInstanceService,
    @Lazy private val planItemService: PlanItemService,
    @Lazy private val taskService: TaskService,
) : StageReviewService {

    private val logger by LoggerDelegation()

    override fun startReview(planItemInstance: PlanItemInstance) {
        val planItemDto = planItemService.getPlanItemDtoOrNull(planItemInstance.id)!!
        startReview(planItemDto)
    }

    override fun startReview(dto: PlanItemDto) {
        if (evaluate(dto)) {
            val reviewStarter = ReviewStarter<PlanItemDto>(caseInstanceService, planItemService)
            reviewStarter.start(dto)
            logger.info("Review cascade for stage '${dto.name}', id '${dto.id}' was started.")
        }
    }

    override fun evaluate(dto: PlanItemDto): Boolean {
        when (dto.type) {
            PlanItemType.GENERIC_EVENT_LISTENER, PlanItemType.SIGNAL_EVENT_LISTENER, PlanItemType.TIMER_EVENT_LISTENER,
            PlanItemType.USER_EVENT_LISTENER, PlanItemType.VARIABLE_EVENT_LISTENER,
            ->
                throw StageReviewServiceException("Dto with id '${dto.id}', type '${dto.type}' is not a stage but an event listener")

            PlanItemType.MILESTONE, PlanItemType.PLAN_FRAGMENT ->
                throw StageReviewServiceException("Dto with id '${dto.id}', type '${dto.type}' is not a stage but a milestone or plan fragment")

            PlanItemType.HUMAN_TASK, PlanItemType.PROCESS_TASK, PlanItemType.CASE_TASK, PlanItemType.CASE_PAGE_TASK,
            PlanItemType.DECISION_TASK, PlanItemType.EXTERNAL_WORKER_TASK, PlanItemType.HTTP_SERVICE_TASK,
            PlanItemType.SERVICE_TASK,
            ->
                throw StageReviewServiceException("Dto with id '${dto.id}', type '${dto.type}' is not a stage but a task")

            else -> {
                // quality items do not need start the review cascade
                if (dto.isQualityShell) return false

                // these review states do not need start the review cascade
                return dto.reviewState !in setOf(
                    ReviewState.REVIEW_NOT_REQUIRED,
                    ReviewState.APPROVED,  // review cascade concluded
                    ReviewState.REJECTED,  // has already a review cascade
                    ReviewState.IN_REVIEW,  // has already a review cascade
                    ReviewState.CORRECTED,  // has already a review cascade
                )
            }
        }
    }

    override fun approveReview(reviewTaskDto: TaskDto, principal: String) {

        // get the corresponding regular stage dto (the id is stored in the local variable of the review task)
        val regularStageDto = planItemService.getHistoricPlanItemDto(reviewTaskDto.qualityLinkId!!)

        // the review state of the review task should be equal to the review state of the regular task
        if (reviewTaskDto.reviewState != regularStageDto.reviewState) {
            throw StageReviewServiceException("Review state of regular stage and corresponding review task are not the same")
        }

        when (reviewTaskDto.reviewState) {
            ReviewState.IN_REVIEW,
            -> {
                // update the review state of the regular and review task
                // (otherwise a new review task might be created or the complete function is not executed)
                val reviewStateUpdater = ReviewStateUpdater<PlanItemDto>(planItemService, caseInstanceService)
                reviewStateUpdater.start(reviewTaskDto, ReviewState.APPROVED)

                // complete the review task (copy the new review state)
                taskService.completeTask(reviewTaskDto.copy(reviewState = ReviewState.APPROVED))
                logger.info("Review task [${reviewTaskDto.name}] was completed by user $principal")
            }

            else -> {
                throw StageReviewServiceException(
                    "Unsupported review state '${reviewTaskDto.reviewState.state}' for stage review approval, " +
                            "the only supported state is '${ReviewState.IN_REVIEW.state}'."
                )
            }

        }


    }

    override fun rejectReview(reviewTaskDto: TaskDto, principal: String) {
        TODO("define how to handle stage rejection (QUAL-493)")
    }

}

class StageReviewServiceException(message: String? = null, cause: Throwable? = null): RuntimeException(message, cause)