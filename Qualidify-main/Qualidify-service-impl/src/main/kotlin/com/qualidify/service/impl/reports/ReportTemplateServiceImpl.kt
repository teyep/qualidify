package com.qualidify.service.impl.reports

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.ReportTemplateId
import com.qualidify.service.report.ReportTemplateService
import com.qualidify.service.report.dto.ReportTemplateDto
import com.qualidify.service.report.exceptions.ReportTemplateNotFoundException
import org.springframework.stereotype.Service
import java.util.*

/**
 * An implementation of ReportTemplateService, for manipulating ReportServiceTemplate objects in the database
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Service
class ReportTemplateServiceImpl(
    private val reportServiceTemplateRepository: ReportServiceTemplateRepository,
    private val reportServiceTemplateDtoMapper: ReportServiceTemplateDtoMapper,
) : ReportTemplateService {

    private fun isSaveableDto(dto: ReportTemplateDto): Boolean {
        return true
    }

    override fun countByQuery(query: CountQuery) = reportServiceTemplateRepository.countResponse(query)


    override fun findByQuery(query: SearchQuery) = reportServiceTemplateRepository.findAllByQuery(query)
        .map { reportServiceTemplateDtoMapper.convertToDto(it) }
        .toList()


    override fun getTemplateById(id: UUID): ReportTemplateDto {
        val result = reportServiceTemplateRepository.findById(ReportTemplateId(id))
        if (result.isPresent) {
            return reportServiceTemplateDtoMapper.convertToDto(result.get())
        } else {
            throw ReportTemplateNotFoundException("Report service template was not found")
        }
    }

    override fun getAllTemplates(): Collection<ReportTemplateDto> {
        return reportServiceTemplateRepository.findAll()
            .map { reportServiceTemplateDtoMapper.convertToDto(it) }
    }

    override fun create(dto: ReportTemplateDto) {
        if (isSaveableDto(dto)) {
            val model = reportServiceTemplateDtoMapper.convertToModel(dto)
            reportServiceTemplateRepository.save(model)
        }
    }

    override fun deleteById(id: UUID) {
        reportServiceTemplateRepository.deleteById(ReportTemplateId(id))
    }
}
