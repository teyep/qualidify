package com.qualidify.service.impl.security

import com.qualidify.service.security.AclRequest
import com.qualidify.service.security.AclRequestService
import com.qualidify.service.security.dto.Permission
import org.springframework.context.annotation.Profile
import org.springframework.security.acls.domain.BasePermission
import org.springframework.security.acls.domain.GrantedAuthoritySid
import org.springframework.security.acls.domain.ObjectIdentityImpl
import org.springframework.security.acls.domain.PrincipalSid
import org.springframework.security.acls.model.*
import org.springframework.stereotype.Service
import java.util.stream.IntStream


@Profile("disabled")
@Service
class AclRequestServiceImpl(private val aclService: MutableAclService) : AclRequestService {

    override fun create(request: AclRequest) {
        val objectIdentity: ObjectIdentity = ObjectIdentityImpl(request.clazz, request.id)
        val acl = getMutableAcl(objectIdentity)
        val permissions: List<Permission> = request.permissions
        val roles: Set<String> = request.roles
        val users: Set<String> = request.users
        val isOwner: Boolean = request.isOwner
        cartesianProductRolesAndPermissions(roles, permissions)
            .forEach {
                acl!!.insertAce(acl.entries.size,
                    map(it.second),
                    it.first,
                    isOwner
                )
            }
        cartesianProductUsersAndPermissions(users, permissions)
            .forEach {
                acl!!.insertAce(acl.entries.size,
                    map(it.second),
                    it.first,
                    isOwner
                )
            }
        aclService.updateAcl(acl)
    }


    override fun update(request: AclRequest) {
        val objectIdentity: ObjectIdentity = ObjectIdentityImpl(request.clazz, request.id)
        val acl = aclService.readAclById(objectIdentity) as MutableAcl

        IntStream.range(0, acl.entries.size).forEach { acl.deleteAce(0) }
        val permissions: List<Permission> = request.permissions
        val roles: Set<String> = request.roles
        val users: Set<String> = request.users
        val isOwner: Boolean = request.isOwner

        cartesianProductRolesAndPermissions(roles, permissions)
            .forEach {
                acl.insertAce(acl.entries.size,
                    map(it.second),
                    it.first,
                    isOwner)
            }
        cartesianProductUsersAndPermissions(users, permissions)
            .forEach {
                acl.insertAce(acl.entries.size,
                    map(it.second),
                    it.first,
                    isOwner)
            }
        aclService.updateAcl(acl)
    }

    private fun map(p: Permission): org.springframework.security.acls.model.Permission? {
        return when (p) {
            Permission.READ -> BasePermission.READ
            Permission.WRITE -> BasePermission.WRITE
            Permission.CREATE -> BasePermission.CREATE
            Permission.DELETE -> BasePermission.DELETE
            Permission.ADMIN -> BasePermission.ADMINISTRATION
            else -> null
        }

    }

    override fun delete(request: AclRequest) {
        val objectIdentity: ObjectIdentity = ObjectIdentityImpl(request.clazz, request.id)
        aclService.deleteAcl(objectIdentity, true)
    }


    private fun getMutableAcl(objectIdentity: ObjectIdentity): MutableAcl? {
        var acl = try {
            aclService.readAclById(objectIdentity) as MutableAcl
        } catch (nfe: NotFoundException) {
            aclService.createAcl(objectIdentity)
        }
        return acl
    }

    private fun cartesianProductRolesAndPermissions(
        roles: Set<String>,
        permissions: List<Permission>,
    ): List<Pair<Sid, Permission>> {
        val product: MutableList<Pair<Sid, Permission>> = ArrayList()
        roles.forEach { sid ->
            permissions.forEach {
                product.add(Pair(GrantedAuthoritySid(sid), it))
            }
        }
        return product
    }

    private fun cartesianProductUsersAndPermissions(
        roles: Set<String>,
        permissions: List<Permission>,
    ): List<Pair<Sid, Permission>> {
        val product: MutableList<Pair<Sid, Permission>> = ArrayList()
        roles.forEach { sid ->
            permissions.forEach {
                product.add(Pair(PrincipalSid(sid), it))
            }
        }
        return product
    }


}