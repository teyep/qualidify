package com.qualidify.service.impl.document

import com.qualidify.model.entities.DocumentContent
import com.qualidify.model.entities.DocumentId
import org.springframework.data.jpa.repository.JpaRepository

/**
 * A repository for retrieving and persisting [DocumentContent] entities in and from a datasource
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface DocumentContentRepository : JpaRepository<DocumentContent, DocumentId> {
}