package com.qualidify.service.impl.quality.exceptions

class UnsupportedQualityLinkKeyException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)
