package com.qualidify.service.impl.security

import com.google.common.base.Preconditions
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.SecurityGroup
import com.qualidify.model.entities.SecurityGroupId
import com.qualidify.model.entities.SecurityRole
import com.qualidify.service.security.SecurityService
import com.qualidify.service.security.dto.SecurityGroupDto
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils.hasText
import java.util.*

/**
 * An implementation of the [SecurityService] interface
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class SecurityServiceImpl(
    private val securityGroupRepository: SecurityGroupRepository,
    private val securityRoleRepository: SecurityRoleRepository,
    private val groupMapper: SecurityGroupMapper,
) : SecurityService {

    override fun createGroup(group: SecurityGroupDto) {
        Preconditions.checkArgument(hasText(group.name), "Security Group Name should be filled.")
        Preconditions.checkArgument(
            hasText(group.description),
            "Security Group description should be filled."
        )
        val optionalExistingGroup = securityGroupRepository.findById(SecurityGroupId(group.name))
        if (optionalExistingGroup.isPresent) {
            throw IllegalArgumentException("Securitygroup ${group.name} already exists!")
        }
        securityGroupRepository.save(groupMapper.convertToModel(group))
    }

    override fun update(group: SecurityGroupDto) {
        Preconditions.checkArgument(hasText(group.name), "Security Group Name should be filled.")
        Preconditions.checkArgument(
            hasText(group.description),
            "Security Group description should be filled."
        )
        val optionalGroup = getSecurityGroupByName(group.name)
        if (optionalGroup.isPresent) {

            val securityRoles = securityRoleRepository.availableRoles()
                .asSequence()
                .filter { group.roles.contains(it) }
                .map { SecurityRole(it) }
                .toSet()

            val groupToUpdate = optionalGroup.get().also {
                it.description = group.description
                it.enabled = group.enabled
                it.roles = securityRoles
            }

            securityGroupRepository.save(groupToUpdate)
        } else {
            throw SecurityGroupNotFoundException("Cannot find the SecurityGroup ${group.name}")
        }
    }

    override fun availableRoles() = securityRoleRepository.availableRoles()

    override fun countByQuery(query: CountQuery) = securityGroupRepository.countResponse(query)

    override fun findByQuery(query: SearchQuery): Collection<SecurityGroupDto> {
        val groups: Page<SecurityGroup> = securityGroupRepository.findAllByQuery(query)
        return groups.asSequence()
            .map { groupMapper.convertToDto(it) }
            .toList()
    }

    override fun findSecurityGroupByName(groupName: String): Optional<SecurityGroupDto> {
        val id = SecurityGroupId(groupName)
        return securityGroupRepository.findById(id)
            .map { groupMapper.convertToDto(it) }
    }


    override fun findSecurityGroupsByNames(groupNames: Collection<String>): Collection<SecurityGroupDto> {
        val groups = securityGroupRepository.findAllById(
            groupNames.asSequence()
                .map { SecurityGroupId(it) }
                .toSet()
        )
        return groups.asSequence()
            .map { groupMapper.convertToDto(it) }
            .toSet()
    }

    private fun getSecurityGroupByName(groupName: String): Optional<SecurityGroup> {
        return securityGroupRepository.findById(SecurityGroupId(groupName))
    }

}

class SecurityGroupNotFoundException(message: String? = null, throwable: Throwable? = null) :
    RuntimeException(message, throwable)
