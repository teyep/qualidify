package com.qualidify.service.impl.projects

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.metadata.DataType
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.model.entities.ProjectTemplateId
import com.qualidify.service.impl.process.mappers.ProjectTemplateDtoMapper
import com.qualidify.service.projects.ProjectTemplateService
import com.qualidify.service.projects.dto.ProjectTemplateDto
import com.qualidify.service.projects.exceptions.ProjectTemplateNotFoundException
import org.springframework.stereotype.Service
import java.util.*

/**
 * An implementation of the [ProjectTemplateService] interface,
 * to retrieve the [ProjectTemplateDto] from Flowable
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Service
class ProjectTemplateServiceImpl(
    private val projectTemplateRepository: ProjectTemplateRepository,
    private val mapper: ProjectTemplateDtoMapper,
    private val projectInstanceStarter: ProjectInstanceStarter,
) : ProjectTemplateService {

    val logger by LoggerDelegation()

    override fun countByQuery(query: CountQuery) = projectTemplateRepository.countResponse(query)

    override fun findByQuery(query: SearchQuery): Collection<ProjectTemplateDto> {
        return projectTemplateRepository.findAllByQuery(query)
            .map { mapper.convertToDto(it) }
            .toList()
    }

    override fun startProject(dto: ProjectTemplateDto): String {
        logger.info("A new project was started with name '${dto.name}' and id '${dto.id}'")
        return projectInstanceStarter.start(dto).id.unbox().toString()
    }

    override fun getColumnDefinitionsForProjectInfo(dto: ProjectTemplateDto): List<ColumnDef> {
        return listOf(
            ColumnDef(
                name = "name",
                dataType = DataType.STRING,
                attributes = mapOf(
                    "placeholder" to dto.name,
                    "isRequired" to true.toString(),
                    "minLength" to "5"

                )
            ),
            ColumnDef(
                name = "description",
                dataType = DataType.TEXT,
                attributes = mapOf(
                    "placeholder" to dto.name,
                    "isRequired" to true.toString(),
                    "minLength" to "8"
                ),
            )
        )
    }

    override fun getColumnDefinitionsForProjectVariables(dto: ProjectTemplateDto): List<ColumnDef> {
        return dto.variables.asSequence().map { mapToColumnDefinition(it) }.toList()
    }

    override fun getProjectTemplateByIdentity(id: UUID): ProjectTemplateDto {
        return projectTemplateRepository.findById(ProjectTemplateId(id))
            .map { mapper.convertToDto(it) }
            .orElseThrow { ProjectTemplateNotFoundException("Could not find the projectTemplate with id = $id.") }
    }

    private fun mapToColumnDefinition(projectVariable: ProjectTemplateDto.ProjectTemplateVariableDto): ColumnDef {
        return ColumnDef(
            name = projectVariable.name,
            description = projectVariable.description,
            dataType = projectVariable.datatype,
            fieldAccess = ColumnDef.FieldAccess.MAP,
            attributes = mapOf(
                "label" to projectVariable.name,
                "placeholder" to if (projectVariable.value != null) projectVariable.value.toString() else "",
                "isRequired" to projectVariable.required.toString(),
            )
        )
    }

}

