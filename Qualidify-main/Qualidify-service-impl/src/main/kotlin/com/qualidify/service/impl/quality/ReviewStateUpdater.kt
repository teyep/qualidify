package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.exceptions.CaseInstanceNotFoundException
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.HasQuality
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import org.flowable.common.engine.api.FlowableObjectNotFoundException

/**
 * Disposable ReviewStateUpdater class taking care of updating review state variable in runtime or historic tables.
 *
 * @author Menno Tol
 * @since 1.0
 */
class ReviewStateUpdater<D : HasQuality>(
    private val planItemService: PlanItemService,
    private val caseInstanceService: CaseInstanceService,
) {

    private val logger by LoggerDelegation()

    private lateinit var regularItem: D
    private lateinit var reviewTaskPlanItem: PlanItemDto
    private lateinit var newReviewState: ReviewState

    fun start(
        regularItemDto: D,
        newReviewState: ReviewState,
        reviewTaskPlanItem: PlanItemDto? = null,
    ) {
        this.regularItem = regularItemDto
        this.newReviewState = newReviewState
        this.reviewTaskPlanItem = reviewTaskPlanItem ?: getReviewTaskPlanItem(regularItem.qualityLinkId!!)

        update()
    }

    fun start(
        taskDto: TaskDto,
        newReviewState: ReviewState,
    ) {
        // if isQualityShell = true, this is a review task
        if (taskDto.isQualityShell) {
            this.regularItem = getRegularItem(taskDto)
            this.newReviewState = newReviewState
            this.reviewTaskPlanItem = getReviewTaskPlanItem(taskDto.planItemInstanceId)
        }

        // if the isQualityShell is false, this is the regular item
        else {
            @Suppress("UNCHECKED_CAST")
            this.regularItem = taskDto as D
            this.newReviewState = newReviewState
            this.reviewTaskPlanItem = getReviewTaskPlanItem(taskDto.qualityLinkId!!)
        }

        update()
    }

    private fun getReviewTaskPlanItem(planItemId: String): PlanItemDto {
        return planItemService.getPlanItemDtoOrNull(planItemId)!!
    }

    @Suppress("UNCHECKED_CAST")
    private fun getRegularItem(reviewTaskDto: TaskDto): D {
        return when (reviewTaskDto.qualityLinkKey) {
            QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID -> {
                planItemService.getPlanItemDtoOrNull(reviewTaskDto.qualityLinkId!!) as D
            }

            QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID -> {
                planItemService.getPlanItemDtoOrNull(reviewTaskDto.qualityLinkId!!) as D
            }

            QualityLinkKey.REGULAR_CASE_ID -> {
                try {
                    caseInstanceService.getCaseInstanceDtoById(reviewTaskDto.qualityLinkId!!)
                } catch (exception: CaseInstanceNotFoundException) {
                    caseInstanceService.getHistoricCaseInstanceDtoById(reviewTaskDto.qualityLinkId!!)
                } as D
            }

            else -> throw ReviewStateUpdaterException("Update of review states not implemented for qualityLinkKey '${reviewTaskDto.qualityLinkKey}'.")
        }
    }

    // updates the ReviewState variable of the regular item and the review task
    private fun update() {
        // first try the runtime tables, if the instance has been removed from the runtime tables,
        // try the historic tables
        try {
            updateRuntimeVariables()
            logger.info("Runtime variable 'reviewState' of regular item '${regularItem.id}' was updated to '${ReviewState.APPROVED.state}'")
        } catch (exception: FlowableObjectNotFoundException) {
            logger.info("Could not update runtime variable 'reviewState' of regular item '${regularItem.id}', " + "trying to update the historic variable")
            updateHistoricVariables()
            logger.info("Historic variable 'reviewState' of regular item '${regularItem.id}' was updated to '${ReviewState.APPROVED.state}'")
        }
    }

    private fun updateRuntimeVariables() {
        when (regularItem) {
            is TaskDto -> {
                setPlanItemReviewState((regularItem as TaskDto).planItemInstanceId, newReviewState)
                setPlanItemReviewState(reviewTaskPlanItem.id, newReviewState)
            }

            is PlanItemDto -> {
                setPlanItemReviewState(regularItem.id, newReviewState)
                setPlanItemReviewState(reviewTaskPlanItem.id, newReviewState)
            }

            is CaseInstanceDto -> {
                setCaseInstanceReviewState(regularItem.id, newReviewState)
                setPlanItemReviewState(reviewTaskPlanItem.id, newReviewState)
            }

            else -> throw ReviewStateUpdaterException("Review for type '${regularItem::class}' is not implemented.")
        }
    }

    private fun updateHistoricVariables() {
        // set the review state for the review task (not historic!)
        setPlanItemReviewState(reviewTaskPlanItem.id, newReviewState)

        // get the historic variable
        when (regularItem) {
            is TaskDto -> {
                val historicVariable = planItemService.getHistoricPlanItemVariable(
                    (regularItem as TaskDto).planItemInstanceId,
                    "reviewState"
                )
                planItemService.updateHistoricVariable(historicVariable, newReviewState.state)
            }

            is PlanItemDto -> {
                val historicVariable = planItemService.getHistoricPlanItemVariable(
                    regularItem.id,
                    "reviewState"
                )
                planItemService.updateHistoricVariable(historicVariable, newReviewState.state)
            }

            is CaseInstanceDto -> {
                val historicVariable = caseInstanceService.getHistoricCaseVariable(
                    regularItem.id,
                    "reviewState"
                )
                caseInstanceService.updateHistoricVariable(historicVariable, newReviewState.state)
            }
        }

    }

    private fun setCaseInstanceReviewState(caseInstanceId: String, reviewState: ReviewState) =
        caseInstanceService.setCaseVariable(caseInstanceId, "reviewState", reviewState.state)

    private fun setPlanItemReviewState(planItemId: String, reviewState: ReviewState) =
        planItemService.setPlanItemVariable(planItemId, "reviewState", reviewState.state)

}

class ReviewStateUpdaterException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)
