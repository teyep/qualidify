package com.qualidify.service.impl.storage

import com.qualidify.model.entities.TempFileStorage
import com.qualidify.model.entities.TempFileStorageId
import com.qualidify.service.storage.StorageService
import com.qualidify.service.storage.exceptions.StorageFileException
import org.springframework.core.io.Resource
import org.springframework.security.util.InMemoryResource
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.io.InputStream

@Service
open class TemporaryFileStorageServiceImpl(
    val tempFileStorageRepository: TempFileStorageRepository
) : StorageService {

    override fun store(file: MultipartFile): String {
        val bytes = file.bytes
        val hash = HashUtils.md5(bytes)
        val temporaryFile = TempFileStorage(hash, bytes)

        tempFileStorageRepository.save(temporaryFile)
        return hash;
    }

    override fun load(identifier: String): InputStream {
        val file = tempFileStorageRepository.findById(TempFileStorageId(identifier))
        return file.map { ByteArrayInputStream(it.content) }
            .orElseThrow { StorageFileException("Could not find file with hash $identifier.") }
    }

    override fun loadAsResource(identifier: String): Resource {
        val file = tempFileStorageRepository.findById(TempFileStorageId(identifier))
        return InMemoryResource(file.map { it.content }
            .orElseThrow { StorageFileException("Could not find file with hash $identifier.") }
        )
    }


}