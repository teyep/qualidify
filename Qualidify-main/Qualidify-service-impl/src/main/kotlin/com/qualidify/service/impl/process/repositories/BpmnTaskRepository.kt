package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.BpmnTaskJpa


/**
 * Read only repository fo [BpmnTaskJpa]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface BpmnTaskRepository : ReadOnlyQueryablePagingAndSortingRepository<BpmnTaskJpa, String>