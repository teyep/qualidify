package com.qualidify.service.impl.reports

import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.ReportTemplate
import com.qualidify.model.entities.ReportTemplateId

interface ReportServiceTemplateRepository : QueryablePagingAndSortingRepository<ReportTemplate, ReportTemplateId>
