package com.qualidify.service.impl.process.mappers

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.ProjectInstance
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.impl.process.repositories.CmmnHistoricCaseInstanceRepository
import com.qualidify.service.impl.projects.QualityShellNotFoundException
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.exceptions.CaseInstanceNotFoundException
import com.qualidify.service.projects.dto.ProjectInstanceDto

@DtoMapper
class ProjectInstanceDtoMapper(
    private val cmmnCaseInstanceRepository: CmmnCaseInstanceRepository,
    private val caseInstanceDtoMapper: CaseInstanceDtoMapper,
    private val cmmnHistoricCaseInstanceRepository: CmmnHistoricCaseInstanceRepository,
) : EntityDtoMapper<ProjectInstance, ProjectInstanceDto> {

    override fun convertToModel(dto: ProjectInstanceDto): ProjectInstance {
        TODO("Not yet implemented")
    }

    override fun convertToDto(entity: ProjectInstance): ProjectInstanceDto {
        val caseInstanceDto = getCaseInstanceDto(entity, quality = false)
        val qualityShellInstanceDto = getCaseInstanceDto(entity, quality = true)

        return ProjectInstanceDto(
            id = entity.id.unbox().toString(),
            name = entity.name,
            description = entity.description,
            qualityShellId = entity.qualityShellId,
            qualityShellInstanceDto = qualityShellInstanceDto,
            qualityShellInstanceState = qualityShellInstanceDto.state,
            regularCaseId = entity.regularCaseId,
            regularCaseInstanceDto = caseInstanceDto,
            regularCaseInstanceState = caseInstanceDto.state,
            startTime = entity.startTime,
            fields = entity.customFields.asSequence()
                .map { it.field to it.value }
                .toMap().toMutableMap(),
            variables = entity.variables.asSequence()
                .map {
                    ProjectInstanceDto.ProjectInstanceVariableDto(
                        name = it.name,
                        datatype = it.datatype,
                        value = it.value
                    )
                }
                .toList(),
            users = entity.users.asSequence()
                .map { it.username }
                .toList(),
            state = entity.state,
        )
    }

    private fun getCaseInstanceDto(entity: ProjectInstance, quality: Boolean = false): CaseInstanceDto {
        val caseInstance = if (quality) {
            cmmnCaseInstanceRepository.findById(entity.qualityShellId)
        } else {
            cmmnCaseInstanceRepository.findById(entity.regularCaseId)
        }

        val historicCaseInstance = if (quality) {
            cmmnHistoricCaseInstanceRepository.findById(entity.qualityShellId)
        } else {
            cmmnHistoricCaseInstanceRepository.findById(entity.regularCaseId)
        }

        return when {
            caseInstance.isPresent ->
                caseInstanceDtoMapper.convertToDto(caseInstance.get())

            caseInstance.isEmpty && historicCaseInstance.isPresent ->
                caseInstanceDtoMapper.convertToDto(historicCaseInstance.get())

            else ->
                if (quality) {
                    throw QualityShellNotFoundException(
                        "Could not find a (historic) quality shell instance with id '${entity.qualityShellId}'"
                    )
                } else {
                    throw CaseInstanceNotFoundException(
                        "Could not find a (historic) case instance with id '${entity.regularCaseId}'"
                    )
                }
        }

    }

}