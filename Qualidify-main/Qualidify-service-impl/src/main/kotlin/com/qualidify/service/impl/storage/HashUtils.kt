package com.qualidify.service.impl.storage

import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

/**
 * A helper class for centralized hashing of files
 *
 * @author Marcel Pot
 */
object HashUtils {

    /**
     * Create a md5-hash of a ByteArray
     * @param bytes a ByteArray containing the content of the file to hash
     * @return a string with the Hash
     */
    fun md5(bytes: ByteArray): String {
        val md: MessageDigest = MessageDigest.getInstance("MD5")
        md.update(bytes)
        val digest: ByteArray = md.digest()
        return DatatypeConverter.printHexBinary(digest).uppercase()
    }
}