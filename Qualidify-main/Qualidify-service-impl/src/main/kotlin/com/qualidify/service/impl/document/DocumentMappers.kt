package com.qualidify.service.impl.document

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.*
import com.qualidify.service.document.dto.DocumentContentDto
import com.qualidify.service.document.dto.DocumentDto
import com.qualidify.service.document.dto.LabelDto

@DtoMapper
open class DocumentMapper : EntityDtoMapper<Document, DocumentDto> {
    override fun convertToModel(dto: DocumentDto) =
        Document(
            id = DocumentId(dto.documentId),
            path = dto.path,
            name = dto.name,
            type = dto.type,
            size = dto.size,
            version = dto.version,
            attributes = dto.attributes.asSequence()
                .map { DocumentAttribute(it.key, it.value) }
                .toSet(),
            labels = dto.labels.asSequence()
                .map { Label(it.name) }
                .toList()
        )


    override fun convertToDto(entity: Document) = DocumentDto(
        documentId = entity.id.unbox(),
        path = entity.path,
        name = entity.name,
        type = entity.type,
        size = entity.size,
        version = entity.version,
        attributes = entity.attributes.asSequence()
            .map { it.key to it.value }
            .toMap(),
        labels = entity.labels.asSequence()
            .map { LabelDto(it.id.unbox()) }
            .toList().toTypedArray()
    )

}

@DtoMapper
open class DocumentContentMapper : EntityDtoMapper<DocumentContent, DocumentContentDto> {
    override fun convertToModel(dto: DocumentContentDto) = DocumentContent(
        id = DocumentId(dto.documentId),
        content = dto.content.copyOf()
    )

    override fun convertToDto(entity: DocumentContent) = DocumentContentDto(
        documentId = entity.id.unbox(),
        content = entity.content.copyOf()
    )
}

@DtoMapper
open class LabelMapper : EntityDtoMapper<Label, LabelDto> {
    override fun convertToModel(dto: LabelDto) = Label(dto.name)

    override fun convertToDto(entity: Label) = LabelDto(entity.id.unbox())
}