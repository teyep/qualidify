package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.CmmnPlanItemJpa
import com.qualidify.service.process.dto.PlanItemType


/**
 * Read only repository for [CmmnPlanItemJpa]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface CmmnPlanItemRepository : ReadOnlyQueryablePagingAndSortingRepository<CmmnPlanItemJpa, String> {

    /**
     * Find all plan items based on a definition type
     * @param[type] the type of the plan items to look for
     * @return a collection of plan items with requested type
     */
    fun findAllByDefinitionType(type: String): List<CmmnPlanItemJpa>

    /**
     * Find all plan items based on a case instance id and definition type
     * @param [caseInstanceId] the caseInstance
     * @param [type] the type of the plan items to look for
     * @return a collection of plan items with requested type belonging to a case instance
     */
    fun findAllByCaseInstanceIdAndDefinitionType(caseInstanceId: String, type: String): List<CmmnPlanItemJpa>

    /**
     * Count all plan items with stage
     * @param [stage] the requested stage id
     * @return the number of plan items with stage id
     */
    fun countByStageEquals(stageId: String): Long

    /**
     * Find all plan items with stage id
     * @param [stageId] the requested stage
     * @return a collection of plan items with the stage id
     */
    fun findAllByStageEquals(stageId: String): List<CmmnPlanItemJpa>

    /**
     * Find all plan items with stageId based on a type
     * @param [stageId] the requested stage or null for top level events
     * @param [type] the type of the plan items to look for
     * @return a collection of plan items with requested type belonging to a stage
     */
    fun findAllByStageEqualsAndDefinitionType(stageId: String?, type: String): List<CmmnPlanItemJpa>

    /**
     * Find all plan items with caseInstanceId and stageId based on a type
     * @param [caseInstanceId] the requested case instance id
     * @param [stageId] the requested stage or null for top level events
     * @param [type] the type of the plan items to look for
     * @return a collection of plan items with requested type belonging to a stage
     */
    fun findAllByCaseInstanceIdEqualsAndStageEqualsAndDefinitionType(
        caseInstanceId: String, stageId: String? = null, type: String,
    ): List<CmmnPlanItemJpa>

    /**
     * Find all plan items with case instance id and type
     * @param [caseInstanceId] the requested case instance id
     * @param [type] the requested [PlanItemType]
     * @return a collection of plan items with case instance id and type
     */
    fun findAllByCaseInstanceIdEqualsAndDefinitionTypeEquals(
        caseInstanceId: String,
        type: String,
    ): List<CmmnPlanItemJpa>

    /**
     * Finds all generic event listeners by case instance id
     * @param [caseInstanceId] the requested case instance id
     * @return a collection of event listener with case instance id
     */
    fun findEventListenersByCaseInstanceId(caseInstanceId: String) =
        findAllByCaseInstanceIdEqualsAndDefinitionTypeEquals(caseInstanceId, PlanItemType.GENERIC_EVENT_LISTENER.type)

    /**
     * Find all plan item instances by case instance id
     * @param caseInstanceId the id of the case instance
     * @return a collection of plan item instances with the case instance id
     */
    fun findAllByCaseInstanceIdEquals(caseInstanceId: String): List<CmmnPlanItemJpa>

}
