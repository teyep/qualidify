package com.qualidify.service.impl.configuration

import com.qualidify.core.jpa.annotations.SoftDelete
import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.ConfigurationDomain
import com.qualidify.model.entities.ConfigurationDomainId

/**
 * Repository for retrieving and persisting [ConfigurationDomain]'s to a datasource
 * @author Marcel Pot
 * @since 1.0
 */
@SoftDelete
interface ConfigurationDomainRepository :
    QueryablePagingAndSortingRepository<ConfigurationDomain, ConfigurationDomainId>