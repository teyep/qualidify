package com.qualidify.service.impl.document

import com.qualidify.model.entities.Label
import com.qualidify.model.entities.LabelId
import org.springframework.data.repository.PagingAndSortingRepository

/**
 * A repository for retrieving and persisting [Label] entities in and from a datasource
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface LabelRepository : PagingAndSortingRepository<Label, LabelId> {
}