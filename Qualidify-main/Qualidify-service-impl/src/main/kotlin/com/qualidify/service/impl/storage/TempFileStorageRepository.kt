package com.qualidify.service.impl.storage

import com.qualidify.model.entities.TempFileStorage
import com.qualidify.model.entities.TempFileStorageId
import org.springframework.data.jpa.repository.JpaRepository

interface TempFileStorageRepository : JpaRepository<TempFileStorage, TempFileStorageId> {
}