package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.*
import org.springframework.stereotype.Component

/**
 * Routes the task actions (complete, approve, reject) to the correct service.
 * All task complete actions should pass via this service, to prevent inadvertently bypassing the quality cascade that
 * should be started.
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
class ReviewResponseRouterImpl(
    private val taskReviewService: TaskReviewService,
    private val stageReviewService: StageReviewService,
    private val caseReviewService: CaseReviewService,
) : ReviewResponseRouter {

    val logger by LoggerDelegation()

    override fun routeRequest(qualityTaskDto: TaskDto, principal: String, response: ReviewResponse) {

        when (val pair = Pair(response, qualityTaskDto.qualityLinkKey)) {
            Pair(ReviewResponse.APPROVE, QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID) ->
                return taskReviewService.approveReview(qualityTaskDto, principal)

            Pair(ReviewResponse.REJECT, QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID) ->
                return taskReviewService.rejectReview(qualityTaskDto, principal)

            Pair(ReviewResponse.APPROVE, QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID) ->
                return stageReviewService.approveReview(qualityTaskDto, principal)
            Pair(ReviewResponse.REJECT, QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID) ->
                return stageReviewService.rejectReview(qualityTaskDto, principal)

            Pair(ReviewResponse.APPROVE, QualityLinkKey.REGULAR_CASE_ID) ->
                return caseReviewService.approveReview(qualityTaskDto, principal)
            Pair(ReviewResponse.REJECT, QualityLinkKey.REGULAR_CASE_ID) ->
                return caseReviewService.rejectReview(qualityTaskDto, principal)


            else -> {
                throw ReviewResponseRouterException(
                    "Could not find key '${pair.second}' in the local variable of plan item id '${qualityTaskDto.planItemInstanceId}'"
                )
            }

        }

    }
}

class ReviewResponseRouterException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)
