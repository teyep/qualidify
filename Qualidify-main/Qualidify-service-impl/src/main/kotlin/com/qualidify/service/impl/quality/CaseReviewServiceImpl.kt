package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.projects.TaskService
import com.qualidify.service.projects.dto.TaskDto
import com.qualidify.service.quality.CaseReviewService
import com.qualidify.service.quality.ReviewState
import org.flowable.cmmn.api.runtime.CaseInstance
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

/**
 * Starts the case review cascade based on a case instance obtained from a listener,
 * see [CaseCompletedListener].
 *
 * @author Menno Tol
 * @since 1.0
 */
@Service
class CaseReviewServiceImpl(
    private val caseInstanceService: CaseInstanceService,
    @Lazy private val planItemService: PlanItemService,
    @Lazy private val taskService: TaskService,
) : CaseReviewService {

    private val logger by LoggerDelegation()

    override fun startReview(caseInstance: CaseInstance) {
        val caseInstanceDto = caseInstanceService.getCaseInstanceDtoById(caseInstance.id)
        startReview(caseInstanceDto)
    }

    override fun startReview(dto: CaseInstanceDto) {
        if (evaluate(dto)) {
            val reviewStarter = ReviewStarter<CaseInstanceDto>(caseInstanceService, planItemService)
            reviewStarter.start(dto)
            logger.info("Review cascade for case '${dto.name}', id '${dto.id}' was started.")
        }
    }

    override fun evaluate(dto: CaseInstanceDto): Boolean {
        // quality items do not need start the review cascade
        if (dto.isQualityShell) return false

        // these review states do not need start the review cascade
        return dto.reviewState !in setOf(
            ReviewState.REVIEW_NOT_REQUIRED,
            ReviewState.APPROVED,  // review cascade concluded
            ReviewState.REJECTED,  // has already a review cascade
            ReviewState.IN_REVIEW,  // has already a review cascade
            ReviewState.CORRECTED,  // has already a review cascade
        )
    }

    override fun approveReview(reviewTaskDto: TaskDto, principal: String) {

        // get the corresponding regular case instance (the id is stored in the local variable of the review task)
        val regularCaseDto = caseInstanceService.getHistoricCaseInstanceDtoById(reviewTaskDto.qualityLinkId!!)

        // the review state of the review task should be equal to the review state of the regular item
        if (reviewTaskDto.reviewState != regularCaseDto.reviewState) {
            throw CaseReviewServiceException("Review state of regular case and corresponding review task are not the same")
        }

        when (reviewTaskDto.reviewState) {
            ReviewState.IN_REVIEW,
            -> {
                // update the review state of the regular case and review task
                // (otherwise a new review task might be created or the complete function is not executed)
                val reviewStateUpdater = ReviewStateUpdater<CaseInstanceDto>(planItemService, caseInstanceService)
                reviewStateUpdater.start(reviewTaskDto, ReviewState.APPROVED)

                // complete the review task (copy the new review state)
                taskService.completeTask(reviewTaskDto.copy(reviewState = ReviewState.APPROVED))
                logger.info("Review task [${reviewTaskDto.name}] was completed by user $principal")
            }

            else -> {
                throw CaseReviewServiceException(
                    "Unsupported review state '${reviewTaskDto.reviewState.state}' for case review approval, " +
                            "the only supported state is '${ReviewState.IN_REVIEW.state}'."
                )
            }
        }
    }

    override fun rejectReview(reviewTaskDto: TaskDto, principal: String) {
        TODO("Not yet implemented")
    }

}

class CaseReviewServiceException(message: String? = null, cause: Throwable? = null): RuntimeException(message, cause)