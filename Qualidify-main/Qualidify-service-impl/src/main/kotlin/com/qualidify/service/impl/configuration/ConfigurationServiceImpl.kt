package com.qualidify.service.impl.configuration

import com.google.common.base.Preconditions
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.ConfigurationDomain
import com.qualidify.model.entities.ConfigurationDomainId
import com.qualidify.service.configuration.ConfigurationDomainNotFoundException
import com.qualidify.service.configuration.ConfigurationService
import com.qualidify.service.configuration.dto.ConfigurationCodeDto
import com.qualidify.service.configuration.dto.ConfigurationDomainDto
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils

/**
 * An implementation of the [ConfigurationService]
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class ConfigurationServiceImpl(
    val configurationDomainRepository: ConfigurationDomainRepository,
    val mapper: ConfigurationCodeDtoMapper,
) : ConfigurationService {

    override fun create(domain: ConfigurationDomainDto) {
        Preconditions.checkArgument(StringUtils.hasText(domain.domain), "Configuration Domain must have a name.")
        Preconditions.checkArgument(
            StringUtils.hasText(domain.description),
            "Configuration Domain must have a description."
        )
        val configurationDomain = ConfigurationDomain(domain.domain, domain.description)
        configurationDomainRepository.save(configurationDomain)
    }

    override fun update(domain: ConfigurationDomainDto) {
        create(domain)
    }

    override fun delete(domain: ConfigurationDomainDto) {
        configurationDomainRepository.deleteById(ConfigurationDomainId(domain.domain))
    }

    override fun createCode(code: ConfigurationCodeDto) {
        Preconditions.checkArgument(
            StringUtils.hasText(code.domain),
            "Configuration code must have a configurationDomain."
        )
        Preconditions.checkArgument(StringUtils.hasText(code.code), "Configuration code must have a value.")
        Preconditions.checkArgument(
            StringUtils.hasText(code.description),
            "Configuration code must have a description."
        )

        val optionalConfigurationDomain = configurationDomainRepository.findById(ConfigurationDomainId(code.domain))
        val configurationDomain = optionalConfigurationDomain
            .orElseThrow { ConfigurationDomainNotFoundException("The configurationDomain ${code.domain} cannot be found!") }

        val optionalConfigurationCode = configurationDomain.getByCode(code.code)
        if (optionalConfigurationCode.isPresent) {
            //update
            val configurationCode = optionalConfigurationCode.get()
            configurationCode.description = code.description
            configurationCode.value = code.value
            configurationCode.active = code.active
            configurationCode.dataType = code.dataType
        } else {
            val configurationCode = mapper.convertToModel(code)
            configurationDomain.codes.add(configurationCode)
        }
        configurationDomainRepository.save(configurationDomain)
    }

    override fun updateCode(code: ConfigurationCodeDto) {
        createCode(code)
    }

    override fun countByQuery(query: CountQuery) = configurationDomainRepository.countResponse(query)

    override fun findByQuery(query: SearchQuery) = configurationDomainRepository.findAllByQuery(query)
        .asSequence()
        .map { domain -> mapper.toDto(domain) }
        .flatMap { it.asSequence() }
        .toList()

}