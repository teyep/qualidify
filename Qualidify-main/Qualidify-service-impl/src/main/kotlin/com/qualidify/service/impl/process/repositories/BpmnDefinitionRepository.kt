package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.BpmnDefinitionJpa

/**
 * Read only repository fo [BpmnDefinitionJpa]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface BpmnDefinitionRepository : ReadOnlyQueryablePagingAndSortingRepository<BpmnDefinitionJpa, String> {
}