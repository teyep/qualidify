package com.qualidify.service.impl.security

import com.qualidify.service.security.roles.Roles
import org.springframework.stereotype.Component
import kotlin.reflect.KClass
import kotlin.reflect.full.memberProperties

/**
 * Repository for SecurityRoles
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
class SecurityRoleRepository {

    val roles: MutableList<String> by lazy { read() }

    private fun read(): MutableList<String> {
        val foundRoles: MutableList<String> = mutableListOf()
        readConstants(Roles::class, foundRoles)
        return foundRoles;
    }

    private fun readConstants(kClass: KClass<out Any>, foundRoles: MutableList<String>) {
        kClass.memberProperties.filter { it.isConst && it.name != "INSTANCE" }
            .forEach { foundRoles.add(it.getter.call() as String) }
        kClass.nestedClasses.forEach { readConstants(it, foundRoles) }
    }

    fun availableRoles(): List<String> = roles.toList()
}