package com.qualidify.service.impl.user

import com.google.common.base.Preconditions
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.MetadataValidationService
import com.qualidify.model.entities.SecurityGroupId
import com.qualidify.model.entities.User
import com.qualidify.model.entities.UserId
import com.qualidify.service.impl.security.SecurityGroupRepository
import com.qualidify.service.user.UserService
import com.qualidify.service.user.dto.CreateUserCommand
import com.qualidify.service.user.dto.UserDto
import com.qualidify.service.user.exceptions.UserNotFoundException
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import java.util.*

/**
 * An implementation of the [UserService] interface
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class UserServiceImpl(
    private val userDataRepository: UserDataRepository,
    private val securityGroupRepository: SecurityGroupRepository,
    private val metadataValidationService: MetadataValidationService,
    private val dtoMapper: UserMapper,
) : UserService {

    override fun countByQuery(query: CountQuery) = userDataRepository.countResponse(query)

    override fun findByQuery(query: SearchQuery): Collection<UserDto> {
        val users: Iterable<User> = userDataRepository.findAllByQuery(query)
        return users.asSequence()
            .map { internalDtoMapper(it) }
            .toList()
    }

    override fun create(command: CreateUserCommand): UserDto {
        validateUser(command.userName, command.password, command.email)
        validateUserFields(command.fields)

        val newUser = User.builder()
            .withUserId(UUID.randomUUID())
            .withUsername(command.userName)
            .withEmail(command.email)
            .withPassword(command.password)
            .withEnabled(true)
            .withNonExpired(true)
            .withAccountNonLocked(true)
            .withCredentialsNonExpired(true)
            .withFields(UserFieldMapper.INSTANCE.convertToModel(command.fields))
            .build()

        val createdUser = linkGroupsToUser(newUser, command.securityGroups)
        return dtoMapper.convertToDto(createdUser)
    }

    override fun update(user: UserDto): UserDto {
        validateUser(user.username, user.email)
        validateUserFields(user.fields)

        val userToChange = getUserByUserId(user.userId!!).also {
            it.username = user.username
            it.fields = UserFieldMapper.INSTANCE.convertToModel(user.fields)
        }
        val changedUser = linkGroupsToUser(userToChange, user.securityGroups)
        return dtoMapper.convertToDto(changedUser)
    }

    override fun linkGroupsToUser(userId: UUID, groups: Collection<String>) {
        val userToChange = getUserByUserId(userId)
        linkGroupsToUser(userToChange, groups)
    }

    private fun linkGroupsToUser(user: User, groups: Collection<String>): User {
        val securityGroups = securityGroupRepository.findAllById(getGroupIds(groups)).toSet()
        val changedUser = user.also {
            it.groups = securityGroups
        }
        return userDataRepository.save(changedUser)
    }

    private fun getGroupIds(groups: Collection<String>): Set<SecurityGroupId> {
        return groups.asSequence()
            .map { SecurityGroupId(it) }
            .toSet()
    }

    override fun findUserByName(name: String): UserDto {
        return internalDtoMapper(getUserByName(name))
    }

    override fun findUserByUserId(userId: UUID): UserDto {
        return internalDtoMapper(getUserByUserId(userId))
    }

    private fun getUserByUserId(userId: UUID): User {
        return userDataRepository.findById(UserId(userId))
            .orElseThrow { UserNotFoundException("User with id $userId cannot be found.") }
    }

    private fun getUserByName(name: String): User {
        return userDataRepository.findByUsernameIgnoreCase(name)
            .orElseThrow { UserNotFoundException("User $name cannot be found.") }
    }

    private fun validateUser(username: String, password: String, email: String) {
        Preconditions.checkArgument(StringUtils.hasText(username), "User name not filled in.")
        Preconditions.checkArgument(StringUtils.hasText(password), "Password not filled in.")
        Preconditions.checkArgument(StringUtils.hasText(email), "Email not filled in.")
    }

    private fun validateUser(username: String, email: String) {
        Preconditions.checkArgument(StringUtils.hasText(username), "User name not filled in.")
        Preconditions.checkArgument(StringUtils.hasText(email), "Email not filled in.")
    }

    private fun validateUserFields(fields: Map<String, String?>) {
        metadataValidationService.checkAllCustomFieldsAreAllowed(Reference.USER, fields)
        metadataValidationService.checkAllMandatoryCustomFieldsArePresent(Reference.USER, fields)
    }

    private fun internalDtoMapper(user: User): UserDto {
        return dtoMapper.convertToDto(user)
    }
}