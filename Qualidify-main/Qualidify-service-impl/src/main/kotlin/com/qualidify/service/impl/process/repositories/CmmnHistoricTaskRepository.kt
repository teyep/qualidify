package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.CmmnHistoricTaskJpa


/**
 * Read only repository fo [CmmnHistoricTaskJpa]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface CmmnHistoricTaskRepository : ReadOnlyQueryablePagingAndSortingRepository<CmmnHistoricTaskJpa, String>