package com.qualidify.service.impl.process

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.flowable.CmmnCaseInstanceJpa
import com.qualidify.service.impl.process.mappers.CaseInstanceDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.impl.process.repositories.CmmnHistoricCaseInstanceRepository
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.exceptions.CaseInstanceNotFoundException
import org.flowable.cmmn.api.CmmnHistoryService
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.engine.CmmnEngine
import org.flowable.cmmn.engine.impl.util.CommandContextUtil
import org.flowable.variable.api.history.HistoricVariableInstance
import org.flowable.variable.service.impl.persistence.entity.HistoricVariableInstanceEntityImpl
import org.flowable.variable.service.impl.persistence.entity.VariableInstanceEntityImpl
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import java.util.*

/**
 * An implementation of the [CaseInstanceService] interface, to retrieve the [CaseInstanceDto] from Flowable
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Service
open class CaseInstanceServiceImpl(
    private val cmmnCaseInstanceRepository: CmmnCaseInstanceRepository,
    private val cmmnHistoricCaseInstanceRepository: CmmnHistoricCaseInstanceRepository,
    private val mapper: CaseInstanceDtoMapper,
    @Lazy private val cmmnEngine: CmmnEngine,
    @Lazy private val cmmnRuntimeService: CmmnRuntimeService,
    @Lazy private val cmmnHistoryService: CmmnHistoryService,
) : AbstractQueryableService<CmmnCaseInstanceJpa, CaseInstanceDto>(cmmnCaseInstanceRepository),
    CaseInstanceService {

    private val logger by LoggerDelegation()

    override fun mapEntityToDto(entity: CmmnCaseInstanceJpa): CaseInstanceDto =
        mapper.convertToDto(entity)

    override fun getHistoricCaseInstanceDtoById(caseInstanceId: String): CaseInstanceDto =
        cmmnHistoricCaseInstanceRepository.findById(caseInstanceId)
            .map {
                mapper.convertToDto(it)
            }.orElseThrow {
                CaseInstanceServiceException("Could not find a historic case instance with id '$caseInstanceId'.")
            }


    override fun getCaseInstanceDtoById(caseInstanceId: String): CaseInstanceDto =
        cmmnCaseInstanceRepository.findById(caseInstanceId)
            .map { mapEntityToDto(it) }
            .orElseThrow { CaseInstanceNotFoundException("Cannot find a case instance with id=$caseInstanceId.") }

    override fun complete(dto: CaseInstanceDto, currentUser: String) {
        cmmnRuntimeService.completeCaseInstance(dto.id)
        logger.info("Case '${dto.caseDefinition.name}' with id '${dto.id}' was completed by user '$currentUser'")
    }

    override fun cancel(dto: CaseInstanceDto, currentUser: String) {
        cmmnRuntimeService.terminateCaseInstance(dto.id)
        logger.info("Case '${dto.caseDefinition.name}' with id '${dto.id}' was terminated by user '$currentUser'")
    }

    override fun getCaseVariable(caseInstanceId: String, variableName: String): String? =
        cmmnRuntimeService.getVariable(caseInstanceId, variableName) as? String

    override fun getCaseVariables(caseInstanceId: String): Map<String, Any?> {
        val variables = cmmnRuntimeService.getVariables(caseInstanceId)
        if (variables.isEmpty()) {
            throw CaseInstanceServiceException("Could not find any variables belonging to case with id '$caseInstanceId' in the runtime tables.")
        }
        return variables
    }

    override fun getHistoricCaseVariable(caseInstanceId: String, variableName: String): HistoricVariableInstance =
        cmmnHistoryService.createHistoricVariableInstanceQuery()
            .caseInstanceId(caseInstanceId)
            .excludeTaskVariables()  // do not include task-local variables
            .variableName(variableName)
            .list()
            .single { it.subScopeId == null }  // remove the variables that belong to plan items (i.e. the sub-scope)

    override fun getHistoricCaseVariables(caseInstanceId: String): Map<String, Any?> =
        cmmnHistoryService.createHistoricVariableInstanceQuery()
            .caseInstanceId(caseInstanceId)
            .list()
            .associate { it.variableName to it.value }  // fancy function to create a map

    override fun setCaseVariable(caseInstanceId: String, variableName: String, variableValue: String) =
        cmmnRuntimeService.setVariable(caseInstanceId, variableName, variableValue)

    override fun updateHistoricVariable(historicVariable: HistoricVariableInstance, variableValue: String) {

        // cast historic variable as entity impl (so it can be changed)
        historicVariable as HistoricVariableInstanceEntityImpl

        // convert to actual variable instance entity impl and update the review state
        val updatedVariable = VariableInstanceEntityImpl().also {
            it.id = historicVariable.id
            it.name = historicVariable.name
            it.scopeId = historicVariable.scopeId
            it.subScopeId = historicVariable.subScopeId
            it.scopeType = historicVariable.scopeType
            it.textValue = if (historicVariable.textValue != null) variableValue else null
            it.textValue2 = historicVariable.textValue2
            it.doubleValue = historicVariable.doubleValue
            it.longValue = historicVariable.longValue
            it.type = historicVariable.variableType
            it.originalPersistentState = historicVariable.originalPersistentState
        }

        // update the historic variable by executing a recordVariableUpdate command using the command executor
        cmmnEngine.cmmnEngineConfiguration.commandExecutor.execute { commandContext ->
            CommandContextUtil.getHistoricVariableService(commandContext)
                .recordVariableUpdate(updatedVariable, Date())
        }

    }

}

class CaseInstanceServiceException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)

