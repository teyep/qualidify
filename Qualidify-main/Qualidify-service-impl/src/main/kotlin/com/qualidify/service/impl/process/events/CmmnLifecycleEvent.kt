package com.qualidify.service.impl.process.events

import com.qualidify.service.process.dto.CaseInstanceDto

/**
 * Event adapting a Flowable Cmmn lifecycle event
 *
 * @author Marcel Pot
 * @since 1.0
 *
 */
open class CmmnLifecycleEvent(
    val caseInstance: CaseInstanceDto,
    val oldState: String,
    val newState: String,
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CmmnLifecycleEvent) return false

        if (caseInstance != other.caseInstance) return false
        if (oldState != other.oldState) return false
        if (newState != other.newState) return false

        return true
    }

    override fun hashCode(): Int {
        var result = caseInstance.hashCode()
        result = 31 * result + oldState.hashCode()
        result = 31 * result + newState.hashCode()
        return result
    }
}

open class CmmnEndingEvent(caseInstance: CaseInstanceDto, oldState: String, newState: String) :
    CmmnLifecycleEvent(caseInstance, oldState, newState)

class ActivatedCmmnEvent(caseInstance: CaseInstanceDto) : CmmnLifecycleEvent(caseInstance, "", "active")
class CompletedCmmnEvent(caseInstance: CaseInstanceDto) : CmmnEndingEvent(caseInstance, "active", "completed")
class TerminatedCmmnEvent(caseInstance: CaseInstanceDto) : CmmnEndingEvent(caseInstance, "active", "terminated")