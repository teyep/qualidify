package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.FormDefinitionJpa


/**
 * Read only repository fo [FormDefinitionJpa]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface FormDefinitionRepository : ReadOnlyQueryablePagingAndSortingRepository<FormDefinitionJpa, String> {

    fun findByDeploymentId(deploymentId: String): List<FormDefinitionJpa>

}