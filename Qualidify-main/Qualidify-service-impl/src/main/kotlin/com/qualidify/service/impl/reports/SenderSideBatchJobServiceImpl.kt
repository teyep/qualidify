package com.qualidify.service.impl.reports

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.model.entities.BatchJobId
import com.qualidify.service.report.SenderSideBatchJobService
import com.qualidify.service.report.dto.BatchJobArtifactDto
import com.qualidify.service.report.dto.BatchJobDto
import com.qualidify.service.report.dto.ReportTemplateDto
import org.springframework.stereotype.Service
import java.util.*

@Service
class SenderSideBatchJobServiceImpl(
    private val batchJobRepository: BatchJobRepository,
    private val batchJobArtifactRepository: BatchJobArtifactRepository,
    private val batchJobDtoMapper: BatchJobDtoMapper,
    private val batchJobArtifactDtoMapper: BatchJobArtifactDtoMapper,
    private val reportTemplateBatchJobConversionService: ReportTemplateBatchJobConversionServiceImpl
) : SenderSideBatchJobService {

    override fun getById(id: UUID): BatchJobDto? {
        val found = batchJobRepository.findById(BatchJobId(id))
        return if (found.isPresent) batchJobDtoMapper.convertToDto(found.get()) else null
    }

    override fun createBatchJob(dto: BatchJobDto) {
        batchJobRepository.save(
            batchJobDtoMapper.convertToModel(dto)
        )
    }

    override fun scheduleFromReportTemplateDto(dto: ReportTemplateDto, parameters: ObjectMap) {
        createBatchJob(this.reportTemplateBatchJobConversionService.convert(dto, parameters))
    }

    override fun getArtifact(id: UUID): BatchJobArtifactDto? {
        val found = batchJobArtifactRepository.findById(BatchJobId(id))
        return if (found.isPresent) batchJobArtifactDtoMapper.convertToDto(found.get()) else null
    }

    override fun hasArtifact(jobId: UUID): Boolean {
        return batchJobArtifactRepository.findById(BatchJobId(jobId)).isPresent
    }

    override fun countByQuery(query: CountQuery): CountResponse = batchJobRepository.countResponse(query)

    override fun findByQuery(query: SearchQuery): Collection<BatchJobDto> = batchJobRepository.findAllByQuery(query)
        .map { batchJobDtoMapper.convertToDto(it) }
        .toList()
}
