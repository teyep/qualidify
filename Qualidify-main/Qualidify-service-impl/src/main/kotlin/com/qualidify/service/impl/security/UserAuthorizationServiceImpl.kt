package com.qualidify.service.impl.security

import com.qualidify.model.entities.SecurityGroupId
import com.qualidify.model.entities.User
import com.qualidify.model.entities.UserId
import com.qualidify.service.impl.user.UserDataRepository
import com.qualidify.service.security.UserAuthorizationService
import com.qualidify.service.user.exceptions.UserNotFoundException
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.util.*

/**
 * Implementation of the userService for authorization purposes
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class UserAuthorizationServiceImpl(
    val userRepository: UserDataRepository,
    val groupRepository: SecurityGroupRepository,
) : UserAuthorizationService {


    override val currentUserName: String?
        get() {
            return determineUserDetails()?.username
        }

    private fun determineUserDetails(): UserDetails? {
        val context = SecurityContextHolder.getContext()
        val principal = context.authentication.principal
        if (principal is UserDetails) {
            val userDetails = context.authentication.principal as UserDetails
            return userDetails
        }
        return null
    }

    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByUsernameIgnoreCase(username)
            .orElseThrow { UserNotFoundException("User with $username not found!") }
        return createUserDetails(user)
    }

    override fun loadUserById(id: UUID): UserDetails {
        val user =
            userRepository.findById(UserId(id)).orElseThrow { UserNotFoundException("User with id $id not found!") }
        return createUserDetails(user)
    }

    override fun loadUsersByIds(ids: Collection<UUID>): List<UserDetails> {
        val userids = ids.map { UserId(it) }.toSet()
        val users = userRepository.findAllById(userids)
        return users.asSequence().map { createUserDetails(it) }.toList()
    }

    override fun loadGroupByName(id: String): GrantedAuthority {
        val group = groupRepository.findById(SecurityGroupId(id))
            .orElseThrow { SecurityGroupNotFoundException("Group with $id not found!") }
        return SimpleGrantedAuthority(group.id.unbox())
    }

    override fun loadGroupsByNames(ids: Collection<String>): Collection<GrantedAuthority> {
        val groupIds = ids.map { SecurityGroupId(it) }.toSet()
        val groups = groupRepository.findAllById(groupIds)
        return groups.asSequence()
            .map { SimpleGrantedAuthority(it.id.unbox()) }
            .toSet()
    }

    override fun loadGroupsByUserName(userName: String): Collection<GrantedAuthority> {
        return groupRepository.findAllByUserName(userName)
            .map { SimpleGrantedAuthority(it.id.unbox()) }
            .toSet()
    }

    override fun loadGroupsByUserNames(userNames: Collection<String>): Collection<GrantedAuthority> {
        return groupRepository.findAllByUserNames(userNames)
            .map { SimpleGrantedAuthority(it.id.unbox()) }
            .toSet()
    }

    override fun isUserLoggedIn() = isUserLoggedIn(SecurityContextHolder.getContext().authentication)

    /**
     * Check if a user is currently loggedin
     * @param authentication the authentication to check
     * @return a boolean indicating if a user is logged in
     */
    fun isUserLoggedIn(authentication: Authentication?): Boolean {
        return (authentication != null
                && authentication !is AnonymousAuthenticationToken)
    }

    private fun createUserDetails(user: User): UserDetails {
        val roles = user.roles.asSequence().toList().toTypedArray()
        return org.springframework.security.core.userdetails.User.withUsername(user.username)
            .password(user.password)
            .roles(*roles)
            .build()
    }

}