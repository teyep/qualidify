package com.qualidify.service.impl.document

import com.qualidify.core.jpa.annotations.SoftDelete
import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.Document
import com.qualidify.model.entities.DocumentId
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

/**
 * A repository for retrieving and persisting [Document] entities in and from a datasource
 *
 * @author Marcel Pot
 * @since 1.0
 */
@SoftDelete
interface DocumentRepository : QueryablePagingAndSortingRepository<Document, DocumentId>,
    JpaSpecificationExecutor<Document> {

    /**
     * Get a collection of Documents that are in a specified path
     * @param path The path requested
     * @return an interable of Documents
     */
    fun findAllByPath(path: String): Iterable<Document>

    /**
     * Get a document by path, name and type
     * @param path the path
     * @param name the name
     * @param type the type
     * @return an optional Document
     */
    fun findDocumentByPathAndNameAndTypeAndDeletedIsFalse(
        path: String,
        name: String,
        type: String,
    ): Optional<Document>

}