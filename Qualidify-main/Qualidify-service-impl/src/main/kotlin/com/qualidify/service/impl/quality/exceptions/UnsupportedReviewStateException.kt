package com.qualidify.service.impl.quality.exceptions

class UnsupportedReviewStateException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)
