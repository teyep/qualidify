package com.qualidify.service.impl.projects

import com.qualidify.core.jpa.annotations.SoftDelete
import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.ProjectTemplate
import com.qualidify.model.entities.ProjectTemplateId

/**
 * Repository for persisting and retrieving projectTemplates from the datastorage
 *
 * @author Marcel Pot
 * @since 1.0
 */
@SoftDelete
interface ProjectTemplateRepository : QueryablePagingAndSortingRepository<ProjectTemplate, ProjectTemplateId>