package com.qualidify.service.impl.projects

import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.ProjectInstance
import com.qualidify.model.entities.ProjectInstanceId
import com.qualidify.service.impl.process.events.CmmnEndingEvent
import com.qualidify.service.impl.process.mappers.ProjectInstanceDtoMapper
import com.qualidify.service.projects.ProjectInstanceService
import com.qualidify.service.projects.dto.ProjectInstanceDto
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import java.util.*


/**
 * An implementation of the [ProjectInstanceService] interface, to retrieve the [ProjectInstanceDto]
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Service
open class ProjectInstanceServiceImpl(
    private val projectInstanceRepository: ProjectInstanceRepository,
    private val mapper: ProjectInstanceDtoMapper,
) : AbstractQueryableService<ProjectInstance, ProjectInstanceDto>(projectInstanceRepository),
    ProjectInstanceService {

    override fun mapEntityToDto(entity: ProjectInstance): ProjectInstanceDto = mapper.convertToDto(entity)

    override fun getProjectInstanceDtoByCmmnIdOrQualityShellId(id: String): ProjectInstanceDto {
        return if (projectInstanceRepository.findProjectInstanceByQualityShellId(id).isPresent) {
            projectInstanceRepository.findProjectInstanceByQualityShellId(id)
                .map { mapEntityToDto(it) }
                .get()
        } else {
            projectInstanceRepository.findProjectInstanceByRegularCaseId(id)
                .map { mapEntityToDto(it) }
                .get()
        }
    }

    override fun getProjectInstanceById(id: String): ProjectInstanceDto? {
        val projectId = getProjectInstanceIdFromString(id)
        return projectInstanceRepository.findById(projectId)
            .map { mapEntityToDto(it) }
            .orElse(null)
    }

    override fun getProjectInstanceByCmmnId(cmmnInstanceId: String): ProjectInstanceDto? {
        return projectInstanceRepository.findProjectInstanceByRegularCaseId(cmmnInstanceId)
            .map { mapEntityToDto(it) }
            .orElse(null)
    }

    override fun isProjectAvailable(id: String): Boolean {
        val projectId = getProjectInstanceIdFromString(id)
        return projectInstanceRepository.existsById(projectId)
    }

    private fun getProjectInstanceIdFromString(id: String) = ProjectInstanceId(UUID.fromString(id))

    private fun finishProjectInstance(caseInstanceId: String, state: String) {
        val projectInstance = projectInstanceRepository.findProjectInstanceByRegularCaseId(caseInstanceId)
        if (projectInstance.isPresent) {
            projectInstanceRepository.save(projectInstance.get().also {
                it.state = "completed"
                it.regularCaseState = state
            })
        }
    }

    @EventListener
    open fun handleCmmnEndingEvent(event: CmmnEndingEvent) {
        finishProjectInstance(event.caseInstance.id, event.newState)
    }

}

class ProjectInstanceServiceException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)