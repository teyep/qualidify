package com.qualidify.service.impl.reports

import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.BatchJobArtifact
import com.qualidify.model.entities.BatchJobId

/**
 * Repository for persisting and retrieving ReportResultContent objects from the data storage
 *
 * @author Niels Visscher
 * @since 1.0
 */
interface BatchJobArtifactRepository: QueryablePagingAndSortingRepository<BatchJobArtifact, BatchJobId> {
}
