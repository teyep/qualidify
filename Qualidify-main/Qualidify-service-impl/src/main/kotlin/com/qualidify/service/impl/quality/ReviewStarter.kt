package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.process.exceptions.CaseInstanceNotFoundException
import com.qualidify.service.quality.HasQuality
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import com.qualidify.service.quality.ReviewType

/**
 * Disposable ReviewStarter class taking care of linking the regular item with the correct review
 * plan item and trigger the instantiation of the corresponding review task.
 *
 * NB: combination of a disposable object with late-init variables ensures that no cross-contamination is possible.
 *
 * @author Menno Tol
 * @since 1.0
 */
open class ReviewStarter<D : HasQuality>(
    private val caseInstanceService: CaseInstanceService,
    private val planItemService: PlanItemService,
) {

    private val logger by LoggerDelegation()

    private lateinit var regularItem: D
    private lateinit var qualityShellCaseInstance: CaseInstanceDto
    private lateinit var reviewType: ReviewType
    private lateinit var reviewStarterEventListener: PlanItemDto
    private lateinit var nextReviewTaskPlanItem: PlanItemDto

    fun start(dto: D) {
        this.regularItem = dto
        findQualityShellCaseInstance()
        determineReviewType()
        findReviewStarterEventListener()
        findNextReviewTaskPlanItem()
        makeReviewLinks()
        renameReviewTaskPlanItem()
        triggerReviewStarterEventListener()
        updateReviewStates()
    }

    // finds the corresponding quality shell instance
    private fun findQualityShellCaseInstance() {
        try {
            qualityShellCaseInstance = caseInstanceService.getCaseInstanceDtoById(
                regularItem.qualityShellId
                    ?: throw ReviewStarterException("The 'cmmnQualityShellId' property of dto '${regularItem.name}', id '${regularItem.id}' is 'null'")
            )
        } catch (exception: CaseInstanceNotFoundException) {
            throw ReviewStarterException(
                "The quality shell instance with id '${regularItem.qualityShellId}' could not be found.",
                exception.cause
            )
        }
    }

    // determines the type of review based on the incoming dto
    private fun determineReviewType() {
        reviewType = when (regularItem) {
            is CaseInstanceDto -> ReviewType.CASE
            is PlanItemDto -> when ((regularItem as PlanItemDto).type) {
                PlanItemType.STAGE -> ReviewType.STAGE
                PlanItemType.HUMAN_TASK -> ReviewType.TASK
                else -> throw ReviewStarterException("Review for plan item type '${(regularItem as PlanItemDto).type}' not implemented.")
            }

            else -> throw ReviewStarterException("Review for item '${regularItem}' not implemented.")
        }
    }

    // searches the quality shell for the event listener responsible for firing review tasks
    private fun findReviewStarterEventListener() {
        val listeners = planItemService.getEventListenersByCaseInstanceId(qualityShellCaseInstance.id)
            .filter { it.state == PlanItemState.AVAILABLE }
            .filter { it.name.lowercase() == "Start ${reviewType.type} review".lowercase() }

        if (listeners.isEmpty()) {
            throw ReviewStarterException("No ${reviewType.type} review starter found for id '${qualityShellCaseInstance.id}'")
        } else if (listeners.size > 1) {
            throw ReviewStarterException("Multiple active review starters found with name 'Start ${reviewType.type} review'")
        }

        val listener = listeners.first()
        if (listener.isQualityShell && listener.type == PlanItemType.GENERIC_EVENT_LISTENER) {
            reviewStarterEventListener = listener
        } else throw ReviewStarterException(
            "Dto is not a qualityShell, or not of type 'genericeventlistener' " +
                    "or does not have name 'Start ${reviewType.type} review'."
        )
    }

    // searches for the next review task plan item that is in state 'available' or 'wait_repetition'
    private fun findNextReviewTaskPlanItem() {
        val reviewPlanItems = planItemService.getPlanItemsByStageAndDefinition(reviewStarterEventListener)
            .filter { it.name.lowercase() == "review ${reviewType.type}" }
            .filter { it.state != PlanItemState.ACTIVE }
            .sortedBy { it.timeCreated }

        if (reviewPlanItems.isEmpty()) {
            throw ReviewStarterException(
                "The plan item for the next review task cannot be found " +
                        "or is not in state 'available' or 'wait_repetition'"
            )
        }

        // if the list contains >1 plan item, the newest item will become the next review task
        // => take the last item of the list sorted by time created
        nextReviewTaskPlanItem = reviewPlanItems.last()
    }

    // stores the id of the regular item in the quality link key local variable of the review task plan item
    // and vice versa
    private fun makeReviewLinks() {
        if (regularItem.projectInstanceId != nextReviewTaskPlanItem.projectInstanceId) {
            throw ReviewStarterException(
                "The review starter and regular item do not belong to the same project"
            )
        }

        // determine the quality link key based on the type of the regular item
        val qualityLinkKey = when (reviewType) {
            ReviewType.CASE -> QualityLinkKey.REGULAR_CASE_ID
            ReviewType.STAGE -> QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID
            ReviewType.TASK -> QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID
            else -> throw ReviewStarterException("Review for plan item type '${reviewType.type}' not implemented.")
        }

        // write the quality links of the review task
        planItemService.setPlanItemVariable(
            nextReviewTaskPlanItem.id,
            "qualityLinkKey",
            qualityLinkKey.key
        )
        planItemService.setPlanItemVariable(
            nextReviewTaskPlanItem.id,
            qualityLinkKey.key,
            regularItem.id
        )

        // write the quality links of the regular item
        when (qualityLinkKey) {
            // for a plan item the link is stored as local variable
            QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID, QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID -> {
                planItemService.setPlanItemVariable(
                    regularItem.id,
                    "qualityLinkKey",
                    QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID.key
                )
                planItemService.setPlanItemVariable(
                    regularItem.id,
                    QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID.key,
                    nextReviewTaskPlanItem.id
                )
            }
            // for a case instance the link is stored as global variable (cases don't have local variables)
            QualityLinkKey.REGULAR_CASE_ID -> {
                caseInstanceService.setCaseVariable(
                    regularItem.id,
                    "qualityLinkKey",
                    QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID.key
                )
                caseInstanceService.setCaseVariable(
                    regularItem.id,
                    QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID.key,
                    nextReviewTaskPlanItem.id
                )
            }

            else -> throw ReviewStarterException("Making review links for '$qualityLinkKey' not implemented.")

        }

        logger.info(
            "Task review links for plan item '${regularItem.name}, ${regularItem.id}' were created, " +
                    "the id of the corresponding review task plan item is '${nextReviewTaskPlanItem.id}'"
        )
    }

    // rename the review task plan item based on the name of the corresponding regular item.
    private fun renameReviewTaskPlanItem() {
        planItemService.setPlanItemVariable(
            nextReviewTaskPlanItem.id,
            "localizedName",
            "Review for ${reviewType.type}: '${regularItem.name}'"
        )
    }

    // updates the ReviewState variable of the regular item and the review task
    private fun updateReviewStates() {
        val reviewStateUpdater = ReviewStateUpdater<D>(planItemService, caseInstanceService)
        reviewStateUpdater.start(regularItem, ReviewState.IN_REVIEW, nextReviewTaskPlanItem)
    }

    // fire the review starter event listener
    // this will instantiate the corresponding review task from the 'nextReviewTaskPlanItem'
    private fun triggerReviewStarterEventListener() {
        try {
            planItemService.completeGenericEventListener(reviewStarterEventListener)
            logger.info(
                "Event listener for the review of ${reviewType.type} '${regularItem.name}', " +
                        "'${regularItem.id}' has been triggered."
            )
        } catch (exception: Exception) {
            throw ReviewStarterException(
                "Flowable exception when starting '${reviewType.type}' review for id '${reviewStarterEventListener.id}'.",
                exception
            )
        }
    }

}

class ReviewStarterException(message: String? = null, cause: Throwable? = null) : RuntimeException(message, cause)