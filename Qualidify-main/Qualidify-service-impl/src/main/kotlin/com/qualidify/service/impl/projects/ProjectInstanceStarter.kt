package com.qualidify.service.impl.projects

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.model.entities.ProjectInstance
import com.qualidify.model.entities.ProjectInstanceId
import com.qualidify.model.entities.ProjectInstanceVariable
import com.qualidify.model.entities.User
import com.qualidify.service.impl.quality.ReviewStateFiller
import com.qualidify.service.impl.user.UserDataRepository
import com.qualidify.service.projects.dto.ProjectTemplateDto
import org.flowable.cmmn.api.CmmnRuntimeService
import org.flowable.cmmn.api.runtime.CaseInstance
import org.flowable.cmmn.api.runtime.CaseInstanceBuilder
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.util.*

/**
 * Start a project and creates a project instance. The project consists of
 * - a quality shell (parent of the regular case),
 * - a regular case (the case that contains the tasks, child of the quality shell)
 * - a project instance wrapper containing info (such as variables and references to the parent and child case)
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
class ProjectInstanceStarter(
    private val cmmnRuntimeService: CmmnRuntimeService,
    private val projectInstanceRepository: ProjectInstanceRepository,
    private val userDataRepository: UserDataRepository,
    private val reviewStateFiller: ReviewStateFiller,
) {

    private val logger by LoggerDelegation()
    private val qualityShellKey = "cmmnQualityShell"

    fun start(projectTemplateDto: ProjectTemplateDto): ProjectInstance {
        val projectInstanceId = ProjectInstanceId(UUID.randomUUID()).id.toString()
        val qualityShellBuilder = getQualityShellBuilder(projectInstanceId, projectTemplateDto)
        val qualityShell = startQualityShell(qualityShellBuilder)
        val regularCase = getRegularCaseInstance(qualityShell, projectTemplateDto, projectInstanceId)
        val projectInstance = createProjectInstance(projectInstanceId, projectTemplateDto, qualityShell, regularCase)

        val savedProjectInstance = saveProjectInstance(projectInstance)
        reviewStateFiller.fill(qualityShell, regularCase)

        return savedProjectInstance
    }

    private fun getQualityShellBuilder(
        projectInstanceId: String,
        dto: ProjectTemplateDto,
    ): CaseInstanceBuilder {
        val qualityShellBuilder = cmmnRuntimeService.createCaseInstanceBuilder().caseDefinitionKey(qualityShellKey)
        addVariablesToQualityShell(projectInstanceId, dto, qualityShellBuilder)
        return qualityShellBuilder
    }

    /**
     * Adds the required case variables to the quality shell (i.e. the outer case):
     * 1) projectInstanceId: the id of the parent project instance
     * 2) isQualityShellMark: a Boolean tag indicating if the case instance is a qualityShell
     * 3) regularCaseDefinitionKey: passed automatically via the <caseRefExpression> xml-tag to start the child case
     */
    private fun addVariablesToQualityShell(
        projectInstanceId: String,
        dto: ProjectTemplateDto,
        builder: CaseInstanceBuilder,
    ) {
        if (dto.cmmnModel == "") {
            throw NoCaseDefinitionProvidedException(
                "The cmmnModel attribute of dto '$dto' is empty, please provide a valid cmmnModel"
            )
        } else {
            builder.variable("projectInstanceId", projectInstanceId)
            builder.variable("isQualityShell", true)
            builder.variable("regularCaseDefinitionKey", dto.cmmnModel)
        }
    }

    private fun startQualityShell(builder: CaseInstanceBuilder): CaseInstance {
        try {
            val qualityShell = builder.start()
            logger.info("A new quality shell was started, the case instance received id '${qualityShell.id}'.")
            return qualityShell
        } catch (exception: FlowableObjectNotFoundException) {
            if (exception.message == "No case definition found for key $qualityShellKey") {
                throw QualityShellNotFoundException(
                    "The cmmn quality shell case definition (key: '$qualityShellKey') could not be found"
                )
            } else {
                throw CaseDefinitionNotFoundException(
                    "The case definition for key '${builder.variables["regularCaseDefinitionKey"]}' could not be found."
                )
            }
        }
    }

    /**
     * Gets the regular (child) case instance using a double check query of the regularCaseDefinitionKey and the
     * qualityInstanceId. There should only be a single result.
     */
    private fun getRegularCaseInstance(
        qualityShell: CaseInstance,
        projectTemplateDto: ProjectTemplateDto,
        projectInstanceId: String,
    ): CaseInstance {
        val regularCase = cmmnRuntimeService.createCaseInstanceQuery()
            .caseDefinitionKey(qualityShell.caseVariables["regularCaseDefinitionKey"].toString())
            .caseInstanceParentId(qualityShell.id)  // the quality shell id
            .singleResult()

        logger.info("The child case item started in quality shell id '${qualityShell.id}' received id '${regularCase.id}'")

        addVariablesToRegularCase(regularCase, qualityShell, projectTemplateDto, projectInstanceId)
        addRegularCaseToQualityShell(regularCase, qualityShell)

        return regularCase
    }

    /**
     * Adds the required case variables to the regular case (i.e. the child case of the quality shell):
     * 1) projectInstanceId: the id of the governing project instance
     * 2) isQualityShellMark: a Boolean tag indicating if the case instance is a qualityShell
     * 3) cmmnQualityShellId: the id of the corresponding quality shell
     * 4) any required case variables for the regular case (which will be copied from the project template dto)
     */
    private fun addVariablesToRegularCase(
        regularCase: CaseInstance,
        qualityShell: CaseInstance,
        projectTemplateDto: ProjectTemplateDto,
        projectInstanceId: String,
    ) {
        val variables = mutableMapOf<String, Any?>(
            "projectInstanceId" to projectInstanceId,
            "isQualityShell" to false,
            "cmmnQualityShellId" to qualityShell.id
        )

        projectTemplateDto.variables.forEach {
            variables[it.name] = it.value
        }

        cmmnRuntimeService.setVariables(regularCase.id, variables)
    }

    private fun addRegularCaseToQualityShell(regularCase: CaseInstance, qualityShell: CaseInstance) {
        cmmnRuntimeService.setVariable(qualityShell.id, "cmmnCaseInstanceId", regularCase.id)
    }

    private fun createProjectInstance(
        projectInstanceId: String,
        dto: ProjectTemplateDto, qualityShell: CaseInstance, regularCase: CaseInstance,
    ) = ProjectInstance(
        id = ProjectInstanceId(UUID.fromString(projectInstanceId)),
        templateId = UUID.fromString(dto.id),
        name = dto.name,
        description = dto.description,
        qualityShellId = qualityShell.id,
        qualityShellInstanceState = "active",
        regularCaseId = regularCase.id,
        regularCaseState = "active",
        startTime = LocalDateTime.now(),
        variables = dto.variables.asSequence()
            .map {
                ProjectInstanceVariable(
                    name = it.name,
                    datatype = it.datatype,
                    value = it.value
                )
            }
            .toList(),
        users = mapUsernamesToUsers(dto.users),
        state = "active",
    )

    private fun saveProjectInstance(projectInstance: ProjectInstance) = projectInstanceRepository.save(projectInstance)

    private fun mapUsernamesToUsers(userNames: Collection<String>): List<User> {
        return userNames.map { userDataRepository.findByUsernameIgnoreCase(it) }
            .filter { it.isPresent }
            .map { it.get() }
            .toList()
    }

}

class NoCaseDefinitionProvidedException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)

class QualityShellNotFoundException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)

class CaseDefinitionNotFoundException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)
