package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.quality.CaseReviewService
import org.flowable.cmmn.api.listener.CaseInstanceLifecycleListener
import org.flowable.cmmn.api.runtime.CaseInstance
import org.springframework.stereotype.Component

/**
 * Listener required for starting quality actions of case instances.
 * The listener is added to the engine using the [FlowableConfiguration] class.
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
class CaseCompletedListener(private val caseReviewService: CaseReviewService) : CaseInstanceLifecycleListener {

    private val logger by LoggerDelegation()

    override fun getSourceState(): String = "active"

    override fun getTargetState(): String = "completed"

    override fun stateChanged(caseInstance: CaseInstance, oldState: String, newState: String) {

        if (caseChecker(caseInstance)) {
            logger.info(
                "Publishing state changed event for case ${caseInstance.caseDefinitionId}. " +
                        "Case has changed from $oldState to $newState"
            )
            caseReviewService.startReview(caseInstance)
        }

    }

    // very basic check based on case definition id of the quality shell
    private fun caseChecker(caseInstance: CaseInstance): Boolean {
        return caseInstance.caseDefinitionId != "cmmnQualityShell"
    }

}