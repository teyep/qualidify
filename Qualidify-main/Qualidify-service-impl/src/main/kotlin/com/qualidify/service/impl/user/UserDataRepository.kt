package com.qualidify.service.impl.user

import com.qualidify.core.jpa.annotations.SoftDelete
import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.User
import com.qualidify.model.entities.UserId
import java.util.*

/**
 * Repository for Users
 *
 * @author Marcel Pot
 * @since 1.0
 */
@SoftDelete
interface UserDataRepository : QueryablePagingAndSortingRepository<User, UserId> {

    /**
     * Find a user by its name while ignoring the case
     * @param username the username
     * @return an optional with or without a User
     */
    fun findByUsernameIgnoreCase(username: String): Optional<User>
}