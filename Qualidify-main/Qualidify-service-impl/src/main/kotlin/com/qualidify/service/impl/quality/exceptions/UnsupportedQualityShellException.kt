package com.qualidify.service.impl.quality.exceptions

class UnsupportedQualityShellException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)
