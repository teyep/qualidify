package com.qualidify.service.impl.storage

import com.qualidify.model.entities.FileStorage
import com.qualidify.model.entities.FileStorageId
import org.springframework.data.jpa.repository.JpaRepository

interface FileStorageRepository : JpaRepository<FileStorage, FileStorageId> {
}