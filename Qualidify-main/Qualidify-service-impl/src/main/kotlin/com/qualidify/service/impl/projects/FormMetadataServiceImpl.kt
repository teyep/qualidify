package com.qualidify.service.impl.projects

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.DataType
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.service.process.exceptions.FormNotFoundException
import com.qualidify.service.projects.FormMetadataService
import com.qualidify.service.projects.TaskService
import org.flowable.cmmn.api.CmmnTaskService
import org.flowable.common.engine.api.FlowableObjectNotFoundException
import org.flowable.form.api.FormModel
import org.flowable.form.api.FormRepositoryService
import org.flowable.form.model.FormField
import org.flowable.form.model.SimpleFormModel
import org.springframework.stereotype.Service

/**
 * An implementation of the [FormMetadataService] where flowable implements the logic
 * of the implementation.
 *
 * NB: Flowable makes a distinction between startForms and taskForms, startForms are used to start a BPMN process and
 * require a link to the process definition. TaskForms are used for saving and completing data belonging to
 * task instances (i.e. with a taskId). Here only taskForms are implemented.
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Service
class FormMetadataServiceImpl(
    private val formRepositoryService: FormRepositoryService,
    private val cmmnTaskService: CmmnTaskService,
    private val taskService: TaskService,
) : FormMetadataService {

    private val logger by LoggerDelegation()

    override fun loadFormModelMetadataByFormKey(formDefinitionKey: String): List<ColumnDef> {
        try {
            val formDefinition = formRepositoryService.getFormModelByKey(formDefinitionKey)
            return mapFormModelToMetadataList(formDefinition.formModel)
        } catch (exception: FlowableObjectNotFoundException) {
            throw FormNotFoundException("Cannot find form with key $formDefinitionKey", exception)
        }
    }

    override fun loadFormModelMetadataByTaskId(taskId: String): List<ColumnDef> {
        try {
            val formDefinition = cmmnTaskService.getTaskFormModel(taskId)
            return mapFormModelToMetadataList(formDefinition.formModel)
        } catch (exception: FlowableObjectNotFoundException) {
            throw FormNotFoundException("Cannot find form for task with $taskId", exception)
        }
    }

    /**
     * Stores the entered data in the database ("ACT_HI_VARINST").
     *
     * NB: the task is not completed and the form can be filled and saved again, this
     * will overwrite the data in the varinst table!
     */
    override fun saveTaskFormData(taskId: String, data: Map<String, Any?>) {
        try {
            // an "empty" dataset will still contain the initiator user, therefore size starts at 1
            if (getFormVariables(taskId).size > 1) {
                logger.warn("Data of task with id $taskId will be overwritten")
            }
            cmmnTaskService.setVariablesLocal(taskId, data)
            logger.info("Saved data $data for task with id $taskId")
        } catch (exception: FlowableObjectNotFoundException) {
            throw FormNotFoundException("Cannot find task with taskId $taskId", exception)
        }
    }

    override fun completeTaskFormData(taskId: String, data: Map<String, Any?>) {
        val taskDto = taskService.getTaskDtoById(taskId)

        try {
            // an "empty" dataset will still contain the initiator user, therefore size starts at 1
            if (getFormVariables(taskId).size > 1) {
                logger.warn("Data of task with id $taskId will be overwritten")
            }
            cmmnTaskService.setVariablesLocal(taskId, data)
            taskService.completeOrResolveTask(taskDto, null)
            logger.info("Completed task with id $taskId by storing data $data")
        } catch (exception: FlowableObjectNotFoundException) {
            throw FormNotFoundException("Cannot find task with taskId $taskId", exception)
        }
    }

    override fun emptyFormVariables(taskId: String): Map<String, Any?> {
        try {
            val formDefinition = cmmnTaskService.getTaskFormModel(taskId)
            return mapFormModelToObject(formDefinition.formModel)
        } catch (exception: FlowableObjectNotFoundException) {
            throw FormNotFoundException("Cannot find form for task with $taskId", exception)
        }
    }

    private fun mapFormModelToObject(formModel: FormModel): Map<String, Any?> {
        return if (formModel is SimpleFormModel) {
            formModel.listAllFields().asSequence()
                .map { it.name to null }
                .toMap()
        } else {
            mapOf()
        }

    }

    override fun getFormVariables(taskId: String): Map<String, Any?> {
        try {
            return cmmnTaskService.getVariableInstancesLocal(taskId).entries.asSequence()
                .map { it.key to it.value.value }
                .toMap()
        } catch (exception: FlowableObjectNotFoundException) {
            throw FormNotFoundException(
                "Cannot find form for task with taskId $taskId, or the task has been completed", exception
            )
        }
    }

    private fun mapFormModelToMetadataList(formModel: FormModel): List<ColumnDef> {
        return if (formModel is SimpleFormModel) {
            formModel.listAllFields().asSequence()
                .map { mapToColumnMetadata(it) }
                .toList()
        } else {
            listOf()
        }
    }

    /**
     * NB: the mapping of Flowable form fields is not consistent with the qualidify ColumnDef naming
     * the FormField id becomes the ColumnDef name and becomes an html identifier
     * the FormField name becomes the ColumnDef description and is also used as a label when displaying the formField
     */
    private fun mapToColumnMetadata(field: FormField): ColumnDef {
        return ColumnDef(
            name = field.id,
            description = field.name,
            dataType = mapToTypeMetadata(field),
            fieldAccess = ColumnDef.FieldAccess.MAP,
            attributes = mapOf(
                "label" to field.name,
                "placeholder" to field.placeholder,
                "isRequired" to field.isRequired.toString(),
            )
        )
    }

    private fun mapToTypeMetadata(field: FormField) =
        when (field.type) {
            "text" -> DataType.STRING
            "multi-line-text" -> DataType.TEXT
            "integer" -> DataType.INTEGER
            "boolean" -> DataType.BOOLEAN
            "date" -> DataType.DATE
            "dropdown" -> DataType.COLLECTION
            "radio-buttons" -> TODO()
            "people" -> TODO()
            "functional-group" -> TODO()
            "upload" -> DataType.BINARY
            "expression" -> TODO()
            else -> {
                logger.warn("Unknown formfield type ${field.type} - mapping to textfield")
                DataType.STRING
            }
        }
}

