package com.qualidify.service.impl.process

import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.flowable.BpmnDefinitionJpa
import com.qualidify.service.impl.process.repositories.BpmnDefinitionRepository
import com.qualidify.service.process.BpmnDefinitionService
import com.qualidify.service.process.dto.BusinessProcessDefinitionDto
import org.flowable.engine.ProcessEngine
import org.springframework.stereotype.Service

/**
 * An implementation of the [BpmnDefinitionService], to retrieve the [BusinessProcessDefinitionDto] from Flowable
 *
 * @author Menno Tol
 * @author Marcel Pot
 * @since 1.0
 */
@Service
open class BpmnDefinitionServiceImpl(
    private val bpmnDefinitionRepository: BpmnDefinitionRepository,
    private val processEngine: ProcessEngine,
) : AbstractQueryableService<BpmnDefinitionJpa, BusinessProcessDefinitionDto>(bpmnDefinitionRepository),
    BpmnDefinitionService {

    override fun mapEntityToDto(entity: BpmnDefinitionJpa) = BusinessProcessDefinitionDto(
        id = entity.id,
        key = entity.key,
        name = entity.name,
        version = entity.version,
        graphicalNotation = entity.hasGraphicalNotation
    )

    override fun startProcess(dto: BusinessProcessDefinitionDto) {
        processEngine.runtimeService.startProcessInstanceById(dto.id)
    }


}