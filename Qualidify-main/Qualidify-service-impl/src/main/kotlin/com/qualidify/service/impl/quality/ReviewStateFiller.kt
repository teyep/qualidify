package com.qualidify.service.impl.quality

import com.qualidify.service.impl.quality.exceptions.UnsupportedQualityShellException
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.process.dto.CaseInstanceDto
import com.qualidify.service.quality.ReviewState
import org.flowable.cmmn.api.runtime.CaseInstance
import org.springframework.stereotype.Component

@Component
class ReviewStateFiller(
    private val planItemService: PlanItemService,
    private val caseInstanceService: CaseInstanceService,
) {

    fun fill(qualityShell: CaseInstance, regularCase: CaseInstance) {

        // check that case instances are correct
        val qualityShellDto: CaseInstanceDto = caseInstanceService.getCaseInstanceDtoById(qualityShell.id)
        val regularCaseDto: CaseInstanceDto = caseInstanceService.getCaseInstanceDtoById(regularCase.id)
        if (!qualityShellDto.isQualityShell) {
            throw UnsupportedQualityShellException("Wrong 'isQualityShell' label for quality shell [$qualityShellDto].")
        }
        if (regularCaseDto.isQualityShell) {
            throw UnsupportedQualityShellException("Wrong 'isQualityShell' label for regular case [$regularCaseDto].")
        }

        val qualityShellPlanItemIds = planItemService.getPlanItemIdsByCase(qualityShell.id)
        // for quality items review is not required
        setReviewState(qualityShellPlanItemIds, ReviewState.REVIEW_NOT_REQUIRED)

        // TODO: make setting review state configurable (QUAL-500)
        val regularCasePlanItemIds = planItemService.getPlanItemIdsByCase(regularCase.id)
        // for regular items review is required
        setReviewState(regularCasePlanItemIds, ReviewState.REVIEW_REQUIRED)

    }

    private fun setReviewState(planItemIds: List<String>, reviewState: ReviewState) =
        planItemIds.forEach {
            planItemService.setPlanItemVariable(it, "reviewState", reviewState.state)
        }

}