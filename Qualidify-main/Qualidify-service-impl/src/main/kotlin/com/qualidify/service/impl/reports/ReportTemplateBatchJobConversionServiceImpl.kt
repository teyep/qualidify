package com.qualidify.service.impl.reports

import com.fasterxml.jackson.databind.ObjectMapper
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.model.entities.BatchJobStatus
import com.qualidify.model.entities.BatchJobType
import com.qualidify.service.report.ReportBatchJobParameters
import com.qualidify.service.report.ReportTemplateBatchJobConversionService
import com.qualidify.service.report.dto.BatchJobDto
import com.qualidify.service.report.dto.ReportTemplateDto
import org.springframework.stereotype.Service
import java.util.*

/**
 * Default implementation of [ReportTemplateBatchJobConversionService]
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Service
class ReportTemplateBatchJobConversionServiceImpl(
    private val objectMapper: ObjectMapper
) : ReportTemplateBatchJobConversionService {

    override fun convert(reportTemplate: ReportTemplateDto, parameters: ObjectMap): BatchJobDto {
        return BatchJobDto(
            batchJobId = UUID.randomUUID(),
            jobType = BatchJobType.JasperReportJob,
            jobName = reportTemplate.name,
            parameters = objectMapper.writeValueAsString(ReportBatchJobParameters(reportTemplate.id, parameters)),
            status = BatchJobStatus.NEW.toString(),
            finishedDate = null
        )
    }
}
