package com.qualidify.service.impl.reports

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.ReportServiceResource
import com.qualidify.model.entities.ReportTemplate
import com.qualidify.service.report.dto.ReportTemplateDto
import com.qualidify.service.report.dto.ReportTemplateResourceDto

@DtoMapper
class ReportServiceTemplateDtoMapper : EntityDtoMapper<ReportTemplate, ReportTemplateDto> {

    override fun convertToModel(dto: ReportTemplateDto): ReportTemplate {
        return ReportTemplate(
            dto.id,
            dto.name,
            dto.description,
            dto.content,
            dto.parameters,
            mutableSetOf()
        )
    }

    private fun convertResourceToDto(resource: ReportServiceResource): ReportTemplateResourceDto {
        return ReportTemplateResourceDto(resource.resourceName, resource.resourceData.copyOf())
    }

    override fun convertToDto(entity: ReportTemplate): ReportTemplateDto {
        return ReportTemplateDto(
            entity.id.unbox(),
            entity.templateName,
            entity.templateDescription,
            entity.templateData.copyOf(),
            entity.templateParameters,
            entity.resources.map { convertResourceToDto(it) }.toTypedArray()
        )
    }
}
