package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.quality.TaskReviewService
import org.flowable.cmmn.api.delegate.DelegatePlanItemInstance
import org.flowable.cmmn.api.listener.PlanItemInstanceLifecycleListener
import org.flowable.cmmn.api.runtime.PlanItemInstance
import org.springframework.stereotype.Component

/**
 * Listener required for starting quality actions of task-type plan items.
 * The listener is added to the engine using the [FlowableConfiguration] class.
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
class TaskCompletedListener(private val taskReviewService: TaskReviewService) : PlanItemInstanceLifecycleListener {

    private val logger by LoggerDelegation()

    override fun getSourceState(): String = "active"

    override fun getTargetState(): String = "completed"

    override fun stateChanged(planItemInstance: DelegatePlanItemInstance, oldState: String, newState: String) {

        if (taskChecker(planItemInstance)) {
            logger.info(
                "Publishing state changed event for task '${planItemInstance.planItemDefinition}'. " +
                        "Task has changed from '$oldState' to '$newState'."
            )
            taskReviewService.startReview(planItemInstance)
        }
    }

    private fun taskChecker(planItemInstance: PlanItemInstance): Boolean {
        val excludedType = setOf(
            PlanItemType.GENERIC_EVENT_LISTENER.type,
            PlanItemType.SIGNAL_EVENT_LISTENER.type,
            PlanItemType.TIMER_EVENT_LISTENER.type,
            PlanItemType.USER_EVENT_LISTENER.type,
            PlanItemType.VARIABLE_EVENT_LISTENER.type,
            PlanItemType.STAGE.type,
            PlanItemType.MILESTONE.type,
            PlanItemType.PLAN_FRAGMENT.type
        )

        // very basic check based on plan item definition type
        return planItemInstance.planItemDefinitionType !in excludedType
    }

}

