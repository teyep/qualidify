package com.qualidify.service.impl.projects

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.CmmnTaskJpa
import java.util.*


/**
 * Read only repository for [CmmnTaskJpa]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface CmmnTaskRepository : ReadOnlyQueryablePagingAndSortingRepository<CmmnTaskJpa, String> {


    /**
     * Find a task based on its corresponding planItemInstanceId
     * @param [planItemInstanceId] the requested planItemInstanceId
     * @return the task with the planItemInstanceId
     */
    fun findByPlanItemInstanceId(planItemInstanceId: String): Optional<CmmnTaskJpa>

    /**
     * Count all tasks that have a parent stage in common
     * @param [stageId] the id of the requested stageId
     * @return the number of tasks with stageId
     */
    fun countByStageEquals(stageId: String): Long

    /**
     * Find all tasks that have a parent stage in common
     * @param [stageId] the id of the requested stage
     * @return a collection of tasks with the stageId
     */
    fun findAllByStageEquals(stageId: String): List<CmmnTaskJpa>

}