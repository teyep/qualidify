package com.qualidify.service.impl.reports

import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.BatchJob
import com.qualidify.model.entities.BatchJobId

/**
 * Repository for persisting and retrieving [BatchJob] objects from the data storage
 * Note: There's an equally-named BatchJobRepository in Qualidify-reports
 *
 * @author Niels Visscher
 * @since 1.0
 */
interface BatchJobRepository : QueryablePagingAndSortingRepository<BatchJob, BatchJobId> {
}
