package com.qualidify.service.impl.configuration

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.ConfigurationCode
import com.qualidify.model.entities.ConfigurationDomain
import com.qualidify.service.configuration.dto.ConfigurationCodeDto
import java.util.stream.Collectors

@DtoMapper
open class ConfigurationCodeDtoMapper : EntityDtoMapper<ConfigurationCode, ConfigurationCodeDto> {

    override fun convertToModel(dto: ConfigurationCodeDto): ConfigurationCode {
        return ConfigurationCode(dto.domain, dto.code, dto.description, dto.dataType, dto.value, dto.active)
    }

    override fun convertToDto(entity: ConfigurationCode): ConfigurationCodeDto {
        return ConfigurationCodeDto(
            entity.configurationDomain!!.id.unbox(),
            entity.code,
            entity.description,
            entity.dataType,
            entity.value,
            entity.active
        )
    }

    fun toDto(domain: ConfigurationDomain): List<ConfigurationCodeDto> {
        return domain.codes.stream()
            .map { entity: ConfigurationCode -> this.convertToDto(entity) }
            .collect(Collectors.toList())
    }
}