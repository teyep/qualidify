package com.qualidify.service.impl.process

import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.flowable.FormDefinitionJpa
import com.qualidify.service.impl.process.repositories.FormDefinitionRepository
import com.qualidify.service.process.FormDefinitionService
import com.qualidify.service.process.dto.FormDefinitionDto
import com.qualidify.service.process.dto.FormDefinitionUpload
import org.flowable.form.api.FormRepositoryService
import org.springframework.stereotype.Service

/**
 * An implementation of the [FormDefinitionService] interface
 * , to retrieve the [FormDefinitionDto] from Flowable
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
open class FormDefinitionServiceImpl(
    private val formDefinitionRepository: FormDefinitionRepository,
    private val formRepositoryService: FormRepositoryService,
) : AbstractQueryableService<FormDefinitionJpa, FormDefinitionDto>(formDefinitionRepository), FormDefinitionService {

    override fun mapEntityToDto(entity: FormDefinitionJpa) = FormDefinitionDto(
        id = entity.id,
        name = entity.name,
        key = entity.key,
        version = entity.version
    )

    override fun create(command: FormDefinitionUpload): FormDefinitionDto {
        val deployment = formRepositoryService.createDeployment()
            .addFormBytes(command.file.name, command.file.bytes)
            .deploy()

        return formDefinitionRepository.findByDeploymentId(deploymentId = deployment.id)
            .asSequence()
            .map { mapEntityToDto(it) }
            .first()

    }
}

