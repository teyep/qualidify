package com.qualidify.service.impl.security

import com.qualidify.core.jpa.annotations.SoftDelete
import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.SecurityGroup
import com.qualidify.model.entities.SecurityGroupId
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * Repository for securitygroups
 *
 * @author Marcel Pot
 * @since 1.0
 */
@SoftDelete
interface SecurityGroupRepository : QueryablePagingAndSortingRepository<SecurityGroup, SecurityGroupId> {

    @Query(
        "SELECT DISTINCT sg.* FROM securitygroup sg " +
                "LEFT JOIN user_securitygroup usg ON usg.securitygroup_id=sg.id " +
                "LEFT JOIN users u ON u.id = usg.user_id " +
                "WHERE u.username = :userName ", nativeQuery = true
    )
    fun findAllByUserName(@Param("userName") userName: String): List<SecurityGroup>

    @Query(
        "SELECT DISTINCT sg.* FROM securitygroup sg " +
                "LEFT JOIN user_securitygroup usg ON usg.securitygroup_id=sg.id " +
                "LEFT JOIN users u ON u.id = usg.user_id " +
                "WHERE u.username in :userNames ", nativeQuery = true
    )
    fun findAllByUserNames(@Param("userNames") userName: Collection<String>): List<SecurityGroup>

}