package com.qualidify.service.impl.security

import com.qualidify.service.security.SessionService
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
class SessionServiceImpl(
    val authenticationManager: ReactiveAuthenticationManager
) : SessionService {
    override fun login(userName: String, password: String): Mono<Boolean> {
        return authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                userName, password
            )
        ).flatMap {  details ->
            ReactiveSecurityContextHolder.getContext().map { it.authentication = details }
        }
            .thenReturn(true)
    }

    override fun logout(): Mono<Boolean> {
        return ReactiveSecurityContextHolder.getContext()
            .map {
                it.authentication = null
                true
            }
    }

    override fun isLoggedIn(): Mono<Boolean> {
        return ReactiveSecurityContextHolder.getContext()
            .map {
                it.authentication != null &&
                        it.authentication !is AnonymousAuthenticationToken
            }
            .switchIfEmpty(false.toMono())
    }
}