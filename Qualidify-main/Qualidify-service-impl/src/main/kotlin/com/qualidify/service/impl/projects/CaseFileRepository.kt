package com.qualidify.service.impl.projects

import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.model.entities.CaseFile
import java.util.*

interface CaseFileRepository : QueryablePagingAndSortingRepository<CaseFile, UUID>