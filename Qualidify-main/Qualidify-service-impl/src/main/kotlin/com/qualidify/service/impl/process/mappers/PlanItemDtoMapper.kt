package com.qualidify.service.impl.process.mappers

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.flowable.definition.CmmnPlanItemJpaDef
import com.qualidify.service.process.dto.PlanItemDto
import com.qualidify.service.process.dto.PlanItemState
import com.qualidify.service.process.dto.PlanItemType
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState

@DtoMapper
class PlanItemDtoMapper : EntityDtoMapper<CmmnPlanItemJpaDef, PlanItemDto> {

    override fun convertToModel(dto: PlanItemDto): CmmnPlanItemJpaDef {
        TODO("Not yet implemented")
    }

    override fun convertToDto(entity: CmmnPlanItemJpaDef): PlanItemDto =
        PlanItemDto(
            id = entity.id,
            name = entity.localizedName ?: entity.name,
            caseInstanceId = entity.caseInstanceId,
            projectInstanceId = entity.projectInstanceId ?: "PROJECT_INSTANCE_NOT_AVAILABLE",
            stage = entity.stage,
            reference = entity.reference,
            type = PlanItemType.getByName(entity.definitionType),
            state = PlanItemState.getByName(entity.state),
            completable = entity.completable,
            timeCreated = entity.timeCreated,
            historic = false,
            isQualityShell = entity.isQualityShell,
            regularCaseId = entity.regularCaseId,
            qualityShellId = entity.qualityShellId,
            qualityLinkKey = QualityLinkKey.getByName(entity.qualityLinkKey),
            qualityLinkId = entity.qualityLinkId,
            reviewState = ReviewState.getByName(entity.reviewState),
        )


}