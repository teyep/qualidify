package com.qualidify.service.impl.process.mappers

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.ProjectTemplate
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.qualidify.service.projects.dto.ProjectTemplateDto

@DtoMapper
class ProjectTemplateDtoMapper(
    private val cmmnCaseDefinitionRepository: CmmnCaseDefinitionRepository,
) : EntityDtoMapper<ProjectTemplate, ProjectTemplateDto> {

    override fun convertToModel(dto: ProjectTemplateDto): ProjectTemplate {
        TODO("Not yet implemented")
    }

    override fun convertToDto(entity: ProjectTemplate): ProjectTemplateDto {
        val caseDefinition = cmmnCaseDefinitionRepository.findByKey(entity.cmmnModel)

        /**
         * Manual mapping of the embedded caseDefinitionDto's graphical notation to null
         * to prevent sending an image with all parent CaseInstanceDto's
         * (which are included in PlanItems, Tasks etc.).
         * If required the graphics can be obtained via the deploymentResourceId
         */
        return ProjectTemplateDto(
            id = entity.id.unbox().toString(),
            name = entity.name,
            description = entity.description,
            caseDefinition = CaseDefinitionDto(
                id = caseDefinition.id,
                name = caseDefinition.name,
                key = caseDefinition.key,
                version = caseDefinition.version,
                deploymentResourceId = caseDefinition.deploymentResource?.deploymentId,
                graphicalNotation = null
            ),
            cmmnModel = caseDefinition.key,
            variables = entity.variables.asSequence()
                .map {
                    ProjectTemplateDto.ProjectTemplateVariableDto(
                        name = it.name,
                        description = it.description,
                        datatype = it.datatype,
                        value = it.defaultValue,
                        required = it.required,
                        userInput = it.userInput
                    )
                }
                .toList(),
            fields = entity.customFields.asSequence()
                .map { it.field to it.value }
                .toMap().toMutableMap()
        )
    }

}