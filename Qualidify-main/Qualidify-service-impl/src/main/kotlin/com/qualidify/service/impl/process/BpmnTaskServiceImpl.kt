package com.qualidify.service.impl.process


import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.flowable.BpmnTaskJpa
import com.qualidify.service.impl.process.repositories.BpmnTaskRepository
import com.qualidify.service.process.BpmnTaskService
import com.qualidify.service.process.dto.BusinessProcessTaskDto
import org.springframework.stereotype.Service


/**
 * An implementation of the [BpmnTaskService], to retrieve the [BusinessProcessTaskDto] from Flowable
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Service
open class BpmnTaskServiceImpl(
    private val bpmnTaskRepository: BpmnTaskRepository,
) : AbstractQueryableService<BpmnTaskJpa, BusinessProcessTaskDto>(bpmnTaskRepository), BpmnTaskService {

    override fun mapEntityToDto(entity: BpmnTaskJpa) = BusinessProcessTaskDto(
        id = entity.id,
        name = entity.name,
        owner = entity.owner,
        assignee = entity.assignee,
        type = entity.type ?: "bpmn"  // Elvis operator cannot be removed!
    )
}

