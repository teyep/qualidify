package com.qualidify.service.impl.document

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchCriterium
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.model.entities.Document
import com.qualidify.model.entities.DocumentAttribute
import com.qualidify.model.entities.DocumentContent
import com.qualidify.model.entities.DocumentId
import com.qualidify.service.document.DocumentService
import com.qualidify.service.document.dto.DocumentContentDto
import com.qualidify.service.document.dto.DocumentDto
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
open class DocumentServiceImpl(
    val documentRepository: DocumentRepository,
    val labelRepository: LabelRepository,
    val documentContentRepository: DocumentContentRepository,
    val documentMapper: DocumentMapper,
    val labelMapper: LabelMapper,
    val documentContentMapper: DocumentContentMapper,
) : DocumentService {

    override fun createDocument(path: String, name: String, type: String, content: ByteArray) =
        createDocument(path, name, type, 1, emptyMap(), content)

    override fun createDocument(
        path: String,
        name: String,
        type: String,
        attributes: Map<String, String?>,
        content: ByteArray,
    ) = createDocument(path, name, type, 1, attributes, content)

    private fun createDocument(
        path: String,
        name: String,
        type: String,
        version: Long,
        attributes: Map<String, String?>,
        content: ByteArray,
    ): DocumentDto {
        val document = Document(
            id = DocumentId(UUID.randomUUID()),
            path = path,
            name = name,
            type = type,
            version = version,
            size = content.size.toLong(),
            attributes = attributes.asSequence()
                .map { DocumentAttribute(it.key, it.value) }
                .toSet()
        )
        val documentContent = DocumentContent(
            id = document.id,
            content = content
        )
        return innerSave(document, documentContent)
    }

    override fun createDocument(document: DocumentDto, content: DocumentContentDto): DocumentDto {
        return createDocument(document.path, document.name, document.type, document.attributes, content.content)
    }

    override fun updateDocument(path: String, name: String, type: String, content: ByteArray): DocumentDto {

        val previousDocument = retrieveOptionalDocument(path, name, type)
            .orElseThrow { DocumentNotFoundException("The document $path$name.$type cannot be found") }
            .also { it.deleted = true }

        documentRepository.save(previousDocument)
        return createDocument(
            path = path,
            name = name,
            type = type,
            version = previousDocument.version + 1,
            attributes = previousDocument.attributes.asSequence()
                .map { it.key to it.value }
                .toMap(),
            content = content)
    }

    override fun updateDocument(document: DocumentDto, content: DocumentContentDto): DocumentDto {
        return updateDocument(document.path, document.name, document.type, content.content)
    }


    override fun getDocument(path: String, name: String, type: String): DocumentDto {
        return retrieveOptionalDocument(path, name, type)
            .map { documentMapper.convertToDto(it) }
            .orElseThrow { DocumentNotFoundException("The document $path$name.$type cannot be found") }
    }

    override fun getDocumentById(documentId: UUID): DocumentDto {
        return documentRepository.findById(DocumentId(documentId))
            .map { documentMapper.convertToDto(it) }
            .orElseThrow { DocumentNotFoundException("The Document for $documentId cannot be found") }
    }

    override fun getDocumentContent(documentId: UUID): DocumentContentDto {
        return documentContentRepository.findById(DocumentId(documentId))
            .map { documentContentMapper.convertToDto(it) }
            .orElseThrow { DocumentNotFoundException("The Document Content for $documentId cannot be found") }
    }

    override fun getDocumentContent(documentDto: DocumentDto) =
        getDocumentContent(documentDto.path, documentDto.name, documentDto.type)

    override fun getDocumentContent(path: String, name: String, type: String): DocumentContentDto {
        val document = retrieveDocument(path, name, type)
        return getDocumentContent(documentId = document.id.unbox())
    }

    private fun innerSave(
        document: Document,
        content: DocumentContent,
    ): DocumentDto {
        save(document)
        save(content)
        return documentMapper.convertToDto(document)
    }

    private fun save(document: Document) = documentRepository.save(document)

    private fun save(documentContent: DocumentContent) = documentContentRepository.save(documentContent)

    private fun retrieveOptionalDocument(
        path: String,
        name: String,
        type: String,
    ) = documentRepository.findDocumentByPathAndNameAndTypeAndDeletedIsFalse(path, name, type)

    private fun retrieveDocument(
        path: String,
        name: String,
        type: String,
    ) = retrieveOptionalDocument(path, name, type)
        .orElseThrow { DocumentNotFoundException("The document $path$name.$type cannot be found") }

    override fun getAllDocumentsInPath(path: String): Array<DocumentDto> {
        return documentRepository.findAllByPath(path).asSequence()
            .map { documentMapper.convertToDto(it) }
            .toList()
            .toTypedArray()
    }

    override fun getAllDocumentsByType(type: String): Array<DocumentDto> {
        return findByQuery(SearchQuery<DocumentDto>(0, 0, listOf(SearchCriterium("type" to type)))).toTypedArray()
    }

    override fun countByQuery(query: CountQuery) = documentRepository.countResponse(query)

    override fun findByQuery(query: SearchQuery): Collection<DocumentDto> {
        return documentRepository.findAllByQuery(query)
            .map { documentMapper.convertToDto(it) }
            .toList()
    }
}

class DocumentNotFoundException(message: String = "", cause: Throwable? = null) : RuntimeException(message, cause)