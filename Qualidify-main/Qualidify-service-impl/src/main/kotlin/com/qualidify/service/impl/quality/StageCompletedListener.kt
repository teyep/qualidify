package com.qualidify.service.impl.quality

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.quality.StageReviewService
import org.flowable.cmmn.api.delegate.DelegatePlanItemInstance
import org.flowable.cmmn.api.listener.PlanItemInstanceLifecycleListener
import org.flowable.cmmn.api.runtime.PlanItemInstance
import org.springframework.stereotype.Component

/**
 * Listener required for starting quality actions of stage-type plan items.
 * The listener is added to the engine using the [FlowableConfiguration] class.
 *
 * @author Menno Tol
 * @since 1.0
 */
@Component
class StageCompletedListener(private val stageReviewService: StageReviewService) : PlanItemInstanceLifecycleListener {

    private val logger by LoggerDelegation()

    override fun getSourceState(): String = "active"

    override fun getTargetState(): String = "completed"

    override fun stateChanged(planItemInstance: DelegatePlanItemInstance, oldState: String, newState: String) {

        if (stageChecker(planItemInstance)) {
            logger.info(
                "Publishing state changed event for stage ${planItemInstance.planItemDefinition}. " +
                        "Stage has changed from $oldState to $newState"
            )
            stageReviewService.startReview(planItemInstance)
        }
    }

    // very basic check based on plan item definition type
    private fun stageChecker(planItemInstance: PlanItemInstance): Boolean {
        return planItemInstance.planItemDefinitionType == "stage"
    }

}

