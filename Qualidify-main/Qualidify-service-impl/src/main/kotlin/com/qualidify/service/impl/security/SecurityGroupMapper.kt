package com.qualidify.service.impl.security

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.model.entities.SecurityGroup
import com.qualidify.model.entities.SecurityRole
import com.qualidify.service.security.dto.SecurityGroupDto
import java.util.stream.Collectors

@DtoMapper
open class SecurityGroupMapper : EntityDtoMapper<SecurityGroup, SecurityGroupDto> {

    override fun convertToModel(dto: SecurityGroupDto): SecurityGroup {
        val roles = dto.roles.stream()
            .map { SecurityRole(it) }
            .collect(Collectors.toSet())
        return SecurityGroup(dto.name, dto.description, roles, dto.enabled)
    }

    override fun convertToDto(entity: SecurityGroup): SecurityGroupDto {
        return SecurityGroupDto(entity.id.unbox(),
            entity.description,
            entity.enabled,
            entity.roles.stream()
                .map { it.name }
                .collect(Collectors.toSet()))
    }


}