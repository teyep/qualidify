package com.qualidify.service.impl.process.repositories

import com.qualidify.core.query.repository.ReadOnlyQueryablePagingAndSortingRepository
import com.qualidify.model.entities.flowable.CmmnCaseInstanceJpa

/**
 * Read only repository fo [CmmnCaseInstanceJpa]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface CmmnCaseInstanceRepository : ReadOnlyQueryablePagingAndSortingRepository<CmmnCaseInstanceJpa, String> {
}