package com.qualidify.service.impl.process

import com.qualidify.core.query.service.AbstractQueryableService
import com.qualidify.model.entities.flowable.CmmnCaseDefinitionJpa
import com.qualidify.service.impl.process.mappers.CaseDefinitionDtoMapper
import com.qualidify.service.impl.process.mappers.CaseInstanceDtoMapper
import com.qualidify.service.impl.process.repositories.CmmnCaseDefinitionRepository
import com.qualidify.service.impl.process.repositories.CmmnCaseInstanceRepository
import com.qualidify.service.process.CaseDefinitionService
import com.qualidify.service.process.dto.CaseDefinitionDto
import com.qualidify.service.process.dto.CaseDefinitionUploadDto
import com.qualidify.service.process.dto.CaseInstanceDto
import org.flowable.cmmn.engine.CmmnEngine
import org.springframework.stereotype.Service
import java.io.BufferedReader

/**
 * An implementation of the [CaseDefinitionService] interface, to retrieve the [CaseDefinitionDto] from Flowable
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
@Service
open class CaseDefinitionServiceImpl(
    private val cmmnEngine: CmmnEngine,
    private val cmmnCaseDefinitionRepository: CmmnCaseDefinitionRepository,
    private val caseDefinitionDtoMapper: CaseDefinitionDtoMapper,
    private val cmmnCaseInstanceRepository: CmmnCaseInstanceRepository,
    private val caseInstanceDtoMapper: CaseInstanceDtoMapper
) : AbstractQueryableService<CmmnCaseDefinitionJpa, CaseDefinitionDto>(cmmnCaseDefinitionRepository),
    CaseDefinitionService {

    override fun mapEntityToDto(entity: CmmnCaseDefinitionJpa): CaseDefinitionDto =
        caseDefinitionDtoMapper.convertToDto(entity)

    override fun create(command: CaseDefinitionUploadDto): CaseDefinitionDto {
        val deployment = cmmnEngine.cmmnRepositoryService.createDeployment()
            .addBytes(command.file.name, command.file.bytes)
            .deploy()

        val caseDefinition = cmmnCaseDefinitionRepository.findById(deployment.id).get()

        return caseDefinitionDtoMapper.convertToDto(caseDefinition)
    }

    override fun startProject(dto: CaseDefinitionDto): CaseInstanceDto {
        val caseInstance = cmmnEngine.cmmnRuntimeService.createCaseInstanceBuilder()
            .caseDefinitionId(dto.id)
            .start()

        val cmmnCaseInstanceJpa = cmmnCaseInstanceRepository.findById(caseInstance.id).get()
        return caseInstanceDtoMapper.convertToDto(cmmnCaseInstanceJpa)
    }

    override fun upload(upload: CaseDefinitionUploadDto) {
        cmmnEngine.cmmnRepositoryService.createDeployment()
            .addBytes(upload.file.name, upload.file.bytes)
            .deploy()
    }

    override fun getXml(caseDefinitionKey: String): String {
        val definition = cmmnEngine.cmmnRepositoryService
            .createCaseDefinitionQuery().caseDefinitionKey(caseDefinitionKey).latestVersion().singleResult()

        return cmmnEngine.cmmnRepositoryService
            .getResourceAsStream(definition.deploymentId, definition.resourceName)
            .bufferedReader()
            .use(BufferedReader::readText)
    }

    override fun getCaseDefinitionDtoByKey(key: String): CaseDefinitionDto {
        val cmmnCaseDefinitionJpa = cmmnCaseDefinitionRepository.findByKey(key)

        return caseDefinitionDtoMapper.convertToDto(cmmnCaseDefinitionJpa)
    }

}

