package com.qualidify.service.impl.quality

import com.qualidify.model.entities.flowable.*
import com.qualidify.service.impl.quality.exceptions.UnsupportedQualityLinkKeyException
import com.qualidify.service.process.CaseInstanceService
import com.qualidify.service.process.PlanItemService
import com.qualidify.service.quality.QualityLinkKey
import com.qualidify.service.quality.ReviewState
import org.flowable.cmmn.api.runtime.CaseInstance

/**
 * Obtains the review state of the item.
 * @param service the service that is used to get the variable where the review state is stored
 * @param entity any supported entity
 * @return the [ReviewState] if stored in the variable, or [ReviewState.UNDEFINED] if none can be found
 *
 * @author Menno Tol
 * @since 1.0
 */
fun getReviewState(service: Any, entity: Any): ReviewState {
    val reviewState = when (entity) {
        is CmmnPlanItemJpa -> {
            (service as PlanItemService).getPlanItemVariable(entity.id, "reviewState")
        }

        is CmmnTaskJpa -> {
            (service as PlanItemService).getPlanItemVariable(entity.planItemInstanceId, "reviewState")
        }

        is CmmnHistoricPlanItemJpa -> {
            (service as PlanItemService).getHistoricPlanItemVariable(entity.id, "reviewState")
                .value as? String
        }

        is CmmnCaseInstanceJpa -> {
            (service as CaseInstanceService).getCaseVariable(entity.id, "reviewState")
        }

        is CmmnHistoricCaseInstanceJpa -> {
            (service as CaseInstanceService).getHistoricCaseVariable(entity.id, "reviewState")
                .value as? String
        }

        is CaseInstance -> {
            (service as CaseInstanceService).getCaseVariable(entity.id, "reviewState")
        }

        else -> throw QualityNotSupportedException("Entity '$entity' does not support the required quality actions.")
    }

    return if (reviewState == null) {
        ReviewState.UNDEFINED
    } else {
        ReviewState.getByName(reviewState as String)
    }
}

/**
 * Obtains the id of the linked quality item (e.g. case instance, plan item, task) if there exists one.
 * @param service the service that is used to get the variable where the quality link id is stored
 * @param entity any supported entity
 * @param isQualityShell a boolean indicating if the item is a quality or regular item
 * @return a string with the id of the linked task or null if none can be found
 *
 * @throws UnsupportedQualityLinkKeyException if a combination of unsupported items is searched for
 * (e.g. a quality plan item cannot have another quality item as link)
 *
 * @author Menno Tol
 * @since 1.0
 */
fun getQualityLinkId(service: Any, entity: Any, isQualityShell: Boolean): String? {

    QualityLinkKey.values().forEach { qualityLinkKey ->
        val id = when (entity) {
            is CmmnPlanItemJpa -> {
                (service as PlanItemService).getPlanItemVariable(entity.id, qualityLinkKey.key)
            }

            is CmmnTaskJpa -> {
                (service as PlanItemService).getPlanItemVariable(entity.planItemInstanceId, qualityLinkKey.key)
            }

            is CmmnHistoricPlanItemJpa -> {
                (service as PlanItemService).getHistoricPlanItemVariable(entity.id, qualityLinkKey.key)
                    .value as? String
            }

            is CmmnCaseInstanceJpa -> {
                (service as CaseInstanceService).getCaseVariable(entity.id, qualityLinkKey.key)
            }

            is CmmnHistoricCaseInstanceJpa -> {
                (service as CaseInstanceService).getHistoricCaseVariable(entity.id, qualityLinkKey.key)
                    .value as? String
            }

            is CaseInstance -> {
                (service as CaseInstanceService).getCaseVariable(entity.id, qualityLinkKey.key)
            }

            else -> throw QualityNotSupportedException("Entity '$entity' does not support the required quality actions.")
        }

        // check that no incorrect quality link key was used
        if (id != null && isQualityShell && qualityLinkKey == QualityLinkKey.REVIEW_TASK_PLAN_ITEM_ID) {
            throw UnsupportedQualityLinkKeyException("A quality item should have a review task as quality link.")
        }

        if (id != null && !isQualityShell && qualityLinkKey == QualityLinkKey.REGULAR_TASK_PLAN_ITEM_ID) {
            throw UnsupportedQualityLinkKeyException("A non-quality item should not have a regular task plan item as quality link.")
        }

        if (id != null && !isQualityShell && qualityLinkKey == QualityLinkKey.REGULAR_STAGE_PLAN_ITEM_ID) {
            throw UnsupportedQualityLinkKeyException("A non-quality item should not have a regular stage plan item as quality link.")
        }

        // if no exceptions occurred and id != null return the id
        if (id != null) return id
    }
    return null
}

fun getQualityLinkKey(caseInstanceService: CaseInstanceService, caseInstance: CaseInstance): QualityLinkKey {
    val keys = QualityLinkKey.getKeySet()

    val key = try {
        caseInstanceService.getCaseVariables(caseInstance.id)
            .keys
            .single { it in keys }
    } catch (exception: Exception) {
        // throw QualityLinkKeyNotFoundException("Key in global variable is not part of '$keys'")
    }

    return QualityLinkKey.getByName(key as? String)
}

class QualityNotSupportedException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)