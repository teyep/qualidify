package com.qualidify.configuration

import com.qualidify.service.impl.security.AllowEverythingRolePermissionEvaluator
import com.qualidify.service.impl.security.ReactiveUserAuthorizationServiceImpl
import com.qualidify.service.impl.security.SecuredRolePermissionEvaluator
import com.qualidify.service.security.CurrentUser
import com.qualidify.service.security.UserAuthorizationService
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.context.annotation.Scope
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import java.util.*

@Configuration
open class SecurityConfiguration(
    private val userAuthorizationService: UserAuthorizationService,
    private val reactiveUserAuthorizationServiceImpl: ReactiveUserAuthorizationServiceImpl
) {

    @Bean
    open fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    open fun currentUser(): CurrentUser {
        val username = userAuthorizationService.currentUserName
        val user =
            if (username != null) Optional.of(userAuthorizationService.loadUserByUsername(username)) else Optional.empty()
        return CurrentUser { user }
    }

    @Bean
    @Profile("!nosec")
    open fun rolePermissionEvaluator() = SecuredRolePermissionEvaluator()

    @Bean
    @Profile("nosec")
    open fun noRolePermissionEvaluator() = AllowEverythingRolePermissionEvaluator()

    @Bean
    open fun authenticationManager(): ReactiveAuthenticationManager {
        val authenticationManager =
            UserDetailsRepositoryReactiveAuthenticationManager(reactiveUserAuthorizationServiceImpl)
        authenticationManager.setPasswordEncoder(passwordEncoder())
        return authenticationManager
    }

}
