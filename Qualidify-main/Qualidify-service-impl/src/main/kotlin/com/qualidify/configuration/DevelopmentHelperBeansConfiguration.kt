package com.qualidify.configuration

import com.qualidify.core.log.LoggerDelegation
import org.h2.tools.Server
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import java.sql.SQLException

@Profile("dev")
@Configuration
open class DevelopmentHelperBeansConfiguration {

    val logger by LoggerDelegation()

    @Bean(initMethod = "start", destroyMethod = "stop")
    @Throws(SQLException::class)
    open fun inMemoryH2DatabaseServer(): Server? {
        val port = 9090
        logger.info("In-memory H2 database server started on port: '$port'")
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "$port")
    }

}