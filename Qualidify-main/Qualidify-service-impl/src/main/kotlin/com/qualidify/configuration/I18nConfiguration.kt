package com.qualidify.configuration

import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ResourceBundleMessageSource

/**
 * Configure Spring beans for Internationalization (I18n)
 *
 * @author Marcel Pot
 */
@Configuration
open class I18nConfiguration {
    private val RESOURCE_BUNDLE = "i18n/messages"

    /**
     * Configuration of the message source as spring bean
     *
     * @return the MessageSource
     */
    @Bean
    open fun i18nMessageSource(): MessageSource {
        val messageSource = ResourceBundleMessageSource()
        messageSource.setBasename(RESOURCE_BUNDLE)
        messageSource.setDefaultEncoding("UTF-8")
        return messageSource
    }
}