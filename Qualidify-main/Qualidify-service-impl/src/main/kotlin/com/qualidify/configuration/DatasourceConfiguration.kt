package com.qualidify.configuration


import com.qualidify.core.db.multitenancy.MultiTenantDatasource
import com.qualidify.core.db.multitenancy.MultiTenantLiquibase
import com.qualidify.core.db.multitenancy.TenantsProperties
import org.springframework.beans.factory.support.DefaultListableBeanFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.DependsOn
import org.springframework.core.env.Environment
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource
import java.io.IOException
import javax.sql.DataSource

/**
 * Bean configuration for Datasources, Liquibase and Database Initializers
 * @author Marcel Pot
 * @since 1.0
 */
@Configuration
//@RefreshScope
@EnableConfigurationProperties(TenantsProperties::class)
open class DatasourceConfiguration(
    val tenantsProperties: TenantsProperties,
    val factory: DefaultListableBeanFactory,
    val environment: Environment
) {

    private val dataSources: MutableMap<Any, Any> = HashMap()

    init {
        resolveDataSources()
    }

    /**
     * Create a [MultiTenantDatasource] based on the properties configured
     * at [TenantsProperties]
     * @return a DataSource Bean
     */
    @Bean
    open fun dataSource(): DataSource {
        val datasource: AbstractRoutingDataSource = MultiTenantDatasource()
        datasource.setTargetDataSources(dataSources)
        datasource.setDefaultTargetDataSource(determineDefaultDatasource())
        datasource.afterPropertiesSet()
        return datasource
    }


    /**
     * This bean handles the database modifications described in a liquibase
     * xml configuration file
     * @param[dataSource] The default datasource
     * @return a SpringLiquibase bean
     */
    @Bean
    @DependsOn(value = ["dataSource"])
    open fun liquibase(dataSource: DataSource): MultiTenantLiquibase {
        val liquibase = MultiTenantLiquibase()
        liquibase.dataSource = dataSource
        liquibase.changeLog = "classpath:db/liquibase-changelog.xml"
        liquibase.tenants = dataSources.entries.map { it.key as String }
        liquibase.setShouldRun(true)
        liquibase.beanName = "liquibase"
        liquibase.contexts = getActiveSpringProfiles()
        return liquibase

    }

    private fun getActiveSpringProfiles() = (environment.activeProfiles + environment.defaultProfiles)
        .joinToString(separator = ",")

    private fun resolveDataSources() {
        for (entry in tenantsProperties.tenants.entries) {
            val dataSourceBuilder = DataSourceBuilder.create()

            val tenant = entry.key
            try {

                val datasourceProps = entry.value.datasource!!

                dataSourceBuilder.driverClassName(datasourceProps.driverClassName)
                dataSourceBuilder.username(datasourceProps.username)
                dataSourceBuilder.password(datasourceProps.password)
                dataSourceBuilder.url(datasourceProps.url)

                val dataSource = dataSourceBuilder.build()

                dataSources[tenant.uppercase()] = dataSource

                factory.registerSingleton("dataSource_$tenant", dataSource)

            } catch (npe: NullPointerException) {
                throw MultitenantDatasourceException("Datasource properties missing for Tenant $tenant", npe)
            } catch (ioe: IOException) {
                throw MultitenantDatasourceException("Problem with connection to Tenant $tenant", ioe)
            }
        }
    }

    private fun determineDefaultDatasource(): DataSource {
        try {
            return dataSources[defaultTenant()] as DataSource
        } catch (npe: NullPointerException) {
            throw MultitenantDatasourceException("Default Tenant is not configured!", npe)
        }
    }

    private fun defaultTenant() = tenantsProperties.defaultTenant.uppercase()

}

class MultitenantDatasourceException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause) {

}
