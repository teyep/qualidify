package com.qualidify.configuration


import com.qualidify.core.log.LoggerDelegation
import com.qualidify.service.impl.process.events.FlowableSpringEventRepublisher
import com.qualidify.service.impl.quality.CaseCompletedListener
import com.qualidify.service.impl.quality.StageCompletedListener
import com.qualidify.service.impl.quality.TaskCompletedListener
import com.qualidify.service.security.UserAuthorizationService
import org.flowable.cmmn.engine.CmmnEngineConfiguration
import org.flowable.common.engine.impl.interceptor.CommandContext
import org.flowable.idm.api.Group
import org.flowable.idm.api.GroupQuery
import org.flowable.idm.api.User
import org.flowable.idm.api.UserQuery
import org.flowable.idm.engine.IdmEngineConfiguration
import org.flowable.idm.engine.impl.GroupQueryImpl
import org.flowable.idm.engine.impl.IdmIdentityServiceImpl
import org.flowable.idm.engine.impl.UserQueryImpl
import org.flowable.idm.spring.SpringIdmEngineConfiguration
import org.flowable.spring.boot.EngineConfigurationConfigurer
import org.flowable.spring.security.GroupDetails
import org.flowable.spring.security.UserDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

/**
 * This class takes care of the configuration of Flowable
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Configuration
open class FlowableConfiguration {

    private val logger by LoggerDelegation()

    @Autowired
    open fun extraConfigurationCmmnEngineConfiguration(
        cmmnEngineConfiguration: CmmnEngineConfiguration,
        republisher: FlowableSpringEventRepublisher,
        taskCompletedListener: TaskCompletedListener,
        stageCompletedListener: StageCompletedListener,
        caseCompletedListener: CaseCompletedListener,
        ) {
        cmmnEngineConfiguration.addCaseInstanceLifeCycleListener(republisher)
        cmmnEngineConfiguration.addCaseInstanceLifeCycleListener(caseCompletedListener)
        cmmnEngineConfiguration.addPlanItemInstanceLifeCycleListener(stageCompletedListener)
        cmmnEngineConfiguration.addPlanItemInstanceLifeCycleListener(taskCompletedListener)
    }


    /**
     * This bean configures to use the identityService registered in spring context
     * as source for all the user en group questions
     * @param authorizationService the UserAuthorizationService
     * @return EngineConfigurationConfigurer
     */
    @Bean
    @Primary
    open fun idmEngineConfigurationConfigurer(authorizationService: UserAuthorizationService):
            EngineConfigurationConfigurer<SpringIdmEngineConfiguration> {
        return EngineConfigurationConfigurer<SpringIdmEngineConfiguration>
        { config: SpringIdmEngineConfiguration ->
            logger.info("Flowable Configuration injected Qualidify User Service.")
            config.idmIdentityService =
                QualidifyIdentityService(authorizationService, config)
        }
    }


}

/**
 * A Service for answering user and group questions from Flowable.
 * It will create user and group queries and those queries will deliver the result
 *
 * @author Marcel Pot
 * @since 1.0
 */
open class QualidifyIdentityService(
    val authorizationService: UserAuthorizationService,
    val config: IdmEngineConfiguration,
) : IdmIdentityServiceImpl(config) {

    private val logger by LoggerDelegation()

    override fun createUserQuery(): UserQuery {
        return QualidifyUserQuery(authorizationService)
    }

    override fun createGroupQuery(): GroupQuery {
        return QualidifyGroupQuery(authorizationService)
    }
}

/**
 * A Query class for users
 *
 * @author Marcel Pot
 * @since 1.0
 */
class QualidifyUserQuery(val authorizationService: UserAuthorizationService) : UserQueryImpl() {

    override fun executeCount(commandContext: CommandContext?): Long {
        return execute().size.toLong()
    }

    override fun executeList(commandContext: CommandContext?): List<User> {
        return execute()
    }

    private fun execute(): List<User> {
        val result: MutableList<User> = ArrayList()

        if (getId() != null) {
            Optional.ofNullable(authorizationService.loadUserById(UUID.fromString(id)))
                .map { mapper(it) }
                .ifPresent(result::add)
        } else if (getIds() != null) {
            result.addAll(
                authorizationService.loadUsersByIds(ids.map { UUID.fromString(it) })
                    .map { mapper(it) }
            )
        }

        return result
    }

    private fun mapper(user: UserDetails): User {
        return QFUser(user.username)
    }

}

/**
 * A Query class for groups
 *
 * @author Marcel Pot
 * @since 1.0
 */
class QualidifyGroupQuery(val authorizationService: UserAuthorizationService) : GroupQueryImpl() {

    val logger by LoggerDelegation()

    override fun executeCount(commandContext: CommandContext?): Long {
        return execute().size.toLong()
    }

    override fun executeList(commandContext: CommandContext?): List<Group> {
        return execute()
    }

    private fun execute(): List<Group> {
        val result: MutableList<Group> = ArrayList()

        if (getId() != null) {
            Optional.ofNullable(authorizationService.loadGroupByName(id))
                .map { mapper(it) }
                .ifPresent(result::add)
        } else if (getIds() != null) {
            result.addAll(
                authorizationService.loadGroupsByNames(ids)
                    .map { mapper(it) }

            )
        } else if (getUserId() != null) {
            authorizationService.loadGroupsByUserName(userId)
                .map { mapper(it) }
                .forEach(result::add)
        } else if (getUserIds() != null) {
            authorizationService.loadGroupsByUserNames(userIds)
                .map { mapper(it) }
                .forEach(result::add)
        }
        return result
    }

    private fun mapper(group: GrantedAuthority): Group {
        return QFGroup(group.authority)
    }


}

/**
 * Qualidify Flowable User implementation
 * @author Marcel Pot
 * @since 1.0
 */
data class QFUser(val name: String) : UserDto(name, "", "", "", name, "", "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is QFUser) return false

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}

/**
 * Qualidify Flowable Group implementation
 * @author Marcel Pot
 * @since 1.0
 */
data class QFGroup(val groupName: String) : GroupDetails(groupName, groupName, "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is QFGroup) return false

        if (groupName != other.groupName) return false

        return true
    }

    override fun hashCode(): Int {
        return groupName.hashCode()
    }
}


