package com.qualidify.configuration

import org.springframework.cache.CacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.convert.support.DefaultConversionService
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler
import org.springframework.security.acls.AclPermissionEvaluator
import org.springframework.security.acls.domain.*
import org.springframework.security.acls.jdbc.BasicLookupStrategy
import org.springframework.security.acls.jdbc.JdbcMutableAclService
import org.springframework.security.acls.jdbc.LookupStrategy
import org.springframework.security.acls.model.PermissionGrantingStrategy
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration
import org.springframework.security.core.authority.SimpleGrantedAuthority
import javax.sql.DataSource

@Profile("disabled")
@Configuration
open class AclConfiguration(
    private val dataSource: DataSource,
    private val cacheManager: CacheManager,
) {
    @Bean
    open fun defaultMethodSecurityExpressionHandler(): MethodSecurityExpressionHandler {
        val expressionHandler = DefaultMethodSecurityExpressionHandler()
        val permissionEvaluator = AclPermissionEvaluator(aclService())
        expressionHandler.setPermissionEvaluator(permissionEvaluator)
        return expressionHandler
    }

    @Bean
    open fun aclService(): JdbcMutableAclService {
        val aclService = JdbcMutableAclService(dataSource, lookupStrategy(), aclCache())
        aclService.setAclClassIdSupported(true)
        return aclService
    }

    @Bean
    open fun aclAuthorizationStrategy(): AclAuthorizationStrategy {
        return AclAuthorizationStrategyImpl(SimpleGrantedAuthority("ROLE_ADMIN"))
    }

    @Bean
    open fun permissionGrantingStrategy(): PermissionGrantingStrategy {
        return DefaultPermissionGrantingStrategy(ConsoleAuditLogger())
    }

    @Bean
    open fun aclCache(): SpringCacheBasedAclCache {
        return SpringCacheBasedAclCache(
            cacheManager.getCache("acl"),
            permissionGrantingStrategy(),
            aclAuthorizationStrategy())
    }

    @Bean
    open fun lookupStrategy(): LookupStrategy {
        val lookupStrategy = BasicLookupStrategy(
            dataSource,
            aclCache(),
            aclAuthorizationStrategy(),
            auditLogger()
        )
        lookupStrategy.setAclClassIdSupported(true)
        lookupStrategy.setConversionService(DefaultConversionService())
        return lookupStrategy
    }

    @Bean
    open fun auditLogger(): AuditLogger {
        return ConsoleAuditLogger()
    }
}

@Profile("disabled")
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
open class AclMethodSecurityConfiguration(private val dataSource: DataSource) : GlobalMethodSecurityConfiguration() {


//    override fun createExpressionHandler(): MethodSecurityExpressionHandler {
//        return defaultMethodSecurityExpressionHandler()
//    }
}