package com.qualidify.configuration

import com.qualidify.core.jpa.annotations.EnableSoftDeleteJpaRepositories
import com.qualidify.core.jpa.audit.SecurityAuditorAware
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.repository.config.EnableJpaAuditing



/**
 * A Configuration for the JPA functionality
 *
 * It enables the following functionalities:
 * - The Auditing is enabled with [EnableJpaAuditing] and will use the [SecurityAuditorAware] class
 * to find the author of the change.
 * - The JPA repositories are enabled which are build in the package [com.qualidify.model] and this
 * will use a custom Factory Bean to add functionality (Some kind of aspects) to the repositories
 *
 * @author Marcel Pot
 */
@Configuration
@EnableJpaAuditing
@EnableSoftDeleteJpaRepositories(value = ["com.qualidify.service.impl"])
open class PersistanceConfiguration {

    /**
     * This beans handles the auditor questions the system asks when
     * trying to persist entities to the database
     * @return an AuditorAware Bean
     */
    @Bean
    open fun auditorProvider(@Value("\${system.auditor:SYSTEM}") defaultAuditor: String): AuditorAware<String> {
        return SecurityAuditorAware(defaultAuditor)
    }
}


