package com.qualidify.configuration

import org.springframework.security.config.annotation.web.builders.WebSecurity

object CombinedSecurityUtil {

    fun webSecurityConfiguration(web: WebSecurity) {
        web.ignoring().antMatchers( // client-side JS code
            "/VAADIN/**",  // the standard favicon URI
            "/favicon.ico",  // the robots exclusion standard
            "/robots.txt",  // web application manifest
            "/manifest.webmanifest",
            "/sw.js",
            "/offline-page.html",  // icons and images
            "/offline-stub.html",  // since vaadin 23
            "/sw-runtime-resources-precache.js",  // since vaadin 23
            "/icons/**",
            "/images/**",
            "/h2-console/**"
        )
    }
}