package com.qualidify.configuration

import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.concurrent.ConcurrentMapCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary


/**
 * A Configuration for the Cache configuration
 *
 * @author Marcel Pot
 */
@Configuration
@EnableCaching
open class CacheConfiguration {

    @Bean
    @Primary
    open fun cacheManager(): CacheManager {

        return ConcurrentMapCacheManager("acl")
    }

//    @Bean
//    open fun cacheResolver(): CacheResolver {
//        return MultipleCacheResolver(alternateCacheManager(), cacheManager())
//    }
//
//    class MultipleCacheResolver(
//        private val simpleCacheManager: CacheManager,
//        private val caffeineCacheManager: CacheManager,
//    ) :
//        CacheResolver {
//        override fun resolveCaches(context: CacheOperationInvocationContext<*>): Collection<Cache> {
//            val caches: MutableCollection<Cache> = ArrayList()
//            context.
//
//            if ("getOrderDetail" == context.getMethod()
//                    .getName()
//            ) {
//                caches.add(caffeineCacheManager.getCache(ORDER_CACHE))
//            } else {
//                caches.add(simpleCacheManager.getCache(ORDER_PRICE_CACHE))
//            }
//            return caches
//        }
//
//    }
}