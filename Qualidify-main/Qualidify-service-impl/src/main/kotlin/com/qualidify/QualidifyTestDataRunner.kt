package com.qualidify

import com.qualidify.model.entities.*
import com.qualidify.service.impl.reports.BatchJobArtifactRepository
import com.qualidify.service.impl.reports.BatchJobRepository
import com.qualidify.service.impl.reports.ReportServiceTemplateRepository
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.util.*

@Component
@Profile("dev")
class QualidifyTestDataRunner(
    val reportServiceTemplateRepository: ReportServiceTemplateRepository,
    val batchJobRepository: BatchJobRepository,
    val batchJobArtifactRepository: BatchJobArtifactRepository
) : ApplicationRunner {

    private fun getTemplateData(filename: String): ByteArray  {
        return this.javaClass.classLoader.getResource(REPORTTEMPLATE_DIRECTORY + filename).readBytes()
    }

    private fun getTemplateResource(filename: String, resourceName: String): ReportServiceResource {
        return ReportServiceResource(
            resourceName,
            this.getTemplateData(filename)
        )
    }

    override fun run(args: ApplicationArguments) {
        //The data is moved to data-dev.sql
        // This testrunner is a placeholder for extra functionality testing if necessary

        val template: ReportTemplate = ReportTemplate(
            ReportTemplateId(UUID.randomUUID()),
            "Project instance users",
            "Reports the users associated with each project instance",
            getTemplateData("report_PI_users.jasper"),
            // Template parameter definitions
            """
                [
                    {
                        "name": "reportTitle",
                        "description": "Report title",
                        "dataType": "STRING",
                        "attributes": {
                            "isRequired": "true"
                        }
                    },
                    {
                        "name": "docReference",
                        "description": "Document reference",
                        "dataType": "STRING",
                        "attributes": {
                            "isRequired": "true"
                        }
                    },
                    {
                        "name": "docAuthor",
                        "description": "Document author",
                        "dataType": "STRING",
                        "attributes": {
                            "isRequired": "true"
                        }
                    },
                    {
                        "name": "docDate",
                        "description": "Document date",
                        "dataType": "DATE",
                        "attributes": {
                            "id": "docDate",
                            "isRequired": "true"
                        }
                    },
                    {
                        "name": "docVersion",
                        "description": "Document version",
                        "dataType": "STRING",
                        "attributes": {
                            "id": "docVersion",
                            "isRequired": "true"
                        }
                    }
                ]
            """.trimIndent(),
            mutableSetOf(
                this.getTemplateResource("report_PI_users_sub.jasper", "UsersPerProjectInstance/report_PI_users_sub.jasper"),
            )
        )
        this.reportServiceTemplateRepository.save(template)

        val batchJob = BatchJob(
            id = BatchJobId(UUID.randomUUID()),
            jobType = BatchJobType.JasperReportJob,
            jobName = "Job",
            parameters = "Parameters",
            status = BatchJobStatus.DONE,
            finishedDate = Date()
        )
        this.batchJobRepository.save(batchJob)


        val batchJobArtifact = BatchJobArtifact(
            batchJob.id, 1, "text/plain", "Test content".toByteArray(), Date())
        this.batchJobArtifactRepository.save(batchJobArtifact)

    }

    companion object {
        const val REPORTTEMPLATE_DIRECTORY: String = "dev-resources/reporttemplates/projectinstanceusers/"
    }
}
