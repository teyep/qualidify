package com.qualidify

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class QualidifyApplication

fun main(args: Array<String>) {
    runApplication<QualidifyApplication>(*args)
}
