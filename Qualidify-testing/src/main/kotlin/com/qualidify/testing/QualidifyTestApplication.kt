package com.qualidify.testing

import com.qualidify.rest.QualidifyRestApplication
import org.springframework.boot.runApplication

/**
 * The test application will start the rest server for testing. Startup is configured by the spring-boot-maven-plugin.
 * Loaded profiles are defined in the corresponding pom.xml file ('default' and 'nouser').
 *
 * @author Menno Tol
 * @since 1.0 QUAL-429
 */
fun main(args: Array<String>) {
    runApplication<QualidifyRestApplication>(*args)
}