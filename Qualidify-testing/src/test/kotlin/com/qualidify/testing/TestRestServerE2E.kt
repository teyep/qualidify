package com.qualidify.testing

import org.apache.hc.client5.http.classic.methods.HttpGet
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder
import org.apache.hc.core5.http.HttpStatus
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 * The first E2E test to be performed: tests if the rest server is running as expected.
 *
 * @author Menno Tol
 * @since 1.0 QUAL-429
 */
@Order(1)
internal class TestRestServerE2E : AbstractRestServerTestCase() {

    @Test
    fun `Test if rest server is running`() {
        val request = HttpGet(url)
        val response = HttpClientBuilder.create().build().execute(request)

        val acceptableStatusCodes = listOf(
            HttpStatus.SC_NOT_FOUND,
            HttpStatus.SC_UNAUTHORIZED
        )

        assertTrue(response.code in acceptableStatusCodes, "The rest server is not responding on '$url'.")

    }

    @Test
    fun `Test if security is disabled`() {
        val request = HttpGet(url)
        val response = HttpClientBuilder.create().build().execute(request)

        assertEquals(
            HttpStatus.SC_NOT_FOUND, response.code,
            "Security should be disabled, please use the 'nouser' and 'default' profiles to start the rest server."
        )

    }

}