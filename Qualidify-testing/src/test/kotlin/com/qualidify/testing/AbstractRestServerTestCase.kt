package com.qualidify.testing

import com.qualidify.core.log.LoggerDelegation
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInfo
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Abstract E2E test class providing basic resources.
 *
 * @author Menno Tol
 * @since 1.0 QUAL-429
 */
abstract class AbstractRestServerTestCase {

    private var startTime: Long? = null
    private val port = 5555
    val url = "http://localhost:$port/api"
    val logger by LoggerDelegation()

    @BeforeEach
    @Throws(Exception::class)
    fun setupTest() {
        startTime = System.nanoTime()
    }

    @AfterEach
    fun tearDown(testInfo: TestInfo){
        val timeElapsed = (System.nanoTime() - startTime!!) / 100_000_000.0
        logger.info("Executed ${this.javaClass.simpleName}.`${testInfo.displayName}`, " +
                "took ${BigDecimal(timeElapsed).setScale(1, RoundingMode.HALF_UP)} seconds")
    }
}
