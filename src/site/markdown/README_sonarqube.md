# SONARQUBE #

Sonarqube is used for code quality and unit test coverage. This part of the documentation will explain how to install
and use Sonarqube

### Installation ###

    docker pull sonarqube

    docker volume create sonarqube-conf 
    docker volume create sonarqube-data
    docker volume create sonarqube-logs
    docker volume create sonarqube-extensions

    docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 
    -v sonarqube-conf:/opt/sonarqube/conf -v sonarqube-data:/opt/sonarqube/data 
    -v sonarqube-logs:/opt/sonarqube/logs -v sonarqube-extensions:/opt/sonarqube/extensions sonarqube

The SonarQube page is reachable at localhost:9000 and when visiting this page it will request a username and password
combination. The default is admin:admin and after the first login, it will request to change the password.

At first you need to create a SonarQube Authentication Token (See section maven setup)

### Starting and stopping ###

For starting or stopping the following commands can be used:

    docker container start sonarqube
    docker container stop sonarqube
    docker container restart sonarqube

Choose the one wich fits best for your action

##### Maven setup #####

For Maven it can be useful to add the following to the settings.xml file in your _user/.m2_ folder

~~~~
<settings>
    <pluginGroups>
        <pluginGroup>org.sonarsource.scanner.maven</pluginGroup>
    </pluginGroups>
    <profiles>
        <profile>
            <id>sonar</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <sonar.host.url>http://localhost:9000</sonar.host.url>             
                <sonar.login>[!! fill your sonarqube authentication token !!]</sonar.login>
            </properties>
        </profile>
     </profiles>
</settings>
~~~~

So you can specify your own url and sonarqube authentication token.

The maven command

    mvn clean *** sonar:sonar -P sonar

with _***_  a maven phase executing the tests (for instance verify | test | install)
will push the code to Sonarqube for validation(where the _-P sonar_
is needed if you have specified the url and token in your settings.xml)

Otherwise you need to add them manually to your commandline:

    mvn clean *** sonar:sonar -Dsonar.login=yourAuthenticationToken -Dsonar.host.url=http://localhost:9000







