# Testing #

### Environment ###

The Qualidify test environment consists of three different test types:
* Unit testing 
* Integration testing 
* End-to-end testing

Each of the different suites are activated or deactivated by a different maven profile (unit, it, e2e). The profiles are located in the
pom.xml files of Qualidify-bom (for unit and it) and Qualidify-gui (for e2e). E2E tests are only configured
within the Qualidify-gui module.

When running tests using maven (see below) the unit tests are performed using Surefire, and will fail the build immediately when they don't pass. 
Integration and end-to-end tests are performed using Failsafe on the jar package. Within a module all integration and
end-to-end tests are performed before the build will be failed (i.e. verification). 

For the end-to-end tests a server running the Qualidify-gui application is required. 
* When using maven a server will be started automatically, after which the E2E tests are performed
* When using IntelliJ with E2E tests, the server must be started manually

### Execution ###

Currently, the unit test profile is active by default (`mvn clean install` will run only unit tests).
Activating or deactivating profiles van be done using maven tags: 
* DSkipUnit for _**skipping**_ unit tests: `mvn clean install -DSkipUnit`
* Dit for _**running**_ integration tests: `mvn clean install -Dit` 
* De2e for _**running**_ end-to-end tests: `mvn clean install -De2e`

