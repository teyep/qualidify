# Databases #

### H2 Database for Development ###

In the development phase the system can run with the _dev_-profile. The server will startup an in memory H2-database.
This database can be accessed on two different ways:

1. The web console is also available at http://localhost:8080/h2-console/
   when the following property is set as below:

        spring:
            h2:
                console:
                    enabled: true

2. With any database tool we can make connection with the following url (Username and password are as configured in
   application-dev.yaml file):

        jdbc:h2:tcp://localhost:9090/mem:testdb1

### Postgress Database for System tests ###

For system development phase it is important that your data will stay in the state you left it. In that case a real
database (postgres) needs to be configured. In spring boot the _sys_-profile can be used to use this setup, but first
the database needs to be configured.

#### The Postgres database installment ####

The Postgres database can be installed as a docker image:

    docker pull postgres
    docker run --name postgres -p 5432:5432 -e POSTGRES_USER=qualidify -e POSTGRES_PASSWORD=4m -d postgres 

This postgress-docker-image is running on the default port 5432 of localhost and can be used for datastorage now

With any database tool the connection can be made with the following url :

    jdbc:postgresql://localhost:5432/postgres

and username and password as configured above with the properties POSTGRES_USER and POSTGRES_PASSWORD

#### Use Postgress database ####

With the profile _sys_ the postgressdatabase will be used. Intellij can be configured to run this profile with:

![img.png](/../README_images/IntelliJAddProfile.png)
![img.png](/../README_images/IntelliJCEAddProfile.png)

The first time the Progress database is used it will be empty. To fill the database with some users and data do the
following use the `test-data` profile:




