# Security #

### Security Profiles ###

At this moment there are three security profiles available:

* _nosec_ a profile indicating no security is offered. This is very useful in the development stage where you don't
  want to be bothered by login requests every time the server is restarted
* _singleserver_ a profile indicating the server is handling its own login request
* _nouser_ a profile indicating that user-level security is disabled

