# Flowable #

Flowable can be used for designing BPMN. This chapter of the documentation describes how to start and use the flowable
Modeller

### Starting up ###

    docker run --name Flowable -p 9001:8080 flowable/flowable-ui


        http://localhost:9001/flowable-ui/ 
        (login/password: admin/test)

        Flowable Modeler | http://localhost:9001/flowable-ui/modeler/
        Flowable Task | http://localhost:9001/flowable-ui/workflow/
        Flowable Admin | http://localhost:9001/flowable-ui/admin/

The flowable engine is started at port 9001. Do not configure the ports as 9001 in the Flowable Admin!