package com.qualidify.metadata

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


internal class DataTypeTest {
    @Test
    fun testIsReveratable() {
        assertThat(DataType.BOOLEAN.isRevertable("TRUE")).`as`("Boolean True is revertable").isTrue
        assertThat(DataType.BOOLEAN.isRevertable("FALSE")).`as`("Boolean False is revertable").isTrue
        // will be reverted to false!!
        assertThat(DataType.BOOLEAN.isRevertable("ERROR")).`as`("Boolean XXX is revertable").isTrue
        assertThat(DataType.INTEGER.isRevertable("1")).`as`("Integer 1 is revertable").isTrue
        assertThat(DataType.INTEGER.isRevertable("-3")).`as`("Integer -3 is revertable").isTrue
        assertThat(DataType.INTEGER.isRevertable("6 Apples"))
            .`as`("Integer '6 Apples' is not revertable").isFalse
        assertThat(DataType.DOUBLE.isRevertable("1.0")).`as`("Double 1.0 is revertable").isTrue
        assertThat(DataType.DOUBLE.isRevertable("3.14")).`as`("Double 3.14 is revertable").isTrue
        assertThat(DataType.DOUBLE.isRevertable("-2.78")).`as`("Double -2.78 is revertable").isTrue
        assertThat(DataType.DOUBLE.isRevertable("-1,68")).`as`("Double 1,68 is not revertable").isFalse
        assertThat(DataType.STRING.isRevertable("A Big black cat with 9 legs"))
            .`as`("String 'A Big black cat with 9 legs' is revertable").isTrue
    }

    @Test
    fun testRevert() {
        assertThat(DataType.BOOLEAN.revert("TRUE")).`as`("Boolean True is revertable")
            .isEqualTo(true)
        assertThat(DataType.BOOLEAN.revert("FALSE")).`as`("Boolean False is revertable")
            .isEqualTo(false)
        // will be reverted to false!!
        assertThat(DataType.BOOLEAN.revert("ERROR")).`as`("Boolean XXX is revertable")
            .isEqualTo(false)
        assertThat(DataType.INTEGER.revert("1")).`as`("Integer 1 is revertable").isEqualTo(1)
        assertThat(DataType.INTEGER.revert("-3")).`as`("Integer -3 is revertable").isEqualTo(-3)
        assertThat(DataType.DOUBLE.revert("1.0")).`as`("Double 1.0 is revertable").isEqualTo(1.0)
        assertThat(DataType.DOUBLE.revert("3.14")).`as`("Double 3.14 is revertable").isEqualTo(3.14)
        assertThat(DataType.DOUBLE.revert("-2.78")).`as`("Double -2.78 is revertable").isEqualTo(-2.78)
        assertThat(DataType.STRING.revert("A Big black cat with 9 legs"))
            .`as`("String 'A Big black cat with 9 legs' is revertable").isEqualTo("A Big black cat with 9 legs")
    }
}