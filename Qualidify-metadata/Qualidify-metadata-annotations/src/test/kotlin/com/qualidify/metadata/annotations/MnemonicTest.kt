package com.qualidify.metadata.annotations

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.core.annotation.AnnotatedElementUtils

class MnemonicTest {

    @Test
    fun `test 'inheritence' of Mnemonic Annotation`() {
        val mergedAnnotation =
            AnnotatedElementUtils.getMergedAnnotation(MnemonicTestObject::class.java, DtoMetadata::class.java)

        assertThat(mergedAnnotation?.singular)
            .`as`("Singular attribute is overwritten with Mnemonic singular attribute")
            .isEqualTo("test")
        assertThat(mergedAnnotation?.plural)
            .`as`("Plural attribute is overwritten with Mnemonic plural attribute")
            .isEqualTo("Tests")
        assertThat(mergedAnnotation?.enableAddition)
            .`as`("EnableAddition attribute is using the default of Mnemonic attribute")
            .isTrue()
        assertThat(mergedAnnotation?.enableDelete)
            .`as`("EnableDelete attribute is using the overriden value of Mnemonic attribute")
            .isFalse()
    }
}

@Mnemonic("TEST", singular = "test", plural = "Tests", enableDelete = false)
data class MnemonicTestObject(
    val identity: String,
)