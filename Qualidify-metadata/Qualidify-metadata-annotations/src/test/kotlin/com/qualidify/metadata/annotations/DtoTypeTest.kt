package com.qualidify.metadata.annotations

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.types.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.core.annotation.AnnotatedElementUtils
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class DtoTypeTest {

    @Test
    fun `Test the right datatype per annotated field`() {
        assertThat(getDataType("bigDecimalType")).isEqualTo(DataType.BIGDECIMAL)
        assertThat(getDataType("booleanType")).isEqualTo(DataType.BOOLEAN)
        assertThat(getDataType("checkBoxGroupType")).isEqualTo(DataType.COLLECTION)
        assertThat(getDataType("comboBoxType")).isEqualTo(DataType.COLLECTION)
        assertThat(getDataType("customFields")).isEqualTo(DataType.CUSTOM)
        assertThat(getDataType("dateTimeType")).isEqualTo(DataType.DATETIME)
        assertThat(getDataType("dateType")).isEqualTo(DataType.DATE)
        assertThat(getDataType("editorType")).isEqualTo(DataType.RICH_TEXT)
        assertThat(getDataType("emailType")).isEqualTo(DataType.EMAIL)
        assertThat(getDataType("gridType")).isEqualTo(DataType.COLLECTION)
        assertThat(getDataType("integerType")).isEqualTo(DataType.INTEGER)
        assertThat(getDataType("numberType")).isEqualTo(DataType.DOUBLE)
        assertThat(getDataType("passwordType")).isEqualTo(DataType.PASSWORD)
        assertThat(getDataType("radioButtonType")).isEqualTo(DataType.ENUMERATION)
        assertThat(getDataType("selectType")).isEqualTo(DataType.ENUMERATION)
        assertThat(getDataType("textType")).isEqualTo(DataType.STRING)
        assertThat(getDataType("textArea")).isEqualTo(DataType.TEXT)
        assertThat(getDataType("timeType")).isEqualTo(DataType.TIME)
        assertThat(getDataType("uploadType")).isEqualTo(DataType.BINARY)
    }

    private fun getDataType(field: String): DataType? {
        return AnnotatedElementUtils
            .getMergedAnnotation(DefaultAnnotationConfiguration::class.java.getDeclaredField(field),
                DtoType::class.java)?.value
    }
}

object DefaultAnnotationConfiguration {

    @BigDecimalType
    var bigDecimalType: BigDecimalType? = null

    @BooleanType
    var booleanType: Boolean? = null

    @CheckBoxGroupType
    var checkBoxGroupType: Set<Any>? = null

    @ComboBoxType
    var comboBoxType: DataType? = null

    @CustomType(reference = Reference.USER)
    var customFields: Map<String, String>? = null

    @DateTimeType
    var dateTimeType: LocalDateTime? = null

    @DateType
    var dateType: LocalDate? = null

    @EditorType
    var editorType: String? = null

    @EmailType
    var emailType: String? = null

    @GridType(mnemonic = "Something")
    var gridType: Collection<Any>? = null

    @IntegerType
    var integerType: Int? = null

    @NumberType
    var numberType: Double? = null

    @PasswordType
    var passwordType: String? = null

    @RadioButtonType
    var radioButtonType: DataType? = null

    @SelectType
    var selectType: DataType? = null

    @TextAreaType
    var textArea: String? = null

    @TextType
    var textType: String? = null

    @TimeType
    var timeType: LocalTime? = null

    @UploadType
    var uploadType: ByteArray? = null


}