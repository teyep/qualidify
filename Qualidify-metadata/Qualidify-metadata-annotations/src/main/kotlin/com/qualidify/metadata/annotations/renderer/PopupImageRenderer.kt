package com.qualidify.metadata.annotations.renderer

@Retention
@Target(AnnotationTarget.FIELD)
@Renderer(RendererType.POPUP_IMAGE)
annotation class PopupImageRenderer(
    /**
     * Configure the width of the image.
     * Default setting is 100% of the possible space
     */
    val width: String = "100%",

    /**
     * Configure the height of the image.
     * Default setting is 100% of the possible space
     */
    val height: String = "100%",
)
