package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the RadioButton
 * ?rename?
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.ENUMERATION)
annotation class RadioButtonType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Add a itemprovider
     */
    val itemProvider: String = "",

    /**
     * Add HelperText to the inputForm (default is empty)
     */
    val helperText: String = "",
)