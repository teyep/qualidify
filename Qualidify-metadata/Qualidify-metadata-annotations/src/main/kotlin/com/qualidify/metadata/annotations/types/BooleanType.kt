package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the CheckBox
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.BOOLEAN)
annotation class BooleanType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Show an aria label
     */
    val ariaLabel: String = "",

    /**
     * set the focus to this input field
     */
    val autoFocus: Boolean = false,
)