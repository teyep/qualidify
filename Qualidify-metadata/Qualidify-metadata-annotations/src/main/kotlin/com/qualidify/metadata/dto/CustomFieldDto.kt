package com.qualidify.metadata.dto

import com.qualidify.metadata.DataType
import java.io.Serializable

data class CustomFieldDto(
    val field: String,
    val datatype: DataType,
    val value: String,
) : Serializable {

    companion object {

        @JvmStatic
        fun stringField(field: String, value: String): CustomFieldDto {
            return CustomFieldDto(field, DataType.STRING, value)
        }

        @JvmStatic
        fun intField(field: String, value: String): CustomFieldDto {
            return CustomFieldDto(field, DataType.INTEGER, value)
        }

        @JvmStatic
        fun booleanField(field: String, value: String): CustomFieldDto {
            return CustomFieldDto(field, DataType.BOOLEAN, value)
        }

        @JvmStatic
        fun doubleField(field: String, value: String): CustomFieldDto {
            return CustomFieldDto(field, DataType.DOUBLE, value)
        }


    }
}