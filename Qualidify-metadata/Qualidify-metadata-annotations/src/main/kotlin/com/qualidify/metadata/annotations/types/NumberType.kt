package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the NumberField
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.DOUBLE)
annotation class NumberType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Give a title for this input field
     */
    val title: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Describes the minimum value used for the input field
     * Default is set to {Double.MIN_VALUE}
     */
    val min: Double = Double.MIN_VALUE,

    /**
     * Describes the maximum value used for the input field
     * Default is set to {Double.MAX_VALUE}
     */
    val max: Double = Double.MAX_VALUE,

    /**
     * A number to be used as default value
     */
    val defaultValue: Double = 0.0,

    /**
     * Indicator for using the {NumberType.defaultValue} in the input field
     */
    val useDefaultValue: Boolean = false,

    /**
     * Use a step value for increasing / decreasing functionality
     * Default is set to 1
     */
    val stepValue: Double = 1.0,

    /**
     * Give a hint to the user (default is an empty string)
     */
    val helperText: String = "",

    )