package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the IntegerField
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.INTEGER)
annotation class IntegerType(

    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Describes the minimum value of the input value
     * Default is set to {Integer.MIN_VALUE}
     */
    val min: Int = Int.MIN_VALUE,

    /**
     * Describes the maximum value of the input value
     * Default is set to {Int.MAX_VALUE}
     */
    val max: Int = Int.MAX_VALUE,

    /**
     * Describes the default Value used in the form,
     * Default it is set to zero, but showing of the value depends
     * on the attribute @see{IntegerType.useDefaultValue}
     */
    val defaultValue: Int = 0,

    /**
     * Indicates if the @see{IntegerType.defaultValue} will be used
     * Default setting is false
     */
    val useDefaultValue: Boolean = false,

    /**
     * Set a placeholder for the input field
     */
    val placeholder: String = "",

    /**
     * Indicator of delta for increasing/decreasing the integer field
     * Default setting is 1
     */
    val stepValue: Int = 1,

    /**
     * Add HelperText to the inputForm (default is empty)
     */
    val helperText: String = "",

    )