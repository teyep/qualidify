package com.qualidify.metadata.annotations

/**
 * Indicator for Dto Fields for editor purposes and mapping on the jpa entities
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD, AnnotationTarget.ANNOTATION_CLASS)
annotation class DtoColumn(
    /**
     * Configurable field to change the description of this field in the
     * Overview and the editor
     *
     * @return the description of this field
     */
    val description: String = "",

    /**
     * Indicator if the column should not be used by the editor
     *
     * @return a boolean indicating the use of this column
     */
    val hidden: Boolean = false,

    /**
     * Indicator if the column should be visible in the view screen.
     * If false, this still means that the property is still editable in
     * the edit screen.
     *
     * @return a boolean indicating if this column is visible in
     * the overview screen
     */
    val viewable: Boolean = true,

    /**
     * Indicator if the column should be editable in the form screen.
     *
     * @return a boolean indicating if this column is visible
     * in the form editor
     */
    val editable: Boolean = true,

    /**
     * Indicator if the column can be filtered in the overview screen
     *
     * @return a boolean indicating if this column can be used in filters
     */
    val searchable: Boolean = true,

    /**
     * Indicator if the column can be ordered in the overview screen
     *
     * @return a boolean indicating if this column can be used in filters
     */
    val sortable: Boolean = true,

    /**
     * Specify in which order the columns show up
     *
     * @return an order number
     */
    val order: Int = Int.MIN_VALUE,

    /**
     * Specify the segments (=tabs) where this property should be visible
     *
     * @return tabs
     */
    val segments: Array<Segment> = arrayOf(Segment("DEFAULT"))
)