package com.qualidify.metadata

/**
 * A Dto object containing file information for upload purposes
 *
 * @author Marcel Pot
 * @since 1.0
 */
data class UploadFile(val name: String, val bytes: ByteArray) : java.io.Serializable