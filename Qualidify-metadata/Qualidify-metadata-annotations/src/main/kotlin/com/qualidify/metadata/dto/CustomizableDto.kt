package com.qualidify.metadata.dto

import java.io.Serializable
import java.util.*

/**
 * An interface for Customizable Dtos using Custom Fields
 *
 * @author Marcel Pot
 */
interface CustomizableDto : Serializable {
    fun set(key: String, value: String?)
    fun set(customFieldDto: CustomFieldDto)
    fun optionalGet(key: String): Optional<String>
    fun get(key: String): String?
}

/**
 * An abstract interpretation of the Customizable Dto
 *
 * @author Marcel Pot
 */
abstract class CustomizableDtoHelper : CustomizableDto {
    abstract val fields: MutableMap<String, String?>

    companion object {
        val FIELDS_PREFIX = "fields."
        val FIELDS_REGEX = Regex("\\[(.*?)\\]")
    }

    override fun set(key: String, value: String?) {
        val convertedKey = convert(key)
        fields.put(convertedKey, value)
    }

    override fun set(customFieldDto: CustomFieldDto) {
        set(convert(customFieldDto.field), customFieldDto.value)
    }

    override fun optionalGet(key: String): Optional<String> {
        return Optional.ofNullable(get(key))
    }

    override fun get(key: String): String? {
        return fields.get(convert(key))
    }

    private fun convert(key: String): String {
        if (FIELDS_REGEX.containsMatchIn(key)) {
            return FIELDS_REGEX.find(key)?.groupValues?.get(1) ?: key
        }
        return key.removePrefix(FIELDS_PREFIX)
    }
}

/**
 * A simple Map implementation of the Customizable Dto
 *
 * @author Marcel Pot
 */
open class CustomizableDtoMap(override val fields: MutableMap<String, String?>) : CustomizableDtoHelper()