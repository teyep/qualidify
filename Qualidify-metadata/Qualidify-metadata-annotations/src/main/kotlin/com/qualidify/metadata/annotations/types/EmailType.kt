package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the Email
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.EMAIL)
annotation class EmailType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Give a title for this input field
     */
    val title: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Define a maximum length for the input field
     * (Default is {Int.MAX_VALUE}})
     */
    val maxLength: Int = Int.MAX_VALUE,

    /**
     * Define a minimum length for the input field
     * (Default is 0)
     */
    val minLength: Int = 0,

    /**
     * Configure a pattern for this input field
     */
    val pattern: String = "",

    /**
     * Add a placeholder for this input field
     */
    val placeholder: String = "",

    /**
     * Add a clear button for this input field
     * (default not present)
     */
    val clearButtonVisible: Boolean = false,

    /**
     * set the focus to this input field
     */
    val autoFocus: Boolean = false,

    /**
     * auto select the input field
     */
    val autoSelect: Boolean = false,

    /**
     * Prevent input of invalid text
     */
    val preventInvalidInput: Boolean = false,
)