package com.qualidify.metadata.annotations

import org.springframework.core.annotation.AliasFor


/**
 * An annotation to indicate a Dto to enable automatic detection in the frontend.
 * The mnemonic contains a information about displaying name and link
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@DtoMetadata(singular = "Mnemonic", plural = "Menomics") //should be merged with values below
annotation class Mnemonic(
    /**
     * The real mnemonic, used as name in hyperlinks etc in the frontend
     */
    val value: String,

    /**
     * A configuration place for the name (singular) of the Entity
     * No default because the name is required
     */
    @get:AliasFor(annotation = DtoMetadata::class, attribute = "singular")
    val singular: String,

    /**
     * A configuration place for the plural name of entities
     */
    @get:AliasFor(annotation = DtoMetadata::class, attribute = "plural")
    val plural: String,

    /**
     * Indicates if the annotated object can be created
     * Default is true
     */
    @get:AliasFor(annotation = DtoMetadata::class)
    val enableAddition: Boolean = true,

    /**
     * Indicates if the annotated object can be modified
     * Default is true
     */
    @get:AliasFor(annotation = DtoMetadata::class)
    val enableUpdate: Boolean = true,

    /**
     * Indictates if the annotated object can be deleted
     * Default is true
     */
    @get:AliasFor(annotation = DtoMetadata::class, attribute = "enableDelete")
    val enableDelete: Boolean = true,

    /**
     * Indicates if there exist a search or filter option for this annotated object
     * Default is true
     */
    @get:AliasFor(annotation = DtoMetadata::class)
    val enableFiltering: Boolean = true,
)