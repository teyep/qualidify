package com.qualidify.metadata.annotations

/**
 * Alias for [DtoColumn] with value hidden = true
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoColumn(hidden = true)
annotation class Hidden