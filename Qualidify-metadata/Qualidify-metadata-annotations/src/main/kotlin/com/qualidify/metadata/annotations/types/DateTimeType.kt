package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the DateTime
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.DATETIME)
annotation class DateTimeType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Define a maximum date for the inputfield (Default is empty)
     */
    val max: String = "",

    /**
     * Define a minimum date for the inputfield (Default is empty)
     */
    val min: String = "",

    /**
     * Define a step size (in seconds) for the input field
     */
    val stepValue: Long = 0,

    /**
     * Open the datepicker automaticaly
     */
    val autoOpen: Boolean = false,

    /**
     * Open the datapicker
     */
    val open: Boolean = false,


    /**
     * Show the weeknumbers (default is false)
     */
    val weekNumbersVisible: Boolean = false,

    /**
     * Add a placeholder for the date (default is empty)
     */
    val datePlaceholder: String = "",

    /**
     * Add a placeholder for the time (default is empty)
     */
    val timePlaceholder: String = "",

    /**
     * Add HelperText to the inputForm (default is empty)
     */
    val helperText: String = "",

    )