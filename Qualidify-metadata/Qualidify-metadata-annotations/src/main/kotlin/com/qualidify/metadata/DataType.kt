package com.qualidify.metadata

import com.qualidify.metadata.annotations.types.*
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

/**
 * Datatype is an indicator for the kind of data used to store data
 * @author Marcel Pot
 * @since 1.0
 */
enum class DataType(val visible: Boolean, private val revertible: Boolean) : Serializable {

    /**
     * A ByteArray, can be used for two different types (or arrays of ...)
     * <li> [ByteArray] </li>
     * <li> [UploadFile]<li>
     */
    BINARY(true, true),

    /**
     * A floating number containing more precision than a default [float] and [double]
     * It will always map to a [BigDecimal] object
     */
    BIGDECIMAL(true, true),

    /**
     * A boolean
     */
    BOOLEAN(true, true),

    /**
     * A child collection of objects,
     * It cannot automatically being mapped!
     */
    COLLECTION(false, false),

    /**
     * An indicator that the configuration of this fields is stored in the database
     * Normally it is stored as a map of key-value pairs, see [CustomField]
     */
    CUSTOM(false, false),

    /**
     * A Date, maps to [LocalDate]
     */
    DATE(true, true),

    /**
     * A Datetime, maps to [LocalDateTime]
     */
    DATETIME(true, true),

    /**
     * A floating number, can have rounding issues, for more precision move to [BIGDECIMAL]
     */
    DOUBLE(true, true),

    /**
     * An email field, with validations on email format.
     * It will map to [String]
     */
    EMAIL(true, true),

    /**
     * An enumeration used to pick one option from a  list of options
     * It cannot automatically being mapped!
     */
    ENUMERATION(false, false),

    /**
     * An Integer number, maps always to [Int]
     */
    INTEGER(true, true),

    /**
     * A Map
     */
    MAP(true, false),

    /**
     * A text type where the visibility is hidden
     * It maps to [String]
     */
    PASSWORD(true, true),

    /**
     * A text object, where the format of the text also is stored in the value
     * It will map to [String]
     */
    RICH_TEXT(true, true),

    /**
     * A text object, usable for small text inputs
     * It will map to [String]
     */
    STRING(true, true),

    /**
     * A text object, usable for text inputs where the input is a larger than [STRING]
     * It will map to [String]
     */
    TEXT(true, true),

    /**
     * Time, maps to [LocalTime]
     */
    TIME(true, true);


    fun isRevertable(value: String): Boolean {
        try {
            revert(value)
        } catch (_: Exception) {
            return false
        }
        return true
    }


    fun revert(value: String): Any {
        if (!this.revertible) throw DataTypeException("DataType conversion not supported")

        return when (this) {
            BINARY -> revertBinary(value)
            BIGDECIMAL -> BigDecimal(value)
            BOOLEAN -> java.lang.Boolean.valueOf(value)
            DATE -> LocalDate.parse(value)
            DATETIME -> LocalDateTime.parse(value)
            DOUBLE -> java.lang.Double.valueOf(value)
            EMAIL -> value
            INTEGER -> Integer.valueOf(value)
            PASSWORD -> value
            RICH_TEXT -> value
            STRING -> value
            TEXT -> value
            TIME -> LocalTime.parse(value)
            else -> return value
        }
    }

    private fun revertBinary(value: String): Any {
        return value.toByteArray()
    }


}

class DataTypeException(msg: String? = null, cause: Throwable? = null) : RuntimeException(msg, cause)