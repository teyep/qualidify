package com.qualidify.metadata.annotations

/**
 * Indicator for Dto commands (no entity relation)
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
annotation class DtoCommand(
    val name: String
)
