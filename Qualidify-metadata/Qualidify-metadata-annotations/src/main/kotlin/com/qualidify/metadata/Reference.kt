package com.qualidify.metadata

enum class Reference(val description: String) {
    USER("User"),
    PROJECT_TEMPLATE("Project Template"),
    PROJECT_INSTANCE("Project Instance");

}