package com.qualidify.metadata.annotations

import com.qualidify.metadata.DataType


/**
 * Annotation to override the standard type recognition of the frontend
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.ANNOTATION_CLASS)
annotation class DtoType(
    /**
     * Indicator of the type of data.
     * Default is String
     */
    val value: DataType = DataType.STRING,
)


