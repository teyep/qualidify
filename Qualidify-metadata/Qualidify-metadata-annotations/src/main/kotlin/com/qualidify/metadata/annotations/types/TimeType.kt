package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the Time
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.TIME)
annotation class TimeType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Define a maximum time for the inputfield (Default is empty)
     */
    val max: String = "",

    /**
     * Define a minimum time for the inputfield (Default is empty)
     */
    val min: String = "",

    /**
     * Open the timepicker automaticaly
     */
    val autoOpen: Boolean = false,

    /**
     * Is the clear button available (default is false)
     */
    val clearButtonVisible: Boolean = false,

    /**
     * Add a placeholder for the time (default is empty)
     */
    val placeholder: String = "",

    /**
     * Add HelperText to the inputForm (default is empty)
     */
    val helperText: String = "",
)