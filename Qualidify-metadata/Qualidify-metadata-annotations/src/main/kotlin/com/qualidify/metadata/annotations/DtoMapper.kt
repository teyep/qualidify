package com.qualidify.metadata.annotations

import org.springframework.stereotype.Component

/**
 * A Stereotype to indicate if the class is a data mapper for Dto to Entity and visa versa
 *
 * @author Marcel Pot
 */
@Retention
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Component
annotation class DtoMapper