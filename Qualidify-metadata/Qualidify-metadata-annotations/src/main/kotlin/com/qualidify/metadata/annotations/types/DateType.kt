package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the DateType
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.DATE)
annotation class DateType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Define a maximum date for the inputfield (Default is empty)
     */
    val max: String = "",

    /**
     * Define a minimum date for the inputfield (Default is empty)
     */
    val min: String = "",

    /**
     * Open the datepicker automaticaly
     */
    val autoOpen: Boolean = false,

    /**
     * Open the datapicker
     */
    val open: Boolean = false,

    /**
     * Is the clear button available (default is false)
     */
    val clearButtonVisible: Boolean = false,

    /**
     * Show the weeknumbers (default is false)
     */
    val weekNumbersVisible: Boolean = false,

    /**
     * Add a placeholder for the date (default is empty)
     */
    val placeHolder: String = "",

    /**
     * Add HelperText to the inputForm (default is empty)
     */
    val helperText: String = "",
)