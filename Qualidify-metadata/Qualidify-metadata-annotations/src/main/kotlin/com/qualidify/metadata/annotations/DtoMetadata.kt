package com.qualidify.metadata.annotations

/**
 * Indicator for Dto classes for mapping on the JPA entities en showing in the GUI
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
annotation class DtoMetadata(

    /**
     * A configuration place for the name (singular) of the Entity
     * No default because the name is required
     */
    val singular: String,

    /**
     * A configuration place for the plural name of entities
     */
    val plural: String,

    /**
     * Indicates if the annotated object can be created
     * Default is true
     */
    val enableAddition: Boolean = true,

    /**
     * Indicates if the annotated object can be modified
     * Default is true
     */
    val enableUpdate: Boolean = true,

    /**
     * Indicates if the annotated object can be deleted
     * Default is true
     */
    val enableDelete: Boolean = true,

    /**
     * Indicates if there exist a search or filter option for this annotated object
     * Default is true
     */
    val enableFiltering: Boolean = true,
)