package com.qualidify.metadata.annotations


/**
 * An annotation indicating extra buttons on the frontend for an annotated object.
 * @see [Action] for more information on displaying information
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
annotation class Actions(
    val value: Array<Action>,
)

/**
 * An annotation indicating extra actions on the frontend special used for actions
 * available on multiple items
 * @see [Action] for more information on displaying information
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-549
 */
@Retention
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
annotation class BatchActions(
    val value: Array<Action>
)

/**
 * An annotation indicating the format of an extra button for an annotated object.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Repeatable
@Retention()
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
annotation class Action(
    /**
     * The label on the button
     */
    val label: String,

    /**
     * A free string to define the action.
     * It should be configured in the [DataConnector] how to act on
     * this action
     */
    val action: String,

    /**
     * Indicator if the buttons are placed in front of or at the end of a Gridrow
     * Default is placing at the end of a Gridrow
     */
    val position: Position = Position.END,

    /**
     * In case of more buttons, there can be an order given
     */
    val order: Int = 0,
)

/**
 * The ButtonPosition gives the position of the added [Action]s
 *
 * @author Marcel Pot
 * @since 1.0
 */
enum class Position {
    BEGIN,
    END
}


