package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the TextArea
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.TEXT)
annotation class TextAreaType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Indicate if this input field is required
     */
    val isRequired: Boolean = false,

    /**
     * Define a maximum length for the input field
     * (Default is {Int.MAX_VALUE}})
     */
    val maxLength: Int = Int.MAX_VALUE,

    /**
     * Define a minimum length for the input field
     * (Default is 0)
     */
    val minLength: Int = 0,

    /**
     * Add a placeholder for this input field
     */
    val placeHolder: String = "",

    /**
     * Add a clear button for this input field
     * (default not present)
     */
    val clearButtonVisible: Boolean = false,

    /**
     * set the focus to this input field
     */
    val autoFocus: Boolean = false,

    /**
     * auto select the input field
     */
    val autoSelect: Boolean = false,
)