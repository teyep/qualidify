package com.qualidify.metadata.annotations.renderer

/**
 * Indicator for different Rendering functionalities in the GUI
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.ANNOTATION_CLASS)
annotation class Renderer(
    /**
     * The type of the rendering. Default will be set to NONE
     */
    val value: RendererType = RendererType.NONE,
)

/**
 * Enumaration of Rendering types
 *
 * @author Marcel Pot
 * @since 1.0
 */
enum class RendererType {
    /**
     * No Rendering
     */
    NONE,

    /**
     * Use the active Rendering. Used with Boolean values
     */
    ACTIVE,

    /**
     * Use image rendering
     */
    IMAGE,
    POPUP_IMAGE
}



