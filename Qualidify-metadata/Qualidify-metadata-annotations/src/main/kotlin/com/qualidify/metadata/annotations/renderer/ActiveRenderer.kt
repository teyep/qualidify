package com.qualidify.metadata.annotations.renderer

@Retention
@Target(AnnotationTarget.FIELD)
@Renderer(RendererType.ACTIVE)
annotation class ActiveRenderer {
}