package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the Enumaration
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.COLLECTION)
annotation class ComboBoxType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Add a itemProvider for this input field
     */
    val itemProvider: String = "",

    /**
     * Add a placeholder for this input field
     */
    val placeholder: String = "",

    /**
     * set the field to automatically open
     */
    val autoOpen: Boolean = false,

    /**
     * set the focus to this input field
     */
    val autoFocus: Boolean = false,

    /**
     * Add a page size
     */
    val pageSize: Int = Int.MIN_VALUE,

    /**
     * Add a pattern
     */
    val pattern: String = "",

    /**
     * Is the clear button available (default is false)
     */
    val clearButtonVisible: Boolean = false,

    /**
     * Is custom input of user allowed (default false)
     */
    val allowCustomValue: Boolean = false,

    /**
     * Add HelperText to the inputForm (default is empty)
     */
    val helperText: String = "",
)