package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the Rich Text Editor
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.RICH_TEXT)
annotation class EditorType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * configure how the data should be read and perstisted
     * Default is html
     */
    val dataFormat: DataFormat = DataFormat.HTML,
)

/**
 * Data format used in the Rich Text Editor.
 *
 * @author Marcel Pot
 * @since 1.0
 */
enum class DataFormat {
    /**
     * This is the default format for the RichTextEditor.
     * More information can be found at [https://quilljs.com/docs/delta/]
     */
    DELTA,

    /**
     * The HTML format
     */
    HTML,
}