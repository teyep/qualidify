package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the Upload
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.BINARY)
annotation class UploadType(

    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * Configure the max file size
     */
    val maxFileSize: Int = Int.MIN_VALUE,

    /**
     * Configure the number of files for upload (default = 1)
     */
    val maxFiles: Int = 1,

    /**
     * Configure the accepted filetypes
     */
    val acceptedFileTypes: Array<String> = [],

    /**
     * Enable auto uploading
     */
    val autoUpload: Boolean = true,

    /**
     * Is drop allowed on this field (default is false)
     */
    val dropAllowed: Boolean = false,


    )

