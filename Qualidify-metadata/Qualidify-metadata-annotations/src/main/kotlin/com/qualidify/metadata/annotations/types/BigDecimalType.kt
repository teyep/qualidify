package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoType

/**
 * Annotation for defining configuration of the BigDecimal
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.BIGDECIMAL)
annotation class BigDecimalType(
    /**
     * Override the automatic ID generation of the input field
     * Default is empty
     */
    val id: String = "",

    /**
     * Give a title for this input field
     */
    val title: String = "",

    /**
     * Override the automatic label generation of the input field
     * Default is empty
     */
    val label: String = "",

    /**
     * set the focus to this input field
     */
    val autoFocus: Boolean = false,

    /**
     * auto select the input field
     */
    val autoSelect: Boolean = false,

    /**
     * A number to be used as default value
     */
    val defaultValue: Double = 0.0,

    /**
     * Indicator for using the {NumberType.defaultValue} in the input field
     */
    val useDefaultValue: Boolean = false,

    /**
     * Give a hint to the user (default is an empty string)
     */
    val helperText: String = "",
)