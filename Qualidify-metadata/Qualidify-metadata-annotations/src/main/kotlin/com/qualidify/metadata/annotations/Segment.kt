package com.qualidify.metadata.annotations

/**
 * A Segment definition
 */
@Retention
annotation class Segment(

    /**
     *  Segment name
     *
     *  @return name of the segment
     */
    val value: String
)

annotation class SegmentDefinition(
    /**
     *  Segment name
     *
     *  @return name of the segment
     */
    val value: String,

    val description: String = ""
)

/**
 * Indicator of available segments for an object
 */
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Retention
annotation class Segments(
    /**
     * Indicating the order of the segments
     *
     * @return array of tabs
     */
    val value: Array<SegmentDefinition>
)
