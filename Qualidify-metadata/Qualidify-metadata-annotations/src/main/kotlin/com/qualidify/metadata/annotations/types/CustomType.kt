package com.qualidify.metadata.annotations.types

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.DtoType


/**
 * A annotation indicating that the fields are custom.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.FIELD)
@DtoType(DataType.CUSTOM)
annotation class CustomType(
    /**
     * A reference value indicating which entity the custom fields are defined for.
     *
     * @return Reference
     */
    val reference: Reference
)