package com.qualidify.metadata.annotations

import kotlin.reflect.KClass

/**
 * An annotation for constructors indicating the kind of strategy pattern used for creation of the Object
 * If missing it is assumed the creation will be default: Usage of the empty constructor.
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Retention
@Target(AnnotationTarget.CLASS)
annotation class CreationStrategy(
    /**
     * The strategy used for creation
     */
    val value: CreationStrategyPattern = CreationStrategyPattern.MAP_DTO,

    /**
     * This field can be used in two ways:
     * <li> when [CreationStrategyPattern.OBJECT_CONSTRUCTOR] the fields of the creationClass
     * describing the information needed for the creation of the object.
     * The annotated Dto uses a different object for creation, and it should
     * need a constructor with creationClass as parameter. </li>
     * <li> when [CreationStrategyPattern.FACTORY] the creation of the object is handeled
     * on a different path
     */
    val creationClass: KClass<*> = Nothing::class,
)

/**
 * The different approaches of creation of an object
 *
 * @author Marcel Pot
 * @since 1.0
 */
enum class CreationStrategyPattern {
    /**
     * Will use a Map Editor instead of a dto editor, where all columns are configurable
     * The creation of the DTO's is done based on the key value pairs in the map after 'saving'
     * the entity
     */
    MAP_DTO,

    /**
     * Will use a Dto Editor where the creation strategy uses an empty constructor
     */
    EMPTY_CONSTRUCTOR,

    /**
     * This option will spin of a wizard asking for information based on an other object
     * to fill the information needed for creation of the object
     */
    OBJECT_CONSTRUCTOR,

    /**
     * This option will use a Factory for the creation of an object
     */
    FACTORY

}