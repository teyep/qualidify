package com.qualidify.metadata.service

import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.service.dto.*
import org.springframework.stereotype.Repository
import java.util.*

/**
 * A static MetaData Service to retrieve all configured information for a specific class
 *
 * @author Marcel Pot
 * @since 1.0
 */

@Repository
open class Metadata(metadataService: MetadataService, validationService: MetadataValidationService) {

    companion object : MetadataService {
        private lateinit var metadataService: MetadataService
        private lateinit var metadataValidationService: MetadataValidationService

        /**
         * A static setter, to create the static behaviour
         * @param instance a MetaDataService
         */
        fun setMetaDataService(metadataService: MetadataService) {
            Companion.metadataService = metadataService
        }

        /**
         * A static setter, to create the static behaviour
         * @param instance a MetadataValidationService
         */
        fun setMetadataValidationService(metadataValidationService: MetadataValidationService) {
            Companion.metadataValidationService = metadataValidationService
        }

        @JvmStatic
        override fun getDtoDefinition(mnemonic: String): DtoDef? {
            return metadataService.getDtoDefinition(mnemonic)
        }

        @JvmStatic
        override fun getSingularName(mnemonic: String): String? {
            return getDtoDefinition(mnemonic)?.name
        }

        @JvmStatic
        override fun getPluralName(mnemonic: String): String? {
            return getDtoDefinition(mnemonic)?.pluralName
        }

        @JvmStatic
        override fun isAdditionEnabled(mnemonic: String): Boolean {
            return metadataService.isAdditionEnabled(mnemonic)
        }

        @JvmStatic
        override fun isUpdateEnabled(mnemonic: String): Boolean {
            return metadataService.isUpdateEnabled(mnemonic)
        }

        @JvmStatic
        override fun isDeleteEnabled(mnemonic: String): Boolean {
            return metadataService.isDeleteEnabled(mnemonic)
        }

        @JvmStatic
        override fun isFilteringEnabled(mnemonic: String): Boolean {
            return metadataService.isFilteringEnabled(mnemonic)
        }

        @JvmStatic
        override fun getColumns(mnemonic: String): List<ColumnDef> {
            return metadataService.getColumns(mnemonic)
        }

        @JvmStatic
        override fun getCustomFieldsForReference(reference: Reference): List<CustomFieldDef> {
            return metadataService.getCustomFieldsForReference(reference)
        }

        @JvmStatic
        override fun getMnemonicList(): List<String> {
            return metadataService.getMnemonicList();
        }

        override fun getCreationStrategyPattern(mnemonic: String): CreationStrategyPattern? {
            return metadataService.getCreationStrategyPattern(mnemonic)
        }

        @JvmStatic
        override fun getActionsForType(mnemonic: String): List<ActionInfo> {
            return metadataService.getActionsForType(mnemonic)
        }

        @JvmStatic
        override fun getBatchActionsForType(mnemonic: String): List<ActionInfo> {
            return metadataService.getBatchActionsForType(mnemonic)
        }

        @JvmStatic
        override fun getCreationCommand(mnemonic: String): DtoDef? {
            return metadataService.getCreationCommand(mnemonic)
        }


        @JvmStatic
        override fun getRendererForColumn(mnemonic: String, column: String): RendererInfo? {
            return metadataService.getRendererForColumn(mnemonic, column)
        }

        @JvmStatic
        override fun getSegments(mnemonic: String): List<SegmentInfo> {
            return metadataService.getSegments(mnemonic)
        }


        /**
         * Get a Stream of column defintions for a specific class for editing purposes
         * @param beanType the class
         * @param <T> the type of the class
         * @return a List of column definitions available for editing
         */
        @JvmStatic
        fun <T : Any> getColumnsForEditing(beanType: Class<T>): List<ColumnDef> {
            val mnemonic = ""
            return getColumnsForEditing(mnemonic)

        }

        /**
         * Get a Stream of column defintions for a specific class for editing purposes
         * @param mnemonic of the class
         * @return a List of column definitions available for editing
         */
        @JvmStatic
        fun getColumnsForEditing(mnemonic: String): List<ColumnDef> {
            return metadataService.getColumns(mnemonic).asSequence()
                .filter { !it.hidden }
                .toList()
        }

        /**
         * Get a Stream of column defintions for a specific class for viewing purposes
         * @param beanType the class
         * @param <T> the type of the class
         * @return a List of column definitions available for viewing
         */
        @JvmStatic
        fun <T : Any> getColumnsForViewing(beanType: Class<T>): List<ColumnDef> {
            val mnemonic = ""
            return getColumnsForViewing(mnemonic)
        }

        /**
         * Get a Stream of column defintions for a specific class for viewing purposes
         * @param beanType the class
         * @param <T> the type of the class
         * @return a List of column definitions available for viewing
         */
        @JvmStatic
        fun getColumnsForViewing(mnemonic: String): List<ColumnDef> {
            return metadataService.getColumns(mnemonic).asSequence()
                .filter { !it.hidden }
                .filter { it.viewable }
                .toList()
        }

        /**
         * Get the ValidationService
         * @return the MetadataValidationService
         */
        @JvmStatic
        fun getMetadataValidationService(): MetadataValidationService {
            return metadataValidationService
        }
    }

    init {
        setMetaDataService(metadataService)
        setMetadataValidationService(validationService)
    }


}