package com.qualidify.metadata.service.dto

class ObjectMap(content: Map<String, Any?>) : HashMap<String, Any?>(content) {
    constructor() : this(emptyMap())
    constructor(vararg pairs: Pair<String, Any?>) : this(pairs.toMap())
}
