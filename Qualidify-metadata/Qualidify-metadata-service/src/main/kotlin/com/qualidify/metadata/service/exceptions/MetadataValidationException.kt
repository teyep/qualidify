package com.qualidify.metadata.service.exceptions

/**
 * An exception indicating a problem while validating based on metadata
 *
 * @author Marcel Pot
 * @since 1.0
 */
class MetadataValidationException(message: String, cause: Throwable? = null) : MetadataException(message, cause)