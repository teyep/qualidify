package com.qualidify.metadata.service

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.Reference

/**
 * A service interface for validating metadata configured objects
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface MetadataValidationService {

    /**
     * Validate an data tranfer object
     * @param mnemonic the mnemonic
     * @param dto the object (with an identity)
     * @throws MetadataValidationException
     */
    fun validate(mnemonic: String, dto: HasIdentity)

    /**
     * Check if all mandatory fields are filled in (defined in [CustomField])
     * @param reference the reference of the customfields
     * @param fields the fields to validate
     * @throws MetadataValidationException
     */
    fun checkAllMandatoryCustomFieldsArePresent(reference: Reference, fields: Map<String, *>)

    /**
     * Check if all fields are allowed (defined in [CustomField])
     * @param reference the reference of the customfields
     * @param fields the fields to validate
     * @throws MetadataValidationException
     */
    fun checkAllCustomFieldsAreAllowed(reference: Reference, fields: Map<String, *>)

}
