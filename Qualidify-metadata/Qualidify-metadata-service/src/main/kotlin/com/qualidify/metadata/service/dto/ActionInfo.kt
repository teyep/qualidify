package com.qualidify.metadata.service.dto

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.qualidify.metadata.annotations.Position

/**
 * A Value object containing information for GridButtons
 *
 * @author Marcel Pot
 * @since 1.0
 */
@JsonPropertyOrder(value = ["label", "action", "position", "order"])
data class ActionInfo(
        val label: String,
        val action: String,
        val position: Position = Position.END,
        val order: Int = 0,
)