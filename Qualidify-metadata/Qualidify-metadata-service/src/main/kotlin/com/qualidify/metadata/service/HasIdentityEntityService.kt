package com.qualidify.metadata.service

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import java.io.Serializable

/**
 * A Service for Metadata Entities which implementing the [HasIdentity] interface
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface HasIdentityEntityService {

    /**
     * Create an object
     *
     * @param mnemonic the mnemonic of the object
     * @param hasIdentity the data of the object
     */
    fun create(mnemonic: String, hasIdentity: HasIdentity)

    /**
     * Update an object
     *
     * @param mnemonic the mnemonic of the object
     * @param hasIdentity the data of the object
     */
    fun update(mnemonic: String, hasIdentity: HasIdentity)

    /**
     * Delete an object
     *
     * @param mnemonic the mnemonic of the object
     * @param hasIdentity the data of the object
     */
    fun delete(mnemonic: String, hasIdentity: HasIdentity)

    /**
     * A slightly different approach of the [Queryable] interface because
     * this method does need the mnemonic and a type paramater
     *
     * @param E the type of the database entity
     * @param mnemonic the mnemonic of the object
     * @param query the query containing the count selection
     */
    fun <E : Serializable> countByQuery(mnemonic: String, query: CountQuery): CountResponse

    /**
     * A slightly different approach of the [Queryable] interface because
     * this method does need the mnemonic and a type paramater
     *
     * @param D the type of the dto returning
     * @param E the type of the database entity
     * @param mnemonic the mnemonic of the object
     * @param query the query containing the search selection
     */
    fun <D : HasIdentity, E : Serializable> findByQuery(mnemonic: String, query: SearchQuery): Collection<D>
}