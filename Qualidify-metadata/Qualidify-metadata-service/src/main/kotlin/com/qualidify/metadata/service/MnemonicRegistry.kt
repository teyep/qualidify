package com.qualidify.metadata.service

import com.qualidify.core.servicelocator.ServiceLocator
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct
import kotlin.reflect.KClass

interface MnemonicRegistry {

    /**
     * Get the entriesset of this registry
     * @return the entrieset with mnemonic pointing to kotlin classes
     */
    val entries: Set<Map.Entry<String, KClass<*>>>

    /**
     * Get the Kotlin class beloning to a mnemonic
     * @param[menmonic] the Mnemonic
     * @return the Kotlin Class
     */
    fun get(menmonic: String): KClass<*>?

    /**
     * Get the mnemonic of a class
     * @param[kClass] the Kotlin class
     * @return the mnemonic pointing to this kotlin class
     */
    fun mnemonicOf(kClass: KClass<*>): String?


    fun addForScanning(vararg kClass: KClass<*>)

    companion object {

        private var instance: MnemonicRegistry? = null

        fun get(): MnemonicRegistry {
            if (instance == null) {
                instance = ServiceLocator.getServiceByType(MnemonicRegistry::class.java)
            }
            return instance!!
        }
    }

    @Component
    class MnemonicRegistryInjector(
        val registry: MnemonicRegistry,
    ) {
        /**
         * Post construction post processing taking care of injecting the Registry into the companion object
         */
        @PostConstruct
        fun postConstruct() {
            instance = registry
        }
    }
}