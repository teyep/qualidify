package com.qualidify.metadata.service

import com.qualidify.core.query.service.Queryable
import com.qualidify.metadata.service.dto.CustomFieldDef

/**
 * A Service for Custom Fields Definitions
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface CustomFieldService : Queryable<CustomFieldDef> {

    /**
     * Saves a CustomField
     * @param[field] the CustomField to create
     */
    fun create(field: CustomFieldDef)


    /**
     * Update a CustomField
     * @param[field] The CustomField to update
     */
    fun update(field: CustomFieldDef)

    /**
     * Deletes a CustomField (will be deactivated)
     * @param[field] the CustomField
     */
    fun delete(field: CustomFieldDef)

}