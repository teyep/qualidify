package com.qualidify.metadata.service.repository

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import java.io.Serializable

/**
 * A Repository interface for CustomMetadata
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface CustomMetadataEntityRepository {
    /**
     * Save a metadata entity
     * @param entity the entity to save
     * @param E the Type of the Entity
     * @return the saved Entity
     */
    fun <E> save(entity: E): E

    /**
     * Update a metadata entity
     * @param entity the entity to update
     * @param E the Type of the Entity
     * @return the updated Entity
     */
    fun <E> update(entity: E): E

    /**
     * Delete a metadata entity
     * @param entity the entity to delete
     * @param E the Type of the Entity
     */
    fun <E> delete(entity: E)

    /**
     * Find all metadata entities
     * @param mnemonic the Mnemonic
     * @param specification the specification
     * @param E the Type of the Entity
     * @param pageable the pageinfo
     * @return A paged slice of the data
     */
    fun <E : Any> findAll(mnemonic: String, specification: Specification<E>?, pageable: Pageable?): Page<E>

    /**
     * Count all metadata entities
     * @param mnemonic the Mnemonic
     * @param specification the specification
     * @param E the Type of the Entity
     * @return the number of found entities
     */
    fun <E : Any> countAll(mnemonic: String, specification: Specification<E>?): Long
}

/**
 * Find a paginated slice of the data based on a SearchQuery
 * Written as extension function to prevent Spring to create a query for this function
 * @param mnemonic the mnemonic
 * @param query the SearchQuery
 * @return a pageable slice of the data
 */
fun <T : Serializable> CustomMetadataEntityRepository.findAllByQuery(mnemonic: String, query: SearchQuery): Page<T> {
    val page = query.toPage()
    val specification = query.toSpecification<T>()
    return findAll(mnemonic, specification, page)
}


/**
 * Count the data based on the specification in the countQuery
 * Written as extension function to prevent Spring to create a query for this function
 * @param mnemonic the mnemonic
 * @param query the CountQuery
 * @return the number of records found
 */
fun <T : Serializable> CustomMetadataEntityRepository.countByQuery(mnemonic: String, query: CountQuery): Long {
    val specification = query.toSpecification<T>()
    return countAll(mnemonic, specification)
}

/**
 * Returns a Count response based on the specification in the countQuery
 * Written as extension function to prevent Spring to create a query for this function
 * @param mnemonic the mnemonic
 * @param query the CountQuery
 * @return the number of records found
 */
fun <T : Serializable> CustomMetadataEntityRepository.countResponse(
    mnemonic: String,
    query: CountQuery
): CountResponse {
    return CountResponse(countByQuery<T>(mnemonic, query))
}