package com.qualidify.metadata.service.dto

data class DtoView(
    val dtoDef: DtoDef?,
    val actions: Array<ActionInfo> = emptyArray(),
    val batchActions: Array<ActionInfo> = emptyArray(),
    val segment: Array<SegmentInfo> = emptyArray()
)

