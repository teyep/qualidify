package com.qualidify.metadata.service.dto

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.Hidden
import com.qualidify.metadata.annotations.types.BooleanType
import com.qualidify.metadata.annotations.types.EnumerationType
import java.util.*


@JsonPropertyOrder(value = ["id", "reference", "field", "description", "dataType", "validator", "mandatory", "active"])
data class CustomFieldDef(
    @Hidden
    var id: UUID?,

    @EnumerationType(itemProvider = "referenceEnumItemProvider")
    var reference: Reference,

    var field: String,

    var description: String,

    @EnumerationType(itemProvider = "dataTypeEnumItemProvider")
    var dataType: DataType,

    @EnumerationType(itemProvider = "validatorItemProvider")
    var validator: String? = null,

    @BooleanType
    var mandatory: Boolean = false,

    @BooleanType
    var active: Boolean = true,

    ) : HasIdentity {

    override fun identity(): Array<Any> {
        return arrayOf(id!!)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CustomFieldDef

        if (reference != other.reference) return false
        if (field != other.field) return false

        return true
    }

    override fun hashCode(): Int {
        var result = reference.hashCode()
        result = 31 * result + field.hashCode()
        return result
    }

    fun toColumnDef(): ColumnDef = ColumnDef(
        name = field,
        description = description,
        dataType = dataType,
        attributes = mapOf(),
        false,
        true,
        true,
        true,
        fieldAccess = ColumnDef.FieldAccess.MAP
    )

}