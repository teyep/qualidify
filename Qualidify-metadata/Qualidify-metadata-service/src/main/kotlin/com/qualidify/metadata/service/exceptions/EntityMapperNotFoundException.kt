package com.qualidify.metadata.service.exceptions

class EntityMapperNotFoundException(s: String? = null, throwable: Throwable? = null) : MetadataException(s, throwable)