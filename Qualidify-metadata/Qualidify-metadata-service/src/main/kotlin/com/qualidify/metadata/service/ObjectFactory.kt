package com.qualidify.metadata.service

/**
 * An interface for the CreationStrategy Pattern [CreationStrategyPattern.FACTORY]
 * indicating how an object D can be created based on an object C
 *
 * @param D the type of object to create
 * @param C the type of command object containing all creation information
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface ObjectFactory<D, C> {

    /**
     * Create an object D out of an object C
     * @param command the command information containing the data for the creation of object D
     * @return a object of class D
     */
    fun create(command: C): D
}