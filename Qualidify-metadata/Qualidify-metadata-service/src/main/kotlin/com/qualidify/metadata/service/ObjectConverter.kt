package com.qualidify.metadata.service


import com.qualidify.metadata.service.dto.ObjectMap
import kotlin.reflect.KClass

interface ObjectConverter {
    /**
     * Get a Map with all properties of an object
     * @param bean the object to read
     * @param T the type of the bean
     * @return a Map with all fields and their values
     */
    fun <T> convertToMap(bean: T): ObjectMap

    /**
     * Map an object to a Map
     * @param bean the object to map
     * @param beanType the kclass of the object
     * @param T the type of the object
     * @return an objectMap containing all fields as a key value pair
     */
    fun <T : Any> convertToMapWithCustomFields(bean: T, beanType: KClass<out T>): ObjectMap

    /**
     * Map a DtoMap to a Dto object
     * @param map an objectMap
     * @param beanType the kotlin class of the object
     * @param T the type of the object
     * @return the dto object
     */
    fun <T : Any> convertToObject(map: Map<String, Any?>, beanType: KClass<T>): T

}