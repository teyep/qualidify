package com.qualidify.metadata.service

import com.qualidify.metadata.annotations.CreationStrategy
import com.qualidify.metadata.annotations.CreationStrategyPattern
import kotlin.reflect.KClass

interface ObjectCreator {
    /**
     * Create an object based on a class. This function will scan for the [CreationStrategy] annotation
     * on the class and try to create the object
     *
     * @param beanType the type to create
     * @param params   the parameters for the constructor
     * @param T     the type to create
     * @return the object of type T
     * @throws DtoCreationException if a problem occurs
     */
    fun <T : Any> create(beanType: KClass<T>, vararg params: Any?): T

    /**
     * Get a Factory for creation of an object
     * @param beanType the type to create
     * @param T      the type to create
     * @param command   the parameters for the constructor
     * @param C the command Type
     * @return an objectFactory
     */
    fun <T : Any, C : Any> getObjectFactory(beanType: KClass<T>, command: KClass<C>): ObjectFactory<T, C>

    /**
     * Determine the Strategy to create a new object for a beantype
     *
     * @param beanType the type to create
     * @param T   the Type to create
     * @return the creation strategy
     */
    fun <T : Any> getCreationStrategy(beanType: KClass<T>): CreationStrategy?

    /**
     * Determine the Strategy to create a new object for a beantype
     *
     * @param beanType the type to create
     * @param T   the Type to create
     * @return the creation strategy
     */
    fun <T : Any> determineCreationStrategyPattern(beanType: KClass<T>): CreationStrategyPattern
}

