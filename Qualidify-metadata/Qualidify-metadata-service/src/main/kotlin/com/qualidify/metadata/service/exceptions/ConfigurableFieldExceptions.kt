package com.qualidify.metadata.service.exceptions

class ConfigurableFieldNotFoundException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)