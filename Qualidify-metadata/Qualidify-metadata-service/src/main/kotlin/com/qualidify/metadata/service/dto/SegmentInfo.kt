package com.qualidify.metadata.service.dto

/**
 * Data class containing information for segmentation of a dto
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-386
 */
data class SegmentInfo(
    val name: String,
    val description: String = ""

) {
    companion object {
        @JvmStatic
        fun default() = SegmentInfo("DEFAULT")
    }
}