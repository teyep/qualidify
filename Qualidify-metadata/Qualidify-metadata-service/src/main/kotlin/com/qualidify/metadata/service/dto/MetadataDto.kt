package com.qualidify.metadata.service.dto

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.DtoColumn
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.Hidden
import com.qualidify.metadata.annotations.Mnemonic
import com.qualidify.metadata.annotations.types.EnumerationType
import com.qualidify.metadata.annotations.types.GridType
import com.qualidify.metadata.service.dto.MetadataObjectDto.Companion.MNEMONIC
import java.io.Serializable

/**
 * A Dto for Metadata Objects for updating purposes
 *
 * @author Marcel Pot
 * @since 1.0
 */

@Mnemonic(MNEMONIC,
    singular = "Metadata Object",
    plural = "Metadata Objects",
    enableAddition = false)
data class MetadataObjectDto(
    @DtoColumn(editable = false)
    val mnemonic: String?,
    val singlename: String,
    val pluralname: String,

    @SearchQueryHint("addable")
    val addEnabled: Boolean,
    @SearchQueryHint("updatable")
    val updateEnabled: Boolean,
    @SearchQueryHint("removable")
    val deleteEnabled: Boolean,
    @SearchQueryHint("searchable")
    val searchEnabled: Boolean,

    @DtoColumn(viewable = false)
    @GridType(mnemonic = "MetadataColumn")
    val columns: List<MetadataColumnDto>,
) : HasIdentity, Serializable {


    @DtoMetadata(
        singular = "Metadata Column",
        plural = "Metadata Columns",
        enableAddition = false)
    data class MetadataColumnDto(
        val name: String,
        val description: String,

        @EnumerationType(itemProvider = "dataTypeEnumItemProvider")
        val dataType: DataType,


        @Hidden
        val attributes: Map<String, String> = mapOf(),

        val sortorder: Int,
        val hidden: Boolean = false,
        val viewable: Boolean = true,
        val editable: Boolean = true,
        val searchable: Boolean = true,
        val sortable: Boolean = true,
    ) : Serializable

    override fun identity(): Array<Any> {
        return arrayOf(mnemonic!!)
    }

    companion object {
        const val MNEMONIC: String = "Metadataobject"
    }
}