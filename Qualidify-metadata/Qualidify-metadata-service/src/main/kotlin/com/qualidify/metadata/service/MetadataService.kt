package com.qualidify.metadata.service

import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.service.dto.*

/**
 * A Service for MetaData configuration
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface MetadataService {
    /**
     * Get the full metadata definition of the beantype
     * @param[mnemonic] the mnemonic of the Dto
     * @return the full definition
     */
    fun getDtoDefinition(mnemonic: String): DtoDef?

    /**
     * Get the name of this beanType in singular form
     * @param[mnemonic] the mnemonic of the Dto
     * @return the name (singular)
     */
    fun getSingularName(mnemonic: String): String?

    /**
     * Get the name of this beanType in plural form
     * @param[mnemonic] the mnemonic of the Dto
     * @return the name (plural)
     */
    fun getPluralName(mnemonic: String): String?

    /**
     * Checks if addition is enabled for a specific class
     * @param[mnemonic] the mnemonic of the Dto
     * @return boolean indicating if addition is enabled
     */
    fun isAdditionEnabled(mnemonic: String): Boolean

    /**
     * Checks if update is enabled for a specific class
     * @param[mnemonic] the mnemonic of the Dto
     * @return boolean indicating if update is enabled
     */
    fun isUpdateEnabled(mnemonic: String): Boolean

    /**
     * Checks if deletion is enabled for a specific class
     * @param[mnemonic] the mnemonic of the Dto
     * @return boolean indicating if deletion is enabled
     */
    fun isDeleteEnabled(mnemonic: String): Boolean

    /**
     * Checks if filtering is enabled for a specific class
     * @param[mnemonic] the mnemonic of the Dto
     * @return boolean indicating if filtering is enabled
     */
    fun isFilteringEnabled(mnemonic: String): Boolean

    /**
     * Get a List of column defintions for a specific class
     * @param[mnemonic] the mnemonic of the Dto
     * @return a Flux of column definitions
     */
    fun getColumns(mnemonic: String): List<ColumnDef>

    /**
     * Get all defined custom fields for a [Reference]
     * @param reference The reference
     * @return a flux of CustomFields
     */
    fun getCustomFieldsForReference(reference: Reference): List<CustomFieldDef>

    /**
     * Get a list of all available mnemonics
     * @return a list with all mnemonics
     */
    fun getMnemonicList(): List<String>

    /**
     * Get the adviced CreationStrategy Pattern for this bean
     * @param[mnemonic] the mnemonic of the Dto
     * @return The CreationStrategyPattern
     */
    fun getCreationStrategyPattern(mnemonic: String): CreationStrategyPattern?

    /**
     * Get the (entity) action information
     * @param[mnemonic] the mnemonic of the Dto
     * @return a list with Action information
     */
    fun getActionsForType(mnemonic: String): List<ActionInfo>

    /**
     * Get the bulk action information
     * @param[mnemonic] the mnemonic of the Dto
     * @return a list with Action information
     */
    fun getBatchActionsForType(mnemonic: String): List<ActionInfo>

    /**
     * Get the Creation class for a mnemonic
     * Is some cases it is usefull to have another dto as creation class than
     * the usual view
     * @param[mnemonic] the mnemonic of the Dto
     * @return a Dto Definition
     */
    fun getCreationCommand(mnemonic: String): DtoDef?

    /**
     * Get the Renderer for a specific field
     * @param[mnemonic] the mnemonic of the Dto
     * @param column the name of the column
     * @return the Renderer information if available
     */
    fun getRendererForColumn(mnemonic: String, column: String): RendererInfo?

    /**
     * Get the segments for a mnemonic
     * @param[mnemonic] the mnenomic of the Dto
     * @return An ordered list containing the segments information
     */
    fun getSegments(mnemonic: String): List<SegmentInfo>
}




