package com.qualidify.metadata.service.dto

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.qualidify.core.identity.HasIdentity

/**
 * A value object containing metadata of an object, indicating how an object should
 * be presented on the screens
 *
 * @author Marcel Pot
 * @since 1.0
 */
@JsonPropertyOrder(
    value = [
        "mnemonic",
        "name",
        "pluralName",
        "addEnabled",
        "updateEnabled",
        "deleteEnabled",
        "searchEnabled",
        "columns"]
)
data class DtoDef
@JvmOverloads constructor(
    val mnemonic: String,
    val name: String,
    val pluralName: String,

    val addEnabled: Boolean = true,
    val updateEnabled: Boolean = true,
    val deleteEnabled: Boolean = true,
    val searchEnabled: Boolean = true,
    var columns: MutableList<ColumnDef> = mutableListOf(),
) : HasIdentity {

    fun addColumn(column: ColumnDef) {
        column.copy(mnemonic = this.mnemonic)
        columns.add(column.copy(mnemonic = this.mnemonic))
    }

    override fun toString(): String {
        return "DtoDef(" +
                "mnemonic='$mnemonic, " + System.lineSeparator() +
                "name='$name', " + System.lineSeparator() +
                "pluralName='$pluralName', " + System.lineSeparator() +
                "addEnabled=$addEnabled, " + System.lineSeparator() +
                "updateEnabled=$updateEnabled, " + System.lineSeparator() +
                "deleteEnabled=$deleteEnabled, " + System.lineSeparator() +
                "searchEnabled=$searchEnabled, " + System.lineSeparator() +
                "columns=   $columns)"
    }

    override fun identity(): Array<Any> {
        return arrayOf(mnemonic)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DtoDef) return false

        if (mnemonic != other.mnemonic) return false
        if (name != other.name) return false
        if (pluralName != other.pluralName) return false
        if (addEnabled != other.addEnabled) return false
        if (updateEnabled != other.updateEnabled) return false
        if (deleteEnabled != other.deleteEnabled) return false
        if (searchEnabled != other.searchEnabled) return false
        if (!columns.equals(other.columns)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + pluralName.hashCode()
        result = 31 * result + mnemonic.hashCode()
        result = 31 * result + addEnabled.hashCode()
        result = 31 * result + updateEnabled.hashCode()
        result = 31 * result + deleteEnabled.hashCode()
        result = 31 * result + searchEnabled.hashCode()
        result = 31 * result + columns.hashCode()
        return result
    }


}

