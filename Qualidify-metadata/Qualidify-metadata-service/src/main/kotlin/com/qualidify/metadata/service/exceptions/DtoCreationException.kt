package com.qualidify.metadata.service.exceptions

/**
 * An Exception indicating the creation of a DTO has failed
 *
 * @author Marcel Pot
 * @since 1.0
 */
class DtoCreationException(message: String? = null, cause: Throwable? = null) : MetadataException(message, cause)