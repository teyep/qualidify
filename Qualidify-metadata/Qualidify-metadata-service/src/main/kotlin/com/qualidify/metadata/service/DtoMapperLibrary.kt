package com.qualidify.metadata.service

import com.qualidify.core.identity.HasIdentity
import kotlin.reflect.KClass

interface DtoMapperLibrary {
    /**
     * Map a dto to an entity based on registered mappers
     * @param dto the dto
     * @param D the type of the dto
     * @pararm E the type of the entity
     * @return the entity
     */
    fun <E : Any, D> convertToEntity(dto: D): E where D : Any, D : HasIdentity

    /**
     * Map an entity to a dto based on registered mappers
     * @param entity the entity
     * @param D the type of the dto
     * @pararm E the type of the entity
     * @return the dto
     */
    fun <E : Any, D> convertToDto(entity: E): D where D : Any, D : HasIdentity

    /**
     * convert a PojoMap combined with a mnemonic to an Entity
     * @param mnemonic the Mnemonic
     * @param map the PojoMap
     * @param E The type of the entity
     * @return the Dto
     */
    fun <E : Any> convertFromPojoMapToEntity(mnemonic: String, map: Map<String, Any?>): E

    /**
     * Determine which entity is used for a mnemonic
     * @param mnemonic the mnemonic
     * @return the entity class belonging to this entity
     */
    fun getEntityClassFromMnemonic(mnemonic: String): KClass<*>

    /**
     * Determine which entity is used for a mnemonic
     * @param mnemonic the mnemonic
     * @return the entity class belonging to this entity
     */
    fun getDtoClassFromMnemonic(mnemonic: String): KClass<*>
}