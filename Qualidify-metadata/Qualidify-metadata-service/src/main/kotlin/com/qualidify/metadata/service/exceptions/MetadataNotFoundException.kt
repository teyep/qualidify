package com.qualidify.metadata.service.exceptions

/**
 * An exception for the possibility that the metadata cannot be found in the repository
 *
 * @author Marcel Pot
 * @since 1.0
 */
class MetadataNotFoundException(message: String) : MetadataException(message)