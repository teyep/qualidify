package com.qualidify.metadata.service.dto

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.types.AttributeConstants
import org.apache.commons.lang3.StringUtils
import java.io.Serializable

/**
 * A value object containing metadata of a column/field of an object. It will indicate
 * how this column should be presented on the screen
 *
 * @author Marcel Pot
 * @since 1.0
 */

@JsonPropertyOrder(value = ["name", "description", "dataType", "attributes", "hidden", "viewable", "editable", "searchable", "sortable", "useMapAccess", "mnemonic"])
@JsonIgnoreProperties(value = ["label", "id", "itemProvider", "reference"])
data class ColumnDef @JvmOverloads constructor(
    val name: String,
    val description: String = name,
    val dataType: DataType,
    val attributes: Map<String, String> = mapOf(),

    val hidden: Boolean = false,
    val viewable: Boolean = true,
    val editable: Boolean = true,
    val searchable: Boolean = true,
    val sortable: Boolean = true,

    val sortOrder: Int = 0,
    val mnemonic: String = "",
    val fieldAccess: FieldAccess = FieldAccess.DEFAULT,
    val segments: Array<String> = emptyArray(),
    val rendererInfo: RendererInfo? = null
) : Serializable {

    /**
     * Property-based creator, to construct a ColumnDef from a JSON entry
     */
    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    constructor(
        @JsonProperty("name") name: String,
        @JsonProperty("description") description: String,
        @JsonProperty("dataType") dataType: String,
        @JsonProperty("attributes") attributes: Map<String, String>
    ) : this(name, description, DataType.valueOf(dataType), attributes)

    constructor(
        name: String,
        description: String = name,
        dataType: String,

        hidden: Boolean = false,
        viewable: Boolean = true,
        editable: Boolean = true,
        searchable: Boolean = true,
        sortable: Boolean = true,

        sortOrder: Int = 0,
        mnemonic: String = "",
    ) : this(
        name = name,
        description = description,
        dataType = DataType.valueOf(dataType),
        hidden = hidden,
        viewable = viewable,
        editable = editable,
        searchable = searchable,
        sortable = sortable,
        sortOrder = sortOrder,
        mnemonic = mnemonic
    )

    val label: String?
        get() = getStringAttribute(AttributeConstants.LABEL)

    val id: String?
        get() = getStringAttribute(AttributeConstants.ID)

    val itemProvider: String?
        get() = getStringAttribute(AttributeConstants.ITEM_PROVIDER)

    val reference: Reference?
        get() = getEnumAttribute<Reference>(AttributeConstants.REFERENCE)

    private fun removeEmptyString(it: String?) = if (!StringUtils.isEmpty(it)) it else null

    fun getIntegerAttribute(key: String): Int? {
        return attributes.get(key)?.toIntOrNull()
    }

    fun getIntegerAttributeOrDefault(key: String, defaultValue: Int): Int {
        return attributes.get(key)?.toIntOrNull() ?: defaultValue
    }

    fun getLongAttribute(key: String): Long? {
        return attributes.get(key)?.toLongOrNull()
    }

    fun getLongAttributeOrDefault(key: String, defaultValue: Long): Long {
        return attributes.get(key)?.toLongOrNull() ?: defaultValue
    }

    fun getDoubleAttribute(key: String): Double? {
        return attributes.get(key)?.toDoubleOrNull()
    }

    fun getDoubleAttributeOrDefault(key: String, defaultValue: Double): Double {
        return attributes.get(key)?.toDoubleOrNull() ?: defaultValue
    }

    fun getBooleanAttribute(key: String): Boolean? {
        return attributes.get(key)?.toBoolean()
    }

    fun getBooleanAttributeOrDefault(key: String, defaultValue: Boolean): Boolean {
        return attributes.get(key)?.toBoolean() ?: defaultValue
    }

    fun getStringAttribute(key: String): String? {
        return removeEmptyString(attributes.get(key))
    }

    fun getStringAttributeOrDefault(key: String, defaultValue: String): String {
        return removeEmptyString(attributes.get(key)) ?: defaultValue
    }

    fun getClassAttribute(key: String): Class<*>? {
        return attributes.get(key)?.let { Class.forName(it) }
    }

    fun getStringArrayAttribute(key: String): Array<String> {
        val commaSeperatedString = attributes.get(key)
        if (StringUtils.isEmpty(commaSeperatedString)) {
            return emptyArray()
        } else {
            return commaSeperatedString?.split(",")?.toTypedArray() ?: emptyArray()
        }
    }

    inline fun <reified E : Enum<E>> getEnumAttribute(key: String): E? {
        return attributes.get(key)?.let { enumValueOf<E>(it) }
    }

    override fun toString(): String {
        return "ColumnDef(" +
                "mnemonic=$mnemonic, " +
                "name=$name, " +
                "description=$description, " +
                "dataType=$dataType, " +
                "attributes=$attributes, " +
                "hidden=$hidden, " +
                "viewable=$viewable, " +
                "editable=$editable, " +
                "searchable=$searchable) "
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ColumnDef) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (dataType != other.dataType) return false
        if (hidden != other.hidden) return false
        if (viewable != other.viewable) return false
        if (editable != other.editable) return false
        if (searchable != other.searchable) return false
        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + dataType.hashCode()
        result = 31 * result + hidden.hashCode()
        result = 31 * result + viewable.hashCode()
        result = 31 * result + editable.hashCode()
        result = 31 * result + searchable.hashCode()
        return result
    }

    enum class FieldAccess {
        DEFAULT,
        MAP
    }
}