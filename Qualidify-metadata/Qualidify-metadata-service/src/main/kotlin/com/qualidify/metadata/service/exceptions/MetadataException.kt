package com.qualidify.metadata.service.exceptions

open class MetadataException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)