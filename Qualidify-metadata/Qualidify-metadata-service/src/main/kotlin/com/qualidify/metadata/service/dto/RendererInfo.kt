package com.qualidify.metadata.service.dto

import com.fasterxml.jackson.annotation.JsonPropertyOrder


/**
 * A Value object containing information for Rendering
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-554
 */
@JsonPropertyOrder(value = ["name", "attributes"])
data class RendererInfo(
    val name: String,
    val attributes: Map<String, String> = emptyMap()
)
