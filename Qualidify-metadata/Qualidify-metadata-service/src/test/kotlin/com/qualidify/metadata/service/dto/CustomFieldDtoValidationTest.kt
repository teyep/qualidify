package com.qualidify.metadata.service.dto

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*
import javax.validation.Validation
import javax.validation.Validator

internal class CustomFieldDtoValidationTest {

    private var validator: Validator? = null

    @BeforeEach
    fun setUp() {
        val factory = Validation.buildDefaultValidatorFactory()
        validator = factory.validator
    }

    @Test
    fun testConfigurableFieldValidationSuccesfull() {
        val field = CustomFieldDef(
            UUID.randomUUID(),
            Reference.USER,
            "EMAIL",
            "email",
            DataType.EMAIL,
            null,
            true,
            false
        )

        val violations = validator!!.validate(field)
        Assertions.assertThat(violations).`as`("ConfigurableFieldDto is perfectly filled").isEmpty()
    }


}