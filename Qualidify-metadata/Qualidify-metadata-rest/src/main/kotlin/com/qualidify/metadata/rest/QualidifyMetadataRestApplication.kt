package com.qualidify.metadata.rest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class QualidifyMetadataRestApplication

fun main(args: Array<String>) {
    runApplication<QualidifyMetadataRestApplication>(*args)
}