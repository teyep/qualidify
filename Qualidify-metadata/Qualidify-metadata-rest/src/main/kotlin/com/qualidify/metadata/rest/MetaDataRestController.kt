package com.qualidify.metadata.rest

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.RestQuery
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.HasIdentityEntityService
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.dto.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.Serializable

@RestController
@RequestMapping("/metadata")
class MetaDataRestController(
    val metadataService: MetadataService,
    val hasIdentityEntityService: HasIdentityEntityService
) {

    @PostMapping("/{mnemonic}/count")
    fun countByMnemonic(
        @PathVariable("mnemonic") mnemonic: String,
        @RequestBody query: RestQuery
    ): Mono<CountResponse> {
        return Mono.fromCallable {
            hasIdentityEntityService.countByQuery<HasIdentity>(mnemonic, query.toCountQuery<HasIdentity>())
        }
    }

    @PostMapping("/{mnemonic}/find")
    fun findByMnemonic(
        @PathVariable("mnemonic") mnemonic: String,
        @RequestBody query: RestQuery
    ): Flux<out HasIdentity> {
        return Mono.fromCallable {
            hasIdentityEntityService.findByQuery<HasIdentity, Serializable>(
                mnemonic,
                query.toSearchQuery<HasIdentity>()
            )
        }.flatMapIterable { it }
    }

    @GetMapping("/dtoview/{mnemonic}")
    fun getDtoViewByMnemonic(@PathVariable("mnemonic") mnemonic: String): Mono<DtoView> {
        return Mono.fromCallable {
            DtoView(
                dtoDef = metadataService.getDtoDefinition(mnemonic),
                actions = metadataService.getActionsForType(mnemonic).toTypedArray(),
                batchActions = metadataService.getBatchActionsForType(mnemonic).toTypedArray(),
                segment = metadataService.getSegments(mnemonic).toTypedArray()
            )
        }
    }

    @GetMapping("/dtodef/{mnemonic}")
    fun getDtoDefininitionByMnemonic(@PathVariable("mnemonic") mnemonic: String): Mono<DtoDef> {
        return Mono.fromCallable {
            metadataService.getDtoDefinition(mnemonic)
        }
    }

    @GetMapping("/actions/{mnemonic}")
    fun getActionsByMnemonic(@PathVariable("mnemonic") mnemonic: String): Flux<ActionInfo> {
        return Flux.fromIterable(metadataService.getActionsForType(mnemonic))
    }

    @GetMapping("/batchactions/{mnemonic}")
    fun getBatchActionsByMnemonic(@PathVariable("mnemonic") mnemonic: String): Flux<ActionInfo> {
        return Flux.fromIterable(metadataService.getBatchActionsForType(mnemonic))
    }

    @GetMapping("/renderer/{mnemonic}/{field}")
    fun getRendererInfoByMnemonicAndField(
        @PathVariable("mnemonic") mnemonic: String,
        @PathVariable("field") field: String
    ): Mono<RendererInfo> {
        return Mono.fromCallable {
            metadataService.getRendererForColumn(mnemonic, field)
        }
    }

    @GetMapping("/segments/{mnemonic}")
    fun getSegments(@PathVariable("mnemonic") mnemonic: String): Flux<SegmentInfo> {
        return Flux.fromIterable(metadataService.getSegments(mnemonic))
    }

    @GetMapping("/customfields/{reference}")
    fun getCustomFieldsForReference(@PathVariable("reference") reference: String): Flux<CustomFieldDef> {
        return Mono.fromCallable {
            metadataService.getCustomFieldsForReference(reference = Reference.valueOf(reference.uppercase()))
        }.flatMapIterable { it }
    }

    @ExceptionHandler(
        value = [
            ClassNotFoundException::class,
            IllegalArgumentException::class,
            ClassCastException::class
        ]
    )
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleException(exception: Exception) = exception.message


}