package com.qualidify.metadata.rest

import com.qualidify.core.query.RestQuery
import com.qualidify.metadata.service.CustomFieldService
import com.qualidify.metadata.service.dto.CustomFieldDef
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/customfield")
class CustomFieldRestController(private val customFieldService: CustomFieldService) {

    @PostMapping("/count")
    fun count(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            customFieldService.countByQuery(query = query.toCountQuery<CustomFieldDef>())
        }

    @PostMapping("/find")
    fun find(@RequestBody query: RestQuery) =
        Mono.fromCallable {
            customFieldService.findByQuery(query = query.toSearchQuery<CustomFieldDef>())
        }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun create(@RequestBody dto: CustomFieldDef) {
        customFieldService.create(dto)
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun update(@RequestBody dto: CustomFieldDef) {
        customFieldService.update(dto)
    }

    @PutMapping("/delete")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun delete(@RequestBody dto: CustomFieldDef) {
        customFieldService.delete(dto)
    }

    @ExceptionHandler(value = [IllegalArgumentException::class])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun badRequest(exception: Exception) = exception.message

}