package com.qualidify.metadata.rest

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.RestQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.HasIdentityEntityService
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.dto.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import java.io.Serializable
import java.util.*

@WebFluxTest(controllers = [MetaDataRestController::class])
@ExtendWith(SpringExtension::class)
@WithMockUser
class MetaDataRestControllerTest {

    @Autowired
    private lateinit var webClient: WebTestClient

    @MockBean
    lateinit var metadataService: MetadataService

    @MockBean
    lateinit var hasIdentityEntityService: HasIdentityEntityService

    @Test
    fun mnemonicCount() {
        val countQuery = CountQuery<HasIdentity>()
        val result = (12..16).random().toLong()

        `when`(hasIdentityEntityService.countByQuery<HasIdentity>("mnemonic", countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/metadata/mnemonic/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(RestQuery())
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(hasIdentityEntityService).countByQuery<HasIdentity>("mnemonic", countQuery)
    }

    @Test
    fun mnemonicFind() {
        val searchQuery = SearchQuery<HasIdentity>()

        `when`(hasIdentityEntityService.findByQuery<HasIdentity, Serializable>("mnemonic", searchQuery))
            .thenReturn(listOf(MnemonicClass(), MnemonicClass(), MnemonicClass()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/metadata/mnemonic/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(RestQuery())
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.length()").isEqualTo(3)

        verify(hasIdentityEntityService).findByQuery<HasIdentity, Serializable>("mnemonic", searchQuery)
    }

    @Test
    fun getDtoViewByMnemonic() {
        `when`(metadataService.getDtoDefinition("mnemonic")).thenReturn(DtoDef("mnemonic", "m", "m&m"))
        `when`(metadataService.getActionsForType("mnemonic"))
            .thenReturn(listOf(ActionInfo("Eat", "eat"), ActionInfo("Hide", "hide")))
        `when`(metadataService.getBatchActionsForType("mnemonic"))
            .thenReturn(listOf(ActionInfo("Gobble", "gobble")))
        `when`(metadataService.getSegments("mnemonic")).thenReturn(listOf(SegmentInfo.default()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/metadata/dtoview/mnemonic")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
               {
                  "dtoDef": {
                    "mnemonic": "mnemonic",
                    "name": "m",
                    "pluralName": "m&m",
                    "addEnabled": true,
                    "updateEnabled": true,
                    "deleteEnabled": true,
                    "searchEnabled": true,
                    "columns": []
                  },
                  "actions": [
                    {
                      "label": "Eat",
                      "action": "eat",
                      "position": "END",
                      "order": 0
                    },
                    {
                      "label": "Hide",
                      "action": "hide",
                      "position": "END",
                      "order": 0
                    }
                  ],
                  "batchActions": [
                    {
                      "label": "Gobble",
                      "action": "gobble",
                      "position": "END",
                      "order": 0
                    }
                  ],
                  "segment": [
                    {
                      "name": "DEFAULT",
                      "description": ""
                    }
                  ]
               }
            """.trimIndent()
            )

        verify(metadataService, times(1)).getDtoDefinition("mnemonic")
        verify(metadataService, times(1)).getActionsForType("mnemonic")
        verify(metadataService, times(1)).getBatchActionsForType("mnemonic")
        verify(metadataService, times(1)).getSegments("mnemonic")
    }

    @Test
    fun getDtoDefinitionByMnemonic() {
        `when`(metadataService.getDtoDefinition("mnemonic")).thenReturn(DtoDef("mnemonic", "m", "m&m"))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/metadata/dtodef/mnemonic")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.mnemonic").isEqualTo("mnemonic")
            .jsonPath("$.pluralName").isEqualTo("m&m")

        verify(metadataService).getDtoDefinition("mnemonic")
    }

    @Test
    fun getActions() {

        `when`(metadataService.getActionsForType("project_Template"))
            .thenReturn(listOf(ActionInfo("Start", "start"), ActionInfo("Delete", "delete")))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/metadata/actions/project_Template")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                [
                  {
                    "label":"Start",
                    "action":"start",
                    "position":"END",
                    "order":0
                  },
                  {
                    "label":"Delete",
                    "action":"delete",
                    "position":"END",
                    "order":0
                  }
                ]
            """.trimIndent()

            )
        verify(metadataService, times(1)).getActionsForType("project_Template")
        verifyNoMoreInteractions(metadataService)
    }

    @Test
    fun getBatchActions() {

        `when`(metadataService.getBatchActionsForType("project_Template"))
            .thenReturn(listOf(ActionInfo("Remove All", "removeAll")))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/metadata/batchactions/project_Template")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                [
                  {
                    "label":"Remove All",
                    "action":"removeAll",
                    "position":"END",
                    "order":0
                  }
                ]
            """.trimIndent()

            )
        verify(metadataService, times(1)).getBatchActionsForType("project_Template")
        verifyNoMoreInteractions(metadataService)
    }

    @Test
    fun getRendererInfo() {

        `when`(metadataService.getRendererForColumn("project_Template", "manager"))
            .thenReturn(RendererInfo("GOLDFISH", mapOf("asImage" to "true")))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/metadata/renderer/project_Template/manager")
            .exchange()
            .expectStatus().isOk
            .expectBody()

            .json(
                """
                  {
                    "name":"GOLDFISH",
                    "attributes":    
                      {
                        "asImage": "true"
                      }

                  }
            """.trimIndent()

            )
        verify(metadataService, times(1)).getRendererForColumn("project_Template", "manager")
        verifyNoMoreInteractions(metadataService)
    }


    @Test
    fun getCustomFieldsForReference() {
        `when`(metadataService.getCustomFieldsForReference(Reference.PROJECT_TEMPLATE)).thenReturn(
            listOf(
                CustomFieldDef(
                    UUID.randomUUID(),
                    Reference.PROJECT_TEMPLATE,
                    "ImportantField",
                    "Very (!) Important field for project managers",
                    DataType.STRING
                )
            )
        )

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/metadata/customfields/project_Template")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$[0].field").isEqualTo("ImportantField")

        verify(metadataService).getCustomFieldsForReference(Reference.PROJECT_TEMPLATE)

    }

    @Test
    fun getSegments() {
        `when`(metadataService.getSegments("math")).thenReturn(
            listOf(
                SegmentInfo("Circle"),
                SegmentInfo("Line"),
                SegmentInfo("Spherical")
            )
        )

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .get().uri("/metadata/segments/math")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .json(
                """
                  [
                    {"name":"Circle","description":""},
                    {"name":"Line","description":""},
                    {"name":"Spherical","description":""}
                  ]
                """.trimIndent()
            )

        verify(metadataService).getSegments("math")

    }


    class MnemonicClass : HasIdentity {
        override fun identity(): Array<Any> {
            return arrayOf("I am ID")
        }

    }

}