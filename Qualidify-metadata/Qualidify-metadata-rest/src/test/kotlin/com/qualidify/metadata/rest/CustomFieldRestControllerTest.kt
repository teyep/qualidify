package com.qualidify.metadata.rest

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.RestQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.CustomFieldService
import com.qualidify.metadata.service.dto.CustomFieldDef
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.util.*


@WebFluxTest(controllers = [CustomFieldRestController::class])
@ExtendWith(SpringExtension::class)
@WithMockUser
internal class CustomFieldRestControllerTest() {

    @Autowired
    private lateinit var webClient: WebTestClient

    @MockBean
    lateinit var customFieldService: CustomFieldService

    @Test
    fun count() {
        val countQuery = CountQuery<CustomFieldDef>()
        val result = (7..22).random().toLong()

        `when`(customFieldService.countByQuery(countQuery))
            .thenReturn(CountResponse(result))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/customfield/count")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(RestQuery())
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.count").isEqualTo(result)

        verify(customFieldService).countByQuery(countQuery)
    }

    @Test
    fun find() {
        val searchQuery = SearchQuery<CustomFieldDef>()

        `when`(customFieldService.findByQuery(searchQuery))
            .thenReturn(listOf(customFieldDef(), customFieldDef(), customFieldDef()))

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/customfield/find")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(RestQuery())
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.length()").isEqualTo(3)

        verify(customFieldService).findByQuery(searchQuery)
    }

    @Test
    fun create() {
        val customField = customFieldDef()
        doNothing()
            .`when`(customFieldService).create(customField)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/customfield/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(customField)
            .exchange()
            .expectStatus().isAccepted
            .expectBody()
            .isEmpty

        verify(customFieldService).create(customField)
    }

    @Test
    fun create_IllegalArgumentException() {
        val customField = customFieldDef()
        doThrow(IllegalArgumentException("Missing reference"))
            .`when`(customFieldService).create(customField)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .post().uri("/customfield/create")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(customField)
            .exchange()
            .expectStatus().isBadRequest
            .expectBody(String::class.java)
            .isEqualTo("Missing reference")

        verify(customFieldService).create(customField)
    }

    @Test
    fun update() {
        val customFieldDto = customFieldDef()

        doNothing()
            .`when`(customFieldService).update(customFieldDto)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/customfield/update")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(customFieldDto)
            .exchange()
            .expectStatus().isAccepted
            .expectBody()
            .isEmpty


        verify(customFieldService).update(customFieldDto)
    }

    @Test
    fun delete() {
        val customField = customFieldDef()
        doNothing()
            .`when`(customFieldService).delete(customField)

        webClient
            .mutateWith(SecurityMockServerConfigurers.csrf())
            .put().uri("/customfield/delete")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(customField)
            .exchange()
            .expectStatus().isAccepted
            .expectBody()
            .isEmpty

        verify(customFieldService).delete(customField)
    }


    fun customFieldDef() =
        CustomFieldDef(
            id = UUID.randomUUID(),
            reference = Reference.USER,
            field = "EMAIL",
            description = "Email of the user",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )
}