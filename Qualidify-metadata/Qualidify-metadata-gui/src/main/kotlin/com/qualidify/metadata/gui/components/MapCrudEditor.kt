package com.qualidify.metadata.gui.components

import com.qualidify.metadata.gui.components.crud.InternalBinderCrudEditor
import com.qualidify.metadata.gui.components.crud.InternalCrudEditor
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.Metadata.Companion.getColumnsForEditing
import com.qualidify.metadata.service.ObjectConverter
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.ObjectMap
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.data.binder.Binder

class MapCrudEditor<T : Any>(
    private val beanType: Class<T>,
    private val formView: FormLayout = FormLayout(),
    private val formFieldComponentFactory: FormFieldComponentFactory,
) : InternalCrudEditor<T> {

    private var data: ObjectMap? = null
    private val binder = Binder(ObjectMap::class.java)
    private val delegatingEditor: InternalCrudEditor<ObjectMap>

    init {
        delegatingEditor = InternalBinderCrudEditor(binder, formView)
        addColumnsBasedOnMetaData()
    }

    override fun setItem(item: T?) {
        setItem(item, false)
    }

    override fun setItem(item: T?, validate: Boolean) {
        data = ObjectConverter.convertToMapWithCustomFields(item, beanType)
        delegatingEditor.setItem(data, validate)
    }

    override fun getItem(): T? {
        if (data == null) {
            return null;
        }
        return ObjectConverter.convertToObject(data!!, beanType)
    }

    override fun clear() {
        delegatingEditor.clear()
        data = null
    }

    override fun validate(): Boolean {
        return delegatingEditor.validate()
    }

    override fun writeItemChanges() {
        delegatingEditor.writeItemChanges()
    }

    override val view: Component?
        get() = delegatingEditor.view


    private fun addColumnsBasedOnMetaData() {
        columns
            .stream()
            .filter { !it.hidden }
            .forEach { columnDefinition: ColumnDef -> innerAddColumn(columnDefinition) }
    }

    private fun innerAddColumn(columnDefinition: ColumnDef) {
        formView.add(
            formFieldComponentFactory.createInputField(binder, columnDefinition.copy(useMapAccess = true))
        )

    }

    private val columns: List<ColumnDef>
        get() {
            return getColumnsForEditing(beanType)
        }

}


