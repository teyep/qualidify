package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.ItemLabelGenerator
import com.vaadin.flow.component.checkbox.CheckboxGroup
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.provider.ListDataProvider

/**
 * A builder for CheckboxGroup Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 * @param T the type of the field value
 */
class CheckboxGroupBuilder<D : Any, T>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, CheckboxGroup<T>, Set<T>>(definition, binder, enableLabel) {

    override val component = CheckboxGroup<T>()

    @Suppress("UNCHECKED_CAST")
    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {

        component.setId(definition.id ?: ("checkboxgroup" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        definition.getStringAttribute(AttributeConstants.HELPER_TEXT)?.let {
            component.helperText = it
        }

        definition.itemProvider?.run {
            val itemProvider = providerFactory?.getItemProvider(this)
            itemProvider?.run {
                component.setItems(this.toListDataProvider() as ListDataProvider<T>)
                component.setItemLabelGenerator(this as ItemLabelGenerator<T>)
            }
        }
        component.isReadOnly = !definition.editable
    }

}