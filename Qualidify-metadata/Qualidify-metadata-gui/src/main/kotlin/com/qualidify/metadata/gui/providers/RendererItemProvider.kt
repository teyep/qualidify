package com.qualidify.metadata.gui.providers

import com.qualidify.metadata.gui.renderers.RenderFactoryCollection
import org.springframework.stereotype.Component

/**
 * An Item Provider for the All Renderers
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
class RendererItemProvider(val renderFactoryCollection: RenderFactoryCollection) :
    RetrieveCollectionItemProvider<String>() {
    override fun retrieve(): Collection<String> {
        return renderFactoryCollection.getRenderNames()
    }
}