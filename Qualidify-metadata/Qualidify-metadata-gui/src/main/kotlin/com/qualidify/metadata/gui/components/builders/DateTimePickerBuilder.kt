package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.datetimepicker.DateTimePicker
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.validator.DateTimeRangeValidator
import java.time.Duration
import java.time.LocalDateTime

/**
 * A builder for DateTimePicker Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
class DateTimePickerBuilder<D : Any>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, DateTimePicker, LocalDateTime>(definition, binder, enableLabel) {

    override val component: DateTimePicker = DateTimePicker()

    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("datetimepicker" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        val max = definition.getStringAttribute(AttributeConstants.MAX)
        val min = definition.getStringAttribute(AttributeConstants.MIN)
        if (min != null || max != null) {
            min?.let { component.max = LocalDateTime.parse(it) }
            max?.let { component.min = LocalDateTime.parse(it) }
            addValidator(DateTimeRangeValidator("This field is not within range of ($min, $max)!",
                component.min,
                component.max))
        }

        definition.getStringAttribute(AttributeConstants.DATE_PLACEHOLDER)?.let {
            component.datePlaceholder = it
        }
        definition.getStringAttribute(AttributeConstants.TIME_PLACEHOLDER)?.let {
            component.timePlaceholder = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_OPEN)?.let {
            component.isAutoOpen = it
        }
        definition.getLongAttribute(AttributeConstants.STEP_VALUE)?.let {
            if (it != 0L) component.step = Duration.ofSeconds(it)
        }
        definition.getBooleanAttribute(AttributeConstants.WEEK_NUMBER_VISIBLE)?.let {
            component.isWeekNumbersVisible = it
        }
        definition.getStringAttribute(AttributeConstants.HELPER_TEXT)?.let {
            component.helperText = it
        }
        component.isReadOnly = !definition.editable

    }

}