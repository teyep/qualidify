package com.qualidify.metadata.gui.i18n

import com.vaadin.flow.i18n.I18NProvider
import java.util.*

/**
 * A Translation service that will translate message codes with help of the local
 * into the right message. It implements the Vaadin I18nProvider to let Vaadin also
 * take benefit of this behaviour
 *
 * @author Marcel Pot
 */
interface TranslationService : I18NProvider {
    /**
     * Get the translation for key with given locale.
     * <p>
     * Note! For usability and catching missing translations implementation
     * should never return a null, but an exception string e.g. '!{key}!'
     *
     * @param key
     *            translation key
     * @param locale
     *            locale to use
     * @param defaultMessage
     *            default message, if key not found
     * @param params
     *            parameters used in translation string
     * @return translation for key if found otherwise defaultmessage
     */
    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun getTranslation(key: String, locale: Locale, defaultMessage: String, vararg params: Any): String
}