package com.qualidify.metadata.gui.providers

import com.qualidify.metadata.DataType
import com.vaadin.flow.function.SerializablePredicate
import org.springframework.stereotype.Component

/**
 * An Item Provider for the Enum DataType
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
class DataTypeEnumItemProvider : EnumItemProvider<DataType>(DataType::class.java,
    SerializablePredicate {
        it.visible
    }) {


}