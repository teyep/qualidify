package com.qualidify.metadata.gui.providers

import java.io.Serializable

/**
 * Interface for executing actions. The actions are free format.
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface ActionExecutor<D : Serializable> {

    /**
     * Execute an action on/for a dto.
     * @param action a free format String
     * @param dto the object on which the action can be performed
     * @return an Outcome object indicating success or failure
     */
    fun executeAction(action: String, dto: D): Outcome<Any>

    /**
     * Check if an action is enabled. Default is true.
     * Override this method to create your own enabling strategy
     * @param action a free format String
     * @param dto the object on which the action can be performed
     * @return a boolean indicting if the action is enabled for this dto
     */
    fun isActionEnabled(action: String, dto: D) = true

    /**
     * Check if an action is visible. Default is true.
     * Override this method to create your own visibility strategy
     * @param action a free format String
     * @param dto the object on which the action can be performed
     * @return a boolean indicting if the action is visible for this dto
     */
    fun isActionVisible(action: String, dto: D) = true

    fun refresh() = Unit

}