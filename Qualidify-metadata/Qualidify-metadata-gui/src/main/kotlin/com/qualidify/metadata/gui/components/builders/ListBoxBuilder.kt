package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.listbox.ListBox
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.provider.ListDataProvider

/**
 * A builder for ListBox Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 * @param T the type of the fieldvalue
 */
class ListBoxBuilder<D : Any, T>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, ListBox<T>, T>(definition, binder, enableLabel) {

    override val component: ListBox<T> = ListBox()

    @Suppress("UNCHECKED_CAST")
    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("listbox" + "-" + definition.name.lowercase()))

        definition.itemProvider?.run {
            val itemProvider = providerFactory?.getItemProvider(this)
            itemProvider?.run {
                component.setItems(this.toListDataProvider() as ListDataProvider<T>)
            }
        }
        component.isReadOnly = !definition.editable
    }
}