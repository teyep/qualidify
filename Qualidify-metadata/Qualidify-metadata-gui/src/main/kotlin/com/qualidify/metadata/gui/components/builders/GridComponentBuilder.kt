package com.qualidify.metadata.gui.components.builders

import com.google.common.base.Preconditions
import com.qualidify.metadata.gui.components.factory.FieldGateFactory
import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.gui.components.InlineEditor
import com.qualidify.metadata.gui.components.InlineListEditor
import com.qualidify.metadata.gui.components.InlineMapEditor
import com.qualidify.metadata.gui.components.InlineSetEditor
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.ObjectCreator
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.data.binder.Binder
import java.io.Serializable

/**
 * A builder for InnerGrid Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class GridComponentBuilder<D : Any, B : Serializable>(
    val definition: ColumnDef,
    val binder: Binder<D> = Binder(),
) : ComponentFieldBuilder<InlineEditor<*>> {

    private var formFieldComponentFactory: FormFieldComponentFactory? = null
    private var objectCreator: ObjectCreator? = null

    @Suppress("UNCHECKED_CAST")
    override fun build(): InlineEditor<*> {
        Preconditions.checkNotNull(formFieldComponentFactory, "FormFieldComponentFactory cannot be empty")
        Preconditions.checkNotNull(objectCreator, "ObjectCreator cannot be empty")

        val typeOfColumn = Metadata.getClassTypeOfColumn<Any>(definition)

        val component = when (typeOfColumn) {
            Map::class.java -> {
                createInlineMapEditor(definition, binder)
            }
            List::class.java -> {
                createInlineListEditor(definition, binder)
            }
            else -> {
                createInlineSetEditor(definition, binder)
            }
        }


        return component
    }

    @Suppress("UNCHECKED_CAST")
    private fun createInlineListEditor(definition: ColumnDef, binder: Binder<D>): InlineEditor<List<B>> {
        val collectionClass = definition.getClassAttribute(AttributeConstants.BEAN_TYPE) as Class<B>

        val inlineListEditor = InlineListEditor<B>(
            collectionClass,
            this.formFieldComponentFactory!!,
            { objectCreator!!.create(collectionClass) },
        )

        inlineListEditor.setId(definition.id ?: ("grid" + "-" + definition.name.lowercase()))
        inlineListEditor.getElement().setAttribute("colspan", "2");

        binder.forField(inlineListEditor)
            .bind(valueProvider(), setter())

        return inlineListEditor
    }

    @Suppress("UNCHECKED_CAST")
    protected fun createInlineSetEditor(definition: ColumnDef, binder: Binder<D>): InlineEditor<Set<B>> {
        val collectionClass = definition.getClassAttribute(AttributeConstants.BEAN_TYPE) as Class<B>

        val inlineSetEditor = InlineSetEditor(
            collectionClass,
            this.formFieldComponentFactory!!,
            { objectCreator!!.create(collectionClass) },
        )

        inlineSetEditor.setId(definition.id ?: ("grid" + "-" + definition.name.lowercase()))
        inlineSetEditor.getElement().setAttribute("colspan", "2");

        binder.forField(inlineSetEditor)
            .bind(valueProvider(), setter())

        return inlineSetEditor
    }

    @Suppress("UNCHECKED_CAST")
    protected fun createInlineMapEditor(definition: ColumnDef, binder: Binder<D>): InlineEditor<Map<String, B>> {
        val collectionClass = definition.getClassAttribute(AttributeConstants.BEAN_TYPE) as Class<B>

        val inlineMapEditor = InlineMapEditor(
            collectionClass,
            this.formFieldComponentFactory!!,
        )
        inlineMapEditor.setId(definition.id ?: ("grid" + "-" + definition.name.lowercase()))
        inlineMapEditor.element.setAttribute("colspan", "2")

        binder.forField(inlineMapEditor)
            .bind(valueProvider(), setter())

        //Setter<D, Map<String, B>>)

        return inlineMapEditor
    }

    fun withFormFieldComponentFactory(formFieldComponentFactory: FormFieldComponentFactory): GridComponentBuilder<D, B> {
        this.formFieldComponentFactory = formFieldComponentFactory
        return this
    }

    fun withObjectCreator(objectCreator: ObjectCreator): GridComponentBuilder<D, B> {
        this.objectCreator = objectCreator
        return this
    }

    protected fun <D : Any, F> valueProvider(): (D) -> F? {
        return { bean: D -> FieldGateFactory.getValue(bean, definition.name) }
    }

    protected fun <D : Any, F> setter(): (D, F?) -> Unit {
        return { bean: D, value: F? -> FieldGateFactory.setValue<D, F>(bean, definition.name, value) }
    }

}
