package com.qualidify.metadata.gui.components

import com.qualidify.metadata.DataType
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.ObjectMap
import com.vaadin.flow.data.binder.Binder
import org.springframework.util.CollectionUtils
import java.io.Serializable

/**
 * An implementation of a Grid, where a [Binder] can be used
 * for key value pair items
 *
 * @author Marcel Pot
 * @since 1.0
 */
class InlineMapEditor<B : Serializable>(
    val beanType: Class<B>,
    formFieldComponentFactory: FormFieldComponentFactory,
) : InlineEditor<Map<String, B>>(formFieldComponentFactory) {


    companion object {
        const val KEY = "KEY"
        const val VALUE = "VALUE"
    }

    init {
        setupGrid()
        setUpGridListeners()
        add(grid)
    }


    private fun setupGrid() {
        addColumn(binder, ColumnDef("key", "key", DataType.STRING, useMapAccess = true)) { it.get(KEY) }
        addColumn(binder, ColumnDef("value", "value", DataType.STRING, useMapAccess = true)) { it.get(VALUE) }
    }

    private fun setUpGridListeners() {
        defaultGridValueChangeListener()
        defaultGridItemDoubleClickListener()
        defaultGridCloseListener()
    }

    //TODO :this function is at this moment only applicable for Map<String, Primitive>
    //      Update when needed
    private fun convertToObjectMap(entry: Map.Entry<String, B>): ObjectMap {
        val newMap = ObjectMap()
        newMap.put(KEY, entry.key)
        newMap.put(VALUE, entry.value)
        return newMap
    }

    //TODO :this function is at this moment only applicable for Map<String, Primitive>
    //      Update when needed
    @Suppress("UNCHECKED_CAST")
    private fun convertFromObjectMap(objectMap: ObjectMap): Pair<String, B> {
        return Pair(objectMap[KEY] as String, objectMap[VALUE] as B)
    }

    override fun setValue(values: Map<String, B>?) {
        this.values.clear()
        if (!CollectionUtils.isEmpty(values)) {
            values!!.entries
                .map { convertToObjectMap(it) }
                .forEach { this.values.add(it) }
        }
        grid.setItems(this.values)
    }

    override fun getValue(): Map<String, B> {
        return this.values
            .map { convertFromObjectMap(it) }
            .toMap()
    }


}