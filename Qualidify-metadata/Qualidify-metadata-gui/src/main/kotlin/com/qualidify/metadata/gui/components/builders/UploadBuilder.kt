package com.qualidify.metadata.gui.components.builders

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.gui.components.factory.FieldGateFactory
import com.qualidify.metadata.UploadFile
import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.upload.SucceededEvent
import com.vaadin.flow.component.upload.Upload
import com.vaadin.flow.component.upload.UploadI18N
import com.vaadin.flow.component.upload.UploadI18N.*
import com.vaadin.flow.component.upload.UploadI18N.Uploading.RemainingTime
import com.vaadin.flow.component.upload.receivers.MemoryBuffer
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer
import com.vaadin.flow.data.binder.Binder
import org.apache.commons.lang3.StringUtils
import java.util.*


/**
 * A builder for Upload Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
class UploadBuilder<D>(
    val definition: ColumnDef,
    val binder: Binder<D>? = null,
) : ComponentFieldBuilder<Upload> {
    var maxFileSize = Int.MAX_VALUE

    private val logger by LoggerDelegation()

    override fun build(): Upload {
        val maxFiles = definition.getIntegerAttributeOrDefault(AttributeConstants.MAX_FILES, 1)

        return if (maxFiles == 1) {
            buildSingleFileUpload()
        } else {
            buildMultiFileUpload(maxFiles)
        }
    }


    private fun buildSingleFileUpload(): Upload {
        val buffer = MemoryBuffer()
        val component = Upload(buffer)
        component.i18n = UploadBuilderI18N()

        component.setId(definition.id ?: ("upload" + "-" + definition.name.lowercase()))

        definition.label?.let {
            if (!StringUtils.isEmpty(it)) component.i18n.addFiles.setOne(it)
        }

        definition.getIntegerAttribute(AttributeConstants.MAX_FILE_SIZE)?.let {
            if (it > 0) {
                if (it <= maxFileSize) {
                    component.maxFileSize = it
                } else {
                    logger.warn("Configured maxFileSize on upload configuration is greater than the system configuration -> fall back to system configuration!")
                    component.maxFileSize = maxFileSize
                }
            }
        }
        component.maxFiles = 1

        definition.getStringArrayAttribute(AttributeConstants.ACCEPTED_FILE_TYPES).let {
            if (!it.isEmpty()) component.setAcceptedFileTypes(*it)
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_UPLOAD)?.let {
            component.isAutoUpload = it
        }
        definition.getBooleanAttribute(AttributeConstants.DROP_ALLOWED)?.let {
            component.isDropAllowed = it
        }
        if (binder != null) {
            component.addSucceededListener { event ->
                acceptFile(binder.bean, buffer, event)
            }
        }
        return component
    }

    private fun acceptFile(
        bean: D,
        buffer: MemoryBuffer,
        event: SucceededEvent,
    ) {
        val setter = FieldGateFactory.determineFieldSetter(bean!!::class.java, bean, definition.name)

        val type = Metadata.getClassTypeOfColumn<Any>(definition)
        if (type == UploadFile::class.java) {
            val uploadFile = UploadFile(event.fileName, buffer.inputStream.readAllBytes())
            setter.invoke(bean, uploadFile)
        } else {
            setter.invoke(bean, buffer.inputStream.readAllBytes())
        }
    }

    private fun buildMultiFileUpload(maxFiles: Int): Upload {
        val buffer = MultiFileMemoryBuffer()
        val component = Upload(buffer)
        component.i18n = UploadBuilderI18N()

        component.setId(definition.id ?: ("upload" + "-" + definition.name.lowercase()))

        definition.description.let {
            component.i18n.addFiles.setMany(it)
        }

        definition.getIntegerAttribute(AttributeConstants.MAX_FILE_SIZE)?.let {
            if (it > 0) {
                if (it <= maxFileSize) {
                    component.maxFileSize = it
                } else {
                    logger.warn("Configured maxFileSize on upload configuration is greater than the system configuration -> fall back to system configuration!")
                    component.maxFileSize = maxFileSize
                }
            }
        }
        component.maxFiles = maxFiles

        definition.getStringArrayAttribute(AttributeConstants.ACCEPTED_FILE_TYPES).let {
            if (!it.isEmpty()) component.setAcceptedFileTypes(*it)
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_UPLOAD)?.let {
            component.isAutoUpload = it
        }
        definition.getBooleanAttribute(AttributeConstants.DROP_ALLOWED)?.let {
            component.isDropAllowed = it
        }
        if (binder != null) {
            component.addSucceededListener { event ->
                acceptFiles(binder.bean, buffer, event)
            }
        }
        return component
    }


    private fun acceptFiles(
        bean: D,
        buffer: MultiFileMemoryBuffer,
        event: SucceededEvent,
    ) {
        val setter = FieldGateFactory.determineFieldSetter(bean!!::class.java, bean, definition.name)

        //only usefull for Arrays of Uploadfiles at this moment
        val uploadFiles = buffer.files.asSequence()
            .map { UploadFile(it, buffer.getInputStream(it).readAllBytes()) }
            .toHashSet()
            .toArray()

        setter.invoke(bean, uploadFiles)

    }


    fun withMaxFileSize(maxFileSize: Int): UploadBuilder<D> {
        this.maxFileSize = maxFileSize
        return this
    }

    class UploadBuilderI18N : UploadI18N() {
        init {
            dropFiles = DropFiles()
                .setOne("Drop file here")
                .setMany("Drop files here")
            addFiles = AddFiles()
                .setOne("Upload File...")
                .setMany("Upload Files...")
            error = Error()
                .setTooManyFiles("Too Many Files.")
                .setFileIsTooBig("File is Too Big.")
                .setIncorrectFileType("Incorrect File Type.")
            uploading = Uploading()
                .setStatus(Uploading.Status()
                    .setConnecting("Connecting...")
                    .setStalled("Stalled")
                    .setProcessing("Processing File...")
                    .setHeld("Queued"))
                .setRemainingTime(RemainingTime()
                    .setPrefix("remaining time: ")
                    .setUnknown("unknown remaining time"))
                .setError(Uploading.Error()
                    .setServerUnavailable("Upload failed, please try again later")
                    .setUnexpectedServerError("Upload failed due to server error")
                    .setForbidden("Upload forbidden"))
            units = Units()
                .setSize(listOf("B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"))
        }
    }
}