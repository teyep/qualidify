package com.qualidify.metadata.gui.providers

import java.util.concurrent.Future

/**
 * A Result interface, containing the possible result, but also the possible error
 * for a command send to the commandgateway
 *
 * @author Marcel Pot
 */
interface Outcome<T> {
    /**
     * Get the result of the command
     *
     * @return the result of the command
     */
    val result: T

    /**
     * Get the error if the command generated one
     *
     * @return the error (In case of no error, this will be an empty string)
     */
    val error: String

    /**
     * Indicator if the command has succeeded
     *
     * @return a success boolean
     */
    val isSuccess: Boolean

    /**
     * Indicator if the command has been processed
     *
     * @return a boolean indicating if the process is finished
     */
    val isDone: Boolean

    companion object {
        /**
         * Empty success Result Factory. Can be used in cases
         *  *  No result object is excepted.
         *  *  Only Send command and do not wait for return value.
         *
         * @return a successful Result
         */
        @JvmStatic
        fun success(): Outcome<Any> {
            return object : Outcome<Any> {
                override val result: String
                    get() = "OK"
                override val error: String
                    get() = ""
                override val isSuccess: Boolean
                    get() = true
                override val isDone: Boolean
                    get() = true
            }
        }

        /**
         * Value based success Result Factory.
         * (Standard behaviour)
         * @param value the success object
         * @param <T> the type of the success object
         * @return a successful Result
         */
        @JvmStatic
        fun <T> success(value: T): Outcome<T> {
            return object : Outcome<T> {
                override val result: T
                    get() = value
                override val error: String
                    get() = ""
                override val isSuccess: Boolean
                    get() = true
                override val isDone: Boolean
                    get() = true
            }
        }

        /**
         * Empty failure Result Factory. Can be used in cases
         *  *  No result object is excepted.
         *  *  Only Send command and do not wait for return value.
         *
         * @return a failing Result
         */
        @JvmStatic
        fun fail(): Outcome<Any> {
            return object : Outcome<Any> {
                override val result: Any
                    get() {
                        throw OutcomeException(error)
                    }
                override val error: String
                    get() = "Unknown Command failure result."
                override val isSuccess: Boolean
                    get() = false
                override val isDone: Boolean
                    get() = true
            }
        }

        /**
         * Exception based success Result Factory.
         * (Standard behaviour in case of error)
         * @param throwable the throwable
         * @param <T> the expected type of result
         * @return a failing Result
         */
        @JvmStatic
        fun <T> fail(throwable: Throwable): Outcome<T> {
            return object : Outcome<T> {
                override val result: T
                    get() {
                        throw OutcomeException("Result error ", throwable)
                    }
                override val error: String
                    get() = "" + throwable.message
                override val isSuccess: Boolean
                    get() = false
                override val isDone: Boolean
                    get() = true
            }
        }

        @JvmStatic
        fun <T> forbidden(): Outcome<T> {
            return object : Outcome<T> {
                override val result: T
                    get() {
                        throw OutcomeException(error)
                    }
                override val error: String
                    get() = "Command Action is forbidden"
                override val isSuccess: Boolean
                    get() = false
                override val isDone: Boolean
                    get() = true
            }
        }

        @JvmStatic
        fun <T> unknown(action: String): Outcome<T> {
            return object : Outcome<T> {
                override val result: T
                    get() {
                        throw OutcomeException(error)
                    }
                override val error: String
                    get() = "Action $action does not exist"
                override val isSuccess: Boolean
                    get() = false
                override val isDone: Boolean
                    get() = true
            }
        }

        /**
         * A Result factory that will wait for a future to return the right result
         *
         * @param future The future to wait on
         * @return the Result (success of failing, dependent on the future)
         */
        @JvmStatic
        fun <T> of(future: Future<T>): Outcome<T> {
            return object : Outcome<T> {
                override val result: T
                    get() {
                        try {
                            return future.get()
                        } catch (exception: Exception) {
                            throw OutcomeException(
                                "Result error ",
                                exception
                            );
                        }
                    }
                override val error: String
                    get() {
                        try {
                            future.get()
                            return ""
                        } catch (exception: Exception) {
                            return "" + exception;
                        }
                    }
                override val isSuccess: Boolean
                    get() {
                        try {
                            future.get()
                            return true
                        } catch (exception: Exception) {
                            return false
                        }
                    }
                override val isDone: Boolean
                    get() = future.isDone
            }
        }

        /**
         * A simple OK constant Result
         */
        @JvmStatic
        val OK = success()

        /**
         * A simple Error constant Result
         */
        @JvmStatic
        val ERROR = fail()

        /**
         * A Simple Forbidden constant Result
         */
        @JvmStatic
        val FORBIDDEN = forbidden<Any>()

    }
}

/**
 * A Simple Result without taking care of the classType of the result
 *
 * @author Marcel Pot
 */
class SimpleOutcome : Outcome<Any?> {

    private val success: Boolean
    private val exception: Exception?
    val value: Any?


    constructor(exception: Exception) {
        this.exception = exception
        this.success = false
        this.value = null
    }

    constructor(result: Any) {
        this.exception = null
        this.success = true
        this.value = result
    }

    override val result: Any?
        get() {
            if (!success) {
                throw OutcomeException(exception!!.message, exception)
            }
            return value
        }
    override val error: String
        get() = "" + exception!!.message
    override val isSuccess: Boolean
        get() = success
    override val isDone: Boolean
        get() = true

    companion object {
        /**
         * A simple OK constant Result
         */
        @JvmStatic
        val OK = SimpleOutcome(Object())

        /**
         * A Simple ERROR constant Result
         */
        @JvmStatic
        val ERROR = SimpleOutcome(OutcomeException("Unknown Command failure result."))

        /**
         * A Result factory that will wait for a future to return the right result
         *
         * @param future The future to wait on
         * @return the Result (success of failing, dependent on the future)
         */
        @JvmStatic
        fun of(future: Future<Any>): SimpleOutcome {
            return try {
                val value = future.get()
                SimpleOutcome(value)
            } catch (exception: Exception) {
                SimpleOutcome(exception)
            }
        }
    }
}

/**
 * An Exception when the [Outcome] of the [CudService] isn't what is expected.
 * It can wrap an original exception, but also create a new one
 *
 * @author Marcel Pot
 */
class OutcomeException : RuntimeException {
    constructor(message: String?, cause: Throwable?) : super(message, cause) {}
    constructor(message: String?) : super(message) {}
}