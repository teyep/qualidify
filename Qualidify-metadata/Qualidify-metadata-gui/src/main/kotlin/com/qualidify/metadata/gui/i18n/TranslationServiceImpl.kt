package com.qualidify.metadata.gui.i18n

import com.qualidify.core.log.LoggerDelegation
import org.springframework.context.MessageSource
import org.springframework.context.NoSuchMessageException
import java.util.*

/**
 * An implementation of the Translation service that will translate message codes with
 * help of the local into the right message. It implements the Vaadin I18nProvider to
 * let Vaadin also take benefit of this behaviour
 *
 * @author Marcel Pot
 */
class TranslationServiceImpl(private val messageSource: MessageSource) :
    TranslationService {

    private val logger by LoggerDelegation()

    override fun getProvidedLocales(): List<Locale> {
        return LOCALES
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun getTranslation(key: String, locale: Locale, vararg params: Any?): String {
        try {
            return messageSource.getMessage(key, params, locale)
        } catch (e: NoSuchMessageException) {
            logger.warn("The key $key cannot be found in the message source for locale $locale.", e);
            return messageSource.getMessage(key, params, key, locale)
        }
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun getTranslation(key: String, locale: Locale, defaultMessage: String, vararg params: Any): String {
        return messageSource.getMessage(key, params, defaultMessage, locale)
    }

    companion object {
        /**
         * Define all available locals in the system.
         * NOTE: Can we make this dependend on the resource bundle?
         */
        private val LOCALES = Arrays.asList(Locale.ENGLISH, Locale("nl_NL"))
    }
}