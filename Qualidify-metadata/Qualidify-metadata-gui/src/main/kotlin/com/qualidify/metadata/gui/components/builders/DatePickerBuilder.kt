package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.datepicker.DatePicker
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.validator.DateRangeValidator
import java.time.LocalDate

/**
 * A builder for DatePicker Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
class DatePickerBuilder<D : Any>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, DatePicker, LocalDate>(definition, binder, enableLabel) {

    override val component: DatePicker = DatePicker()

    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("datepicker" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        definition.getStringAttribute(AttributeConstants.MAX)?.let {
            component.max = LocalDate.parse(it)
        }
        definition.getStringAttribute(AttributeConstants.MIN)?.let {
            component.min = LocalDate.parse(it)
        }
        val max = definition.getStringAttribute(AttributeConstants.MAX)
        val min = definition.getStringAttribute(AttributeConstants.MIN)
        if (min != null || max != null) {
            min?.let { component.max = LocalDate.parse(it) }
            max?.let { component.min = LocalDate.parse(it) }
            addValidator(DateRangeValidator("This field is not within range of ($min, $max)!",
                component.min,
                component.max))
        }

        definition.getStringAttribute(AttributeConstants.PLACEHOLDER)?.let {
            component.placeholder = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_OPEN)?.let {
            component.isAutoOpen = it
        }
        definition.getBooleanAttribute(AttributeConstants.OPEN)?.let {
            component.isOpened = it
        }
        definition.getBooleanAttribute(AttributeConstants.CLEAR_BUTTON_VISIBILE)?.let {
            component.isClearButtonVisible = it
        }
        definition.getBooleanAttribute(AttributeConstants.WEEK_NUMBER_VISIBLE)?.let {
            component.isWeekNumbersVisible = it
        }
        definition.getStringAttribute(AttributeConstants.HELPER_TEXT)?.let {
            component.helperText = it
        }
        component.isReadOnly = !definition.editable

    }

}