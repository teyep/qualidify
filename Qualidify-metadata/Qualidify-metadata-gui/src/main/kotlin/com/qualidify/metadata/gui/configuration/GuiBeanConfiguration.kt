package com.qualidify.metadata.gui.configuration

import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.gui.components.factory.ProviderFactory
import com.qualidify.metadata.gui.components.factory.ProviderFactoryImplWithServiceLocator
import com.qualidify.metadata.gui.i18n.TranslationService
import com.qualidify.metadata.gui.i18n.TranslationServiceImpl
import com.qualidify.metadata.service.ObjectCreator
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.context.ApplicationContext
import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.*


/**
 * All bean configuration for Graphical User interface for metadata purposes are defined
 * in this configuration class
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Configuration
open class GuiBeanConfiguration(
    private val ctx: ApplicationContext
) {

    /**
     * Create a translation service bean for translation based on a messageSource
     * @param messageSource the source of the messages
     * @return a translation service
     */
    @ConditionalOnBean(MessageSource::class)
    @Bean
    open fun translationService(@Qualifier("i18nMessageSource") messageSource: MessageSource): TranslationService {
        return TranslationServiceImpl(messageSource)
    }

    /**
     * A lookup service for specific dataproviders, itemproviders, and labelgenerators registered
     * as bean in the spring context/
     * @return the providerFactory
     */
    @Bean
    open fun providerFactory(): ProviderFactory {
        return ProviderFactoryImplWithServiceLocator()
    }

    /**
     * A factory for form-fields (input fields)
     * @param objectCreator a creator of objects based on their [CreationStrategyPattern]
     * @return the factory
     */
    @Bean
    open fun formFieldComponentFactory(
        objectCreator: ObjectCreator,
        @Value("\${qualidify.frontend.upload.maxfilesize:10240}") maxFileSize: Int,
    ): FormFieldComponentFactory {
        return FormFieldComponentFactory(providerFactory(), objectCreator, maxFileSize)
    }


}

