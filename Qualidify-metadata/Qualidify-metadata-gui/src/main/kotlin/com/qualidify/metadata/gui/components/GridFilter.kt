package com.qualidify.metadata.gui.components

import com.vaadin.flow.data.provider.SortDirection
import java.io.Serializable

/**
 * Implementation of a filter by keeping track on which fields should be filtered
 * and which direction a column should be filtered
 *
 * @author Marcel Pot
 * @since 1.0 (QUAL-422)
 */
data class GridFilter
    (
    val constraints: MutableMap<String, String?> = linkedMapOf(),
    val sortOrders: MutableMap<String, SortDirection?> = linkedMapOf()
) : Serializable