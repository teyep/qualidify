package com.qualidify.metadata.gui.providers

import com.qualidify.metadata.gui.components.GridFilter
import com.vaadin.flow.data.provider.DataProvider
import java.io.Serializable

/**
 * A combination of CommandCenter and DataProvider interface to take care of all data-related
 * actions from and to the backend.
 *
 * NB: DataConnectors implementing this interface need to be annotated with @VaadinSessionScope
 * to work in a multibrowser environment
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface DataConnector<T : Serializable> : DataProvider<T, GridFilter>,
    CudService<T> {
}

