package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.textfield.IntegerField
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.validator.IntegerRangeValidator

/**
 * A builder for Integer Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
class IntegerFieldBuilder<D : Any>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, IntegerField, Int>(definition, binder, enableLabel) {

    override val component: IntegerField = IntegerField()

    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("integerfield" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        definition.getStringAttribute(AttributeConstants.TITLE)?.let {
            component.title = it
        }
        definition.getBooleanAttribute(AttributeConstants.IS_REQUIRED)?.let {
            component.isRequiredIndicatorVisible = it
            if (it) {
                addRequiredFieldValidators()
            }
        }

        val max = definition.getIntegerAttribute(AttributeConstants.MAX)
        val min = definition.getIntegerAttribute(AttributeConstants.MIN)
        if (min != null || max != null) {
            min?.let { component.max = it }
            max?.let { component.min = it }
            addValidator(IntegerRangeValidator("This field is not within range of ($min, $max)!", min, max))
        }
        definition.getStringAttribute(AttributeConstants.PLACEHOLDER)?.let {
            component.placeholder = it
        }
        definition.getBooleanAttribute(AttributeConstants.USE_DEFAULT_VALUE)?.let {
            val defaultValue = definition.getIntegerAttribute(AttributeConstants.DEFAULT_VALUE)
            component.value = defaultValue
        }
        definition.getIntegerAttribute(AttributeConstants.STEP_VALUE)?.let {
            component.step = it
        }
        definition.getStringAttribute(AttributeConstants.HELPER_TEXT)?.let {
            component.helperText = it
        }
        component.isReadOnly = !definition.editable

    }
}