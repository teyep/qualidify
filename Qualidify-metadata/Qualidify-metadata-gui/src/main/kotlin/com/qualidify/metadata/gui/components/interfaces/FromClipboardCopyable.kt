package com.qualidify.metadata.gui.components.interfaces

import com.vaadin.flow.component.ClientCallable
import com.vaadin.flow.component.HasElement
import com.vaadin.flow.component.UI

/**
 * Vaadin interface for copying from clipboard to a Vaadin element
 *
 * @author Marcel Pot
 */
interface FromClipboardCopyable : HasElement {

    /**
     * Execute the copy mechanism
     *
     * Default implementation will copy the clipboard and proceed with the content
     * to the [FromClipboardCopyable.doPasteFromClipboard] method
     */
    fun doCopyFromClipboard() {
        UI.getCurrent()
            .page
            .executeJs(
                "navigator.clipboard.readText().then(clipText => $0.\$server.doPasteFromClipboard(clipText)) ",
                element
            )
    }

    /**
     * Hook to add the content of the clipboard to the requested place
     * @param clipText the text from the clipboard
     */
    @ClientCallable
    fun doPasteFromClipboard(clipText: String)
}