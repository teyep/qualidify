package com.qualidify.metadata.gui.renderers

import com.qualidify.metadata.gui.components.factory.FieldGateFactory
import com.vaadin.flow.component.html.Image
import com.vaadin.flow.data.renderer.ComponentRenderer
import com.vaadin.flow.data.renderer.Renderer
import com.vaadin.flow.server.InputStreamFactory
import com.vaadin.flow.server.StreamResource
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.Serializable


@Component
class ImageRendererFactory<T : Serializable> : RendererFactory<T, Any> {

    override fun create(item: T, field: String, attributes: Map<String, Any?>): Renderer<T> {
        //val attributes = Metadata.getRendererAttributesForColumn(item::class.java, field)
        val content = FieldGateFactory.getValue<T, ByteArray>(item, field)
        val streamResource =
            StreamResource("test", InputStreamFactory { return@InputStreamFactory ByteArrayInputStream(content) })

        return ImageRenderer(attributes) { Image(streamResource, " alt") }
    }

    override fun create(attributes: Map<String, Any?>, provider: (T) -> Any): Renderer<T> {
        return ImageRenderer(attributes) {
            val byteArray = provider.invoke(it)

            val img = if (byteArray is ByteArray) {
                Image(StreamResource("test", InputStreamFactory { ByteArrayInputStream(byteArray) }), "alt")
            } else {
                emptyImage()
            }
            return@ImageRenderer img
        }
    }

    private fun emptyImage(): Image {
        val img = Image();
        img.isVisible = false
        return img;
    }


    class ImageRenderer<T>(
        val attributes: Map<String, Any?>,
        val imageGenerator: (T) -> Image,
    ) : ComponentRenderer<Image, T>() {

        override fun createComponent(item: T): Image {
            val image: Image = imageGenerator.invoke(item)
            attributes["height"]?.let { image.height = it as String }
            attributes["width"]?.let { image.width = it as String }
            return image
        }
    }


}


