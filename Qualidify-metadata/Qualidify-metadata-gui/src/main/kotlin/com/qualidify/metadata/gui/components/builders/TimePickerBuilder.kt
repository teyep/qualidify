package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.timepicker.TimePicker
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.validator.RangeValidator
import java.time.LocalTime

/**
 * A builder for TimePicker Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
class TimePickerBuilder<D : Any>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, TimePicker, LocalTime>(definition, binder, enableLabel) {

    override val component: TimePicker = TimePicker()

    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("timepicker" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        val max = definition.getStringAttribute(AttributeConstants.MAX)
        val min = definition.getStringAttribute(AttributeConstants.MIN)
        if (min != null || max != null) {
            min?.let { component.max = LocalTime.parse(it) }
            max?.let { component.min = LocalTime.parse(it) }
            addValidator(TimeRangeValidator("This field is not within range of ($min, $max)!",
                component.min,
                component.max))
        }

        definition.getBooleanAttribute(AttributeConstants.IS_REQUIRED)?.let {
            component.isRequiredIndicatorVisible = it
            if (it) {
                addRequiredFieldValidators()
            }
        }

        definition.getStringAttribute(AttributeConstants.MAX)?.let {
            component.max = LocalTime.parse(it)
        }
        definition.getStringAttribute(AttributeConstants.MIN)?.let {
            component.min = LocalTime.parse(it)
        }
        definition.getStringAttribute(AttributeConstants.PLACEHOLDER)?.let {
            component.placeholder = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_OPEN)?.let {
            component.isAutoOpen = it
        }
        definition.getBooleanAttribute(AttributeConstants.CLEAR_BUTTON_VISIBILE)?.let {
            component.isClearButtonVisible = it
        }
        definition.getStringAttribute(AttributeConstants.HELPER_TEXT)?.let {
            component.helperText = it
        }
        component.isReadOnly = !definition.editable
    }

    //Check: does this work for times in two days? (where 0:00 is inbetween)
    internal class TimeRangeValidator(
        errorMessage: String?,
        minValue: LocalTime?,
        maxValue: LocalTime?,
    ) :
        RangeValidator<LocalTime>(errorMessage, Comparator.naturalOrder(), minValue, maxValue)
}
