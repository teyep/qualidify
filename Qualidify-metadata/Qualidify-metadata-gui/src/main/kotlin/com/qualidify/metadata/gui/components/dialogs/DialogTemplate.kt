package com.qualidify.metadata.gui.components.dialogs

/**
 * Dialog template extending [AbstractDialogTemplate], with some extra functionality as identity usage
 *
 * @author Marcel Pot
 * @since 1.0
 */
open class DialogTemplate(val identity: String) : AbstractDialogTemplate() {
    override fun initialize() {
        setId("dialog-${identity}")
        super.initialize()
    }


}