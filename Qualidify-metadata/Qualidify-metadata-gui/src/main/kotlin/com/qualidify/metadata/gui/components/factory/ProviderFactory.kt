package com.qualidify.metadata.gui.components.factory

import com.qualidify.core.servicelocator.ServiceLocator
import com.qualidify.metadata.gui.providers.ItemProvider
import com.vaadin.flow.data.renderer.Renderer

interface ProviderFactory {
    /**
     * Get The ListDataProvider (an inmemory DataProvider) based on the name
     *
     * @param beanName the beanname
     * @return the ListDataProvider
     */
    @Suppress("UNCHECKED_CAST")
    fun getItemProvider(beanName: String): ItemProvider<*, *>?

    /**
     * Get The Renderer based on the beanname
     *
     * @param beanName the beanname
     * @return The Renderer
     * */
    fun getRenderer(beanName: String): Renderer<*>?
}

/**
 * A Factory acting as a lookup alias for the ApplicationContext by using the Service
 * Locator. This Factory will provide the beans which can act as data providers for
 * Vaadin multi select components
 *
 * @author Marcel Pot
 * @since 1.0
 */
class ProviderFactoryImplWithServiceLocator : ProviderFactory {

    @Suppress("UNCHECKED_CAST")
    override fun getItemProvider(beanName: String): ItemProvider<*, *>? {
        return ServiceLocator.getServiceByName(beanName, ItemProvider::class.java)
    }

    override fun getRenderer(beanName: String): Renderer<*>? {
        return ServiceLocator.getServiceByName(beanName, Renderer::class.java)
    }
}