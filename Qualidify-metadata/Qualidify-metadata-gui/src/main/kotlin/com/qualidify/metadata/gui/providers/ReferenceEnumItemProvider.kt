package com.qualidify.metadata.gui.providers

import com.qualidify.metadata.Reference
import org.springframework.stereotype.Component

/**
 * An Item Provider for the Enum Reference
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
class ReferenceEnumItemProvider
    : EnumItemProvider<Reference>(com.qualidify.metadata.Reference::class.java)