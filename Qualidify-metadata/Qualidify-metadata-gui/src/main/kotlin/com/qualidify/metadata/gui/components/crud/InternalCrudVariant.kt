package com.qualidify.metadata.gui.components.crud


enum class InternalCrudVariant(
    val variantName: String
) {
    NO_BORDER("no-border");
}
