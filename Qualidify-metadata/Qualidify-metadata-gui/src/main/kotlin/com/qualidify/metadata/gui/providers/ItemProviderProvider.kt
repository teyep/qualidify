package com.qualidify.metadata.gui.providers

import com.qualidify.core.servicelocator.ServiceLocator
import org.springframework.stereotype.Component

/**
 * An Item Provider for the All itemproviders
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
class ItemProviderProvider :
    RetrieveCollectionItemProvider<String>() {

    override fun retrieve(): Collection<String> {
        return ServiceLocator.getServicesByType(ItemProvider::class.java).keys
    }
}