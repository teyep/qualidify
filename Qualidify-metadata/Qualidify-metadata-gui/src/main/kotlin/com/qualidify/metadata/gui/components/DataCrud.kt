package com.qualidify.metadata.gui.components

import com.qualidify.metadata.gui.components.crud.InternalCrud
import com.qualidify.metadata.gui.components.crud.InternalCrudEditor
import com.qualidify.metadata.gui.providers.DataConnector
import java.io.Serializable

class DataCrud<T : Serializable>(beanType: Class<T>, grid: DataGrid<T>, editor: InternalCrudEditor<T>) :
    InternalCrud<T>(
        beanType, grid, editor
    ) {

    init {
        setSizeFull()
        setId("datacrud-" + beanType.simpleName.lowercase())
    }

    fun setFilterConstraint(key: String, value: String) {
        (this.getGrid() as DataGrid<T>).setFilterConstraint(key, value)
    }

    fun removeFilterConstraint(key: String) {
        (this.getGrid() as DataGrid<T>).removeFilterConstraint(key)
    }

    fun getDataConnector(): DataConnector<T>{
        return (this.getGrid() as DataGrid<T>).getDataConnector()
    }
}