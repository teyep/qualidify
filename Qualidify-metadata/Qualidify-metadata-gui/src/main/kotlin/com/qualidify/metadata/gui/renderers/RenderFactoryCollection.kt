package com.qualidify.metadata.gui.renderers


import com.qualidify.metadata.gui.components.factory.FieldGateFactory
import com.qualidify.metadata.service.Metadata
import com.vaadin.flow.data.renderer.Renderer
import org.springframework.stereotype.Service
import java.io.Serializable

/**
 * A factory for Renderers
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class RenderFactoryCollection(val renderers: Map<String, RendererFactory<*, *>>) {

    companion object {
        const val factory: String = "Factory"
    }

    /**
     * Get a list of all active Renders available
     * @return a list of all available Renderers
     */
    fun getRenderNames(): List<String> {
        return renderers.keys.asSequence()
            .map { getRendererFromFactoryName(it) }
            .toList()
    }

    /**
     * Get a Renderer object
     * @param name, the name of the Renderer
     * @param attributes a map with render properties
     * @param provider the valueProvider
     * @return the Renderer
     */
    @Suppress("UNCHECKED_CAST")
    fun <T : Serializable, F> getRenderer(
        name: String,
        attributes: Map<String, Any?> = emptyMap(),
        provider: (T) -> F,
    ): Renderer<T> {
        val factoryName = getFactoryNameFromRenderer(name)
        val rendererFactory =
            renderers[factoryName] ?: throw RendererNotFoundException("The configured Renderer $name cannot be found!")
        return (rendererFactory as RendererFactory<T, F>).create(attributes = attributes, provider = provider)
    }

    /**
     * Get a Renderer object
     * @param name, the name of the Renderer
     * @param beanType the beanType
     * @param field the name of the field
     * @param attributes a map with render properties
     * @return the Renderer
     */
    fun <T : Serializable> getRenderer(
        name: String,
        beanType: Class<T>,
        field: String,
        attributes: Map<String, Any?> = emptyMap(),
    ): Renderer<T> {
        return getRenderer(
            name = name,
            attributes = attributes,
            provider = FieldGateFactory.getFieldReader(beanType, field)
        )
    }

    /**
     * Get a Renderer object
     * @param annotation, the Renderer annotation
     * @param beanType the beanType
     * @param field the name of the field
     * @return the Renderer
     */
    fun <T : Serializable> getRenderer(annotation: Annotation, beanType: Class<T>, field: String): Renderer<T> {
        val rendererName = annotation.annotationClass.simpleName
        val attributes = Metadata.getRendererAttributesForColumn(beanType, field).orEmpty()
        return getRenderer(rendererName!!, beanType, field, attributes)

    }

    private fun getRendererFromFactoryName(factoryName: String) = factoryName.replace(factory, "")
    private fun getFactoryNameFromRenderer(rendererName: String): String {
        val chars = (rendererName + factory).toCharArray();
        chars[0] = Character.toLowerCase(chars[0]);
        return String(chars);
    }
}

/**
 * An exception indicating a problem with finding a Renderer
 *
 * @author Marcel Pot
 * @since 1.0
 */
class RendererNotFoundException(message: String, cause: Throwable? = null) : RuntimeException(message, cause) {
}