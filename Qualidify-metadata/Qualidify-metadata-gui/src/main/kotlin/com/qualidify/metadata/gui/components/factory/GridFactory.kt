package com.qualidify.metadata.gui.components.factory

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.util.html.toHtmlCase
import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.Position
import com.qualidify.metadata.gui.components.DataGrid
import com.qualidify.metadata.gui.components.MapCrudViewer
import com.qualidify.metadata.gui.components.crud.InternalCrud
import com.qualidify.metadata.gui.providers.ActionExecutor
import com.qualidify.metadata.gui.renderers.RenderFactoryCollection
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.dto.ActionInfo
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.DtoDef
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.ItemLabelGenerator
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.grid.Grid.Column
import com.vaadin.flow.component.grid.HeaderRow
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.H3
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.provider.ListDataProvider
import com.vaadin.flow.data.renderer.LitRenderer
import com.vaadin.flow.data.value.ValueChangeMode
import org.springframework.stereotype.Service
import java.io.Serializable
import java.util.function.Consumer

/**
 * Factory for creating DataGrids
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class GridFactory(
    val providerFactory: ProviderFactory,
    val renderFactoryCollection: RenderFactoryCollection,
    val formFieldComponentFactory: FormFieldComponentFactory
) {

    private val logger by LoggerDelegation()

    /**
     * Create a Grid for a specific Bean
     * @param beanType the class of the bean
     * @param T the type of the bean
     * @param filterEnabled indicates a filterbar will be added
     * @return the Grid for the bean
     */
    fun <T : Serializable> getGrid(beanType: Class<T>, filterEnabled: Boolean): DataGrid<T> {
        val dtoDefinition = Metadata.getDtoDefinition(beanType)
        return getGrid(beanType, dtoDefinition, filterEnabled)
    }

    /**
     * Create a Grid for a specific Bean
     * @param beanType the class of the bean
     * @param T the type of the bean
     * @return the Grid for the bean
     */
    fun <T : Serializable> getGrid(beanType: Class<T>): DataGrid<T> {
        val dtoDefinition = Metadata.getDtoDefinition(beanType)
        return getGrid(beanType, dtoDefinition, dtoDefinition.searchEnabled)
    }

    private fun <T : Serializable> getGrid(
        beanType: Class<T>,
        dtoDefinition: DtoDef,
        filterEnabled: Boolean,
    ): DataGrid<T> {
        val dataGrid = GridBuilder(beanType, filterEnabled)
            .addColumnsBasedOnMetaData()
            .setEditableColumnFlag(dtoDefinition.updateEnabled)
            .build()
        dataGrid.addSortListener { it }
        return dataGrid
    }

    /**
     * Builder for Grid with customizations
     *
     * @param beanType the type of the bean
     * @param T the bean type
     *
     * @author Marcel Pot
     * @since 1.0
     */
    inner class GridBuilder<T : Serializable>(private val beanType: Class<T>, private val filterEnabled: Boolean) {

        private val grid: DataGrid<T>
        private val filterRow: HeaderRow?
        private var isUpdatable = false

        init {
            grid = DataGrid(beanType, false)
            grid.removeAllColumns()
            filterRow = if (filterEnabled) grid.appendHeaderRow() else null
        }


        /**
         * Add a column to the grid
         *
         * @param columnDefinition a columnDefinition
         * @return the builder
         */
        fun addColumnByDefinition(columnDefinition: ColumnDef): GridBuilder<T> {
            innerAddColumn(columnDefinition)
            return this
        }

        /**
         * Add a columns to the grid
         *
         * @param columnDefinitions a collection of columnDefinitions
         * @return the builder
         */
        fun addColumnsByDefinition(columnDefinitions: Collection<ColumnDef>): GridBuilder<T> {
            columnDefinitions.forEach(Consumer { columnDefinition: ColumnDef ->
                innerAddColumn(
                    columnDefinition
                )
            })
            return this
        }

        /**
         * Add columns to the grid based on the MetaDataConfiguration
         *
         * @return the builder
         */
        fun addColumnsBasedOnMetaData(): GridBuilder<T> {
            getGridButtonsForPosition(Position.BEGIN)
                .forEach { addAction(it) }

            Metadata.getColumnsForViewing(beanType)
                .forEach { columnDefinition: ColumnDef ->
                    innerAddColumn(
                        columnDefinition
                    )
                }

            getGridButtonsForPosition(Position.END)
                .forEach { addAction(it) }

            return this
        }

        /**
         * Add the edit button to the grid
         *
         * @return the builder
         */
        fun addEditColumn(): GridBuilder<T> {
            isUpdatable = true
            return this
        }

        /**
         * Set with a boolean if the updatecolumn should be added to the grid
         * @param flag the flag indicating if the column should be visible
         * @return the builder
         */
        fun setEditableColumnFlag(flag: Boolean): GridBuilder<T> {
            isUpdatable = flag
            return this
        }

        /**
         * Build the grid
         *
         * @return the grid
         */
        fun build(): DataGrid<T> {
            if (isUpdatable) InternalCrud.addEditColumn(grid)
            else addViewColumn(grid)
            return grid
        }

        private fun addViewColumn(grid: DataGrid<T>): Column<T> {
            return grid.addColumn(
                LitRenderer.of<T>(
                    "<vaadin-crud-view aria-label='View' role=button>" +
                        "<div class=crud-view-dialog part=icon>" +
                            "<vaadin-button class=crud-view-button @click=\${clickEvent}>" +
                                "<vaadin-icon icon='lumo:search'></vaadin-icon>" +
                            "</vaadin-button>" +
                        "</div>" +
                    "</vaadin-crud-view>"
                ).withFunction("clickEvent") { dto ->
                    val mapCrudViewer = MapCrudViewer(beanType, formFieldComponentFactory, dto)
                    val dialog = Dialog(H3("View item"))
                    dialog.maxWidth = "864px"
                    dialog.maxHeight = "700px"
                    dialog.add(mapCrudViewer.formView)
                    dialog.open()
                }
            )
                .setKey("vaadin-crud-view-column")
                .setWidth("4em")
                .setFlexGrow(0)
        }

        private fun addAction(action: ActionInfo) {
            grid.addComponentColumn { getActionComponent(action, it) }.apply {
                this.key = action.action
                this.flexGrow = 0
            }
        }

        private fun getActionComponent(info: ActionInfo, bean: T): Component {
            val dataProvider = grid.dataProvider
            if (dataProvider !is ActionExecutor<*>) {
                logger.warn("Action '${info.action}' is configured, but the dataprovider is not an ActionExecutor!")
            } else {
                @Suppress("UNCHECKED_CAST") val executor = dataProvider as ActionExecutor<T>
                if (executor.isActionVisible(info.action, bean)) {
                    val button = Button(info.label) {
                        executor.executeAction(info.action, bean)
                        executor.refresh()
                        dataProvider.refreshAll()
                    }
                    button.isEnabled = executor.isActionEnabled(info.action, bean)
                    button.setId("${info.label}-${buttonInfo(bean)}".toHtmlCase())  // for E2E testing purposes
                    return button
                }
            }
            //default fall back to empty element
            return Div()
        }

        private fun buttonInfo(bean: T): String {
            val str = bean.toString()
            val dto = str.substringBefore("(").trim()  // dto name
            val key = str.substringAfter("Key=").substringBefore(",")  // key name
            return if (str.contains("Key=")) key else dto
        }

        private fun innerAddColumn(columnDefinition: ColumnDef) =
            ColumnFiller(columnDefinition).fill()

        private fun getGridButtonsForPosition(position: Position): List<ActionInfo> {
            return Metadata.getActionsForType(beanType).asSequence()
                .filter { it.position == position }
                .sortedBy { it.order }
                .toList()

        }

        inner class ColumnFiller(val columnDefinition: ColumnDef) {
            fun fill() {
                val rendererAnnotation = determineRendererAnnotation()
                val column = if (rendererAnnotation != null) {
                    fillColumnWithRenderer(rendererAnnotation)
                } else {
                    if (columnDefinition.useMapAccess) {
                        fillColumnUsingMapAccess()
                    } else {
                        fillColumnUsingStandardDtoAccess()
                    }
                }
                if (columnDefinition.searchable && filterRow != null) {
                    val filterField = FilterFieldFactory().createFilterField(grid, column)
                    filterField.setId("filter-${column.key.lowercase()}")
                    filterRow.getCell(column).setComponent(filterField)
                }
            }

            private fun fillColumnUsingStandardDtoAccess() =
                grid.addColumn(columnDefinition.name)
                    .setHeader(columnDefinition.description)
                    .setSortable(columnDefinition.sortable)


            private fun fillColumnUsingMapAccess() =
                grid.addColumn(
                    FieldGateFactory.getFieldReader(
                        beanType,
                        columnDefinition.name
                    )
                )
                    .setKey(columnDefinition.name)
                    .setHeader(columnDefinition.description)
                    .setSortable(columnDefinition.sortable)


            private fun fillColumnWithRenderer(rendererAnnotation: Annotation): Column<T> {
                val renderer = renderFactoryCollection.getRenderer(rendererAnnotation, beanType, columnDefinition.name)
                return grid.addColumn(renderer, columnDefinition.name)
                    .setKey(columnDefinition.name)
                    .setHeader(columnDefinition.description)
                    .setSortable(columnDefinition.sortable)
            }

            private fun determineRendererAnnotation() =
                Metadata.getRendererForColumn(columnDefinition.dto?.dtoClass as Class<T>, columnDefinition.name)

        }


    }


    /**
     * An inner class for combining the functionality for filter fields
     *
     * @author Marcel Pot
     * @since 1.0
     */
    inner class FilterFieldFactory {

        /**
         * Create a Filter Field
         */
        fun <T : Serializable> createFilterField(grid: DataGrid<T>, column: Column<T>): Component {
            val dataType = Metadata.getColumnsForViewing(grid.beanType)
                .filter { it.name == column.key }
                .map { it.dataType }
                .firstOrNull()
            return when (dataType) {
                DataType.ENUMERATION_LARGE -> createComboBox(grid, column)
                else -> createTextField(grid, column)
            }
        }

        private fun <T : Serializable> determineKey(column: Column<T>): String {
            return column.key
        }

        private fun <T : Serializable> createTextField(grid: DataGrid<T>, column: Column<T>): Component {
            val textField = TextField()
            textField.element.setAttribute("crud-role", "Search")
            textField.addValueChangeListener { event ->
                val key = determineKey(column)
                grid.removeFilterConstraint(key)
                if (!textField.isEmpty) {
                    grid.setFilterConstraint(key, event.value)
                }
                grid.dataProvider?.refreshAll()
            }
            textField.valueChangeMode = ValueChangeMode.EAGER

            textField.setSizeFull()
            textField.placeholder = "Filter"
            return textField
        }

        @Suppress("UNCHECKED_CAST")
        private fun <T : Serializable> createComboBox(grid: DataGrid<T>, column: Column<T>): Component {
            val comboBox = ComboBox<T>()
            comboBox.isAllowCustomValue = true
            Metadata.getColumnsForViewing(grid.beanType)
                .filter { it.name == column.key }
                .mapNotNull { it.itemProvider }
                .map { providerFactory.getItemProvider(it) }
                .forEach {
                    comboBox.setItems(it?.toListDataProvider() as ListDataProvider<T>)
                    comboBox.setItemLabelGenerator(it as ItemLabelGenerator<T>)
                }

            comboBox.element.setAttribute("crud-role", "Search")
            comboBox.addValueChangeListener { event ->
                val key = determineKey(column)
                grid.removeFilterConstraint(key)
                if (!comboBox.isEmpty) {
                    grid.setFilterConstraint(key, event.value.toString())
                }
                grid.dataProvider?.refreshAll()
            }
            comboBox.setSizeFull()
            comboBox.placeholder = "Filter"
            return comboBox
        }

    }


}

