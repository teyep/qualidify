package com.qualidify.metadata.gui.components.dialogs

import com.vaadin.flow.component.dialog.Dialog

/**
 * Abstract Dialog centralizing all functionality and displaying properties at one place
 *
 * @author Marcel Pot
 * @since 1.0
 */
abstract class AbstractDialogTemplate : Dialog() {
    protected open fun initialize() {
        isCloseOnEsc = false
        isCloseOnOutsideClick = false
        isModal = true
    }

    open fun show() = open()


}


