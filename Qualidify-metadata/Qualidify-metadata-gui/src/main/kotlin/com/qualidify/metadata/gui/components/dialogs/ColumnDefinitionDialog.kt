package com.qualidify.metadata.gui.components.dialogs

import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.ObjectMap
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.data.binder.Binder

class ColumnDefinitionDialog(
    private val formFieldComponentFactory: FormFieldComponentFactory,
    val columnDef: List<ColumnDef>,
) : DialogTemplate("input-dialog") {

    private val bean = ObjectMap()
    private val binder: Binder<ObjectMap> = Binder()

    constructor(formFieldComponentFactory: FormFieldComponentFactory, beanType: Class<*>) : this(
        formFieldComponentFactory,
        Metadata.getColumnsForEditing(beanType))

    init {
        initialize()
        addColumnDefinitions(columnDef)
        this.add(HorizontalLayout(cancelButton(), okButton()))
    }

    private fun addColumnDefinitions(columnDefinitions: List<ColumnDef>) {
        columnDefinitions.asSequence()
            .forEach { bean[it.name] = Any() }
        binder.setBean(bean)
        for (columnDef in columnDefinitions) {
            this.add(formFieldComponentFactory.createInputField(binder, columnDef))
        }
    }

    private fun cancelButton(): Button {
        val cancelButton = Button("Cancel") {
            binder.bean = null
            close()
        }
        return cancelButton
    }

    private fun okButton(): Button {
        val okButton = Button("OK") {
            binder.validate()
            close()
        }
        okButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        return okButton
    }

}