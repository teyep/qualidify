package com.qualidify.metadata.gui.components

import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.ObjectConverter
import com.qualidify.metadata.service.dto.ObjectMap
import com.vaadin.flow.component.HasValue
import com.vaadin.flow.data.binder.Binder
import org.springframework.util.CollectionUtils
import java.io.Serializable
import java.util.function.Supplier

/**
 * A [HasValue] implementation of a Grid, where a [Binder] can be used
 * for List Collections of a specific Bean
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param beanType the bean used in the collection
 * @param C the type of the Bean used in the collection
 * @param formFieldComponentFactory the factory to create input fields
 */
class InlineListEditor<C : Serializable>(
    val beanType: Class<C>,
    formFieldComponentFactory: FormFieldComponentFactory,
    val supplier: Supplier<C>,
) : InlineEditor<List<C>>(formFieldComponentFactory) {
    init {
        setupGrid()
        setupGridListeners()
        add(grid)
    }

    private fun setupGrid() {
        Metadata.getColumnsForEditing(beanType)
            .forEach { cd ->
                addColumn(binder, cd) { it.get(cd.name) }
            }
    }

    private fun setupGridListeners() {
        defaultGridValueChangeListener()
        defaultGridItemDoubleClickListener()
        defaultGridCloseListener()

        if (Metadata.isAdditionEnabled(beanType)) {
            val contextMenu = grid.addContextMenu()
            contextMenu.addItem(
                "Add new"
            ) {
                val newValue = toDtoMap(supplier.get())
                values.add(newValue)
                grid.dataProvider.refreshAll()
                grid.editor.editItem(newValue)
            }
        }
    }



    override fun setValue(values: List<C>?) {
        this.values.clear()
        if (!CollectionUtils.isEmpty(values)) {
            values!!.forEach { this.values.add(toDtoMap(it)) }
        }
        grid.setItems(this.values)
    }

    override fun getValue(): List<C> {
        return this.values.map { fromDtoMap(it) }.toList()
    }

    protected fun toDtoMap(item: C): ObjectMap {
        return ObjectConverter.convertToMapWithCustomFields(item, beanType)
    }

    protected fun fromDtoMap(objectMap: ObjectMap): C {
        return ObjectConverter.convertToObject(objectMap, beanType)
    }
}
