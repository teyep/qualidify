package com.qualidify.metadata.gui.components.builders

/**
 * An interface for all vaadin component builders
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface ComponentFieldBuilder<C> {
    /**
     * Build the component
     */
    fun build(): C
}