package com.qualidify.metadata.gui.components.interfaces

import com.vaadin.flow.component.UI

/**
 * Vaadin interface for copying from a Vaadin element to the clipboard
 *
 * @author Marcel Pot
 */
interface ToClipboardCopyable {
    /**
     * Execute the copy mechanism
     *
     * Default implementation will copy text to the clipboard with a Javascript implementation
     *
     * @param text the text to add to the clipboard
     */
    fun doCopyToClipboard(text: String?) {
        UI.getCurrent().page.executeJs(
            "const el = document.createElement('textarea'); " +
                    " el.value = $0; " +
                    " document.body.appendChild(el); " +
                    " el.select();" +
                    " document.execCommand('copy');" +
                    " document.body.removeChild(el);", text
        )


        // UI.getCurrent().page.executeJs("window.copyToClipboard($0)", text)
    }
}