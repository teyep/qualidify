package com.qualidify.metadata.gui.components.crud

import com.vaadin.flow.component.Component
import com.vaadin.flow.component.HasValue
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.binder.ValidationException


class InternalBinderCrudEditor<E : Any>(
    private val binder: Binder<E>,
    override val view: Component? = null
) :
    InternalCrudEditor<E> {


    private var editableItem: E? = null

    override fun setItem(item: E?, validate: Boolean) {
        this.editableItem = item
        binder.readBean(item)
        if (validate) {
            binder.validate()
        }
    }

    override fun getItem(): E? {
        return editableItem
    }


    override fun writeItemChanges() {
        try {
            binder.writeBean(editableItem)
        } catch (e: ValidationException) {
            throw IllegalStateException(e)
        }
    }

    /**
     * {@inheritDoc}
     *
     * Frees the item, and lazily clears all input fields.
     */
    override fun clear() {
        editableItem = null
        binder.readBean(null)
        binder.fields.forEach { obj: HasValue<*, *> -> obj.clear() }
    }

    override fun validate(): Boolean {
        return binder.validate().isOk
    }


}
