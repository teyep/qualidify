package com.qualidify.metadata.gui.components

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.ObjectMap
import com.vaadin.flow.component.HasValue
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.grid.ItemDoubleClickEvent
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.renderer.ComponentRenderer
import com.vaadin.flow.function.ValueProvider
import com.vaadin.flow.shared.Registration


abstract class InlineEditor<C>(
    val formFieldComponentFactory: FormFieldComponentFactory,
) : Div(),
    HasValue<HasValue.ValueChangeEvent<C>, C> {

    protected val values: MutableList<ObjectMap> = mutableListOf()
    protected val grid: Grid<ObjectMap> = Grid(ObjectMap::class.java, false)
    protected val binder = Binder(ObjectMap::class.java)

    private var readOnly: Boolean = false
    private var requiredIndicatorVisible: Boolean = false

    private val logger by LoggerDelegation()

    init {
        grid.editor.binder = binder
    }

    @Suppress("UNCHECKED_CAST")
    protected fun addColumn(
        binder: Binder<ObjectMap>,
        columnDef: ColumnDef,
        valueProvider: ValueProvider<ObjectMap, Any>,
    ) {
        val useDetailsSection = columnDef.getBooleanAttribute(AttributeConstants.DETAILS)

        val inputField = formFieldComponentFactory.createInputField(binder,
            columnDef.copy(useMapAccess = true),
            false)

        if (useDetailsSection != null) {
            grid.setItemDetailsRenderer(ComponentRenderer { item ->
                (inputField as InlineEditor<*>).value = (valueProvider.apply(item))
                inputField
            })
        } else {
            grid
                .addColumn(valueProvider)
                .setHeader(columnDef.name)
                .setEditorComponent(inputField)
        }
    }

    protected fun defaultGridValueChangeListener() {
        binder.addValueChangeListener {
            grid.editor.refresh()
        }
    }

    protected fun defaultGridItemDoubleClickListener() {
        grid.addItemDoubleClickListener { event: ItemDoubleClickEvent<ObjectMap> ->
            grid.setDetailsVisible(event.item, true)
            grid.editor.editItem(event.item)
        }
    }

    protected fun defaultGridCloseListener() {
        grid.editor.addCloseListener {
            if (binder.bean != null) {
                logger.debug(binder.bean.toString())
            }
        }
    }


    override fun setReadOnly(readOnly: Boolean) {
        this.readOnly = readOnly
    }

    override fun isReadOnly(): Boolean {
        return this.readOnly
    }

    override fun setRequiredIndicatorVisible(requiredIndicatorVisible: Boolean) {
        this.requiredIndicatorVisible = requiredIndicatorVisible
    }

    override fun isRequiredIndicatorVisible(): Boolean {
        return this.requiredIndicatorVisible
    }

    override fun addValueChangeListener(listener: HasValue.ValueChangeListener<in HasValue.ValueChangeEvent<C>>): Registration? {
        return null
    }
}