package com.qualidify.metadata.gui.components.crud

import com.vaadin.flow.component.Component
import java.io.Serializable

interface InternalCrudEditor<E : Any> : Serializable {


    /**
     * Sets an item to be edited. This could be a newly instantiated item or an
     * existing item from the grid. Initial validation will be skipped.
     *
     * @param item
     * the item to edit
     * @see .setItem
     */
    fun setItem(item: E?) {
        setItem(item, false)
    }

    /**
     * Sets an item to be edited. This could be a newly instantiated item or an
     * existing item from the grid.
     *
     * @param item
     * the item to edit
     * @param validate
     * if true the item will be validated immediately
     */
    fun setItem(item: E?, validate: Boolean)

    /**
     * Returns the item being edited.
     *
     * @return the item being edited
     */
    fun getItem(): E?

    /**
     * Clears the editor.
     */
    fun clear()

    /**
     * Runs validations on the data entered into an editor and returns their
     * validity but could also have side-effects such as showing visual
     * indicators for invalid fields.
     *
     * @return true if valid or false if otherwise
     */
    fun validate(): Boolean

    /**
     * Writes any pending input update (if any) to the item.
     */
    fun writeItemChanges()

    val view: Component?

}