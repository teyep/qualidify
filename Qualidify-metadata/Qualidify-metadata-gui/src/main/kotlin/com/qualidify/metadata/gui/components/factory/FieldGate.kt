package com.qualidify.metadata.gui.components.factory

import com.qualidify.metadata.dto.CustomizableDto
import org.springframework.util.ReflectionUtils

typealias FieldReader<O> = (O) -> Any?
typealias FieldSetter<O> = (O, Any?) -> Unit

/**
 * A factory for determining the right access gate for a specific object
 * It will determine the [FieldReader] or the [FieldSetter] based on the type of the bean
 *
 * @author
 * @since 1.0
 */
@Suppress("UNCHECKED_CAST")
object FieldGateFactory {

    /**
     * Get the right [FieldReader] based on bean
     * @param beanType the type of the Bean
     * @param bean the Bean where the fields needs to be approached
     * @param field the name of the field to be approached
     * @param T the type of the Bean
     * @return the right FieldGate
     * @throws NotImplementedError when no FieldGate can be determined
     */
    @JvmStatic
    fun <T> determineFieldReader(beanType: Class<out T>, bean: T, field: String): FieldReader<T> {
        when (bean) {
            is CustomizableDto -> return CustomizableDtoFieldReader(field) as FieldReader<T>
            is Map<*, *> -> return MapFieldReader(field) as FieldReader<T>
            else -> return StandardDtoFieldReader(beanType, field)
        }
    }

    /**
     * Meaningfull helper function for getting a FieldReader
     * @param bean the Bean where the fields needs to be approached
     * @param field the name of the field to be approached
     * @param T the type of the Bean
     * @return the right FieldReader
     */
    @JvmStatic
    inline fun <reified T> getFieldReader(bean: T, field: String): FieldReader<T> {
        return determineFieldReader(T::class.java, bean, field)
    }

    /**
     * Meaningfull helper function for getting a ValueProvider
     * @param beanType the type of the Bean
     * @param bean the Bean where the fields needs to be approached
     * @param field the name of the field to be approached
     * @param T the type of the Bean
     * @return the right ValueProvider
     */
    @JvmStatic
    fun <T> getFieldReader(beanType: Class<T>, bean: T, field: String): FieldReader<T> {
        return determineFieldReader(beanType, bean, field)
    }

    /**
     * Meaningfull helper function for getting a ValueProvider
     * @param beanType the type of the Bean
     * @param bean the Bean where the fields needs to be approached
     * @param field the name of the field to be approached
     * @param T the type of the Bean
     * @return the right ValueProvider
     */
    @JvmStatic
    fun <T> getFieldReader(beanType: Class<T>, field: String): FieldReader<T> {
        return { determineFieldReader(beanType, it, field).invoke(it) }
    }

    /**
     * Helper function for getting the Value of a bean field
     * @param bean the Bean where the fields needs to be approached
     * @param field the name of the field to be approached
     * @param T the type of the Bean
     * @param F the type of the Field
     * @return the right value
     * @throws NotImplementedError when no FieldGate can be determined
     */
    @JvmStatic
    fun <T : Any, F> getValue(bean: T, field: String): F {
        return determineFieldReader(bean::class.java, bean, field).invoke(bean) as F
    }

    /**
     * Get the right [FieldSetter] based on bean
     * @param beanType the type of the Bean
     * @param bean the Bean where the fields needs to be approached
     * @param field the name of the field to be approached
     * @param T the type of the Bean
     * @return the right FieldGate
     */
    @JvmStatic
    fun <T> determineFieldSetter(beanType: Class<out T>, bean: T, field: String): FieldSetter<T> {
        when (bean) {
            is CustomizableDto -> return CustomizableDtoFieldSetter(field) as FieldSetter<T>
            is Map<*, *> -> return MapFieldSetter(field) as FieldSetter<T>
            else -> return StandardDtoFieldSetter(beanType, field)
        }
    }

    /**
     * Meaningfull helper function for getting a Setter
     * @param bean the Bean where the fields needs to be approached
     * @param field the name of the field to be approached
     * @return the right Setter
     * @throws NotImplementedError when no FieldGate can be determined
     */
    @JvmStatic
    inline fun <reified T> getSetter(bean: T, field: String): FieldSetter<T> {
        return determineFieldSetter(T::class.java, bean, field)
    }

    /**
     * Helper function for setting the Value of a bean field
     * @param bean the Bean where the fields needs to be approached
     * @param field the name of the field to be approached
     * @param value the value to set
     * @param T the type of the Bean
     * @param F the type of the Field
     * @throws NotImplementedError when no FieldGate can be determined
     */
    @JvmStatic
    fun <T : Any, F> setValue(bean: T, field: String, value: F?) {
        determineFieldSetter(bean::class.java, bean, field).invoke(bean, value)
    }
}

/**
 * An access gate for reading a field  of a [MutableMap]
 *
 * @author Marcel Pot
 * @since 1.0
 */
class MapFieldReader(val key: String) : FieldReader<Map<String, Any?>> {
    override fun invoke(map: Map<String, Any?>): Any? {
        return map[key]
    }
}

/**
 * An access gate for setting a field  of a [MutableMap]
 *
 * @author Marcel Pot
 * @since 1.0
 */
class MapFieldSetter(val key: String) : FieldSetter<MutableMap<String, Any?>> {
    override fun invoke(map: MutableMap<String, Any?>, value: Any?) {
        map[key] = value
    }
}


/**
 * An access gate for reading a field  of a [CustomizableDto]
 *
 * @author Marcel Pot
 * @since 1.0
 */
class CustomizableDtoFieldReader(val key: String) : FieldReader<CustomizableDto> {
    override fun invoke(map: CustomizableDto): Any? {
        return map.get(key)
    }
}

/**
 * An access gate for setting a field  of a [CustomizableDto]
 *
 * @author Marcel Pot
 * @since 1.0
 */
class CustomizableDtoFieldSetter(val key: String) : FieldSetter<CustomizableDto> {
    override fun invoke(map: CustomizableDto, value: Any?) {
        map.set(key, value?.toString() ?: "")
    }
}

/**
 * An access gate for reading a field  of any object
 *
 * @author Marcel Pot
 * @since 1.0
 */
class StandardDtoFieldReader<T>(val beanType: Class<out T>, val key: String) : FieldReader<T> {
    override fun invoke(item: T): Any? {
        return ReflectionUtils.findField(beanType, key)?.let { field ->
            field.isAccessible = true
            return@let field.get(item)
        }
    }
}

/**
 * An access gate for setting a field  of any object
 *
 * @author Marcel Pot
 * @since 1.0
 */
class StandardDtoFieldSetter<T>(val beanType: Class<out T>, val key: String) : FieldSetter<T> {
    override fun invoke(item: T, value: Any?) {
        ReflectionUtils.findField(beanType, key)?.let { field ->
            field.isAccessible = true
            field.set(item, value)
        }
    }
}

