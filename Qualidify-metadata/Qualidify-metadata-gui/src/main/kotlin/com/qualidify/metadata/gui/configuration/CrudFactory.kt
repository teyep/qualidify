package com.qualidify.metadata.gui.configuration

import com.qualidify.metadata.annotations.CreationStrategyPattern.*
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.metadata.gui.components.MapCrudEditor
import com.qualidify.metadata.gui.components.crud.InternalCrud
import com.qualidify.metadata.gui.components.crud.InternalCrud.*
import com.qualidify.metadata.gui.components.crud.InternalCrudI18n
import com.qualidify.metadata.gui.components.dialogs.ObjectCreationWizard
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.gui.components.factory.GridFactory
import com.qualidify.metadata.gui.i18n.TranslationService
import com.qualidify.metadata.gui.providers.DataConnector
import com.qualidify.metadata.gui.providers.MetadataConnector
import com.qualidify.metadata.gui.providers.Outcome
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.Metadata.Companion.isAdditionEnabled
import com.qualidify.metadata.service.ObjectCreator
import com.vaadin.flow.component.ClickEvent
import com.vaadin.flow.component.ComponentEventListener
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.component.html.Span
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.BeanCreationException
import org.springframework.beans.factory.config.DependencyDescriptor
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Scope
import org.springframework.core.ResolvableType
import java.io.Serializable
import java.util.*

/**
 * Configuration Class for Grid and Crud Spring beans
 *
 * @author Marcel Pot
 */
@Configuration
open class CrudFactory(
    private val applicationContext: ApplicationContext,
    private val gridFactory: GridFactory,
    private val formFieldComponentFactory: FormFieldComponentFactory,
    private val objectCreator: ObjectCreator,
    private val translationService: TranslationService,
) {

    /**
     * Generate a Crud based on the generic type.
     * Scope is prototype to ensure every bean-request leads to a new instance
     *
     * @param injectionPoint the request information (including generics)
     * @param T            The generic type requested
     * @return the crud of type T
     */
    @Suppress("SpringJavaInjectionPointsAutowiringInspection", "UNCHECKED_CAST")
    @Bean
    @Primary
    @Scope("prototype")
    open fun <T : Serializable> defaultCrud(injectionPoint: DependencyDescriptor): DataCrud<T> {
        val resolvableType = injectionPoint.resolvableType
        val genericType = resolvableType.getGeneric()
        val beanType = genericType.resolve() as Class<T>
        return buildCrud(beanType = beanType)
    }

    /**
     * Generate a Crud based on a mnemonic String.
     * Scope is prototype to ensure every bean-request leads to a new instance
     *
     * @param mnemonic the Mnemonic (@link{com.qualidify.metadata.annotations.Mnemonic})
     * @param <T>      The MetaDrivenDto
     * @return the crud of type T
    </T> */
    @Suppress("UNCHECKED_CAST", "SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    @Scope("prototype")
    open fun <T : Serializable> mnemonicCrud(mnemonic: String): DataCrud<T> {
        val beanType = Metadata.getDtoByMnemonic(mnemonic) as Class<T>
        return buildCrud(beanType, mnemonic)
    }

    private fun <T : Serializable> buildCrud(beanType: Class<T>, mnemonic: String? = null): DataCrud<T> {
        val editor = buildEditor(beanType)
        val grid = buildGrid(beanType)
        val dataConnector = getDataConnector(beanType, mnemonic)

        val dataCrud = DataCrud(beanType, grid, editor)
        dataCrud.setI18n(crudI18n(Locale.ENGLISH))

        dataCrud.addSaveListener(upsert(dataConnector))
        dataCrud.addDeleteListener(delete(dataConnector))
        dataCrud.dataProvider = dataConnector

        addToolbar(dataCrud)
        return dataCrud
    }

    private fun <T : Serializable> buildEditor(beanType: Class<T>) =
        MapCrudEditor(beanType, FormLayout(), formFieldComponentFactory)

    private fun <T : Serializable> buildGrid(beanType: Class<T>) = gridFactory.getGrid(beanType)

    @Suppress("UNCHECKED_CAST")
    fun <T : Serializable> getDataConnector(beanType: Class<T>, mnemonic: String? = null): DataConnector<T> {
        if (StringUtils.isEmpty(mnemonic)) {
            return getDataConnectorWithResolvableType(beanType)
        } else {
            return applicationContext.getBean(MetadataConnector::class.java, mnemonic) as DataConnector<T>
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : Serializable> getDataConnectorWithResolvableType(beanType: Class<T>): DataConnector<T> {
        val resolvableType = ResolvableType.forClassWithGenerics(DataConnector::class.java, beanType)
        val beanProvider = applicationContext.getBeanProvider<Any>(resolvableType)

        return Optional.ofNullable(beanProvider.ifAvailable as DataConnector<T>)
            .orElseThrow { BeanCreationException("DataConnector could not be found for generic $beanType.") }
    }


    private fun crudI18n(locale: Locale): InternalCrudI18n {
        val identity = "defaultcrud"
        val i18N = InternalCrudI18n.createDefault()
        i18N.newItem = translationService.getTranslation("$identity.newItem", locale)
        i18N.saveItem = translationService.getTranslation("$identity.saveItem", locale)
        i18N.cancel = translationService.getTranslation("$identity.cancel", locale)
        i18N.deleteItem = translationService.getTranslation("$identity.deleteItem", locale)
        i18N.editLabel = translationService.getTranslation("$identity.editLabel", locale)

        val confirm = i18N.confirm

        val delete = confirm?.delete

        delete?.title = translationService.getTranslation("$identity.confirm.delete.title", locale)
        delete?.content = translationService.getTranslation("$identity.confirm.delete.content", locale)
        delete?.button?.confirm = translationService.getTranslation("$identity.confirm.delete.button.confirm", locale)
        delete?.button?.dismiss = translationService.getTranslation("$identity.confirm.delete.button.dismiss", locale)

        val cancel = confirm?.cancel

        cancel?.title = translationService.getTranslation("$identity.confirm.cancel.title", locale)
        cancel?.content = translationService.getTranslation("$identity.confirm.cancel.content", locale)
        cancel?.button?.confirm = translationService.getTranslation("$identity.confirm.cancel.button.confirm", locale)
        cancel?.button?.dismiss = translationService.getTranslation("$identity.confirm.cancel.button.dismiss", locale)
        return i18N
    }


    private fun <T : Serializable> delete(dataConnector: DataConnector<T>): ComponentEventListener<DeleteEvent<T>> {
        return ComponentEventListener { e: InternalCrud.DeleteEvent<T> ->
            val outcome: Outcome<Any> = dataConnector.delete(e.item!!)
            checkOutcome<Any>(outcome)
        }
    }

    private fun <T : Serializable> upsert(dataConnector: DataConnector<T>): ComponentEventListener<SaveEvent<T>> {
        return ComponentEventListener { e: SaveEvent<T> ->
            val isNew = e.source.element.getProperty("__isNew", false)
            val outcome: Outcome<Any>
            outcome = if (isNew) {
                dataConnector.create(e.item!!)
            } else {
                dataConnector.update(e.item!!)
            }
            checkOutcome<Any>(outcome)
        }
    }

    private fun <R> checkOutcome(outcome: Outcome<R>) {
        if (!outcome.isSuccess) {
            try {
                outcome.result
            } catch (t: Exception) {
                throw t
            }
        }
    }

    private fun <T : Serializable> addToolbar(crud: DataCrud<T>) {
        addToolbar(crud) { determineCreationStrategy(crud) }
    }

    private fun <T : Serializable> determineCreationStrategy(dataCrud: DataCrud<T>) {
        val creationStrategyPattern = objectCreator.determineCreationStrategyPattern(dataCrud.beanType!!)
        when (creationStrategyPattern) {
            EMPTY_CONSTRUCTOR, MAP_DTO -> dataCrud.edit(
                objectCreator.create(dataCrud.beanType!!),
                EditMode.NEW_ITEM
            )

            OBJECT_CONSTRUCTOR, FACTORY -> {
                val dialog: Dialog = ObjectCreationWizard(dataCrud, formFieldComponentFactory, objectCreator)
                dialog.open()
            }
        }
    }


    private fun <T : Serializable> addToolbar(
        crud: DataCrud<T>,
        listener: ComponentEventListener<ClickEvent<Button?>>
    ) {
        val footer = Span()
        footer.element.setAttribute("flex", "1")
        if (isAdditionEnabled(crud.beanType!!)) {
            val newItemButton = Button("Add item")
            newItemButton.setId("add-item")
            newItemButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
            newItemButton.addClickListener(listener)
            crud.setToolbar(footer, newItemButton)
        } else {
            crud.setToolbar(footer)
        }
    }


}