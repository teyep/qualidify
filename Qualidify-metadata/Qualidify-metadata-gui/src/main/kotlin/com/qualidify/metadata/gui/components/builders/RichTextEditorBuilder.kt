package com.qualidify.metadata.gui.components.builders

/**
 * A builder for RichText Editor Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
//class RichTextEditorBuilder<D : Any>(
//    definition: ColumnDef,
//    binder: Binder<D>? = null,
//    enableLabel: Boolean = true,
//) : AbstractComponentFieldBuilder<D, RichTextEditorWrapper, String>(definition, binder, enableLabel) {
//
//    override lateinit var component: RichTextEditorWrapper
//
//    init {
//        val dataFormat = definition.getEnumAttribute(AttributeConstants.DATA_FORMAT) ?: DataFormat.HTML
//        component = RichTextEditorWrapper(DataFormat.HTML.equals(dataFormat))
//    }
//
//    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
//        component.setId(definition.id ?: ("editor" + "-" + definition.name.lowercase()))
//    }
//
//}
//
///**
// * An wapper for the [RichTextEditor] to let the editor handle html as value
// *
// * @author Marcel Pot
// * @since 1.0
// */
//class RichTextEditorWrapper(val isHtml: Boolean) : Div(),
//    HasValue<AbstractField.ComponentValueChangeEvent<RichTextEditor, String>, String> {
//
//    val instance = RichTextEditor()
//
//    init {
//        if (isHtml) {
//            instance.asHtml()
//        }
//        element.setAttribute("colspan", "2")
//        instance.getElement().setAttribute("colspan", "2")
//        add(instance)
//    }
//
//    override fun getValue(): String {
//        return if (isHtml) instance.htmlValue else instance.value
//    }
//
//    override fun setValue(value: String?) {
//        if (isHtml) {
//            instance.asHtml().value = value
//        } else {
//            instance.value = value
//        }
//    }
//
//    override fun setReadOnly(readOnly: Boolean) {
//        return instance.setReadOnly(readOnly)
//    }
//
//    override fun isReadOnly(): Boolean {
//        return instance.isReadOnly
//    }
//
//    override fun setRequiredIndicatorVisible(requiredIndicatorVisible: Boolean) {
//        return instance.setRequiredIndicatorVisible(requiredIndicatorVisible)
//    }
//
//    override fun isRequiredIndicatorVisible(): Boolean {
//        return instance.isRequiredIndicatorVisible
//    }
//
//    override fun addValueChangeListener(changeListener: HasValue.ValueChangeListener<in AbstractField.ComponentValueChangeEvent<RichTextEditor, String>>?): Registration {
//        return instance.addValueChangeListener(changeListener)
//    }
//
//    override fun setId(id: String) {
//        instance.setId(id)
//    }
//
////    override fun getElement(): Element {
////        return instance.element
////    }
//
//}