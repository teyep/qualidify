package com.qualidify.metadata.gui.components

import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.ObjectConverter
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.ObjectMap
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.Tag
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.data.binder.Binder

@Tag("crud-view")
class MapCrudViewer<T : Any>(
    private val beanType: Class<T>,
    private val formFieldComponentFactory: FormFieldComponentFactory,
    dto: T
) : Component() {

    val formView: FormLayout = FormLayout()
    private val binder = Binder(ObjectMap::class.java)
    private var objectMap: ObjectMap = ObjectConverter.convertToMapWithCustomFields(dto, beanType)

    init {
        binder.bean = objectMap
        addColumnsBasedOnMetaData()
    }

    private fun addColumnsBasedOnMetaData() {
        columns
            .stream()
            .filter { !it.hidden }
            .forEach { columnDefinition: ColumnDef -> innerAddColumn(columnDefinition) }
    }

    private fun innerAddColumn(columnDefinition: ColumnDef) {
        formView.add(
            formFieldComponentFactory.createInputField(binder, columnDefinition.copy(useMapAccess = true))
        )
    }

    private val columns: List<ColumnDef>
        get() {
            return Metadata.getColumnsForEditing(beanType)
        }


}

