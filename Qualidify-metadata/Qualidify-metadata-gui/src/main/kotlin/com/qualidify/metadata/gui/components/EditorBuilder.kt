package com.qualidify.metadata.gui.components


import com.qualidify.metadata.gui.components.crud.InternalBinderCrudEditor
import com.qualidify.metadata.gui.components.crud.InternalCrudEditor
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.binder.BeanValidationBinder
import com.vaadin.flow.data.binder.Binder
import java.io.Serializable
import java.util.function.Consumer

/**
 * Builder for the CrudEditor
 *
 * @param T the Dto type
 */
class EditorBuilder<T : Serializable> protected constructor(
    private val beanType: Class<T>,
    private val formFieldComponentFactory: FormFieldComponentFactory
) {

    private val binder: Binder<T>
    private val form: FormLayout

    init {
        binder = BeanValidationBinder<T>(beanType)
        form = FormLayout()
    }

    /**
     * Add a column to the grid
     *
     * @param columnName The name of the column
     * @return the builder
     */
    fun addColumn(columnName: String?): EditorBuilder<T> {
        val textField = TextField(columnName)
        form.add(textField)
        binder.forField(textField).bind(columnName)
        return this
    }

    /**
     * Add a column to the grid
     *
     * @param columnDefinition a columnDefinition
     * @return the builder
     */
    fun addColumnByDefinition(columnDefinition: ColumnDef): EditorBuilder<T> {
        innerAddColumn(columnDefinition)
        return this
    }

    /**
     * Add a columns to the grid
     *
     * @param columnDefinitions a collection of columnDefinitions
     * @return the builder
     */
    fun addColumnsByDefinition(columnDefinitions: Collection<ColumnDef>): EditorBuilder<T> {
        columnDefinitions.forEach(Consumer { columnDefinition: ColumnDef -> innerAddColumn(columnDefinition) })
        return this
    }

    /**
     * Add columns to the grid based on the MetaDataConfiguration
     *
     * @return the builder
     */
    fun addColumnsBasedOnMetaData(): EditorBuilder<T> {
        Metadata.getColumnsForEditing(beanType)
            .forEach(Consumer { columnDefinition: ColumnDef -> innerAddColumn(columnDefinition) })
        return this
    }

    /**
     * Build the crud editor
     *
     * @return the CrudEditor
     */
    fun build(): InternalCrudEditor<T> {
        return InternalBinderCrudEditor<T>(binder, form)
    }

    private fun innerAddColumn(columnDefinition: ColumnDef) {
        form.add(
            formFieldComponentFactory.createInputField(binder, columnDefinition)
        )
    }

    companion object {
        /**
         * Initialize the builder
         *
         * @param beanType the classType
         * @param <T>      must implement Serializable
         * @return the builder
        </T> */
        fun <T : Serializable> of(
            beanType: Class<T>,
            inputFieldComponentFactory: FormFieldComponentFactory
        ): EditorBuilder<T> {
            return EditorBuilder(beanType, inputFieldComponentFactory)
        }
    }


}