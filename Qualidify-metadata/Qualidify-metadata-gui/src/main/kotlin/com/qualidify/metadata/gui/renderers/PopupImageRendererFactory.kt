package com.qualidify.metadata.gui.renderers

import com.qualidify.metadata.gui.components.factory.FieldGateFactory
import com.qualidify.metadata.gui.components.dialogs.ImageDialog
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.html.Image
import com.vaadin.flow.data.renderer.ComponentRenderer
import com.vaadin.flow.data.renderer.Renderer
import com.vaadin.flow.server.InputStreamFactory
import com.vaadin.flow.server.StreamResource
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.Serializable


@Component
class PopupImageRendererFactory<T : Serializable> : RendererFactory<T, Any> {

    override fun create(item: T, field: String, attributes: Map<String, Any?>): Renderer<T> {
        //val attributes = Metadata.getRendererAttributesForColumn(item::class.java, field)
        val content = FieldGateFactory.getValue<T, ByteArray>(item, field)
        val streamResource =
            StreamResource("test", InputStreamFactory { return@InputStreamFactory ByteArrayInputStream(content) })

        return PopupImageRenderer(attributes) { Image(streamResource, " alt") }
    }

    override fun create(attributes: Map<String, Any?>, provider: (T) -> Any): Renderer<T> {
        return PopupImageRenderer(attributes) {
            val byteArray = provider.invoke(it)
            if (byteArray is ByteArray) {
                return@PopupImageRenderer Image(StreamResource("test",
                    InputStreamFactory { ByteArrayInputStream(byteArray) }), "alt")
            } else {
                return@PopupImageRenderer null
            }

        }
    }

    private fun emptyImage(): Image {
        val img = Image();
        img.isVisible = false
        return img;
    }


    class PopupImageRenderer<T>(
        val attributes: Map<String, Any?>,
        val imageGenerator: (T) -> Image?,
    ) : ComponentRenderer<Button?, T>() {

        override fun createComponent(item: T): Button {
            val image: Image? = imageGenerator.invoke(item)
            if (image != null) {
                image.height = "32px"
                val button = Button(image)

                button.addClickListener {
                    val dialogImage = imageGenerator.invoke(item)!!
                    attributes["height"]?.let { dialogImage.height = it as String }
                    attributes["width"]?.let { dialogImage.width = it as String }
                    val dialog = ImageDialog(dialogImage)
                    dialog.open()
                }
                return button
            } else {
                val button = Button()
                button.isVisible = false
                return button
            }

        }
    }


}


