package com.qualidify.metadata.gui.components.crud

import com.vaadin.flow.component.*
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.dependency.JsModule
import com.vaadin.flow.component.dependency.NpmPackage
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.component.grid.ItemClickEvent
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.data.renderer.Renderer
import com.vaadin.flow.data.renderer.TemplateRenderer
import com.vaadin.flow.dom.Element
import com.vaadin.flow.internal.JsonSerializer
import com.vaadin.flow.shared.Registration
import elemental.json.JsonObject
import java.util.*
import java.util.stream.Collectors


@Tag("vaadin-crud")
@NpmPackage(value = "@vaadin/polymer-legacy-adapter", version = "23.0.7")
@JsModule("@vaadin/polymer-legacy-adapter/style-modules.js")
@NpmPackage(value = "@vaadin/crud", version = "23.0.7")
@NpmPackage(value = "@vaadin/vaadin-crud", version = "23.0.7")
@JsModule("@vaadin/crud/src/vaadin-crud.js")
@JsModule("@vaadin/crud/src/vaadin-crud-edit-column.js")
open class InternalCrud<E : Any>() : Component(), HasSize, HasTheme {

    private val newListeners: MutableSet<ComponentEventListener<NewEvent<E>>> = LinkedHashSet()
    private val editListeners: MutableSet<ComponentEventListener<EditEvent<E>>> = LinkedHashSet()
    private val saveListeners: MutableSet<ComponentEventListener<SaveEvent<E>>> = LinkedHashSet()
    private val cancelListeners: MutableSet<ComponentEventListener<CancelEvent<E>>> = LinkedHashSet()
    private val deleteListeners: MutableSet<ComponentEventListener<DeleteEvent<E>>> = LinkedHashSet()
    /**
     * Gets the runtime bean type information
     *
     * @return the bean type
     */
    /**
     * Sets the runtime bean type information. If no grid exists a built-in grid
     * is created since the bean type information is now known. When injecting a
     * [Crud] with @Id this method must be called before the
     * crud is put into use.
     *
     * @param beanType
     * the bean type
     */
    var beanType: Class<E>? = null
        get() {
            if (field == null) {
                throw IllegalStateException(
                    "The bean type must be initialized before event processing"
                )
            }
            return field
        }
        set(beanType) {
            Objects.requireNonNull(beanType, "Bean type cannot be null")
            field = beanType
            if (grid == null) {
                setGrid(InternalGrid(beanType!!, true, true))
            }
        }

    private var grid: InternalGrid<E>? = null
    private var editor: InternalCrudEditor<E>? = null
    private var gridActiveItem: E? = null
    /**
     * Gets visiblity state of toolbar
     *
     * @param
     * @return true if toolbar is visible false otherwise
     */
    /**
     * Controls visiblity of toolbar
     *
     * @param value
     */
    var toolbarVisible: Boolean = true
        set(value) {
            field = value
            if (value) {
                element.setProperty("noToolbar", false)
            } else {
                element.setProperty("noToolbar", true)
            }
        }
    private var saveBtnDisabledOverridden: Boolean = false

    /**
     * Gets the Crud save button
     *
     *
     * NOTE: State of the button set with
     * [com.vaadin.flow.component.HasEnabled.setEnabled] will
     * remain even if dirty state of the crud changes
     *
     * @return the save button
     * @see Crud.setDirty
     */
    val saveButton: Button

    /**
     * Gets the Crud cancel button
     *
     * @return the cancel button
     */
    val cancelButton: Button

    /**
     * Gets the Crud editor delete button
     *
     * @return the delete button
     */
    val deleteButton: Button

    /**
     * Instantiates a new Crud using a custom grid.
     *
     * @param beanType
     * the class of items
     * @param grid
     * the grid with which the items listing should be displayed
     * @param editor
     * the editor for manipulating individual items
     * @see com.vaadin.flow.component.crud.Crud.Crud
     */
    constructor(beanType: Class<E>, grid: InternalGrid<E>, editor: InternalCrudEditor<E>) : this() {
        setGrid(grid)
        setEditor(editor)
        this.beanType = beanType
    }

    /**
     * Instantiates a new Crud for the given bean type and uses the supplied
     * editor. Furthermore, it displays the items using the built-in grid.
     *
     * @param beanType
     * the class of items
     * @param editor
     * the editor for manipulating individual items
     * @see CrudGrid
     *
     * @see com.vaadin.flow.component.crud.Crud.Crud
     */
    constructor(beanType: Class<E>?, editor: InternalCrudEditor<E>) : this() {
        setEditor(editor)
        this.beanType = beanType
    }

    private inner class SaveButton() : Button() {
        override fun onEnabledStateChanged(enabled: Boolean) {
            super.onEnabledStateChanged(enabled)
            saveBtnDisabledOverridden = true
            overrideSaveDisabled(enabled)
        }
    }

    override fun onAttach(attachEvent: AttachEvent) {
        super.onAttach(attachEvent)
        if (saveBtnDisabledOverridden) {
            overrideSaveDisabled(saveButton.isEnabled)
        }
        element.executeJs("this.__validate = function () {return true;}")
    }

    private fun overrideSaveDisabled(enabled: Boolean) {
        element.executeJs(
            "this.__isSaveBtnDisabled = () => {return $0;}",
            !enabled
        )
    }

    private fun registerHandlers() {
        ComponentUtil.addListener(this, NewEvent::class.java)
        {
            try {
                editor?.setItem(if (it.item != null) it.item as E else beanType!!.newInstance() as E)
                clearActiveItem()
                setClientIsNew(true)
            } catch (ex: Exception) {
                throw CrudException(
                    "Unable to instantiate new bean", ex
                )
            }
            val item = editor!!.getItem()
            val eventWithNewItem = NewEvent(
                it.source!! as InternalCrud<E>,
                it.isFromClient(),
                item!!,
                null
            )
            newListeners.forEach { listener ->
                listener.onComponentEvent(eventWithNewItem)
            }
        }

        ComponentUtil.addListener(this, EditEvent::class.java)
        {
            if (editor!!.getItem() !== it.item) {
                editor!!.setItem(it.item as E, true)
                setOpened(true)
                setClientIsNew(false)
                if (isEditOnClick && grid is InternalGrid<E>) {
                    grid!!.select(it.item)
                }
            }

            editListeners
                .forEach { listener -> listener.onComponentEvent(it as EditEvent<E>) }
        }

        ComponentUtil.addListener(this, CancelEvent::class.java)
        {
            cancelListeners
                .forEach { listener -> listener.onComponentEvent(it as CancelEvent<E>) }

            if (gridActiveItem != null && editor!!.getItem() === gridActiveItem
                || gridActiveItem == null
            ) {
                setOpened(false)
                editor!!.clear()
                clearActiveItem()
            }
        }

        ComponentUtil.addListener(this, SaveEvent::class.java)
        {
            if (!editor!!.validate()) {
                return@addListener
            }
            editor!!.writeItemChanges()
            try {
                saveListeners.forEach { listener -> listener.onComponentEvent(it as SaveEvent<E>) }
                setOpened(false)
                editor!!.clear()
            } finally {
                if (grid!!.getDataProvider() != null) {
                    grid!!.getDataProvider()!!.refreshAll()
                }
            }
        }

        ComponentUtil.addListener(this, DeleteEvent::class.java)
        {
            try {
                deleteListeners.forEach { listener -> listener.onComponentEvent(it as DeleteEvent<E>) }
                setOpened(false)
                editor!!.clear()
            } finally {
                grid!!.getDataProvider()!!.refreshAll()
            }
        }
    }

    /**
     * Initiates an item edit from the server-side. This sets the supplied item
     * as the working bean and opens the edit dialog.
     *
     * @param item
     * the item to be edited
     * @param editMode
     * the edit mode
     */
    fun edit(item: E, editMode: EditMode) {
        val event: InnerCrudEvent<E>
        if (editMode == EditMode.NEW_ITEM) {
            event = NewEvent(this, false, item, null)
        } else {
            setDirty(false)
            event = EditEvent(this, false, item)
        }
        setOpened(true)
        ComponentUtil.fireEvent(this, event)
    }

    private fun setClientIsNew(isNew: Boolean) {
        element.setProperty("__isNew", isNew)
    }

    /**
     * Opens or closes the editor. In most use cases opening or closing the
     * editor is automatically done by the component and this method does not
     * need to be called.
     *
     * @param opened
     * true to open or false to close
     */
    fun setOpened(opened: Boolean) {
        element.callFunction("set", "editorOpened", opened)
    }

    /**
     * Set the dirty state of the Crud.
     *
     *
     * A dirty Crud has its editor Save button enabled. Ideally a Crud
     * automatically detects if it is dirty based on interactions with the form
     * fields within it but in some special cases (e.g with composites) this
     * might not be automatically detected. For such cases this method could be
     * used to explicitly set the dirty state of the Crud editor.
     *
     *
     * NOTE: editor Save button will not be automatically enabled in case its
     * enabled state was changed with [Crud.getSaveButton]
     *
     * @param dirty
     * true if dirty and false if otherwise.
     * @see .getSaveButton
     */
    fun setDirty(dirty: Boolean) {
        element.executeJs("this.set('__isDirty', $0)", dirty)
    }

    /**
     * Gets the grid
     *
     * @return the grid
     */
    fun getGrid(): InternalGrid<E> {
        if (grid == null) {
            throw IllegalStateException(
                "The grid must be initialized before event processing"
            )
        }
        return grid!!
    }

    /**
     * Sets the grid
     *
     * @param grid
     * the grid
     */
    fun setGrid(grid: InternalGrid<E>) {
        if ((this.grid != null && (element == this.grid!!.element.parent))) {
            this.grid!!.element.removeFromParent()
        }
        this.grid = grid
        grid.element.setAttribute(SLOT_KEY, GRID_SLOT_NAME)

        // It might already have a parent e.g when injected from a template
        if (grid.element.parent == null) {
            element.appendChild(grid.element)
        }
    }

    /**
     * Gets the crud editor.
     *
     * @return the crud editor
     */
    fun getEditor(): InternalCrudEditor<E> {
        if (editor == null) {
            throw IllegalStateException(
                "The editor must be initialized before event processing"
            )
        }
        return editor!!
    }

    /**
     * Sets the editor. When injecting a [Crud] with @Id this
     * method must be called before the crud is put into use.
     *
     * @param editor
     * the editor
     */
    fun setEditor(editor: InternalCrudEditor<E>) {
        if ((this.editor != null) && (this.editor!!.view != null) && (this.editor!!
                .view!!.element.parent === element)
        ) {
            this.editor!!.view!!.element.removeFromParent()
        }
        this.editor = editor

        // It might already have a parent e.g when injected from a template
        if ((editor.view != null
                    && editor.view!!.element.parent == null)
        ) {
            editor.view!!.element.setAttribute(
                SLOT_KEY,
                FORM_SLOT_NAME
            )
            element.appendChild(editor.view!!.element)
        }
    }
    /**
     * Gets the current editor position on the desktop screen.
     *
     *
     * The default position is [CrudEditorPosition.OVERLAY].
     *
     * @return the editor position
     */
    /**
     * Sets how editor will be presented on desktop screen.
     *
     *
     * The default position is [CrudEditorPosition.OVERLAY].
     *
     * @param editorPosition
     * the editor position, never `null`
     * @see CrudEditorPosition
     */
    var editorPosition: InternalCrudEditorPosition
        get() = InternalCrudEditorPosition.toPosition(
            element.getProperty("editorPosition", ""),
            InternalCrudEditorPosition.OVERLAY
        )
        set(editorPosition) {
            if (editorPosition == null) {
                throw IllegalArgumentException(
                    "The 'editorPosition' argument can not be null"
                )
            }
            element.setProperty(
                "editorPosition",
                editorPosition.editorPosition
            )
        }
    private var gridItemClickRegistration: Registration? = null

    /**
     * Instantiates a new Crud with no grid, editor and runtime bean type
     * information. The editor and bean type must be initialized before a Crud
     * is put into full use therefore this constructor only exists for partial
     * initialization in order to support template binding.
     *
     * <pre>
     * Example:
     * `
     * &#064;Id
     * Crud<Person> crud;
     *
     * &#064;Id
     * private TextField firstName;
     *
     * &#064;Id
     * private TextField lastName;
     *
     * &#064;Override
     * protected void onAttach(AttachEvent attachEvent) {
     * super.onAttach(attachEvent);
     *
     * Binder<Person> binder = new Binder<>(Person.class);
     * binder.bind(firstName, Person::getFirstName, Person::setFirstName);
     * binder.bind(lastName, Person::getLastName, Person::setLastName);
     *
     * crud.setEditor(new BinderCrudEditor<>(binder));
     * crud.setBeanType(Person.class);
     *
     * crud.setDataProvider(new PersonCrudDataProvider());
     * }
    ` *
    </pre> *
     *
     * @see .setEditor
     * @see .setBeanType
     */
    init {
        setI18n(InternalCrudI18n.createDefault(), false)
        registerHandlers()
        saveButton = SaveButton()
        saveButton.element.setAttribute("slot", "save-button")
        saveButton.addThemeName("primary")
        element.appendChild(saveButton.element)

        cancelButton = Button()
        cancelButton.element.setAttribute("slot", "cancel-button")
        cancelButton.addThemeName("tertiary")
        element.appendChild(cancelButton.element)

        deleteButton = Button()
        deleteButton.element.setAttribute("slot", "delete-button")
        deleteButton.addThemeNames("tertiary", "error")
        element.appendChild(deleteButton.element)

    }
    /**
     * Gets whether click on row to edit item is enabled or not.
     *
     * @return `true` if enabled, `false` otherwise
     */
    /**
     * Sets the option to open item to edit by row click.
     *
     *
     * If enabled, it removes the edit column created by [CrudGrid].
     *
     * @param editOnClick
     * `true` to enable it (`false`, by default).
     */
    var isEditOnClick: Boolean
        get() {
            return element.getProperty("editOnClick", false)
        }
        set(editOnClick) {
            element.setProperty("editOnClick", editOnClick)
            if (editOnClick) {
                if (getGrid() is InternalGrid<*>) {
                    grid!!.setSelectionMode(Grid.SelectionMode.SINGLE)
                    if (hasEditColumn(grid!!)) {
                        removeEditColumn(grid!!)
                    }
                }
                gridItemClickRegistration = grid!!.addItemClickListener(
                    { e: ItemClickEvent<E> ->
                        gridActiveItem = e.getItem()
                    })
            } else if (gridItemClickRegistration != null) {
                clearActiveItem()
                gridItemClickRegistration!!.remove()
                if (grid is InternalGrid<*>) {
                    addEditColumn(grid!!)
                    grid!!.setSelectionMode(Grid.SelectionMode.NONE)
                }
            }
        }

    private fun clearActiveItem() {
        gridActiveItem = null
        grid!!.deselectAll()
    }

    /**
     * Sets the content of the toolbar. Any content with the attribute
     * `new-button` triggers a new item creation.
     *
     * @param components
     * the content to be set
     */
    fun setToolbar(vararg components: Component) {
        val existingToolbarElements: Array<Element> = element.children
            .filter { TOOLBAR_SLOT_NAME == it.getAttribute(SLOT_KEY) }
            .toList().toTypedArray()


        element.removeChild(*existingToolbarElements)
        val newToolbarElements: Array<Element> = Arrays.stream(components)
            .map { obj: Component -> obj.getElement() }
            .map {
                it.setAttribute(
                    SLOT_KEY,
                    TOOLBAR_SLOT_NAME
                )
            }
            .toList().toTypedArray()
        element.appendChild(*newToolbarElements)
    }

    /**
     * Sets the internationalized messages to be used by this crud instance.
     *
     * @param i18n
     * the internationalized messages
     * @see CrudI18n.createDefault
     */
    fun setI18n(i18n: InternalCrudI18n) {
        setI18n(i18n, true)
    }

    private fun setI18n(i18n: InternalCrudI18n, fireEvent: Boolean) {
        element.setPropertyJson("i18n", JsonSerializer.toJson(i18n))
        if (fireEvent) {
            ComponentUtil.fireEvent(
                grid,
                InternalCrudI18nUpdatedEvent(this, false, i18n)
            )
        }
    }

    /**
     * Adds theme variants to the component.
     *
     * @param variants
     * theme variants to add
     */
    fun addThemeVariants(vararg variants: InternalCrudVariant) {
        val variantNames: List<String> = variantNames(*variants)
        themeNames.addAll(variantNames)
        if (grid is InternalGrid<*>) {
            (grid as InternalGrid<*>).addCrudThemeVariants(variantNames)
        }
    }

    /**
     * Removes theme variants from the component.
     *
     * @param variants
     * theme variants to remove
     */
    fun removeThemeVariants(vararg variants: InternalCrudVariant) {
        val variantNames: List<String> = variantNames(*variants)
        themeNames.removeAll(variantNames)
        if (grid is InternalGrid<*>) {
            (grid as InternalGrid<*>).removeCrudThemeVariants(variantNames)
        }
    }

    /**
     * Registers a listener to be notified when the user starts to create a new
     * item.
     *
     * @param listener
     * a listener to be notified
     * @return a handle that can be used to unregister the listener
     */
    fun addNewListener(
        listener: ComponentEventListener<NewEvent<E>>
    ): Registration {
        newListeners.add(listener)
        return Registration { newListeners.remove(listener) }
    }

    /**
     * Registers a listener to be notified when the user starts to edit an
     * existing item.
     *
     * @param listener
     * a listener to be notified
     * @return a handle that can be used to unregister the listener
     */
    fun addEditListener(
        listener: ComponentEventListener<EditEvent<E>>
    ): Registration {
        editListeners.add(listener)
        return Registration { editListeners.remove(listener) }
    }

    /**
     * Registers a listener to be notified when the user tries to save a new
     * item or modifications to an existing item.
     *
     * @param listener
     * a listener to be notified
     * @return a handle that can be used to unregister the listener
     */
    fun addSaveListener(
        listener: ComponentEventListener<SaveEvent<E>>
    ): Registration {
        saveListeners.add(listener)
        return Registration { saveListeners.remove(listener) }
    }

    /**
     * Registers a listener to be notified when the user cancels a new item
     * creation or existing item modification in progress.
     *
     * @param listener
     * a listener to be notified
     * @return a handle that can be used to unregister the listener
     */
    fun addCancelListener(
        listener: ComponentEventListener<CancelEvent<E>>
    ): Registration {
        cancelListeners.add(listener)
        return Registration { cancelListeners.remove(listener) }
    }

    /**
     * Registers a listener to be notified when the user tries to delete an
     * existing item.
     *
     * @param listener
     * a listener to be notified
     * @return a handle that can be used to unregister the listener
     */
    fun addDeleteListener(
        listener: ComponentEventListener<DeleteEvent<E>>
    ): Registration {
        deleteListeners.add(listener)
        return Registration { deleteListeners.remove(listener) }
    }
    /**
     * Gets the data provider supplying the grid data.
     *
     * @return the data provider for the grid
     */
    /**
     * Sets the data provider for the grid.
     *
     * @param provider
     * the data provider for the grid
     */
    var dataProvider: DataProvider<E, *>?
        get() {
            return grid!!.dataProvider
        }
        set(provider) {
            grid!!.setDataProvider(provider!!)
        }

    /**
     * The base class for all Crud events.
     *
     * @param <E>
     * the bean type
     * @see com.vaadin.flow.component.crud.Crud.Crud
    </E> */
    abstract class InnerCrudEvent<E : Any>(source: InternalCrud<E>, fromClient: Boolean) :
        ComponentEvent<InternalCrud<E>>(source, fromClient) {
        /**
         * Gets the item being currently edited.
         *
         * @return the item being currently edited
         */
        open val item: E?
            get() {
                return getSource()!!.editor!!.getItem()
            }
    }

    /**
     * Event fired when the user cancels the creation of a new item or
     * modifications to an existing item.
     *
     * @param <E>
     * the bean type
    </E> */
    @DomEvent("cancel")
    class CancelEvent<E : Any>(
        source: InternalCrud<E>, fromClient: Boolean,
        @EventData(EVENT_PREVENT_DEFAULT_JS) ignored: Any?
    ) :
        InnerCrudEvent<E>(source, fromClient)

    /**
     * Event fired when the user tries to delete an existing item.
     *
     * @param <E>
     * the bean type
    </E> */
    @DomEvent("delete")
    class DeleteEvent<E : Any>
    /**
     * Creates a new event using the given source and indicator whether the
     * event originated from the client side or the server side.
     *
     * @param source
     * the source component
     * @param fromClient
     * `true` if the event originated from the client
     * @param ignored
     * an ignored parameter for a side effect
     */
        (
        source: InternalCrud<E>, fromClient: Boolean,
        @EventData(EVENT_PREVENT_DEFAULT_JS) ignored: Any?
    ) :
        InnerCrudEvent<E>(source, fromClient)

    /**
     * Event fired when the user starts to edit an existing item.
     *
     * @param <E>
     * the bean type
    </E> */
    @DomEvent("edit")
    class EditEvent<E : Any>(source: InternalCrud<E>, fromClient: Boolean, override val item: E) :
        InnerCrudEvent<E>(source, fromClient) {

        constructor(
            source: InternalCrud<E>, fromClient: Boolean,
            @EventData("event.detail.item") item: JsonObject,
            @EventData("event.preventDefault()") ignored: Any?
        ) : this(source, fromClient, source.grid!!.dataCommunicator.keyMapper[item.getString("key")])

    }

    /**
     * Event fired when the user starts to create a new item.
     *
     * @param <E>
     * the bean type
    </E> */
    @DomEvent("new")
    class NewEvent<E : Any>(
        source: InternalCrud<E>,
        fromClient: Boolean,
        override val item: E?,
        @EventData("event.preventDefault()") ignored: Any?
    ) : InnerCrudEvent<E>(source, fromClient) {

        constructor(
            source: InternalCrud<E>,
            fromClient: Boolean, @EventData("event.preventDefault()") ignored: Any?
        ) : this(source, fromClient, null, ignored)

    }

    /**
     * Event fired when the user tries to save a new item or modifications to an
     * existing item.
     *
     * @param <E>
     * the bean type
    </E> */
    @DomEvent("save")
    class SaveEvent<E : Any>
        (
        source: InternalCrud<E>, fromClient: Boolean,
        @EventData(EVENT_PREVENT_DEFAULT_JS) ignored: Any?
    ) :
        InnerCrudEvent<E>(source, fromClient)

    /**
     * Determines whether an item presented for editing is to be treated as a
     * new item or an existing item.
     *
     * @see Crud.edit
     */
    enum class EditMode {
        /**
         * The item presented for editing should be treated as a new item.
         */
        NEW_ITEM,

        /**
         * The item presented for editing should be treated as an existing item.
         */
        EXISTING_ITEM
    }

    companion object {
        private const val EDIT_COLUMN_KEY: String = "vaadin-crud-edit-column"
        private const val EVENT_PREVENT_DEFAULT_JS: String = "event.preventDefault()"
        private const val FORM_SLOT_NAME: String = "form"
        private const val GRID_SLOT_NAME: String = "grid"
        private const val SLOT_KEY: String = "slot"
        private const val TOOLBAR_SLOT_NAME: String = "toolbar"

        private fun variantNames(vararg variants: InternalCrudVariant): List<String> {
            return Arrays.stream(variants)
                .map({ obj: InternalCrudVariant -> obj.variantName })
                .collect(Collectors.toList())
        }
        /**
         * A helper method to add an edit column to a grid. Clicking on the edit
         * cell for a row opens the item for editing in the editor. Additionally,
         * the i18n object is used for setting the aria-label for the button,
         * improving accessibility.
         *
         * @param grid
         * the grid in which to add the edit column
         * @param crudI18n
         * the i18n object for localizing the accessibility of the edit
         * column
         */
        /**
         * A helper method to add an edit column to a grid. Clicking on the edit
         * cell for a row opens the item for editing in the editor.
         *
         * @param grid
         * the grid in which to add the edit column
         * @see .addEditColumn
         * @see .removeEditColumn
         * @see .hasEditColumn
         */
        fun addEditColumn(grid: InternalGrid<*>, crudI18n: InternalCrudI18n = InternalCrudI18n.createDefault()) {
            grid.addColumn { TemplateRenderer.of<String>(createEditColumnTemplate(crudI18n)) as Renderer<String> }
                .setKey(EDIT_COLUMN_KEY).setWidth("4em").setFlexGrow(0)
        }

        private fun createEditColumnTemplate(crudI18n: InternalCrudI18n): String {
            return ("<vaadin-crud-edit aria-label=\"" + crudI18n.editLabel
                    + "\"></vaadin-crud-edit>")
        }

        /**
         * Removes the crud edit column from a grid
         *
         * @param grid
         * the grid from which to remove the edit column
         * @see .addEditColumn
         * @see .hasEditColumn
         */
        fun removeEditColumn(grid: InternalGrid<*>) {
            grid.removeColumnByKey(EDIT_COLUMN_KEY)
        }

        /**
         * Checks if an edit column has been added to the Grid using
         * `Crud.addEditColumn(Grid)`
         *
         * @param grid
         * the grid to check
         * @return true if an edit column is present or false if otherwise
         * @see Crud.addEditColumn
         */
        fun hasEditColumn(grid: InternalGrid<*>): Boolean {
            return grid.getColumnByKey(EDIT_COLUMN_KEY) != null
        }
    }
}

class CrudException(msg: String? = null, cause: Throwable? = null) : RuntimeException(msg, cause)