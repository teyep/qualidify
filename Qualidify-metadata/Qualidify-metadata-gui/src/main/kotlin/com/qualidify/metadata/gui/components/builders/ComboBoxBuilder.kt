package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.ItemLabelGenerator
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.provider.ListDataProvider

/**
 * A builder for ComboBox Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 * @param T the type of the Field value
 */
class ComboBoxBuilder<D : Any, T>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, ComboBox<T>, T>(definition, binder, enableLabel) {

    override val component: ComboBox<T> = ComboBox()

    @Suppress("UNCHECKED_CAST")
    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("combobox" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        definition.getStringAttribute(AttributeConstants.PLACEHOLDER)?.let {
            component.placeholder = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_OPEN)?.let {
            component.isAutoOpen = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_FOCUS)?.let {
            component.isAutofocus = it
        }
        definition.getIntegerAttribute(AttributeConstants.PAGE_SIZE)?.let {
            if (it > 0) component.pageSize = it
        }
        definition.getBooleanAttribute(AttributeConstants.CLEAR_BUTTON_VISIBILE)?.let {
            component.isClearButtonVisible = it
        }
        definition.getBooleanAttribute(AttributeConstants.ALLOC_CUSTOM_VALUE)?.let {
            component.isAllowCustomValue = it
        }
        definition.getStringAttribute(AttributeConstants.HELPER_TEXT)?.let {
            component.helperText = it
        }
        definition.itemProvider?.run {
            val itemProvider = providerFactory?.getItemProvider(this)
            itemProvider?.run {
                component.setItems(this.toListDataProvider() as ListDataProvider<T>)
                component.setItemLabelGenerator(this as ItemLabelGenerator<T>)
            }
        }
        component.isReadOnly = !definition.editable
    }
}
