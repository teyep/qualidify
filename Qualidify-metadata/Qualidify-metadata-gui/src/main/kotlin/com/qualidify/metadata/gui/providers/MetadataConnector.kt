package com.qualidify.metadata.gui.providers

import org.springframework.context.annotation.Scope
import java.io.Serializable

@Scope("prototype")
interface MetadataConnector<T : Serializable> : DataConnector<T> {
    var mnemonic: String
}