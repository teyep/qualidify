package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.binder.ValidationResult
import com.vaadin.flow.data.binder.ValueContext
import com.vaadin.flow.data.validator.AbstractValidator

/**
 * A builder for TextField Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
class TextFieldBuilder<D : Any>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, TextField, String>(definition, binder, enableLabel) {

    override val component: TextField = TextField()

    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("textfield" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        definition.getStringAttribute(AttributeConstants.TITLE)?.let {
            component.title = it
        }
        definition.getBooleanAttribute(AttributeConstants.IS_REQUIRED)?.let {
            component.isRequired = it
            if (it) {
                addRequiredFieldValidators()
                addValidator(
                    predicate = { component.value.toString().trim() != "" },
                    errorMessage = "Required field cannot be empty"
                )
            }
        }
        definition.getIntegerAttribute(AttributeConstants.MAX_LENGTH)?.let {
            component.maxLength = it
        }
        definition.getIntegerAttribute(AttributeConstants.MIN_LENGTH)?.let {
            component.minLength = it
        }
        definition.getStringAttribute(AttributeConstants.PATTERN)?.let {
            component.pattern = it
        }
        definition.getStringAttribute(AttributeConstants.PLACEHOLDER)?.let {
            component.placeholder = it
        }
        definition.getBooleanAttribute(AttributeConstants.CLEAR_BUTTON_VISIBILE)?.let {
            component.isClearButtonVisible = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_FOCUS)?.let {
            component.isAutofocus = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_SELECT)?.let {
            component.isAutoselect = it
        }
        definition.getBooleanAttribute(AttributeConstants.PREVENT_INVALID_INPUT)?.let {
            component.isPreventInvalidInput = it
        }
        component.isReadOnly = !definition.editable

        addValidator(TextFieldValidator("Problem"))
    }

    internal class TextFieldValidator<T>(errorMessage: String) : AbstractValidator<T>(errorMessage) {
        override fun apply(value: T, context: ValueContext): ValidationResult {
            return this.toResult(value, isValid(context))
        }

        protected fun isValid(context: ValueContext): Boolean {
            return !(context.component.get() as TextField).isInvalid
        }
    }
}

