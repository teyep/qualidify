package com.qualidify.metadata.gui.renderers

import com.qualidify.metadata.gui.components.factory.FieldGateFactory
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.HasComponents
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.data.renderer.ComponentRenderer
import com.vaadin.flow.data.renderer.Renderer
import java.io.Serializable

@org.springframework.stereotype.Component
class ActiveRendererFactory<T : Serializable> : RendererFactory<T, Boolean> {

    override fun create(item: T, field: String, attributes: Map<String, Any?>): Renderer<T> {
        return create(attributes) { FieldGateFactory.getValue(item, field) }
    }

    override fun create(attributes: Map<String, Any?>, provider: (T) -> Boolean): Renderer<T> {
        return ActiveRenderer(provider)
    }

}

//@org.springframework.stereotype.Component
class ActiveRenderer<T : Serializable>(val provider: (T) -> Any) :
    ComponentRenderer<Component, T>() {

    class IconComponent : Div(), HasComponents

    override fun createComponent(item: T): Component {
        val value = provider.invoke(item)
        val component = IconComponent()
        when (value) {
            is Boolean -> {

                val icon: Icon
                if (value) {
                    icon = VaadinIcon.CHECK.create()
                    icon.setColor("green")
                } else {
                    icon = VaadinIcon.CLOSE.create()
                    icon.setColor("red")
                }
                component.add(icon)
            }
            else -> {
                val icon: Icon
                icon = VaadinIcon.CLOSE.create()
                icon.setColor("red")
                component.add(icon)
            }
        }
        return component
    }


}