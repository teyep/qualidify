package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.textfield.BigDecimalField
import com.vaadin.flow.data.binder.Binder
import java.math.BigDecimal

/**
 * A builder for BigDecimal Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
class BigDecimalFieldBuilder<D : Any>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, BigDecimalField, BigDecimal>(definition, binder, enableLabel) {

    override val component: BigDecimalField = BigDecimalField()


    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("bigdecimalfield" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        definition.getStringAttribute(AttributeConstants.TITLE)?.let {
            component.title = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_FOCUS)?.let {
            component.isAutofocus = it
        }
        definition.getBooleanAttribute(AttributeConstants.AUTO_SELECT)?.let {
            component.isAutoselect = it
        }
        definition.getBooleanAttribute(AttributeConstants.CLEAR_BUTTON_VISIBILE)?.let {
            component.isClearButtonVisible = it
        }
        definition.getBooleanAttribute(AttributeConstants.USE_DEFAULT_VALUE)?.let {
            val defaultValue = definition.getDoubleAttribute(AttributeConstants.DEFAULT_VALUE) ?: 0.0
            component.value = BigDecimal.valueOf(defaultValue)
        }
        definition.getStringAttribute(AttributeConstants.HELPER_TEXT)?.let {
            component.helperText = it
        }
        component.isReadOnly = !definition.editable
    }

}