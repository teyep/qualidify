package com.qualidify.metadata.gui.components.crud

import com.vaadin.flow.component.ComponentEvent


class InternalCrudI18nUpdatedEvent(
    source: InternalCrud<*>, fromClient: Boolean,
    val i18n: InternalCrudI18n
) : ComponentEvent<InternalCrud<*>>(source, fromClient)