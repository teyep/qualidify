package com.qualidify.metadata.gui.components.crud

import java.util.*

enum class InternalCrudEditorPosition(val editorPosition: String) {
    /**
     * Editor form is positioned inside the overlay
     */
    OVERLAY(""),

    /**
     * Editor form is positioned below the grid
     */
    BOTTOM("bottom"),

    /**
     * Editor form is positioned on the grid side
     *
     *
     * - `right` - if ltr <br></br>
     * - `left` - if rtl
     */
    ASIDE("aside");

    companion object {
        fun toPosition(
            editorPosition: String,
            defaultValue: InternalCrudEditorPosition
        ): InternalCrudEditorPosition {
            return Arrays.stream(InternalCrudEditorPosition.values())
                .filter {
                    it.editorPosition == editorPosition
                }
                .findFirst().orElse(defaultValue)
        }
    }
}