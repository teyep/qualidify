package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.gui.components.factory.FieldGateFactory
import com.qualidify.metadata.gui.components.factory.ProviderFactory
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.HasValue
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.binder.Validator
import com.vaadin.flow.function.SerializablePredicate

/**
 * Abstract Input Field builder for building vaadin components
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param D the type of the dto
 * @param C the type of the Vaadin component
 * @param F the type of the field
 *
 * @param definition the columndefinition
 * @param binder the Binder (null allowed)
 * @param enableLabel a boolean indicating if the label is visible
 */
abstract class AbstractComponentFieldBuilder<D : Any, C : HasValue<*, F>, F>(
    protected val definition: ColumnDef,
    protected val binder: Binder<D>? = null,
    protected val enableLabel: Boolean = true,
) : ComponentFieldBuilder<C> {

    abstract val component: C
    private val validators: MutableList<Validator<F>> = mutableListOf()
    protected var providerFactory: ProviderFactory? = null

    /**
     * Generate the component with all configured attributes
     *
     * @return The vaadin component
     */
    override fun build(): C {
        val fieldBinder = if (binder != null) {
            binder.forField(component)
        } else {
            Binder<D>().forField(component)
        }

        configureComponent(definition, enableLabel)
        validators.forEach { fieldBinder.withValidator(it) }
        fieldBinder.bind(valueProvider(), setter())
        return component
    }

    protected abstract fun configureComponent(definition: ColumnDef, enableLabel: Boolean)

    protected fun <T : Any> valueProvider(): (T) -> F? {
        return { bean: T -> FieldGateFactory.getValue(bean, definition.name) }
    }

    protected fun <T : Any, F> setter(): (T, F?) -> Unit {
        return { bean: T, value: F? -> FieldGateFactory.setValue<T, F>(bean, definition.name, value) }
    }

    /**
     * Add a field validator
     *
     * @param predicate the Predicate
     * @param errorMessage the errorMessage
     * @return the builder
     */
    fun addValidator(
        predicate: SerializablePredicate<F>,
        errorMessage: String,
    ): AbstractComponentFieldBuilder<D, C, F> {
        return addValidator(Validator.from(predicate, errorMessage))
    }

    /**
     * Add a field validator
     *
     * @param validator the Validator
     * @return the builder
     */
    fun addValidator(validator: Validator<F>): AbstractComponentFieldBuilder<D, C, F> {
        validators.add(validator)
        return this
    }


    fun addRequiredFieldValidators() {
        addValidator(predicate = { component.value != null }, errorMessage = "Required field cannot be null")
        addValidator(predicate = { !component.isEmpty }, errorMessage = "Required field cannot be empty")
    }


    /**
     * Add a providerFactory
     * @param providerFactory the providerFactory
     * @return the builder
     */
    fun withProviderFactory(providerFactory: ProviderFactory): AbstractComponentFieldBuilder<D, C, F> {
        this.providerFactory = providerFactory
        return this
    }
}