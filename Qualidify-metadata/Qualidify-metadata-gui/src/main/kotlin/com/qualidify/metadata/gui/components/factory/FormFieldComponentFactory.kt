package com.qualidify.metadata.gui.components.factory

import com.qualidify.metadata.DataType
import com.qualidify.metadata.gui.components.InlineEditor
import com.qualidify.metadata.gui.components.builders.*
import com.qualidify.metadata.service.ObjectCreator
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.Component
import com.vaadin.flow.component.checkbox.Checkbox
import com.vaadin.flow.component.checkbox.CheckboxGroup
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.datepicker.DatePicker
import com.vaadin.flow.component.datetimepicker.DateTimePicker
import com.vaadin.flow.component.listbox.ListBox
import com.vaadin.flow.component.radiobutton.RadioButtonGroup
import com.vaadin.flow.component.select.Select
import com.vaadin.flow.component.textfield.*
import com.vaadin.flow.component.timepicker.TimePicker
import com.vaadin.flow.component.upload.Upload
import com.vaadin.flow.data.binder.Binder
import java.io.Serializable


/**
 * A field component factory for building input fields based on a columnDefinition
 *
 * @author Marcel Pot
 * @since 1.0
 */
@org.springframework.stereotype.Component
class FormFieldComponentFactory(
    private val providerFactory: ProviderFactory,
    private val objectCreator: ObjectCreator,
    private val maxFileSize: Int,
) {

    /**
     * Create an inputfield based on a column definition, and update the binder
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param enableLabel  indicator to enable/disable the label
     * @param D          Dto
     */
    @JvmOverloads
    fun <D : Any> createInputField(
        binder: Binder<D>,
        definition: ColumnDef,
        enableLabel: Boolean = true,
    ): Component {
        val config = FormFieldConfiguration()
        config.label = enableLabel
        return createInputField(binder, definition, config)
    }

    /**
     * Create an inputfield based on a column definition, and update the binder
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config     a configuration map
     * @param D          Dto
     */
    private fun <D : Any> createInputField(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration,
    ): Component {
        return when (definition.dataType) {
            DataType.BINARY -> createUpload(binder, definition, config)
            DataType.BIGDECIMAL -> createBigDecimalField(binder, definition, config)
            DataType.BOOLEAN -> createCheckBox(binder, definition, config)
            DataType.CHILD_COLLECTION -> createGrid(binder, definition, config)
            DataType.COLLECTION_SMALL -> createCheckBoxGroup<D, Any>(binder, definition, config)
            DataType.COLLECTION_LARGE -> createComboBox<D, Any>(binder, definition, config)
            DataType.DATE -> createDatePicker(binder, definition, config)
            DataType.DATETIME -> createDateTimePicker(binder, definition, config)
            DataType.DOUBLE -> createNumberField(binder, definition, config)
            DataType.EMAIL -> createEmailField(binder, definition, config)
            DataType.ENUMERATION_SMALL -> createRadioButtonGroup<D, Any>(binder, definition, config)
            DataType.ENUMERATION_LARGE -> createSelect<D, Any>(binder, definition, config)
            DataType.INTEGER -> createIntegerField(binder, definition, config)
            DataType.PASSWORD -> createPasswordField(binder, definition, config)
//            DataType.RICH_TEXT -> createRichTextEditor(binder, definition, config)
            DataType.STRING -> createTextField(binder, definition, config)
            DataType.TEXT -> createTextArea(binder, definition, config)
            DataType.TIME -> createTimePicker(binder, definition, config)
            else -> createTextField(binder, definition, config)
        }
    }


    /**
     * create a TextField
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D        Dto
     * @return the textfield
     */
    @JvmOverloads
    fun <D : Any> createTextField(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): TextField {
        return TextFieldBuilder(definition, binder, config.label).build()
    }

    /**
     * create a TextArea
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D        Dto
     * @return the textArea
     */
    @JvmOverloads
    fun <D : Any> createTextArea(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): TextArea {
        return TextAreaBuilder(definition, binder, config.label).build()
    }

    /**
     * create a Rich Text Editor
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D        Dto
     * @return the richtTextEditor
     */
//    @JvmOverloads
//    fun <D : Any> createRichTextEditor(
//        binder: Binder<D>,
//        definition: ColumnDef,
//        config: FormFieldConfiguration = FormFieldConfiguration(),
//    ): RichTextEditorWrapper {
//        return RichTextEditorBuilder(definition, binder, config.label).build()
//    }

    /**
     * create a Email
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D        Dto
     * @return the textfield
     */
    @JvmOverloads
    fun <D : Any> createEmailField(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): EmailField {
        return EmailFieldBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a PasswordField
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D      Dto
     */
    @JvmOverloads
    fun <D : Any> createPasswordField(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): PasswordField {
        return PasswordFieldBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a NumberField (for Doubles)
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D       Dto
     * @return the numberField
     */
    @JvmOverloads
    fun <D : Any> createNumberField(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): NumberField {
        return NumberFieldBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a IntegerField
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D       Dto
     * @return the integerField
     */
    @JvmOverloads
    fun <D : Any> createIntegerField(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): IntegerField {
        return IntegerFieldBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a DatePicker
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D       Dto
     * @return the datePicker
     */
    @JvmOverloads
    fun <D : Any> createDatePicker(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): DatePicker {
        return DatePickerBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a TimePicker
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D       Dto
     * @return the timePicker
     */
    @JvmOverloads
    fun <D : Any> createTimePicker(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): TimePicker {
        return TimePickerBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a DateTimePicker
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D       Dto
     * @return the dateTimePicker
     */
    @JvmOverloads
    fun <D : Any> createDateTimePicker(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): DateTimePicker {
        return DateTimePickerBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a BigDecimalField
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D       Dto
     * @return the bigDecimalField
     */
    @JvmOverloads
    fun <D : Any> createBigDecimalField(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): BigDecimalField {
        return BigDecimalFieldBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a CheckBox
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D        Dto
     * @return the checkbox
     */
    @JvmOverloads
    fun <D : Any> createCheckBox(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): Checkbox {
        return CheckBoxBuilder(definition, binder, config.label).build()
    }

    /**
     * Create a RadioButton
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D        Dto
     * @param T        The type of the input
     * @return the checkbox
     */

    @JvmOverloads
    fun <D : Any, T : Any> createRadioButtonGroup(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): RadioButtonGroup<T> {
        return RadioButtonGroupBuilder<D, T>(definition, binder, config.label)
            .withProviderFactory(providerFactory)
            .build()

    }

    /**
     * Create a CheckBoxGroup
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D        Dto
     * @param T        The type of the input
     * @return the CheckBoxGroup
     */
    @JvmOverloads
    fun <D : Any, T : Any> createCheckBoxGroup(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): CheckboxGroup<T> {
        return CheckboxGroupBuilder<D, T>(definition, binder, config.label)
            .withProviderFactory(providerFactory)
            .build()
    }

    /**
     * Create a ComboBox
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D      Dto
     * @param T       ComboBox Type

     * @return the comboBox
     */
    @JvmOverloads
    fun <D : Any, T : Any> createComboBox(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): ComboBox<T> {
        return ComboBoxBuilder<D, T>(definition, binder, config.label)
            .withProviderFactory(providerFactory)
            .build()
    }

    /**
     * Create a Select
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D      Dto
     * @param T       ComboBox Type

     * @return the Select
     */
    @JvmOverloads
    fun <D : Any, T : Any> createSelect(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): Select<T> {
        return SelectBuilder<D, T>(definition, binder, config.label)
            .withProviderFactory(providerFactory)
            .build()
    }

    /**
     * Create a ListBox
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D      Dto
     * @param T       ComboBox Type

     * @return the comboBox
     */
    @JvmOverloads
    fun <D : Any, T> createListBox(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): ListBox<T> {
        return ListBoxBuilder<D, T>(definition, binder, config.label)
            .withProviderFactory(providerFactory)
            .build()
    }

    /**
     * Create an Upload
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D        Dto
     * @return an Upload form field
     */
    fun <D> createUpload(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): Upload {
        return UploadBuilder<D>(definition, binder)
            .withMaxFileSize(maxFileSize)
            .build()
    }


    /**
     * Create a Grid
     *
     * @param binder     the binder to update
     * @param definition the column definition
     * @param config    the optional configuration parameters
     * @param D       Dto
     * @return the CheckBoxGroup
     * */
    @JvmOverloads
    fun <D : Any> createGrid(
        binder: Binder<D>,
        definition: ColumnDef,
        config: FormFieldConfiguration = FormFieldConfiguration(),
    ): InlineEditor<*> {
        return GridComponentBuilder<D, Serializable>(definition, binder)
            .withFormFieldComponentFactory(this)
            .withObjectCreator(objectCreator)
            .build()
    }

    class FormFieldConfiguration {
        var label = true

        fun disableLabel(): FormFieldConfiguration {
            label = false
            return this
        }

        fun enableLabel(): FormFieldConfiguration {
            label = true
            return this
        }
    }
}

class FormComponentBuildException(message: String, throwable: Throwable? = null) : RuntimeException(message, throwable)

