package com.qualidify.metadata.gui.components.dialogs

import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.gui.components.DataCrud
import com.qualidify.metadata.gui.components.crud.InternalCrud
import com.qualidify.metadata.gui.components.factory.FormFieldComponentFactory
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.ObjectConverter
import com.qualidify.metadata.service.ObjectCreator
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.ObjectMap
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import com.vaadin.flow.data.binder.Binder
import java.io.Serializable

/**
 * A Dialog requesting specific information for starting a CrudEdit. Specific in use
 * when the dto (of Type T) has a [com.qualidify.model.annotations.CreationStrategyPattern] annotated
 * on his not-empty constructor
 * @param <T> The Type of the class where extra information is needed
 *
 * @author Marcel Pot
 * @since 1.0
 */
class ObjectCreationWizard<T : Serializable>(
    private val dataCrud: DataCrud<T>,
    private val formFieldComponentFactory: FormFieldComponentFactory,
    private val objectCreator: ObjectCreator,
) :
    DialogTemplate("information-request") {

    private val binder: Binder<ObjectMap> = Binder()
    private val createCommandType: Class<*>

    /**
     *
     * Get an instance of an information request dialog, to retrieve information before the
     * real adding of an instance can be started
     * @param crud The crud where edited on
     * @param objectCreator an objectCreator
     */
    init {
        initialize()

        createCommandType = Metadata.getCreationCommand(dataCrud.beanType!!)!!

        val columnDefinitions = Metadata.getColumns(createCommandType)

        addColumnDefinitions(columnDefinitions)
        this.add(HorizontalLayout(cancelButton(), proceedButton()))

    }

    private fun addColumnDefinitions(columnDefinitions: List<ColumnDef>) {
        val bean = ObjectMap()
        columnDefinitions.asSequence()
            .forEach { bean[it.name] = "" }
        binder.setBean(bean)
        for (columnDef in columnDefinitions) {
            this.add(formFieldComponentFactory.createInputField(binder, columnDef))
        }
    }

    private fun cancelButton(): Button {
        val cancelButton = Button("Cancel") {
            binder.bean = null
            close()
        }
        cancelButton.setId("cancel")
        return cancelButton
    }

    private fun proceedButton(): Button {
        val okButton = Button("Proceed") {
            binder.validate()
            close()

            val command = ObjectConverter.convertToObject(binder.bean, createCommandType)
            val newInstance = objectCreator.create(dataCrud.beanType!!, command)

            if (CreationStrategyPattern.OBJECT_CONSTRUCTOR == objectCreator.getCreationStrategy(dataCrud.beanType!!)?.value) {
                dataCrud.edit(newInstance, InternalCrud.EditMode.NEW_ITEM)
            } else {
                dataCrud.getGrid().dataProvider?.refreshAll()
            }
        }
        okButton.setId("proceed")
        okButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        return okButton
    }

    override fun initialize() {
        dataCrud.saveButton.setId("save-button")
        dataCrud.cancelButton.setId("cancel-button")
        dataCrud.deleteButton.setId("delete-button")

        isCloseOnEsc = true
        isCloseOnOutsideClick = false
        isModal = true
        setId("information-request")
    }
}