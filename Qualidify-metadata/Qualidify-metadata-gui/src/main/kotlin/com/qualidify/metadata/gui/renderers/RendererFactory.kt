package com.qualidify.metadata.gui.renderers

import com.qualidify.metadata.gui.components.factory.FieldGateFactory
import com.vaadin.flow.data.renderer.Renderer
import java.io.Serializable

/**
 * Factory for Renderers
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface RendererFactory<T : Serializable, F> {

    /**
     * Create a renderer based on an object and a fieldname
     * @param item the object to read
     * @param field the field of the object to read
     * @param attributes the attributes needed for the rendering
     * @param T the type of the object
     * @param F the type of the field
     * @return a Renderer
     */
    fun create(item: T, field: String, attributes: Map<String, Any?> = emptyMap()): Renderer<T> {
        return create(attributes = attributes) { FieldGateFactory.getValue<T, F>(item, field) }
    }

    /**
     * Create a renderer based on a provider lambda
     * @param attributes the attributes needed for the rendering
     * @param provider the lambda translate the type F of an object T
     * @param T the type of the object
     * @param F the type of the field
     * @return a Renderer
     */
    fun create(attributes: Map<String, Any?>, provider: (T) -> F): Renderer<T>
}