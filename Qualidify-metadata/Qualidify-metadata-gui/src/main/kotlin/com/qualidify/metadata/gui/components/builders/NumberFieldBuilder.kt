package com.qualidify.metadata.gui.components.builders

import com.qualidify.metadata.annotations.types.AttributeConstants
import com.qualidify.metadata.service.dto.ColumnDef
import com.vaadin.flow.component.textfield.NumberField
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.validator.DoubleRangeValidator

/**
 * A builder for Number Fields
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param definition the columnDefinition
 * @param binder the binder
 * @param enableLabel indicator if label needs to be shown
 * @param D the type of the DTO
 */
class NumberFieldBuilder<D : Any>(
    definition: ColumnDef,
    binder: Binder<D>? = null,
    enableLabel: Boolean = true,
) : AbstractComponentFieldBuilder<D, NumberField, Double>(definition, binder, enableLabel) {

    override val component: NumberField = NumberField()

    override fun configureComponent(definition: ColumnDef, enableLabel: Boolean) {
        component.setId(definition.id ?: ("numberfield" + "-" + definition.name.lowercase()))
        component.label = if (enableLabel) definition.label ?: definition.name else ""

        definition.getStringAttribute(AttributeConstants.TITLE)?.let {
            component.title = it
        }
        definition.getBooleanAttribute(AttributeConstants.IS_REQUIRED)?.let {
            component.isRequiredIndicatorVisible = it
            if (it) {
                addRequiredFieldValidators()
            }
        }

        val max = definition.getDoubleAttribute(AttributeConstants.MAX)
        val min = definition.getDoubleAttribute(AttributeConstants.MIN)
        if (min != null || max != null) {
            min?.let { component.max = it }
            max?.let { component.min = it }
            addValidator(DoubleRangeValidator("This field is not within range of ($min, $max)!", min, max))
        }
        definition.getBooleanAttribute(AttributeConstants.USE_DEFAULT_VALUE)?.let {
            val defaultValue = definition.getDoubleAttribute(AttributeConstants.DEFAULT_VALUE)
            component.value = defaultValue
        }
        definition.getDoubleAttribute(AttributeConstants.STEP_VALUE)?.let {
            component.step = it
        }
        definition.getStringAttribute(AttributeConstants.HELPER_TEXT)?.let {
            component.helperText = it
        }
        component.isReadOnly = !definition.editable
    }
}