package com.qualidify.metadata.gui.components.dialogs

import com.vaadin.flow.component.html.Image

/**
 * For displaying images
 *
 * @author Marcel Pot
 * @since 1.0
 */
class ImageDialog(val image: Image) : DialogTemplate("image-dialog") {
    init {
        initialize()
        this.isCloseOnEsc = true
        add(image)
    }
}