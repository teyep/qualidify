package com.qualidify.metadata.gui.components.crud

import com.vaadin.flow.internal.JsonSerializer
import elemental.json.JsonFactory
import elemental.json.JsonValue
import elemental.json.impl.JreJsonFactory
import org.apache.commons.io.IOUtils
import java.io.IOException
import java.io.Serializable
import java.nio.charset.StandardCharsets

class InternalCrudI18n : Serializable {
    /**
     * Gets the new button and editor title text
     *
     * @return the new button and editor title text
     */
    /**
     * Sets the new button and editor title text
     *
     * @param newItem
     * the new button and editor title text
     */
    var newItem: String? = null
    /**
     * Gets the edit editor title text
     *
     * @return the edit editor title text
     */
    /**
     * Sets the edit editor title text
     *
     * @param editItem
     * the edit editor title text
     */
    var editItem: String? = null
    /**
     * Gets the save button text
     *
     * @return the save button text
     */
    /**
     * Sets the save button text
     *
     * @param saveItem
     * the save button text
     */
    var saveItem: String? = null
    /**
     * Gets the delete button text
     *
     * @return the delete button text
     */
    /**
     * Sets the delete button text
     *
     * @param deleteItem
     * the delete button text
     */
    var deleteItem: String? = null
    /**
     * Gets the cancel button text
     *
     * @return the cancel button text
     */
    /**
     * Sets the cancel button text
     *
     * @param cancel
     * the cancel button text
     */
    var cancel: String? = null
    /**
     * Gets the edit button aria label
     *
     * @return the edit button aria label
     */
    /**
     * Sets the edit button aria label
     *
     * @param editLabel
     * the edit button aria label
     */
    var editLabel: String? = null
    /**
     * Gets the confirmation dialogs
     *
     * @return the confirmation dialogs
     */
    /**
     * Sets the confirmation dialogs
     *
     * @param confirm
     * the confirmation dialogs
     */
    var confirm: Confirmations? = null

    override fun toString(): String {
        return ("CrudI18n{" + "newItem='" + newItem + '\'' + ", editItem='"
                + editItem + '\'' + ", saveItem='" + saveItem + '\''
                + ", deleteItem='" + deleteItem + '\'' + ", cancel='" + cancel
                + '\'' + ", editLabel='" + editLabel + '\'' + ", confirm="
                + confirm + '}')
    }

    /**
     * The confirmation dialogs used in the component
     */
    class Confirmations : Serializable {
        /**
         * Gets the delete confirmation dialog
         *
         * @return the delete confirmation dialog
         */
        /**
         * Sets the delete confirmation dialog
         *
         * @param delete
         * the delete confirmation dialog
         */
        var delete: Confirmation? = null
        /**
         * Gets the cancel confirmation dialog
         *
         * @return the cancel confirmation dialog
         */
        /**
         * Sets the cancel confirmation dialog
         *
         * @param cancel
         * the cancel confirmation dialog
         */
        var cancel: Confirmation? = null

        override fun toString(): String {
            return ("Confirmations{" + "delete=" + delete + ", cancel=" + cancel
                    + '}')
        }

        /**
         * Represents texts in the confirmation dialogs
         */
        class Confirmation : Serializable {
            /**
             * Gets the main content in a dialog
             *
             * @return the main content
             */
            /**
             * Sets the main content in a dialog
             *
             * @param content
             * the main content
             */
            var content: String? = null
            /**
             * Gets the confirmation options in a dialog
             *
             * @return the confirmation options
             */
            /**
             * Sets the confirmation options in a dialog
             *
             * @param button
             * the confirmation options
             */
            var button: Button? = null
            /**
             * Gets the title on a dialog
             *
             * @return the title
             */
            /**
             * Sets the title on a dialog
             *
             * @param title
             * the title
             */
            var title: String? = null

            override fun toString(): String {
                return (javaClass.simpleName + "{" + "content='" + content
                        + '\'' + ", confirmationOptions=" + button + ", title='"
                        + title + '\'' + '}')
            }

            /**
             * The confirmation options on a dialog
             */
            class Button : Serializable {
                /**
                 * Gets the confirm text
                 *
                 * @return the confirm text
                 */
                /**
                 * Sets the confirm text
                 *
                 * @param confirm
                 * the confirm text
                 */
                var confirm: String? = null
                /**
                 * Gets the dismiss text
                 *
                 * @return the dismiss text
                 */
                /**
                 * Sets the dismiss text
                 *
                 * @param dismiss
                 * the dismiss text
                 */
                var dismiss: String? = null

                override fun toString(): String {
                    return ("Button{" + "confirm='" + confirm + '\''
                            + ", dismiss='" + dismiss + '\'' + '}')
                }
            }
        }
    }

    companion object {
        private var DEFAULT_I18N: JsonValue? = null

        init {
            try {
                val JSON_FACTORY: JsonFactory = JreJsonFactory()
                DEFAULT_I18N = JSON_FACTORY.parse(
                    IOUtils.toString(
                        InternalCrudI18n::class.java.getResource("i18n.json"),
                        StandardCharsets.UTF_8
                    )
                )
            } catch (e: IOException) {
                throw IllegalStateException(
                    "Cannot find the default i18n configuration"
                )
            }
        }

        /**
         * Creates a new instance with the default messages
         *
         * @return a new instance with the default messages
         */
        fun createDefault(): InternalCrudI18n {
            return JsonSerializer.toObject(InternalCrudI18n::class.java, DEFAULT_I18N)
        }
    }
}
