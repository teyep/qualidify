package com.qualidify.metadata.gui.providers

import com.vaadin.flow.component.ItemLabelGenerator
import com.vaadin.flow.data.provider.AbstractDataProvider
import com.vaadin.flow.data.provider.DataProvider
import com.vaadin.flow.data.provider.ListDataProvider
import com.vaadin.flow.data.provider.Query
import com.vaadin.flow.function.SerializablePredicate
import java.io.Serializable
import java.util.stream.Stream
import kotlin.streams.toList

/**
 * An interface for Item Providing Collections for frontend purposes
 * @param T the item type
 * @param F the filter
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface ItemProvider<T, F : Any> : DataProvider<T, F>, ItemLabelGenerator<T> {

    /**
     * Fetch all the data and return it into a ListDataProvider
     * @return a ListDataProvider
     */
    fun toListDataProvider(): ListDataProvider<T> {
        return toListDataProvider(Query());
    }

    /**
     * Fetch all the data and return it into a ListDataProvider
     * @param query to tweak the behaviour of the data
     * @return a ListDataProvider
     */
    fun toListDataProvider(query: Query<T, F>): ListDataProvider<T> {
        return ListDataProvider(fetch(query).toList())
    }
}

/**
 * An implementation of the [ItemProvider] Interface specific for Collections
 * @param T the item type
 *
 * @author Marcel Pot
 * @since 1.0
 */
open class CollectionItemProvider<T : Serializable>(
    val collection: Collection<T>,
    val filter: SerializablePredicate<T>? = null,
) : ItemProvider<T, SerializablePredicate<T>>,
    AbstractDataProvider<T, SerializablePredicate<T>>() {

    override fun isInMemory() = true

    override fun size(query: Query<T, SerializablePredicate<T>>): Int {
        return getFilteredDataStream(query).count().toInt()
    }

    override fun fetch(query: Query<T, SerializablePredicate<T>>): Stream<T> {
        return getFilteredDataStream(query)
    }

    private fun getFilteredDataStream(query: Query<T, SerializablePredicate<T>>): Stream<T> {
        var stream = collection.stream()
        filter?.run { stream = stream.filter(this) }
        return query.filter.map { stream.filter(it) }.orElse(stream)
    }

    override fun apply(item: T) = item.toString()
}

/**
 * An implementation of the [ItemProvider] Interface specific for Collections that needs to be retrieved
 * @param T the item type
 *
 * @author Marcel Pot
 * @since 1.0
 */
abstract class RetrieveCollectionItemProvider<T : Serializable>(
    val filter: SerializablePredicate<T>? = null,
) : ItemProvider<T, SerializablePredicate<T>>,
    AbstractDataProvider<T, SerializablePredicate<T>>() {

    override fun isInMemory() = true

    override fun size(query: Query<T, SerializablePredicate<T>>): Int {
        return getFilteredDataStream(query).count().toInt()
    }

    override fun fetch(query: Query<T, SerializablePredicate<T>>): Stream<T> {
        return getFilteredDataStream(query)
    }

    private fun getFilteredDataStream(query: Query<T, SerializablePredicate<T>>): Stream<T> {
        val stream = retrieve().stream()
        filter?.let { stream.filter(it) }
        return query.filter.map { stream.filter(it) }.orElse(stream)
    }

    override fun apply(item: T) = item.toString()

    /**
     * Retrieve the data
     * @return a collection with the data
     */
    abstract fun retrieve(): Collection<T>
}

/**
 * A specific form of the [ItemProvider] interface constructed for Enumerations
 * @param E the type of the enumeration
 *
 * @author Marcel Pot
 * @since 1.0
 */
abstract class EnumItemProvider<E : Enum<E>>(enumType: Class<E>, filter: SerializablePredicate<E>? = null) :
    CollectionItemProvider<E>(listOf(*enumType.enumConstants), filter)
