package com.qualidify.metadata.gui.providers

import java.io.Serializable

/**
 * An interface for default CUD (Create, Update and Delete) actions (= commands)
 * @param <D> The Type of the object to CUD
 *
 * @author Marcel Pot
</D> */
interface CudService<D : Serializable?> : Serializable {

    /**
     * Create an Object
     * @param dto the object to save
     * @return a result indicating succes or failure
     */
    fun create(dto: D): Outcome<Any>

    /**
     * Update the Object
     * @param dto the object to update
     * @return a result indicating succes or failure
     */
    fun update(dto: D): Outcome<Any>

    /**
     * Delete the Object
     * @param dto the object to delete
     * @return a result indicating succes or failure
     */
    fun delete(dto: D): Outcome<Any>
}