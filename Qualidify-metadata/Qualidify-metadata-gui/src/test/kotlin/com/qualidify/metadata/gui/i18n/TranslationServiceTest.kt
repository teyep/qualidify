package com.qualidify.metadata.gui.i18n

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.context.MessageSource
import java.util.*

/**
 * Unit test the functionality of the TranslationService
 *
 * @author Marcel Pot
 */
@ExtendWith(MockitoExtension::class)
internal class TranslationServiceTest {
    @Mock
    private val messageSource: MessageSource? = null

    @InjectMocks
    private val translationService: TranslationServiceImpl? = null

    @Test
    fun providedLocalsTest() {
        Assertions.assertThat(translationService!!.providedLocales)
            .`as`("The number of locales available in the translationservice")
            .hasSize(2)
            .contains(Locale.ENGLISH)
            .contains(LOCALE_NL)
    }

    @Test
    fun translationTest() {

        Mockito.`when`(messageSource!!.getMessage(TEST_QUESTION, arrayOf(), Locale.ENGLISH))
            .thenReturn(QUESTION_ENGLISH)

        Mockito.`when`(messageSource.getMessage(TEST_QUESTION, arrayOf(), LOCALE_NL))
            .thenReturn(QUESTION_DUTCH)

        Assertions.assertThat(translationService!!.getTranslation(TEST_QUESTION, Locale.ENGLISH))
            .`as`("I18N test question in english")
            .isEqualTo(QUESTION_ENGLISH)

        Assertions.assertThat(translationService.getTranslation(TEST_QUESTION, LOCALE_NL))
            .`as`("I18N test question in ducth")
            .isEqualTo(QUESTION_DUTCH)

        Mockito.verify(messageSource, Mockito.times(1)).getMessage(TEST_QUESTION, arrayOf(), Locale.ENGLISH)
        Mockito.verify(messageSource, Mockito.times(1)).getMessage(TEST_QUESTION, arrayOf(), LOCALE_NL)
        Mockito.verifyNoMoreInteractions(messageSource)
    }

    companion object {
        private const val TEST_QUESTION = "test.question"
        private const val QUESTION_ENGLISH = "Why should we say hello the world?"
        private const val QUESTION_DUTCH = "Waarom moeten we de wereld hallo zeggen?"

        @JvmStatic
        val LOCALE_NL = Locale("nl_NL")
    }
}