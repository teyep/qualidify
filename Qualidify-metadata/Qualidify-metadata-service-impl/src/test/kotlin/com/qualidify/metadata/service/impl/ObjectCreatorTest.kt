package com.qualidify.metadata.service.impl

import com.qualidify.metadata.annotations.CreationStrategy
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.service.ObjectFactory
import com.qualidify.metadata.service.exceptions.DtoCreationException
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.ObjectProvider
import org.springframework.context.ApplicationContext
import org.springframework.core.ResolvableType
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class ObjectCreatorTest {

    @Mock
    lateinit var objectProviderMock: ObjectProvider<ObjectFactory<FactoryConstructionClass, String>>

    @Mock
    lateinit var context: ApplicationContext

    @InjectMocks
    lateinit var creator: ObjectCreatorImpl

    @Test
    fun testObjectCreationWithoutConstructor() {

        val emptyConstructorObject = creator.create(EmptyConstructorClass::class)

        assertThat(emptyConstructorObject).isNotNull.isInstanceOf(
            EmptyConstructorClass::class.java
        )
        verifyNoInteractions(context)
    }

    @Test
    fun testObjectCreationWithEmptyConstructor() {
        val emptyConstructorObject = creator.create(EmptyConstructorClass2::class)

        assertThat(emptyConstructorObject).isNotNull.isInstanceOf(
            EmptyConstructorClass2::class.java
        )
        verifyNoInteractions(context)
    }

    @Test
    fun testObjectCreationWithSingleParameterConstructor() {

        val oneParameterConstructorObject = creator.create(OneParameterConstructorClass::class, 1)

        assertThat(oneParameterConstructorObject).isNotNull.isInstanceOf(OneParameterConstructorClass::class.java)
        assertThat(oneParameterConstructorObject.secret).isOne

        verifyNoInteractions(context)
    }

    @Test
    fun testObjectCreationWithSingleParameterConstructor_withWrongParameter() {
        assertThatThrownBy { creator.create(OneParameterConstructorClass::class, "Drieëenheid") }
            .isInstanceOf(DtoCreationException::class.java)
            .hasMessage("Dto OneParameterConstructorClass cannot be created with parameter-constructor")
        verifyNoInteractions(context)
    }

    @Test
    fun testObjectCreationWithMultipleParameterConstructor() {

        val multipleConstructorObject = creator.create(MultipleConstructorClass::class, "Of July")

        assertThat(multipleConstructorObject).isNotNull.isInstanceOf(MultipleConstructorClass::class.java)
        assertThat(multipleConstructorObject.name).isEqualTo("Of July")
        assertThat(multipleConstructorObject.order).isEqualTo(4)

        verifyNoInteractions(context)
    }


    @Test
    fun `Test Factory Pattern`() {
        `when`(context.getBeanProvider<ObjectFactory<FactoryConstructionClass, String>>(any(ResolvableType::class.java)))
            .thenReturn(objectProviderMock)
        `when`(objectProviderMock.ifAvailable)
            .thenReturn(FactoryConstructionClassFactory())

        val factoryConstructionObject = creator.create(FactoryConstructionClass::class, "TESTSTRING")

        assertThat(factoryConstructionObject).isNotNull.isInstanceOf(FactoryConstructionClass::class.java)
        assertThat(factoryConstructionObject.name).isEqualTo("TESTSTRING")
        assertThat(factoryConstructionObject.id).isNotNull()

        verify(
            context,
            times(1)
        ).getBeanProvider<ObjectFactory<FactoryConstructionClass, String>>(any(ResolvableType::class.java))
        verify(objectProviderMock, times(1)).ifAvailable
        verifyNoMoreInteractions(context)
    }

    @Test
    fun `Test Map Dto Pattern without default parameters`() {
        val defaultConstructionObject = creator.create(DefaultConstructionClass::class)

        assertThat(defaultConstructionObject).isNotNull.isInstanceOf(DefaultConstructionClass::class.java)
        assertThat(defaultConstructionObject.name).isNull()
        assertThat(defaultConstructionObject.order).isZero() // Integers are default 0

        verifyNoInteractions(context)
    }

    @Test
    fun `Test Map Dto Pattern with default parameters`() {

        val defaultConstructionWithDefaultParameterObject =
            creator.create(DefaultConstructionWithDefaultParameterClass::class)

        assertThat(defaultConstructionWithDefaultParameterObject).isNotNull.isInstanceOf(
            DefaultConstructionWithDefaultParameterClass::class.java
        )
        assertThat(defaultConstructionWithDefaultParameterObject.month).isEqualTo("July") //default for empty constructor
        assertThat(defaultConstructionWithDefaultParameterObject.day).isEqualTo(4)

        verifyNoInteractions(context)
    }
}

@CreationStrategy(CreationStrategyPattern.EMPTY_CONSTRUCTOR)
internal class EmptyConstructorClass

@CreationStrategy(CreationStrategyPattern.EMPTY_CONSTRUCTOR)
internal class EmptyConstructorClass2 constructor()

@CreationStrategy(CreationStrategyPattern.OBJECT_CONSTRUCTOR)
internal class OneParameterConstructorClass(val secret: Int)

@CreationStrategy(CreationStrategyPattern.OBJECT_CONSTRUCTOR, creationClass = String::class)
internal class MultipleConstructorClass {
    val name: String
    var order: Int
        private set

    constructor() : this("Mars") {}


    constructor(name: String) {
        this.name = name
        order = 4
    }

    constructor(name: String, order: Int) {
        this.name = name
        this.order = order
    }
}

internal class DefaultConstructionClass(
    val name: String,
    var order: Int,
)

internal class DefaultConstructionWithDefaultParameterClass(
    val month: String = "July",
    var day: Int = 4,
)

@CreationStrategy(CreationStrategyPattern.FACTORY, creationClass = String::class)
internal class FactoryConstructionClass(
    val id: UUID,
    val name: String,
)

internal class FactoryConstructionClassFactory : ObjectFactory<FactoryConstructionClass, String> {
    override fun create(command: String): FactoryConstructionClass {
        return FactoryConstructionClass(UUID.randomUUID(), command)
    }
}

