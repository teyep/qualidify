package com.qualidify.metadata.service.testdata

import com.qualidify.metadata.annotations.CreationStrategy
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.annotations.DtoCommand

@CreationStrategy(CreationStrategyPattern.OBJECT_CONSTRUCTOR, creationClass = Virus.CreateVirusCommand::class)
data class Virus(
    val name: String,
    val duplicationTime: Int,


    ) {

    constructor(command: CreateVirusCommand) : this(command.name, command.duplicationTime, command.description)

    constructor(name: String, duplicationTime: Int, description: String) : this(name, duplicationTime) {
        println(description)
    }

    constructor(name: String) : this(name, 1)

    @DtoCommand("CreateVirusCommand")
    data class CreateVirusCommand(
        var name: String,
        var duplicationTime: Int,
        var description: String,
    )
}
