package com.qualidify.metadata.service.testdata

import com.qualidify.core.identity.HasIdentity

open class Plant(val name: String) : HasIdentity {

    override fun identity(): Array<Any> = arrayOf(name)

}

class Pinophyta(name: String, val height: Int? = null) : Plant(name)