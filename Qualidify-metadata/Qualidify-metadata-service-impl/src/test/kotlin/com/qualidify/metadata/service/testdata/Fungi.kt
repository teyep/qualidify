package com.qualidify.metadata.service.testdata

import com.qualidify.metadata.annotations.DtoMetadata


data class Fungi(
    val name: String,
    val isToxic: Boolean = false,
)

@DtoMetadata(singular = "Fungi", plural = "Fungis")
data class FungiDto(
    val name: String,
    val isToxic: Boolean = false,
)