package com.qualidify.metadata.service.impl

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.metadata.service.dto.DtoDef
import com.qualidify.metadata.service.exceptions.MetadataValidationException
import com.qualidify.metadata.service.testdata.Pinophyta
import com.qualidify.metadata.service.testdata.Plant
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class MetadataValidationServiceTest {

    @Mock
    lateinit var metadataService: MetadataService


    @InjectMocks
    lateinit var fixture: MetadataValidationServiceImpl

    @Test
    fun `Check all mandatory CustomFields are present with exact matching fields`() {

        `when`(metadataService.getCustomFieldsForReference(Reference.USER))
            .thenReturn(customDefinitions(true))

        val map = mapOf<String, Any>("WIDTH" to 800, "HEIGHT" to 600)

        fixture.checkAllMandatoryCustomFieldsArePresent(Reference.USER, map)

        verify(metadataService, times(1)).getCustomFieldsForReference(Reference.USER)
        verifyNoMoreInteractions(metadataService)

    }

    @Test
    fun `Check all mandatory CustomFields are present with missing field`() {

        `when`(metadataService.getCustomFieldsForReference(Reference.USER))
            .thenReturn(customDefinitions(true))

        val map = mapOf<String, Any>("WIDTH" to 800)

        assertThatThrownBy {
            fixture.checkAllMandatoryCustomFieldsArePresent(Reference.USER, map)
        }.isInstanceOf(MetadataValidationException::class.java)
            .hasMessage("Not all mandatory fields are present")

        verify(metadataService, times(1)).getCustomFieldsForReference(Reference.USER)
        verifyNoMoreInteractions(metadataService)
    }

    @Test
    fun `Check all mandatory CustomFields are present with extra fields`() {

        `when`(metadataService.getCustomFieldsForReference(Reference.USER))
            .thenReturn(customDefinitions(true))

        val map = mapOf<String, Any>("WIDTH" to 800, "HEIGHT" to 600, "COLOR" to "Green", "WATERMARK" to false)

        fixture.checkAllMandatoryCustomFieldsArePresent(Reference.USER, map)

        verify(metadataService, times(1)).getCustomFieldsForReference(Reference.USER)
        verifyNoMoreInteractions(metadataService)

    }

    @Test
    fun `Check all mandatory CustomFields are present with no mandatory fields`() {

        `when`(metadataService.getCustomFieldsForReference(Reference.USER))
            .thenReturn(customDefinitions(false))

        val map = mapOf<String, Any>("WIDTH" to 800, "HEIGHT" to 600, "COLOR" to "Green")
        fixture.checkAllMandatoryCustomFieldsArePresent(Reference.USER, map)

        verify(metadataService, times(1)).getCustomFieldsForReference(Reference.USER)
        verifyNoMoreInteractions(metadataService)
    }

    @Test
    fun `Check all CustomFields are allowed with exact matching fields`() {

        `when`(metadataService.getCustomFieldsForReference(Reference.USER))
            .thenReturn(customDefinitions(true))

        val map = mapOf<String, Any>("WIDTH" to 800, "HEIGHT" to 600)
        fixture.checkAllCustomFieldsAreAllowed(Reference.USER, map)

        verify(metadataService, times(1)).getCustomFieldsForReference(Reference.USER)
        verifyNoMoreInteractions(metadataService)

    }

    @Test
    fun `Check all CustomFields are allowed with missing fields`() {

        `when`(metadataService.getCustomFieldsForReference(Reference.USER))
            .thenReturn(customDefinitions(true))

        val map = mapOf<String, Any>("HEIGHT" to 600)
        fixture.checkAllCustomFieldsAreAllowed(Reference.USER, map)


        verify(metadataService, times(1)).getCustomFieldsForReference(Reference.USER)
        verifyNoMoreInteractions(metadataService)

    }

    @Test
    fun `Check all CustomFields are allowed with extra fields`() {

        `when`(metadataService.getCustomFieldsForReference(Reference.USER))
            .thenReturn(customDefinitions(true))

        val map = mapOf<String, Any>("HEIGHT" to 600, "WIDTH" to 800, "COLOR" to "Purple")
        assertThatThrownBy {
            fixture.checkAllCustomFieldsAreAllowed(Reference.USER, map)
        }.isInstanceOf(MetadataValidationException::class.java)
            .hasMessage("Not all custom fields are allowed")

        verify(metadataService, times(1)).getCustomFieldsForReference(Reference.USER)
        verifyNoMoreInteractions(metadataService)

    }

    @Test
    fun `Validation Test`() {

        val plant = Plant("Taraxacum officinale")

        `when`(metadataService.getColumns("Plant"))
            .thenReturn(PlantDtoDefinition().columns)

        fixture.validate("Plant", plant)

        verify(metadataService, times(1)).getColumns("Plant")
    }

    @Test
    fun `Validation Test with null value`() {

        val juniperus = Pinophyta("Juniperus communis")

        `when`(metadataService.getColumns("Pinophyta"))
            .thenReturn(PlantDtoDefinition(mapOf("height" to DataType.INTEGER)).columns)
        assertThatThrownBy {
            fixture.validate("Pinophyta", juniperus)
        }.isInstanceOf(MetadataValidationException::class.java)
            .hasMessage("DataType is not of the right type.")

        verify(metadataService, times(1)).getColumns("Pinophyta")

    }

    private fun customDefinitions(mandatory: Boolean): List<CustomFieldDef> {
        val customFields: MutableList<CustomFieldDef> = ArrayList()
        customFields.add(
            CustomFieldDef(
                id = UUID.randomUUID(),
                reference = Reference.USER,
                field = "WIDTH",
                description = "Width",
                dataType = DataType.INTEGER,
                validator = "",
                mandatory = mandatory,
                active = true
            )
        )
        customFields.add(
            CustomFieldDef(
                id = UUID.randomUUID(),
                reference = Reference.USER,
                field = "HEIGHT",
                description = "Height",
                dataType = DataType.INTEGER,
                validator = "",
                mandatory = mandatory,
                active = true
            )
        )
        return customFields
    }

    private fun PlantDtoDefinition(extraColumns: Map<String, DataType> = emptyMap()) = DtoDef(
        name = "Plant",
        pluralName = "Plants",
        mnemonic = "Plant"

    ).also {
        it.addColumn(
            ColumnDef(
                name = "name",
                description = "name",
                dataType = DataType.STRING,
            )
        )
        extraColumns.entries
            .map { extraColumn ->
                ColumnDef(
                    name = extraColumn.key,
                    description = extraColumn.key,
                    dataType = extraColumn.value
                )
            }.forEach { def ->
                it.addColumn(def)
            }
    }


}