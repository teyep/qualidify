package com.qualidify.metadata.service.impl.harvest

import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.renderer.ImageRenderer
import com.qualidify.metadata.annotations.types.CustomType
import com.qualidify.metadata.service.dto.ActionInfo
import com.qualidify.metadata.service.dto.RendererInfo
import com.qualidify.metadata.service.dto.SegmentInfo
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test

internal class MetadataStructureUtilsTest {


    @Test
    fun getCreationStrategyPattern() {
        val creationStrategyPattern_1 =
            MetadataStructureUtils.getCreationStrategyPattern(StructureTestObject_1::class)
        assertThat(creationStrategyPattern_1)
            .`as`("Informationrequest Creation Strategy Pattern expected")
            .isEqualTo(CreationStrategyPattern.OBJECT_CONSTRUCTOR)

        val creationStrategyPattern_2 =
            MetadataStructureUtils.getCreationStrategyPattern(StructureTestObject_2::class)
        assertThat(creationStrategyPattern_2)
            .`as`("Default pattern expected")
            .isEqualTo(CreationStrategyPattern.MAP_DTO)
    }

    @Test
    fun determineClassTypeOfField() {
        val fieldType_1 =
            MetadataStructureUtils.determineClassTypeOfField(StructureTestObject_1::class, "second")
        assertThat(fieldType_1).`as`("Expected fieldtype of StructureTestObject_1.second is String")
            .isEqualTo(String::class)

        val fieldType_2 =
            MetadataStructureUtils.determineClassTypeOfField(StructureTestObject_2::class, "third")
        assertThat(fieldType_2).`as`("Expected fieldtype of StructureTestObject_2.third is List")
            .isEqualTo(List::class)
    }


    @Test
    fun determineGenericClassTypeOfCollection() {
        assertThatThrownBy {
            MetadataStructureUtils.determineGenericClassTypeOfCollection(StructureTestObject_1::class, "third")
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Cannot determine a generic type of a non-collection field")

        val fieldType_2 =
            MetadataStructureUtils.determineGenericClassTypeOfCollection(StructureTestObject_2::class, "third")
        assertThat(fieldType_2).`as`("Expected generic fieldtype of StructureTestObject_2.second is Boolean")
            .isEqualTo(java.lang.Boolean::class)
    }

    @Test
    fun determineCustomField() {
        val customField = MetadataStructureUtils.determineCustomField(StructureTestObject_3::class)
        assertThat(customField).`as`("CustomField should be found").isNotNull
        assertThat(customField?.name).`as`("CustomField should be found").isEqualTo("third")

        val notExistingCustomField = MetadataStructureUtils.determineCustomField(StructureTestObject_1::class)
        assertThat(notExistingCustomField).`as`("CustomField should not be found").isNull()
    }

    @Test
    fun `detect Action Annotation on object`() {
        val actions_1 = MetadataStructureUtils.determineActions(StructureTestObject_1::class)
        assertThat(actions_1).`as`("No Actions found on StructureTestObject_1").isEmpty()

        val actions_2 = MetadataStructureUtils.determineActions(StructureTestObject_2::class)
        assertThat(actions_2).`as`("Two Actions should be available in the right order")
            .hasSize(2)
            .containsExactly(
                ActionInfo("First", "FirstAction", Position.END, 1),
                ActionInfo("Second", "SecondAction", Position.END, 2)
            )

        val actions_3 = MetadataStructureUtils.determineActions(StructureTestObject_3::class)
        assertThat(actions_3).`as`("One Action should be available")
            .hasSize(1)
            .contains(
                ActionInfo("Touch", "Touche", Position.END, 2)
            )
    }

    @Test
    fun `detect BatchAction Annotation on object`() {
        val actions_1 = MetadataStructureUtils.determineBatchActions(StructureTestObject_1::class)
        assertThat(actions_1).`as`("No BatchAction found on StructureTestObject_1").isEmpty()

        val actions_2 = MetadataStructureUtils.determineBatchActions(StructureTestObject_2::class)
        assertThat(actions_2).`as`("One BatchAction should be available")
            .hasSize(2)
            .contains(
                ActionInfo("Delete All", "DeleteAll", Position.END, 1),
                ActionInfo("Refresh", "Refresh", Position.END, 2)
            )

        val actions_3 = MetadataStructureUtils.determineBatchActions(StructureTestObject_3::class)
        assertThat(actions_3).`as`("No BatchAction  should be available")
            .isEmpty()
    }


    @Test
    fun `detect Renderer on field`() {
        val noRenderer = MetadataStructureUtils.determineRenderer(StructureTestObject_2::class, "first")
        assertThat(noRenderer).`as`("No Renderer active on first field").isNull()

        val imageRenderer = MetadataStructureUtils.determineRenderer(StructureTestObject_2::class, "second")
        assertThat(imageRenderer).`as`("ImagerRenderer active on second field")
            .isInstanceOf(RendererInfo::class.java)
            .isEqualTo(RendererInfo("IMAGE", mapOf("width" to "100%", "height" to "100%")))
    }

    @Test
    fun `detect Segments on class`() {
        val noSegmentDefinitionsAvailable = MetadataStructureUtils.determineSegmentation(StructureTestObject_1::class)

        assertThat(noSegmentDefinitionsAvailable).isEqualTo(emptyList<SegmentInfo>())

        val segmentDefinitionAvailable = MetadataStructureUtils.determineSegmentation(StructureTestObject_2::class)

        assertThat(segmentDefinitionAvailable).isEqualTo(
            listOf(
                SegmentInfo("DEFAULT", "Default Description"),
                SegmentInfo("SPECIAL", "")
            )
        )
    }


}

@CreationStrategy(
    value = CreationStrategyPattern.OBJECT_CONSTRUCTOR,
    creationClass = String::class
)
data class StructureTestObject_1(
    val first: String,
    val second: String,
    val third: String,
) {
    constructor() : this("", "", "")
    constructor(first: String) : this(first, "", "")
}

@BatchActions(
    [
        Action("Refresh", "Refresh", order = 2),
        Action("Delete All", "DeleteAll", order = 1)
    ]
)
@Actions(
    [
        Action("Second", "SecondAction", order = 2),
        Action("First", "FirstAction", order = 1)
    ]
)
@Segments(
    [
        SegmentDefinition("DEFAULT", "Default Description"),
        SegmentDefinition("SPECIAL")
    ]
)
data class StructureTestObject_2
    (
    val first: String,
    @ImageRenderer
    val second: ByteArray,
    val third: List<Boolean>,
) {
    constructor() : this("", "".toByteArray(), emptyList())
    constructor(first: String) : this(first, "".toByteArray(), emptyList())
}


@Action("Touch", "Touche", order = 2)
data class StructureTestObject_3
    (
    val first: String,
    val second: String,
    @CustomType(Reference.USER)
    val third: Map<String, String>,
) {
    constructor() : this("", "", emptyMap())
    constructor(first: String) : this(first, "", emptyMap())
}