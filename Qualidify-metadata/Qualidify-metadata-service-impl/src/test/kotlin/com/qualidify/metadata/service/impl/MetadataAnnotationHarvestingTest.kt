package com.qualidify.metadata.service.impl

import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.Metadata.Companion.getColumns
import com.qualidify.metadata.service.Metadata.Companion.isAdditionEnabled
import com.qualidify.metadata.service.Metadata.Companion.isDeleteEnabled
import com.qualidify.metadata.service.Metadata.Companion.isFilteringEnabled
import com.qualidify.metadata.service.Metadata.Companion.isUpdateEnabled
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.MetadataValidationService
import com.qualidify.metadata.service.MnemonicRegistry
import com.qualidify.metadata.service.impl.harvest.MetadataHarvester
import com.qualidify.metadata.service.impl.repositories.*
import com.qualidify.metadata.service.testdata.FungiDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.SpyBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional

/**
 * Test the static MetaData supplier when setup is correct.
 *
 * @author Marcel Pot
 */
@ExtendWith(SpringExtension::class)
@EntityScan(basePackages = ["com.qualidify.model.entities"])
@SpringBootTest
@Transactional
internal open class MetadataAnnotationHarvestingTest {
    @SpyBean
    lateinit var metadataHarvester: MetadataHarvester

    @Autowired
    lateinit var customFieldRepository: CustomFieldRepository

    @Autowired
    lateinit var mnemonicRegistry: MnemonicRegistry

    @Autowired
    lateinit var metadataObjectRepository: MetadataObjectRepository

    var metadataValidationService: MetadataValidationService? = null

    var metadataService: MetadataService? = null

    @BeforeEach
    fun setup() {
        metadataService = MetadataServiceImpl(metadataObjectRepository, customFieldRepository, mnemonicRegistry)
        metadataValidationService = MetadataValidationServiceImpl(metadataService!!)
        Metadata(metadataService!!, metadataValidationService!!)
    }

    @Test
    fun testDtoMetaData() {
        assertThat(isAdditionEnabled("Fungi")).isTrue
        assertThat(isDeleteEnabled("Fungi")).isTrue
        assertThat(isUpdateEnabled("Fungi")).isTrue
        assertThat(
            isFilteringEnabled(
                "Fungi"
            )
        ).isTrue
        assertThat(getColumns("Fungi").size).isEqualTo(2)

        verify(metadataHarvester, Mockito.times(1))!!.harvest(FungiDto::class)
    }


}