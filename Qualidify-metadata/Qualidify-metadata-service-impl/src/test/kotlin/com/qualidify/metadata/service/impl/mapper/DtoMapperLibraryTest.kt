package com.qualidify.metadata.service.impl.mapper

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.service.DtoMapperLibrary
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.metadata.service.exceptions.EntityMapperNotFoundException
import com.qualidify.metadata.service.testdata.TestDataFactory
import com.qualidify.model.entities.metadata.CustomField
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import java.io.Serializable
import java.util.*


internal class DtoMapperLibraryTest {

    val mappers: List<EntityDtoMapper<*, out HasIdentity>> = listOf(
        CustomFieldDtoMapper()
    )
    val fixture: DtoMapperLibrary = DtoMapperLibraryImpl(mappers)

    @Test
    fun `Test DtoMapper Library with enough mappers and converting to entity`() {
        val id = UUID.randomUUID()
        val customFieldDto = TestDataFactory.getCustomFieldDefinitionDto(id)

        val entity = fixture.convertToEntity<CustomField, CustomFieldDef>(customFieldDto)

        assertThat(entity).`as`("Check the instance of the mapped instance")
            .isInstanceOf(CustomField::class.java)
            .`as`("Check the data of the mapped instance")
            .isEqualTo(
                TestDataFactory.getCustomFieldEntity(id)
            )
    }

    @Test
    fun `Test DtoMapper Library with enough mappers and converting to dto`() {
        val id = UUID.randomUUID()
        val customFieldEntity = TestDataFactory.getCustomFieldEntity(id)

        val entity = fixture.convertToDto<CustomField, CustomFieldDef>(customFieldEntity)

        assertThat(entity).`as`("Check the instance of the mapped instance")
            .isInstanceOf(CustomFieldDef::class.java)
            .`as`("Check the data of the mapped instance")
            .isEqualTo(
                TestDataFactory.getCustomFieldDefinitionDto(id)
            )
    }

    @Test
    fun `Test DtoMapper Library with missing mapper and converting to entity`() {
        val dto = UnknownDto()

        assertThatThrownBy {
            fixture.convertToEntity<UnknownEntity, UnknownDto>(dto)
        }.isInstanceOf(EntityMapperNotFoundException::class.java)
            .hasMessage("Entity mapper for dto class $dto not found!")
    }

    @Test
    fun `Test DtoMapper Library with missing mapper and converting to dto`() {
        val entity = UnknownEntity()

        assertThatThrownBy {
            fixture.convertToDto<UnknownEntity, UnknownDto>(entity)
        }.isInstanceOf(EntityMapperNotFoundException::class.java)
            .hasMessage("Entity mapper for entity class $entity not found!")
    }


    @Test
    fun `Test DtoMapper Library converting from POJO map to Entity with unknown mapper`() {
        val pojoMap = emptyMap<String, Any?>()
        assertThatThrownBy {
            fixture.convertFromPojoMapToEntity<UnknownEntity>("Unknown", pojoMap)
        }.isInstanceOf(EntityMapperNotFoundException::class.java)
            .hasMessage("Entity mapper for mnemonic Unknown not found!")
    }

    @Test
    fun `Test get EntityClass for a unknown Mnemonic`() {
        assertThatThrownBy {
            fixture.getEntityClassFromMnemonic("Unknown")
        }.isInstanceOf(EntityMapperNotFoundException::class.java)
            .hasMessage("Entity mapper for mnemonic Unknown not found!")
    }

    @Test
    fun `Test get DtoClass for a unknown Mnemonic`() {
        assertThatThrownBy {
            fixture.getDtoClassFromMnemonic("UnknownDto")
        }.isInstanceOf(EntityMapperNotFoundException::class.java)
            .hasMessage("Entity mapper for mnemonic UnknownDto not found!")
    }

}

internal class UnknownDto : Serializable, HasIdentity {
    override fun identity(): Array<Any> = arrayOf("id")
}

internal class UnknownEntity : Serializable