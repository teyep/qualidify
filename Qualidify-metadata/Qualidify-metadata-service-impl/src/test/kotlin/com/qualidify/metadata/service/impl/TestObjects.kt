package com.qualidify.metadata.service.impl

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.annotations.Mnemonic

@Mnemonic("movie", singular = "Movie", plural = "Movies")
data class Movie(
    var name: String,
    var year: Int,
) : HasIdentity {
    constructor() : this("", 0)

    override fun identity(): Array<Any> {
        return arrayOf(name);
    }

}

data class MovieEntity(
    var name: String,
    var year: Int,
)