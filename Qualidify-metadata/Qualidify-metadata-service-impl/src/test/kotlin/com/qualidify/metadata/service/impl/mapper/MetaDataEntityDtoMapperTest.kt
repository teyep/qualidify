package com.qualidify.metadata.service.impl.mapper

import com.qualidify.metadata.service.dto.DtoDef
import com.qualidify.metadata.service.testdata.TestDataFactory
import com.qualidify.model.entities.metadata.MetadataObject
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class MetaDataEntityDtoMapperTest {

    val mapper: MetadataEntityDtoMapper = MetadataEntityDtoMapper()

    @Test
    fun `Convert a MetadataObject Entity to a MetadataObjectDto`() {
        val entity = TestDataFactory.getMetaDataObjectEntity("mnemonic")

        val dto = mapper.convertToDto(entity)

        Assertions.assertThat(dto).`as`("Check the instance of the mapped instance")
            .isInstanceOf(DtoDef::class.java)
            .`as`("Check the data of the mapped instance")
            .isEqualTo(
                TestDataFactory.getDtoDef("mnemonic")
            )
    }

    @Test
    fun `Convert a CustomField Entity to a CustomFieldDefinitionDto`() {
        val dto = TestDataFactory.getDtoDef("mnemonix")

        val entity = mapper.convertToModel(dto)

        Assertions.assertThat(entity).`as`("Check the instance of the mapped instance")
            .isInstanceOf(MetadataObject::class.java)
            .`as`("Check the data of the mapped instance")
            .isEqualTo(
                TestDataFactory.getMetaDataObjectEntity("mnemonix")
            )
    }
}