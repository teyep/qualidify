package com.qualidify.metadata.service.testdata

import com.qualidify.metadata.annotations.Action
import com.qualidify.metadata.annotations.Actions
import com.qualidify.metadata.annotations.BatchActions
import java.util.*

@Actions(
    [Action("Feed", "Feed"),
        Action("Pat", "Pat")
    ]
)
@BatchActions(
    [Action("Milk All", "milkAll")]
)
class Animal(
    val name: Optional<String>,
    val age: Int,
    val bones: Collection<Bone>,
)

@Action("Break", "Break")
data class Bone(
    val weight: Double,
)