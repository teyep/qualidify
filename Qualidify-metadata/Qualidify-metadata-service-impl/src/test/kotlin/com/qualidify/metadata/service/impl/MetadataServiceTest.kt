package com.qualidify.metadata.service.impl

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.service.MnemonicRegistry
import com.qualidify.metadata.service.dto.*
import com.qualidify.metadata.service.impl.harvest.AnnotationHarvester
import com.qualidify.metadata.service.impl.mapper.MetadataEntityDtoMapper
import com.qualidify.metadata.service.impl.repositories.*
import com.qualidify.metadata.service.testdata.*
import com.qualidify.model.entities.metadata.CustomField
import com.qualidify.model.entities.metadata.CustomFieldId
import com.qualidify.model.entities.metadata.MetadataObject
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*
import kotlin.reflect.KClass

/**
 * Test the MetaDataService with the custom field implementation
 *
 * @author Marcel Pot
 * @since 1.0
 */
@ExtendWith(MockitoExtension::class)
internal class MetadataServiceTest {

    private val annotationHarvester = AnnotationHarvester()

    @Mock
    private lateinit var metadataRepository: MetadataObjectRepository

    @Mock
    private lateinit var customFieldRepository: CustomFieldRepository

    @Mock
    private lateinit var mnemonicRegistry: MnemonicRegistry

    @InjectMocks
    private lateinit var metaDataService: MetadataServiceImpl

    @Test
    fun `test isAddition enabled`() {
        `when`(metadataRepository.findById("Animal"))
            .thenReturn(Optional.of(metadataObject(Animal::class)))

        val isAdditionEnabled = metaDataService.isAdditionEnabled("Animal")

        assertThat(isAdditionEnabled).isTrue()

        verifyNoInteractions(customFieldRepository)
    }

    @Test
    fun `test getDtoDefinition without explicit configuration`() {
        `when`(metadataRepository.findById("Fungi"))
            .thenReturn(Optional.of(metadataObject(FungiDto::class)))

        val expectedDtoDefinition = DtoDef(
            name = "Fungi",
            pluralName = "Fungis",
            mnemonic = "Fungi"

        ).also {
            it.addColumn(
                ColumnDef(
                    name = "name",
                    description = "name",
                    dataType = DataType.STRING,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "maxLength" to "2147483647",
                        "pattern" to "",
                        "autoFocus" to "false",
                        "autoSelect" to "false",
                        "minLength" to "0",
                        "placeHolder" to "",
                        "clearButtonVisible" to "false"
                    )

                )
            )
            it.addColumn(
                ColumnDef(
                    name = "isToxic",
                    description = "isToxic",
                    dataType = DataType.BOOLEAN,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "renderer" to "",
                        "ariaLabel" to ""
                    )
                )
            )
        }


        assertThat(metaDataService.getDtoDefinition("Fungi"))
            .isInstanceOf(DtoDef::class.java)
            .isEqualTo(expectedDtoDefinition)

        assertThat(metaDataService.getSingularName("Fungi")).isEqualTo("Fungi")
        assertThat(metaDataService.getPluralName("Fungi")).isEqualTo("Fungis")
        assertThat(metaDataService.isAdditionEnabled("Fungi")).isTrue
        assertThat(metaDataService.isDeleteEnabled("Fungi")).isTrue
        assertThat(metaDataService.isUpdateEnabled("Fungi")).isTrue
        assertThat(metaDataService.isFilteringEnabled("Fungi")).isTrue
        assertThat(metaDataService.getColumns("Fungi"))
            .hasSize(2).containsAll(expectedDtoDefinition.columns)

        verify(metadataRepository, times(8))
            .findById("Fungi")

        verifyNoMoreInteractions(metadataRepository, customFieldRepository)

    }

    @Test
    fun `test getDtoDefinition with explicit configuration`() {
        `when`(metadataRepository.findById("Table"))
            .thenReturn(Optional.of(metadataObject(Table::class)))

        val expectedDtoDefinition = DtoDef(
            name = "Table",
            pluralName = "A lot of tables",
            mnemonic = "Table",
            addEnabled = false,
            deleteEnabled = false

        ).also {
            it.addColumn(
                ColumnDef(
                    name = "height",
                    description = "TableHeight",
                    dataType = DataType.INTEGER,
                    editable = false,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "min" to "-2147483648",
                        "max" to "2147483647",
                        "prefix" to "",
                        "suffix" to "",
                        "defaultValue" to "0",
                        "stepValue" to "1",
                        "useDefaultValue" to "false",
                        "helperText" to ""
                    )
                )
            )
            it.addColumn(
                ColumnDef(
                    name = "name",
                    description = "name",
                    dataType = DataType.STRING,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "maxLength" to "2147483647",
                        "pattern" to "",
                        "autoFocus" to "false",
                        "autoSelect" to "false",
                        "minLength" to "0",
                        "placeHolder" to "",
                        "clearButtonVisible" to "false"
                    )
                )
            )
            it.addColumn(
                ColumnDef(
                    name = "brand",
                    description = "brand",
                    dataType = DataType.STRING,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "maxLength" to "2147483647",
                        "pattern" to "",
                        "autoFocus" to "false",
                        "autoSelect" to "false",
                        "minLength" to "0",
                        "placeHolder" to "",
                        "clearButtonVisible" to "false"
                    )
                )
            )
            it.addColumn(
                ColumnDef(
                    name = "numberOfLegs",
                    description = "numberOfLegs",
                    dataType = DataType.INTEGER,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "min" to "-2147483648",
                        "max" to "2147483647",
                        "prefix" to "",
                        "suffix" to "",
                        "defaultValue" to "0",
                        "stepValue" to "1",
                        "useDefaultValue" to "false",
                        "helperText" to ""
                    )
                )
            )
            it.addColumn(
                ColumnDef(
                    name = "price",
                    description = "price",
                    dataType = DataType.BIGDECIMAL,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "min" to Double.MIN_VALUE.toString(),
                        "max" to Double.MAX_VALUE.toString(),
                        "prefix" to "",
                        "suffix" to "",
                        "defaultValue" to "0.0",
                        "stepValue" to "1",
                        "useDefaultValue" to "false",
                        "helperText" to ""
                    )
                )
            )

        }


        assertThat(metaDataService.getDtoDefinition("Table"))
            .isInstanceOf(DtoDef::class.java)
            .isEqualTo(expectedDtoDefinition)

        assertThat(metaDataService.getSingularName("Table")).isEqualTo("Table")
        assertThat(metaDataService.getPluralName("Table")).isEqualTo("A lot of tables")
        assertThat(metaDataService.isAdditionEnabled("Table")).isFalse()
        assertThat(metaDataService.isDeleteEnabled("Table")).isFalse()
        assertThat(metaDataService.isUpdateEnabled("Table")).isTrue
        assertThat(metaDataService.isFilteringEnabled("Table")).isTrue
        assertThat(metaDataService.getColumns("Table"))
            .hasSize(5).containsAll(expectedDtoDefinition.columns)

        verify(metadataRepository, times(8))
            .findById("Table")

        verifyNoMoreInteractions(metadataRepository, customFieldRepository)

    }

    @Test
    fun `Test getDtoDefinition with CustomFields`() {
        `when`(mnemonicRegistry.get("Drawing"))
            .thenReturn(DrawingDto::class)
        `when`(metadataRepository.findById("Drawing"))
            .thenReturn(Optional.of(metadataObject(DrawingDto::class)))
        `when`(customFieldRepository.findAllByReferenceAndActive(Reference.USER))
            .thenReturn(customDefinitions())

        val expectedDtoDefinition = DtoDef(
            name = "Drawing",
            pluralName = "Drawings",
            mnemonic = "Drawing"
        ).also {
            it.addColumn(
                ColumnDef(
                    name = "color",
                    description = "color",
                    dataType = DataType.STRING,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "maxLength" to "2147483647",
                        "pattern" to "",
                        "autoFocus" to "false",
                        "autoSelect" to "false",
                        "minLength" to "0",
                        "placeHolder" to "",
                        "clearButtonVisible" to "false"
                    )
                )
            )
            it.addColumn(
                ColumnDef(
                    name = "backgroundcolor",
                    description = "backgroundcolor",
                    dataType = DataType.STRING,
                    attributes = mapOf(
                        "id" to "",
                        "label" to "",
                        "maxLength" to "2147483647",
                        "pattern" to "",
                        "autoFocus" to "false",
                        "autoSelect" to "false",
                        "minLength" to "0",
                        "placeHolder" to "",
                        "clearButtonVisible" to "false"
                    )
                )
            )
            it.addColumn(
                ColumnDef(
                    name = "WIDTH",
                    description = "Width",
                    dataType = DataType.INTEGER,
                    attributes = mapOf(),
                    fieldAccess = ColumnDef.FieldAccess.MAP
                )
            )
            it.addColumn(
                ColumnDef(
                    name = "HEIGHT",
                    description = "Height",
                    dataType = DataType.INTEGER,
                    attributes = mapOf(),
                    fieldAccess = ColumnDef.FieldAccess.MAP
                )
            )

        }
        assertThat(metaDataService.getDtoDefinition("Drawing"))
            .isInstanceOf(DtoDef::class.java)
            .isEqualTo(expectedDtoDefinition)
        assertThat(metaDataService.getColumns("Drawing"))
            .hasSize(4).containsAll(expectedDtoDefinition.columns)

        verify(metadataRepository, times(2))
            .findById("Drawing")
        verify(customFieldRepository, times(2)).findAllByReferenceAndActive(Reference.USER)
        verifyNoMoreInteractions(metadataRepository, metadataRepository)
    }

    @Test
    fun `test get all customfields names for a reference`() {
        `when`(customFieldRepository.findAllByReference(Reference.USER)).thenReturn(customDefinitions())

        assertThat(metaDataService.getCustomFieldsForReference(Reference.USER))
            .`as`("Get all customfields for reference")
            .hasSize(2)

        verify(customFieldRepository, times(1)).findAllByReference(Reference.USER)
        verifyNoMoreInteractions(customFieldRepository)
        verifyNoInteractions(metadataRepository)

    }

    @Test
    fun `test get mnemonic list`() {
        `when`(metadataRepository.findMnemonicList()).thenReturn(
            listOf("FUNGI", "BACTERIA", "ANIMAL", "VIRUS")
        )


        val mnemonics = metaDataService.getMnemonicList()

        assertThat(mnemonics).isEqualTo(listOf("FUNGI", "BACTERIA", "ANIMAL", "VIRUS"))

        verify(metadataRepository, times(1)).findMnemonicList()
        verifyNoMoreInteractions(metadataRepository)
        verifyNoInteractions(customFieldRepository)
    }

    @Test
    fun `test get CreationStrategyPattern`() {
        `when`(mnemonicRegistry.get("Virus")).thenReturn(Virus::class)
        `when`(mnemonicRegistry.get("Fungi")).thenReturn(Fungi::class)
        val pattern = metaDataService.getCreationStrategyPattern("Virus")
        assertThat(pattern).`as`("Pattern of @CreationStrategy should be found")
            .isEqualTo(CreationStrategyPattern.OBJECT_CONSTRUCTOR)

        val noPattern = metaDataService.getCreationStrategyPattern("Fungi")

        assertThat(noPattern).`as`("Mo Pattern configured @CreationStrategy should be lead to default creation strategy (MAP_DTO)")
            .isEqualTo(CreationStrategyPattern.MAP_DTO)

        verifyNoInteractions(customFieldRepository, metadataRepository)
    }

    @Test
    fun `test Actions`() {
        `when`(mnemonicRegistry.get("Animal")).thenReturn(Animal::class)
        `when`(mnemonicRegistry.get("Bone")).thenReturn(Bone::class)
        val animalButtons = metaDataService.getActionsForType("Animal")
        assertThat(animalButtons).`as`("Two actions expected on animal").hasSize(2)
            .contains(ActionInfo("Feed", "Feed"), ActionInfo("Pat", "Pat"))

        val boneButtons = metaDataService.getActionsForType("Bone")
        assertThat(boneButtons).`as`("One button expected on bone")
            .hasSize(1)
            .contains(ActionInfo("Break", "Break"))

        verifyNoInteractions(metadataRepository, customFieldRepository)
    }

    @Test
    fun `test BatchActions`() {
        `when`(mnemonicRegistry.get("Animal")).thenReturn(Animal::class)

        val animalButtons = metaDataService.getBatchActionsForType("Animal")
        assertThat(animalButtons).`as`("One BatchActions  expected on animal").hasSize(1)
            .contains(ActionInfo("Milk All", "milkAll"))

        verifyNoInteractions(metadataRepository, customFieldRepository)
    }


    @Test
    fun `Test Renderers`() {
        `when`(mnemonicRegistry.get("GaleryImage")).thenReturn(GaleryImage::class)
        `when`(mnemonicRegistry.get("PaintedImage")).thenReturn(PaintedImage::class)

        val renderer = metaDataService.getRendererForColumn("GaleryImage", "image")
        assertThat(renderer).`as`("@ImageRenderer expected on GaleryImage.image")
            .isNotNull
            .isInstanceOf(RendererInfo::class.java)
            .isEqualTo(RendererInfo("IMAGE", mapOf("width" to "100%", "height" to "100%")))

        val noRenderer = metaDataService.getRendererForColumn("PaintedImage", "image")

        assertThat(noRenderer).`as`("@ImageRenderer not expected on GaleryImage.image")
            .isNull()
    }


    @Test
    fun `test Segmentation`() {
        `when`(mnemonicRegistry.get("PaintedImage")).thenReturn(PaintedImage::class)
        `when`(mnemonicRegistry.get("Photo")).thenReturn(Photo::class)
        `when`(mnemonicRegistry.get("GaleryImage")).thenReturn(GaleryImage::class)

        `when`(metadataRepository.findById("PaintedImage"))
            .thenReturn(Optional.of(metadataObject(PaintedImage::class)))
        `when`(metadataRepository.findById("Photo"))
            .thenReturn(Optional.of(metadataObject(Photo::class)))

        val noSegmentationDefined = metaDataService.getSegments("PaintedImage")
        assertThat(noSegmentationDefined).isEqualTo(listOf(SegmentInfo.default()))

        val segmentInfoOnDtoColumn = metaDataService.getSegments("Photo")
        assertThat(segmentInfoOnDtoColumn).isEqualTo(listOf(SegmentInfo("FRAME"), SegmentInfo("DIGITAL")))

        val segmentInfoOnDto = metaDataService.getSegments("GaleryImage")
        assertThat(segmentInfoOnDto).isEqualTo(listOf(SegmentInfo("DEFAULT", "Default"), SegmentInfo("FRAME", "Frame")))


    }

    private fun customDefinitions(): List<CustomField> {
        val customFields: MutableList<CustomField> = ArrayList()
        customFields.add(
            CustomField(
                CustomFieldId(UUID.randomUUID()),
                Reference.USER,
                "WIDTH",
                "Width",
                DataType.INTEGER
            )
        )
        customFields.add(
            CustomField(
                CustomFieldId(UUID.randomUUID()),
                Reference.USER,
                "HEIGHT",
                "Height",
                DataType.INTEGER
            )
        )
        return customFields
    }

    private fun metadataObject(dto: KClass<*>): MetadataObject {
        val mapper = MetadataEntityDtoMapper()
        return mapper.convertToModel(annotationHarvester.harvest(dto))
    }
}