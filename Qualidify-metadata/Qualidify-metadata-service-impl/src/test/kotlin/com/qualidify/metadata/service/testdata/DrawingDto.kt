package com.qualidify.metadata.service.testdata

import com.qualidify.core.query.annotation.SearchQueryHint
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.types.CustomType
import com.qualidify.metadata.dto.CustomFieldDto
import com.qualidify.metadata.dto.CustomizableDto
import java.util.*

class DrawingDto(
    private val color: String,
    @SearchQueryHint("background.color")
    private val backgroundcolor: String,
) : CustomizableDto {

    @CustomType(reference = Reference.USER)
    private val fields: Set<DtoField> = mutableSetOf()


    override operator fun set(key: String, value: String?) {}
    override fun set(customFieldDto: CustomFieldDto) {}
    override fun optionalGet(key: String): Optional<String> {
        return Optional.empty()
    }

    override operator fun get(key: String): String? {
        return null
    }
}