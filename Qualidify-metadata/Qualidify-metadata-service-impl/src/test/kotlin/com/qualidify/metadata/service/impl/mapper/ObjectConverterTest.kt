package com.qualidify.metadata.service.impl.mapper

import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.MetadataValidationService
import com.qualidify.metadata.service.impl.ObjectConverterImpl
import com.qualidify.metadata.service.testdata.Fungi
import com.qualidify.metadata.service.testdata.FungiDto
import com.qualidify.metadata.service.testdata.Furniture
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.math.BigDecimal
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class ObjectConverterTest {

    @Mock
    lateinit var metadataService: MetadataService

    @Mock
    lateinit var metadataValidationService: MetadataValidationService

    @InjectMocks
    lateinit var metadata: Metadata


    @Test
    fun `Test converting objects to Map`() {
        val champignon = Fungi("Agaricus bisporus")

        val champignonMap = ObjectConverterImpl().convertToMapWithCustomFields(champignon, Fungi::class)

        assertThat(champignonMap).isInstanceOf(Map::class.java)
            .containsEntry("name", "Agaricus bisporus")
            .containsEntry("isToxic", false)

        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Test converting map to object`() {
        val champignonMap = mapOf<String, Any>("name" to "Agaricus bisporus", "isToxic" to false)

        val champignon = ObjectConverterImpl().convertToObject(champignonMap, Fungi::class)

        assertThat(champignon).isInstanceOf(Fungi::class.java)
            .isEqualTo(Fungi("Agaricus bisporus"))
    }


    @Test
    fun `Test the getMap function without null values`() {
        val fungi = FungiDto("Amanita muscaria", true)

        val map = ObjectConverterImpl().convertToMap(fungi)

        assertThat(map).`as`("Metadatamapper will map object fields to a key-value map")
            .isInstanceOf(Map::class.java)
            .hasSize(2)
            .containsEntry("name", "Amanita muscaria")
            .containsEntry("isToxic", true)
    }

    @Test
    fun `Test the getMap function with null values`() {
        val furniture = Furniture("Rietveldstoel", null, 4, BigDecimal.valueOf(3000))

        val map = ObjectConverterImpl().convertToMap(furniture)

        assertThat(map).`as`("Metadatamapper will map object fields to a key-value map")
            .isInstanceOf(Map::class.java)
            .hasSize(4)
            .containsEntry("name", "Rietveldstoel")
            .containsEntry("brand", null)
            .containsEntry("numberOfLegs", 4)
            .containsEntry("price", BigDecimal.valueOf(3000))
    }


}