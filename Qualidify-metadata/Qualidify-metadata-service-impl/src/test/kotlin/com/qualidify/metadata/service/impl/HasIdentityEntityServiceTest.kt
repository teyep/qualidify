package com.qualidify.metadata.service.impl

import com.qualidify.metadata.service.*
import com.qualidify.metadata.service.repository.CustomMetadataEntityRepository
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class HasIdentityEntityServiceTest {
    @Mock
    lateinit var validator: MetadataValidationService

    @Mock
    lateinit var repository: CustomMetadataEntityRepository

    @Mock
    lateinit var dtoMapperLibrary: DtoMapperLibrary

    @Mock
    lateinit var metadataService: MetadataService

    @InjectMocks
    lateinit var fixture: HasIdentityEntityServiceImpl

    @InjectMocks
    lateinit var metadata: Metadata


    @Test
    fun `Create a metadata based object`() {
        `when`(dtoMapperLibrary.convertFromPojoMapToEntity<MovieEntity>(anyString(), anyMap()))
            .thenReturn(BACK_TO_THE_FUTURE_ENTITY)
        `when`(repository.save(BACK_TO_THE_FUTURE_ENTITY)).thenReturn(BACK_TO_THE_FUTURE_ENTITY)

        fixture.create(MOVIE, BACK_TO_THE_FUTURE)

        verify(validator, times(1)).validate(MOVIE, BACK_TO_THE_FUTURE)
        verify(repository, times(1)).save(BACK_TO_THE_FUTURE_ENTITY)
        verifyNoMoreInteractions(validator, repository, dtoMapperLibrary)
    }

    @Test
    fun `Update a metadata based object`() {
        `when`(dtoMapperLibrary.convertFromPojoMapToEntity<MovieEntity>(anyString(), anyMap()))
            .thenReturn(BACK_TO_THE_HISTORY_ENTITY)
        `when`(repository.update(BACK_TO_THE_HISTORY_ENTITY)).thenReturn(BACK_TO_THE_HISTORY_ENTITY)

        fixture.update(MOVIE, BACK_TO_THE_HISTORY)

        verify(validator, times(1)).validate(MOVIE, BACK_TO_THE_HISTORY)
        verify(repository, times(1)).update(BACK_TO_THE_HISTORY_ENTITY)
        verifyNoMoreInteractions(validator, repository, dtoMapperLibrary)
    }

    @Test
    fun `Delete a metadata based object`() {
        `when`(dtoMapperLibrary.convertFromPojoMapToEntity<MovieEntity>(anyString(), anyMap()))
            .thenReturn(BACK_TO_THE_FUTURE_ENTITY)

        fixture.delete(MOVIE, BACK_TO_THE_FUTURE)

        verify(validator, times(1)).validate(MOVIE, BACK_TO_THE_FUTURE)
        verify(repository, times(1)).delete(BACK_TO_THE_FUTURE_ENTITY)
        verifyNoMoreInteractions(validator, repository, dtoMapperLibrary)
    }


    companion object {
        const val MOVIE = "movie"
        val BACK_TO_THE_FUTURE = Movie("Back to the Future", 1985)
        val BACK_TO_THE_HISTORY = Movie("Back to the Future", 20)
        val BACK_TO_THE_FUTURE_ENTITY = MovieEntity("Back to the Future", 1985)
        val BACK_TO_THE_HISTORY_ENTITY = MovieEntity("Back to the Future", 20)

    }
}