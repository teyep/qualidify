package com.qualidify.metadata.service.impl.harvest


import com.qualidify.metadata.DataType
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.testdata.*
import org.assertj.core.api.Assertions
import org.assertj.core.api.Condition
import org.assertj.core.api.Fail
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

/**
 * A Test for Annotation Harvesting on the classpath.
 * The AnnotationHarvester will scan the classpath for for specific
 * classes and will interpret the code and return the result in a
 * MetaData structure {@link TableMetaData, ColumnMetaData and TypeMetaData}
 *
 * @author Marcel Pot
 */
internal class AnnotationHarversterTest {

    private var harvester: MetadataHarvester? = null

    @BeforeEach
    fun setup() {
        harvester = AnnotationHarvester()
    }

    @Test
    fun testHarvesting() {
        val metaData = harvester!!.harvest(Painter::class)
        Assertions.assertThat(metaData.addEnabled).`as`("Addition is enabled on class Painter").isTrue
        Assertions.assertThat(metaData.updateEnabled).`as`("Update is enabled on class Painter").isTrue
        Assertions.assertThat(metaData.deleteEnabled).`as`("Delete is enabled on class Painter").isTrue
        Assertions.assertThat(metaData.searchEnabled).`as`("Filtering is enabled on class Painter").isTrue
        Assertions.assertThat(metaData.columns).`as`("Number of fields detected on class Painter").hasSize(4)

            .`as`("ColumnmetaData first value")
            .has(equalNameCondition("name"), Assertions.atIndex(0))
            .has(equalDescriptionCondition("name"), Assertions.atIndex(0))
            .has(isEditable, Assertions.atIndex(0))
            .has(isViewable, Assertions.atIndex(0))
            .has(isVisible, Assertions.atIndex(0))
            .has(isSearchable, Assertions.atIndex(0))
            .has(equalDataType(DataType.STRING), Assertions.atIndex(0))


            .`as`("ColumnmetaData second value")
            .has(equalNameCondition("placeOfBirth"), Assertions.atIndex(1))
            .has(equalDescriptionCondition("placeOfBirth"), Assertions.atIndex(1))
            .has(not(isEditable), Assertions.atIndex(1))
            .has(isViewable, Assertions.atIndex(1))
            .has(isVisible, Assertions.atIndex(1))
            .has(isSearchable, Assertions.atIndex(1))
            .has(equalDataType(DataType.TEXT), Assertions.atIndex(1))

            .`as`("ColumnmetaData third value")
            .has(equalNameCondition("yearOfBirth"), Assertions.atIndex(2))
            .has(equalDescriptionCondition("Year of Birth"), Assertions.atIndex(2))
            .has(isEditable, Assertions.atIndex(2))
            .has(isViewable, Assertions.atIndex(2))
            .has(isVisible, Assertions.atIndex(2))
            .has(isSearchable, Assertions.atIndex(2))
            .has(equalDataType(DataType.INTEGER), Assertions.atIndex(2))

            .`as`("ColumnmetaData fourth value")
            .has(equalNameCondition("isAlive"), Assertions.atIndex(3))
            .has(equalDescriptionCondition("Is Alive?"), Assertions.atIndex(3))
            .has(isEditable, Assertions.atIndex(3))
            .has(not(isViewable), Assertions.atIndex(3))
            .has(isVisible, Assertions.atIndex(3))
            .has(isSearchable, Assertions.atIndex(3))
            .has(equalDataType(DataType.BOOLEAN), Assertions.atIndex(3))
    }

    @Test
    fun testHarvestingWithOrdering() {
        val metaData = harvester!!.harvest(Painting::class)
        Assertions.assertThat(metaData.addEnabled).`as`("Addition is enabled on class Painting").isTrue
        Assertions.assertThat(metaData.updateEnabled).`as`("Update is enabled on class Painting").isTrue
        Assertions.assertThat(metaData.deleteEnabled).`as`("Delete is enabled on class Painting").isTrue
        Assertions.assertThat(metaData.searchEnabled).`as`("Filtering is enabled on class Painting").isTrue
        Assertions.assertThat(metaData.columns).`as`("Number of fields detected on class Painter").hasSize(4)

            .`as`("ColumnmetaData first value")
            .has(equalNameCondition("name"), Assertions.atIndex(0))
            .has(equalDescriptionCondition("name"), Assertions.atIndex(0))

            .`as`("ColumnmetaData second value")
            .has(equalNameCondition("painter"), Assertions.atIndex(1))
            .has(equalDescriptionCondition("Painter"), Assertions.atIndex(1))

            .`as`("ColumnmetaData third value")
            .has(equalNameCondition("year"), Assertions.atIndex(2))
            .has(equalDescriptionCondition("Year of origin"), Assertions.atIndex(2))
            .has(equalDataType(DataType.INTEGER), Assertions.atIndex(2))


            .`as`("ColumnmetaData fourth value")
            .has(equalNameCondition("price"), Assertions.atIndex(3))
            .has(equalDescriptionCondition("price"), Assertions.atIndex(3))
            .has(equalDataType(DataType.DOUBLE), Assertions.atIndex(3))

    }

    @Test
    fun testHarvestingWithProviders() {
        val metaData = harvester!!.harvest(Paint::class)
        Assertions.assertThat(metaData.addEnabled).`as`("Addition is enabled on class Paint").isFalse
        Assertions.assertThat(metaData.updateEnabled).`as`("Update is enabled on class Paint").isTrue
        Assertions.assertThat(metaData.deleteEnabled).`as`("Delete is enabled on class Paint").isFalse
        Assertions.assertThat(metaData.searchEnabled).`as`("Filtering is enabled on class Paint").isTrue
        Assertions.assertThat(metaData.columns).`as`("Number of fields detected on class Painter").hasSize(3)

            .`as`("ColumnmetaData first value")
            .has(equalNameCondition("name"), Assertions.atIndex(0))
            .has(equalDescriptionCondition("name"), Assertions.atIndex(0))

            .`as`("ColumnmetaData second value")
            .has(equalNameCondition("color"), Assertions.atIndex(1))
            .has(equalDescriptionCondition("color"), Assertions.atIndex(1))
            .has(equalItemProvider("colorProvider"), Assertions.atIndex(1))
            .has(equalDataType(DataType.ENUMERATION), Assertions.atIndex(1))

            .`as`("ColumnmetaData third value")
            .has(equalNameCondition("usedIn"), Assertions.atIndex(2))
            .has(equalDescriptionCondition("usedIn"), Assertions.atIndex(2))
            .has(equalItemProvider("paintingProvider"), Assertions.atIndex(2))
            .has(equalDataType(DataType.ENUMERATION), Assertions.atIndex(2))
    }

    @Test
    fun testMetaData_noAnnotations() {
        val metaData = harvester!!.harvest(Furniture::class)

        Assertions.assertThat(metaData.addEnabled).`as`("If class is not annotated: addition is enabled ").isTrue
        Assertions.assertThat(metaData.updateEnabled).`as`("If class is not annotated: update is enabled ").isTrue
        Assertions.assertThat(metaData.deleteEnabled).`as`("If class is not annotated: deletion is enabled ").isTrue
        Assertions.assertThat(metaData.searchEnabled)
            .`as`("If class is not annotated: filtering is enabled ").isTrue
        Assertions.assertThat(metaData.columns.size).`as`("If class is not annotated: all fields are expected")
            .isEqualTo(4)
    }

    @Test
    fun testMetaData_withAnnotationsAndInheritance() {
        val metaData = harvester!!.harvest(Chair::class)

        Assertions.assertThat(metaData.addEnabled)
            .`as`("If inherited class is  annotated: addition is enabled ").isTrue
        Assertions.assertThat(metaData.updateEnabled)
            .`as`("If inherited class is  annotated: update is enabled ").isTrue
        Assertions.assertThat(metaData.deleteEnabled)
            .`as`("If inherited class is  annotated: deletion is enabled ").isFalse
        Assertions.assertThat(metaData.searchEnabled)
            .`as`("If inherited class is  annotated: filtering is enabled ").isTrue
        Assertions.assertThat(metaData.columns.size).`as`("If inherited class is  annotated: all fields are expected")
            .isEqualTo(5)

        val columns = metaData.columns

        Assertions.assertThat(columns[0].name).`as`("First Field is Color").isEqualTo("color")
        Assertions.assertThat(columns[0].searchable).`as`("Color is not Searchable").isFalse
        Assertions.assertThat(columns[0].editable).`as`("Color is not Editable").isFalse
        Assertions.assertThat(columns[0].viewable).`as`("Color is Viewable").isTrue
        Assertions.assertThat(columns[0].hidden).`as`("Color is not Hidden").isFalse
        Assertions.assertThat(columns[0].fieldAccess)
            .`as`("Color is has no configurable fields").isEqualTo(ColumnDef.FieldAccess.DEFAULT)
    }

    @Test
    fun testMetaData_typeMetaDataWithoutAnnotations() {
        val metaData = harvester!!.harvest(WallPaper::class)

        Assertions.assertThat(metaData.addEnabled)
            .`as`("Simple annotated class: addition is default enabled ").isTrue
        Assertions.assertThat(metaData.updateEnabled)
            .`as`("Simple annotated class: update is default enabled ").isTrue
        Assertions.assertThat(metaData.deleteEnabled)
            .`as`("Simple annotated class: deletion is default enabled ").isTrue
        Assertions.assertThat(metaData.searchEnabled)
            .`as`("Simple annotated class: filtering is default enabled ").isTrue
        Assertions.assertThat(metaData.columns.size).`as`("If class is  annotated: all fields are expected")
            .isEqualTo(4)

        val columns = metaData.columns

        for (cmd in columns) {
            when (cmd.name) {
                "available" -> {
                    Assertions.assertThat(cmd.dataType).isEqualTo(DataType.BOOLEAN)
                }
                "images" -> {
                    Assertions.assertThat(cmd.dataType).isEqualTo(DataType.COLLECTION)
                }
                "backgroundColor" -> {
                    Assertions.assertThat(cmd.dataType).isEqualTo(DataType.STRING)
                }
                "length" -> {
                    Assertions.assertThat(cmd.dataType).isEqualTo(DataType.INTEGER)
                }
                else -> Fail.fail<Any>("Unexpected field encountered")
            }
        }
    }

    @Test
    fun testMetaData_uploadTest() {
        val metaData = harvester!!.harvest(PaintedImage::class)
        Assertions.assertThat(metaData.columns).`as`("If class is  annotated: all fields are expected").hasSize(1)
            .`as`("Upload Annotated Field")
            .has(equalNameCondition("image"), Assertions.atIndex(0))
            .has(equalDataType(DataType.BINARY), Assertions.atIndex(0))

    }


    private fun not(condition: Condition<ColumnDef>): Condition<in ColumnDef> {
        return Condition({ cmd: ColumnDef -> !condition.matches(cmd) }, "Not " + condition.description())
    }

    fun equalNameCondition(name: String): Condition<ColumnDef> {
        return Condition({ cmd: ColumnDef -> cmd.name == name }, "name")
    }

    fun equalDescriptionCondition(description: String): Condition<ColumnDef> {
        return Condition({ cmd: ColumnDef -> cmd.description == description }, "description")
    }

    val isEditable: Condition<ColumnDef>
        get() = Condition({ obj: ColumnDef -> obj.editable }, "editable")
    val isViewable: Condition<ColumnDef>
        get() = Condition({ obj: ColumnDef -> obj.viewable }, "viewable")
    val isVisible: Condition<ColumnDef>
        get() = Condition({ cmd: ColumnDef -> !cmd.hidden }, "not hidden")
    val isSearchable: Condition<ColumnDef>
        get() = Condition({ obj: ColumnDef -> obj.searchable }, "searchable")

    fun equalDataType(dataType: DataType): Condition<ColumnDef> {
        return Condition({ cmd: ColumnDef -> cmd.dataType == dataType }, "DataType: $dataType")
    }


    fun equalItemProvider(itemprovider: String): Condition<ColumnDef> {
        return Condition(
            { cmd: ColumnDef -> itemprovider == cmd.attributes.get("itemProvider") },
            "Itemprovider :$itemprovider"
        )
    }

}