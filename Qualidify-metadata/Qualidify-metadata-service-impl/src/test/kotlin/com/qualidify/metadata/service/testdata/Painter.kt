package com.qualidify.metadata.service.testdata

import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.renderer.ImageRenderer
import com.qualidify.metadata.annotations.types.*
import java.math.BigDecimal
import java.util.*

class Painter(name: String, placeOfBirth: String, yearOfBirth: Int, alive: Boolean) {
    @Hidden
    private val id: UUID

    @DtoColumn
    var name: String

    @TextAreaType
    @DtoColumn(editable = false)
    var placeOfBirth: String

    @IntegerType
    @DtoColumn(description = "Year of Birth")
    val yearOfBirth: Int

    @BooleanType
    @DtoColumn(description = "Is Alive?", viewable = false)
    var isAlive: Boolean

    init {
        this.id = UUID.randomUUID()
        this.name = name
        this.placeOfBirth = placeOfBirth
        this.yearOfBirth = yearOfBirth
        this.isAlive = alive
    }
}

class Painting(year: Int, name: String, painter: Painter, price: BigDecimal) {
    @Hidden
    private val id: UUID

    @DtoColumn(order = 5, description = "Year of origin")
    private val year: Int

    @DtoColumn(order = 10)
    @NumberType
    private var price: BigDecimal

    @DtoColumn(order = 1)
    private val name: String

    @DtoColumn(order = 2, description = "Painter")
    private val painter: Painter

    init {
        id = UUID.randomUUID()
        this.year = year
        this.name = name
        this.painter = painter
        this.price = price
    }
}

@DtoMetadata(singular = "Paint", plural = "Paints", enableAddition = false, enableDelete = false)
class Paint {
    private val name: String? = null


    @EnumerationType(itemProvider = "colorProvider")
    private val color: Color = Color.BLUE

    @EnumerationType(itemProvider = "paintingProvider")
    private val usedIn: Collection<Painting> = emptySet();
}

data class PaintedImage(
    @UploadType
    val image: ByteArray,
)

@Segments(
    [
        SegmentDefinition("DEFAULT", "Default"),
        SegmentDefinition("FRAME", "Frame"),
    ]
)
data class GaleryImage(
    @ImageRenderer
    @DtoColumn(segments = [Segment("FRAME")])
    val image: ByteArray,
)


data class Photo(
    @ImageRenderer(width = "100%", height = "100%")
    @DtoColumn(segments = [Segment("FRAME"), Segment("DIGITAL")])
    val image: ByteArray
)

enum class Color {
    RED, GREEN, BLUE
}


