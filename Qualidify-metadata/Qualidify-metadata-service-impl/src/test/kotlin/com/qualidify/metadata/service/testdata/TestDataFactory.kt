package com.qualidify.metadata.service.testdata

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.metadata.service.dto.DtoDef
import com.qualidify.model.entities.metadata.CustomField
import com.qualidify.model.entities.metadata.CustomFieldId
import com.qualidify.model.entities.metadata.MetadataColumn
import com.qualidify.model.entities.metadata.MetadataObject
import java.util.*

internal class TestDataFactory {

    companion object {

        fun getCustomFieldDefinitionDto(id: UUID) = CustomFieldDef(
            id = id,
            description = "A custom field for Users to persist the email information",
            reference = Reference.USER,
            field = "EMAIL",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )

        fun getCustomFieldEntity(id: UUID) = CustomField(
            id = CustomFieldId(id),
            description = "A custom field for Users to persist the email information",
            reference = Reference.USER,
            field = "EMAIL",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )

        fun getMetaDataObjectEntity(mnemonic: String) = MetadataObject(
            mnemonic = mnemonic,
            singlename = "Object",
            pluralname = "Objects",
            addable = true,
            updatable = true,
            removable = true,
            searchable = true,
            columns = listOf(
                MetadataColumn(
                    mnemonic = mnemonic,
                    name = "First Row",
                    description = "First row",
                    dataType = DataType.STRING,
                    sortorder = 1
                ),
                MetadataColumn(
                    mnemonic = mnemonic,
                    name = "Second Row",
                    description = "Second row",
                    dataType = DataType.DOUBLE,
                    sortorder = 2
                ),
            )
        )


        fun getDtoDef(mnemonic: String) = DtoDef(
            mnemonic = mnemonic,
            name = "Object",
            pluralName = "Objects",
            addEnabled = true,
            updateEnabled = true,
            deleteEnabled = true,
            searchEnabled = true,
            columns = mutableListOf(
                ColumnDef(
                    mnemonic = mnemonic,
                    name = "First Row",
                    description = "First row",
                    dataType = DataType.STRING,
                    sortOrder = 1
                ),
                ColumnDef(
                    mnemonic = mnemonic,
                    name = "Second Row",
                    description = "Second row",
                    dataType = DataType.DOUBLE,
                    sortOrder = 2
                )
            )
        )
    }
}