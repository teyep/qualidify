package com.qualidify.metadata.service.impl

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.metadata.service.impl.mapper.CustomFieldDtoMapper
import com.qualidify.metadata.service.impl.repositories.CustomFieldRepository
import com.qualidify.model.entities.metadata.CustomField
import com.qualidify.model.entities.metadata.CustomFieldId
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
class CustomFieldServiceTest {

    @Mock
    lateinit var repository: CustomFieldRepository

    @Mock
    lateinit var mapper: CustomFieldDtoMapper

    @InjectMocks
    lateinit var fixture: CustomFieldServiceImpl

    @Test
    fun `Create a new CustomField`() {
        val customField = CustomFieldDef(
            id = UUID.randomUUID(),
            reference = Reference.USER,
            field = "EMAIL",
            description = "Email of the user",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )
        val customFieldEntity = CustomField(
            id = CustomFieldId(customField.id!!),
            reference = Reference.USER,
            field = "EMAIL",
            description = "Email of the user",
            dataType = DataType.EMAIL,
            mandatory = true
        )
        `when`(mapper.convertToModel(customField)).thenReturn(customFieldEntity)
        `when`(repository.save(customFieldEntity)).thenReturn(customFieldEntity)

        fixture.create(customField)

        verify(mapper, times(1)).convertToModel(customField)
        verify(repository, times(1)).save(customFieldEntity)
        verifyNoMoreInteractions(mapper, repository)
    }

    @Test
    fun `Create a new CustomField with empty field`() {
        val customField = CustomFieldDef(
            id = UUID.randomUUID(),
            reference = Reference.USER,
            field = "",
            description = "Email of the user",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )

        assertThatThrownBy {
            fixture.create(customField)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Field must have text.")
    }

    @Test
    fun `Create a new CustomField with empty description`() {
        val customField = CustomFieldDef(
            id = UUID.randomUUID(),
            reference = Reference.USER,
            field = "EMAIL",
            description = "",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )

        assertThatThrownBy {
            fixture.create(customField)
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Description should have text.")
    }

    @Test
    fun `Delete a CustomField`() {
        val customField = CustomFieldDef(
            id = UUID.randomUUID(),
            reference = Reference.USER,
            field = "EMAIL",
            description = "Email of the user",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )

        fixture.delete(customField)

        verify(repository, times(1)).deleteById(CustomFieldId(customField.id!!))
        verifyNoMoreInteractions(repository)
        verifyNoInteractions(mapper)
    }

    @Test
    fun `Delete a CustomField without id`() {
        val customField = CustomFieldDef(
            null,
            reference = Reference.USER,
            field = "EMAIL",
            description = "Email of the user",
            dataType = DataType.EMAIL,
            validator = null,
            mandatory = true,
            active = true
        )

        assertThatThrownBy {
            fixture.delete(customField)
        }.isInstanceOf(NullPointerException::class.java)
            .hasMessage("Cannot delete a Field without id.")

        verifyNoInteractions(repository, mapper)
    }
}