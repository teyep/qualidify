package com.qualidify.metadata.service.testdata

import com.qualidify.metadata.annotations.DtoColumn
import com.qualidify.metadata.annotations.DtoMetadata
import com.qualidify.metadata.annotations.types.BooleanType
import com.qualidify.metadata.annotations.types.EnumerationType
import com.qualidify.metadata.annotations.types.GridType
import com.qualidify.metadata.annotations.types.IntegerType
import java.math.BigDecimal
import java.math.BigInteger

open class Furniture(
    val name: String? = null,
    open val brand: String? = null,
    val numberOfLegs: Int = 0,
    val price: BigDecimal? = null,
)

@DtoMetadata(singular = "Chair", plural = "Chairs", enableDelete = false)
class Chair(
    name: String?,
    brand: String?,
    price: BigDecimal?,

    @EnumerationType
    @DtoColumn(searchable = false, editable = false)
    private val color: Color,
) : Furniture(name, brand, 4, price) {

}

@DtoMetadata(singular = "Table", plural = "A lot of tables", enableAddition = false, enableDelete = false)
class Table(
    name: String,
    @DtoColumn(searchable = true, editable = false, description = "TableHeight")
    val height: Int,
) : Furniture(name, "SQUARE united", 4, BigDecimal.valueOf(1700.0))


@DtoMetadata(singular = "Wallpaper", plural = "WallPapers")
class WallPaper {
    internal enum class Image {
        BIRD, DOG, MOON
    }

    @BooleanType
    private val available = false

    @GridType(mnemonic = "Image")
    private val images: Set<Image>? = null

    private val backgroundColor: String? = null

    @IntegerType
    private val length: BigInteger? = null
}
