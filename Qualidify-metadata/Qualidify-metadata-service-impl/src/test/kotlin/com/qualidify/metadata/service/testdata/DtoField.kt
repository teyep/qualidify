package com.qualidify.metadata.service.testdata

internal class DtoField(
    private val field: String,
    private val value: String,
)