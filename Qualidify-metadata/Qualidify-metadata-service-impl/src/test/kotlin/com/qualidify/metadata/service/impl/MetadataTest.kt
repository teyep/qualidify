package com.qualidify.metadata.service.impl

import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.MetadataValidationService
import com.qualidify.metadata.service.dto.ActionInfo
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.metadata.service.dto.DtoDef
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

/**
 * Metadata test. Because the Metadata is a static delegation class,
 * it will test mostly test for delegation. Different outcomes are tested
 * in [MetadataServiceTest]
 *
 * @author Marcel Pot
 * @since 1.0
 */
@ExtendWith(MockitoExtension::class)
internal class MetadataTest {

    @Mock
    private lateinit var metadataService: MetadataService

    @Mock
    private lateinit var metadataValidationService: MetadataValidationService

    @InjectMocks
    private lateinit var metadata: Metadata


    @Test
    fun `Get a Dto Definition`() {
        `when`(metadataService.getDtoDefinition("Fungi"))
            .thenReturn(FungiDtoDefinition())

        val dtoDef = Metadata.getDtoDefinition("Fungi")

        assertThat(dtoDef).`as`("Metadata is only delegating to service")
            .isEqualTo(FungiDtoDefinition())

        verify(metadataService, times(1)).getDtoDefinition("Fungi")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get a singular Name`() {
        `when`(metadataService.getDtoDefinition("Fungi"))
            .thenReturn(FungiDtoDefinition())

        val name = Metadata.getSingularName("Fungi")

        assertThat(name).`as`("Metadata is only delegating to service")
            .isEqualTo("Fungi")

        verify(metadataService, times(1)).getDtoDefinition("Fungi")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get a plural Name`() {
        `when`(metadataService.getDtoDefinition("Fungi"))
            .thenReturn(FungiDtoDefinition())

        val name = Metadata.getPluralName("Fungi")

        assertThat(name).`as`("Metadata is only delegating to service")
            .isEqualTo("Fungis")

        verify(metadataService, times(1)).getDtoDefinition("Fungi")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Check if addition is enabled`() {
        `when`(metadataService.isAdditionEnabled("Fungi"))
            .thenReturn(true)
        `when`(metadataService.isAdditionEnabled("Animal"))
            .thenReturn(false)

        val isFungiAddable = Metadata.isAdditionEnabled("Fungi")
        val isAnimalAddable = Metadata.isAdditionEnabled("Animal")

        assertThat(isFungiAddable).`as`("Metadata is only delegating to service")
            .isTrue()
        assertThat(isAnimalAddable).`as`("Metadata is only delegating to service")
            .isFalse()

        verify(metadataService, times(1)).isAdditionEnabled("Fungi")
        verify(metadataService, times(1)).isAdditionEnabled("Animal")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Check if update is enabled`() {
        `when`(metadataService.isUpdateEnabled("Fungi"))
            .thenReturn(true)
        `when`(metadataService.isUpdateEnabled("Animal"))
            .thenReturn(false)

        val isFungiEditable = Metadata.isUpdateEnabled("Fungi")
        val isAnimalEditable = Metadata.isUpdateEnabled("Animal")

        assertThat(isFungiEditable).`as`("Metadata is only delegating to service")
            .isTrue()
        assertThat(isAnimalEditable).`as`("Metadata is only delegating to service")
            .isFalse()

        verify(metadataService, times(1)).isUpdateEnabled("Fungi")
        verify(metadataService, times(1)).isUpdateEnabled("Animal")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Check if delete is enabled`() {
        `when`(metadataService.isDeleteEnabled("Fungi"))
            .thenReturn(true)
        `when`(metadataService.isDeleteEnabled("Animal"))
            .thenReturn(false)

        val isFungiDeletable = Metadata.isDeleteEnabled("Fungi")
        val isAnimalDeletable = Metadata.isDeleteEnabled("Animal")

        assertThat(isFungiDeletable).`as`("Metadata is only delegating to service")
            .isTrue()
        assertThat(isAnimalDeletable).`as`("Metadata is only delegating to service")
            .isFalse()

        verify(metadataService, times(1)).isDeleteEnabled("Fungi")
        verify(metadataService, times(1)).isDeleteEnabled("Animal")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Check if filtering is enabled`() {
        `when`(metadataService.isFilteringEnabled("Fungi"))
            .thenReturn(true)
        `when`(metadataService.isFilteringEnabled("Animal"))
            .thenReturn(false)

        val isFungiSearcheable = Metadata.isFilteringEnabled("Fungi")
        val isAnimalSearcheable = Metadata.isFilteringEnabled("Animal")

        assertThat(isFungiSearcheable).`as`("Metadata is only delegating to service")
            .isTrue()
        assertThat(isAnimalSearcheable).`as`("Metadata is only delegating to service")
            .isFalse()

        verify(metadataService, times(1)).isFilteringEnabled("Fungi")
        verify(metadataService, times(1)).isFilteringEnabled("Animal")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get a Dto Columns `() {
        `when`(metadataService.getColumns("Fungi"))
            .thenReturn(FungiDtoDefinition().columns)

        val columnDefs = Metadata.getColumns("Fungi")

        assertThat(columnDefs).`as`("Metadata is only delegating to service")
            .isEqualTo(FungiDtoDefinition().columns)

        verify(metadataService, times(1)).getColumns("Fungi")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get the CustomField for Reference`() {
        `when`(metadataService.getCustomFieldsForReference(Reference.USER))
            .thenReturn(customDefinitions())

        val customFields = Metadata.getCustomFieldsForReference(Reference.USER)

        assertThat(customFields).`as`("Metadata is only delegating to service")
            .isEqualTo(customDefinitions())

        verify(metadataService, times(1)).getCustomFieldsForReference(Reference.USER)
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get a list with all mnemonics`() {
        `when`(metadataService.getMnemonicList())
            .thenReturn(listOf("FUNGI", "BACTERIA", "ANIMAL", "VIRUS"))

        val customFields = Metadata.getMnemonicList()

        assertThat(customFields).`as`("Metadata is only delegating to service")
            .isEqualTo(listOf("FUNGI", "BACTERIA", "ANIMAL", "VIRUS"))

        verify(metadataService, times(1)).getMnemonicList()
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get the creationStrategy Pattern`() {
        `when`(metadataService.getCreationStrategyPattern("Fungi"))
            .thenReturn(CreationStrategyPattern.OBJECT_CONSTRUCTOR)

        val pattern = Metadata.getCreationStrategyPattern("Fungi")

        assertThat(pattern).`as`("Metadata is only delegating to service")
            .isEqualTo(CreationStrategyPattern.OBJECT_CONSTRUCTOR)

        verify(metadataService, times(1)).getCreationStrategyPattern("Fungi")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get the Actions for a dto`() {
        `when`(metadataService.getActionsForType("Animal"))
            .thenReturn(listOf(ActionInfo("Feed", "Feed"), ActionInfo("Pat", "Pat")))

        val actions = Metadata.getActionsForType("Animal")

        assertThat(actions).`as`("Metadata is only delegating to service")
            .isEqualTo(listOf(ActionInfo("Feed", "Feed"), ActionInfo("Pat", "Pat")))

        verify(metadataService, times(1))
            .getActionsForType("Animal")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    /** No delegation test!!! **/
    @Test
    fun `Get the columns for Editing`() {
        `when`(metadataService.getColumns("Animal"))
            .thenReturn(AnimalDtoDefinition().columns)

        val columnDefs = Metadata.getColumnsForEditing("Animal")

        assertThat(columnDefs).`as`("Metadata is filtering  the result of the service")
            .hasSize(2)
            .isEqualTo(
                AnimalDtoDefinition().columns.filter { !it.hidden }.toList()
            )

        verify(metadataService, times(1)).getColumns("Animal")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get the columns for Viewing`() {
        `when`(metadataService.getColumns("Animal"))
            .thenReturn(AnimalDtoDefinition().columns)

        val columnDefs = Metadata.getColumnsForViewing("Animal")

        assertThat(columnDefs).`as`("Metadata is filtering  the result of the service")
            .hasSize(1)
            .isEqualTo(
                AnimalDtoDefinition().columns.filter { !it.hidden }.filter { it.viewable }.toList()
            )

        verify(metadataService, times(1)).getColumns("Animal")
        verifyNoMoreInteractions(metadataService)
        verifyNoInteractions(metadataValidationService)
    }

    @Test
    fun `Get the validationService`() {

        val validationService = Metadata.getMetadataValidationService()
        assertThat(validationService).`as`("Get the validationservice")
            .isEqualTo(metadataValidationService)

        verifyNoInteractions(metadataService, metadataValidationService)
    }

    private fun AnimalDtoDefinition() = DtoDef(
        name = "Animal",
        pluralName = "Animals",
        mnemonic = "Animal",
        addEnabled = false,
        updateEnabled = false,
        deleteEnabled = false,
        searchEnabled = false

    ).also {
        it.addColumn(
            ColumnDef(
                name = "name",
                description = "name",
                dataType = DataType.STRING
            )
        )
        it.addColumn(
            ColumnDef(
                name = "age",
                description = "age",
                dataType = DataType.INTEGER,
                hidden = true
            )
        )
        it.addColumn(
            ColumnDef(
                name = "bones",
                description = "bones",
                dataType = DataType.COLLECTION,
                viewable = false
            )
        )
    }

    private fun FungiDtoDefinition() = DtoDef(
        name = "Fungi",
        pluralName = "Fungis",
        mnemonic = "Fungi"

    ).also {
        it.addColumn(
            ColumnDef(
                name = "name",
                description = "name",
                dataType = DataType.STRING
            )
        )
        it.addColumn(
            ColumnDef(
                name = "isToxic",
                description = "isToxic",
                dataType = DataType.BOOLEAN
            )
        )
    }

    private fun customDefinitions(): List<CustomFieldDef> {
        val customFields: MutableList<CustomFieldDef> = ArrayList()
        customFields.add(
            CustomFieldDef(
                id = UUID.randomUUID(),
                reference = Reference.USER,
                field = "WIDTH",
                description = "Width",
                dataType = DataType.INTEGER,
                validator = "",
                active = true,
                mandatory = true
            )
        )
        customFields.add(
            CustomFieldDef(
                id = UUID.randomUUID(),
                reference = Reference.USER,
                field = "HEIGHT",
                description = "Height",
                dataType = DataType.INTEGER,
                validator = "",
                active = true,
                mandatory = true
            )
        )
        return customFields
    }

}