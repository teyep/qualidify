package com.qualidify.metadata.service.impl

import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
@EntityScan(basePackages = ["com.qualidify.model.entities"])
open class QualidifyMetadataServiceTestApplication {

    @Test
    fun contextLoads() {
    }

}