package com.qualidify.metadata.service.impl.mapper

import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.metadata.service.testdata.TestDataFactory
import com.qualidify.model.entities.metadata.CustomField
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

internal class CustomFieldDtoMapperTest {
    val mapper: CustomFieldDtoMapper = CustomFieldDtoMapper()

    @Test
    fun `Convert a CustomField Entity to a CustomFieldDefinitionDto`() {
        val id = UUID.randomUUID()
        val entity = TestDataFactory.getCustomFieldEntity(id)

        val dto = mapper.convertToDto(entity)

        assertThat(dto).`as`("Check the instance of the mapped instance")
            .isInstanceOf(CustomFieldDef::class.java)
            .`as`("Check the data of the mapped instance")
            .isEqualTo(
                TestDataFactory.getCustomFieldDefinitionDto(id)
            )
    }

    @Test
    fun `Convert a CustomFieldDefinitionDto to a CustomField Entity`() {
        val id = UUID.randomUUID()
        val dto = TestDataFactory.getCustomFieldDefinitionDto(id)

        val entity = mapper.convertToModel(dto)

        assertThat(entity).`as`("Check the instance of the mapped instance")
            .isInstanceOf(CustomField::class.java)
            .`as`("Check the data of the mapped instance")
            .isEqualTo(
                TestDataFactory.getCustomFieldEntity(id)
            )
    }


}