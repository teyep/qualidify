package com.qualidify.metadata.service.impl.repositories

import com.qualidify.model.entities.metadata.MetadataObject
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

/**
 * A repository for [MetadataObject]
 *
 * @author Marcel Pot
 * @since 1.0
 */
//@Repository
interface MetadataObjectRepository : JpaRepository<MetadataObject, String> {

    @Query("select m.mnemonic from MetadataObject m where m.mnemonic is not null")
    fun findMnemonicList(): List<String>
}