package com.qualidify.metadata.service.impl

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
open class QualidifyMetadataServiceApplication {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(QualidifyMetadataServiceApplication::class.java, *args)
        }
    }

}