package com.qualidify.metadata.service.impl.mapper

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.model.entities.metadata.CustomField
import com.qualidify.model.entities.metadata.CustomFieldId
import java.util.*

@DtoMapper
open class CustomFieldDtoMapper : EntityDtoMapper<CustomField, CustomFieldDef> {

    override fun convertToModel(dto: CustomFieldDef) = CustomField(
        id = CustomFieldId(dto.id ?: UUID.randomUUID()),
        reference = dto.reference,
        field = dto.field,
        dataType = dto.dataType,
        description = dto.description,
        validator = dto.validator,
        active = dto.active
    )

    override fun convertToDto(entity: CustomField) = CustomFieldDef(
        id = entity.id.unbox(),
        reference = entity.reference,
        field = entity.field,
        description = entity.description,
        dataType = entity.dataType,
        validator = entity.validator,
        mandatory = entity.mandatory,
        active = entity.active
    )
}
