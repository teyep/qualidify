package com.qualidify.metadata.service.impl

import com.qualidify.metadata.service.MnemonicRegistry
import com.qualidify.metadata.service.impl.harvest.MetadataHarvester
import com.qualidify.metadata.service.impl.mapper.MetadataEntityDtoMapper
import com.qualidify.metadata.service.impl.repositories.MetadataObjectRepository
import javax.annotation.PostConstruct

/**
 * Bean defininition for reading classpath and pushing missing
 * dto definition to a repository
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-525
 */
class MetadataInjector(
    val metadataHarvester: MetadataHarvester,
    val metadataObjectRepository: MetadataObjectRepository,
    val mnemonicRegistry: MnemonicRegistry,
    val mapper: MetadataEntityDtoMapper
) {

    @PostConstruct
    fun start() {
        mnemonicRegistry.entries
            .forEach {
                if (!checkExistingMnemonic(it.key)) {
                    val dtoDef = metadataHarvester.harvest(it.value)
                    val metaObject = mapper.convertToModel(dtoDef)
                    metadataObjectRepository.save(metaObject)
                }
            }
    }

    private fun checkExistingMnemonic(mnemonic: String): Boolean {
        return metadataObjectRepository.existsById(mnemonic)
    }
}