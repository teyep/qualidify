package com.qualidify.metadata.service.impl.harvest

import com.qualidify.metadata.DataType
import com.qualidify.metadata.DataType.*
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.annotations.renderer.Renderer
import com.qualidify.metadata.annotations.types.*
import com.qualidify.metadata.service.dto.ActionInfo
import com.qualidify.metadata.service.dto.RendererInfo
import com.qualidify.metadata.service.dto.SegmentInfo
import org.springframework.core.annotation.AnnotatedElementUtils
import org.springframework.core.annotation.AnnotationUtils
import java.lang.reflect.Field
import java.lang.reflect.ParameterizedType
import kotlin.reflect.KClass

/**
 * A helper class for metadata structure determination
 *
 * @author Marcel Pot
 * @since 1.0
 */
internal object MetadataStructureUtils {

    /**
     * Determine the creation strategy for a class
     * @param beanType the class to search for
     * @param T the type of the bean
     * @return the [CreationStrategy] annotation found for this class
     */
    fun <T : Any> getCreationStrategy(beanType: KClass<T>): CreationStrategy? {
        return AnnotatedElementUtils.getMergedAnnotation(beanType.java, CreationStrategy::class.java)
    }


    /**
     * Determine the creation strategy for a class
     * @param beanType the class to search for
     * @param T the type of the bean
     * @return the [CreationStrategyPattern] found for this class, or otherwise the default
     */
    fun <T : Any> getCreationStrategyPattern(beanType: KClass<T>): CreationStrategyPattern {
        val annotatation: CreationStrategy? = getCreationStrategy(beanType)
        return annotatation?.value ?: CreationStrategyPattern.MAP_DTO
    }


    /**
     * Determine the renderer type of a field
     * @param beanType the class to search for
     * @param fieldName the name of the field to check
     * @return found Rendering information
     */
    fun determineRenderer(beanType: KClass<*>, fieldName: String): RendererInfo? {
        if (beanType.java.declaredFields.asSequence()
                .map { it.name }
                .contains(fieldName)
        ) {
            val field: Field = beanType.java.getDeclaredField(fieldName)
            return field.declaredAnnotations.asSequence()
                .filter { AnnotatedElementUtils.isAnnotated(it.annotationClass.java, Renderer::class.java) }
                .map {

                    RendererInfo(
                        determineRenderType(field)!!.name,
                        determineRendererAttributes(field, it)
                    )

                }

                .firstOrNull()

        } else return null
    }

    private fun determineRenderType(
        field: Field,
    ) = AnnotatedElementUtils.getMergedAnnotation(field, Renderer::class.java)?.value

    private fun determineRendererAttributes(
        field: Field,
        it: Annotation
    ) = AnnotatedElementUtils.getMergedAnnotationAttributes(
        field,
        it.annotationClass.qualifiedName!!,
        true,
        true
    )
        ?.asSequence()
        ?.map { it.key to it.value.toString() }
        ?.toMap()
        ?: emptyMap()

    /**
     * Determine the  type of a  field
     * @param beanType the class to search in
     * @param fieldName the name of the field to check
     * @param T the type of the bean
     * @return the class type
     */
    fun <T : Any> determineClassTypeOfField(beanType: KClass<T>, fieldName: String): KClass<*> {
        val collectionField: Field = beanType.java.getDeclaredField(fieldName)
        return collectionField.type.kotlin
    }

    /**
     * Determine the generic type of a collection field
     * @param beanType the class to search in
     * @param fieldName the name of the field to check
     * @param T the type of the bean
     * @return the generic class type
     * @throws IllegalArgumentException when the field is not a collection
     */
    fun <T : Any> determineGenericClassTypeOfCollection(beanType: KClass<T>, fieldName: String): KClass<*> {
        val collectionField: Field = beanType.java.getDeclaredField(fieldName)
        if (Collection::class.java.isAssignableFrom(collectionField.type)) {
            val collectionType: ParameterizedType = collectionField.getGenericType() as ParameterizedType
            val collectionClass = collectionType.getActualTypeArguments().get(0) as Class<*>
            return collectionClass.kotlin
        }
        throw IllegalArgumentException("Cannot determine a generic type of a non-collection field")
    }


    /**
     * Determine the field annotated with the [CustomType] annotation
     * @param beanType the beanType
     * @param T the type of the bean
     * @return the field (nullable)
     */
    fun <T : Any> determineCustomField(beanType: KClass<T>): Field? {
        return beanType.java.declaredFields.asSequence()
            .filter { it.isAnnotationPresent(CustomType::class.java) }
            .firstOrNull()
    }

    fun determineTypeAttributes(field: Field, dataType: DataType? = null): Map<String, String> {
        val attributes: Map<String, String> = field.declaredAnnotations
            .filter { it.annotationClass.simpleName!!.endsWith("Type") }
            .map {

                AnnotationUtils.getAnnotationAttributes(field, it)
                    .entries
                    .asSequence()
                    .map { it.key to attributeToString(it.value) }
                    .toMutableList()
                    .also { list -> list.add("type" to it.annotationClass.simpleName!!) }
            }
            .fold(
                mutableMapOf(),
                { accumulator, items -> accumulator.also { accumulator.putAll(items) } }
            )
        if (attributes.isEmpty()) {
            return defaultAttributes(dataType)
        }
        return attributes
    }

    private fun defaultAttributes(dataType: DataType?): Map<String, String> {
        return when (dataType) {
            BINARY -> getDefaultValuesOfAnnotation(UploadType::class.java)
            BOOLEAN -> getDefaultValuesOfAnnotation(BooleanType::class.java)
            COLLECTION -> getDefaultValuesOfAnnotation(CheckBoxGroupType::class.java)
            CUSTOM -> getDefaultValuesOfAnnotation(CustomType::class.java)
            DATE -> getDefaultValuesOfAnnotation(DateType::class.java)
            DATETIME -> getDefaultValuesOfAnnotation(DateTimeType::class.java)
            DOUBLE -> getDefaultValuesOfAnnotation(NumberType::class.java)
            EMAIL -> getDefaultValuesOfAnnotation(EmailType::class.java)
            ENUMERATION -> getDefaultValuesOfAnnotation(RadioButtonType::class.java)
            INTEGER -> getDefaultValuesOfAnnotation(IntegerType::class.java)
            PASSWORD -> getDefaultValuesOfAnnotation(PasswordType::class.java)
            RICH_TEXT -> getDefaultValuesOfAnnotation(EditorType::class.java)
            STRING -> getDefaultValuesOfAnnotation(TextType::class.java)
            TEXT -> getDefaultValuesOfAnnotation(TextAreaType::class.java)
            TIME -> getDefaultValuesOfAnnotation(TimeType::class.java)
            else -> getDefaultValuesOfAnnotation(TextType::class.java)
        }
    }

    private fun attributeToString(attribute: Any): String {
        return when (attribute) {
            is Class<*> -> attribute.name
            is Array<*> -> attribute.asSequence().filterNotNull().joinToString { attributeToString(it) }
            else -> attribute.toString()
        }
    }

    private fun getDefaultValuesOfAnnotation(annotationClass: Class<out Annotation>): Map<String, String> {
        return annotationClass.declaredMethods.asSequence()
            .map { it.name to (AnnotationUtils.getDefaultValue(annotationClass, it.name)?.toString() ?: "") }
            .toMap().toMutableMap()
            .also { it.put("type", annotationClass.simpleName) }
    }

    /**
     * Determine the class annotations for Actions
     * @param beanType the beantype
     * @return a list with GridButton information
     */
    fun determineActions(beanType: KClass<*>): List<ActionInfo> {
        if (beanType.java.isAnnotationPresent(Actions::class.java)) {
            return beanType.java.getDeclaredAnnotation(Actions::class.java).value.asSequence()
                .map { ActionInfo(it.label, it.action, it.position, it.order) }
                .sortedBy { it.order }
                .toList()
        } else {
            return beanType.java.getDeclaredAnnotationsByType(Action::class.java).asSequence()
                .map { ActionInfo(it.label, it.action, it.position, it.order) }
                .sortedBy { it.order }
                .toList()
        }
    }

    /**
     * Determine the class annotations for BulkActions
     * @param beanType the beantype
     * @return a list with GridButton information
     */
    fun determineBatchActions(beanType: KClass<*>): List<ActionInfo> {
        if (beanType.java.isAnnotationPresent(BatchActions::class.java)) {
            return beanType.java.getDeclaredAnnotation(BatchActions::class.java).value.asSequence()
                .map { ActionInfo(it.label, it.action, it.position, it.order) }
                .sortedBy { it.order }
                .toList()
        }
        return emptyList()
    }

    /**
     * Determine segmentation definitions
     * @param beanType the beantype
     * @return a list of Segmentation information
     */
    fun determineSegmentation(beanType: KClass<*>): List<SegmentInfo> {
        if (beanType.java.isAnnotationPresent(Segments::class.java)) {
            return beanType.java.getDeclaredAnnotation(Segments::class.java).value.asSequence()
                .map { SegmentInfo(it.value, it.description) }
                .toList()
        }
        return emptyList()
    }
}