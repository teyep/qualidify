package com.qualidify.metadata.service.impl.repositories

import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.metadata.Reference
import com.qualidify.model.entities.metadata.CustomField
import com.qualidify.model.entities.metadata.CustomFieldId
import java.util.*

/**
 * Repository for CustomFields
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface CustomFieldRepository : QueryablePagingAndSortingRepository<CustomField, CustomFieldId> {

    /**
     * Find all active Customfields based on refercence
     * @param reference the reference
     * @param active the indicator for active entities (default = true)
     * @return a list of CustomField
     */
    fun findAllByReferenceAndActive(reference: Reference, active: Boolean = true): List<CustomField>


    /**
     * Find all Customfields based on Reference
     * @param reference the reference
     * @return a list of CustomFields
     */
    fun findAllByReference(reference: Reference): List<CustomField>


    /**
     * Find one CustomField based on (unique) Reference and Field combination
     * @param reference the reference
     * @param field
     * @return an optional of a Customfield
     */
    fun findOneByReferenceAndField(
        reference: Reference,
        field: String
    ): Optional<CustomField?>


}