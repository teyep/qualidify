package com.qualidify.metadata.service.impl

import com.google.common.base.Preconditions
import com.google.gson.Gson
import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.service.ObjectCreator
import com.qualidify.metadata.service.ObjectFactory
import com.qualidify.metadata.service.exceptions.DtoCreationException
import com.qualidify.metadata.service.impl.harvest.MetadataStructureUtils
import org.springframework.beans.factory.BeanCreationException
import org.springframework.context.ApplicationContext
import org.springframework.core.ResolvableType
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

/**
 * A Factory for objects. Based on the CreationStrategy the Object will be created
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
class ObjectCreatorImpl(val context: ApplicationContext) : ObjectCreator {

    private val objectMapper: Gson
    private val logger by LoggerDelegation()

    override fun <T : Any> create(beanType: KClass<T>, vararg params: Any?): T {
        Preconditions.checkNotNull(beanType, "BeanType cannot be null")
        val creationStrategyPattern = determineCreationStrategyPattern(beanType)
        return when (creationStrategyPattern) {
            CreationStrategyPattern.OBJECT_CONSTRUCTOR -> objectConstructorCreation(beanType, params)
            CreationStrategyPattern.FACTORY -> factoryConstruction(beanType, params)
            CreationStrategyPattern.MAP_DTO -> createEmptyBean(beanType)
            CreationStrategyPattern.EMPTY_CONSTRUCTOR -> emptyConstructorCreation(beanType)
        }
    }

    /**
     * Create an empty bean
     *
     * @param beanType the bean to create
     * @param T     the type of the bean
     * @return The empty bean
     */
    fun <T : Any> createEmptyBean(beanType: KClass<T>): T {
        return objectMapper.fromJson("{}", beanType.java)
    }

    /**
     * Determine the Strategy to create a new object for a beantype
     *
     * @param beanType the type to create
     * @param T   the Type to create
     * @return the creation strategy
     */
    override fun <T : Any> determineCreationStrategyPattern(beanType: KClass<T>) =
        MetadataStructureUtils.getCreationStrategyPattern(beanType)


    override fun <T : Any> getCreationStrategy(beanType: KClass<T>) =
        MetadataStructureUtils.getCreationStrategy(beanType)

    /**
     * Create an object based on the empty constructor
     *
     * @param beanType the type to create
     * @param T     the type to create
     * @return the new instance
     * @throws DtoCreationException
     */
    fun <T : Any> emptyConstructorCreation(beanType: KClass<T>): T {
        return try {
            beanType.java.getDeclaredConstructor().newInstance()
        } catch (e: Exception) {
            throw DtoCreationException("Dto " + beanType.simpleName + " cannot be created with an empty constructor", e)
        }
    }

    /**
     * Create an object based on the non-empty constructor
     *
     * @param beanType the type to create
     * @param params   the parameters for the constructor
     * @param <T>      the type to create
     * @return the new instance
     * @throws DtoCreationException
     */
    fun <T : Any> objectConstructorCreation(beanType: KClass<T>, params: Array<out Any?>): T {
        try {
            try {
                val paramTypes = params.asSequence()
                    .filterNotNull()
                    .map { it::class.java }
                    .toList()
                    .toTypedArray()
                val constructor = beanType.java.getDeclaredConstructor(*paramTypes)
                logger.trace("Try to create beanType with parameters ${params}")
                return constructor.newInstance(*params)
            } catch (e: NoSuchMethodException) {
                return tryAllConstructors(beanType, params)
            }
        } catch (e: NoSuchMethodException) {
            throw DtoCreationException("Dto ${beanType.simpleName} cannot find the constructor.", e)
        } catch (e: IllegalArgumentException) {
            throw DtoCreationException(
                "Dto {beanType.name} cannot be created because of argument type mismatch", e
            )
        } catch (e: Exception) {
            throw DtoCreationException("Dto ${beanType.simpleName} cannot be created with parameter-constructor", e)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : Any> tryAllConstructors(beanType: KClass<T>, params: Array<out Any?>): T {
        val constructors = beanType.constructors
        for (constructor in constructors) {
            try {
                return constructor.call(*params)
            } catch (e: Exception) {
                logger.warn("Constructor $constructor cannot be used for instantiating $beanType with $params")
            }
        }
        throw DtoCreationException("Dto ${beanType.simpleName} cannot find the constructor.")
    }


    @Suppress("UNCHECKED_CAST")
    fun <T : Any> factoryConstruction(beanType: KClass<T>, params: Array<out Any?>): T {
        val commandType = getCreationStrategy(beanType)?.creationClass!!
        return innerFactoryConstruction(beanType, commandType, params)
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : Any, C : Any> innerFactoryConstruction(
        beanType: KClass<T>,
        commandType: KClass<C>,
        params: Array<out Any?>
    ): T {
        val factory = getObjectFactory(beanType, commandType)
        try {
            val parameter = commandType.java.cast(params[0])
            return factory.create(parameter)
        } catch (e: ClassCastException) {
            throw DtoCreationException(
                "Dto {beanType.name} cannot be created because of argument type mismatch", e
            )
        }
    }

    override fun <T : Any, C : Any> getObjectFactory(beanType: KClass<T>, command: KClass<C>): ObjectFactory<T, C> {
        val resolvableType = ResolvableType.forClassWithGenerics(ObjectFactory::class.java, beanType.java, command.java)
        val beanProvider = context.getBeanProvider<ObjectFactory<T, C>>(resolvableType)
        val objectFactory = beanProvider.ifAvailable

        if (objectFactory == null) {
            throw BeanCreationException("ObjectFactory could not be found for generics $beanType and $command.")
        }
        return objectFactory
    }


    /**
     * Convert a string to a type
     *
     * @param inputString the input
     * @param type        the Type to convert to
     * @param <T>         the Type to convert to
     * @return an object of type T.
     */
    fun <T : Any> convert(inputString: String?, type: KClass<T>): T {
        return objectMapper.fromJson(inputString, type.java)
    }

    init {
        objectMapper = Gson()
    }
}

