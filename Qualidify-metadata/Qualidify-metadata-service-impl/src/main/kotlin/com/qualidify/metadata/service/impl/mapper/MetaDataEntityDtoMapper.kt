package com.qualidify.metadata.service.impl.mapper

import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.DtoMapper
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.DtoDef
import com.qualidify.metadata.service.dto.RendererInfo
import com.qualidify.model.entities.metadata.MetadataColumn
import com.qualidify.model.entities.metadata.MetadataObject

/**
 * Mapper for mapping [MetadataObject] model entities to and from [DtoDef] objects
 *
 * @author Marcel Pot
 * @since 1.0
 */
@DtoMapper
class MetadataEntityDtoMapper() :
    EntityDtoMapper<MetadataObject, DtoDef> {

    override fun convertToModel(dto: DtoDef): MetadataObject {
        val columns = dto.columns.asSequence()
            .map {
                MetadataColumn(
                    mnemonic = dto.mnemonic,
                    name = it.name,
                    description = it.description,
                    dataType = it.dataType,
                    attributes = it.attributes,
                    sortorder = it.sortOrder,
                    hidden = it.hidden,
                    viewable = it.viewable,
                    editable = it.editable,
                    searchable = it.searchable,
                    sortable = it.sortable,
                    segments = it.segments.copyOf(),
                    renderer = it.rendererInfo?.name,
                    rendererAttributes = it.rendererInfo?.attributes?.toMap() ?: emptyMap()
                )
            }
            .toMutableList()

        return MetadataObject(
            mnemonic = dto.mnemonic,
            singlename = dto.name,
            pluralname = dto.pluralName,
            addable = dto.addEnabled,
            updatable = dto.updateEnabled,
            removable = dto.deleteEnabled,
            searchable = dto.searchEnabled,
            columns = columns
        )
    }

    override fun convertToDto(entity: MetadataObject): DtoDef {
        val columns = entity.columns.asSequence()
            .map {
                ColumnDef(
                    name = it.name,
                    description = it.description,
                    dataType = it.dataType,
                    attributes = it.attributes.toMap(),
                    hidden = it.hidden,
                    viewable = it.viewable,
                    editable = it.editable,
                    searchable = it.searchable,
                    sortable = it.sortable,
                    sortOrder = it.sortorder,
                    mnemonic = it.mnemonic,
                    segments = it.segments.copyOf(),
                    rendererInfo = if (it.renderer != null) RendererInfo(
                        it.renderer!!,
                        it.rendererAttributes.toMap()
                    ) else null
                )
            }
            .sortedBy { it.sortOrder }
            .toList()

        val dto = DtoDef(
            mnemonic = entity.mnemonic!!,
            name = entity.singlename,
            pluralName = entity.pluralname,
            addEnabled = entity.addable,
            updateEnabled = entity.updatable,
            deleteEnabled = entity.removable,
            searchEnabled = entity.searchable,
            columns = columns.toMutableList()
        )
        return dto
    }
}

