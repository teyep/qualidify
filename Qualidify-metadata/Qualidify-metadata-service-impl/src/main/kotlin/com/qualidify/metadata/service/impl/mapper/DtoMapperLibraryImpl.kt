package com.qualidify.metadata.service.impl.mapper

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.metadata.annotations.Mnemonic
import com.qualidify.metadata.service.DtoMapperLibrary
import com.qualidify.metadata.service.ObjectConverter
import com.qualidify.metadata.service.exceptions.EntityMapperNotFoundException
import com.qualidify.metadata.service.impl.ObjectConverterImpl
import java.text.MessageFormat
import kotlin.reflect.KClass

/**
 * A Collection of entity to dto mappers with indices on
 *  *  dto's
 *  *  entities
 *  *  mnemonics
 *
 * @author Marcel Pot
 * @since 1.0
 */
open class DtoMapperLibraryImpl(mappers: List<EntityDtoMapper<*, *>>) : DtoMapperLibrary {

    private val dtoMappers: MutableMap<KClass<*>, EntityDtoMapper<*, *>> = HashMap()
    private val entityMappers: MutableMap<KClass<*>, EntityDtoMapper<*, *>> = HashMap()
    private val mnemonicMap: MutableMap<String, Pair<KClass<*>, KClass<*>>> = HashMap()

    private val objectConverter: ObjectConverter = ObjectConverterImpl()

    init {
        mappers.forEach { fillMaps(it) }
    }

    /**
     * Map a dto to an entity based on registered mappers
     * @param dto the dto
     * @param D the type of the dto
     * @pararm E the type of the entity
     * @return the entity
     */
    override fun <E : Any, D> convertToEntity(dto: D): E  where D : Any, D : HasIdentity {
        return getMapperByDto<E, D>(dto).convertToModel(dto)
    }

    /**
     * Map an entity to a dto based on registered mappers
     * @param entity the entity
     * @param D the type of the dto
     * @pararm E the type of the entity
     * @return the dto
     */
    override fun <E : Any, D> convertToDto(entity: E): D where D : Any, D : HasIdentity {
        return getMapperByEntity<E, D>(entity).convertToDto(entity)
    }

    /**
     * convert a PojoMap combined with a mnemonic to an Entity
     * @param mnemonic the Mnemonic
     * @param map the PojoMap
     * @param E The type of the entity
     * @return the Dto
     */
    override fun <E : Any> convertFromPojoMapToEntity(mnemonic: String, map: Map<String, Any?>): E {
        val convertToClass = getDtoClassFromMnemonic(mnemonic)
        val pojo = objectConverter.convertToObject(map, convertToClass)
        return convertToEntity(pojo as HasIdentity)
    }

    /**
     * Determine which entity is used for a mnemonic
     * @param mnemonic the mnemonic
     * @return the entity class belonging to this entity
     */
    override fun getEntityClassFromMnemonic(mnemonic: String): KClass<*> {
        if (!mnemonicMap.containsKey(mnemonic)) {
            throw EntityMapperNotFoundException("Entity mapper for mnemonic $mnemonic not found!")
        }
        return mnemonicMap[mnemonic]!!.first
    }

    /**
     * Determine which entity is used for a mnemonic
     * @param mnemonic the mnemonic
     * @return the entity class belonging to this entity
     */
    override fun getDtoClassFromMnemonic(mnemonic: String): KClass<*> {
        if (!mnemonicMap.containsKey(mnemonic)) {
            throw EntityMapperNotFoundException("Entity mapper for mnemonic $mnemonic not found!")
        }
        return mnemonicMap[mnemonic]!!.second
    }

    @Suppress("UNCHECKED_CAST")
    private fun <E : Any, D> getMapperByEntity(entity: E): EntityDtoMapper<E, D> where D : Any, D : HasIdentity {
        if (!entityMappers.containsKey(entity::class)) {
            throw EntityMapperNotFoundException(
                MessageFormat.format(
                    "Entity mapper for entity class {0} not found!",
                    entity
                )
            )
        }
        return entityMappers[entity::class] as EntityDtoMapper<E, D>
    }


    private fun fillMaps(dtoMapper: EntityDtoMapper<*, *>) {
        var dtoType: KClass<*>? = null
        var entityType: KClass<*>? = null
        for (m in dtoMapper.javaClass.methods) {
            if (m.name == "convertToModel" && m.parameterTypes[0].canonicalName != "java.lang.Object") {
                dtoType = m.parameterTypes[0].kotlin
            }
            if (m.name == "convertToDto" && m.parameterTypes[0].canonicalName != "java.lang.Object") {
                entityType = m.parameterTypes[0].kotlin
            }
        }
        if (dtoType != null) {
            dtoMappers[dtoType] = dtoMapper
        }
        if (entityType != null) {
            entityMappers[entityType] = dtoMapper
            if (dtoType != null) {
                val annotation = dtoType.java.getAnnotation(Mnemonic::class.java)
                if (annotation != null) {
                    mnemonicMap[annotation.value] = entityType to dtoType
                } else {
                    mnemonicMap[dtoType.simpleName!!.replace("Dto", "")] = entityType to dtoType
                }
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <E : Any, D> getMapperByDto(dto: D): EntityDtoMapper<E, D> where D : Any, D : HasIdentity {
        if (!dtoMappers.containsKey(dto::class)) {
            throw EntityMapperNotFoundException(
                MessageFormat.format("Entity mapper for dto class {0} not found!", dto)
            )
        }
        return dtoMappers[dto::class] as EntityDtoMapper<E, D>
    }


}


