package com.qualidify.metadata.service.impl.configuration

import com.qualidify.core.db.multitenancy.TenantsProperties
import com.qualidify.core.jpa.annotations.EnableSoftDeleteJpaRepositories
import com.qualidify.core.jpa.mapper.EntityDtoMapper
import com.qualidify.core.servicelocator.ApplicationContextServiceResolver
import com.qualidify.core.servicelocator.ServiceLocator
import com.qualidify.core.servicelocator.ServiceResolver
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.MnemonicRegistry
import com.qualidify.metadata.service.impl.MetadataInjector
import com.qualidify.metadata.service.impl.MetadataServiceImpl
import com.qualidify.metadata.service.impl.MnemonicRegistryImpl
import com.qualidify.metadata.service.impl.harvest.AnnotationHarvester
import com.qualidify.metadata.service.impl.harvest.MetadataHarvester
import com.qualidify.metadata.service.impl.mapper.DtoMapperLibraryImpl
import com.qualidify.metadata.service.impl.mapper.MetadataEntityDtoMapper
import com.qualidify.metadata.service.impl.repositories.*
import com.qualidify.metadata.service.repository.CustomMetadataEntityRepository
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.concurrent.ConcurrentMapCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory

@Configuration
@EnableSoftDeleteJpaRepositories(basePackages = ["com.qualidify.metadata.service.impl.repositories"])
@EnableConfigurationProperties(TenantsProperties::class)
@EnableCaching
open class MetadataServiceConfiguration {

    /**
     * A Registry of all mnemonics pointing to a dto class
     * @return the MnemonicRegistry
     */
    @Bean
    open fun mnemonicRegistry() = MnemonicRegistryImpl(metaDataHarvester())

    /**
     * An Injector used for scanning the classpath and injecting the found
     * Dto classes as dto definitions into a repository
     * @param harvester the metadata harvester
     * @param repository the repository to push to
     * @param registry the metadata register keeping track of mnemnonics and kclasses
     * @param mapper a mapper between [DtoDef]s and [MetadataObject]
     * @return an Injector bean
     */
    @Bean
    open fun metadataInjector(
        harvester: MetadataHarvester,
        repository: MetadataObjectRepository,
        registry: MnemonicRegistry,
        mapper: MetadataEntityDtoMapper
    ) = MetadataInjector(harvester, repository, registry, mapper)

    /**
     * A Service for handling metadata requests
     * @param metadataRepository the repository for metadata
     * @param customFieldRepository the customfield configuration repository
     * @param mnemonicRegistry the register keeping track of mnemonic -> kclass coupling
     * @return an implementation of the MetaDataService
     */
    @Bean
    open fun metadataService(
        metadataRepository: MetadataObjectRepository,
        customFieldRepository: CustomFieldRepository,
        mnemonicRegistry: MnemonicRegistry
    ): MetadataService =
        MetadataServiceImpl(metadataRepository, customFieldRepository, mnemonicRegistry)

    /**
     * A Library of all DtoMappers registered in the application context
     * @param mappers a list of all registered mappers implementing the interface [EntityDtoMapper]
     * @return the DtoMapperLibrary
     */
    @Bean
    open fun dtoMapperLibrary(mappers: List<EntityDtoMapper<*, *>>): DtoMapperLibraryImpl {
        return DtoMapperLibraryImpl(mappers)
    }

    @Bean
    open fun customMetadataEntityRepository(
        @Qualifier("metadataServiceEntityManager") entityManager: EntityManager,
        dtoMapperLibrary: DtoMapperLibraryImpl,
    ): CustomMetadataEntityRepository {
        return MnemonicEntityRepository(entityManager, dtoMapperLibrary)
    }

    @Primary
    @Bean
    open fun metadataServiceEntityManager(entityManagerFactory: EntityManagerFactory): EntityManager {
        return entityManagerFactory.createEntityManager()
    }

    @Bean
    @ConditionalOnMissingBean(ServiceLocator::class)
    open fun serviceLocator(resolver: ServiceResolver): ServiceLocator {
        ServiceLocator.INSTANCE.setResolver(resolver)
        return ServiceLocator.INSTANCE
    }

    @Bean
    @ConditionalOnMissingBean(ServiceResolver::class)
    open fun ServiceResolver(): ServiceResolver {
        return ApplicationContextServiceResolver()
    }


    @Bean
    open fun metaDataHarvester(): MetadataHarvester {
        return AnnotationHarvester()
    }

    @Bean
    open fun metadataCacheManager(): CacheManager {
        return ConcurrentMapCacheManager("metadata", "mnemonic", "actions")
    }


}