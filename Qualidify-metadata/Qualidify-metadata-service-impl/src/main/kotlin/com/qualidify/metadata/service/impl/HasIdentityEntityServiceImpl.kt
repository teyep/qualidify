package com.qualidify.metadata.service.impl

import com.qualidify.core.identity.HasIdentity
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.metadata.service.DtoMapperLibrary
import com.qualidify.metadata.service.HasIdentityEntityService
import com.qualidify.metadata.service.MetadataValidationService
import com.qualidify.metadata.service.ObjectConverter
import com.qualidify.metadata.service.repository.CustomMetadataEntityRepository
import com.qualidify.metadata.service.repository.countResponse
import com.qualidify.metadata.service.repository.findAllByQuery
import org.springframework.stereotype.Service
import java.io.Serializable
import javax.transaction.Transactional

/**
 * An implementation of the [HasIdentityEntityService]
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
@Transactional
open class HasIdentityEntityServiceImpl(
    private val validator: MetadataValidationService,
    private val customMetadataEntityRepository: CustomMetadataEntityRepository,
    private val dtoMapperLibrary: DtoMapperLibrary,
) : HasIdentityEntityService {

    val objectConverter: ObjectConverter = ObjectConverterImpl()

    fun save(mnemonic: String, dtoMap: Map<String, Any?>) {
        val entity = dtoMapperLibrary.convertFromPojoMapToEntity<Any>(
            mnemonic = mnemonic,
            map = dtoMap
        )
        customMetadataEntityRepository.save(entity)
    }

    override fun create(mnemonic: String, hasIdentity: HasIdentity) {
        validator.validate(mnemonic, hasIdentity)
        save(mnemonic, objectConverter.convertToMapWithCustomFields(hasIdentity, hasIdentity::class))
    }

    fun update(mnemonic: String, dtoMap: Map<String, Any?>) {
        val entity = dtoMapperLibrary.convertFromPojoMapToEntity<Any>(
            mnemonic = mnemonic,
            map = dtoMap
        )
        customMetadataEntityRepository.update(entity)
    }

    override fun update(mnemonic: String, hasIdentity: HasIdentity) {
        validator.validate(mnemonic, hasIdentity)
        update(mnemonic, objectConverter.convertToMapWithCustomFields(hasIdentity, hasIdentity::class))
    }

    fun delete(mnemonic: String, dtoMap: Map<String, Any?>) {
        val entity = dtoMapperLibrary.convertFromPojoMapToEntity<Any>(
            mnemonic = mnemonic,
            map = dtoMap
        )
        customMetadataEntityRepository.delete(entity)
    }

    override fun delete(mnemonic: String, hasIdentity: HasIdentity) {
        validator.validate(mnemonic, hasIdentity)
        delete(mnemonic, objectConverter.convertToMapWithCustomFields(hasIdentity, hasIdentity::class))
    }

    override fun <E : Serializable> countByQuery(mnemonic: String, query: CountQuery): CountResponse {
        return customMetadataEntityRepository.countResponse<E>(mnemonic, query)
    }

    override fun <D : HasIdentity, E : Serializable> findByQuery(mnemonic: String, query: SearchQuery): Collection<D> {
        return customMetadataEntityRepository.findAllByQuery<E>(mnemonic, query)
            .map { e -> dtoMapperLibrary.convertToDto<E, D>(e) }
            .toList()
    }
}