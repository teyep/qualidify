package com.qualidify.metadata.service.impl.harvest

import com.qualidify.metadata.service.dto.DtoDef
import kotlin.reflect.KClass


/**
 * An interface for Metadata Harvesting
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface MetadataHarvester {

    /**
     * Determine the metadata for a class Type (dto)
     * @param[dto] the class to harvest
     * @return the metadata
     */
    fun harvest(dto: KClass<*>): DtoDef

    /**
     * Scan the classpath for occurences of annotated Dto's
     * @param[packageName] the base package to scan
     * @return a map of mnemonic to Classes
     */
    fun scan(packageName: String): Map<String, KClass<*>>

    /**
     * Determine the mnemonic of a given class
     * @param[dto] the class to harvest
     * @return the mnemonic
     */
    fun mnemonic(dto: KClass<*>): String
}