package com.qualidify.metadata.service.impl

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.DataType
import com.qualidify.metadata.Reference
import com.qualidify.metadata.annotations.CreationStrategyPattern
import com.qualidify.metadata.dto.CustomizableDto
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.MnemonicRegistry
import com.qualidify.metadata.service.dto.*
import com.qualidify.metadata.service.exceptions.MetadataNotFoundException
import com.qualidify.metadata.service.impl.harvest.MetadataStructureUtils
import com.qualidify.metadata.service.impl.mapper.CustomFieldDtoMapper
import com.qualidify.metadata.service.impl.mapper.MetadataEntityDtoMapper
import com.qualidify.metadata.service.impl.repositories.CustomFieldRepository
import com.qualidify.metadata.service.impl.repositories.MetadataObjectRepository
import org.springframework.cache.annotation.Cacheable
import java.util.function.Predicate
import javax.transaction.Transactional

/**
 * Implementation of the MetaDataService to give information about the metadata
 * configuration of specific classes
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Transactional
open class MetadataServiceImpl(
    private val metadataRepository: MetadataObjectRepository,
    private val customFieldRepository: CustomFieldRepository,
    private val mnemonicRegistry: MnemonicRegistry,
) :
    MetadataService {

    val metadataMapper = MetadataEntityDtoMapper()
    val customFieldDtoMapper = CustomFieldDtoMapper()


    private val logger by LoggerDelegation()

    override fun getSingularName(mnemonic: String): String? {
        return getDtoDefinition(mnemonic).name
    }

    override fun getPluralName(mnemonic: String): String? {
        return getDtoDefinition(mnemonic).pluralName
    }

    override fun isAdditionEnabled(mnemonic: String): Boolean {
        return getDtoDefinition(mnemonic).addEnabled
    }

    override fun isUpdateEnabled(mnemonic: String): Boolean {
        return getDtoDefinition(mnemonic).updateEnabled
    }

    override fun isDeleteEnabled(mnemonic: String): Boolean {
        return getDtoDefinition(mnemonic).deleteEnabled
    }

    override fun isFilteringEnabled(mnemonic: String): Boolean {
        return getDtoDefinition(mnemonic).searchEnabled
    }

    @Cacheable("metadata", cacheManager = "metadataCacheManager")
    override fun getDtoDefinition(mnemonic: String): DtoDef {
        return metadataRepository.findById(mnemonic)
            .map { metadataMapper.convertToDto(it) }
            .map { convertCustomFields(it) }
            .orElseThrow { MetadataNotFoundException("Cannot find metadata with mnemonic $mnemonic.") }
    }


    override fun getColumns(mnemonic: String): List<ColumnDef> {
        return getDtoDefinition(mnemonic).columns
    }

    override fun getCustomFieldsForReference(reference: Reference): List<CustomFieldDef> {
        return customFieldRepository.findAllByReference(reference)
            .map { customFieldDtoMapper.convertToDto(it) }
    }

    override fun getMnemonicList(): List<String> {
        return metadataRepository.findMnemonicList()
    }

    override fun getCreationCommand(mnemonic: String): DtoDef? {
        val kClass = mnemonicRegistry.get(mnemonic)
        if (kClass != null) {
            val creationCommandClass = MetadataStructureUtils.getCreationStrategy(kClass)?.creationClass
            if (creationCommandClass != null) {
                return getDtoDefinition(creationCommandClass.simpleName!!)
            }
        }
        return null
    }

    override fun getCreationStrategyPattern(mnemonic: String): CreationStrategyPattern? {
        val kClass = mnemonicRegistry.get(mnemonic)
        if (kClass != null) {
            return MetadataStructureUtils.getCreationStrategyPattern(kClass)
        }
        return null
    }

    @Cacheable("actions", cacheManager = "metadataCacheManager")
    override fun getActionsForType(mnemonic: String): List<ActionInfo> {
        val kClass = mnemonicRegistry.get(mnemonic)
        if (kClass != null) {
            return MetadataStructureUtils.determineActions(kClass)
        } else {
            return emptyList()
        }
    }

    override fun getBatchActionsForType(mnemonic: String): List<ActionInfo> {
        val kClass = mnemonicRegistry.get(mnemonic)
        if (kClass != null) {
            return MetadataStructureUtils.determineBatchActions(kClass)
        } else {
            return emptyList()
        }
    }


    override fun getRendererForColumn(mnemonic: String, column: String): RendererInfo? {
        val kClass = mnemonicRegistry.get(mnemonic)
        if (kClass != null) {
            return MetadataStructureUtils.determineRenderer(kClass, column)
        }
        return null
    }

    override fun getSegments(mnemonic: String): List<SegmentInfo> {
        val kClass = mnemonicRegistry.get(mnemonic)
        if (kClass != null) {
            //determine based on Segments annotation
            val segmentation = MetadataStructureUtils.determineSegmentation(kClass)
            if (!segmentation.isEmpty()) {
                return segmentation
            }
        }
        //Determine based on Segment (in DtoColumn) annotation
        return getDtoDefinition(mnemonic).columns
            .flatMap { it.segments.asSequence() }
            .map { SegmentInfo(it) }
            .distinct()
    }


    private fun convertCustomFields(dtoDef: DtoDef): DtoDef {
        if (dtoDef.columns.any { it.dataType == DataType.CUSTOM }) {
            val newColumnDef = dtoDef.columns.asSequence()
                .flatMap { convertColumnIfCustom(it) }
                .toList()
            dtoDef.columns.clear()
            dtoDef.columns.addAll(newColumnDef)
        }
        return dtoDef
    }

    private fun convertColumnIfCustom(columnDef: ColumnDef): List<ColumnDef> {
        if (CustomizableColumnPredicate().test(columnDef)) {
            val reference = columnDef.reference
            if (reference != null) {
                return customFieldRepository.findAllByReferenceAndActive(reference)
                    .map {
                        ColumnDef(
                            name = it.field,
                            description = it.description,
                            dataType = it.dataType,
                            attributes = mapOf(),
                            false,
                            true,
                            true,
                            true,
                            fieldAccess = ColumnDef.FieldAccess.MAP
                        )
                    }
            }
        }
        return listOf(columnDef)
    }


    inner class CustomizableColumnPredicate() : Predicate<ColumnDef> {

        override fun test(columnDefinition: ColumnDef): Boolean {
            if (columnDefinition.dataType == DataType.CUSTOM) {
                val beanType = mnemonicRegistry.get(columnDefinition.mnemonic)

                if (beanType == null)
                    return false

                return CustomizableDto::class.java.isAssignableFrom(beanType.java)
            }
            return false
        }


    }
}
