package com.qualidify.metadata.service.impl

import com.google.common.base.Preconditions.checkArgument
import com.google.common.base.Preconditions.checkNotNull
import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import com.qualidify.metadata.service.CustomFieldService
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.metadata.service.exceptions.ConfigurableFieldNotFoundException
import com.qualidify.metadata.service.impl.mapper.CustomFieldDtoMapper
import com.qualidify.metadata.service.impl.repositories.CustomFieldRepository
import com.qualidify.model.entities.metadata.CustomFieldId
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils

/**
 * An implementation of the [CustomFieldService]
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class CustomFieldServiceImpl(
    val customFieldRepository: CustomFieldRepository,
    val mapper: CustomFieldDtoMapper,
) : CustomFieldService {

    override fun create(field: CustomFieldDef) {
        checkArgument(StringUtils.hasText(field.field), "Field must have text.")
        checkArgument(StringUtils.hasText(field.description), "Description should have text.")

        val customField = mapper.convertToModel(field)
        customFieldRepository.save(customField)
    }

    override fun update(field: CustomFieldDef) {
        checkNotNull(field.id, "Field must have an identity")
        checkArgument(StringUtils.hasText(field.field), "Field must have text.")
        checkArgument(StringUtils.hasText(field.description), "Description should have text.")

        val customField = customFieldRepository.findById(CustomFieldId(field.id!!))

        customFieldRepository.save(
            customField.map { cf ->
                cf.also {
                    it.description = field.description
                    it.dataType = field.dataType
                    it.validator = field.validator
                    it.mandatory = field.mandatory
                    it.active = field.active
                }
            }.orElseThrow {
                ConfigurableFieldNotFoundException("Cannot find a Custom Field with id = '${field.id}")
            }
        )

    }

    override fun delete(field: CustomFieldDef) {
        checkNotNull(field.id, "Cannot delete a Field without id.")
        //softDelete is handled by entity annotations
        customFieldRepository.deleteById(CustomFieldId(field.id!!))
    }

    override fun countByQuery(query: CountQuery) = customFieldRepository.countResponse(query)

    override fun findByQuery(query: SearchQuery) = customFieldRepository.findAllByQuery(query).asSequence()
        .map { mapper.convertToDto(it) }
        .toList()
}
