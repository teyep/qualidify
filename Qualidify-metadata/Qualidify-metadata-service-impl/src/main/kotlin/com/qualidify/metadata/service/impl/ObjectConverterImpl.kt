package com.qualidify.metadata.service.impl

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.dto.CustomizableDto
import com.qualidify.metadata.service.Metadata
import com.qualidify.metadata.service.MnemonicRegistry
import com.qualidify.metadata.service.ObjectConverter
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.ObjectMap
import com.qualidify.metadata.service.impl.harvest.MetadataStructureUtils
import org.springframework.core.KotlinReflectionParameterNameDiscoverer
import org.springframework.core.ParameterNameDiscoverer
import org.springframework.stereotype.Service
import org.springframework.util.ReflectionUtils
import kotlin.reflect.KClass


/**
 * A Converter for Object to Map<String, Any> Mapping (and visa versa)
 * It will adjust the map for configurable fields based on the metadataconfiguration
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class ObjectConverterImpl : ObjectConverter {

    val objectMapper: Gson
    val discoverer: ParameterNameDiscoverer

    private val logger by LoggerDelegation()

    init {
        objectMapper = GsonBuilder()
            .serializeNulls()
            .create()
        discoverer = KotlinReflectionParameterNameDiscoverer()
    }

    /**
     * Get a Map with all properties of an object
     * @param bean the object to read
     * @param T the type of the bean
     * @return a Map with all fields and their values
     */
    override fun <T> convertToMap(bean: T): ObjectMap {
        val beantype: Class<*> = bean!!::class.java
        val map = ObjectMap()
        for (field in beantype.getDeclaredFields()) {
            val value: Any? = getFieldValue(bean, field.name)
            map[field.name] = value
        }
        return map
    }

    /**
     * Get the value of a field
     * @param T the type of the bean
     * @param fieldname the name of the field to read
     * @return the value of the field
     */
    fun <T> getFieldValue(bean: T, fieldname: String): Any? {
        return ReflectionUtils.findField(bean!!::class.java, fieldname).let { field ->
            field?.isAccessible = true
            return@let field?.get(bean)
        }
    }


    /**
     * Map an object to a Map
     * @param bean the object to map
     * @param beanType the class of the object
     * @param T the type of the object
     * @return an objectMap containing all fields as a key value pair
     */
    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> convertToMapWithCustomFields(bean: T, beanType: KClass<out T>): ObjectMap {
        val map = convertToMap(bean)
        // specific behaviour for custom fields
        MetadataStructureUtils.determineCustomField(beanType)
            ?.let { field ->
                (map.get(field.name) as Map<String, Any>)
                    .asSequence()
                    .forEach { map.put(it.key, it.value) }
                map.remove(field.name)
            }
        return map
    }


    /**
     * Map a DtoMap to a Dto object
     * @param map an objectMap
     * @param beanType the kotlin class of the object
     * @param T the type of the object
     * @return the dto object
     */
    override fun <T : Any> convertToObject(map: Map<String, Any?>, beanType: KClass<T>): T {
        return tryConstructorCreation(map, beanType) ?: innerConvertToObject(map, beanType)
    }


    private fun <T : Any> innerConvertToObject(map: Map<String, Any?>, beanType: KClass<T>): T {
        val jsonTree = objectMapper.toJsonTree(map)
        val pojo = objectMapper.fromJson(jsonTree, beanType.java)
        val mnemonic = MnemonicRegistry.get().mnemonicOf(beanType)!!

        // specific behaviour for custom fields
        if (pojo is CustomizableDto) {
            Metadata.getColumns(mnemonic)
                .asSequence()
                .filter { it.fieldAccess == ColumnDef.FieldAccess.MAP }
                .forEach { pojo.set(it.name, map.get(it.name).toString()) }
        }
        return pojo;
    }


    private fun <T : Any> tryConstructorCreation(map: Map<String, Any?>, beanType: KClass<T>): T? {
        val constructors = beanType.constructors
            .asSequence()
            .sortedByDescending { it.parameters.size }
            .toList()
        for (constructor in constructors) {

            val parameters = constructor.parameters
                .asSequence()
                .map { it to map.get(it.name) }
                .toMap()

            try {
                return constructor.callBy(parameters)
            } catch (e: Exception) {
                logger.warn("Cannot create object ${beanType} with parameters : ${parameters}")
            }
        }
        return null
    }

}



