package com.qualidify.metadata.service.impl.harvest

import com.qualidify.core.log.LoggerDelegation
import com.qualidify.metadata.DataType
import com.qualidify.metadata.annotations.*
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.DtoDef
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang3.reflect.FieldUtils
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.annotation.AnnotatedElementUtils
import org.springframework.core.type.filter.AnnotationTypeFilter
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.Collectors
import kotlin.reflect.KClass

/**
 * A class for scanning for specific metadata annotations on dto's
 * and storing this in MetaData objects
 *
 * @author Marcel Pot
 */
open class AnnotationHarvester : MetadataHarvester {

    private val orderCounter = AtomicInteger(Int.MIN_VALUE)

    private val logger by LoggerDelegation()

    override fun harvest(dto: KClass<*>): DtoDef {
        logger.trace("Harvesting metadata for ${dto.qualifiedName}")

        val annotation = AnnotatedElementUtils.getMergedAnnotation(dto.java, DtoMetadata::class.java)
        val mnemonic = mnemonic(dto)

        return DtoDef(
            mnemonic = mnemonic,
            name = annotation?.singular ?: mnemonic,
            pluralName = annotation?.plural ?: "${mnemonic}s",
            addEnabled = annotation?.enableAddition ?: true,
            updateEnabled = annotation?.enableUpdate ?: true,
            deleteEnabled = annotation?.enableDelete ?: true,
            searchEnabled = annotation?.enableFiltering ?: true,
            columns = harvestFields(mnemonic, dto)
        )
    }

    override fun scan(packageName: String): Map<String, KClass<*>> {
        logger.info("Scanning $packageName for occurences of metadata annotated classes.")
        val map = mutableMapOf<String, KClass<*>>()
        val scanner = ClassPathScanningCandidateComponentProvider(false)
        scanner.addIncludeFilter(AnnotationTypeFilter(DtoMetadata::class.java))
        scanner.addIncludeFilter(AnnotationTypeFilter(DtoCommand::class.java))

        scanner.findCandidateComponents(packageName).forEach {
            val kClass = determineKClass(it)
            val mnemonic = mnemonic(kClass)
            logger.info("Found metadata: mnemonic $mnemonic pointing to ${kClass.qualifiedName}")
            map[mnemonic] = kClass
        }

        return map
    }

    private fun determineKClass(bd: BeanDefinition): KClass<*> {
        return try {
            Class.forName(bd.beanClassName).kotlin
        } catch (e: ClassNotFoundException) {
            throw RuntimeException("No class found with name " + bd.beanClassName, e)
        }
    }

    override fun mnemonic(kClass: KClass<*>): String {
        val mnemonic = AnnotatedElementUtils.findMergedAnnotation(kClass.java, Mnemonic::class.java)
        if (mnemonic != null) {
            return mnemonic.value
        } else {
            return kClass.simpleName!!.removeSuffix("Dto")
        }
    }

    private fun harvestFields(mnemonic: String, dto: KClass<*>): MutableList<ColumnDef> {
        return FieldUtils.getAllFieldsList(dto.java).stream()
            .filter { field: Field -> isNotHidden(field) }
            .filter { field: Field -> isNotStatic(field) }
            .map { f: Field -> createColumnDefinition(mnemonic, dto, f) }
            .sorted(Comparator.comparingInt(ColumnDef::sortOrder))
            .collect(Collectors.toList())
    }

    private fun isNotHidden(field: Field): Boolean {
        return field.getAnnotation(Hidden::class.java) == null
    }

    private fun isNotStatic(field: Field): Boolean {
        return !Modifier.isStatic(field.modifiers)
    }

    private fun createColumnDefinition(mnemonic: String, dto: KClass<*>, field: Field): ColumnDef {
        val dtoColumn = AnnotatedElementUtils.getMergedAnnotation(field, DtoColumn::class.java)
        val dtoType = AnnotatedElementUtils.getMergedAnnotation(field, DtoType::class.java)

        val dataType: DataType = dtoType?.value ?: DataTypeMapper.determineDataType(field)

        val columnName = field.name.replace("\$delegate", "")
        val columnDescription =
            if (!StringUtils.isEmpty(dtoColumn?.description)) dtoColumn!!.description else columnName

        return ColumnDef(
            mnemonic = mnemonic,
            name = columnName,
            description = columnDescription,
            dataType = dataType,
            attributes = determineAttributes(field, dataType),
            hidden = dtoColumn?.hidden ?: false,
            viewable = dtoColumn?.viewable ?: true,
            editable = dtoColumn?.editable ?: true,
            searchable = dtoColumn?.searchable ?: true,
            sortable = dtoColumn?.sortable ?: true,
            sortOrder = determineSortOrder(dtoColumn),
            segments = translateSegments(dtoColumn),
            fieldAccess = translateFieldAccess(dataType),
            rendererInfo = determineRendererInfo(dto, columnName)
        )
    }

    private fun determineAttributes(
        field: Field,
        dataType: DataType
    ) = MetadataStructureUtils.determineTypeAttributes(field, dataType)


    private fun determineSortOrder(dtoColumn: DtoColumn?) =
        dtoColumn?.order ?: orderCounter.incrementAndGet()

    private fun translateFieldAccess(dataType: DataType) =
        if (isCustomType(dataType)) ColumnDef.FieldAccess.MAP else ColumnDef.FieldAccess.DEFAULT

    private fun translateSegments(dtoColumn: DtoColumn?) =
        dtoColumn?.segments?.map { it.value }?.toTypedArray() ?: arrayOf("DEFAULT")

    private fun isCustomType(dataType: DataType): Boolean {
        return DataType.CUSTOM.equals(dataType)
    }

    private fun determineRendererInfo(
        dto: KClass<*>,
        columnName: String
    ) = MetadataStructureUtils.determineRenderer(dto, columnName)


}