package com.qualidify.metadata.service.impl

import com.qualidify.metadata.service.MnemonicRegistry
import com.qualidify.metadata.service.impl.harvest.MetadataHarvester
import kotlin.reflect.KClass

/**
 * Registry of all meta data annotated dto's
 * @param harvester the Harvester to read the classpath
 *
 * @author Marcel Pot
 */
class MnemonicRegistryImpl(val harvester: MetadataHarvester) : MnemonicRegistry {

    var packageName = "com.qualidify"

    private val map: MutableMap<String, KClass<*>>


    init {
        map = harvester.scan(packageName).toMutableMap()
    }

    /**
     * Get the Kotlin class beloning to a mnemonic
     * @param[menmonic] the Mnemonic
     * @return the Kotlin Class
     */
    override fun get(menmonic: String) = map[menmonic]

    /**
     * Get the entriesset of this registry
     * @return the entrieset with mnemonic pointing to kotlin classes
     */
    override val entries: Set<Map.Entry<String, KClass<*>>>
        get() = map.toMap().entries

    /**
     * Get the mnemonic of a class
     * @param[kClass] the Kotlin class
     * @return the mnemonic pointing to this kotlin class
     */
    override fun mnemonicOf(kClass: KClass<*>) = map.entries
        .firstOrNull { kClass.equals(it.value) }
        ?.key


    override fun addForScanning(vararg kClasses: KClass<*>) {
        kClasses.forEach {
            map[harvester.mnemonic(it)] = it
        }

    }


}