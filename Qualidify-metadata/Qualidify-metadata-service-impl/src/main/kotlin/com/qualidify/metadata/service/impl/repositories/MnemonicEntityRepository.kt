package com.qualidify.metadata.service.impl.repositories

import com.qualidify.metadata.service.impl.mapper.DtoMapperLibraryImpl
import com.qualidify.metadata.service.repository.CustomMetadataEntityRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.query.QueryUtils
import javax.persistence.EntityManager
import kotlin.reflect.KClass

/**
 * A repository for metadata entities (lookup by their mnemonic)
 * It will mimic the behaviour of the default spring JPA repositories, only in this case
 * it can handle all entities which have a DtoMapper where the Dto has a Mnemonic
 *
 * @author Marcel Pot
 * @since 1.0
 */
open class MnemonicEntityRepository(
    private val entityManager: EntityManager,
    private val dtoMapperLibrary: DtoMapperLibraryImpl,
) : CustomMetadataEntityRepository {

    override fun <E> save(entity: E): E {
        entityManager.persist(entity)
        return entity
    }

    override fun <E> update(entity: E): E {
        entityManager.merge(entity)
        return entity
    }

    override fun <E> delete(entity: E) {
        entityManager.remove(entity)
    }

    override fun <E : Any> findAll(mnemonic: String, specification: Specification<E>?, pageable: Pageable?): Page<E> {
        val entityClass: KClass<E> = getEntity(mnemonic)

        val cb = entityManager.criteriaBuilder
        val cq = cb.createQuery(entityClass.java)
        val root = cq.from(entityClass.java)

        specification?.let {
            cq.where(it.toPredicate(root, cq, cb))
        }
        pageable?.let {
            cq.orderBy(QueryUtils.toOrders(pageable.getSort(), root, cb))
        }

        val typedQuery = entityManager.createQuery(cq)

        pageable?.let {
            typedQuery.firstResult = pageable.getOffset().toInt()
            typedQuery.maxResults = pageable.getPageSize()
        }

        val entityList = typedQuery.resultList
        return PageImpl(entityList)
    }

    override fun <E : Any> countAll(mnemonic: String, specification: Specification<E>?): Long {
        val entityClass: KClass<E> = getEntity(mnemonic)

        val cb = entityManager.criteriaBuilder
        val cq = cb.createQuery(Long::class.java)
        val root = cq.from(entityClass.java)

        cq.select(cb.count(root))

        specification?.let {
            cq.where(it.toPredicate(root, cq, cb))
        }
        return entityManager.createQuery(cq).singleResult
    }

    @Suppress("UNCHECKED_CAST")
    private fun <E : Any> getEntity(mnemonic: String): KClass<E> {
        return dtoMapperLibrary.getEntityClassFromMnemonic(mnemonic) as KClass<E>
    }
}

