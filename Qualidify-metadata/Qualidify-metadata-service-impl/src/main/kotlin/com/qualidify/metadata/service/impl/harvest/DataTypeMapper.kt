package com.qualidify.metadata.service.impl.harvest

import com.qualidify.metadata.DataType
import java.lang.reflect.Field

internal object DataTypeMapper {

    fun determineDataType(field: Field): DataType {
        if (field.type.isEnum) {
            return DataType.ENUMERATION
        } else if (MutableCollection::class.java.isAssignableFrom(field.type)) {
            return DataType.COLLECTION
        } else if (Map::class.java.isAssignableFrom(field.type)) {
            return DataType.MAP
        } else {
            return when (field.type.simpleName.lowercase()) {
                "boolean" -> DataType.BOOLEAN
                "int", "integer", "long", "biginteger" -> DataType.INTEGER
                "float", "double" -> DataType.DOUBLE
                "bigdecimal" -> DataType.BIGDECIMAL
                "localdate" -> DataType.DATE
                "localtime" -> DataType.TIME
                "localdatetime" -> DataType.DATETIME
                "uploadfile", "bytearray" -> DataType.BINARY
                else -> DataType.STRING
            }
        }
    }
}