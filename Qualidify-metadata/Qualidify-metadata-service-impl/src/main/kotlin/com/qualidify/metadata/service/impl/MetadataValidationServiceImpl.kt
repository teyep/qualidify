package com.qualidify.metadata.service.impl

import com.qualidify.core.identity.HasIdentity
import com.qualidify.metadata.Reference
import com.qualidify.metadata.service.MetadataService
import com.qualidify.metadata.service.MetadataValidationService
import com.qualidify.metadata.service.ObjectConverter
import com.qualidify.metadata.service.dto.ColumnDef
import com.qualidify.metadata.service.dto.CustomFieldDef
import com.qualidify.metadata.service.exceptions.MetadataValidationException
import org.springframework.stereotype.Service
import java.util.function.Consumer

/**
 * An implementation of the [MetadataValidationService]
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Service
class MetadataValidationServiceImpl
    (
    val metadataService: MetadataService,
) : MetadataValidationService {

    val objectConverter: ObjectConverter = ObjectConverterImpl()


    override fun validate(mnemonic: String, dto: HasIdentity) {
        val columnMetaData: Collection<ColumnDef> = metadataService.getColumns(mnemonic)
        columnMetaData.forEach(Consumer { cmd: ColumnDef ->
            validateColumn(
                dto,
                cmd
            )
        })
    }

    override fun checkAllMandatoryCustomFieldsArePresent(reference: Reference, fields: Map<String, *>) {
        val customfieldDefinitions = getCustomFieldsForReference(reference)

        val allMandatoryFieldsArePresent = customfieldDefinitions.values.asSequence()
            .filter { it.mandatory }
            .map { it.field }
            .all { fields.containsKey(it) }

        if (!allMandatoryFieldsArePresent) {
            throw MetadataValidationException("Not all mandatory fields are present")
        }
    }

    override fun checkAllCustomFieldsAreAllowed(reference: Reference, fields: Map<String, *>) {
        val customfieldDefinitions = getCustomFieldsForReference(reference)

        val allCustomFieldsAreAllowed = fields.keys.asSequence()
            .all { customfieldDefinitions.containsKey(it) }

        if (!allCustomFieldsAreAllowed) {
            throw MetadataValidationException("Not all custom fields are allowed")
        }
    }

    private fun getCustomFieldsForReference(reference: Reference): Map<String, CustomFieldDef> {
        return metadataService.getCustomFieldsForReference(reference)
            .asSequence()
            .associateBy { it.field }
    }

    private fun validateColumn(dto: HasIdentity, column: ColumnDef) {
        val map = objectConverter.convertToMap(dto)
        val value = map[column.name]
        val dataType = column.dataType

        if (!dataType.isRevertable(value.toString())) {
            throw MetadataValidationException("DataType is not of the right type.")
        }
    }


}