package com.qualidify.core.identity

import java.io.Serializable

/**
 * Interface for having an identity on an object
 * based on which can be determined if this object is unique
 * in comparison with other objects
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface HasIdentity : Serializable {

    /**
     * Returns the identity of an object
     * @return the identy
     */
    fun identity(): Array<Any>
}