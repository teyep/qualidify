package com.qualidify.core.identity

import java.io.Serializable

/**
 * A class combining the mnemonic with the identity, to also make detect differences between
 * different objects with the same identity
 *
 * @author Marcel Pot
 * @since 1.0
 */
data class MetadataIdentifier(val mnemonic: String, val id: Array<Any>) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MetadataIdentifier) return false

        if (mnemonic != other.mnemonic) return false
        if (!id.contentEquals(other.id)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = mnemonic.hashCode()
        result = 31 * result + id.contentHashCode()
        return result
    }

    override fun toString(): String {
        return "MetadataIdentifier(mnemonic='$mnemonic', id=${id.contentToString()})"
    }

}