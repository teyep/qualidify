package com.qualidify.core.mail

import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service
import java.util.*

/**
 * A Mailservice with an optional implementation
 *
 * @author Marcel Pot
 */
@Service("mailService")
class MailServiceImpl(private val optionalMailSender: Optional<JavaMailSender>) : MailService {

    override fun sendSimpleMessage(to: String, subject: String, text: String) {
        val message = SimpleMailMessage()
        message.from = "noreply@qualidify.com"
        message.setTo(to)
        message.subject = subject
        message.text = text
        optionalMailSender.ifPresent { sender: JavaMailSender -> sender.send(message) }
    }

    override val isAvailable: Boolean
        get() = optionalMailSender.isPresent
}