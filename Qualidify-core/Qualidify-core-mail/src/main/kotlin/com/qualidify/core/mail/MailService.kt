package com.qualidify.core.mail

/**
 * A MailService for sending emails
 *
 * @author Marcel Pot
 */
interface MailService {
    /**
     * Check if the mailservice is working
     * @return boolean indicating if this service is configured and working
     */
    val isAvailable: Boolean

    /**
     * Send a simple email
     * @param to the Emailadress to write to
     * @param subject the subject of the message
     * @param the text of the message
     */
    fun sendSimpleMessage(to: String, subject: String, text: String)
}