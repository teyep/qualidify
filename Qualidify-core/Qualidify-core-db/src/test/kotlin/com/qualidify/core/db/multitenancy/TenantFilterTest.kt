package com.qualidify.core.db.multitenancy


import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.*
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

internal class TenantFilterTest {
    var capturingTenant: String = ""

    val request: ServletRequest = mock(HttpServletRequest::class.java)

    val response: ServletResponse = mock(ServletResponse::class.java)

    val filterChain: FilterChain = spy(MockFilterChain())

    val fixture = TenantFilter()

    @Test
    fun testTenantFilter() {
        `when`(request.serverName).thenReturn("tenant_3.qualidify.com")

        fixture.doFilter(request, response, filterChain)

        assertThat(capturingTenant).isEqualTo("TENANT_3")

        verify(filterChain, times(1)).doFilter(eq(request), eq(response))
    }

    inner class MockFilterChain : FilterChain {
        override fun doFilter(request: ServletRequest, response: ServletResponse?) {
            capturingTenant = TenantContext.getTenant()
        }

    }

}