package com.qualidify.core.db.multitenancy

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@EnableConfigurationProperties(value = [TenantsProperties::class])
@TestPropertySource(locations = ["classpath:application.yaml"])
@ContextConfiguration(initializers = [ConfigDataApplicationContextInitializer::class])
open class TenantsPropertiesTest {

    @Autowired
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    lateinit var tenantsProperties: TenantsProperties

    @Test
    fun TestLoadingConfigProperties() {
        assertThat(tenantsProperties.defaultTenant).isEqualTo("Tenant_1")
        assertThat(tenantsProperties.tenants).hasSize(2)
        assertThat(tenantsProperties.defaultDatasourceProps()!!.datasource!!.username).isEqualTo("4m1")
    }
}