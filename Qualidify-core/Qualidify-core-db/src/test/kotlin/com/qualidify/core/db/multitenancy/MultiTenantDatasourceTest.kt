package com.qualidify.core.db.multitenancy

import io.mockk.Called
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.sql.Connection
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import javax.sql.DataSource

internal class MultiTenantDatasourceTest {
    companion object {
        const val TENANT_1 = "TENANT_1"
        const val TENANT_2 = "TENANT_2"
    }

    private val datasource1: DataSource = mockk()
    private val datasource2: DataSource = mockk()
    private val defaultDatasource: DataSource = mockk()

    private lateinit var theDatasource: DataSource

    private val numberOfThreadsTenant1 = 13
    private val numberOfThreadsTenant2 = 8
    private val totalNumberOfThreads = numberOfThreadsTenant1 + numberOfThreadsTenant2

    private val blocksReady = CountDownLatch(totalNumberOfThreads)
    private val blockStart = CountDownLatch(1)
    private val blocksDone = CountDownLatch(totalNumberOfThreads)

    @BeforeEach
    fun setup() {
        val multitenantDatasource = MultiTenantDatasource()
        multitenantDatasource.setTargetDataSources(mapOf("TENANT_1" to datasource1, "TENANT_2" to datasource2))
        multitenantDatasource.setDefaultTargetDataSource(defaultDatasource)
        multitenantDatasource.afterPropertiesSet()
        theDatasource = multitenantDatasource
    }

    @Test
    fun testDatabaseRouting() {
        every { datasource1.connection } returns mockConnection()
        every { datasource2.connection } returns mockConnection()

        TenantContext.setTenant(TENANT_1)
        theDatasource.connection
        theDatasource.connection
        TenantContext.clear()
        TenantContext.setTenant(TENANT_2)
        theDatasource.connection
        TenantContext.clear()

        verify(exactly = 2) { datasource1.connection }
        verify(exactly = 1) { datasource2.connection }
        verify { defaultDatasource.connection wasNot Called }
    }

    @Test
    fun testDatabaseRoutingWithoutUppercaseTentants() {
        every { datasource1.connection } returns mockConnection()
        every { datasource2.connection } returns mockConnection()

        TenantContext.setTenant("Tenant_1")
        theDatasource.connection
        theDatasource.connection
        TenantContext.clear()
        TenantContext.setTenant("tenant_2")
        theDatasource.connection
        TenantContext.clear()

        verify(exactly = 2) { datasource1.connection }
        verify(exactly = 1) { datasource2.connection }
        verify { defaultDatasource.connection wasNot Called }
    }

    @Test
    fun testDatabaseRoutingWithoutDefaultFallback() {
        every { defaultDatasource.connection } returns mockConnection()

        TenantContext.setTenant("UnknownTentant")
        theDatasource.connection
        theDatasource.connection
        TenantContext.clear()

        verify { datasource1.connection wasNot Called }
        verify { datasource2.connection wasNot Called }
        verify(exactly = 2) { defaultDatasource.connection }
    }

    @Test
    fun testDatabaseRoutingWithMultipleThreads() {
        every { datasource1.connection } returns mockConnection()
        every { datasource2.connection } returns mockConnection()

        val threads = ArrayList<Runnable>()
        IntRange(1, numberOfThreadsTenant1).forEach { threads.add(runnableGetConnection(TENANT_1)) }
        IntRange(1, numberOfThreadsTenant2).forEach { threads.add(runnableGetConnection(TENANT_2)) }
        threads.shuffle()


        val threadPool = Executors.newFixedThreadPool(totalNumberOfThreads)
        threads.forEach {
            threadPool.submit(it)
        }

        blocksReady.await()
        blockStart.countDown()
        blocksDone.await()

        threadPool.shutdown()

        verify(exactly = numberOfThreadsTenant1) { datasource1.connection }
        verify(exactly = numberOfThreadsTenant2) { datasource2.connection }
        verify { defaultDatasource.connection wasNot Called }
    }

    private fun mockConnection() = mockk<Connection>()

    private fun runnableGetConnection(tenant: String): Runnable {
        return Runnable {
            blocksReady.countDown()
            blockStart.await()

            TenantContext.setTenant(tenant)
            theDatasource.connection
            Thread.sleep(1000)
            TenantContext.clear()

            blocksDone.countDown()

        }
    }

}