package com.qualidify.core.db.multitenancy

import java.io.IOException
import javax.servlet.*
import javax.servlet.http.HttpServletRequest

internal class TenantFilter : Filter {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(
        request: ServletRequest,
        response: ServletResponse?,
        chain: FilterChain
    ) {
        val httpServletRequest: HttpServletRequest = request as HttpServletRequest
//        val tenantName: String = req.getHeader("X-TenantID")
        val tenantName = getSubdomain(httpServletRequest)
        TenantContext.setTenant(tenantName)
        try {
            chain.doFilter(request, response)
        } finally {
            TenantContext.clear()
        }
    }

    private fun getSubdomain(httpServletRequest: HttpServletRequest): String {
        val site = httpServletRequest.serverName
        val siteParts = site.split(".")
        return siteParts[0]
    }
}