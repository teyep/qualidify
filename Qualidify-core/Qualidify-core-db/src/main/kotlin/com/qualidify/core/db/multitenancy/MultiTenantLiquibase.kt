package com.qualidify.core.db.multitenancy

import liquibase.integration.spring.SpringLiquibase
import org.springframework.beans.factory.InitializingBean
import javax.sql.DataSource

/**
 * Liquibase object to support multitenancy, it will route to the correct
 * Datasource based on the users tenant.
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-51
 */
class MultiTenantLiquibase(
) : SpringLiquibase(), InitializingBean {
    var tenants: Collection<String> = emptyList()

    override fun afterPropertiesSet() {
        tenants.asSequence()
            .forEach {
                TenantContext.setTenant(it)
                val liquibase = createLiquibase(dataSource, changeLog, shouldRun)
                liquibase.afterPropertiesSet()
                TenantContext.clear()
            }
    }

    private fun createLiquibase(
        dataSource: DataSource,
        changeLog: String,
        shouldRun: Boolean
    ): SpringLiquibase {
        val liquibase = SpringLiquibase()
        liquibase.changeLog = changeLog
        liquibase.dataSource = dataSource
        liquibase.setShouldRun(shouldRun)
        liquibase.contexts = contexts
        return liquibase
    }

}