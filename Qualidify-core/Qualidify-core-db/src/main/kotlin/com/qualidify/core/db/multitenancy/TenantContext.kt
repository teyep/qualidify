package com.qualidify.core.db.multitenancy

/**
 * Keeps track of the Tenant in a multitenant context.
 * It will use a [ThreadLocal] in the background so the tenant can be
 * different per thread.
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-51
 */
object TenantContext {

    private val contextHolder = ThreadLocal<String>()

    /**
     * Get the current tenant from the current thread
     * @return the current tenant
     */
    fun getTenant() = contextHolder.get()

    /**
     * Set the current tenant in the current thread
     * @param[tenant] the tenant to set
     */
    fun setTenant(tenant: String) = contextHolder.set(tenant.uppercase())

    /**
     * Clear the current tenant in the current thread
     */
    fun clear() = contextHolder.remove()
}