package com.qualidify.core.db.multitenancy

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource

/**
 * Datasource to support multitenancy, it will route to the correct
 * Datasource based on the users tenant.
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-51
 */
class MultiTenantDatasource : AbstractRoutingDataSource() {

    override fun determineCurrentLookupKey() = TenantContext.getTenant()

}