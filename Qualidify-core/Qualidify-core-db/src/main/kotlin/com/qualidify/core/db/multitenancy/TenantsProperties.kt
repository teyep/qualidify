package com.qualidify.core.db.multitenancy


import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

/**
 * A configuration holder for Tenants configuration
 * @author Marcel Pot
 * @since 1.0
 */
@ConfigurationProperties(prefix = "qualidify")
data class TenantsProperties(
    var tenants: Map<String, TenantProps> = emptyMap(),
    var defaultTenant: String = ""
) {

    @ConstructorBinding
    data class TenantProps(
        var datasource: DatasourceProps?
    )

    @ConstructorBinding
    data class DatasourceProps(
        var url: String,
        var driverClassName: String,
        var username: String,
        var password: String
    )

    fun defaultDatasourceProps() = tenants[defaultTenant]

}

