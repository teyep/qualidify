package com.qualidify.core.jpa.repositories

import com.qualidify.core.jpa.annotations.SoftDelete
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation
import org.springframework.data.repository.core.RepositoryInformation
import org.springframework.data.repository.core.RepositoryMetadata
import org.springframework.data.repository.core.support.RepositoryProxyPostProcessor
import javax.persistence.EntityManager

/**
 * Factory for Jpa Repositories implmentations
 * @author Marcel Pot
 * @since 1.0 - QUAL-457
 */
class SoftDeleteJpaRepositoryFactory(entityManager: EntityManager) : JpaRepositoryFactory(entityManager) {

    override fun getRepositoryBaseClass(metadata: RepositoryMetadata): Class<*> {
        return if (checkIfSoftDeleteAnnotationIsPresent(metadata)) {
            SoftDeleteJpaRepository::class.java
        } else super.getRepositoryBaseClass(metadata)
    }


    override fun getTargetRepository(
        information: RepositoryInformation,
        entityManager: EntityManager
    ): JpaRepositoryImplementation<*, *> {
        val targetRepository = super.getTargetRepository(information, entityManager)
        if (targetRepository is SoftDeleteJpaRepository) {
            targetRepository.annotation = retrieveSoftDeleteAnnotation(information)
        }
        return targetRepository
    }

    /**
     * Annotation [SoftDelete] can be present at Repository or Entity
     */
    private fun checkIfSoftDeleteAnnotationIsPresent(metadata: RepositoryMetadata) =
        (metadata.repositoryInterface.isAnnotationPresent(SoftDelete::class.java)
                || metadata.domainType.isAnnotationPresent(SoftDelete::class.java))

    /**
     * Annotation [SoftDelete] can be present at Repository or Entity
     */
    private fun retrieveSoftDeleteAnnotation(information: RepositoryInformation): SoftDelete? =
        information.repositoryInterface.getAnnotation(SoftDelete::class.java) ?: information.domainType.getAnnotation(
            SoftDelete::class.java
        )


    override fun addRepositoryProxyPostProcessor(processor: RepositoryProxyPostProcessor) {
        super.addRepositoryProxyPostProcessor(processor)
    }


}
