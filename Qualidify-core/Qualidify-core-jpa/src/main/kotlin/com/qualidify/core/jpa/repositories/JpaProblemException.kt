package com.qualidify.core.jpa.repositories

/**
 * A Problem occured while doing some JPA Magic
 *
 * @author Marcel Pot
 */
class JpaProblemException(message: String?, cause: Throwable?) : RuntimeException(message, cause) {

    constructor(message: String?) : this(message, null)
    constructor(cause: Throwable?) : this("", cause)

}