package com.qualidify.core.jpa.entities

import com.qualidify.core.jpa.entities.audit.*
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.util.*
import javax.persistence.Embedded
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass

/**
 * An abstract base class for database entities, for collection auditable data
 * by the [AuditingEntityListener]
 *
 * @author Marcel Pot
 * @since 1.0
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractSimpleAuditableBaseEntity(
    @Embedded internal val auditor: Auditor = Auditor(),
) : CreationAuditable by auditor,
    UpdateAuditable by auditor,
    DeleteAuditable by auditor,
    Serializable {

}