package com.qualidify.core.jpa.repositories

import com.qualidify.core.jpa.annotations.SoftDelete
import com.qualidify.core.jpa.entities.AbstractAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractAuditableBaseEntity
import com.qualidify.core.jpa.entities.AbstractSimpleAuditableAndVersionableBaseEntity
import com.qualidify.core.jpa.entities.AbstractSimpleAuditableBaseEntity
import com.qualidify.core.jpa.entities.audit.Auditor
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.provider.PersistenceProvider
import org.springframework.data.jpa.repository.support.JpaEntityInformation
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport
import org.springframework.data.jpa.repository.support.SimpleJpaRepository
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.Assert
import java.io.Serializable
import java.util.*
import java.util.function.Consumer
import javax.persistence.EntityManager
import javax.persistence.TypedQuery
import javax.persistence.criteria.*


/**
 * Override default JPA functionality with SoftDeletion
 */
@SoftDelete
open class SoftDeleteJpaRepository<T, ID : Serializable>(
    val entityInformation: JpaEntityInformation<T, *>,
    val entityManager: EntityManager,
) : SimpleJpaRepository<T, ID>(entityInformation, entityManager) {

    var annotation: SoftDelete? = null

    private val persistenceProvider: PersistenceProvider = PersistenceProvider.fromEntityManager(entityManager)

    constructor(domainClass: Class<T>, entityManager: EntityManager) :
            this(JpaEntityInformationSupport.getEntityInformation(domainClass, entityManager), entityManager)


    private fun getField() = annotation?.column ?: "removedAt"

    private fun getPath(root: Root<*>): Path<Date> {
        val auditorClasses: Set<Class<*>> = setOf(
            AbstractAuditableAndVersionableBaseEntity::class.java,
            AbstractAuditableBaseEntity::class.java,
            AbstractSimpleAuditableAndVersionableBaseEntity::class.java,
            AbstractSimpleAuditableBaseEntity::class.java
        )
        val superClass = root.javaType.getSuperclass()
        return if (auditorClasses.contains(superClass)) {
            root.get<Auditor>("auditor").get("removalDate")
        } else {
            root.get(getField())
        }
    }

    @Suppress("UNCHECKED_CAST")
    @Transactional
    override fun delete(entity: T) {
        Assert.notNull(entity, "Entity must not be null!")
        deleteById(entityInformation.getId(entity) as ID)
    }

    @Transactional
    override fun deleteById(id: ID) {
        Assert.notNull(id, "Identity must not be null!")

        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        val updater: CriteriaUpdate<T> = criteriaBuilder.createCriteriaUpdate(domainClass)
        val root: Root<T> = updater.from(domainClass)
        updater.set(getPath(root), Date())

        updater.where(
            ByIdSpecification(
                id,
                entityInformation
            ).toPredicate(root, null, criteriaBuilder)
        )

        entityManager.createQuery(updater).executeUpdate()
    }

    @Transactional
    override fun deleteAllById(ids: MutableIterable<ID>) {
        //uses deleteById method
        super.deleteAllById(ids)
    }

    @Transactional
    override fun deleteAll(entities: MutableIterable<T>) {
        //uses delete method
        super.deleteAll(entities)
    }

    override fun findById(id: ID): Optional<T> {
        return super.findOne(Specification.where(ByIdSpecification(id, entityInformation)))
    }

    override fun <S : T?> getQuery(spec: Specification<S>?, domainClass: Class<S>, sort: Sort): TypedQuery<S> {
        return super.getQuery(
            if (spec != null) spec.and(DeletedSpecification()) else DeletedSpecification(),
            domainClass,
            sort
        )
    }

    override fun <S : T?> getCountQuery(spec: Specification<S>?, domainClass: Class<S>): TypedQuery<Long> {
        return super.getCountQuery(
            if (spec != null) spec.and(DeletedSpecification()) else DeletedSpecification(),
            domainClass
        )
    }

    private inner class ByIdSpecification<T, ID : Serializable?>(
        private val id: ID,
        private val information: JpaEntityInformation<T, *>
    ) :
        Specification<T> {
        override fun toPredicate(root: Root<T>, query: CriteriaQuery<*>?, criteriaBuilder: CriteriaBuilder): Predicate {
            val predicates: MutableList<Predicate> = mutableListOf()

            if (information.hasCompositeId()) {
                information.idAttributeNames.forEach(Consumer { name: String? ->
                    predicates.add(
                        criteriaBuilder.equal(
                            root.get<Any>(name), information.getCompositeIdAttributeValue(
                                id, name
                            )
                        )
                    )
                })
                return criteriaBuilder.and(*predicates.toTypedArray())
            }
            return criteriaBuilder.equal(root.get<Any>(information.idAttribute.name), id)
        }
    }

    private inner class DeletedSpecification<T>(val isDeleted: Boolean = false) : Specification<T> {

        override fun toPredicate(root: Root<T>, query: CriteriaQuery<*>?, criteriaBuilder: CriteriaBuilder): Predicate {
            return if (isDeleted) {
                criteriaBuilder.isNotNull(getPath(root))
            } else {
                criteriaBuilder.isNull(getPath(root))
            }
        }


    }
}
