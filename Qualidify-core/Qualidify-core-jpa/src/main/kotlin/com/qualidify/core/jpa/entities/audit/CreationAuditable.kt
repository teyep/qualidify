package com.qualidify.core.jpa.entities.audit

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable

interface CreationAuditable {
    val createdOn: Date?

    val createdBy: String?
}

@Embeddable
data class CreationAuditor(

    @Column(name = "created_on", nullable = false, updatable = false)
    @CreatedDate
    override var createdOn: Date? = null,

    @Column(name = "created_by")
    @CreatedBy
    override var createdBy: String? = null
) : CreationAuditable