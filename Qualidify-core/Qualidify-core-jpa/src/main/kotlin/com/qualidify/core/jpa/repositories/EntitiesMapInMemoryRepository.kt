package com.qualidify.core.jpa.repositories

import com.google.common.base.Preconditions
import com.qualidify.core.jpa.repositories.JpaUtils.getId
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.repository.query.FluentQuery
import java.io.Serializable
import java.util.*
import java.util.function.Function
import java.util.stream.Collectors
import java.util.stream.StreamSupport

/**
 * A special case of the [InMemoryRepository] interface, where the entities are stored in memory
 * @param T the type of the entity
 * @param ID the type of the identity, must implement [Serializable]
 *
 * @author Marcel Pot
 */
@Suppress("OverrideDeprecatedMigration")
class EntitiesMapInMemoryRepository<T : Any, ID : Serializable> : InMemoryRepository<T, ID> {

    private val memory: MutableMap<ID, T?> = mutableMapOf()

    override fun findAll(): List<T?> {
        return ArrayList(memory.values)
    }

    private val notYetImplemented: String by lazy {
        "Not yet implemented"
    }

    override fun findAll(sort: Sort): List<T?> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAll(pageable: Pageable): Page<T?> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAllById(ids: Iterable<ID>): List<T?> {
        Preconditions.checkNotNull(ids, "Id's cannot be null when trying to find items by id's.")
        return StreamSupport.stream(ids.spliterator(), false)
            .filter(memory::containsKey)
            .map { key -> memory[key] }
            .collect(Collectors.toList())
    }

    override fun count(): Long {
        return memory.size.toLong()
    }

    override fun deleteById(id: ID) {
        memory.remove(id) ?: throw EmptyResultDataAccessException("Entity with id $id not found.", 1)
    }

    override fun delete(entity: T) {
        val id: ID = getId(entity)
        memory.remove(id)
    }

    override fun deleteAll(iterable: Iterable<T>) {
        StreamSupport.stream(iterable.spliterator(), false)
            .forEach { entity -> this.delete(entity) }
    }

    override fun deleteAll() {
        memory.clear()
    }

    override fun exists(spec: Specification<T>): Boolean {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T> save(entity: S): S {
        Preconditions.checkNotNull(entity, "Entity cannot be null when trying to save it.")
        val id: ID = getId(entity)
        //Id generation?, at this moment not needed!
        memory[id] = entity
        return entity
    }

    override fun <S : T> saveAll(entities: Iterable<S>): List<S> {
        return StreamSupport.stream(entities.spliterator(), false)
            .map { entity: S -> save(entity) }
            .collect(Collectors.toList())
    }

    override fun findById(id: ID): Optional<T> {
        return Optional.ofNullable(memory[id])
    }

    override fun existsById(id: ID): Boolean {
        return memory.containsKey(id)
    }

    override fun flush() = Unit

    override fun <S : T> saveAndFlush(entity: S): S {
        val savedEntity = save(entity)
        flush()
        return savedEntity
    }

    @Deprecated("Deprecated in Java")
    override fun deleteInBatch(entities: Iterable<T>) {
        deleteAll(entities)
    }

    override fun deleteAllInBatch() {
        deleteAll()
    }

    override fun getReferenceById(id: ID): T {
        throw UnsupportedOperationException(notYetImplemented)
    }

    @Deprecated("Deprecated in Java")
    override fun getOne(id: ID): T? {
        return memory[id]
    }

    override fun <S : T?> findOne(example: Example<S>): Optional<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> findAll(example: Example<S>): List<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> findAll(example: Example<S>, sort: Sort): List<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> findAll(example: Example<S>, pageable: Pageable): Page<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> count(example: Example<S>): Long {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> exists(example: Example<S>): Boolean {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAll(spec: Specification<T>?): MutableList<T> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAll(spec: Specification<T>?, pageable: Pageable): Page<T> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAll(spec: Specification<T>?, sort: Sort): MutableList<T> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun count(spec: Specification<T>?): Long {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findOne(spec: Specification<T>?): Optional<T> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun deleteAllInBatch(entities: MutableIterable<T>) {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun deleteAllById(ids: MutableIterable<ID>) {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T> saveAllAndFlush(entities: MutableIterable<S>): MutableList<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun deleteAllByIdInBatch(ids: MutableIterable<ID>) {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun getById(id: ID): T {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T, R : Any?> findBy(
        example: Example<S>,
        queryFunction: Function<FluentQuery.FetchableFluentQuery<S>, R>,
    ): R {
        throw UnsupportedOperationException(notYetImplemented)
    }


}