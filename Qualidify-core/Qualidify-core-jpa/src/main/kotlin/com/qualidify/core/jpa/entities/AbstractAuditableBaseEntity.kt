package com.qualidify.core.jpa.entities

import com.qualidify.core.jpa.entities.audit.*
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.Embedded
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass

/**
 * An abstract base class for database entities where auditable data is automatically
 * collected by the [AuditingEntityListener] and persisted in the object
 *
 * @author Marcel Pot
 * @since 1.0
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractAuditableBaseEntity<T : EntityId<*>>(
    id: T,
    @Embedded private val auditor: Auditor = Auditor(),
) : AbstractBaseEntity<T>(id),
    CreationAuditable by auditor,
    UpdateAuditable by auditor,
    DeleteAuditable by auditor {

    override fun equals(other: Any?): Boolean {
        return (other != null) && (other::class == this::class) && super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}