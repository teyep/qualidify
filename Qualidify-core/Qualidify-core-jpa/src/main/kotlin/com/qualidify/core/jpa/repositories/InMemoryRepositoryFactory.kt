package com.qualidify.core.jpa.repositories

import org.springframework.aop.framework.ProxyFactory
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Component
import java.io.Serializable

/**
 * A Proxy factory for adding [InMemoryRepository]s to JPA Spring [CrudRepository]s
 *
 * @author Marcel Pot
 */
@Component
class InMemoryRepositoryFactory {

    /**
     * Merge a [SingleEntityInMemoryRepository] into a class with the requested interface
     * @param repositoryClass the requested interface class
     * @param R the type of the repository class
     * @return An implementation of the requested repostitory interface backed with
     *      a [SingleEntityInMemoryRepository]
     */
    @Suppress("UNCHECKED_CAST")
    fun <R : CrudRepository<*, *>> getSingleEntityRepository(repositoryClass: Class<R>): R {
        val pf = ProxyFactory()
        pf.setInterfaces(repositoryClass)
        pf.setTarget(SingleEntityInMemoryRepository<Any, Serializable>())
        return pf.proxy as R
    }

    /**
     * Merge a [EntitiesMapInMemoryRepository] into a class with the requested interface
     * @param repositoryClass the requested interface class
     * @param R the type of the repository class
     * @return An implementation of the requested repostitory interface backed with
     *      a [EntitiesMapInMemoryRepository]
     */
    @Suppress("UNCHECKED_CAST")
    fun <R : CrudRepository<*, *>> getMultipleEntitiesRepository(repositoryClass: Class<R>): R {
        val pf = ProxyFactory()
        pf.setInterfaces(repositoryClass)
        pf.setTarget(EntitiesMapInMemoryRepository<Any, Serializable>())
        return pf.proxy as R
    }
}

