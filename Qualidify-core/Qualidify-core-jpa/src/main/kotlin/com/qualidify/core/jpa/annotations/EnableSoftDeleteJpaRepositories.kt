package com.qualidify.core.jpa.annotations

import com.qualidify.core.jpa.repositories.SoftDeleteJpaRepositoryFactoryBean
import org.springframework.context.annotation.ComponentScan.Filter
import org.springframework.core.annotation.AliasFor
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.repository.config.BootstrapMode
import org.springframework.data.repository.config.DefaultRepositoryBaseClass
import org.springframework.data.repository.query.QueryLookupStrategy.Key
import java.lang.annotation.Inherited
import kotlin.reflect.KClass


@Retention()
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Inherited
@EnableJpaRepositories
annotation class EnableSoftDeleteJpaRepositories(
    /**
     * Alias for the {@link #basePackages()} attribute. Allows for more concise annotation declarations e.g.:
     * {@code @EnableSoftDeleteJpaRepositories("org.my.pkg")} instead of {@code @EnableSoftDeleteJpaRepositories(basePackages="org.my.pkg")}.
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val value: Array<String> = emptyArray(),

    /**
     * Base packages to scan for annotated components. {@link #value()} is an alias for (and mutually exclusive with) this
     * attribute. Use {@link #basePackageClasses()} for a type-safe alternative to String-based package names.
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val basePackages: Array<String> = emptyArray(),

    /**
     * Type-safe alternative to {@link #basePackages()} for specifying the packages to scan for annotated components. The
     * package of each class specified will be scanned. Consider creating a special no-op marker class or interface in
     * each package that serves no purpose other than being referenced by this attribute.
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val basePackageClasses: Array<KClass<*>> = emptyArray(),

    /**
     * Specifies which types are eligible for component scanning. Further narrows the set of candidate components from
     * everything in {@link #basePackages()} to everything in the base packages that matches the given filter or filters.
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val includeFilters: Array<Filter> = emptyArray(),

    /**
     * Specifies which types are not eligible for component scanning.
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val excludeFilters: Array<Filter> = emptyArray(),

    /**
     * Returns the postfix to be used when looking up custom repository implementations. Defaults to {@literal Impl}. So
     * for a repository named {@code PersonRepository} the corresponding implementation class will be looked up scanning
     * for {@code PersonRepositoryImpl}.
     *
     * @return
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val repositoryImplementationPostfix: String = "Impl",

    /**
     * Configures the location of where to find the Spring Data named queries properties file. Will default to
     * {@code META-INF/jpa-named-queries.properties}.
     *
     * @return
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val namedQueriesLocation: String = "",

    /**
     * Returns the key of the {@link QueryLookupStrategy} to be used for lookup queries for query methods. Defaults to
     * {@link QueryLookupStrategy.Key#CREATE_IF_NOT_FOUND}.
     *
     * @return
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val queryLookupStrategy: Key = Key.CREATE_IF_NOT_FOUND,

    /**
     * Returns the {@link FactoryBean} class to be used for each repository instance. Defaults to
     * {@link JpaRepositoryFactoryBean}.
     *
     * @return
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val repositoryFactoryBeanClass: KClass<*> = SoftDeleteJpaRepositoryFactoryBean::class,

    /**
     * Configure the repository base class to be used to create repository proxies for this particular configuration.
     *
     * @return
     * @since 1.9
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val repositoryBaseClass: KClass<*> = DefaultRepositoryBaseClass::class,

    // JPA specific configuration

    /**
     * Configures the name of the {@link EntityManagerFactory} bean definition to be used to create repositories
     * discovered through this annotation. Defaults to {@code entityManagerFactory}.
     *
     * @return
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val entityManagerFactoryRef: String = "entityManagerFactory",

    /**
     * Configures the name of the {@link PlatformTransactionManager} bean definition to be used to create repositories
     * discovered through this annotation. Defaults to {@code transactionManager}.
     *
     * @return
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val transactionManagerRef: String = "transactionManager",

    /**
     * Configures whether nested repository-interfaces (e.g. defined as inner classes) should be discovered by the
     * repositories infrastructure.
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val considerNestedRepositories: Boolean = false,

    /**
     * Configures whether to enable default transactions for Spring Data JPA repositories. Defaults to {@literal true}. If
     * disabled, repositories must be used behind a facade that's configuring transactions (e.g. using Spring's annotation
     * driven transaction facilities) or repository methods have to be used to demarcate transactions.
     *
     * @return whether to enable default transactions, defaults to {@literal true}.
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val enableDefaultTransactions: Boolean = true,

    /**
     * Configures when the repositories are initialized in the bootstrap lifecycle. [BootstrapMode.DEFAULT]
     * (default) means eager initialization except all repository interfaces annotated with [Lazy],
     * [BootstrapMode.LAZY] means lazy by default including injection of lazy-initialization proxies into client
     * beans so that those can be instantiated but will only trigger the initialization upon first repository usage (i.e a
     * method invocation on it). This means repositories can still be uninitialized when the application context has
     * completed its bootstrap. [BootstrapMode.DEFERRED] is fundamentally the same as [BootstrapMode.LAZY],
     * but triggers repository initialization when the application context finishes its bootstrap.
     *
     * @return
     * @since 2.1
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val bootstrapMode: BootstrapMode = BootstrapMode.DEFAULT,

    /**
     * Configures what character is used to escape the wildcards _ and % in derived queries with
     * contains, startsWith or endsWith clauses.
     *
     * @return a single character used for escaping.
     */
    @get:AliasFor(annotation = EnableJpaRepositories::class)
    val escapeCharacter: Char = '\\'
)