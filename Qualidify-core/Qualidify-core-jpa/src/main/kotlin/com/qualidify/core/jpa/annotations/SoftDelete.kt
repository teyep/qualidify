package com.qualidify.core.jpa.annotations

import java.lang.annotation.Inherited

@Retention()
@Target(
    AnnotationTarget.CLASS,
    AnnotationTarget.ANNOTATION_CLASS
)
@Inherited
annotation class SoftDelete(
    val column: String = "removalDate"
)
