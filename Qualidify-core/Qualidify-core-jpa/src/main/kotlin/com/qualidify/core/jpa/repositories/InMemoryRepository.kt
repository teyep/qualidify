package com.qualidify.core.jpa.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.NoRepositoryBean
import java.io.Serializable

/**
 * An interface combining two Jpa interfaces: [JpaRepository] and [JpaSpecificationExecutor]
 * @param T the entity
 * @param ID the entity ID
 *
 * @author Marcel Pot
 */
@NoRepositoryBean
interface InMemoryRepository<T, ID : Serializable> : JpaRepository<T, ID>, JpaSpecificationExecutor<T>
