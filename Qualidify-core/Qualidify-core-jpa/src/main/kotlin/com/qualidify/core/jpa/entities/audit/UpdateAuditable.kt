package com.qualidify.core.jpa.entities.audit

import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable


interface UpdateAuditable {
    val modifiedDate: Date?
    val modifiedBy: String?
}

@Embeddable
data class UpdateAuditor(
    @Column(name = "modified_on", nullable = false, updatable = false)
    @LastModifiedDate
    override var modifiedDate: Date? = null,

    @Column(name = "modified_by")
    @LastModifiedBy
    override var modifiedBy: String? = null
) : UpdateAuditable

