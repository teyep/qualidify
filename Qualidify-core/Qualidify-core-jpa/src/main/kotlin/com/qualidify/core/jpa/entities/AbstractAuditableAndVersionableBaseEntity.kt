package com.qualidify.core.jpa.entities

import com.qualidify.core.jpa.entities.audit.*
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import javax.persistence.Embedded
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass
import javax.persistence.Version

/**
 * An abstract base class for database entities where auditable data is automatically
 * collected by the [AuditingEntityListener] and persisted in the object
 * Also versioning is used (takes also care of Concurrency issues)
 *
 * @author Marcel Pot
 * @since 1.0
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractAuditableAndVersionableBaseEntity<T : EntityId<*>>(
    id: T,
    @Embedded private val auditor: Auditor = Auditor(),
) : AbstractBaseEntity<T>(id),
    CreationAuditable by auditor,
    UpdateAuditable by auditor,
    DeleteAuditable by auditor {
    @Version
    var version: Int? = 0

    override fun equals(other: Any?): Boolean {
        return (other != null) && (other::class == this::class) && super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}