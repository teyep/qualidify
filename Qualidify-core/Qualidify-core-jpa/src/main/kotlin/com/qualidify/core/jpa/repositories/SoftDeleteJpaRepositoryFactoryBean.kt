package com.qualidify.core.jpa.repositories

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean
import org.springframework.data.repository.Repository
import org.springframework.data.repository.core.support.RepositoryFactorySupport
import org.springframework.data.repository.core.support.RepositoryProxyPostProcessor
import javax.persistence.EntityManager

/**
 * Factory Bean to create a [SoftDeleteJpaRepositoryFactory]
 *  @param R The Type of the JpaRepository
 *  @param T the Type of the Entity
 *  @param ID The Type of the Identity
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-457
 *
 */
class SoftDeleteJpaRepositoryFactoryBean<R : Repository<T, ID>, T, ID>(
    repositoryInterface: Class<out R>

) : JpaRepositoryFactoryBean<R, T, ID>(repositoryInterface) {

    @Autowired(required = false)
    val postProcessors: List<RepositoryProxyPostProcessor> = emptyList()

    override fun createRepositoryFactory(entityManager: EntityManager): RepositoryFactorySupport {
        val factory = SoftDeleteJpaRepositoryFactory(entityManager)
        postProcessors.stream()
            .forEach { factory.addRepositoryProxyPostProcessor(it) }
        return factory
    }
}