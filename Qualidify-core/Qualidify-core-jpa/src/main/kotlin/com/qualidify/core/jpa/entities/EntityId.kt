package com.qualidify.core.jpa.entities

import java.io.Serializable

/**
 * Interface for primary keys of entities.
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param T the underlying type of the entity id
 */
interface EntityId<T> : Serializable