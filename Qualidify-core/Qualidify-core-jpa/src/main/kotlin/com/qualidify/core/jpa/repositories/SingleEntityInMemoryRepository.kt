package com.qualidify.core.jpa.repositories

import com.google.common.base.Preconditions
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.domain.Example
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.FluentQuery
import java.io.Serializable
import java.util.*
import java.util.function.Function
import java.util.stream.Collectors
import java.util.stream.StreamSupport

/**
 * A special case of the [JpaRepository] interface, where the only one entity is stored in memory
 * @param T the type of the entity
 * @param ID the type of the identity, must implement [Serializable]
 *
 * @author Marcel Pot
 */
open class SingleEntityInMemoryRepository<T : Any, ID : Serializable> :
    InMemoryRepository<T, ID> {

    private var entityPair: Pair<T, ID>? = null

    override fun findAll(): List<T?> {
        return if (entityPair != null) listOf(entityPair?.first) else emptyList()
    }

    private val notYetImplemented: String by lazy {
        "Not yet implemented"
    }

    override fun findAll(sort: Sort): List<T?> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAll(pageable: Pageable): Page<T?> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAllById(ids: Iterable<ID>): List<T?> {
        Preconditions.checkNotNull(ids, "Id's cannot be null when trying to find items by id's.")
        return StreamSupport.stream(ids.spliterator(), false)
            .filter { it.equals(entityPair!!.second) }
            .map { entityPair!!.first }
            .collect(Collectors.toList())
    }

    override fun count(): Long {
        return if (entityPair == null) 0L else 1L
    }

    override fun deleteById(id: ID) {
        if (id == entityPair?.second) {
            entityPair = null
        } else {
            throw EmptyResultDataAccessException("Entity with id $id not found.", 1)
        }
    }

    override fun delete(entity: T) {
        val id: ID = JpaUtils.getId(entity)
        deleteById(id)
    }

    override fun deleteAll(iterable: Iterable<T>) {
        StreamSupport.stream(iterable.spliterator(), false)
            .forEach { entity -> this.delete(entity) }
    }

    override fun deleteAll() {
        entityPair = null
    }

    override fun exists(spec: Specification<T>): Boolean {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T> save(entity: S): S {
        Preconditions.checkNotNull(entity, "Entity cannot be null when trying to save it.")
        val id: ID = JpaUtils.getId(entity)
        if (entityPair == null || entityPair!!.second == id) {
            entityPair = Pair(entity, id)
        } else {
            throw JpaProblemException("Overloading Single Entity Repository with $entity where ${entityPair?.first} already exists.")
        }
        return entity
    }

    override fun <S : T> saveAll(entities: Iterable<S>): List<S> {
        return StreamSupport.stream(entities.spliterator(), false)
            .map { entity: S -> save(entity) }
            .collect(Collectors.toList())
    }

    override fun findById(id: ID): Optional<T> {
        return if (existsById(id)) Optional.ofNullable(entityPair?.first) else Optional.empty()
    }

    override fun existsById(id: ID): Boolean {
        return id == entityPair?.second
    }

    override fun flush() = Unit

    override fun <S : T> saveAndFlush(entity: S): S {
        val savedEntity = save(entity)
        flush()
        return savedEntity
    }

    @Deprecated("Deprecated in Java")
    override fun deleteInBatch(entities: Iterable<T>) {
        deleteAll(entities)
    }

    override fun deleteAllInBatch() {
        deleteAll()
    }

    override fun getReferenceById(id: ID): T {
        throw UnsupportedOperationException(notYetImplemented)
    }

    @Deprecated("Deprecated in Java")
    override fun getOne(id: ID): T? {
        return if (existsById(id)) entityPair?.first else null
    }

    override fun <S : T?> findOne(example: Example<S>): Optional<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> findAll(example: Example<S>): List<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> findAll(example: Example<S>, sort: Sort): List<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> findAll(example: Example<S>, pageable: Pageable): Page<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> count(example: Example<S>): Long {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T?> exists(example: Example<S>): Boolean {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAll(spec: Specification<T>?): MutableList<T> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAll(spec: Specification<T>?, pageable: Pageable): Page<T> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findAll(spec: Specification<T>?, sort: Sort): MutableList<T> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun count(spec: Specification<T>?): Long {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun findOne(spec: Specification<T>?): Optional<T> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun deleteAllInBatch(entities: MutableIterable<T>) {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun deleteAllById(ids: MutableIterable<ID>) {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T> saveAllAndFlush(entities: MutableIterable<S>): MutableList<S> {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun deleteAllByIdInBatch(ids: MutableIterable<ID>) {
        throw UnsupportedOperationException(notYetImplemented)
    }

    @Deprecated("Deprecated in Java")
    override fun getById(id: ID): T {
        throw UnsupportedOperationException(notYetImplemented)
    }

    override fun <S : T, R : Any?> findBy(
        example: Example<S>,
        queryFunction: Function<FluentQuery.FetchableFluentQuery<S>, R>,
    ): R {
        throw UnsupportedOperationException(notYetImplemented)
    }

}