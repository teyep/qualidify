package com.qualidify.core.jpa.entities

import com.google.common.base.MoreObjects
import java.io.Serializable
import java.util.*
import javax.persistence.EmbeddedId
import javax.persistence.MappedSuperclass

/**
 * An abstract base class for database entities where with an [EntityId]
 *
 * @author Marcel Pot
 * @since 1.0
 */
@MappedSuperclass
abstract class AbstractBaseEntity<T : EntityId<*>>(
    @EmbeddedId
    val id: T
) : BaseEntity<T>, Serializable {

    //internal constructor() : this(null)

    override fun equals(other: Any?): Boolean {
        var result = false
        if (this === other) {
            result = true
        } else if (other is AbstractBaseEntity<*>) {
            result = id == other.id
        }
        return result
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }

    override fun toString(): String {
        return MoreObjects.toStringHelper(this)
            .add("id", id)
            .toString()
    }
}