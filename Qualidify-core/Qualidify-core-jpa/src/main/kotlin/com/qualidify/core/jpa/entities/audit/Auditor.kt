package com.qualidify.core.jpa.entities.audit

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class Auditor(
    @Column(name = "created_on", nullable = true, updatable = false)
    @CreatedDate
    override var createdOn: Date? = null,

    @Column(name = "created_by")
    @CreatedBy
    override var createdBy: String? = null,

    @Column(
        name = "modified_on",
        nullable = true,
        updatable = false
    ) @LastModifiedDate
    override var modifiedDate: Date? = null,

    @Column(name = "modified_by")
    @LastModifiedBy
    override var modifiedBy: String? = null,

    @Column(name = "removed_on", nullable = true, updatable = false)
    override val removalDate: Date? = null

) : CreationAuditable, UpdateAuditable, DeleteAuditable
