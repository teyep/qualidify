package com.qualidify.core.jpa.entities.audit

import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable

interface DeleteAuditable {
    val removalDate: Date?
}

@Embeddable
data class DeleteAuditor(

    @Column(name = "removed_on", nullable = true, updatable = false)
    override val removalDate: Date? = null

) : DeleteAuditable