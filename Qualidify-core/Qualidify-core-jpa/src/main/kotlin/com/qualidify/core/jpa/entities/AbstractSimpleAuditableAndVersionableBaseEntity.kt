package com.qualidify.core.jpa.entities

import com.qualidify.core.jpa.entities.audit.*
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import javax.persistence.*

/**
 * An abstract base class for database entities with auditing properties and versioning
 *
 * @author Marcel Pot
 * @since 1.0
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class AbstractSimpleAuditableAndVersionableBaseEntity(
    @Embedded internal val auditor: Auditor = Auditor(),

    ) : CreationAuditable by auditor,
    UpdateAuditable by auditor,
    DeleteAuditable by auditor,
    Serializable {

    @Version
    var version: Int? = 0
}

