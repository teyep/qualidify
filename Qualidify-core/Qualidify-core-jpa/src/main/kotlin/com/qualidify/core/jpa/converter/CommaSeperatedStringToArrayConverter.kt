package com.qualidify.core.jpa.converter

import javax.persistence.AttributeConverter
import javax.persistence.Converter

/**
 * Converter for adding a list of values as a comma seperated list into the database
 *
 * @author Marcel Pot
 * @since 1.0 QUAL-386
 */
@Converter(autoApply = false)
class CommaSeperatedStringToArrayConverter : AttributeConverter<Array<String>, String> {

    private val splitSign = ","

    override fun convertToDatabaseColumn(attribute: Array<String>?): String? {
        return attribute?.joinToString(splitSign)
    }

    override fun convertToEntityAttribute(dbData: String?): Array<String> {
        return dbData?.split(splitSign)
            ?.filter { !it.isEmpty() }
            ?.toTypedArray() ?: emptyArray()
    }
}