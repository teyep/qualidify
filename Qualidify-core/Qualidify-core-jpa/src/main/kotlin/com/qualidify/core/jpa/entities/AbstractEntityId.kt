package com.qualidify.core.jpa.entities

import com.google.common.base.MoreObjects
import java.io.Serializable
import java.util.*
import javax.persistence.MappedSuperclass

/**
 * An abstract base class for EntityId's
 *
 * @author Marcel Pot
 * @since 1.0
 */
@MappedSuperclass
abstract class AbstractEntityId<T : Serializable> : Serializable, EntityId<T> {
    val id: T?

    internal constructor() {
        id = null
    }

    protected constructor(id: T) {
        this.id = id
    }

    protected constructor(entityId: AbstractEntityId<T>) : this(entityId.id!!)

    /**
     * Get the raw identity
     * @return the raw identity
     */
    fun unbox() = id!!

    override fun equals(other: Any?): Boolean {
        var result = false
        if (this === other) {
            result = true
        } else if (other is AbstractEntityId<*>) {
            result = id == other.id
        }
        return result
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }

    override fun toString(): String {
        return MoreObjects.toStringHelper(this)
            .add("id", id)
            .toString()
    }
}