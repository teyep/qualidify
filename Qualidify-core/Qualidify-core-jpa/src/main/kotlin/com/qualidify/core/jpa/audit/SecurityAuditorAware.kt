package com.qualidify.core.jpa.audit

import com.qualidify.core.log.LoggerDelegation
import org.springframework.data.domain.AuditorAware
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

/**
 * An internal class for detecting the author of a change.
 * It will look into the authentication functionality to find the author. If not found,
 * it falls back to SecurityAuditorAware
 *
 * @author Marcel Pot
 */
class SecurityAuditorAware(private val defaultAuditor: String = "SecurityAuditorAware") : AuditorAware<String> {

    private val logger by LoggerDelegation()

    override fun getCurrentAuditor(): Optional<String> {
        val authentication = SecurityContextHolder.getContext().authentication
        if (authentication != null && authentication.isAuthenticated) {
            if (authentication.principal is UserDetails) {
                val userName = (authentication.principal as UserDetails).username
                return Optional.of(userName)
            }
            logger.error("Security: user is authenticated, but without Userdetails!")
        }
        return Optional.of(defaultAuditor)
    }
}