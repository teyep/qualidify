package com.qualidify.core.jpa.converter

import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply = false)
class CommaSeperatedKeyValuePairToMapConverter : AttributeConverter<Map<String, String>, String> {

    private val isSign = "="
    private val splitSign = ","


    override fun convertToDatabaseColumn(attribute: Map<String, String>?): String? {
        return attribute?.entries?.asSequence()
            ?.map { it.key + isSign + it.value }
            ?.joinToString(splitSign)
    }

    override fun convertToEntityAttribute(dbData: String?): Map<String, String> {
        return dbData?.split(splitSign)
            ?.asSequence()
            ?.filter { !it.isEmpty() }
            ?.map { splitKeyValue(it) }
            ?.toMap() ?: emptyMap()
    }

    private fun splitKeyValue(keyValuePair: String): Pair<String, String> {
        val key = keyValuePair.substringBefore(isSign)
        val value = keyValuePair.substringAfter(isSign)
        return Pair(key, value)
    }
}