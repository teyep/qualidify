package com.qualidify.core.jpa.repositories

import java.lang.reflect.Field
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import javax.persistence.EmbeddedId
import javax.persistence.Id


/**
 * A helper class for finding JPA behaviour
 *
 * @author Marcel Pot
 */
object JpaUtils {

    /**
     * Get the annotated Identity of a JPA entity by scanning the class for [Id] or [EmbeddedId] annotations
     * @param entity the entity to scan
     * @param T The type of the entity
     * @param ID The type of the identity
     * @return the identity
     */
    @JvmStatic
    @Suppress("UNCHECKED_CAST")
    fun <T, ID> getId(entity: T): ID {
        val id = try {
            if (getIdField(entity) != null) {
                getIdField(entity)!![entity] as ID
            } else if (getIdMethod(entity) != null) {
                getIdMethod(entity)!!.invoke(entity) as ID
            } else {
                throw IllegalArgumentException("Entity has no @Id annotation: $entity")
            }
        } catch (e: IllegalAccessException) {
            throw JpaProblemException(e)
        } catch (e: InvocationTargetException) {
            throw JpaProblemException(e)
        }
        return id
    }

    /**
     * Set the Identity of a JPA entity by scanning the class for [Id] or [EmbeddedId] annotations
     * @param entity the entity to scan
     * @param T The type of the entity
     * @param ID The type of the identity
     */
    @JvmStatic
    fun <T, ID> setId(entity: T, id: ID) {
        try {
            if (getIdField(entity) != null) {
                getIdField(entity)!![entity] = id
            } else if (getIdMethod(entity) != null) {
                val setMethod = getIdSetMethod(entity, id)
                setMethod?.invoke(entity, id)
            } else {
                throw IllegalArgumentException("Entity has no @Id annotation: $entity")
            }
        } catch (e: IllegalAccessException) {
            throw JpaProblemException(e)
        } catch (e: InvocationTargetException) {
            throw JpaProblemException(e)
        } catch (e: NoSuchMethodException) {
            throw JpaProblemException(e)
        }
    }

    /**
     * Get the annotated Idenity Field where the annotation is of the type [Id] or [EmbeddedId]
     * @param entity The entity to scan
     * @param T the type fo the entity
     * @return The annotated field
     */
    @JvmStatic
    fun <T> getIdField(entity: T): Field? {
        var clazz: Class<*>? = entity!!::class.java
        while (clazz != null) {
            for (field in clazz.declaredFields) {
                if (field.getAnnotation(Id::class.java) != null || field.getAnnotation(EmbeddedId::class.java) != null) {
                    field.isAccessible = true
                    return field
                }
            }
            clazz = clazz.superclass
        }
        return null
    }

    /**
     * Get the annotated Identity Method where the annotation is of the type [Id] or [EmbeddedId]
     * @param entity The entity to scan
     * @param T the type fo the entity
     * @return The annotated method
     */
    @JvmStatic
    fun <T> getIdMethod(entity: T): Method? {
        var clazz: Class<*>? = entity!!::class.java
        while (clazz != null) {
            for (method in clazz.declaredMethods) {
                if (method.getAnnotation(Id::class.java) != null || method.getAnnotation(EmbeddedId::class.java) != null) {
                    method.isAccessible = true
                    return method
                }
            }
            clazz = clazz.superclass
        }
        return null
    }


    /**
     * Get the Setter for the Id property
     * @param entity the entity to scan
     * @param id the identity to set
     * @param T the type of the entity
     * @param ID the type fo the identity
     * @return the setter for the identity
     */
    @JvmStatic
    fun <T, ID> getIdSetMethod(entity: T, id: ID): Method? {
        val getMethod = getIdMethod(entity)
        if (getMethod != null) {
            val setMethodName = getMethod.name.replaceFirst("^get".toRegex(), "set")
            val setMethod = entity!!::class.java.getMethod(setMethodName, id!!::class.java)
            setMethod.isAccessible = true
            return setMethod
        }
        return null
    }
}