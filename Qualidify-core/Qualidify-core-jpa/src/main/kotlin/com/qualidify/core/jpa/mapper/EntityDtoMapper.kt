package com.qualidify.core.jpa.mapper

import com.qualidify.core.identity.HasIdentity

/**
 * A mapper interface taking care of mapping between pojo's
 * At this moment the following mappings are taken care of:
 * - From Dto to Model (=Database entity)
 * - From Model to Dto
 * @param <E> the Entity Type
 * @param <D> the Dto type
 *
 * @author Marcel Pot
 */
interface EntityDtoMapper<E, D: HasIdentity> {
    /**
     * Convert a pojo from Dto to Model
     * @param dto the Dto to convert
     * @return the Model
     */
    fun convertToModel(dto: D): E

    /**
     * Convert a pojo from Model to Dto
     * @param entity the Model to convert
     * @return the Dto
     */
    fun convertToDto(entity: E): D
}