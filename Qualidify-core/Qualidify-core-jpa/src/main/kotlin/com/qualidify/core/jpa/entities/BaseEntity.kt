package com.qualidify.core.jpa.entities

/**
 * Interface for entity objects.
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param T the type of [EntityId] that will be used in this entity
 */
interface BaseEntity<T : EntityId<*>>