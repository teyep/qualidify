package com.qualidify.core.jpa.converter

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class CommaSeperatedKeyValuePairToMapConverterTest {

    val converter = CommaSeperatedKeyValuePairToMapConverter()

    @Test
    fun convertToDatabaseColumn() {
        val nullResult = converter.convertToDatabaseColumn(null)
        Assertions.assertThat(nullResult).isNull()

        val emptyResult = converter.convertToDatabaseColumn(emptyMap())
        Assertions.assertThat(emptyResult).isEqualTo("")

        val fullResult = converter.convertToDatabaseColumn(mapOf("Tom" to "Jerry", "Sylvester" to "Tweety"))
        Assertions.assertThat(fullResult).isEqualTo("Tom=Jerry,Sylvester=Tweety")

    }

    @Test
    fun convertToEntityAttribute() {
        val nullResult = converter.convertToEntityAttribute(null)
        Assertions.assertThat(nullResult).isEqualTo(emptyMap<String, String>())

        val emptyResult = converter.convertToEntityAttribute("")
        Assertions.assertThat(emptyResult).isEqualTo(emptyMap<String, String>())

        val fullResult = converter.convertToEntityAttribute("twee maal drie=vijf,en twee=negen")
        Assertions.assertThat(fullResult).isEqualTo(mapOf("twee maal drie" to "vijf", "en twee" to "negen"))
    }
}