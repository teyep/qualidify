package com.qualidify.core.jpa.repositories

import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.Al
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.C
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.H
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.He
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.N
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.ALKALI
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.ALKALINE
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.CHALCOGENS
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.HALOGENS
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.domain.Example
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

internal class SingleEntityInMemoryRepositoryTest {


    private lateinit var repository: JpaRepository<ChemicalElement, Int>


    @BeforeEach
    internal fun setup() {
        repository = SingleEntityInMemoryRepository()
        repository.save(H)
    }

    @Test
    internal fun testFindAll() {
        val allEntities = repository.findAll()
        assertThat(allEntities).`as`("Repository contains 1 elements")
            .hasSize(1)
            .`as`("Repository containst the one element of the periodic table")
            .contains(H)
    }

    @Test
    internal fun testFindAllWithSortNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Sort.unsorted())
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllWithPageNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Pageable.unpaged())
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllByIds() {
        val fibonacciEntities = repository.findAllById(setOf(1, 2))
        assertThat(fibonacciEntities).`as`("Result contains 5  elements")
            .hasSize(1)
            .`as`("Result containst only one element of the periodic table")
            .containsExactlyInAnyOrder(H)
            .doesNotContain(Al)

    }

    @Test
    internal fun testCount() {
        val firstCount = repository.count()
        assertThat(firstCount).`as`("Repository contains 1 element").isEqualTo(1L)
        repository.save(H);

        val secondCount = repository.count()
        assertThat(secondCount).`as`("Repository contains still 1 element after duplicate injection")
            .isEqualTo(1L)


    }

    @Test
    internal fun testDeleteById() {
        repository.deleteById(1);
        assertThat(repository.findAll()).`as`("Repository is empty")
            .isEmpty()

    }

    @Test
    internal fun testDeleteById_whileNotExisting() {
        assertThatExceptionOfType(EmptyResultDataAccessException::class.java)
            .isThrownBy { repository.deleteById(13) }
            .withMessage("Entity with id 13 not found.")
    }

    @Test
    internal fun testDelete() {
        repository.delete(H);

        assertThat(repository.findAll()).`as`("Repository contains 0 elements")
            .isEmpty()
    }

    @Test
    internal fun testDeleteWithEmbeddableId() {
        val serieRepository: JpaRepository<ChemicalSerie, ChemicalSerieId> = EntitiesMapInMemoryRepository()
        serieRepository.save(HALOGENS)

        assertThat(serieRepository.count()).`as`("Serie Repository contains initially 1 elements")
            .isEqualTo(1)

        serieRepository.delete(HALOGENS)

        assertThat(serieRepository.findAll()).`as`("Serie Repository contains 0 elements")
            .isEmpty()
    }

    @Test
    internal fun testDeleteAll_withSublist() {
        repository.deleteAll(listOf(H))
        assertThat(repository.findAll()).`as`("Repository contains 0 elements")
            .isEmpty()
    }

    @Test
    internal fun testDeleteWithEmbeddableId_withSublist() {
        val serieRepository: JpaRepository<ChemicalSerie, ChemicalSerieId> = SingleEntityInMemoryRepository()
        serieRepository.save(ALKALI)

        assertThat(serieRepository.count()).`as`("Serie Repository contains initially 1 elements")
            .isEqualTo(1)

        serieRepository.deleteAll(listOf(ALKALI))

        assertThat(serieRepository.findAll()).`as`("Serie Repository contains 0 elements")
            .isEmpty()
    }

    @Test
    internal fun testDeleteAll() {
        assertThat(repository.count()).`as`("Repository contains initially 1 elements").isEqualTo(1)
        repository.deleteAll()
        assertThat(repository.findAll()).`as`("Repository contains 0 elements")
            .isEmpty()
    }

    @Test
    internal fun testSave() {
        assertThatExceptionOfType(JpaProblemException::class.java)
            .isThrownBy {
                repository.save(C);
            }

        var entity = repository.save(H)
        assertThat(entity).`as`("Saved entity is equal to the output")
            .isEqualTo(H)
        assertThat(repository.findAll()).`as`("Repository contains 1 elements")
            .hasSize(1)
            .contains(H)

        repository.delete(H)

        entity = repository.save(C)
        assertThat(entity).`as`("Saved entity is equal to the output")
            .isEqualTo(C)
        assertThat(repository.findAll()).`as`("Repository contains 1 elements")
            .hasSize(1)
            .contains(C)
    }

    @Test
    internal fun tesSaveWithEmbeddableId() {
        val serieRepository: JpaRepository<ChemicalSerie, ChemicalSerieId> = SingleEntityInMemoryRepository()
        serieRepository.save(HALOGENS)

        assertThat(serieRepository.count()).`as`("Serie Repository contains initially 1 element")
            .isEqualTo(1)

        val firstEntity = serieRepository.save(HALOGENS)
        assertThat(firstEntity).`as`("Saved entity is equal to the output")
            .isEqualTo(HALOGENS)

        assertThatExceptionOfType(JpaProblemException::class.java)
            .isThrownBy {
                serieRepository.save(ALKALINE)
            }

        serieRepository.delete(HALOGENS)
        val secondEntity = serieRepository.save(ALKALINE)

        assertThat(secondEntity).`as`("Saved entity is equal to the output")
            .isEqualTo(ALKALINE)

        assertThat(serieRepository.findAll()).`as`("Serie Repository contains 1 element")
            .hasSize(1)
            .contains(ALKALINE)
    }

    @Test
    internal fun testSaveAll() {
        val entityList = repository.saveAll(setOf(H))
        assertThat(entityList).`as`("Saved entities are equal to the output")
            .containsExactlyInAnyOrder(H)
        assertThat(repository.findAll()).`as`("Repository contains 1 elements")
            .hasSize(1)
            .contains(H)

        assertThatExceptionOfType(JpaProblemException::class.java)
            .isThrownBy {
                repository.saveAll(setOf(H, He))
            }
    }

    @Test
    internal fun tesSaveAllWithEmbeddableId() {
        val serieRepository: JpaRepository<ChemicalSerie, ChemicalSerieId> = SingleEntityInMemoryRepository()
        serieRepository.save(CHALCOGENS)

        assertThat(serieRepository.count()).`as`("Serie Repository contains initially 1 element")
            .isEqualTo(1)

        val entityList = serieRepository.saveAll(listOf(CHALCOGENS))
        assertThat(entityList).`as`("Saved entities are equal to the output")
            .containsExactlyInAnyOrder(CHALCOGENS)

        assertThat(serieRepository.findAll()).`as`("Serie Repository contains 1 elements")
            .hasSize(1)
            .contains(CHALCOGENS)
    }

    @Test
    internal fun testFindById() {
        assertThat(repository.findById(1)).`as`("Find Hydrogen by id.").isPresent
            .isEqualTo(Optional.of(H))
        assertThat(repository.findById(9)).`as`("Find not existing Fluorine by id.").isNotPresent
            .isEqualTo(Optional.empty<ChemicalElement>())
    }

    @Test
    internal fun testExistById() {
        assertThat(repository.existsById(1)).`as`("Does Hydrogen exist by id>").isTrue()
        assertThat(repository.existsById(9)).`as`("Does Fluorine exist by id?").isFalse()
    }

    @Test
    internal fun testFlush() {
        assertThat(repository.findAll()).`as`("Repository contains 1 element")
            .hasSize(1)
            .`as`("Repository contains one elements of the periodic table")
            .contains(H)
        repository.flush()
        assertThat(repository.findAll()).`as`("Repository still contains 1elementafter flush")
            .hasSize(1)
            .`as`("Repository contains one element of the periodic table")
            .contains(H)
    }

    @Test
    internal fun testSaveAndFlush() {
        assertThat(repository.count()).isEqualTo(1)

        val entity = repository.saveAndFlush(H)
        assertThat(entity).`as`("Saved entity is equal to the output")
            .isEqualTo(H)
        assertThat(repository.findAll()).`as`("Repository contains 1 elements")
            .hasSize(1)
            .contains(H)

        assertThatExceptionOfType(JpaProblemException::class.java)
            .isThrownBy {
                repository.saveAndFlush(N)
            }
        assertThat(repository.findAll()).`as`("Repository contains 1 elements")
            .hasSize(1)
            .contains(H)
    }


    @Test
    internal fun testDeleteAllInBatch() {
        assertThat(repository.count()).`as`("Repository contains initially 1 elements").isEqualTo(1)
        repository.deleteAllInBatch()
        assertThat(repository.findAll()).`as`("Repository contains after deleteAllInBatch 0 elements")
            .isEmpty()
    }


    @Test
    internal fun testFindOneWithExampleNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findOne(Example.of(H))
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllWithExampleNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Example.of(H))
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllWithExampleAndSortNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Example.of(H), Sort.unsorted())
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllWithExampleAndPageNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Example.of(H), Pageable.unpaged())
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testCountWithExampleNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.count(Example.of(H))
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testExistWithExampleNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.exists(Example.of(H))
            }.withMessage("Not yet implemented")
    }

}


