package com.qualidify.core.jpa.repositories

import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.Al
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.B
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.Be
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.C
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.F
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.H
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.He
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.Li
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.N
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.Ne
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.O
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.ALKALI
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.ALKALINE
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.CHALCOGENS
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.HALOGENS
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.NOBLE
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.PNICTOGENS
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.domain.Example
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

internal class EntitiesMapInMemoryRepositoryTest {


    private lateinit var repository: JpaRepository<ChemicalElement, Int>


    @BeforeEach
    internal fun setup() {
        repository = EntitiesMapInMemoryRepository()
        val firstElementsOfPeriodicTable = listOf(H, He, Li, Be, B, C, N, O)
        repository.saveAll(firstElementsOfPeriodicTable)
    }

    @Test
    internal fun testFindAll() {
        val allEntities = repository.findAll()
        assertThat(allEntities).`as`("Repository contains 8 elements")
            .hasSize(8)
            .`as`("Repository containst the first 8 elements of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, Be, B, C, N, O)
    }

    @Test
    internal fun testFindAllWithSortNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Sort.unsorted())
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllWithPageNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Pageable.unpaged())
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllByIds() {
        val fibonacciEntities = repository.findAllById(setOf(1, 2, 3, 5, 8, 13))
        assertThat(fibonacciEntities).`as`("Result contains 5  elements")
            .hasSize(5)
            .`as`("Result containst the first 5 fibonaccie elements of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, B, O)
            .doesNotContain(Al)

    }

    @Test
    internal fun testCount() {
        val firstCount = repository.count()
        assertThat(firstCount).`as`("Repository contains 8 elements").isEqualTo(8L)
        repository.save(C);

        val secondCount = repository.count()
        assertThat(secondCount).`as`("Repository contains still 8 elements after duplicate injection")
            .isEqualTo(8L)
        repository.save(Al);

        val thirdCount = repository.count()
        assertThat(thirdCount).`as`("Repository contains 9 elements after injection of Aluminium")
            .isEqualTo(9L)
    }

    @Test
    internal fun testDeleteById() {
        repository.deleteById(5);

        assertThat(repository.findAll()).`as`("Repository contains 7 elements")
            .hasSize(7)
            .`as`("Repository containst the first 8 elements without B of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, Be, C, N, O)
    }

    @Test
    internal fun testDeleteById_whileNotExisting() {
        assertThatExceptionOfType(EmptyResultDataAccessException::class.java)
            .isThrownBy { repository.deleteById(13) }
            .withMessage("Entity with id 13 not found.")
    }

    @Test
    internal fun testDelete() {
        repository.delete(Be);

        assertThat(repository.findAll()).`as`("Repository contains 7 elements")
            .hasSize(7)
            .`as`("Repository containst the first 8 elements without Be of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, B, C, N, O)
    }

    @Test
    internal fun testDeleteWithEmbeddableId() {
        val serieRepository: JpaRepository<ChemicalSerie, ChemicalSerieId> = EntitiesMapInMemoryRepository()
        serieRepository.saveAll(listOf(ALKALI, ALKALINE, PNICTOGENS, CHALCOGENS, HALOGENS, NOBLE))

        assertThat(serieRepository.count()).`as`("Serie Repository contains initially 6 elements")
            .isEqualTo(6)

        serieRepository.delete(ALKALINE)

        assertThat(serieRepository.findAll()).`as`("Serie Repository contains 5 elements")
            .hasSize(5)
            .containsExactlyInAnyOrder(ALKALI, PNICTOGENS, CHALCOGENS, HALOGENS, NOBLE)
    }

    @Test
    internal fun testDeleteAll_withSublist() {
        repository.deleteAll(listOf(B, Be))
        assertThat(repository.findAll()).`as`("Repository contains 6 elements")
            .hasSize(6)
            .`as`("Repository containst the first 8 elements without B and Be of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, C, N, O)
    }

    @Test
    internal fun testDeleteWithEmbeddableId_withSublist() {
        val serieRepository: JpaRepository<ChemicalSerie, ChemicalSerieId> = EntitiesMapInMemoryRepository()
        serieRepository.saveAll(listOf(ALKALI, ALKALINE, PNICTOGENS, CHALCOGENS, HALOGENS, NOBLE))

        assertThat(serieRepository.count()).`as`("Serie Repository contains initially 6 elements")
            .isEqualTo(6)

        serieRepository.deleteAll(listOf(ALKALINE, ALKALINE, ALKALI, NOBLE))

        assertThat(serieRepository.findAll()).`as`("Serie Repository contains 5 elements")
            .hasSize(3)
            .containsExactlyInAnyOrder(PNICTOGENS, CHALCOGENS, HALOGENS)
    }

    @Test
    internal fun testDeleteAll() {
        assertThat(repository.count()).`as`("Repository contains initially 8 elements").isEqualTo(8)
        repository.deleteAll()
        assertThat(repository.findAll()).`as`("Repository contains 0 elements")
            .isEmpty()
    }

    @Test
    internal fun testSave() {
        val entity = repository.save(F)
        assertThat(entity).`as`("Saved entity is equal to the output")
            .isEqualTo(F)
        assertThat(repository.findAll()).`as`("Repository contains 9 elements")
            .hasSize(9)
            .`as`("Repository containst the first 9 elements  of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, Be, B, C, N, O, F)
    }

    @Test
    internal fun tesSaveWithEmbeddableId() {
        val serieRepository: JpaRepository<ChemicalSerie, ChemicalSerieId> = EntitiesMapInMemoryRepository()
        serieRepository.saveAll(listOf(ALKALI, ALKALINE, PNICTOGENS, CHALCOGENS, HALOGENS))

        assertThat(serieRepository.count()).`as`("Serie Repository contains initially 5 elements")
            .isEqualTo(5)

        val firstEntity = serieRepository.save(NOBLE)
        val secondEntity = serieRepository.save(ALKALINE)

        assertThat(firstEntity).`as`("Saved entity is equal to the output")
            .isEqualTo(NOBLE)
        assertThat(secondEntity).`as`("Saved entity is equal to the output")
            .isEqualTo(ALKALINE)

        assertThat(serieRepository.findAll()).`as`("Serie Repository contains 6 elements")
            .hasSize(6)
            .containsExactlyInAnyOrder(ALKALI, ALKALINE, PNICTOGENS, CHALCOGENS, HALOGENS, NOBLE)
    }

    @Test
    internal fun testSaveAll() {
        val entityList = repository.saveAll(setOf(He, F, Ne, Al))
        assertThat(entityList).`as`("Saved entities are equal to the output")
            .containsExactlyInAnyOrder(He, F, Ne, Al)
        assertThat(repository.findAll()).`as`("Repository contains 11 elements")
            .hasSize(11)
            .`as`("Repository containst the first 10 elements and Aluminium of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, Be, B, C, N, O, F, Ne, Al)
    }

    @Test
    internal fun tesSaveAllWithEmbeddableId() {
        val serieRepository: JpaRepository<ChemicalSerie, ChemicalSerieId> = EntitiesMapInMemoryRepository()
        serieRepository.saveAll(listOf(ALKALI, ALKALINE, PNICTOGENS, CHALCOGENS, HALOGENS))

        assertThat(serieRepository.count()).`as`("Serie Repository contains initially 5 elements")
            .isEqualTo(5)

        val entityList = serieRepository.saveAll(listOf(ALKALINE, CHALCOGENS, NOBLE, CHALCOGENS))
        assertThat(entityList).`as`("Saved entities are equal to the output")
            .containsExactlyInAnyOrder(ALKALINE, CHALCOGENS, NOBLE, CHALCOGENS)

        assertThat(serieRepository.findAll()).`as`("Serie Repository contains 6 elements")
            .hasSize(6)
            .containsExactlyInAnyOrder(ALKALI, ALKALINE, PNICTOGENS, CHALCOGENS, HALOGENS, NOBLE)
    }

    @Test
    internal fun testFindById() {
        assertThat(repository.findById(8)).`as`("Find Oxygen by id.").isPresent
            .isEqualTo(Optional.of(O))
        assertThat(repository.findById(9)).`as`("Find not existing Fluorine by id.").isNotPresent
            .isEqualTo(Optional.empty<ChemicalElement>())
    }

    @Test
    internal fun testExistById() {
        assertThat(repository.existsById(8)).`as`("Does Oxygen exist by id>").isTrue()
        assertThat(repository.existsById(9)).`as`("Does Fluorine exist by id?").isFalse()
    }

    @Test
    internal fun testFlush() {
        assertThat(repository.findAll()).`as`("Repository contains 8 elements")
            .hasSize(8)
            .`as`("Repository containst the first 8 elements of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, Be, B, C, N, O)
        repository.flush()
        assertThat(repository.findAll()).`as`("Repository still contains 8 elements after flush")
            .hasSize(8)
            .`as`("Repository containst the first 8 elements of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, Be, B, C, N, O)
    }

    @Test
    internal fun testSaveAndFlush() {
        assertThat(repository.count()).isEqualTo(8)

        val entity = repository.saveAndFlush(F)
        assertThat(entity).`as`("Saved entity is equal to the output")
            .isEqualTo(F)
        assertThat(repository.findAll()).`as`("Repository contains 9 elements")
            .hasSize(9)
            .`as`("Repository containst the first 9 elements  of the periodic table")
            .containsExactlyInAnyOrder(H, He, Li, Be, B, C, N, O, F)
    }

    @Test
    internal fun testDeleteAllInBatch() {
        assertThat(repository.count()).`as`("Repository contains initially 8 elements").isEqualTo(8)
        repository.deleteAllInBatch()
        assertThat(repository.findAll()).`as`("Repository contains after deleteAllInBatch 0 elements")
            .isEmpty()
    }

    @Test
    internal fun testFindOneWithExampleNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findOne(Example.of(H))
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllWithExampleNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Example.of(H))
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllWithExampleAndSortNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Example.of(H), Sort.unsorted())
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testFindAllWithExampleAndPageNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.findAll(Example.of(H), Pageable.unpaged())
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testCountWithExampleNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.count(Example.of(H))
            }.withMessage("Not yet implemented")
    }

    @Test
    internal fun testExistWithExampleNotWorking() {
        assertThatExceptionOfType(UnsupportedOperationException::class.java)
            .isThrownBy {
                repository.exists(Example.of(H))
            }.withMessage("Not yet implemented")
    }

}


