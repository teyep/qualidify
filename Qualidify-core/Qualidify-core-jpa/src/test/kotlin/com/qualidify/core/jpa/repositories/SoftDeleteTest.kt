package com.qualidify.core.jpa.repositories

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Transactional
import java.util.*

@ExtendWith(SpringExtension::class)
@SpringBootTest
open class SoftDeleteTest {
    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var carRepository: CarRepository

    @Autowired
    lateinit var alternativeUserRepository: AlternativeUserRepository

    @Autowired
    lateinit var alternativeCarRepository: AlternativeCarRepository

    val user: User
        get() {
            return User(null, "TestUser" + Random().nextInt(1000), null)
        }


    val car: Car
        get() {
            return Car(null, "Saab")
        }

    @Test
    @Transactional
    open fun testSoftDelete() {
        val localUser = user
        userRepository.save(localUser)
        userRepository.delete(localUser)
        assertThat(userRepository.findById(localUser.id!!).isEmpty)
        assertThat(alternativeUserRepository.findById(localUser.id)).contains(localUser)
    }

    @Test
    @Transactional
    open fun testSoftDeleteWithAuditor() {
        val localCar = car
        carRepository.save(localCar)
        carRepository.delete(localCar)
        assertThat(carRepository.findById(localCar.id!!).isEmpty)
        assertThat(alternativeCarRepository.findById(localCar.id)).contains(localCar)
    }

    @Test
    @Transactional
    open fun testSoftDeleteById() {
        val localUser = user
        userRepository.save(localUser)
        userRepository.deleteById(localUser.id!!)
        assertThat(userRepository.findById(localUser.id).isEmpty)
        assertThat(alternativeUserRepository.findById(localUser.id)).contains(localUser)
    }

    @Test
    @Transactional
    open fun testSoftDeleteByIdWithAuditor() {
        val localCar = car
        carRepository.save(localCar)
        carRepository.deleteById(localCar.id!!)
        assertThat(carRepository.findById(localCar.id).isEmpty)
        assertThat(alternativeCarRepository.findById(localCar.id)).contains(localCar)
    }

    @Test
    @Transactional
    open fun testSoftDeleteAllById() {
        val users = getUsers(6)
        users.asSequence().forEach { userRepository.save(it) }

        val userIds = users.asSequence().map { it.id }.toList()
        userRepository.deleteAllById(userIds)

        assertThat(userRepository.findAllById(userIds)).isEmpty()
        assertThat(alternativeUserRepository.findAllById(userIds)).hasSize(6)
    }

    @Test
    @Transactional
    open fun testSoftDeleteAll() {
        val users = getUsers(5)
        users.asSequence().forEach { userRepository.save(it) }

        userRepository.deleteAll(users)

        val userIds = users.asSequence().map { it.id }.toList()
        assertThat(userRepository.findAllById(userIds)).isEmpty()
        assertThat(alternativeUserRepository.findAllById(userIds)).hasSize(5)
    }

    private fun getUsers(numberOfUsers: Int): Collection<User> {
        return (1..numberOfUsers).asSequence()
            .map { user }
            .toList()
    }
}



