package com.qualidify.core.jpa.converter

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CommaSeperatedStringToArrayConverterTest {

    val converter = CommaSeperatedStringToArrayConverter()

    @Test
    fun convertToDatabaseColumn() {
        val nullResult = converter.convertToDatabaseColumn(null)
        assertThat(nullResult).isNull()

        val emptyResult = converter.convertToDatabaseColumn(emptyArray())
        assertThat(emptyResult).isEqualTo("")

        val fullResult = converter.convertToDatabaseColumn(arrayOf("Tom", "Jerry"))
        assertThat(fullResult).isEqualTo("Tom,Jerry")
    }

    @Test
    fun convertToEntityAttribute() {
        val nullResult = converter.convertToEntityAttribute(null)
        assertThat(nullResult).isEqualTo(emptyArray<String>())

        val emptyResult = converter.convertToEntityAttribute("")
        assertThat(emptyResult).isEqualTo(emptyArray<String>())

        val fullResult = converter.convertToEntityAttribute("Simon,Garfunkel")
        assertThat(fullResult).isEqualTo(arrayOf("Simon", "Garfunkel"))
    }
}