package com.qualidify.core.jpa.repositories

import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.H
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.N
import com.qualidify.core.jpa.repositories.ChemicalElement.Companion.U
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.CHALCOGENS
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.HALOGENS
import com.qualidify.core.jpa.repositories.ChemicalSerie.Companion.NOBLE
import com.qualidify.core.jpa.repositories.Quark.Companion.BOTTOM
import com.qualidify.core.jpa.repositories.Quark.Companion.CHARM
import com.qualidify.core.jpa.repositories.Quark.Companion.DOWN
import com.qualidify.core.jpa.repositories.Quark.Companion.TOP
import com.qualidify.core.jpa.repositories.Quark.Companion.UP
import com.qualidify.core.jpa.repositories.SubatomicParticle.Companion.ELECTRON
import com.qualidify.core.jpa.repositories.SubatomicParticle.Companion.NEUTRON
import com.qualidify.core.jpa.repositories.SubatomicParticle.Companion.PROTON
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Test

internal class JpaUtilsTest {

    @Test
    fun testGetId_byField() {
        //@Id test
        val id = JpaUtils.getId<ChemicalElement, Int>(H)
        assertThat(id).`as`("The returnclass of the Identity is Int")
            .isOfAnyClassIn(Integer::class.java)
            .isEqualTo(1)

        //@EmbeddedId test
        val embeddedId = JpaUtils.getId<ChemicalSerie, ChemicalSerieId>(NOBLE)
        assertThat(embeddedId).`as`("The returnclass of the Identity is ChemicalSerieId")
            .isOfAnyClassIn(ChemicalSerieId::class.java)
            .isEqualTo(NOBLE.id)
    }

    @Test
    fun testGetId_byMethod() {
        //@Id test
        val id = JpaUtils.getId<SubatomicParticle, String>(ELECTRON)
        assertThat(id).`as`("The returnclass of the Identity is String")
            .isOfAnyClassIn(String::class.java)
            .isEqualTo(ELECTRON.name)

        //@EmbbededId test
        val embeddedId = JpaUtils.getId<Quark, QuarkId>(CHARM)
        assertThat(embeddedId).`as`("The returnclass of the Identity is QuarkId")
            .isOfAnyClassIn(QuarkId::class.java)
            .isEqualTo(CHARM.id)
    }

    @Test
    fun testGetId_whileNoAnnotation() {
        assertThatExceptionOfType(IllegalArgumentException::class.java)
            .isThrownBy {
                JpaUtils.getId<String, String>("TestString");
            }
            .withMessage("Entity has no @Id annotation: TestString")
    }

    @Test
    fun testSetId_byField() {

        //@Id test
        assertThatNoException()
            .isThrownBy {
                JpaUtils.setId(U, 2)
                assertThat(U.atomicNumber).`as`("U has been downgraded to a band").isEqualTo(2)
                JpaUtils.setId(U, 92)
            }


        //@EmbeddedId test
        assertThatNoException()
            .isThrownBy {
                val rememberId = CHALCOGENS.id
                JpaUtils.setId(CHALCOGENS, HALOGENS.id)
                assertThat(CHALCOGENS.id).`as`("Chalogens has changed to halogen").isEqualTo(HALOGENS.id)
                JpaUtils.setId(CHALCOGENS, rememberId)
            }


    }

    @Test
    fun testSetId_byMethod() {
        //@Id test
        assertThatNoException()
            .isThrownBy {
                val rememberId = ELECTRON.getDifficultName()
                JpaUtils.setId(ELECTRON, "Super Electron")
                assertThat(ELECTRON.name).`as`("Electron got a new name").isEqualTo("Super Electron")
                JpaUtils.setId(ELECTRON, rememberId)
            }


        //@EmbeddedId test
        assertThatNoException()
            .isThrownBy {
                val rememberId = DOWN.getMySpecialId()
                JpaUtils.setId(DOWN, UP.id)
                assertThat(DOWN.id).`as`("Down got some therapy").isEqualTo(UP.id)
                JpaUtils.setId(DOWN, rememberId)
            }


    }

    @Test
    fun testSetId_whileNoAnnotation() {
        assertThatExceptionOfType(IllegalArgumentException::class.java)
            .isThrownBy {
                JpaUtils.setId("TestString", "newName");
            }
            .withMessage("Entity has no @Id annotation: TestString")
    }

    @Test
    fun testSetId_byField_withWrongParameterType() {
        //@Id test
        assertThatExceptionOfType(IllegalArgumentException::class.java)
            .isThrownBy {
                JpaUtils.setId(H, "Deuterium")
            }

        //@EmbeddedId test
        assertThatExceptionOfType(IllegalArgumentException::class.java)
            .isThrownBy {
                JpaUtils.setId(CHALCOGENS, "The Chemicals")
            }
    }

    @Test
    fun testSetId_byMethod_withWrongParameterType() {
        //@Id test
        assertThatExceptionOfType(JpaProblemException::class.java)
            .isThrownBy {
                JpaUtils.setId(ELECTRON, -1)
            }

        //@EmbeddedId test
        assertThatExceptionOfType(JpaProblemException::class.java)
            .isThrownBy {
                JpaUtils.setId(BOTTOM, "GROUND")
            }
    }

    @Test
    fun testGetIdField() {
        assertThat(JpaUtils.getIdField(N)).`as`("Chemical Element has an annotated id field")
            .isNotNull
        assertThat(JpaUtils.getIdField(NOBLE)).`as`("Chemical Serie has an annotated @EmbeddedId field")
            .isNotNull
        assertThat(JpaUtils.getIdField(NEUTRON)).`as`("SubatomicParticle has not an annotated field")
            .isNull()
        assertThat(JpaUtils.getIdField(TOP)).`as`("Quarks do not have an annotated id field")
            .isNull()
    }

    @Test
    fun testGetIdMethod() {
        assertThat(JpaUtils.getIdMethod(N)).`as`("Chemical Element has not an annotated id method")
            .isNull()
        assertThat(JpaUtils.getIdMethod(NOBLE)).`as`("Chemical Serie has not an annotated id method")
            .isNull()
        assertThat(JpaUtils.getIdMethod(NEUTRON)).`as`("SubatomicParticle has an annotated method with @Id")
            .isNotNull
        assertThat(JpaUtils.getIdMethod(TOP)).`as`("Quarks have an annotated id method with @EmbeddableId")
            .isNotNull
    }

    @Test
    fun testGetIdSetMethod() {
        assertThat(JpaUtils.getIdSetMethod(N, N.atomicNumber)).`as`("Chemical Element has not an annotated id method")
            .isNull()
        assertThat(JpaUtils.getIdSetMethod(NOBLE, NOBLE.id)).`as`("Chemical Serie has not an annotated id method")
            .isNull()
        assertThat(
            JpaUtils.getIdSetMethod(
                NEUTRON,
                NEUTRON.name
            )
        ).`as`("SubatomicParticle has an annotated method with @Id")
            .isNotNull
        assertThat(JpaUtils.getIdSetMethod(TOP, TOP.id)).`as`("Quarks have an annotated id method with @EmbeddableId")
            .isNotNull
    }

    @Test
    fun testGetIdSetMethod_withWrongParameter() {
        assertThatExceptionOfType(NoSuchMethodException::class.java)
            .isThrownBy { JpaUtils.getIdSetMethod(NEUTRON, TOP.id) }

        assertThatExceptionOfType(NoSuchMethodException::class.java)
            .isThrownBy { JpaUtils.getIdSetMethod(TOP, PROTON.name) }
    }


}