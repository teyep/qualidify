package com.qualidify.core.jpa.repositories

import com.qualidify.core.jpa.annotations.EnableSoftDeleteJpaRepositories
import com.qualidify.core.jpa.annotations.SoftDelete
import com.qualidify.core.jpa.entities.AbstractSimpleAuditableBaseEntity
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*
import javax.persistence.*

@EnableSoftDeleteJpaRepositories
@SpringBootApplication
open class QualidifyCoreJpaRepositoryApplication

fun main(args: Array<String>) {
    runApplication<QualidifyCoreJpaRepositoryApplication>(*args)
}

@SoftDelete
interface UserRepository : JpaRepository<User, Long>

interface AlternativeUserRepository : JpaRepository<User, Long>

@SoftDelete
interface CarRepository : JpaRepository<Car, Long>

interface AlternativeCarRepository : JpaRepository<Car, Long>


@Entity
@Table(name = "users")
data class User(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long?,
    val name: String,

    @Column(name = "removed_at")
    val removalDate: Date?
) {
    constructor() : this(null, "", null)
}

@Entity
@Table(name = "cars")
data class Car(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long?,
    val brand: String,
) : AbstractSimpleAuditableBaseEntity() {
    constructor() : this(null, "")
}

