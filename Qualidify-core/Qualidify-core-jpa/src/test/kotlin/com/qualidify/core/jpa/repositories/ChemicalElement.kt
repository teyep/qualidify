package com.qualidify.core.jpa.repositories

import java.io.Serializable
import javax.persistence.*

@Entity
class ChemicalElement(
    @Id
    var atomicNumber: Int?,
    var symbol: String?,
    var name: String?
) {
    constructor() : this(null, null, null)

    override fun toString(): String {
        return "$name ($symbol) with atomicnumber  $atomicNumber"
    }

    companion object {
        val H = ChemicalElement(1, "H", "Hydrogen")
        val He = ChemicalElement(2, "He", "Helium")
        val Li = ChemicalElement(3, "Li", "Lithium")
        val Be = ChemicalElement(4, "Be", "Beryllium")
        val B = ChemicalElement(5, "B", "Boron")
        val C = ChemicalElement(6, "C", "Carbon")
        val N = ChemicalElement(7, "N", "Nitrogen")
        val O = ChemicalElement(8, "O", "Oxygen")
        val F = ChemicalElement(9, "F", "Fluorine")
        val Ne = ChemicalElement(10, "Ne", "Neon")

        val Al = ChemicalElement(13, "Al", "Aluminium")
        val U = ChemicalElement(92, "U", "Uranium")
    }
}

@Entity
class ChemicalSerie(
    @EmbeddedId
    var id: ChemicalSerieId?,
    var description: String?
) {
    constructor() : this(null, null)

    companion object {
        val ALKALI = ChemicalSerie(ChemicalSerieId("Alkali methals", 1), "First group of the periodic table.")
        val ALKALINE = ChemicalSerie(ChemicalSerieId("Alkaline earth metals", 2), "Second group of the periodic table.")
        val PNICTOGENS = ChemicalSerie(ChemicalSerieId("Pnictogens", 15), "Fortheenth group of the periodic table.")
        val CHALCOGENS = ChemicalSerie(ChemicalSerieId("Chalcogens", 16), "Sixteenth group of the periodic table.")
        val HALOGENS = ChemicalSerie(ChemicalSerieId("Halogens", 17), "Seventeenth group of the periodic table.")
        val NOBLE = ChemicalSerie(ChemicalSerieId("Noble gas", 18), "Eightteenth group of the periodic table.")
    }
}

@Embeddable
class ChemicalSerieId(
    var serieName: String?,
    var groupNumber: Int?

) : Serializable {
    constructor() : this(null, null)
}

@Entity
class SubatomicParticle(
    var name: String?,
    var charge: Short?
) {
    constructor() : this(null, null)

    @Id
    fun getDifficultName(): String? {
        return this.name;
    }

    fun setDifficultName(name: String) {
        this.name = name
    }

    companion object {
        val PROTON = SubatomicParticle("Proton", 1)
        val NEUTRON = SubatomicParticle("Neutron", 0)
        val ELECTRON = SubatomicParticle("Electron", -1)
    }
}


class Quark(
    var id: QuarkId?,
    var description: String?
) {
    constructor() : this(null, null)

    @EmbeddedId
    fun getMySpecialId(): QuarkId? {
        return this.id;
    }

    fun setMySpecialId(id: QuarkId) {
        this.id = id;
    }

    companion object {
        val UP = Quark(QuarkId("Up", 0.67), "Up")
        val DOWN = Quark(QuarkId("DOWN", -0.33), "Down")
        val CHARM = Quark(QuarkId("CHARM", 0.67), "Charm")
        val STRANGE = Quark(QuarkId("STRANGE", -0.33), "Strange")
        val TOP = Quark(QuarkId("TOP", 0.67), "Top")
        val BOTTOM = Quark(QuarkId("BOTTOM", -0.33), "Bottom")
    }
}

@Embeddable
class QuarkId(
    var name: String,
    @Column(nullable = false)
    var charge: Double
) : Serializable {
    constructor() : this("", 0.0)
}
