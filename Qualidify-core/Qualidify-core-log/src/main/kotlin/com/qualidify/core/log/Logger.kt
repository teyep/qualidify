package com.qualidify.core.log

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty


/**
 * Helper function to retrieve the Logger
 * @param[forClass] The class to retrieve the Logger for
 * @return a Logger
 */
fun getLogger(forClass: Class<*>): Logger = LoggerFactory.getLogger(forClass)

/**
 * Determine the javaClass as source where companion objects are stripped
 * @param[javaClass] the Class to check if it is a companion object
 * @return the javaClass
 */
fun <T : Any> getClassForLogging(javaClass: Class<T>): Class<*> = javaClass.enclosingClass?.takeIf { it.kotlin.isCompanion }
        ?: javaClass

/**
 * Marker interface to indicate the class has a logger
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface HasLogger

/**
 * Function belonging to the marker interface [HasLogger]
 * @return a Logger object for the class
 */
inline fun <reified T : HasLogger> T.logger(): Logger = getLogger(getClassForLogging(T::class.java))


/**
 * A LoggerDelagation to add a logger to a class
 * @param[R] the Class to add the logger to as [ReadOnlyProperty]
 *
 * Usage of this [LoggerDelegation] is to add a property in the intended class with
 * the following syntax:
 * ```
 *      val logger by LoggerDelegation()
 * ```
 */
class LoggerDelegation<in R : Any> : ReadOnlyProperty<R, Logger> {
    override fun getValue(thisRef: R, property: KProperty<*>): Logger =
            getLogger(getClassForLogging(thisRef.javaClass))
}
