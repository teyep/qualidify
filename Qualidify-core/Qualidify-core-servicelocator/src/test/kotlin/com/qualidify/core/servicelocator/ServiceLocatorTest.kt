package com.qualidify.core.servicelocator

import com.qualidify.core.servicelocator.ServiceLocator.Companion.getServiceByName
import com.qualidify.core.servicelocator.ServiceLocator.Companion.getServiceByType
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.junit.jupiter.SpringExtension

/**
 * Test the functionality of the ServiceLocator
 *
 * @author Marcel Pot
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [ServiceLocatorConfiguration::class])
internal class ServiceLocatorTest {

    @Mock
    var applicationContext: ApplicationContext? = null

    @InjectMocks
    var resolver: ApplicationContextServiceResolver? = null

    @Test
    fun testWithName() {
        ServiceLocator.INSTANCE.setResolver(resolver)

        `when`(applicationContext!!.getBean("simpleBean")).thenReturn(SimpleBean())

        val simpleBean = getServiceByName("simpleBean")

        assertThat(simpleBean).isNotNull.isInstanceOf(SimpleBean::class.java)
        verify(applicationContext, Mockito.times(1))!!.getBean("simpleBean")
    }

    @Test
    fun testWithNameAndClassType() {
        ServiceLocator.INSTANCE.setResolver(resolver)

        `when`(applicationContext!!.getBean("simpleBean")).thenReturn(SimpleBean())

        val simpleBean = getServiceByName("simpleBean", SimpleBean::class.java)

        assertThat(simpleBean).isNotNull.isInstanceOf(SimpleBean::class.java)
        verify(applicationContext, Mockito.times(1))!!.getBean("simpleBean")
    }

    @Test
    fun testWithOnlyClassType() {
        ServiceLocator.INSTANCE.setResolver(resolver)

        `when`(applicationContext!!.getBean(SimpleBean::class.java)).thenReturn(SimpleBean())

        val simpleBean = getServiceByType(SimpleBean::class.java)

        assertThat(simpleBean).isNotNull.isInstanceOf(SimpleBean::class.java)
        verify(applicationContext, Mockito.times(1))!!.getBean(SimpleBean::class.java)
    }

    @Test
    fun testBeanNotFound() {
        ServiceLocator.INSTANCE.setResolver(resolver)

        `when`(applicationContext!!.getBean("notExistingBean"))
            .thenThrow(NoSuchBeanDefinitionException("notExistingBean"))

        Assertions.assertThatThrownBy { getServiceByName("notExistingBean") }
            .isInstanceOf(NoSuchBeanDefinitionException::class.java)
            .hasMessage("No bean named 'notExistingBean' available")
        verify(applicationContext, Mockito.times(1))!!.getBean("notExistingBean")
    }

    /**
     * A Test Bean for testing purposes
     */
    class SimpleBean
}