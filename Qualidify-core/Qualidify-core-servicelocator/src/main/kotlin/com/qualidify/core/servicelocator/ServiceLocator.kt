package com.qualidify.core.servicelocator

import org.springframework.beans.factory.InitializingBean
import org.springframework.stereotype.Component

/**
 * A Singleton access point for retrieving ServiceBeans based on a ServiceResolver.
 * (Standard resolver will use the Spring Application Context)
 *
 * @author Marcel Pot
 */
enum class ServiceLocator {
    /**
     * Singleton
     */
    INSTANCE;

    private var resolver: ServiceResolver? = null

    fun setResolver(resolver: ServiceResolver?) {
        this.resolver = resolver
    }

    /**
     * A static injector class to make sure the autowiring will be executed and take care of injecting
     * the autowired beans into the singleton / enumeration
     *
     * @author Marcel Pot
     */
    @Component
    class ServiceResolverInjector(private val resolver: ServiceResolver) : InitializingBean {

        /**
         * Post construction post processing taking care of injecting the ServiceResolver into the parent enum
         */
        override fun afterPropertiesSet() {
            INSTANCE.setResolver(resolver)
        }
    }

    companion object {
        /**
         * Get a ServiceBean by the beanname
         * @param name the beanname
         * @return the serviceBean
         */
        @JvmStatic
        fun getServiceByName(name: String): Any? {
            return INSTANCE.resolver!!.getServiceByName(name)
        }

        /**
         * Get a ServiceBean by the beanname and the expected type
         * @param name the beanname
         * @param type the type
         * @param <T> the type class
         * @return the serviceBean
        </T> */
        @JvmStatic
        fun <T> getServiceByName(name: String, type: Class<T>): T? {
            val service = INSTANCE.resolver!!.getServiceByName(name)
            return if (type.isInstance(service)) {
                type.cast(service)
            } else null
        }

        /**
         * Get a ServiceBean by its type
         * @param type the type
         * @return The serviceBean
         */
        @JvmStatic
        fun <T> getServiceByType(type: Class<T>): T? {
            return INSTANCE.resolver!!.getServiceByType(type)
        }

        /**
         * Get serviceBeans per Type
         * @param type the type
         * @return A map with beanname to serviceBeans
         */
        @JvmStatic
        fun <T> getServicesByType(type: Class<T>): Map<String, T> {
            return INSTANCE.resolver!!.getServicesByType(type)
        }


    }
}