package com.qualidify.core.servicelocator

import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Service

/**
 * The ApplicationContextResolver will look up the beans in the Spring Application Context.
 *
 * @author Marcel Pot
 */
@Service
class ApplicationContextServiceResolver : ServiceResolver, ApplicationContextAware {
    private var ctx: ApplicationContext? = null

    override fun getServiceByName(name: String): Any? {
        return ctx!!.getBean(name)
    }

    override fun <T> getServiceByType(clazz: Class<T>): T? {
        return ctx!!.getBean(clazz)
    }

    override fun <T> getServicesByType(clazz: Class<T>): Map<String, T> {
        return ctx!!.getBeansOfType(clazz)
    }

    override fun setApplicationContext(applicationContext: ApplicationContext) {
        ctx = applicationContext
    }


}