package com.qualidify.core.servicelocator

/**
 * An Service Resolver interface to ask the system to find a Service by Name
 *
 * @author Marcel Pot
 */
interface ServiceResolver {
    /**
     * Get a Service by name
     * @param name the name
     * @return the service
     */
    fun getServiceByName(name: String): Any?

    /**
     * Get a Service by its type
     * @param clazz the type
     * @return The Service
     */
    fun <T> getServiceByType(clazz: Class<T>): T?

    /**
     * Get services by their type
     * @param clazz the type
     * @return a Map with beannames to beans
     */
    fun <T> getServicesByType(clazz: Class<T>): Map<String, T>
}