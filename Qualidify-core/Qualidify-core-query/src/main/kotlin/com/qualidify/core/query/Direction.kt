package com.qualidify.core.query

enum class Direction { ASC, DESC }