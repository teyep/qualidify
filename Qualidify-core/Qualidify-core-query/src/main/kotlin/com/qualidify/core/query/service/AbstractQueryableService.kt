package com.qualidify.core.query.service

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.repository.QueryablePagingAndSortingRepository
import com.qualidify.core.query.repository.countResponse
import com.qualidify.core.query.repository.findAllByQuery
import java.io.Serializable
import javax.transaction.Transactional

/**
 * Abstract base class for Services implementing [Queryable] and using a [QueryablePagingAndSortingRepository]
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * @param T type of the entity
 * @param D type of the Dto
 */
@Transactional
abstract class AbstractQueryableService<T : Serializable, D : Serializable>(
    private val queryableRepository: QueryablePagingAndSortingRepository<T, out Any>
) : Queryable<D> {

    override fun countByQuery(query: CountQuery) = queryableRepository.countResponse(query)

    override fun findByQuery(query: SearchQuery): MutableList<D> = queryableRepository.findAllByQuery(query)
        .map { mapEntityToDto(it) }
        .toList()

    /**
     * Take care of the mapping form a jpa entity to a DTO
     * @param entity the entity to map
     * @return a Dto
     */
    protected abstract fun mapEntityToDto(entity: T): D
}