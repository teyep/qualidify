package com.qualidify.core.query

/**
 * Indicating a SearchDirection per key
 *
 * @author Marcel Pot
 * @since 1.0
 */
data class SearchDirection(
    val key: String,
    val direction: Direction?,
) {
    constructor(keyDirectionPair: Pair<String, Direction?>) : this(keyDirectionPair.first, keyDirectionPair.second)
}
