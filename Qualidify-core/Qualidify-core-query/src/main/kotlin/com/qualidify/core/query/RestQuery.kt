package com.qualidify.core.query

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import kotlin.reflect.KClass

@JsonIgnoreProperties(
    ignoreUnknown = true
)
data class RestQuery(
    @JsonProperty("offset")
    val offset: Int = 0,
    @JsonProperty("size")
    val size: Int = Int.MAX_VALUE,
    @JsonProperty("constraints")
    val criteria: List<SearchCriterium> = emptyList(),
    @JsonProperty("sortorders")
    val order: List<SearchDirection> = emptyList(),
) : Serializable {

    inline fun <reified T : Serializable> toSearchQuery() =
        SearchQuery(
            beanType = T::class,
            offset = offset,
            size = size,
            searchCriteria = criteria,
            searchDirections = order
        )

    fun <T : Serializable> toSearchQuery(beanType: KClass<T>) =
        SearchQuery(
            beanType = beanType,
            offset = offset,
            size = size,
            searchCriteria = criteria,
            searchDirections = order
        )

    inline fun <reified T : Serializable> toCountQuery() =
        CountQuery(
            beanType = T::class,
            searchCriteria = criteria
        )

    fun <T : Serializable> toCountQuery(beanType: KClass<T>) =
        CountQuery(
            beanType = beanType,
            searchCriteria = criteria
        )
}

