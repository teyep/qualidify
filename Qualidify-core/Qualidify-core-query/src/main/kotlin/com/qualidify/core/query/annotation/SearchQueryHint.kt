package com.qualidify.core.query.annotation

/**
 * Indicator for mapping the dto field to a Database entity
 *
 * @author Marcel Pot
 * @since 1.0
 *
 */
@Retention
@Target(AnnotationTarget.FIELD)
annotation class SearchQueryHint(
    /**
     * Default behaviour is mapping on equal fieldname (value = ~)
     *
     * It is possible to use joins on the entity type, by specifying
     * the jointable (this is the field in the entity class used by the jointable)
     * before the fieldname.
     * For instance:
     *
     * * **fieldname** will take *fieldname* as fieldname
     * * **jointable.~** will join the table *jointable* and use
     *          the dto-column-name as fieldname.
     * * **jointable.fieldname** will first join *jointable* and take *fieldname* as
     *          fieldname
     * * **jointable1.jointable2.~** will first join *jointable1* and continue
     *          with joining *jointable2* and the dto-column-name as fieldname
     * * **jointable1.jointable2.fieldname** will first join *jointable1* and continue
     *          with joining *jointable2* and the *fieldname* as fieldname
     * * __customfield.\*__ will map [CustomField]s where *customfield* is indicating the name of
     *          the field, and _\*_ will be used as indicator for map like mappings
     *
     * @return the persistance Mapper
     */
    val value: String = "~",

    /**
     * Define the field column name. Default is 'field'
     */
    val fieldColumn: String = "field",

    /**
     * Define the value column name. Default is 'value'
     */
    val valueColumn: String = "value"


)


