package com.qualidify.core.query
import java.io.Serializable
import java.time.Instant

/**
 * A Response of the backend indicating the number of items in the backend
 * So the subscriber can handle according this information
 *
 * @author Marcel Pot
 */
data class CountResponse(val count: Long, val lastEvent: Long = Instant.now().toEpochMilli()) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CountResponse) return false

        if (count != other.count) return false

        return true
    }

    override fun hashCode(): Int {
        return count.hashCode()
    }
}

/**
 * A Response of the backend indicating the count of a subscribed query has changed
 * So the subscriber can handle according this information
 *
 * @author Marcel Pot
 */
class CountChangedUpdate() : Serializable