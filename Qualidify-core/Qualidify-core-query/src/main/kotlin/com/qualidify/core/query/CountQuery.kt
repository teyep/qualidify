package com.qualidify.core.query

import java.io.Serializable
import kotlin.reflect.KClass

data class CountQuery(
    val beanType: KClass<out Any>,
    val searchCriteria: List<SearchCriterium> = emptyList(),
) : Serializable {

    /**
     * Map a countQuery to a predicate
     * @return a predicate
     */
    fun <T : Serializable> toFilter() = SearchQueryUtil.determinePredicate<T>(searchCriteria)

    /**
     * Apply the countQuery to a collection
     * @param collection the collection to count
     * @return the size of the filtered collection
     */
    fun <T : Serializable> apply(collection: Collection<T>) = collection
        .filter(toFilter())
        .size

    /**
     * Get a [SearchQuery] with the same constraints
     * @return a SearchQuery
     */
    fun <T : Serializable> toSearchQuery() = SearchQuery(beanType = beanType, searchCriteria = this.searchCriteria)

    /**
     * Apply the countQuery to a collection
     * Warning: This is a heavy algorithm and you need to search for alternatives
     * @param collection to filter
     * @return the number of items
     */
    fun <T : Serializable> applyCount(collection: Iterable<T>): Long =
        collection.asSequence()
            .filter(toFilter())
            .count().toLong()

    /**
     * Apply the countQuery to a collection and create a [CountResponse]
     * Warning: This is a heavy algorithm, and you need to search for alternatives
     * @param collection to filter
     * @return the count respones of items
     */
    fun <T : Serializable> applyCountResponse(collection: Iterable<T>): CountResponse =
        CountResponse(applyCount(collection))

    /**
     * Map a countQuery to a JPA specification
     * @return the Specification
     */
    fun <T : Serializable> toSpecification() = SearchQueryUtil.specification<T>(this)

    /**
     * Apply an extra filter on the CountQuery
     * @param key, the key to filter with
     * @param value the value to filter on
     * @return a new immutable CountQuery
     */
    fun alsoFilterWith(key: String, value: String) = CountQuery(
        beanType = beanType,
        searchCriteria = searchCriteria.toMutableList().also { it.add(SearchCriterium(key, value)) },
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CountQuery) return false

        if (beanType != other.beanType) return false
        if (searchCriteria != other.searchCriteria) return false

        return true
    }

    override fun hashCode(): Int {
        var result = beanType.hashCode()
        result = 31 * result + searchCriteria.hashCode()
        return result
    }


}

@Suppress("FunctionName")
inline fun <reified T : Any> CountQuery(
    searchCriteria: List<SearchCriterium> = emptyList(),
) = CountQuery(
    beanType = T::class,
    searchCriteria = searchCriteria
)

@Suppress("FunctionName")
inline fun <reified T : Any> CountQuery(
    constraints: Map<String, String?>
) = CountQuery(
    beanType = T::class,
    searchCriteria = constraints.asSequence().map { SearchCriterium(it.key, it.value) }.toList()
)