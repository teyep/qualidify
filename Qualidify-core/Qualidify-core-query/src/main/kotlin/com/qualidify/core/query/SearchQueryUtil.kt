package com.qualidify.core.query


import com.qualidify.core.log.LoggerDelegation
import com.qualidify.core.query.CustomFieldPredicateCreator.Companion.CUSTOMFIELD_REGEXP
import com.qualidify.core.query.SearchOperation.*
import com.qualidify.core.query.annotation.SearchQueryHint
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import java.lang.reflect.Field
import javax.persistence.criteria.*
import kotlin.reflect.KClass


/**
 * A Util class to help map a SearchQuery to parameters for the JPA search
 *
 * @author Marcel Pot
 */
internal object SearchQueryUtil {

    const val FIELD = "field"
    const val VALUE = "value"
    val CUSTOMFIELD_REGEXP = "^(.*?)\\[(.*?)\\]$".toRegex();

    private val logger by LoggerDelegation()

    /**
     * Return a comparator for sorting a collection
     * @param query the SearchQuery
     * @param T the Type of the entities to compare
     * @return a comparator
     */
    @JvmStatic
    fun <T : Any> collectionSort(query: SearchQuery): Comparator<T?> {
        var comparator = Comparator { _: T?, _: T? -> 0 }

        for (sortOrder in query.searchDirections) {
            when (sortOrder.direction) {
                Direction.ASC, null -> comparator = comparator.thenComparing { first, second ->
                    compareValues(
                        valueOf(first, sortOrder.key),
                        valueOf(second, sortOrder.key)
                    )
                }

                Direction.DESC -> comparator = comparator.thenComparing { first, second ->
                    -1 * compareValues(
                        valueOf(
                            first,
                            sortOrder.key
                        ), valueOf(second, sortOrder.key)
                    )
                }

            }
        }
        return comparator
    }

    @JvmStatic
    fun <T> determinePredicate(constraints: List<SearchCriterium>): (T) -> Boolean {
        val predicates = constraints.asSequence()
            .map { constraint ->
                { test: T ->
                    try {
                        val field = stringValueOf(test, constraint.key)
                        field.lowercase().contains(constraint.value.toString().lowercase())
                    } catch (e: Exception) {
                        logger.error("Predicate Problem", e)
                        false
                    }
                }
            }.toList()
        return { it -> predicates.all { predicate -> predicate(it) } }
    }


    @JvmStatic
    fun <T> determinePredicate(constraints: Map<String, String?>): (T) -> Boolean {
        val predicates = constraints.entries.asSequence()
            .map { constraint ->
                { test: T ->
                    try {
                        val field = stringValueOf(test, constraint.key)
                        field.lowercase().contains(constraint.value.toString().lowercase())
                    } catch (e: Exception) {
                        logger.error("Predicate Problem", e)
                        false
                    }
                }
            }.toList()
        return { it -> predicates.all { predicate -> predicate(it) } }
    }

    /**
     * Create a Sort based on a SearchQuery
     * @param query the SearchQuery
     * @return a Sort object used by the jpa repository for sorting
     */
    @JvmStatic
    fun toSort(query: SearchQuery): Sort {
        var sort = Sort.unsorted();
        for (sortOrder in query.searchDirections) {
            val key = determineSearchQueryPath(query.beanType, sortOrder.key)
            when (sortOrder.direction) {
                Direction.ASC -> sort = sort.and(Sort.by(key).ascending())
                Direction.DESC -> sort = sort.and(Sort.by(key).descending())
                else -> continue
            }
        }
        return sort;
    }

    /**
     * Map a countQuery to a @link{org.springframework.data.jpa.domain.Specification}
     * @param query: the CountQuery
     * @return the Specification
     */
    @JvmStatic
    fun <T : Any> specification(query: CountQuery): Specification<T> {
        return SearchCriteriaSpecification(query.beanType, query.searchCriteria)
    }

    /**
     * Map a searchQuery to a @link{org.springframework.data.jpa.domain.Specification}
     * @param query: the SearchQuery
     * @return the Specification
     */
    @JvmStatic
    fun <T : Any> specification(query: SearchQuery): Specification<T> {
        return SearchCriteriaSpecification(query.beanType, query.searchCriteria)
    }


    private fun <T> valueOf(value: T, fieldName: String): String? {
        return try {
            val field: Field = value!!::class.java.getDeclaredField(fieldName)
            field.setAccessible(true)
            field.get(value)?.toString()
        } catch (ex: Exception) {
            throw SearchQueryException("Cannot read field $fieldName from class ${value!!::class.java}", ex)
        }
    }

    private fun <T> stringValueOf(value: T, fieldName: String) = valueOf(value, fieldName).toString()


}

/**
 * An implementation of the [Specification] interface to create a Specification based on a beanType and searchCriteria
 *
 * @author Marcel Pot
 * @since 1.0
 */
internal class SearchCriteriaSpecification<T>(
    val beanType: KClass<out Any>,
    val criteria: List<SearchCriterium>,
) : Specification<T> {

    companion object {
        val logger by LoggerDelegation()
    }

    override fun toPredicate(root: Root<T>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {
        val predicates: MutableList<Predicate> = ArrayList()

        for (criterium in criteria) {
            val hint = determineSearchQueryPath(beanType, criterium.key)

            val predicate = if (isCustomField(hint)) {
                CustomFieldPredicateCreator<T>(beanType, criterium).create(root, query, criteriaBuilder)
            } else if (isJoin(hint)) {
                JoinPredicateCreator<T>(beanType, criterium).create(root, query, criteriaBuilder)
            } else {
                DirectPredicateCreator<T>(beanType, criterium).create(root, query, criteriaBuilder)
            }
            predicate?.let { predicates.add(it) }
        }
        return criteriaBuilder.and(*predicates.toTypedArray())
    }

    private fun isJoin(hint: String) = hint.contains(".")

    private fun isCustomField(hint: String) = hint.matches(CUSTOMFIELD_REGEXP)
}

/**
 * A Predicate Creator for direct mappings
 * @author Marcel Pot
 * @since 1.0
 */
internal class DirectPredicateCreator<T>(val beanType: KClass<out Any>, val criterium: SearchCriterium) :
    AbstractPredicateCreator<T>() {

    val hint = determineSearchQueryPath(beanType, criterium.key)

    override fun create(root: Root<T>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {
        val entityPath = root.get<Any>(hint)
        val value = getValue(entityPath, criterium)

        return createSimpleCriteriaBuilderPredicate(criteriaBuilder, criterium, entityPath, value)
    }
}

/**
 * A Predicate Creator for join mappings.
 * Warning: At this moment only one join is supported
 *
 * @author Marcel Pot
 * @since 1.0
 */
internal class JoinPredicateCreator<T>(val beanType: KClass<out Any>, val criterium: SearchCriterium) :
    AbstractPredicateCreator<T>() {

    val hint = determineSearchQueryPath(beanType, criterium.key)

    override fun create(root: Root<T>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {
        val parts = hint.split(".")
        val entityPath = root.join<Any, Any>(parts[0]).get<Any>(parts[1])
        val value = getValue(entityPath, criterium)

        return createSimpleCriteriaBuilderPredicate(criteriaBuilder, criterium, entityPath, value)
    }
}

/**
 * A Predicate Creator designed for custom fields predicates
 *
 * @author Marcel Pot
 * @since 1.0
 */
internal class CustomFieldPredicateCreator<T>(val beanType: KClass<out Any>, val criterium: SearchCriterium) :
    AbstractPredicateCreator<T>() {

    companion object {
        val CUSTOMFIELD_REGEXP = "^(.*?)\\[(.*?)\\]$".toRegex()
    }

    val searchQueryHint = findSearchQueryHint(beanType, criterium.key)!!
    val searchQueryPath = determineSearchQueryPath(criterium.key, searchQueryHint)

    override fun create(root: Root<T>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate? {
        val parts = CUSTOMFIELD_REGEXP.find(searchQueryPath)!!.groupValues

        val firstPredicate =
            criteriaBuilder.equal(
                root.join<Any, Any>(parts[1]).get<String>(searchQueryHint.fieldColumn), parts[2]
            )

        val entityPath = root.join<Any, Any>(parts[1]).get<String>(searchQueryHint.valueColumn)
        val value = getValue(entityPath, criterium)

        val secondPredicate = createSimpleCriteriaBuilderPredicate(criteriaBuilder, criterium, entityPath, value)

        if (secondPredicate == null) {
            throw CustomFieldSearchQueryException("Problem with CustomField Specification.")
        }

        return criteriaBuilder.and(firstPredicate, secondPredicate)
    }
}

/**
 * Abstract Predicate Creator with a lot of CriteriaBuilder predicates functionality build in
 *
 * @author Marcel Pot
 * @since 1.0
 */
abstract class AbstractPredicateCreator<T>() {

    abstract fun create(root: Root<T>, query: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder): Predicate?

    @Suppress("UNCHECKED_CAST")
    internal fun createSimpleCriteriaBuilderPredicate(
        criteriaBuilder: CriteriaBuilder,
        criterium: SearchCriterium,
        expression: Expression<*>,
        value: Any?,
    ): Predicate? {

        if (value == null) {
            return nullPredicates(criteriaBuilder, criterium, expression)
        }
        val javaType = expression.javaType

        if (Boolean::class.java.isAssignableFrom(javaType)) {
            return booleanPredicates(
                criteriaBuilder,
                criterium,
                expression as Path<Boolean>,
                value as Boolean
            )
        } else if (Number::class.java.isAssignableFrom(javaType)) {
            return numericalPredicates(
                criteriaBuilder,
                criterium,
                expression as Path<Number>,
                value as Number
            )
        } else if (javaType == String::class.java) {
            return stringPredicates(
                criteriaBuilder,
                criterium,
                expression as Path<String>,
                value as String
            )
        } else {
            return stringPredicates(
                criteriaBuilder,
                criterium,
                criteriaBuilder.function("text", String::class.java, expression) as Expression<String>,
                value.toString()
            )
        }
    }

    @Suppress("UNCHECKED_CAST")
    internal fun getValue(expression: Expression<*>, criteria: SearchCriterium): Any? {
        val fieldType = expression.javaType

        if (fieldType.isAssignableFrom(Boolean::class.java)) {
            return criteria.value?.toBoolean()
        } else if (fieldType.isAssignableFrom(Double::class.java)) {
            return criteria.value?.toDouble() ?: 0.0
        } else if (fieldType.isAssignableFrom(Int::class.java)) {
            return criteria.value?.toInt() ?: 0
        } else if (java.lang.Enum::class.java.isAssignableFrom(fieldType) && criteria.value != null) {
            try {
                return java.lang.Enum.valueOf(fieldType as Class<out Enum<*>>, criteria.value)
            } catch (iae: IllegalArgumentException) {
                SearchCriteriaSpecification.logger.warn("Specification Enum error: ${iae.message}")
            }
        }
        return criteria.value
    }

    fun booleanPredicates(
        criteriaBuilder: CriteriaBuilder,
        criterium: SearchCriterium,
        expression: Expression<Boolean>,
        value: Boolean,
    ): Predicate? {
        return when (criterium.operation) {
            LIKE ->
                criteriaBuilder.equal(expression, value)

            else -> null
        }
    }

    fun numericalPredicates(
        criteriaBuilder: CriteriaBuilder,
        criterium: SearchCriterium,
        expression: Expression<out Number>,
        value: Number,
    ): Predicate? {
        return when (criterium.operation) {
            GREATER_THAN ->
                criteriaBuilder.gt(expression, value)

            LESS_THAN ->
                criteriaBuilder.lt(expression, value)

            GREATER_THAN_EQUAL ->
                criteriaBuilder.ge(expression, value)

            LESS_THAN_EQUAL ->
                criteriaBuilder.le(expression, value)

            NOT_EQUAL ->
                criteriaBuilder.notEqual(expression, value)

            EQUAL ->
                criteriaBuilder.equal(expression, value)


            else -> null
        }
    }

    private fun stringPredicates(
        criteriaBuilder: CriteriaBuilder,
        criterium: SearchCriterium,
        expression: Expression<String>,
        value: String,
    ): Predicate? {

        return when (criterium.operation) {
            GREATER_THAN ->
                criteriaBuilder.greaterThan(expression, value)

            LESS_THAN ->
                criteriaBuilder.lessThan(expression, value)

            GREATER_THAN_EQUAL ->
                criteriaBuilder.greaterThanOrEqualTo(expression, value)

            LESS_THAN_EQUAL ->
                criteriaBuilder.lessThanOrEqualTo(expression, value)

            NOT_EQUAL ->
                criteriaBuilder.notEqual(expression, value)

            EQUAL ->
                criteriaBuilder.equal(expression, value)

            LIKE ->
                criteriaBuilder.like(expression, "%$value%")

            NOT_LIKE ->
                criteriaBuilder.notLike(expression, "%$value%")

            IS_NULL ->
                criteriaBuilder.isNull(expression)

        }
    }

    fun nullPredicates(
        criteriaBuilder: CriteriaBuilder,
        criterium: SearchCriterium,
        expression: Expression<*>,
    ): Predicate? {

        return when (criterium.operation) {
            IS_NULL ->
                criteriaBuilder.isNull(expression)

            else -> null
        }
    }
}

fun determineSearchQueryPath(beanType: KClass<out Any>, fieldName: String): String {
    val hint = findSearchQueryHint(beanType, fieldName)
    return determineSearchQueryPath(fieldName, hint)
}

fun determineSearchQueryPath(fieldName: String, hint: SearchQueryHint?): String {
    return hint?.value
        ?.replace("~", fieldName)
        ?.replace("*", fieldName)
        ?: fieldName
}

fun findSearchQueryHint(beanType: KClass<out Any>, fieldName: String): SearchQueryHint? {
    val fields = beanType.java.declaredFields

    if (fields.asSequence().map { it.name }.toList().contains(fieldName)) {
        val field: Field = beanType.java.getDeclaredField(fieldName)
        return field.getAnnotation(SearchQueryHint::class.java)

    } else {
        //determine the customfield annotation, this must be annotated containing a wildcard (*)
        return beanType.java.declaredFields.asSequence()
            .filter { it.isAnnotationPresent(SearchQueryHint::class.java) }
            .map { it.getAnnotation(SearchQueryHint::class.java) }
            .filter { it.value.contains("*") }
            .firstOrNull()

    }

}

open class SearchQueryException(msg: String? = null, cause: Throwable? = null) : RuntimeException(msg, cause)
class CustomFieldSearchQueryException(msg: String? = null, cause: Throwable? = null) : SearchQueryException(msg, cause)