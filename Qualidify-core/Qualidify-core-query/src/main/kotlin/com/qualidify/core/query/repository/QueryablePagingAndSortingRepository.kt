package com.qualidify.core.query.repository

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import com.qualidify.core.query.SearchQueryUtil
import org.springframework.data.domain.Page
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.data.repository.PagingAndSortingRepository


/**
 * A combination of different page, sorting, and filtering methods to retrieve
 * data from a datasource
 * @param T the type of the data
 * @param ID the identity of the data
 *
 * @author Marcel Pot
 * @since 1.0
 */
@NoRepositoryBean
interface QueryablePagingAndSortingRepository<T : Any, ID> : PagingAndSortingRepository<T, ID>,
    JpaSpecificationExecutor<T>

/**
 * Find a paginated slice of the data based on a SearchQuery
 * Written as extension function to prevent Spring to create a query for this function
 * @param query the SearchQuery
 * @return a pageable slice of the data
 */
fun <T : Any, ID> QueryablePagingAndSortingRepository<T, ID>.findAllByQuery(query: SearchQuery): Page<T> {
    val page = query.toPage()
    val specification = SearchQueryUtil.specification<T>(query)
    return findAll(specification, page)
}

/**
 * Find a paginated slice of the data based on a SearchQuery
 * Written as extension function to prevent Spring to create a query for this function
 * @param query the SearchQuery
 * @param mapper a lambda for mapping type T to type D
 * @param D the type of the Dto
 * @return a pageable slice of the data
 */
fun <T : Any, ID, D : Any> QueryablePagingAndSortingRepository<T, ID>.findAllByQueryAndMap(
    query: SearchQuery,
    mapper: (T) -> D
): Page<D> {
    return findAllByQuery(query).map(mapper)
}

/**
 * Count the data based on the specification in the countQuery
 * Written as extension function to prevent Spring to create a query for this function
 * @param query the CountQuery
 * @return the number of records found
 */
fun <T : Any, ID> QueryablePagingAndSortingRepository<T, ID>.countByQuery(query: CountQuery): Long {
    val specification = SearchQueryUtil.specification<T>(query)
    return count(specification)
}

/**
 * Returns a Count response based on the specification in the countQuery
 * Written as extension function to prevent Spring to create a query for this function
 * @param query the CountQuery
 * @return the number of records found
 */
fun <T : Any, ID> QueryablePagingAndSortingRepository<T, ID>.countResponse(query: CountQuery): CountResponse {
    return CountResponse(countByQuery(query))
}

