package com.qualidify.core.query

/**
 * A value object containing information how to filter data
 * based on a key-column and a value
 *
 * @author Marcel Pot
 * @since 1.0
 */
data class SearchCriterium(
    val key: String,
    val value: String?,
    val operation: SearchOperation = SearchOperation.LIKE,
    val ignoreCase: Boolean = true,
) {
    constructor(keyValuePair: Pair<String, String?>) : this(keyValuePair.first, keyValuePair.second)
}