package com.qualidify.core.query.service

import com.qualidify.core.query.CountQuery
import com.qualidify.core.query.CountResponse
import com.qualidify.core.query.SearchQuery
import java.io.Serializable

/**
 * An interface indication if a Service is queryable with [CountQuery] and [SearchQuery]
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface Queryable<T : Serializable> {

    /**
     * Get the number of elements based on the query information
     * @param query the Query information
     * @return a CountResponse
     */
    fun countByQuery(query: CountQuery): CountResponse

    /**
     * Get all elements based on the query information
     * @param query the Query information
     * @return a Collection of elements
     */
    fun findByQuery(query: SearchQuery): Collection<T>

}