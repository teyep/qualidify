package com.qualidify.core.query

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import java.io.Serializable
import kotlin.reflect.KClass

data class SearchQuery(
    val beanType: KClass<out Any>,
    val offset: Int = 0,
    val size: Int = Int.MAX_VALUE,
    val searchCriteria: List<SearchCriterium> = emptyList(),
    val searchDirections: List<SearchDirection> = emptyList(),
) : Serializable {

    /**
     * Map a searchQeury to a comparator
     * @return a comparator
     */
    fun <T : Serializable> toComparator() = SearchQueryUtil.collectionSort<T>(this)

    /**
     * Map a searchQuery to a @link{org.springframework.data.domain.Sort}
     * @return the Sort
     */
    fun toSort(): Sort = SearchQueryUtil.toSort(this)

    /**
     * Map a searchQuery to a predicate
     * @return a predicate
     */
    fun <T : Serializable> toFilter() = SearchQueryUtil.determinePredicate<T>(searchCriteria)

    /**
     * Map a searchQuery to a @link{org.springframework.data.domain.Pageable}
     * @return the Pageable
     */
    fun toPage() = PageRequest.of(offset / size, size, toSort())

    /**
     * Map a searchQuery to a JPA specification
     * @return the Specification
     */
    fun <T : Serializable> toSpecification() = SearchQueryUtil.specification<T>(this)

    /**
     * Apply the searchQuery to a collection
     * Warning: This is a heavy algorithm and you need to search for alternatives
     * @param collection to filter and sort
     * @return a List of filtered and sorted and paginated data
     */
    fun <T : Serializable> applyToList(collection: Iterable<T>): List<T> =
        collection.asSequence()
            .filter(toFilter())
            .sortedWith(toComparator())
            .drop(this.offset)
            .take(this.size)
            .toList()

    /**
     * Apply the searchQuery to a collection
     * Warning: This is a heavy algorithm, and you need to search for alternatives
     * @param collection to filter and sort
     * @return a Page of filtered and sorted data
     */
    fun <T : Serializable> applyToPage(collection: Iterable<T>): Page<T> {
        val subCollection = applyToList(collection)
        return PageImpl(subCollection, toPage(), subCollection.count().toLong())
    }

    /**
     * Get a [CountQuery] with the same constraints
     * @return a CountQuery
     */
    fun <T : Serializable> toCountQuery() = CountQuery(beanType = beanType, searchCriteria = this.searchCriteria)

    /**
     * Apply an extra filter on the SearchQuery
     * @param key, the key to filter with
     * @param value the value to filter on
     * @return a new immutable SearchQuery
     */
    fun alsoFilterWith(key: String, value: String) = SearchQuery(
        beanType = beanType,
        offset = offset,
        size = size,
        searchCriteria = searchCriteria.toMutableList().also { it.add(SearchCriterium(key, value)) },
        searchDirections = searchDirections
    )
}

@Suppress("FunctionName")
inline fun <reified T : Any> SearchQuery(
    offset: Int = 0,
    size: Int = Int.MAX_VALUE,
    searchCriteria: List<SearchCriterium> = emptyList(),
    searchDirections: List<SearchDirection> = emptyList()
) = SearchQuery(
    beanType = T::class,
    offset = offset,
    size = size,
    searchCriteria = searchCriteria,
    searchDirections = searchDirections
)

@Suppress("FunctionName")
inline fun <reified T : Any> SearchQuery(
    offset: Int,
    size: Int,
    constraints: Map<String, String?>,
    sortOrders: Map<String, Direction?>
) = SearchQuery(
    beanType = T::class,
    offset = offset,
    size = size,
    searchCriteria = constraints.asSequence().map { SearchCriterium(it.key, it.value) }.toList(),
    searchDirections = sortOrders.asSequence().map { SearchDirection(it.key, it.value) }.toList()
)

@Suppress("FunctionName")
inline fun <reified T : Any> SearchQuery(
    constraints: Map<String, String?>
) = SearchQuery(
    beanType = T::class,
    offset = 0,
    size = Int.MAX_VALUE,
    searchCriteria = constraints.asSequence().map { SearchCriterium(it.key, it.value) }.toList(),
    searchDirections = emptyList()
)

@Suppress("FunctionName")
inline fun <reified T : Any> SearchQuery(
    constraints: Map<String, String?>,
    sortOrders: Map<String, Direction?>
) = SearchQuery(
    beanType = T::class,
    offset = 0,
    size = Int.MAX_VALUE,
    searchCriteria = constraints.asSequence().map { SearchCriterium(it.key, it.value) }.toList(),
    searchDirections = sortOrders.asSequence().map { SearchDirection(it.key, it.value) }.toList()
)