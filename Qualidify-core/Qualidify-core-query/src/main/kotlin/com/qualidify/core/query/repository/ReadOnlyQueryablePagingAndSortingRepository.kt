package com.qualidify.core.query.repository

import org.springframework.data.repository.NoRepositoryBean

/**
 * An interface for Readonly access on a repository
 *
 * @author Marcel Pot
 * @since 1.0
 */
@NoRepositoryBean
interface ReadOnlyQueryablePagingAndSortingRepository<T : Any, ID> : QueryablePagingAndSortingRepository<T, ID> {

    override fun <S : T> save(entity: S): S {
        throw ModificationOnReadOnlyRepositoryException()
    }

    override fun <S : T> saveAll(entities: MutableIterable<S>): MutableIterable<S> {
        throw ModificationOnReadOnlyRepositoryException()
    }

    override fun deleteById(id: ID) {
        throw ModificationOnReadOnlyRepositoryException()
    }

    override fun delete(entity: T) {
        throw ModificationOnReadOnlyRepositoryException()
    }

    override fun deleteAllById(ids: MutableIterable<ID>) {
        throw ModificationOnReadOnlyRepositoryException()
    }

    override fun deleteAll(entities: MutableIterable<T>) {
        throw ModificationOnReadOnlyRepositoryException()
    }

    override fun deleteAll() {
        throw ModificationOnReadOnlyRepositoryException()
    }
}

/**
 * Exception to indicate a problem modifying data on a readonly repository
 *
 * @author Marcel Pot
 * @since 1.0
 */
class ModificationOnReadOnlyRepositoryException(
    message: String? = "Action is not allowed on a readonly repository",
    throwable: Throwable? = null
) : RuntimeException(message, throwable)