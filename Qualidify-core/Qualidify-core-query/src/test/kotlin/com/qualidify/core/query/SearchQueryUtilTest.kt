package com.qualidify.core.query

import com.qualidify.core.query.SearchQueryUtil.specification
import com.qualidify.core.query.annotation.SearchQueryHint
import org.assertj.core.api.Assertions.assertThat
import org.hibernate.query.criteria.internal.CriteriaBuilderImpl
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.data.domain.Sort
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.*
import javax.persistence.*
import javax.persistence.criteria.*

@ExtendWith(MockitoExtension::class)
internal class SearchQueryUtilTest {

    @Mock
    lateinit var criteriaBuilderMock: CriteriaBuilderImpl

    @Mock
    lateinit var criteriaQueryMock: CriteriaQuery<*>

    @Mock
    lateinit var personRootMock: Root<Person>

    @Test
    fun collectionSortTest() {
        val john = Person(firstname = "John", lastname = "Lennon")
        val paul = Person(
            firstname = "Paul",
            lastname = "McCartney",
            age = ChronoUnit.YEARS.between(LocalDate.of(1942, 6, 18), LocalDate.now()).toString()
        )
        val george = Person(firstname = "George", lastname = "Harrison")
        val ringo = Person(
            firstname = "Ringo",
            lastname = "Starr",
            age = ChronoUnit.YEARS.between(LocalDate.of(1940, 7, 7), LocalDate.now()).toString()
        )

        val beatles = listOf(john, paul, george, ringo)

        val firstnameAscComparator =
            SearchQueryUtil.collectionSort<Person>(SearchQuery<PersonDto>(searchDirections = listOf(SearchDirection("firstname" to Direction.ASC))))
        assertThat(beatles.sortedWith(firstnameAscComparator))
            .`as`("Beatles sorted on first name ascending").isEqualTo(listOf(george, john, paul, ringo))

        val firstnameDescComparator =
            SearchQueryUtil.collectionSort<Person>(SearchQuery<PersonDto>(searchDirections = listOf(SearchDirection("firstname" to Direction.DESC))))
        assertThat(beatles.sortedWith(firstnameDescComparator))
            .`as`("Beatles sorted on first name descending").isEqualTo(listOf(ringo, paul, john, george))

        val ageAscComparator =
            SearchQuery<PersonDto>(searchDirections = listOf(SearchDirection("age" to Direction.ASC)))
                .toComparator<Person>()
        assertThat(beatles.sortedWith(ageAscComparator).subList(2, 4))
            .`as`("Beatles sorted on age inclusive null check").isEqualTo(listOf(paul, ringo))
        assertThat(beatles.sortedWith(ageAscComparator).subList(0, 2))
            .`as`("Beatles sorted on age inclusive null check").contains(john, george)
    }

    @Test
    fun simplePageRequestTest() {
        val query = SearchQuery<Any>(0, 12, emptyList())
        val page = query.toPage()

        assertThat(page.pageNumber).`as`("Pageable pagenumber").isZero
        assertThat(page.pageSize).`as`("Pageable limit").isEqualTo(12)
        assertThat(page.offset).`as`("Pageable offset").isZero
        assertThat(page.sort).`as`("Pageable sort").isNotNull.isEqualTo(Sort.unsorted())
    }

    @Test
    fun ascendingSortPageRequestTest() {
        val query = SearchQuery<PersonDto>(0, 15, searchCriteria(), sortOrderAsc("id", "lastname"))
        val page = query.toPage()

        assertThat(page.pageNumber).`as`("Pageable pagenumber").isZero
        assertThat(page.pageSize).`as`("Pageable limit").isEqualTo(15)
        assertThat(page.offset).`as`("Pageable offset").isZero
        assertThat(page.sort).`as`("Pageable sort").isNotNull
            .isEqualTo(Sort.unsorted().and(Sort.by("id").ascending()).and(Sort.by("lastname").ascending()))
    }

    @Test
    fun descendingSortPageRequestTest() {
        val query = SearchQuery<PersonDto>(46, 15, searchCriteria(), sortOrderDesc("id", "firstname"))
        val page = query.toPage()

        assertThat(page.pageNumber).`as`("Pageable pagenumber").isEqualTo(3)
        assertThat(page.pageSize).`as`("Pageable size").isEqualTo(15)
        assertThat(page.offset).`as`("Pageable offset").isEqualTo(45)
        assertThat(page.sort).`as`("Pageable sort").isNotNull
            .isEqualTo(Sort.unsorted().and(Sort.by("id").descending()).and(Sort.by("firstname").descending()))
    }

    @Test
    fun missingDirectionSortPageRequestTest() {
        val query = SearchQuery<PersonDto>(2, 30, searchCriteria(), sortOrderNull("id", "age"))
        val page = query.toPage()

        assertThat(page.pageNumber).`as`("Pageable pagenumber").isZero
        assertThat(page.pageSize).`as`("Pageable size").isEqualTo(30)
        assertThat(page.offset).`as`("Pageable offset").isZero
        assertThat(page.sort).`as`("Pageable sort").isNotNull
            .isEqualTo(Sort.unsorted())
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun directSpecificationTest_withCountQuery() {

        val firstNamePathMock = mock(
            Path::class.java
        ) as Path<String>

        val likeFirstNamePredicateMock = mock(Predicate::class.java)

        `when`(personRootMock.get<String>("firstname"))
            .thenReturn(firstNamePathMock)
        `when`(firstNamePathMock.javaType)
            .thenReturn(String::class.java)
        `when`(criteriaBuilderMock.like(firstNamePathMock, "%A%"))
            .thenReturn(likeFirstNamePredicateMock)
        `when`(criteriaBuilderMock.and(likeFirstNamePredicateMock))
            .thenReturn(likeFirstNamePredicateMock)

        val query = CountQuery<PersonDto>(searchCriteria("firstname~A"))
        val specification = specification<Person>(query)
        val actualPredicate = specification.toPredicate(personRootMock, criteriaQueryMock, criteriaBuilderMock)

        assertThat(likeFirstNamePredicateMock).isEqualTo(actualPredicate)


        verify(personRootMock, times(1)).get<Any>("firstname")
        verifyNoMoreInteractions(personRootMock)

        verify(firstNamePathMock, times(2)).javaType
        verifyNoMoreInteractions(firstNamePathMock)

        verify(criteriaBuilderMock, times(1)).like(firstNamePathMock, "%A%")
        verify(criteriaBuilderMock, times(1)).and(likeFirstNamePredicateMock)
        verifyNoMoreInteractions(criteriaBuilderMock)

        verifyNoInteractions(criteriaQueryMock, likeFirstNamePredicateMock)

    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun directSpecificationTest_withSearchQuery() {

        val firstNamePathMock = mock(
            Path::class.java
        ) as Path<String>

        val likeFirstNamePredicateMock = mock(Predicate::class.java)

        `when`(personRootMock.get<String>("firstname"))
            .thenReturn(firstNamePathMock)
        `when`(firstNamePathMock.javaType)
            .thenReturn(String::class.java)
        `when`(criteriaBuilderMock.like(firstNamePathMock, "%A%"))
            .thenReturn(likeFirstNamePredicateMock)
        `when`(criteriaBuilderMock.and(likeFirstNamePredicateMock))
            .thenReturn(likeFirstNamePredicateMock)

        val query = SearchQuery<PersonDto>(0, 100, searchCriteria("firstname~A"), sortOrder())
        val specification = specification<Person>(query)
        val actualPredicate = specification.toPredicate(personRootMock, criteriaQueryMock, criteriaBuilderMock)

        assertThat(likeFirstNamePredicateMock).isEqualTo(actualPredicate)

        verify(personRootMock, times(1)).get<Any>("firstname")
        verifyNoMoreInteractions(personRootMock)

        verify(firstNamePathMock, times(2)).javaType
        verifyNoMoreInteractions(firstNamePathMock)

        verify(criteriaBuilderMock, times(1)).like(firstNamePathMock, "%A%")
        verify(criteriaBuilderMock, times(1)).and(likeFirstNamePredicateMock)
        verifyNoMoreInteractions(criteriaBuilderMock)

        verifyNoInteractions(criteriaQueryMock, likeFirstNamePredicateMock)
    }


    @Test
    @Suppress("UNCHECKED_CAST")
    fun joinedFieldSpecificationTest_withCountQuery() {
        val personAddressJoinMock = mock(Join::class.java) as Join<String, String>
        val residenceMock = mock(Path::class.java) as Path<String>

        val likeResidencePredicate = mock(
            Predicate::class.java
        )

        `when`(personRootMock.join<String, String>("address"))
            .thenReturn(personAddressJoinMock)
        `when`<Path<String>>(personAddressJoinMock["residence"])
            .thenReturn(residenceMock)
        `when`(residenceMock.javaType)
            .thenReturn(String::class.java)
        `when`(criteriaBuilderMock.like(residenceMock, "%Rabbit%"))
            .thenReturn(likeResidencePredicate)
        `when`(criteriaBuilderMock.and(likeResidencePredicate))
            .thenReturn(likeResidencePredicate)

        val query = CountQuery<PersonDto>(searchCriteria("residence~Rabbit"))
        val specification = specification<Person>(query)
        val actualPredicate = specification.toPredicate(personRootMock, criteriaQueryMock, criteriaBuilderMock)

        assertThat(likeResidencePredicate).isEqualTo(actualPredicate)

        verify(personRootMock, times(1)).join<Any, Any>("address")
        verifyNoMoreInteractions(personRootMock)

        verify(personAddressJoinMock, times(1)).get<String>("residence")
        verifyNoMoreInteractions(personAddressJoinMock)

        verify(residenceMock, times(2)).javaType
        verifyNoMoreInteractions(residenceMock)

        verify(criteriaBuilderMock, times(1)).like(residenceMock, "%Rabbit%")
        verify(criteriaBuilderMock, times(1)).and(likeResidencePredicate)
        verifyNoMoreInteractions(criteriaBuilderMock)

        verifyNoInteractions(criteriaQueryMock, likeResidencePredicate)

    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun joinedFieldSpecificationTest_withSearchQuery() {
        val personAddressJoinMock = mock(Join::class.java) as Join<String, String>
        val residenceMock = mock(Path::class.java) as Path<String>

        val likeResidencePredicate = mock(
            Predicate::class.java
        )

        `when`(personRootMock.join<String, String>("address"))
            .thenReturn(personAddressJoinMock)
        `when`<Path<String>>(personAddressJoinMock["residence"])
            .thenReturn(residenceMock)
        `when`(residenceMock.javaType)
            .thenReturn(String::class.java)
        `when`(criteriaBuilderMock.like(residenceMock, "%Rabbit%"))
            .thenReturn(likeResidencePredicate)
        `when`(criteriaBuilderMock.and(likeResidencePredicate))
            .thenReturn(likeResidencePredicate)

        val query = SearchQuery<PersonDto>(0, 10, searchCriteria("residence~Rabbit"), sortOrder())
        val specification = specification<Person>(query)
        val actualPredicate = specification.toPredicate(personRootMock, criteriaQueryMock, criteriaBuilderMock)

        assertThat(likeResidencePredicate).isEqualTo(actualPredicate)

        verify(personRootMock, times(1)).join<Any, Any>("address")
        verifyNoMoreInteractions(personRootMock)

        verify(personAddressJoinMock, times(1)).get<String>("residence")
        verifyNoMoreInteractions(personAddressJoinMock)

        verify(residenceMock, times(2)).javaType
        verifyNoMoreInteractions(residenceMock)

        verify(criteriaBuilderMock, times(1)).like(residenceMock, "%Rabbit%")
        verify(criteriaBuilderMock, times(1)).and(likeResidencePredicate)
        verifyNoMoreInteractions(criteriaBuilderMock)

        verifyNoInteractions(criteriaQueryMock, likeResidencePredicate)

    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun customFieldSpecificationTest_withCountQuery() {
        val fieldsMock = mock(Path::class.java) as Path<String>
        val valueMock = mock(Path::class.java) as Path<String>
        val extraFieldsJoinMock = mock(Join::class.java) as Join<Any, Any>

        val equalFieldPredicate = mock(Predicate::class.java)
        val likeValuePredicate = mock(Predicate::class.java)
        val combinationPredicate = mock(Predicate::class.java)

        `when`(personRootMock.join<Any, Any>("extraFields"))
            .thenReturn(extraFieldsJoinMock)
        `when`(valueMock.javaType)
            .thenReturn(String::class.java)
        `when`<Path<String>>(extraFieldsJoinMock["field"])
            .thenReturn(fieldsMock)
        `when`<Path<String>>(extraFieldsJoinMock["value"])
            .thenReturn(valueMock)
        `when`(criteriaBuilderMock.equal(fieldsMock, "email"))
            .thenReturn(equalFieldPredicate)
        `when`(criteriaBuilderMock.like(valueMock, "%Alice%"))
            .thenReturn(likeValuePredicate)
        `when`(criteriaBuilderMock.and(equalFieldPredicate, likeValuePredicate))
            .thenReturn(combinationPredicate)
        `when`(criteriaBuilderMock.and(combinationPredicate))
            .thenReturn(combinationPredicate)

        val query = CountQuery<PersonDto>(searchCriteria("email~Alice"))
        val personSpecification = specification<Person>(query)
        val actualPredicate = personSpecification.toPredicate(personRootMock, criteriaQueryMock, criteriaBuilderMock)

        assertThat(combinationPredicate).isEqualTo(actualPredicate)

        verify(personRootMock, times(2)).join<Any, Any>("extraFields")
        verifyNoMoreInteractions(personRootMock)

        verify(extraFieldsJoinMock, times(1)).get<String>("field")
        verify(extraFieldsJoinMock, times(1)).get<String>("value")
        verifyNoMoreInteractions(extraFieldsJoinMock)

        verify(valueMock, times(2)).javaType
        verifyNoMoreInteractions(valueMock)

        verify(criteriaBuilderMock, times(1)).equal(fieldsMock, "email")
        verify(criteriaBuilderMock, times(1)).like(valueMock, "%Alice%")
        verify(criteriaBuilderMock, times(1)).and(equalFieldPredicate, likeValuePredicate)
        verify(criteriaBuilderMock, times(1)).and(combinationPredicate)
        verifyNoMoreInteractions(criteriaBuilderMock)

        verifyNoInteractions(criteriaQueryMock, equalFieldPredicate)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun customFieldSpecificationTest_withSearchQuery() {
        val fieldsMock = mock(Path::class.java) as Path<String>
        val valueMock = mock(Path::class.java) as Path<String>
        val extraFieldsJoinMock = mock(Join::class.java) as Join<Any, Any>

        val equalFieldPredicate = mock(Predicate::class.java)
        val likeValuePredicate = mock(Predicate::class.java)
        val combinationPredicate = mock(Predicate::class.java)

        `when`(personRootMock.join<Any, Any>("extraFields"))
            .thenReturn(extraFieldsJoinMock)
        `when`(valueMock.javaType)
            .thenReturn(String::class.java)
        `when`<Path<String>>(extraFieldsJoinMock["field"])
            .thenReturn(fieldsMock)
        `when`<Path<String>>(extraFieldsJoinMock["value"])
            .thenReturn(valueMock)
        `when`(criteriaBuilderMock.equal(fieldsMock, "email"))
            .thenReturn(equalFieldPredicate)
        `when`(criteriaBuilderMock.like(valueMock, "%Alice%"))
            .thenReturn(likeValuePredicate)
        `when`(criteriaBuilderMock.and(equalFieldPredicate, likeValuePredicate))
            .thenReturn(combinationPredicate)
        `when`(criteriaBuilderMock.and(combinationPredicate))
            .thenReturn(combinationPredicate)

        val searchQuery = SearchQuery<PersonDto>(0, 10, searchCriteria("email~Alice"), sortOrder())
        val personSpecification = specification<Person>(searchQuery)
        val actualPredicate = personSpecification.toPredicate(personRootMock, criteriaQueryMock, criteriaBuilderMock)

        assertThat(combinationPredicate).isEqualTo(actualPredicate)

        verify(personRootMock, times(2)).join<Any, Any>("extraFields")
        verifyNoMoreInteractions(personRootMock)

        verify(extraFieldsJoinMock, times(1)).get<String>("field")
        verify(extraFieldsJoinMock, times(1)).get<String>("value")
        verifyNoMoreInteractions(extraFieldsJoinMock)

        verify(valueMock, times(2)).javaType
        verifyNoMoreInteractions(valueMock)

        verify(criteriaBuilderMock, times(1)).equal(fieldsMock, "email")
        verify(criteriaBuilderMock, times(1)).like(valueMock, "%Alice%")
        verify(criteriaBuilderMock, times(1)).and(equalFieldPredicate, likeValuePredicate)
        verify(criteriaBuilderMock, times(1)).and(combinationPredicate)
        verifyNoMoreInteractions(criteriaBuilderMock)

        verifyNoInteractions(criteriaQueryMock, equalFieldPredicate)
    }

    private fun searchCriteria(vararg keys: String): List<SearchCriterium> {
        val list = mutableListOf<SearchCriterium>()
        for (key in keys) {
            if (key.contains("~")) {
                list.add(SearchCriterium(key.split("~").toTypedArray()[0] to key.split("~").toTypedArray()[1]))
            } else if (key.contains("=")) {
                list.add(SearchCriterium(key.split("=").toTypedArray()[0] to key.split("=").toTypedArray()[1]))
            } else {
                list.add(SearchCriterium(key to key))
            }
        }
        return list
    }

    private fun sortOrder(): List<SearchDirection> {
        return emptyList()
    }

    private fun sortOrderAsc(vararg keys: String): List<SearchDirection> {
        return keys.asSequence().map { SearchDirection(it, Direction.ASC) }.toList()
    }

    private fun sortOrderDesc(vararg keys: String): List<SearchDirection> {
        return keys.asSequence().map { SearchDirection(it, Direction.DESC) }.toList()
    }

    private fun sortOrderNull(vararg keys: String): List<SearchDirection> {
        return keys.asSequence().map { SearchDirection(it, null) }.toList()
    }
}

internal data class PersonDto(
    val id: UUID? = null,
    val firstname: String? = null,
    val lastname: String? = null,
    val age: String? = null,
    @SearchQueryHint("address.~")
    val street: String? = null,
    @SearchQueryHint("address.~")
    val number: Int = 0,
    @SearchQueryHint("address.~")
    val residence: String? = null,

    @SearchQueryHint("extraFields[*]")
    val fields: MutableMap<String, String> = mutableMapOf(),
)

@Entity
internal data class Person(
    @Id
    var id: UUID? = null,
    var firstname: String? = null,
    var lastname: String? = null,
    var age: String? = null,

    @OneToOne
    var address: Address? = null,

    @ElementCollection
    var extraFields: Set<Fields>? = null,
) : java.io.Serializable

@Embeddable
internal data class Fields(
    var field: String? = null,
    var value: String? = null,
)

@Entity
internal data class Address(
    @Id
    var id: UUID? = null,
    var street: String? = null,
    var number: Int = 0,
    var residence: String? = null,
)