package com.qualidify.core.util.mapper

import com.qualidify.core.util.mapper.DataObjectMapper.Companion.toList
import com.qualidify.core.util.mapper.DataObjectMapper.Companion.toSet
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class DataObjectMapperTest {

    data class Frog(val name: String, val age: Int)
    data class Prince(val name: String, val age: Int)
    data class Dwarf(val name: String, val age: Int)

    data class Pool(val name: String, val members: Set<Frog>)
    data class Song(val name: String, val members: Set<Frog>)
    data class FairyTale(val name: String, val members: Set<Dwarf>)

    data class Stork(val name: String, val age: Int, val stomachContent: Set<*>)

    data class Story(val name: String)
    data class StoryTeller(val name: String, val stories: Set<Story>)
    data class Book(val name: String, val author: String, val stories: List<Story>)

    @Test
    fun `simple fairytale`() {
        val kissMoment = DataObjectMapper<Frog, Prince>()

        assertThat(kissMoment(Frog("Kermit", 18)))
            .isEqualTo(Prince("Kermit", 18))
    }

    @Test
    fun `simple song`() {
        val makeSong = DataObjectMapper<Pool, Song>()

        val frogs = getFrogs()

        val pool = Pool("Er zaten 7 kikkertjes", frogs)

        assertThat(makeSong(pool))
            .isEqualTo(Song("Er zaten 7 kikkertjes", frogs))
    }

    @Test
    fun `real magic`() {
        val frogs = getFrogs()
        val pool = Pool("Er zaten 7 kikkertjes", frogs)

        val makeFairyTale = DataObjectMapper<Pool, FairyTale>()
            .register(Pool::name, { "Snow White" })
            .register(Pool::members, toSet(DataObjectMapper<Frog, Dwarf>()))

        assertThat(makeFairyTale(pool))
            .isEqualTo(FairyTale("Snow White", getDwarfs()))

    }

    @Test
    fun `a happy end`() {
        val frogs = getFrogs()
        val pool = Pool("Er zaten 7 kikkertjes", frogs)

        val happyEnding = DataObjectMapper<Pool, Stork>()
            .register(Pool::name, { "Happy BirdDay" })
            .translate(Pool::members, Stork::stomachContent)
            .supply(Stork::age) { 3 }

        assertThat(happyEnding(pool))
            .isEqualTo(Stork("Happy BirdDay", 3, frogs))

    }

    @Test
    fun `a heavy menu`() {
        val frogs = getFrogs()
        val pool = Pool("Er zaten 7 kikkertjes", frogs)

        val aBigBite = DataObjectMapper<Pool, Stork>()
            .register(Pool::name, { "Pino" })
            .translateAndMap(Pool::members, Stork::stomachContent, toSet(DataObjectMapper<Frog, Dwarf>()))
            .supply(Stork::age) { 5 }

        assertThat(aBigBite(pool))
            .isEqualTo(Stork("Pino", 5, getDwarfs()))
    }

    @Test
    fun `once upon a time to remember`() {
        val stories = setOf(Story("The Little Mermaid"), Story("The Little Match Girl"), Story("The Nightingale"))
        val storyTeller = StoryTeller("Hans Christian Anderson", stories)

        val writeDown = DataObjectMapper<StoryTeller, Book>()
            .translate(StoryTeller::name, Book::author)
            .supply(Book::name) { "FairyTales of Hans Christian Anderson" }
            .register(StoryTeller::stories, toList(DataObjectMapper<Story, Story>()))

        val book = writeDown(storyTeller)
        assertThat(book.name).isEqualTo("FairyTales of Hans Christian Anderson")
        assertThat(book.author).isEqualTo("Hans Christian Anderson")
        assertThat(book.stories).isInstanceOf(List::class.java).containsAnyElementsOf(stories)

    }

    private fun getDwarfs() = setOf(
        Dwarf("Doc", 49),
        Dwarf("Grumpy", 42),
        Dwarf("Happy", 35),
        Dwarf("Sleepy", 28),
        Dwarf("Bashful", 21),
        Dwarf("Sneezy", 14),
        Dwarf("Dopey", 7)
    )

    private fun getFrogs() = getDwarfs().asSequence().map { Frog(it.name, it.age) }.toSet()


}