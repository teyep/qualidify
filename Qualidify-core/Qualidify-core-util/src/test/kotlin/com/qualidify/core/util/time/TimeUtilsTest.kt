package com.qualidify.core.util.time

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


/**
 * A TimeUtils test
 *
 * @author Marcel Pot
 */
internal class TimeUtilsTest {
    val DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter
        .ofPattern("yyyy-MM-dd'T'HH:mm:ss z")

    @Test
    fun fromLocalDateTimeTest() {
        //before DaylightSaving Test
        val date_1 = ZonedDateTime.parse("2021-03-17T08:17:00 CEST", DATE_TIME_FORMATTER)
        val stPatricksDay = LocalDateTime.of(2021, 3, 17, 8, 17, 0)
        assertThat(TimeUtils.fromLocalDateTime(stPatricksDay))
            .`as`("St. Patricksday as an instant")
            .isInstanceOf(Instant::class.java)
            .isEqualTo(date_1.toInstant())

        //after DaylightSaving Test
        val date_2 = ZonedDateTime.parse("2021-08-15T12:34:56 Europe/Amsterdam", DATE_TIME_FORMATTER)
        val assumptionOfMary = LocalDateTime.of(2021, 8, 15, 12, 34, 56)
        assertThat(TimeUtils.fromLocalDateTime(assumptionOfMary))
            .`as`("Assumption of Mary as an instant")
            .isInstanceOf(Instant::class.java)
            .isEqualTo(date_2.toInstant())
    }

    @Test
    fun fromInstantTest() {
        val stPannenkoek = ZonedDateTime.parse("2021-11-29T18:39:00 CEST", DATE_TIME_FORMATTER).toInstant()
        assertThat(TimeUtils.fromInstant(stPannenkoek))
            .`as`("St Pannenkoek as localdatetime")
            .isInstanceOf(LocalDateTime::class.java)
            .isEqualTo(LocalDateTime.of(2021, 11, 29, 18, 39, 0))

    }
}