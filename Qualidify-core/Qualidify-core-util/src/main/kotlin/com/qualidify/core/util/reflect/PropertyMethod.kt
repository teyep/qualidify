package com.qualidify.core.util.reflect

import org.apache.commons.lang3.StringUtils
import org.springframework.util.ReflectionUtils

object PropertyMethod {

    @JvmStatic
    fun <D> getField(beanType: Class<out D>, bean: D, field: String): Any? {
        val method = ReflectionUtils.findMethod(beanType, "get" + StringUtils.capitalize(field))
        return method?.invoke(bean)
    }


}