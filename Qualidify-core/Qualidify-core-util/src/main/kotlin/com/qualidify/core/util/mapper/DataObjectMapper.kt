package com.qualidify.core.util.mapper

import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor

typealias Mapper<I, O> = (I) -> O
typealias ParameterSupplier<O> = () -> O

/**
 * Mapper that can convert one data obejct into another data object.
 *
 * @param I inType (convert from)
 * @param O outType (convert to)
 *
 * @author Marcel Pot
 */
class DataObjectMapper<I : Any, O : Any>(
    private val convertFrom: KClass<I>,
    private val convertTo: KClass<O>
) : Mapper<I, O> {

    companion object {
        /**
         * Factory constructor
         */
        inline operator fun <reified I : Any, reified O : Any> invoke() = DataObjectMapper(I::class, O::class)

        /**
         * A specific collection mapper, it will map the content of a collection with the given mapper
         * to a new Set
         * @param mapper the mapper to use on the content
         * @param I the inType
         * @param O the outType
         */
        fun <I : Any, O : Any> toSet(mapper: Mapper<I, O>) = object : Mapper<Collection<I>, Set<O>> {
            override fun invoke(data: Collection<I>): Set<O> = data.map(mapper).toSet()
        }

        /**
         * A specific collection mapper, it will map the content of a collection with the given mapper
         * to a new List
         * @param mapper the mapper to use on the content
         * @param I the inType
         * @param O the outType
         */
        fun <I : Any, O : Any> toList(mapper: Mapper<I, O>) = object : Mapper<Collection<I>, List<O>> {
            override fun invoke(data: Collection<I>): List<O> = data.map(mapper).toList()
        }
    }

    val fieldMappers = mutableMapOf<String, Mapper<Any, Any>>()
    val parameterSuppliers = mutableMapOf<String, ParameterSupplier<Any>>()
    val translations = mutableMapOf<String, String>()

    private val toConstructor = convertTo.primaryConstructor!!

    private val fromPropertiesByName by lazy {
        convertFrom.memberProperties.associateBy { it.name }
    }

    /**
     * Direct invocation of the mapper.
     * @param input the input data object
     */
    override fun invoke(input: I): O = with(toConstructor) {
        callBy(parameters.associateWith { getValueFor(it, input) })
    }

    private fun getValueFor(parameter: KParameter, data: I): Any? {
        if (parameterSuppliers.containsKey(parameter.name)) {
            return parameterSuppliers[parameter.name]?.invoke()
        }

        val fromProperty = translations.get(parameter.name) ?: parameter.name
        val value = fromPropertiesByName[fromProperty]?.get(data) ?: return null
        return fieldMappers[parameter.name]?.invoke(value) ?: value
    }

    /**
     * An option to add extra mappers for child mapping.
     *
     * @param property the property where the mapping is for
     * @mapper mapper the Mapper to use
     * @param S The input Type of the mapper
     * @param T the output Type of the mapper
     */
    inline fun <reified S : Any, reified T : Any> register(
        property: KProperty1<I, S>,
        crossinline mapper: Mapper<S, T>
    )
            : DataObjectMapper<I, O> = apply {
        this.fieldMappers[property.name] = object : Mapper<Any, Any> {
            override fun invoke(data: Any): Any = mapper.invoke(data as S)
        }
    }

    /**
     * An option to add a parameter supplier for a specific field that is not available in the InType Data Object
     * @param  property the property the mapping is for
     * @param mapper the Mapper to use
     * @param T the Type of the result of the Supplier
     */
    fun <T : Any> supply(property: KProperty1<O, Any?>, mapper: ParameterSupplier<T>)
            : DataObjectMapper<I, O> = apply {
        this.parameterSuppliers[property.name] = object : ParameterSupplier<Any> {
            override fun invoke(): Any = mapper.invoke()
        }
    }

    /**
     * An option to add a parameter name switching for a specific field
     * @param  fromProperty the Parameter to map from
     * @param toProperty the Parameter to map to
     */
    fun translate(fromProperty: KProperty1<I, Any?>, toProperty: KProperty1<O, Any?>)
            : DataObjectMapper<I, O> = apply {
        this.translations[toProperty.name] = fromProperty.name

    }

    /**
     * An option to add extra mappers for child mapping
     * @param property the property where the mapping is for
     * @mapper mapper the Mapper to use
     * @param S the input type if the  mapper
     * @param T the output type of the mapper
     */
    inline fun <reified S : Any, reified T : Any> translateAndMap(
        fromProperty: KProperty1<I, Any?>,
        toProperty: KProperty1<O, Any?>,
        crossinline mapper: Mapper<S, T>
    )
            : DataObjectMapper<I, O> = apply {
        this.translations[toProperty.name] = fromProperty.name
        this.fieldMappers[toProperty.name] = object : Mapper<Any, Any> {
            override fun invoke(data: Any): Any = mapper.invoke(data as S)
        }
    }


}