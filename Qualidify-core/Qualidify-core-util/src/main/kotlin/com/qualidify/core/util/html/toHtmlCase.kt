package com.qualidify.core.util.html

import java.util.*

/**
 * Extension function to the String class that provides a shortcut for transforming any text to a format
 * more common when using html (lowercase, dashed).
 *
 * @author Menno Tol
 * @since 1.0
 */
fun String.toHtmlCase(): String = this.lowercase(Locale.ROOT).replace(" ", "-")