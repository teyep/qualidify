package com.qualidify.core.util.time

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

/**
 * A Helper class for time related conversions
 *
 * @author Marcel Pot
 */
object TimeUtils {


    private val DEFAULT_TIMEZONE = "Europe/Amsterdam"
    val SYSTEM_ZONE = ZoneId.of(DEFAULT_TIMEZONE)

    /**
     * Conversion from LocalDateTime to Instant
     * @param time the LocalDateTime
     * @return the Instant
     */
    fun fromLocalDateTime(time: LocalDateTime): Instant {
        return time.atZone(ZoneId.of(DEFAULT_TIMEZONE)).toInstant();
    }

    /**
     * Conversion from Instant to LocalDateTime
     * @param time the Instant
     * @return the LocalDateTime
     */
    fun fromInstant(time: Instant): LocalDateTime {
        return LocalDateTime.ofInstant(time, SYSTEM_ZONE)
    }
}