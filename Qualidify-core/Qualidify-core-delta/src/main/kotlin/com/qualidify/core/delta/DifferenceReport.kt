package com.qualidify.core.delta

import java.time.Instant

/**
 * A Report containing every change in Objects
 *
 * @author Marcel Pot
 */
data class DifferenceReport(
    val changes: List<ChangeMoment>? = emptyList()
)

/**
 * A Data class containing one step of difference
 *
 * @author Marcel Pot
 */
data class ChangeMoment(
    val moment: Instant,
    val author: String,
    val difference: Set<Delta>? = emptySet()
)