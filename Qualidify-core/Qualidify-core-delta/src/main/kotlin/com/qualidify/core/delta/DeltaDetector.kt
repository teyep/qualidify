package com.qualidify.core.delta


import com.qualidify.core.log.LoggerDelegation
import org.springframework.stereotype.Service
import java.util.*


data class Delta(
    val source: Source,
    val leftValue: Any?,
    val rightValue: Any?,
    val change: ChangeCommand?,
    val optionalAddition: Any? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Delta) return false

        if (!source.equals(other.source)) return false

        return true
    }

    override fun hashCode(): Int {
        return source.hashCode()
    }

    override fun toString(): String {
        return "Delta(source=$source, leftValue=$leftValue?, rightValue=$rightValue?, change=$change, optionalAddition=$optionalAddition)"
    }


}

data class Source(
    val clazz: String,
    val fieldName: String,
    val pointer: String,
    val index: Any? = null

) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Source) return false

        if (pointer != other.pointer) return false

        return true
    }

    override fun hashCode(): Int {
        return pointer.hashCode()
    }
}

enum class ChangeCommand(val s: String) {
    ARRAY_SET_ELEMENT("array.setElement"),
    ARRAY_RESIZE("array.resize"),
    OBJECT_CREATED("object.created"),
    OBJECT_DELETED("object.deleted"),
    OBJECT_ASSIGN_FIELD("object.assignField"),
    OBJECT_FIELD_TYPE_CHANGED("object.fieldTypeChanged"),
    SET_ADD("set.add"),
    SET_REMOVE("set.remove"),
    MAP_PUT("map.put"),
    MAP_REMOVE("map.remove"),
    LIST_RESIZE("list.resize"),
    LIST_SET_ELEMENT("list.setElement")
}

@Service
class DeltaDetector {

    companion object {
        val ROOT = "-root-"
        val ROOTSOURCE = Source(ROOT, ROOT, "")
    }

    private val log by LoggerDelegation()

    private val comparators: List<DeltaComparator> = listOf(
        SameInstanceDeltaComparator(),
        NullableDeltaComparator(),
        DifferentClassDeltaComparator(),
        PrimitiveDeltaComparator(),
        ArrayDeltaComparator(),
        SetDeltaComparator(),
        MapDeltaComparator(),
        ListDeltaCompatator(),
        UnknownCollectionDeltaComparator(),
        PojoDeltaComparator()
    )

    fun compare(left: Any?, right: Any?): Set<Delta> {
        val deltas: MutableSet<Delta> = mutableSetOf()
        val visited: MutableSet<String> = mutableSetOf()

        val stack: Stack<Delta> = Stack()
        stack.push(Delta(ROOTSOURCE, left, right, null))

        while (!stack.isEmpty()) {

            val delta = stack.pop()
            var path = delta.source.pointer

            if (!stack.isEmpty()) {
                path += "." + System.identityHashCode(stack.peek().leftValue)
            }

            log.trace("Delta path is $path.")

            if (visited.contains(path)) {
                //Check for cyclic graphs where the source.pointer is taken into account
                // see [Delta.equals], which means that an instance alone is not enough
                // to skip, the pointer to it must also be identical (before skipping it).
                continue
            }

            visited.add(path)

            for (comparator in comparators) {
                if (comparator.compare(delta, deltas, stack)) {
                    break
                }
            }

        }
        return deltas
    }

}