package com.qualidify.core.delta

import org.springframework.beans.BeanUtils
import java.util.*

/**
 * An Interface for comparing objects in the [DeltaDetector] while collection the delta's
 * and pushing next comparison jobs to the stack.
 *
 * @author Marcel Pot
 */
interface DeltaComparator {

    /**
     * Start the comparing.
     * @param delta contains the different objects for comparing
     * @param deltas is a set of differences found
     * @param stack is a stack with future comparison jobs
     * @return indicator if this comparator has finished comparing
     */
    fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean


}


/**
 * A [DeltaComparator] to check for the same object = same instance (left and right)
 * in [Delta]. Warning: It does not use the the [Any.equals] method!
 *
 * @author Marcel Pot
 */
class SameInstanceDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        return delta.leftValue === delta.rightValue
    }
}

/**
 * A [DeltaComparator] to compare two objects and check if one (and only one!) is null
 *
 * @author Marcel Pot
 */
class NullableDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        if ((delta.leftValue == null) xor (delta.rightValue == null)) {
            if (delta.source.clazz == DeltaDetector.ROOT) {
                // object Creation or Deletion
                if ((delta.leftValue == null)) {
                    deltas.add(
                        delta.copy(
                            change = ChangeCommand.OBJECT_CREATED
                        )
                    )
                } else {
                    deltas.add(
                        delta.copy(
                            change = ChangeCommand.OBJECT_DELETED
                        )
                    )
                }
            } else {
                //assignment
                deltas.add(
                    delta.copy(
                        change = ChangeCommand.OBJECT_ASSIGN_FIELD
                    )
                )
            }
            return true
        }
        return false
    }

}

/**
 * A [DeltaComparator] to compare two objects and checks if they use the same class.
 * There is an exception on the rule above while there is a different inheritence path
 * for collections.
 *
 * @author Marcel Pot
 */
class DifferentClassDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        if (!delta.leftValue!!::class.java.equals(delta.rightValue!!::class.java) &&
            (isNotCollection(delta.leftValue, delta.rightValue))
        ) {
            deltas.add(
                delta.copy(
                    change = ChangeCommand.OBJECT_FIELD_TYPE_CHANGED
                )
            )
            return true
        }

        return false
    }

    private fun isNotCollection(leftValue: Any?, rightValue: Any?) =
        !((leftValue is Map<*, *> && rightValue is Map<*, *>) ||
                (leftValue is Set<*> && rightValue is Set<*>) ||
                (leftValue is List<*> && rightValue is List<*>))

}

/**
 * A [DeltaComparator] to compare two primitive objects. See [BeanUtils.isSimpleValueType] for the
 * is primitive definitions
 *
 * @author Marcel Pot
 */
class PrimitiveDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        if (BeanUtils.isSimpleValueType(delta.leftValue!!::class.java)) {
            if (!delta.leftValue.equals(delta.rightValue)) {
                deltas.add(
                    delta.copy(change = ChangeCommand.OBJECT_ASSIGN_FIELD)
                )

            }
            return true
        }
        return false
    }


}

/**
 * A [DeltaComparator] to compare the content of two Arrays. It will check for length and content
 * differences
 *
 * @author Marcel Pot
 */
class ArrayDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        if (delta.leftValue!!::class.java.isArray()) {
            val leftArray = delta.leftValue as Array<*>
            val rightArray = delta.rightValue!! as Array<*>

            val leftLength: Int = leftArray.size
            val rightLength: Int = rightArray.size

            if (leftLength != rightLength) {
                deltas.add(
                    delta.copy(
                        change = ChangeCommand.ARRAY_RESIZE,
                        optionalAddition = rightLength
                    )
                )
            }

            val arrayType = delta.rightValue::class.java.getComponentType()

            if (BeanUtils.isSimpleValueType(arrayType)) {
                comparePrimitiveArrays(leftArray, rightArray, delta, deltas)
            } else {
                compareComplexArrays(leftArray, rightArray, delta, deltas, stack)
            }
            return true
        }
        return false
    }

    private fun comparePrimitiveArrays(
        leftArray: Array<*>,
        rightArray: Array<*>,
        delta: Delta,
        deltas: MutableSet<Delta>,
    ) {
        val leftLength: Int = leftArray.size
        val systemId = "(" + System.identityHashCode(delta.leftValue) + ")"

        for ((index, rightValue) in rightArray.withIndex()) {
            val sourcePointer = delta.source.copy(
                pointer = systemId + '[' + index + ']',
                index = index
            )

            if (index < leftLength) {
                val leftValue: Any? = leftArray[index]

                if (leftValue == null && rightValue != null || leftValue != null && rightValue == null ||
                    rightValue != leftValue
                ) {
                    pushNewDelta(delta, deltas, sourcePointer, leftValue, rightValue, index)
                }
            } else {
                pushNewDelta(delta, deltas, sourcePointer, null, rightValue, index)
            }
        }
    }

    private fun compareComplexArrays(
        leftArray: Array<*>,
        rightArray: Array<*>,
        delta: Delta,
        deltas: MutableSet<Delta>,
        stack: Stack<Delta>
    ) {
        val leftLength: Int = leftArray.size
        val systemId = "(" + System.identityHashCode(delta.leftValue) + ")"

        for ((index, rightValue) in rightArray.withIndex()) {
            val sourcePointer = delta.source.copy(
                pointer = systemId + '[' + index + ']',
                index = index
            )

            if (index < leftLength) {
                val leftValue = leftArray[index]

                if ((leftValue == null) xor (rightValue == null)) {
                    pushNewDelta(delta, deltas, sourcePointer, leftValue, rightValue, index)
                } else if (leftValue != null && rightValue != null) {
                    stack.push(
                        delta.copy(
                            source = sourcePointer,
                            leftValue = leftValue,
                            rightValue = rightValue,
                            optionalAddition = index
                        )
                    )
                }
            } else {
                pushNewDelta(delta, deltas, sourcePointer, null, rightValue, index)
            }
        }
    }


    private fun pushNewDelta(
        delta: Delta,
        deltas: MutableCollection<Delta>,
        sourcePointer: Source,
        leftValue: Any?,
        rightValue: Any?,
        index: Int
    ) {
        val newDelta = delta.copy(
            change = ChangeCommand.ARRAY_SET_ELEMENT,
            source = sourcePointer,
            leftValue = leftValue,
            rightValue = rightValue,
            optionalAddition = index
        )
        deltas.add(newDelta)
    }
}

/**
 * A [DeltaComparator] for comparing Sets. It will check if the content of the left Set also is contained in
 * the Right Set
 *
 * @author Marcel Pot
 */
class SetDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        if (delta.leftValue is Set<*>) {

            val leftSet = delta.leftValue
            val rightSet = delta.rightValue as Set<*>

            var systemId = "(" + System.identityHashCode(leftSet) + ").remove("

            for (leftValue in leftSet) {
                val sourcePointer = delta.source.copy(
                    pointer = systemId + System.identityHashCode(leftValue) + ')'
                )

                if (!rightSet.contains(leftValue)) {
                    val removeDelta = delta.copy(
                        source = sourcePointer,
                        leftValue = leftValue,
                        rightValue = null,
                        change = ChangeCommand.SET_REMOVE,
                        optionalAddition = null

                    )
                    deltas.add(removeDelta)
                }
            }
            systemId = "(" + System.identityHashCode(rightSet) + ").add("
            for (rightValue in rightSet) {
                val sourcePointer = delta.source.copy(
                    pointer = systemId + System.identityHashCode(rightValue) + ')'
                )

                if (!leftSet.contains(rightValue)) {
                    val addDelta = delta.copy(
                        source = sourcePointer,
                        leftValue = null,
                        rightValue = rightValue,
                        change = ChangeCommand.SET_ADD,
                        optionalAddition = null
                    )
                    deltas.add(addDelta)
                }
            }
            return true
        }
        return false
    }
}

/**
 * A [DeltaComparator] for Maps. It checks if for all keys if the key exist in the other map, and
 * if the value in both maps are equal
 *
 * @author Marcel Pot
 */
class MapDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        if (delta.leftValue is Map<*, *>) {
            val leftMap = delta.leftValue
            val rightMap = delta.rightValue as Map<*, *>

            compareLeftToRights(leftMap, rightMap, delta, deltas, stack)
            compareRightToLeft(leftMap, rightMap, delta, deltas)

            return true
        }
        return false
    }

    private fun compareLeftToRights(
        leftMap: Map<*, *>,
        rightMap: Map<*, *>,
        delta: Delta,
        deltas: MutableSet<Delta>,
        stack: Stack<Delta>
    ) {
        val systemId = "(" + System.identityHashCode(leftMap) + ')'
        for ((key, leftValue) in leftMap.entries) {
            val sourcePointer = delta.source.copy(
                pointer = systemId + "['" + System.identityHashCode(key) + "']",
                index = key
            )

            if (rightMap.containsKey(key)) {
                val rightValue = rightMap[key]

                if ((leftValue == null) xor (rightValue == null)) {
                    pushMapPutDelta(delta, deltas, sourcePointer, rightValue, key)
                } else if (leftValue != null && rightValue != null) {
                    stack.push(
                        delta.copy(
                            source = sourcePointer,
                            leftValue = leftValue,
                            rightValue = rightValue,
                            optionalAddition = key
                        )
                    )
                }
            } else {
                pushMapRemoveDelta(delta, deltas, sourcePointer, leftValue, key)
            }

        }
    }

    private fun compareRightToLeft(
        leftMap: Map<*, *>,
        rightMap: Map<*, *>,
        delta: Delta,
        deltas: MutableSet<Delta>
    ) {
        val systemId = "(" + System.identityHashCode(leftMap) + ')'
        for ((key, rightValue) in rightMap.entries) {
            val sourcePointer = delta.source.copy(
                pointer = systemId + "['" + System.identityHashCode(key) + "']",
                index = key
            )

            if (!leftMap.containsKey(key)) {
                pushMapPutDelta(delta, deltas, sourcePointer, rightValue, key)
            }
        }
    }

    private fun pushMapPutDelta(
        delta: Delta,
        deltas: MutableCollection<Delta>,
        source: Source,
        rightValue: Any?,
        key: Any?
    ) {
        val putDelta = delta.copy(
            source = source,
            leftValue = null,
            rightValue = rightValue,
            optionalAddition = key,
            change = ChangeCommand.MAP_PUT

        )
        deltas.add(putDelta)
    }

    private fun pushMapRemoveDelta(
        delta: Delta,
        deltas: MutableCollection<Delta>,
        source: Source,
        leftValue: Any?,
        key: Any?
    ) {
        val removeDelta = delta.copy(
            source = source,
            leftValue = leftValue,
            rightValue = null,
            optionalAddition = key,
            change = ChangeCommand.MAP_REMOVE
        )
        deltas.add(removeDelta)
    }

}

/**
 * A [DeltaComparator] for comparing Lists. It pushed the objects on the same index position
 * to the stack for further comparison, and in will create delta's if the lists are unequal of length
 *
 * @author Marcel Pot
 */

class ListDeltaCompatator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        if (delta.leftValue is List<*>) {
            val leftList = delta.leftValue
            val rightList = delta.rightValue as List<*>

            if (leftList.size != rightList.size) {
                val resizeDelta = delta.copy(
                    change = ChangeCommand.LIST_RESIZE,
                    optionalAddition = rightList.size
                )
                deltas.add(resizeDelta)
            }

            compareInnerList(leftList, rightList, delta, deltas, stack)
            return true
        }
        return false
    }

    private fun compareInnerList(
        leftList: List<*>,
        rightList: List<*>,
        delta: Delta,
        deltas: MutableSet<Delta>,
        stack: Stack<Delta>
    ) {
        val systemId = "(" + System.identityHashCode(leftList) + ")"

        for ((index, rightValue) in rightList.withIndex()) {
            val sourcePointer = delta.source.copy(
                pointer = systemId + "{" + index + "}",
                index = index
            )
            if (index < leftList.size) {
                val leftValue = leftList[index]

                if ((rightValue == null) xor (leftValue == null)) {
                    pushListSetDelta(delta, deltas, sourcePointer, leftValue, rightValue, index)
                } else if (leftValue != null && rightValue != null) {
                    stack.push(
                        delta.copy(
                            source = sourcePointer,
                            leftValue = leftValue,
                            rightValue = rightValue,
                            optionalAddition = index
                        )
                    )
                }
            } else {
                pushListSetDelta(delta, deltas, sourcePointer, null, rightValue, index)
            }
        }
    }

    private fun pushListSetDelta(
        delta: Delta,
        deltas: MutableCollection<Delta>,
        source: Source,
        leftValue: Any?,
        rightValue: Any?,
        index: Int?
    ) {
        val listSetDelta = delta.copy(
            change = ChangeCommand.LIST_SET_ELEMENT,
            source = source,
            leftValue = leftValue,
            rightValue = rightValue,
            optionalAddition = index
        )
        deltas.add(listSetDelta)
    }
}

/**
 * A [DeltaComparator] to throw an exception if a rare collection is found. If this collection is needed
 * it can be added as a new [DeltaComparator] in [DeltaDetector]
 *
 * @author Marcel Pot
 */
class UnknownCollectionDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {
        if (delta.leftValue is Collection<*>) {
            throw  DeltaDetectionException(
                "A custom Collection detected that does not extend List or Set: " +
                        "${delta.leftValue::class.java.name}. You need to add a new DeltaComparator" +
                        " to support it!  Field: ${delta.source.fieldName}"
            );
        }
        return false
    }
}

/**
 * A [DeltaComparator] to check on Pojo level all difference between fields. It does not detect
 * differences by it self, but every field of the Pojo is pushed to the stack for further investigation
 *
 * @author Marcel Pot
 */
class PojoDeltaComparator() : DeltaComparator {
    override fun compare(delta: Delta, deltas: MutableSet<Delta>, stack: Stack<Delta>): Boolean {

        val fields = delta.leftValue!!::class.java.declaredFields
        val systemId = "(" + System.identityHashCode(delta.leftValue) + ")."

        for (field in fields) {
            try {
                val sourcePointer = delta.source.copy(
                    clazz = delta.leftValue::class.java.name,
                    fieldName = field.name,
                    pointer = systemId + field.name,
                    index = null
                )

                field.trySetAccessible()
                stack.push(
                    delta.copy(
                        source = sourcePointer,
                        leftValue = field[delta.leftValue],
                        rightValue = field[delta.rightValue],
                        optionalAddition = null
                    )
                )
            } catch (exception: Exception) {
                throw DeltaDetectionException(
                    "A Pojo comparison for ${delta.leftValue::class.java.name} failed on the field " +
                            "${field.name}", exception
                )
            }
        }
        return true
    }

}


class DeltaDetectionException : RuntimeException {
    constructor(message: String?, cause: Throwable) : super(message, cause)
    constructor(message: String) : super(message)
}