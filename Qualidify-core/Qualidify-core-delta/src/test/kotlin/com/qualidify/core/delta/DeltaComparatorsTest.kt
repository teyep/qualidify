package com.qualidify.core.delta

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.ArrayBlockingQueue

@ExtendWith(MockitoExtension::class)
class DifferenceComparatorsTest {

    val ROOT = Source("-root-", "-root-", "")

    @Spy
    val deltas: MutableSet<Delta>? = null

    @Spy
    val stack: Stack<Delta>? = null


    @Test
    fun testSameInstanceDeltaComparator_withEqualIntegers() {
        val left = 1
        val right = 1

        val isFinished = SameInstanceDeltaComparator().compare(
            Delta(ROOT, left, right, null),
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Equal Integers are the same instance").isTrue
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSameInstanceDeltaComparator_withUnEqualIntegers() {
        val left = 1
        val right = -3

        val isFinished = SameInstanceDeltaComparator().compare(
            Delta(ROOT, left, right, null),
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Unequal Integers are not the same instance").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSameInstanceDeltaComparator_withEqualStrings() {
        val left = "How a cow catches a hare"
        val right = "How a cow catches a hare"

        val isFinished = SameInstanceDeltaComparator().compare(
            Delta(ROOT, left, right, null),
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Equal Strings are the same instance").isTrue
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSameInstanceDeltaComparator_withUnequalStrings() {
        val left = "How a cow catches a hare"
        val right = "Tall trees catch a lot of wind"

        val isFinished = SameInstanceDeltaComparator().compare(
            Delta(ROOT, left, right, null),
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Unequal Strings are not the same instance").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSameInstanceDeltaComparator_withEqualEqualsMethod() {
        val left = Alpha("A", 1)
        val right = Alpha("A", 1)

        val isFinished = SameInstanceDeltaComparator().compare(
            Delta(ROOT, left, right, null),
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Equal Alpha's are not the same instance").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSameInstanceDeltaComparator_withNulls() {
        val left = null
        val right = null

        val isFinished = SameInstanceDeltaComparator().compare(
            Delta(ROOT, left, right, null),
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Equal Nulls are  the same instance").isTrue
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testNullableDeltaComparator_withIntegers() {
        val left = 1
        val right = 1

        val isFinished = NullableDeltaComparator().compare(
            Delta(ROOT, left, right, null),
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Equal integers are not null").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testNullableDeltaComparator_withUnequalIntegers() {
        val left = 1
        val right = -16

        val isFinished = NullableDeltaComparator().compare(
            Delta(ROOT, left, right, null),
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Unequal integers are not null").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testNullableDeltaComparator_withIntegerAndNull() {
        val left = 1
        val right = null

        val delta = Delta(ROOT, left, right, null)

        val isFinished = NullableDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("One integer and Null are not equal").isTrue
        verify(deltas, times(1)).add(delta.copy(change = ChangeCommand.OBJECT_CREATED))
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testNullableDeltaComparator_withNullAndInteger() {
        val left = 1
        val right = null

        val delta = Delta(ROOT, left, right, null)

        val isFinished = NullableDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Null and One integer are not equal").isTrue
        verify(deltas, times(1)).add(delta.copy(change = ChangeCommand.OBJECT_DELETED))
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    /**
     * Special case, normaly the double null check is catched in [SameInstanceDeltaComparator]
     * But this test will also check if a Null==Null check doesnot raise a delta when not
     * expected
     */
    @Test
    fun testNullableDeltaComparator_withTwoNulls() {
        val left = null
        val right = null

        val delta = Delta(ROOT, left, right, null)

        val isFinished = NullableDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Null and Null are  equal").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testDifferentClassDeltaComparator_withEqualClasses() {
        val left = LocalDate.of(2020, 1, 1)
        val right = LocalDate.of(2021, 12, 5)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two local dates have the same class").isFalse

        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testDifferentClassDeltaComparator_withUnEqualClasses() {
        val left = LocalDate.of(2020, 1, 1)
        val right = LocalDateTime.of(2021, 12, 5, 18, 0, 0)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("A LocalDate is not the same class as LocalDateTime").isTrue

        verify(deltas, times(1)).add(delta.copy(change = ChangeCommand.OBJECT_FIELD_TYPE_CHANGED))
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testDifferentClassDeltaComparator_withDifferentMaps() {
        val left = HashMap<Int, Planet>()
        val right = TreeMap<Int, Planet>()

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two different map implementations are the same class").isFalse

        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }


    @Test
    fun testDifferentClassDeltaComparator_withDifferentMapsGenerics() {
        val left = HashMap<Int, Planet>()
        val right = HashMap<Double, Moon>()

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two different map implementations are the same class").isFalse

        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testDifferentClassDeltaComparator_withDifferentSets() {
        val left = HashSet<Planet>()
        val right = LinkedHashSet<Planet>()

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two different set implementations are the same class").isFalse

        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testDifferentClassDeltaComparator_withDifferentSetsGenerics() {
        val left = LinkedHashSet<Planet>()
        val right = LinkedHashSet<Moon>()

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two different set implementations are the same class").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testDifferentClassDeltaComparator_withDifferentList() {
        val left = LinkedList<Planet>()
        val right = ArrayList<Planet>()

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two different list implementations are the same class").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testDifferentClassDeltaComparator_withDifferentListGenerics() {
        val left = LinkedList<Planet>()
        val right = LinkedList<Moon>()

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two different list implementations are the same class").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testDifferentClassDeltaComparator_withDifferentCollections() {
        val left = LinkedList<Planet>()
        val right = LinkedHashSet<Planet>()

        val delta = Delta(ROOT, left, right, null)

        val isFinished = DifferentClassDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two different collection implementations are not the same class").isTrue
        verify(deltas, times(1)).add(delta.copy(change = ChangeCommand.OBJECT_FIELD_TYPE_CHANGED))
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testPrimitiveDeltaComparator_withEqualStrings() {
        val left = "How a cow catches a hare"
        val right = "How a cow catches a hare"

        val delta = Delta(ROOT, left, right, null)

        val isFinished = PrimitiveDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal strings are equal, check is finished").isTrue
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testPrimitiveDeltaComparator_withUnEqualStrings() {
        val left = "How a cow catches a hare"
        val right = "Tall trees catch a lot of wind"

        val delta = Delta(ROOT, left, right, null)

        val isFinished = PrimitiveDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal strings are equal, check is finished").isTrue
        verify(deltas, times(1)).add(delta.copy(change = ChangeCommand.OBJECT_ASSIGN_FIELD))
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testPrimitiveDeltaComparator_withEqualBigDecimals() {
        val left = BigDecimal(Math.PI)
        val right = BigDecimal(Math.PI)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = PrimitiveDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal bigdecimals are equal, check is finished").isTrue
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testPrimitiveDeltaComparator_withUnEqualBigDecimals() {
        val left = BigDecimal(Math.PI)
        val right = BigDecimal(Math.E)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = PrimitiveDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal strings are equal, check is finished").isTrue
        verify(deltas, times(1)).add(delta.copy(change = ChangeCommand.OBJECT_ASSIGN_FIELD))
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testPrimitiveDeltaComparator_withNonPrimitives() {
        val left = Alpha("A", 1)
        val right = Alpha("A", 1)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = PrimitiveDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Alphas are not primitive, check is unfinished").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testArrayDeltaComparator_withTwoEqualPrimitiveArrays() {
        val left = arrayOf(1, 2, 3, 4)
        val right = arrayOf(1, 2, 3, 4)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testArrayDeltaComparator_withTwoPrimitiveArraysUnequalContent() {
        val left = arrayOf(1, 2, 3, 4)
        val right = arrayOf(1, 2, 7, 4)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal Arrays are not equal, check is finished").isTrue
        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")[2]",
                    index = 2
                ),
                change = ChangeCommand.ARRAY_SET_ELEMENT,
                leftValue = 3,
                rightValue = 7,
                optionalAddition = 2

            )
        )
        verifyNoMoreInteractions(deltas)

        verifyNoInteractions(stack)
    }

    @Test
    fun testArrayDeltaComparator_withTwoPrimitiveArraysUnequalLength_1() {
        val left = arrayOf(1, 2, 3, 4)
        val right = arrayOf(1, 2, 3)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal Arrays are not equal, check is finished").isTrue
        verify(deltas, times(1)).add(
            delta.copy(
                change = ChangeCommand.ARRAY_RESIZE,
                optionalAddition = 3
            )
        )
        verifyNoMoreInteractions(deltas)

        verifyNoInteractions(stack)
    }

    @Test
    fun testArrayDeltaComparator_withTwoPrimitiveArraysUnequalLength_2() {
        val left = arrayOf(1, 2, 3)
        val right = arrayOf(1, 2, 3, 4)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal Arrays are not equal, check is finished").isTrue
        verify(deltas, times(1)).add(
            delta.copy(
                change = ChangeCommand.ARRAY_RESIZE,
                optionalAddition = 3
            )
        )
        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")[3]",
                    index = 3
                ),
                change = ChangeCommand.ARRAY_SET_ELEMENT,
                leftValue = null,
                rightValue = 4,
                optionalAddition = 3

            )
        )

        verifyNoMoreInteractions(deltas)

        verifyNoInteractions(stack)
    }

    @Test
    fun testArrayDeltaComparator_withTwoPrimitiveArraysUnequalLengthAndUnequalContent() {
        val left = arrayOf(1, 2, 3, 4)
        val right = arrayOf(1, 5, 3)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal Arrays are not equal, check is finished").isTrue
        verify(deltas, times(1)).add(
            delta.copy(
                change = ChangeCommand.ARRAY_RESIZE,
                optionalAddition = 3
            )
        )
        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")[1]",
                    index = 1
                ),
                change = ChangeCommand.ARRAY_SET_ELEMENT,
                leftValue = 2,
                rightValue = 5,
                optionalAddition = 1

            )
        )
        verifyNoMoreInteractions(deltas)

        verifyNoInteractions(stack)
    }

    @Test
    fun testArrayDeltaComparator_withTwoEqualArrays() {
        val left = arrayOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97))
        val right = arrayOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97))

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verifyNoInteractions(deltas)
        verify(stack, times(3)).push(any(Delta::class.java))
    }

    @Test
    fun testArrayDeltaComparator_withTwoUnequalArrays_1() {
        val left = arrayOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97))
        val right = arrayOf(Alpha("A", 65), Alpha("a", 97))

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 2 new compare jobs").hasSize(2)

        verify(deltas, times(1)).add(
            delta.copy(
                change = ChangeCommand.ARRAY_RESIZE,
                optionalAddition = 2
            )
        )
        verifyNoMoreInteractions(deltas)

        verify(stack, times(2)).push(any(Delta::class.java))


    }

    @Test
    fun testArrayDeltaComparator_withTwoUnequalArrays_2() {
        val left = arrayOf(Alpha("A", 65), Alpha("a", 97))
        val right = arrayOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97))


        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 2 new compare jobs").hasSize(2)

        verify(deltas, times(1)).add(
            delta.copy(
                change = ChangeCommand.ARRAY_RESIZE,
                optionalAddition = 2
            )
        )
        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")[2]",
                    index = 2
                ),
                change = ChangeCommand.ARRAY_SET_ELEMENT,
                leftValue = null,
                rightValue = Alpha("a", 97),
                optionalAddition = 2

            )
        )

        verifyNoMoreInteractions(deltas)

        verify(stack, times(2)).push(any(Delta::class.java))


    }

    @Test
    fun testArrayDeltaComparator_withTwoUnequalArrays_withNulls() {
        val left = arrayOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97))
        val right = arrayOf(null, Alpha("A", 65), Alpha("a", 97))


        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 2 new compare jobs").hasSize(2)

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")[0]",
                    index = 0
                ),
                change = ChangeCommand.ARRAY_SET_ELEMENT,
                leftValue = Alpha("A", 1),
                rightValue = null,
                optionalAddition = 0

            )
        )

        verifyNoMoreInteractions(deltas)

        verify(stack, times(2)).push(any(Delta::class.java))


    }


    @Test
    fun testArrayDeltaComparator_withLists() {
        val left = listOf(Alpha("A", 65), Alpha("a", 97))
        val right = listOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97))


        val delta = Delta(ROOT, left, right, null)

        val isFinished = ArrayDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two Lists can not be processed by ArrayDeltaComparator").isFalse()

        verifyNoMoreInteractions(deltas)
        verifyNoMoreInteractions(stack)
    }

    @Test
    fun testSetDeltaComparator_withTwoEqualSets() {
        val left = setOf(1, 2, 3, 4)
        val right = setOf(1, 2, 3, 4)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = SetDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Sets are equal, check is finished").isTrue
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSetDeltaComparator_withTwoEqualSetsWithDifferentImplemenation() {
        val left = HashSet(setOf(1, 2, 3, 4))
        val right = TreeSet(setOf(1, 2, 3, 4))

        val delta = Delta(ROOT, left, right, null)

        val isFinished = SetDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Sets with different implemenations are equal, check is finished")
            .isTrue
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSetDeltaComparator_withTwoUnEqualSets() {
        val left = setOf(1, 2, 3, 4)
        val right = setOf(1, 2, 5, 4)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = SetDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal are unequal, check is finished").isTrue

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ").remove(" + System.identityHashCode(3) + ")",
                ),
                change = ChangeCommand.SET_REMOVE,
                leftValue = 3,
                rightValue = null,
                optionalAddition = null

            )
        )
        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(right) + ").add(" + System.identityHashCode(5) + ")",
                ),
                change = ChangeCommand.SET_ADD,
                leftValue = null,
                rightValue = 5,
                optionalAddition = null

            )
        )
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSetDeltaComparator_withTwoUnEqualSets_leftLarger() {
        val left = setOf(1, 2, 3, 4)
        val right = setOf(4, 3, 2)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = SetDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal are unequal, check is finished").isTrue

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ").remove(" + System.identityHashCode(1) + ")",
                ),
                change = ChangeCommand.SET_REMOVE,
                leftValue = 1,
                rightValue = null,
                optionalAddition = null

            )
        )
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSetDeltaComparator_withTwoUnEqualSets_rightLarger() {
        val left = setOf(2, 4)
        val right = setOf(4, 3, 2)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = SetDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal are unequal, check is finished").isTrue

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(right) + ").add(" + System.identityHashCode(3) + ")",
                ),
                change = ChangeCommand.SET_ADD,
                leftValue = null,
                rightValue = 3,
                optionalAddition = null

            )
        )
        verifyNoMoreInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testSetDeltaComparator_withTwoMaps() {
        val left = mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Cameroon" to "Yaounde")
        val right = mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Cameroon" to "Yaounde")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = SetDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two maps cannot be processed by a SetComparator").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testMapDeltaComparator_withTwoEqualMaps() {
        val left = mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Cameroon" to "Yaounde")
        val right = mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Cameroon" to "Yaounde")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = MapDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal maps are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verifyNoInteractions(deltas)
        verify(stack, times(3)).push(any(Delta::class.java))
    }


    @Test
    fun testMapDeltaComparator_withTwoEqualMapsWithDifferentImplementations() {
        val left = HashMap(mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Cameroon" to "Yaounde"))
        val right = TreeMap(mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Cameroon" to "Yaounde"))

        val delta = Delta(ROOT, left, right, null)

        val isFinished = MapDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal maps are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verifyNoInteractions(deltas)
        verify(stack, times(3)).push(any(Delta::class.java))
    }

    @Test
    fun testMapDeltaComparator_withTwoUnEqualMaps() {
        val left = mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Chile" to "Santiago")
        val right = mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Cameroon" to "Yaounde")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = MapDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal maps are unequal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 2 new compare jobs").hasSize(2)

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")['" + System.identityHashCode("Cameroon") + "']",
                    index = "Cameroon"
                ),
                change = ChangeCommand.MAP_PUT,
                leftValue = null,
                rightValue = "Yaounde",
                optionalAddition = "Cameroon"

            )
        )
        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")['" + System.identityHashCode("Chile") + "']",
                    index = "Chile"
                ),
                change = ChangeCommand.MAP_REMOVE,
                leftValue = "Santiago",
                rightValue = null,
                optionalAddition = "Chile"

            )
        )

        verify(stack, times(2)).push(any(Delta::class.java))
    }


    @Test
    fun testMapDeltaComparator_withTwoUnEqualMaps_changedValues() {
        val left = mapOf("Australia" to "Sydney", "Belgium" to "Brussel", "Chile" to "Santiago")
        val right = mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Chile" to "Santiago")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = MapDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal maps are unequal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verify(stack, times(3)).push(any(Delta::class.java))
    }

    @Test
    fun testMapDeltaComparator_withTwoUnEqualMaps_withNullValues() {
        val left = mapOf("Australia" to null, "Belgium" to "Brussel", "Chile" to "Santiago")
        val right = mapOf("Australia" to "Canberra", "Belgium" to "Brussel", "Chile" to "Santiago")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = MapDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal maps are unequal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 2 new compare jobs").hasSize(2)

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")['" + System.identityHashCode("Australia") + "']",
                    index = "Australia"
                ),
                change = ChangeCommand.MAP_PUT,
                leftValue = null,
                rightValue = "Canberra",
                optionalAddition = "Australia"
            )
        )


        verify(stack, times(2)).push(any(Delta::class.java))
    }

    @Test
    fun testMapDeltaComparator_withTwoUnEqualMaps_leftSizeSmaller() {
        val left = mapOf("Australia" to "Sydney", "Belgium" to "Brussel", "Chile" to "Santiago")
        val right = mapOf(
            "Australia" to "Canberra",
            "Belgium" to "Brussel",
            "Chile" to "Santiago",
            "Dominican Republic" to "Santo Domingo"
        )

        val delta = Delta(ROOT, left, right, null)

        val isFinished = MapDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal maps are unequal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")['" + System.identityHashCode("Dominican Republic") + "']",
                    index = "Dominican Republic"
                ),
                change = ChangeCommand.MAP_PUT,
                leftValue = null,
                rightValue = "Santo Domingo",
                optionalAddition = "Dominican Republic"

            )
        )

        verify(stack, times(3)).push(any(Delta::class.java))
    }

    @Test
    fun testMapDeltaComparator_withTwoUnEqualMaps_rightSizeSmaller() {
        val left = mapOf(
            "Australia" to "Canberra",
            "Belgium" to "Brussel",
            "Chile" to "Santiago",
            "Dominican Republic" to "Santo Domingo",
            "Ecuador" to "Quito"
        )
        val right = mapOf(
            "Australia" to "Canberra",
            "Belgium" to "Brussel",
            "Chile" to "Santiago",
            "Dominican Republic" to "Santo Domingo"
        )

        val delta = Delta(ROOT, left, right, null)

        val isFinished = MapDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal maps are unequal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(4)

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + ")['" + System.identityHashCode("Ecuador") + "']",
                    index = "Ecuador"
                ),
                change = ChangeCommand.MAP_REMOVE,
                leftValue = "Quito",
                rightValue = null,
                optionalAddition = "Ecuador"

            )
        )

        verify(stack, times(4)).push(any(Delta::class.java))
    }

    @Test
    fun testMapDeltaComparator_withList() {
        val left = listOf("Bach", "Beethoven", "Brahms")
        val right = listOf("Mozart", "Strauss")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = MapDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two unequal maps are unequal, check is finished").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testListDeltaCompatator_withTwoEqualLists() {
        val left = listOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97))
        val right = listOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97))

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ListDeltaCompatator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verifyNoInteractions(deltas)
        verify(stack, times(3)).push(any(Delta::class.java))
    }

    @Test
    fun testListDeltaCompatator_withTwoEqualListsWithDifferentImplementation() {
        val left = LinkedList(listOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97)))
        val right = ArrayList(listOf(Alpha("A", 1), Alpha("A", 65), Alpha("a", 97)))

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ListDeltaCompatator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verifyNoInteractions(deltas)
        verify(stack, times(3)).push(any(Delta::class.java))
    }

    @Test
    fun testListDeltaCompatator_withTwoUnequalLists_1() {
        val left = listOf("Zeus", "Poseidon", "Hades")
        val right = listOf("Zeus", "Poseidon", "Hades", "Hera")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ListDeltaCompatator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verify(deltas, times(1)).add(
            delta.copy(
                change = ChangeCommand.LIST_RESIZE,
                optionalAddition = 4

            )
        )

        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + "){3}",
                    index = 3
                ),
                change = ChangeCommand.LIST_SET_ELEMENT,
                leftValue = null,
                rightValue = "Hera",
                optionalAddition = 3

            )
        )

        verify(stack, times(3)).push(any(Delta::class.java))
    }

    @Test
    fun testListDeltaCompatator_withTwoUnequalLists_2() {
        val left = listOf("Zeus", "Poseidon", "Hades", "Hera")
        val right = listOf("Zeus", "Poseidon", "Hades")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ListDeltaCompatator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 3 new compare jobs").hasSize(3)

        verify(deltas, times(1)).add(
            delta.copy(
                change = ChangeCommand.LIST_RESIZE,
                optionalAddition = 3

            )
        )

        verify(stack, times(3)).push(any(Delta::class.java))
    }

    @Test
    fun testListDeltaCompatator_withTwoUnequalLists_withNulls() {
        val left = listOf("Zeus", "Poseidon", null)
        val right = listOf("Zeus", null, null, null)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ListDeltaCompatator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two equal Arrays are equal, check is finished").isTrue
        assertThat(stack).`as`("Stack contains 1 new compare jobs").hasSize(1)

        verify(deltas, times(1)).add(
            delta.copy(
                change = ChangeCommand.LIST_RESIZE,
                optionalAddition = 4

            )
        )
        verify(deltas, times(1)).add(
            delta.copy(
                source = ROOT.copy(
                    pointer = "(" + System.identityHashCode(left) + "){3}",
                    index = 1
                ),
                change = ChangeCommand.LIST_SET_ELEMENT,
                leftValue = "Poseidon",
                rightValue = null,
                optionalAddition = 1

            )
        )


        verify(stack, times(1)).push(any(Delta::class.java))
    }

    @Test
    fun testListDeltaCompatator_withTwoSets() {
        val left = setOf("Zeus", "Poseidon", "Hades", "Hera")
        val right = setOf("Zeus", "Poseidon", "Hades")

        val delta = Delta(ROOT, left, right, null)

        val isFinished = ListDeltaCompatator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two Sets cannot be processed by ListDeltaComparator").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testUnknownCollectionDeltaComparator() {
        val left = ArrayBlockingQueue(8, true, setOf("Zeus", "Poseidon", "Hades", "Hera"))
        val right = ArrayBlockingQueue(4, true, setOf("Zeus", "Poseidon", "Hades"))

        val delta = Delta(ROOT, left, right, null)

        assertThatThrownBy {
            UnknownCollectionDeltaComparator().compare(
                delta,
                deltas!!,
                stack!!
            )
        }.`as`("Expects a exception when input is a Collection")
            .isInstanceOf(DeltaDetectionException::class.java)
    }

    @Test
    fun testUnknownCollectionDeltaComparator_withIntegers() {
        val left = 16
        val right = 8

        val delta = Delta(ROOT, left, right, null)

        val isFinished = UnknownCollectionDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("Two Integers cannot be processed by UnknownCollectionDeltaComparator").isFalse
        verifyNoInteractions(deltas)
        verifyNoInteractions(stack)
    }

    @Test
    fun testPojoDeltaComparatorComparator() {
        val left = Alpha("A", 65)
        val right = Alpha("a", 97)

        val delta = Delta(ROOT, left, right, null)

        val isFinished = PojoDeltaComparator().compare(
            delta,
            deltas!!,
            stack!!
        )

        assertThat(isFinished).`as`("The PojoDeltaComparator will always finish (until an exception is thrown)").isTrue
        assertThat(stack).`as`("The stack contains 2 new comparisonjobs")
        verifyNoInteractions(deltas)
        verify(stack, times(2)).push(any(Delta::class.java))
    }

    @Test
    fun testPojoDeltaComparatorComparator_withException() {

        val left = Alpha("a", 97)
        val right = Alpha("A", 65)

        val delta = Delta(ROOT, left, right, null)

        //not realy the position of throwing, but for testing
        //purposes it satisfies
        `when`(stack!!.push(any(Delta::class.java)))
            .thenThrow(SecurityException())

        assertThatThrownBy {
            PojoDeltaComparator().compare(
                delta,
                deltas!!,
                stack
            )
        }.`as`("Expected a DeltaDetectionException")
            .isInstanceOf(DeltaDetectionException::class.java)
            .hasCause(SecurityException())

        verifyNoInteractions(deltas)
    }

}

open class Alpha(
    open val name: String,
    open val place: Int
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Alpha) return false

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}

open class Beta(
    open val name: String,
    open val index: Int
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Alpha) return false

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}
