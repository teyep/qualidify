package com.qualidify.core.delta

import com.qualidify.core.delta.SolarSystemObjects.earth
import com.qualidify.core.delta.SolarSystemObjects.mars
import com.qualidify.core.delta.SolarSystemObjects.mercury
import com.qualidify.core.delta.SolarSystemObjects.solarSystem
import com.qualidify.core.delta.SolarSystemObjects.solarSystemWithMoon
import com.qualidify.core.delta.SolarSystemObjects.solarSystemWithSlightlyLighterMoon
import com.qualidify.core.delta.SolarSystemObjects.venus
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

/**
 * Test for Difference Detection between two Objects
 *
 * @author Marcel Pot
 */
internal class DeltaDetectorTest {


    var deltaDetector: DeltaDetector? = null

    @BeforeEach
    fun setup() {
        deltaDetector = DeltaDetector();
    }

    @Test
    fun initializingObjectDifferenceTest() {
        val creationOfEarth = deltaDetector!!.compare(null, earth)
        assertThat(creationOfEarth).`as`("DiffReport creation of earth should contain one change")
            .hasSize(1).contains(Delta(Source("-root-", "-root-", ""), null, earth, ChangeCommand.OBJECT_CREATED))
    }

    @Test
    fun deletingObjectDifferenceTest() {
        val demolishingOfEarth = deltaDetector!!.compare(earth, null)
        assertThat(demolishingOfEarth).`as`("DiffReport the end of the earth should contain one change")
            .hasSize(1).contains(Delta(Source("-root-", "-root-", ""), earth, null, ChangeCommand.OBJECT_DELETED))
    }

    @Test
    fun completeDifferenceTestWithPlaneObjects() {
        val mercuryVenusDifference = deltaDetector!!.compare(mercury, venus)
        assertThat(mercuryVenusDifference).`as`("DiffReport mercury - venus should contain two changes")
            .hasSize(2)

        val earthMarsDifference = deltaDetector!!.compare(mars, earth)
        assertThat(earthMarsDifference).`as`("DiffReport earth-mars should contain two changes").hasSize(2)
    }

    @Test
    fun noDifferenceTestWithPlainObjects() {
        val mercuryMercuryDifference = deltaDetector!!.compare(mercury, mercury)
        assertThat(mercuryMercuryDifference).`as`("DiffReport mercury - mercury should contain no changes")
            .isEmpty()

        val earthEarthDifference = deltaDetector!!.compare(earth, earth)
        assertThat(earthEarthDifference).`as`("DiffReport earth-earth should contain no changes").isEmpty()
    }

    @Test
    fun differenceTestWithComplexObjects() {
        val solarSystemSolarSystemDifference = deltaDetector!!.compare(solarSystem, solarSystem);

        assertThat(solarSystemSolarSystemDifference).`as`("DiffReport SolarSystem - SolarSystem should contain no changes")
            .isEmpty()

        val solarSystemSolarSystemWithMoonDifference =
            deltaDetector!!.compare(solarSystem, solarSystemWithMoon);

        assertThat(solarSystemSolarSystemWithMoonDifference).`as`("DiffReport SolarSystem - SolarSystemWithMoon should contain one change")
            .hasSize(1)
    }

    @Test
    fun differenceTestWithComplexObjects2() {
        val solarSystemSolarSystemDifference = deltaDetector!!.compare(solarSystem, solarSystem);

        assertThat(solarSystemSolarSystemDifference).`as`("DiffReport SolarSystem - SolarSystem should contain no changes")
            .isEmpty()

        val solarSystemSolarSystemWithMoonDifference =
            deltaDetector!!.compare(solarSystemWithSlightlyLighterMoon, solarSystemWithMoon);

        assertThat(solarSystemSolarSystemWithMoonDifference).`as`("DiffReport SolarSystem - SolarSystemWithMoon should contain one change")
            .hasSize(1)
    }

    @Test
    fun doubleReferenceTest() {
        val redBead = Bead("RED")
        val greenBead = Bead("GREEN")
        val whiteBead = Bead("WHITE")
        val leftChain = Chain(
            arrayOf(
                redBead, redBead, greenBead, whiteBead, whiteBead,
                redBead, redBead, greenBead, whiteBead, whiteBead
            )
        )
        val rightChain = Chain(
            arrayOf(
                redBead, redBead, greenBead, greenBead, whiteBead,
                redBead, redBead, greenBead, greenBead, whiteBead
            )
        )

        val difference = deltaDetector!!.compare(leftChain, rightChain)

        assertThat(difference).`as`("Difference chain should contain one change")
            .hasSize(1)
    }
}

data class Bead(
    val color: String
)

class Chain(
    val Beads: Array<Bead>
)

