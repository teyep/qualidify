package com.qualidify.core.delta

import java.math.BigDecimal
import java.math.BigInteger

object SolarSystemObjects {
    // Sun : Volume	1.41×10^18 km3
    //       Mass	1.9885×10^30 kg
    val sun = Star(BigDecimal("1.41E+18").toBigInteger(), BigDecimal("1.9885E+30").toBigInteger())

    //Mercury:  Volume 6.083×10^10 km3
    //          Mass 3.3011×10^23 kg
    val mercury = Planet(BigDecimal("1.6083E+10").toBigInteger(), BigDecimal("3.3011E+23").toBigInteger())

    //Venus:    Volume 9.2843×10^11 km3
    //          Mass 4.8675×1024 kg
    val venus = Planet(BigDecimal("9.2843E+11").toBigInteger(), BigDecimal("4.8675E+24").toBigInteger())

    //Earth: Volume	1.08321×10^12 km3
    //       Mass	5.97237×10^24 kg
    val earth = Planet(BigDecimal("1.083218E+12").toBigInteger(), BigDecimal("5.97237E+24").toBigInteger())

    // Moon:  Volume	2.1958×10^10 km3
    //        Mass	7.342×10^22 kg
    val moon = Moon(BigDecimal("2.1958E+10").toBigInteger(), BigDecimal("7.342E+22").toBigInteger())

    val earthWithMoon =
        PlanetWithMoon(BigDecimal("1.083218E+12").toBigInteger(), BigDecimal("5.97237E+24").toBigInteger(), moon)

    //Mars: Volume	1.6318×10^11 km3
    //      Mass	6.4171×10^23 kg
    val mars = Planet(BigDecimal("1.6318E+11").toBigInteger(), BigDecimal("6.4171E+23").toBigInteger())

    val solarSystem = SolarSystem(sun, listOf(mercury, venus, earth, mars))

    val solarSystemWithMoon = SolarSystem(sun, listOf(mercury, venus, earthWithMoon, mars))
    val solarSystemWithSlightlyLighterMoon = SolarSystem(
        sun,
        listOf(
            mercury,
            venus,
            earthWithMoon.copy(
                moon = Moon(
                    BigDecimal("2.1958E+10").toBigInteger(),
                    BigDecimal("7.341E+22").toBigInteger()
                )
            ),
            mars
        )
    )

}

data class SolarSystem(
    val sun: Star,
    val planets: List<Planet>
)

data class Star(
    val volume: BigInteger,
    val mass: BigInteger
)

open class Planet(
    open val volume: BigInteger,
    open val mass: BigInteger
)

data class PlanetWithMoon(
    override val volume: BigInteger,
    override val mass: BigInteger,
    val moon: Moon
) : Planet(volume, mass)


data class Moon(
    val volume: BigInteger,
    val mass: BigInteger
)
