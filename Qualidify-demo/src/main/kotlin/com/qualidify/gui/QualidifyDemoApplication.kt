package com.qualidify.gui

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("com.qualidify")
open class QualidifyTestApplication

fun main(args: Array<String>) {
    runApplication<QualidifyTestApplication>(*args)
}