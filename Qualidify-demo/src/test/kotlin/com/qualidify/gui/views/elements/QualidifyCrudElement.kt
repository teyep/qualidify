package com.qualidify.gui.views.elements

/**
 * NB: removed in QUAL-423, but needs to be reinstated with a new setup for E2E testing
 */
//open class QualidifyCrudElement : TestBenchElement() {
//
//    val crudElement: CrudElement
//        get() = `$`(CrudElement::class.java).first()
//
//    fun editor() = crudElement.editor
//
//    fun isEditorOpen() = crudElement.isEditorOpen
//
//    fun clickNewItemButton() {
//        val newButton = crudElement.newItemButton.get()
//        newButton.click()
//        assertThat(isEditorOpen()).`as`("After clicking on New Item, the Editor should open")
//    }
//
//    fun clickSaveButton() {
//        crudElement.editorSaveButton.click()
//        assertThat(isEditorOpen()).`as`("Editor is closed after saving.").isFalse
//    }
//}
//
//open class UserViewElement : QualidifyCrudElement() {
//
//    fun fillEditor(username: String, password: String, email: String): Int {
//        val usernameField = editor().`$`(TextFieldElement::class.java).id("textfield-username")
//        val passwordField = editor().`$`(PasswordFieldElement::class.java).id("passwordfield-password")
//        val emailField = editor().`$`(EmailFieldElement::class.java).id("emailfield-email")
//
//        usernameField.value = username
//        passwordField.value = password
//        emailField.value = email
//
//        clickSaveButton()
//
//        val gridElement: GridElement = crudElement.grid
//        gridElement.scrollToRow(gridElement.rowCount)
//
//        return waitUntil {
//            gridElement.getCell(username)
//        }.row
//    }
//
//}
