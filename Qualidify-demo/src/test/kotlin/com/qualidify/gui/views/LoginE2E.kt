package com.qualidify.gui.views

import com.qualidify.gui.views.helper.E2ETestCase
import com.qualidify.gui.views.helper.findElementByAttributeValue
import com.qualidify.gui.views.helper.findVaadinButton
import org.junit.jupiter.api.Test
import org.openqa.selenium.By

/**
 * End-to-end test that will start the application on a local server and verifies
 * correct login with a valid user.
 *
 * Test will require a server running the Qualidify app on port 8080.
 *
 * @author Menno Tol
 * @since 1.0
 *
 * NB: currently not working since login will always succeed
 */
internal class LoginE2E : E2ETestCase() {

    @Test
    fun `Test if login succeeds`() {
        goto(url + "login")
        val form = driver.findElement(By.tagName("vaadin-login-form"))
        val usernameField = form
            .findElementByAttributeValue("input", "name", "username")
        val passwordField = form
            .findElementByAttributeValue("input", "name", "password")
        val submitButton = form.findVaadinButton("vaadin-login-submit")

        usernameField.sendKeys("admin")
        passwordField.sendKeys("test")
        submitButton.click()

//        assertTrue(driver.findElements(By.tagName("vaadin-login-form")).size < 1)

    }

}