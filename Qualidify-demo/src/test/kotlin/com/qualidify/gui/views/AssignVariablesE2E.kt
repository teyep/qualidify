package com.qualidify.gui.views

/**
 * The AssignVariablesE2E creates new user groups with different right and assigns tasks to users belonging to this group.
 * Uses employeeOnboarding.cmmn.xml
 *
 * @author Menno Tol
 * @since 1.0
 */
//internal class AssignVariablesE2E : AbstractViewE2ETestCase() {
//
//    private val user = UserDto(
//        username = "Karel Kandidaat",
//        email = "karel@kandidaat.nl",
//        password = "altijdmis",
//        securityGroups = setOf("candidate")
//    )
//
//    @BeforeAll
//    fun setupGroups() {
//        setDriver(ChromeDriver())
//        makeGroup(user.securityGroups.first(), "Potential employees")
//        driver.quit()
//
//        setDriver(ChromeDriver())
//        makeUser(user.username, user.email, user.password, user.securityGroups.first())
//        driver.quit()
//    }
//
//    @Test
//    fun `Assign tasks to group`() {
//
//        goto(location + "projects/new")
//
//        val startEmployeeOnboarding = `$`(ButtonElement::class.java).id("employee-onboarding")
//        startEmployeeOnboarding.click()
//
//        await().atMost(Duration.ofSeconds(2)).untilAsserted {
//            assert(`$`(AccordionElement::class.java).exists())
//        }
//
//
//        // open the first accordion panel and enter data
//        val accordion = `$`(AccordionElement::class.java).first()
//        accordion.open(0)
//
//        val nameField = accordion.`$`(TextFieldElement::class.java).id("textfield-name")
//        val projectName = "Onboarding ${user.username}"
//        nameField.value = projectName
//        nameField.submit()
//
//        val descriptionField = accordion.`$`(TextAreaElement::class.java).id("textarea-description")
//        descriptionField.value = "The onboarding case for ${user.username}"
//        descriptionField.submit()
//
//        accordion.open(1)
//        val potentialEmployeeField = accordion.`$`(TextFieldElement::class.java).id("textfield-potentialemployee")
//        potentialEmployeeField.value = user.username
//        potentialEmployeeField.submit()
//
//        val doneButton = `$`(ButtonElement::class.java).id("button-done")
//        doneButton.click()
//
//
//        // complete all required tasks
//        `$`(VerticalLayoutElement::class.java).id("panel-tasksongoing")
//
//        var tasksOngoingGrid = `$`(GridElement::class.java).id("datagrid-tasksongoing")
//        assert(tasksOngoingGrid.isDisplayed)
//
//        var counter = 0
//        while (tasksOngoingGrid.rowCount > 0 && counter < 4) {  // only the first 4 tasks need to be completed
//            menuBarElements(tasksOngoingGrid)[0].buttons[0].click()
//            Thread.sleep(100)  // some time is needed to make sure the contextmenu is opened
//
//            clickContextMenuItem(tasksOngoingGrid, "Complete")
//            counter ++
//        }
//
//
//        // check if the remaining tasks are assigned to the candidate
//        tasksOngoingGrid = `$`(GridElement::class.java).id("datagrid-tasksongoing")
//
//        val assignedTasks = mutableListOf<TestBenchElement>()
//        for (i in 0 until tasksOngoingGrid.rowCount) {
//            val row = tasksOngoingGrid.getRow(i)
//            if (row.text.contains("Assignee")) assignedTasks += row
//        }
//
//        assignedTasks.ifEmpty { fail("No tasks where found that contain 'Assignee' on $tasksOngoingGrid") }
//        assert(assignedTasks.size == 2) { "Two tasks assigned to ${user.username} should be found" }
//        assignedTasks.forEach { assert(it.text.contains(user.username)) }
//
//    }
//
//    private fun makeGroup(name: String, description: String) {
//        goto(location + "securitygroups")
//
//        val addButton = `$`(ButtonElement::class.java).id("add-item")
//        addButton.click()
//
//
//        // open the name layout
//        val nameField = `$`(TextFieldElement::class.java).id("textfield-name")
//        nameField.value = name
//        nameField.submit()
//
//        val proceedButton = `$`(ButtonElement::class.java).id("proceed")
//        proceedButton.click()
//
//
//        // open the editor layout
//        val formLayout = `$`(FormLayoutElement::class.java).attribute("slot", "form").waitForFirst()
//        assert(formLayout.isDisplayed)
//
//        val groupName = formLayout.`$`(TextFieldElement::class.java).id("textfield-name")
//        groupName.value = ""  // field needs to be changed before continuing
//        groupName.value = name
//
//        val descriptionField = formLayout.`$`(TextFieldElement::class.java).id("textfield-description")
//        descriptionField.value = description
//        descriptionField.submit()
//
//        val checkbox = formLayout.`$`(CheckboxElement::class.java).all()
//        checkbox.forEach {
//            if (it.label == "All") {
//                it.submit()
//            }
//        }
//
//        val saveButton = `$`(ButtonElement::class.java)
//            .attributeContains("slot", "save-button").waitForFirst()
//        saveButton.click()
//
//
//        //check if the crud contains the created group
//        val crud = `$`(CrudElement::class.java).id("datacrud-securitygroupdto")
//
//        await().atMost(Duration.ofSeconds(2)).untilAsserted {
//            assert(crud.text.contains(name))
//            assert(crud.text.contains(description))
//        }
//
//        logger.info("Security group $name, $description was created")
//
//    }
//
//    private fun makeUser(name: String, email: String, password: String, group: String) {
//        goto(location + "users")
//
//
//        // click the add item button
//        val crud = `$`(CrudElement::class.java).id("datacrud-userdto")
//        val rowsBefore = crud.grid.rowCount
//        crud.newItemButton.get().click()
//
//
//        // select the formlayout
//        val formLayout = `$`(FormLayoutElement::class.java).attribute("slot", "form").waitForFirst()
//        assert(formLayout.isDisplayed)
//
//
//        // open the new item layout
//        val usernameField = formLayout.`$`(TextFieldElement::class.java).id("textfield-username")
//        usernameField.value = name
//        usernameField.submit()
//
//        val emailField = formLayout.`$`(EmailFieldElement::class.java).id("emailfield-email")
//        emailField.value = email
//        emailField.submit()
//
//        val passwordField = formLayout.`$`(PasswordFieldElement::class.java).id("passwordfield-password")
//        passwordField.value = password
//        passwordField.submit()
//
//
//        // select groups
//        val checkboxGroup = formLayout.`$`(CheckboxElement::class.java).all()
//        checkboxGroup.forEach {
//            if (it.label == group) {
//                it.submit()
//            }
//        }
//
//
//        // save the new user
//        val saveButton = `$`(ButtonElement::class.java).attribute("slot", "save-button").waitForFirst()
//        assert(saveButton.isDisplayed)
//        saveButton.click()
//
//        val rowsAfter = crud.grid.rowCount
//        assertEquals(1, rowsAfter - rowsBefore)
//
//    }
//
//}