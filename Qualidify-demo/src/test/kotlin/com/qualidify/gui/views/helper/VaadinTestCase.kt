package com.qualidify.gui.views.helper

/**
 * A superclass with some helpers to aid TestBench developers.
 *
 * @author Marcel Pot
 * @since 1.0
 *
 * NB: removed in QUAL-423, but needs to be reinstated with a new setup for E2E testing
 */
//abstract class VaadinTestCase : HasDriver, HasTestBenchCommandExecutor, HasElementQuery {
//
//    private var webDriver: WebDriver? = null
//
//    /**
//     * Convenience method the return [TestBenchCommands] for the default [WebDriver] instance.
//     *
//     * @return The driver cast to a TestBenchCommands instance.
//     */
//    fun testBench(): TestBenchCommands = (driver as TestBenchDriverProxy).commandExecutor
//
//    /**
//     * Combines a base URL with an URI to create a final URL. This removes
//     * possible double slashes if the base URL ends with a slash and the URI
//     * begins with a slash.
//     *
//     * @param baseUrl the base URL
//     * @param uri     the URI
//     * @return the URL resulting from the combination of base URL and URI
//     */
//    protected fun concatUrl(baseUrl: String, uri: String): String {
//        return if (baseUrl.endsWith("/") && uri.startsWith("/")) {
//            baseUrl + uri.substring(1)
//        } else baseUrl + uri
//    }
//
//    /**
//     * Returns the [WebDriver] instance previously specified by [.setDriver], or (if the previously
//     * provided WebDriver instance was not already a [TestBenchDriverProxy] instance) a [TestBenchDriverProxy]
//     * that wraps that driver.
//     *
//     * @return the active WebDriver instance
//     */
//    override fun getDriver(): WebDriver = webDriver!!
//
//    /**
//     * Sets the active [WebDriver] that is used by this case
//     *
//     * @param driver The WebDriver instance to set.
//     */
//    fun setDriver(driver: WebDriver?) {
//        var webDriver = driver
//        if (webDriver != null && webDriver !is TestBenchDriverProxy) {
//            webDriver = TestBench.createDriver(webDriver)
//        }
//        this.webDriver = webDriver
//    }
//
//    override fun getContext(): SearchContext = driver
//
//    override fun getCommandExecutor(): TestBenchCommandExecutor =
//            (driver as HasTestBenchCommandExecutor).commandExecutor
//
//    fun findElement(by: By?): WebElement = context.findElement(by)
//
//    fun findElements(by: By?): List<WebElement> = context.findElements(by)
//
//    /**
//     * Decorates the element with the specified Element type, making it possible
//     * to use component-specific API on elements found using standard Selenium
//     * API.
//     *
//     * @param elementType The type (class) containing the API to decorate with
//     * @param element     The element instance to decorate
//     * @return The element wrapped in an instance of the specified element type.
//     */
//    fun <T : TestBenchElement?> wrap(elementType: Class<T>?, element: WebElement): T {
//        return (element as TestBenchElement).wrap(elementType)
//    }
//
//    /**
//     * Executes the given JavaScript in the context of the currently selected
//     * frame or window. The script fragment provided will be executed as the
//     * body of an anonymous function.
//     *
//     * This method wraps any returned [WebElement] as [TestBenchElement].
//     *
//     * @param script the script to execute
//     * @param args   the arguments, available in the script as `arguments[0]...arguments[N]`
//     * @return whatever [org.openqa.selenium.JavascriptExecutor.executeScript] returns
//     * @throws UnsupportedOperationException if the underlying driver does not support JavaScript execution
//     * @see JavascriptExecutor.executeScript
//     */
//    protected fun executeScript(script: String?, vararg args: Any?): Any {
//        return commandExecutor.executeScript(script, *args)
//    }
//
//    companion object {
//
//        private val logger by LoggerDelegation()
//
//        init {
//            val properties = Properties()
//
//            try {
//                properties.load(TestBenchTestCase::class.java
//                        .getResourceAsStream("testbench.properties"))
//            } catch (e: Exception) {
//                logger.warn("Unable to read TestBench properties file", e)
//                throw ExceptionInInitializerError(e)
//            }
//
//            val seleniumVersion = BuildInfo().releaseLabel
//            val testbenchVersion = properties.getProperty("testbench.version")
//            val expectedVersion: String = properties.getProperty("selenium.version")
//
//            if (seleniumVersion == null || seleniumVersion != expectedVersion) {
//                logger.warn(
//                        "This version of TestBench depends on Selenium version "
//                                + expectedVersion + " but version "
//                                + seleniumVersion
//                                + " was found. Make sure you do not have multiple versions of Selenium on the classpath.")
//            }
//
//            LocalProKey.write(ProKey("", ""))
//
//            val product = Product("vaadin-testbench", testbenchVersion)
//            History.setLastCheckTimeNow(product)
//
//        }
//    }
//}
