package com.qualidify.gui.views

/**
 * End-to-end test that verifies if the search field works as expected.
 *
 * Test requires a server running the Qualidify app on port 8080
 *
 * NB: removed in QUAL-423, but needs to be reinstated with a new setup for E2E testing
 */
//internal class SearchFieldE2E : AbstractViewE2ETestCase() {
//
//    @Test
//    fun `Test search field`() {
//
//        goto(location + "projects/new")
//
//        val grid = `$`(GridElement::class.java).waitForFirst()
//        assertEquals(5, grid.rowCount, "Five project templates should be available")
//
//        val searchField = `$`(TextFieldElement::class.java).id("searchfield-projecttemplatedto")
//        searchField.value = "S"
//        searchField.submit()
//
//        Thread.sleep(500)  // sufficient time for the lazy field to update the grid
//
//        assertEquals(
//            1,
//            `$`(GridElement::class.java).waitForFirst().rowCount,
//            "After selecting project templates with 'S' one templates should be shown (Small Requirement)"
//        )
//    }
//
//}