package com.qualidify.gui.views.helper

/**
 * Abstract class for end-to-end testing Vaadin Elements.
 * Type [T] the element to test.
 *
 * @author Menno Tol
 * @since 1.0
 */
//abstract class AbstractViewE2ETestElement<T> : AbstractViewE2ETestCase() {
//    abstract fun openView(): T
//}