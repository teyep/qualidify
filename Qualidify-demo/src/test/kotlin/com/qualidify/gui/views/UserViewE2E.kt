package com.qualidify.gui.views

/**
 * End-to-end test that will start the application on a local server and verifies
 * the addition of a new user.
 *
 * Test will require a server running the Qualidify app on port 8080.
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 *
 * NB: removed in QUAL-423, but needs to be reinstated with a new setup for E2E testing
 */
//@Order(2)
//internal class UserViewE2E : AbstractViewE2ETestElement<UserViewElement>() {
//
//    @Test
//    fun `add new user`() {
//        goto(location + "users")
//
//        val view = openView()
//        view.clickNewItemButton()
//        val rowId = view.fillEditor(
//            username = "Ray Tomlinson",
//            password = "QWERTYUIOP",
//            email = "user@host.com"
//        )
//
//        assertThat(rowId).isPositive
//    }
//
//    override fun openView(): UserViewElement {
//        return `$`(UserViewElement::class.java).waitForFirst()
//    }
//}