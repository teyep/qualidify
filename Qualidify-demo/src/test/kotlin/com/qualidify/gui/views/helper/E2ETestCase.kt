package com.qualidify.gui.views.helper

import com.qualidify.core.log.LoggerDelegation
import io.github.bonigarcia.wdm.WebDriverManager
import org.junit.jupiter.api.*
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import java.time.Duration

/**
 * Abstract class for end-to-end testing user browser drivers.
 * The drivers are managed by the WebDriverManager.
 *
 * NB: the logging level of the WebDriver browser is restricted using logback-test.xml,
 * located in Qualidify-demo/src/test/resources/
 *
 * @author Menno Tol
 * @since 1.0
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class E2ETestCase {

    private var startTime: Long? = null
    lateinit var driver: WebDriver
    val url = "http://localhost:8080/"
    val logger by LoggerDelegation()

    fun goto(url: String) {
        for (i in 1..5) {
            driver[url]
            if (!driver.title.contains("Error")) break
        }
    }

    @BeforeAll
    fun setupClass() {
        WebDriverManager.chromedriver().setup()
        logger.info("Starting the chrome driver")
    }

    @BeforeEach
    @Throws(Exception::class)
    fun setupTest() {
        /**
         * To run in headless mode
         * options.addArguments("--headless", "--disable-gpu");
         */
        val options = ChromeOptions()
        options.addArguments("--remote-allow-origins=*")

        driver = ChromeDriver(options)
        setDriverTimeout()

        /**
         * Define the directory for reference screenshots and for error files (does not work)
         * setScreenshotReferenceDirectory("src/test/screenshots")
         * setScreenshotErrorDirectory("target/screenshot_errors")
         * setScreenshotComparisonCursorDetection(true)
         */

        startTime = System.nanoTime()
    }

    @AfterEach
    fun tearDown() {
        val timeElapsed = (System.nanoTime() - startTime!!) / 100_000_000.0
        logger.info("Executed ${this.javaClass.simpleName}, took $timeElapsed seconds")
        driver.quit()
        logger.info("Closing the driver")
    }

    private fun setDriverTimeout() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5))
    }

}