package com.qualidify.gui.views

import com.qualidify.gui.views.helper.E2ETestCase
import org.junit.jupiter.api.Test
import org.openqa.selenium.By
import org.openqa.selenium.Dimension
import kotlin.test.assertEquals

/**
 * End-to-end test that will start the application on a local server and verifies
 * correct use of the logo.
 *
 * Test will require a server running the Qualidify app on port 8080.
 *
 * @author Marcel Pot
 * @author Menno Tol
 * @since 1.0
 */
internal class BrandExpressionE2E : E2ETestCase() {

    @Test
    fun testIfLogoIsAvailable() {
        goto(url)

        val brandExpressionElement = driver.findElement(By.id("brand-expression"))
        val logo = brandExpressionElement.findElement(By.tagName("img"))

        assertEquals("${url}images/Qualidify-logo.png", logo.getAttribute("src"))
        assertEquals(Dimension(79, 32), logo.size)
    }

}