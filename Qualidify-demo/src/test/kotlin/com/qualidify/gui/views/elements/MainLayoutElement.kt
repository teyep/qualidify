package com.qualidify.gui.views.elements

/**
 * NB: removed in QUAL-423, but needs to be reinstated with a new setup for E2E testing
 */
//@Attribute(name = "id", value = "main-layout")
//open class MainLayoutElement : TestBenchElement() {
//    val brandExpression: DivElement
//        get() = `$`(DivElement::class.java).id("brand-expression")
//}