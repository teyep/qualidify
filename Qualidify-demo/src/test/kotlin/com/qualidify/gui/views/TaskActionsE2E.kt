package com.qualidify.gui.views

/**
 * E2E test for task actions.
 * Uses the EmployeeOnboarding @see [link](dev-resources/cases/employeeOnboarding.cmmn.xml) workflow.
 *
 * @author Menno Tol
 * @since 1.0
 *
 * NB: removed in QUAL-423, but needs to be reinstated with a new setup for E2E testing
 */
//@Order(3)
//internal class TaskActionsE2E : AbstractViewE2ETestCase() {
//
//    @Test
//    fun taskActions() {
//
//        // start and open a new project from the basicRequirementFlow
//        val id = startProject("Employee Onboarding")
//
//        // assign a task to a user
//        var tasksOngoingGrid = `$`(GridElement::class.java).id("datagrid-tasksongoing")
//        assert(tasksOngoingGrid.isDisplayed)
//
//        val rowTask1 = getCell("datagrid-tasksongoing", "Agree start date").row
//        val menuBarTask1 = tasksOngoingGrid.getCell(rowTask1, 1)  // menubar is at index 1
//
//        menuBarElements(menuBarTask1)[0].buttons[0].click()
//        Thread.sleep(100)  // some time is needed to make sure the contextmenu is opened
//
//        clickContextMenuItem(menuBarTask1, "Assign")
//
//        val dialog = `$`(DialogElement::class.java).waitForFirst()
//        assertThat(dialog).isNotNull
//            .`as`("Dialog should exist")
//        val selectField = dialog.`$`(SelectElement::class.java).first()
//        assertThat(selectField.toString()).contains("com.vaadin.flow.component.select.testbench.SelectElement")
//            .`as`("SelectField should be of 'select.testbench', not of 'html.testbench'")
//
//        selectField.selectByText("menno")
//
//        val dialogButton = dialog.`$`(ButtonElement::class.java).first()
//        dialogButton.click()
//
//        val ongoingTask1 = getCell("datagrid-tasksongoing", "Agree start date")
//        assertThat(ongoingTask1.text).contains("Assignee: menno")
//            .`as`("The task should be assigned to 'menno'")
//
//
//        // claim and unclaim a task
//        tasksOngoingGrid = `$`(GridElement::class.java).id("datagrid-tasksongoing")
//        assert(tasksOngoingGrid.isDisplayed)
//
//        val rowClaimTask = getCell("datagrid-tasksongoing", "Allocate office").row
//        val menuBarClaimTask = tasksOngoingGrid.getCell(rowClaimTask, 1)  // menubar is at index 1
//
//        menuBarElements(menuBarClaimTask)[0].buttons[0].click()
//        Thread.sleep(100)  // some time is needed to make sure the contextmenu is opened
//
//        clickContextMenuItem(menuBarClaimTask, "Claim")
//
//        val ongoingTasks2 = getCell("datagrid-tasksongoing", "Allocate office")
//        assertThat(ongoingTasks2.text).contains("Assignee: anonymousUser")
//            .`as`("The task should be assigned to 'anonymousUser'")
//
//        tasksOngoingGrid = `$`(GridElement::class.java).id("datagrid-tasksongoing")
//        assert(tasksOngoingGrid.isDisplayed)
//
//        val rowUnclaimTask = getCell("datagrid-tasksongoing", "Allocate office").row
//        val menuBarUnclaimTask = tasksOngoingGrid.getCell(rowUnclaimTask, 1)  // menubar is at index 1
//
//        menuBarElements(menuBarUnclaimTask)[0].buttons[0].click()
//        Thread.sleep(100)  // some time is needed to make sure the contextmenu is opened
//
//        clickContextMenuItem(menuBarUnclaimTask, "Unclaim")
//
//        val todoTasks2 = getCell("datagrid-tasksongoing", "Allocate office")
//        assertThat(todoTasks2.text).doesNotContain("anonymousUser")
//            .`as`("Anonymous user should not be assignee anymore of the unclaimed task")
//
//
//        // complete a task
//        tasksOngoingGrid = `$`(GridElement::class.java).id("datagrid-tasksongoing")
//        val numberOfTasksTodo = tasksOngoingGrid.rowCount
//
//        val rowCompleteTask = getCell("datagrid-tasksongoing", "Create email address").row
//        val menuBarCompleteTask = tasksOngoingGrid.getCell(rowCompleteTask, 1)  // menubar is at index 1
//
//        menuBarElements(menuBarCompleteTask)[0].buttons[0].click()
//        Thread.sleep(100)  // some time is needed to make sure the contextmenu is opened
//
//        clickContextMenuItem(menuBarCompleteTask, "Complete")
//
//        assertThat(`$`(GridElement::class.java).id("datagrid-tasksongoing").rowCount).isEqualTo(numberOfTasksTodo - 1)
//            .`as`("One task should be removed from the tasks todo list")
//
//
//        // cancel the project
//        val cancelButton = `$`(ButtonElement::class.java).id("button-cancel-project")
//        cancelButton.click()
//
//        val confirmDialog = `$`(DialogElement::class.java).waitForFirst()
//        val confirmCancelButton = confirmDialog.`$`(ButtonElement::class.java).id("confirm")
//        confirmCancelButton.click()
//
//        assertThat(`$`(GridElement::class.java).id("datagrid-tasksongoing").rowCount).isEqualTo(0)
//            .`as`("The task ongoing list should be empty")
//
//        goto(location + "projectinstances")
//        val grid3 = `$`(GridElement::class.java).id("datagrid-projectinstancedto")
//        val row = grid3.getCell(id).row
//
//        assertThat(grid3.getRow(row).text).contains(id)
//            .`as`("The correct row should be selected")
//        assertThat(grid3.getRow(row).text).contains("terminated")
//            .`as`("The project should be terminated")
//
//
//        // check if there are no loose ends when loading main page
//        goto(location)
//
//    }
//
//}