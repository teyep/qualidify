package com.qualidify.gui.views

/**
 * The TouristE2E visits all routes exposed using the @Route annotation and waits for the first VerticalLayoutElement
 * to be loaded.
 *
 * @author Menno Tol
 * @since 1.0
 *
 * NB: removed in QUAL-423, but needs to be reinstated with a new setup for E2E testing
 */
//@Order(5)
//internal class TouristE2E : AbstractViewE2ETestCase() {
//
//    @BeforeAll
//    fun startProjects() {
//
//        val projects = mutableSetOf<String>()
//        projects += "Small Requirement"
//        projects += "Basic Requirement"
//        projects += "Employee Onboarding"
//
//        projects.forEach {
//            setDriver(ChromeDriver())  // driver otherwise not yet available
//            startProject(it)
//            driver.quit()
//        }
//    }
//
//    @ParameterizedTest
//    @MethodSource("getRoutes")
//    fun touring(route: String) {
//
//        logger.info("Testing ${location + route}")
//
//        goto(location + route)
//
//        // load the vaadin app layout (should be loaded within 1 second)
//        val appLayout = `$`(AppLayoutElement::class.java).waitForFirst(1)
//        assertThat(appLayout).isNotNull
//                .`as`("The vaadin-app-layout should have been loaded within 1 second")
//
//        // unhappy paths
//        val divElements = appLayout.`$`(DivElement::class.java).all()
//
//        divElements.forEach {
//            assertThat(it.getProperty("id").toString()).doesNotContain("view-unknown-error")
//                    .`as`("The unknown error page should not be shown")
//            assertThat(it.getProperty("id").toString()).doesNotContain("view-access-denied")
//                    .`as`("The access denied page should not be shown")
//            assertThat(it.getProperty("id").toString()).doesNotContain("view-page-not-found")
//                    .`as`("The page should be found")
//        }
//
//        // happy path
//        assertThat(appLayout.`$`(VerticalLayoutElement::class.java).waitForFirst()).isNotNull
//                .`as`("The first vertical layout element should be loaded correctly")
//
//        testFilters(findCrud(), location, route)
//        testSorters(findCrud(), location, route)
//        testSearchFields(location, route)
//
//    }
//
//    private fun testSearchFields(location: String, route: String) {
//        val textFields = `$`(TextFieldElement::class.java).all()
//        val searchFields = textFields.filter { it.placeholder == "Search by name" }
//        logger.info("Number of search fields found: ${searchFields.size} ")
//
//        searchFields.forEach {
//            val letter = RandomStringUtils.randomAlphabetic(1)
//            val name = it.getAttribute("id")
//            logger.info("Testing search field '$name' with letter '$letter'")
//            it.value = letter
//            it.submit()
//            Thread.sleep(500)  // sufficient time for the lazy field to update the grid
//
//            val message = `$`(DivElement::class.java).attributeContains("class", "v-system-error")
//            if (message.exists()) {
//                fail("Search field '$name' does not work on '${location.trimEnd('/')}/$route'")
//            }
//
//            it.value = ""
//            it.submit()
//            Thread.sleep(500)  // sufficient time for the lazy field to update the grid
//        }
//    }
//
//    private fun testFilters(crud: CrudElement?, location: String, route: String) {
//        val filters = crud?.filterFields
//        logger.info("Number of filter fields found: ${filters?.size ?: 0 }")
//
//        filters?.forEach {
//            val randomString = RandomStringUtils.randomAlphanumeric(8)
//            val name = it.getAttribute("id").substringAfter('-')
//            logger.info("Testing filter field '$name' with '$randomString'...")
//            it.value = randomString
//            it.submit()
//
//            val message = `$`(DivElement::class.java).attributeContains("class", "v-system-error")
//            if (message.exists()) {
//                fail("The filter field for column '$name' does not work on '${location.trimEnd('/')}/$route'")
//            }
//
//            it.value = ""
//            it.submit()
//        }
//    }
//
//    private fun testSorters(crud: CrudElement?, location: String, route: String) {
//        val sorters = crud?.grid?.`$`("vaadin-grid-sorter")?.all()
//        logger.info("Number of sort elements found: ${sorters?.size ?: 0}")
//
//        sorters?.forEach {
//            logger.info("Testing sort element '${it.text}'...")
//            it.click()
//            it.click()
//            it.click()
//
//            val message = `$`(DivElement::class.java).attributeContains("class", "v-system-error")
//            if (message.exists()) {
//                fail("The sorting element for column '${it.text}' does not work on '${location.trimEnd('/')}/$route'")
//            }
//        }
//    }
//
//    private fun findCrud(): CrudElement? {
//        return if (`$`(CrudElement::class.java).exists()) {
//            `$`(CrudElement::class.java).waitForFirst()
//        } else null
//    }
//
//    private fun getRoutes(): Set<String> {
//
//        val routes = mutableSetOf<String>()
//
//        Reflections("com.qualidify.gui")
//                .getTypesAnnotatedWith(Route::class.java)
//                .forEach { routes += it.getDeclaredAnnotation(Route::class.java).value }
//
//        // custom routes not using the @Route annotation
//        routes += "projects/new"
//        routes += "metadata/Metadataobject"
//        // routes += "login"  // uses different layout, tested via LoginFormE2E
//
//        // remove incorrect routes
//        routes -= "___NAMING_CONVENTION___"
//        routes -= "metadata"
//        routes -= "login"
//        routes -= "logout"
//        routes -= "/"
//
//        return routes
//    }
//
//
//}