package com.qualidify.gui.views.helper

import org.openqa.selenium.By
import org.openqa.selenium.WebElement

/**
 * Find an element by its tagname and an attribute key-value pair.
 */
fun WebElement.findElementByAttributeValue(elementTagName: String, attributeName: String, attributeValue: String): WebElement =
    this.findElement(By.cssSelector("$elementTagName[$attributeName='$attributeValue']"))

/**
 * Find a Vaadin button based on it's part attribute value.
 */
fun WebElement.findVaadinButton(partAttributeValue: String) =
    this.findElementByAttributeValue("vaadin-button", "part", partAttributeValue)

//fun setCrudFilter(crud: CrudElement, filterId: String, filterValue: String) {
//    val filter = crud.filterFields[crud.filterFields.indexOf(
//        crud.`$`(TextFieldElement::class.java).id(filterId)
//    )]
//    filter.value = filterValue
//}
//
///*
//* Gets the cell from a grid using search text and id.
//* Implemented as extension function to the AbstractViewE2ETestCase,
//* but listed here to keep all helper functions together.
//*/
//fun AbstractViewE2ETestCase.getCell(gridId: String, searchText: String): GridTHTDElement {
//
//    var cell: GridTHTDElement? = null
//
//    val grid = `$`(GridElement::class.java).id(gridId)
//
//    for (i in 0 until grid.rowCount) {
//        grid.getCell(i, 0)
//        if (grid.getCell(i, 0).text.contains(searchText, false)) {
//            cell = grid.getCell(i, 0)
//        }
//    }
//
//    return cell!!
//}
//
///*
//* Gets the button from a grid cell using search text and id.
//* Implemented as extension function to the AbstractViewE2ETestCase,
//* but listed here to keep all helper functions together.
//*/
//fun AbstractViewE2ETestCase.getButton(gridId: String, searchText: String, buttonId: String): ButtonElement {
//
//    var cell: GridTHTDElement? = null
//
//    val grid = `$`(GridElement::class.java).id(gridId)
//
//    for (i in 0 until grid.rowCount) {
//        grid.getCell(i, 0)
//        if (grid.getCell(i, 0).text.contains(searchText, false)) {
//            cell = grid.getCell(i, 0)
//        }
//    }
//
//    return cell!!.`$`(ButtonElement::class.java).id(buttonId)
//}
//
///**
// * Returns a list with all menubarelements in a grid.
// *
// * @param element: the element to look for menubars
// */
//fun AbstractViewE2ETestCase.menuBarElements(element: TestBenchElement): MutableList<MenuBarElement> =
//    element.`$`(MenuBarElement::class.java).all()
//
//
///**
// * Finds a contextmenuitem in a menubar and clicks it.
// *
// * @param element: the element that contains the menubar. NB: is only used to provide context for the fail message
// * @param searchText: the text that the contextmenuitem should contain
// */
//fun AbstractViewE2ETestCase.clickContextMenuItem(element: TestBenchElement, searchText: String) =
//    `$`("vaadin-context-menu-item").all()
//        .find { it.text == searchText }
//        ?.click()
//        ?: fail("No menu item with text '$searchText' found on '${element.text}'.")
//
///**
// * Starts and opens a project with the specified project name. Tests if all required steps are executed correctly.
// *
// * Implemented as extension function to the [AbstractViewE2ETestCase],
// * but listed here to keep all helper functions together.
// *
// * @param projectName: the project name including spaces (e.g. "Basic Requirement")
// * @return the project id for redirection purposes
// */
//fun AbstractViewE2ETestCase.startProject(projectName: String): String {
//
//    goto(location + "projecttemplates")
//    val crud = `$`(CrudElement::class.java).waitForFirst()
//    setCrudFilter(crud, "filter-name", projectName)
//    assertThat(crud.grid.rowCount).isEqualTo(1)
//        .`as`("After filtering the grid should contain 1 row")
//
//    val row = crud.grid.getRow(0)
//    assertThat(row.text.contains(projectName)).isTrue
//        .`as`("This row should contain the $projectName")
//    assertThat(row.`$`(ButtonElement::class.java).all().size == 1)
//        .`as`("A single button should be present")
//
//    val button = `$`(ButtonElement::class.java).id("start-projecttemplatedto")
//    button.click()
//
//    val id = getMostRecentProjectInstanceId(projectName)
//
//    logger.info("Project '$projectName' was started, received id '$id'")
//
//    return id
//}
//
///**
// * Returns the project instance id of the most recent started project.
// *
// * @author Menno Tol
// * @since 1.0
// */
//fun AbstractViewE2ETestCase.getMostRecentProjectInstanceId(projectName: String): String {
//
//    goto(location + "projectinstances")
//    val crud = `$`(CrudElement::class.java).waitForFirst()
//    setCrudFilter(crud, "filter-cmmninstancestate", "active")
//
//    val sorter = crud.`$`(DateTimePickerElement::class.java).id("datetimepicker-starttime")
//    sorter.click()  // sorted oldest on top
//
//    val grid = `$`(GridElement::class.java).id("datagrid-projectinstancedto")
//    val id = grid.getCell(grid.rowCount - 1, 0).text  // last row, first column should contain id
//
//    goto(location + "projects/" + id)
//    assertThat(`$`(HorizontalLayoutElement::class.java).id("titlebox").text).contains(projectName)
//        .`as`("The titlebox of the project detail bar should display the '$projectName' project")
//
//    return id
//}
//
//
///**
// * Gets the CMMN instance id using the specified project id. Tests if all required steps are executed correctly.
// *
// * Implemented as extension function to the [AbstractViewE2ETestCase],
// * but listed here to keep all helper functions together.
// *
// * @param projectId: the project instance id
// * @return the associated cmmn instance id
// */
//fun AbstractViewE2ETestCase.getCmmnInstanceId(projectId: String): String {
//
//    goto(location + "projectinstances")
//    val crud = `$`(CrudElement::class.java).waitForFirst()
//    setCrudFilter(crud, "filter-id", projectId)
//    assertThat(crud.grid.rowCount).isEqualTo(1)
//        .`as`("After filtering the grid should contain 1 row")
//
//    val grid = `$`(GridElement::class.java).id("datagrid-projectinstancedto")
//
//    return grid.getCell(0, 3).text
//}
//
///**
// * Gets the CMMN task id using the specified CMMN instance id. Tests if all required steps are executed correctly.
// *
// * Implemented as extension function to the [AbstractViewE2ETestCase],
// * but listed here to keep all helper functions together.
// *
// * @param cmmnInstanceId: the cmmn instance id
// * @param searchText: the name of the task
// * @return the id of the task
// */
//fun AbstractViewE2ETestCase.getTaskId(cmmnInstanceId: String, searchText: String): String {
//
//    goto(location + "cmmntasks")
//    val crud = `$`(CrudElement::class.java).waitForFirst()
//    setCrudFilter(crud, "filter-cmmninstanceid", cmmnInstanceId)
//    setCrudFilter(crud, "filter-name", searchText)
//    assertThat(crud.grid.rowCount).isEqualTo(1)
//        .`as`("After filtering the grid should contain 1 row")
//
//    val grid = `$`(GridElement::class.java).id("datagrid-taskdto")
//
//    return grid.getCell(0, 0).text
//}
