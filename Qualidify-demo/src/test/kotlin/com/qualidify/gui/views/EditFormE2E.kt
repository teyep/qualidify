package com.qualidify.gui.views

/**
 * E2E test for entering, saving and editing form data.
 * Uses the smallRequirementCase @see [link](dev-resources/cases/smallRequirement.cmmn.xml) workflow.
 *
 * @author Menno Tol
 * @since 1.0
 * NB: removed in QUAL-423, but needs to be reinstated with a new setup for E2E testing
 */
//@Order(4)
//internal class EditFormE2E : AbstractViewE2ETestCase() {
//
//    @Test
//    fun `Edit form`() {
//
//        // start and open a new project from the smallRequirementCase
//        val projectId = startProject("Small Requirement")
//
//
//        // get the cmmninstance id
//        val cmmnId = getCmmnInstanceId(projectId)
//
//
//        // get the task id
//        val taskId = getTaskId(cmmnId, "Input Requirement")
//
//
//        // open the task named 'Input Requirement'
//        goto(location + "tasks/" + taskId)
//        val taskEditor = `$`(FormLayoutElement::class.java).id("input-requirement")
//
//
//        // check if buttons are disabled before entering text
//        val saveButton = `$`(ButtonElement::class.java).id("savebutton")
//        val completeButton = `$`(ButtonElement::class.java).id("completebutton")
//
//        await().atMost(Duration.ofSeconds(2)).untilAsserted {
//            assertThat(saveButton.isEnabled).isFalse
//            assertThat(completeButton.isEnabled).isFalse
//        }
//
//
//        // test the required text field
//        val requiredTextField = taskEditor.`$`(TextFieldElement::class.java).id("textfield-textinput1")
//        requiredTextField.value = " "
//        taskEditor.`$`(TextAreaElement::class.java).id("textarea-multitextinput2").focus()
//
//        val errorMessage = `$`(DivElement::class.java).attribute("slot", "error-message").first()
//        await().atMost(Duration.ofSeconds(2)).untilAsserted {
//            assertThat(errorMessage.text).isEqualTo("Required field cannot be empty")
//        }
//
//
//        // enter data
//        val textField2 = taskEditor.`$`(TextFieldElement::class.java).id("textfield-textinput1")
//        textField2.value = "testforminput"
//        val textArea = taskEditor.`$`(TextAreaElement::class.java).id("textarea-multitextinput2")
//        textArea.value = "A long piece of text" + "\n" + "on multiple rows."
//        val integerField = taskEditor.`$`(IntegerFieldElement::class.java).id("integerfield-integerinput3")
//        integerField.value = "1234"
//        val checkbox = taskEditor.`$`(CheckboxElement::class.java).id("checkbox-booleaninput4")
//        checkbox.isChecked = true
//        val datePicker = taskEditor.`$`(DatePickerElement::class.java).id("datepicker-dateinput5")
//        datePicker.date = LocalDate.of(2022, 3, 30)
//
//
//        // check if buttons are enabled after entering text and save the task
//        val saveButton2 = `$`(ButtonElement::class.java).id("savebutton")
//        val completeButton2 = `$`(ButtonElement::class.java).id("completebutton")
//
//        await().atMost(Duration.ofSeconds(2)).untilAsserted {
//            assertThat(saveButton2.isEnabled).isTrue
//            assertThat(completeButton2.isEnabled).isTrue
//        }
//
//        val saveButton2b = taskEditor.`$`(ButtonElement::class.java).id("savebutton")
//        saveButton2b.click()
//
//
//        // check if data is correctly loaded
//        goto(location + "cmmntasks")
//        val taskRows = `$`(GridElement::class.java).id("datagrid-taskdto").rowCount
//
//        goto(location + "tasks/" + taskId)
//        val taskForm = `$`(FormLayoutElement::class.java).id("input-requirement")
//
//        val saveButton3 = `$`(ButtonElement::class.java).id("savebutton")
//        val completeButton3 = `$`(ButtonElement::class.java).id("completebutton")
//
//        await().atMost(Duration.ofSeconds(2)).untilAsserted {
//            // after loading the form data, but before changing the data
//            // save button should be disabled and complete button should be enabled
//            assertThat(saveButton3.isEnabled).isFalse
//            assertThat(completeButton3.isEnabled).isTrue
//
//            assertThat(taskForm.`$`(TextFieldElement::class.java).id("textfield-textinput1").value)
//                .isEqualTo("testforminput")
//            assertThat(taskForm.`$`(TextAreaElement::class.java).id("textarea-multitextinput2").value)
//                .isEqualTo("A long piece of text" + "\n" + "on multiple rows.")
//            assertThat(taskForm.`$`(IntegerFieldElement::class.java).id("integerfield-integerinput3").value)
//                .isEqualTo("1234")
//            assertThat(taskForm.`$`(CheckboxElement::class.java).id("checkbox-booleaninput4").isChecked).isTrue
//            assertThat(taskForm.`$`(DatePickerElement::class.java).id("datepicker-dateinput5").date)
//                .isEqualTo(LocalDate.of(2022, 3, 30))
//        }
//
//
//        // complete the task with formdata
//        val completeButton4 = taskForm.`$`(ButtonElement::class.java).id("completebutton")
//        taskForm.`$`(CheckboxElement::class.java).id("checkbox-booleaninput4").isChecked = false  // change some data
//        completeButton4.click()
//
//        goto(location + "cmmntasks")
//        assertThat(`$`(GridElement::class.java).id("datagrid-taskdto").rowCount).isEqualTo(taskRows - 1)
//            .`as`("1 cmmntask should have been removed")
//
//
//        // check if there are no loose ends when loading main page
//        goto(location)
//
//    }
//
//}