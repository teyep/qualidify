package com.qualidify.architecture

import com.structurizr.Workspace
import com.structurizr.io.plantuml.StructurizrPlantUMLWriter
import java.io.StringWriter

/**
 * Export the Structurizr (C4 diagram) to a Json applicable for PlantUML
 * At this moment it will export the json to the console
 *
 * @author Marcel Pot
 * @since 1.0
 */
class PlantUmlStructurizrExporter : StructurizrExporter {

    override fun export(workspace: Workspace) {
        workspace.views.views.forEach {
            val stringWriter = StringWriter()
            val plantUMLWriter = StructurizrPlantUMLWriter()
            plantUMLWriter.write(it, stringWriter)
        }
    }
}