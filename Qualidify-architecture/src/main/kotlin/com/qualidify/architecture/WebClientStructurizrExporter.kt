package com.qualidify.architecture

import com.structurizr.Workspace
import com.structurizr.api.StructurizrClient
import com.structurizr.api.StructurizrClientException
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component

/**
 * Export a Structurizr map of the architecture and sends it to
 * [Structurizr website]( https://structurizr.com/workspace/{structurizr.workspace_id})
 *
 *
 * Structurizr properties are saved in application.yaml
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Primary
@Component
class WebClientStructurizrExporter(val properties: StructurizrProperties) : StructurizrExporter {

    /**
     * Export a workspace
     * @param workspace the workspace to export
     */
    @Throws(StructurizrClientException::class)
    override fun export(workspace: Workspace) {
        val client = StructurizrClient(properties.key, properties.secret)
        client.putWorkspace(properties.workspaceId, workspace)
    }
}