package com.qualidify.architecture

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component

@Component
@PropertySource(value = ["classpath:structurizr.properties"])
@ConfigurationProperties(prefix = "structurizr")
class StructurizrProperties {
    var key: String = ""
    var secret: String = ""
    var workspaceId: Long = 0L

}
