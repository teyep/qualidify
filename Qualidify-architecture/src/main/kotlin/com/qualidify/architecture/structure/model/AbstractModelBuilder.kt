package com.qualidify.architecture.structure.model

import com.structurizr.model.*
import org.springframework.stereotype.Component

abstract class AbstractModelBuilder<T : Element>(protected val model: Model) {

    abstract fun build(): T

}

abstract class AbstractCustomElementModelBuilder(model: Model) : AbstractModelBuilder<CustomElement>(model) {
    abstract val name: String
    abstract val metadata: String
    abstract val description: String

    override fun build(): CustomElement {
        return model.addCustomElement(name, metadata, description)
    }
}

abstract class AbstractSoftwareSystemModelBuilder(model: Model) : AbstractModelBuilder<SoftwareSystem>(model) {
    abstract val name: String
    abstract val description: String


    override fun build(): SoftwareSystem {
        return model.addSoftwareSystem(name, description)
    }
}

abstract class AbstractContainerModelBuilder(val softwareSystem: SoftwareSystem) {

    abstract val name: String
    abstract val description: String
    abstract val technology: String?

    open fun build(): Container {
        return softwareSystem.addContainer(name, description, technology)
    }
}

abstract class AbstractPersonModelBuilder(model: Model) : AbstractModelBuilder<Person>(model) {
    abstract val name: String
    abstract val description: String

    fun build(name: String, description: String): Person {
        return model.addPerson(name, description)
    }

    override fun build(): Person {
        return build(name, description)
    }
}

@Component
class ModelSemaphore(
    customElements: List<CustomElement>,
    persons: List<Person>,
    softwareSystems: List<SoftwareSystem>,
    containers: List<Container>
)