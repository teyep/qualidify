package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames.BEAN_RESTCONTAINER
import com.structurizr.Workspace
import com.structurizr.model.Container
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn(value = ["modelSemaphore"])
class RestControllerContainerViewBuilder(
    workspace: Workspace,
    @Qualifier(BEAN_RESTCONTAINER) val restContainer: Container
) : AbstractNamedViewBuilder(workspace) {

    override val name = "RestController Components"
    override val description = "An Overview of the restcontrollers"

    override fun build() {
        views
            .createComponentView(
                restContainer as Container,
                name,
                description
            ).apply {
                addAllComponents()
            }
    }
}