package com.qualidify.architecture.structure.model

import com.qualidify.architecture.structure.model.BeanNames.BEAN_PERSON_ADMINISTRATOR
import com.qualidify.architecture.structure.model.BeanNames.BEAN_PERSON_PARTICIPANT
import com.qualidify.architecture.structure.model.BeanNames.BEAN_PERSON_PROJECTLEAD
import com.structurizr.model.Model
import com.structurizr.model.Person
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class PersonModelBuilder(model: Model) : AbstractPersonModelBuilder(model) {
    override val name = ""
    override val description = ""

    companion object {
        const val PERSON_ADMIN = "Administrator"
        const val PERSON_ADMIN_DESCRIPTION = "The administrator of the system"

        const val PERSON_LEAD = "Projectlead"
        const val PERSON_LEAD_DESCRIPTION = "The projectlead of a Project"

        const val PERSON_PARTICIPANT = "Participant"
        const val PERSON_PARTICIPANT_DESCRIPTION = "A participant of a Project"
    }

    @Bean(BEAN_PERSON_ADMINISTRATOR)
    open fun buildAdministrator(): Person {
        return super.build(PERSON_ADMIN, PERSON_ADMIN_DESCRIPTION)
    }

    @Bean(BEAN_PERSON_PROJECTLEAD)
    open fun buildProjectLead(): Person {
        return super.build(PERSON_LEAD, PERSON_LEAD_DESCRIPTION)
    }

    @Bean(BEAN_PERSON_PARTICIPANT)
    open fun buildParticipant(): Person {
        return super.build(PERSON_PARTICIPANT, PERSON_PARTICIPANT_DESCRIPTION)
    }
}