package com.qualidify.architecture.structure.model.containers

import com.qualidify.architecture.structure.QualidifyConstants.KOTLIN_SPRING
import com.qualidify.architecture.structure.StructurizrUtil
import com.qualidify.architecture.structure.model.AbstractContainerModelBuilder
import com.qualidify.architecture.structure.model.BeanNames
import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.qualidify.architecture.structure.model.BeanNames.BEAN_SERVICECONTAINER
import com.structurizr.analysis.ComponentFinder
import com.structurizr.analysis.ReferencedTypesSupportingTypesStrategy
import com.structurizr.analysis.SpringComponentFinderStrategy
import com.structurizr.model.Container
import com.structurizr.model.CustomElement
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ServiceContainerModelBuilder(
    @Qualifier(BeanNames.BEAN_QUALIDIFY_SERVER) qualidifyServer: SoftwareSystem
) : AbstractContainerModelBuilder(qualidifyServer) {

    override val name = "General Service"
    override val description = "Serves all processes(Cmmn/Bpmn) flows and also configuration"
    override val technology = KOTLIN_SPRING

    @Bean(BEAN_SERVICECONTAINER)
    open fun create(
        @Qualifier(BEAN_DATABASE) database: CustomElement
    ): Container {
        val container = super.build().apply {
            uses(database, "Persist and load data")
        }
        findComponents(container)
        StructurizrUtil.linkBasedOnConstructorArguments(container.components)
        StructurizrUtil.tagSpringComponents(container.components)
        return container
    }

    private fun findComponents(container: Container) {
        val componentFinder = ComponentFinder(
            container,
            "com.qualidify.service.impl",
            //QualidifySpringComponentFinderStrategy(
            SpringComponentFinderStrategy(
                ReferencedTypesSupportingTypesStrategy(),

                )
        )
        componentFinder.findComponents()
    }


}