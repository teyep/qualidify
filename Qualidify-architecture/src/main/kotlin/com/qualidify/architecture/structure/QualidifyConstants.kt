package com.qualidify.architecture.structure

object QualidifyConstants {
    const val QUALIDIFY_NAME = "Qualidify QMS"
    const val QUALIDIFY_DESCRIPTION = ""

    const val ANGULAR = "Angular"
    const val KOTLIN_SPRING = "Kotlin/Spring"
    const val KOTLIN_SPRINGBATCH = "Kotlin/SpringBatch"
    const val KOTLIN_FLOWABLE = "Kotlin/Flowable"

    const val SPRING_SERVICE = "Spring Service"
    const val SPRING_COMPONENT = "Spring Component"
    const val SPRING_REST_CONTROLLER = "Spring REST Controller"
    const val SPRING_REPOSITORY = "Spring Repository"
    const val SPRING_JOB = "Spring Batch Job"
    const val SPRING_JOB_STEP = "Spring Batch Job Step"
}


