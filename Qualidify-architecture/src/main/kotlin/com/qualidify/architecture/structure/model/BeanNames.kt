package com.qualidify.architecture.structure.model

object BeanNames {


    const val BEAN_DATABASE = "database"

    const val BEAN_PERSON_ADMINISTRATOR = "administrator"
    const val BEAN_PERSON_PARTICIPANT = "participant"
    const val BEAN_PERSON_PROJECTLEAD = "projectLead"

    const val BEAN_QUALIDIFY_SERVER = "qualidifyServer"
    const val BEAN_REPORTSERVER = "reportServer"

    const val BEAN_BATCHCONTAINER = "batchContainer"
    const val BEAN_METADATASERVICECONTAINER = "metadataServiceContainer"
    const val BEAN_SERVICECONTAINER = "serviceContainer"
    const val BEAN_RESTCONTAINER = "restContainer"
    const val BEAN_WEBCONTAINER = "webContainer"


}