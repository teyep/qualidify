package com.qualidify.architecture.structure.model

import com.qualidify.architecture.structure.Style
import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.qualidify.architecture.structure.model.BeanNames.BEAN_REPORTSERVER
import com.structurizr.model.CustomElement
import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ReportSoftwareSystemBuilder(model: Model) : AbstractSoftwareSystemModelBuilder(model) {
    override val name = "ReportServer"
    override val description = "Serving reporting requests"

    @Bean(BEAN_REPORTSERVER)
    open fun create(
        @Qualifier(BEAN_DATABASE) database: CustomElement
    ): SoftwareSystem {
        val reportServer = super.build().apply {
            addTags(Style.TAG_QUALIDIFY)
            uses(database, "Load reporttemplates, and persist the report instances")
        }
        return reportServer
    }
}