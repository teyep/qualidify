package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.qualidify.architecture.structure.model.BeanNames.BEAN_QUALIDIFY_SERVER
import com.structurizr.Workspace
import com.structurizr.model.CustomElement
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn(value = ["modelSemaphore"])
class QualidifySystemContainerViewBuilder(
    workspace: Workspace,
    @Qualifier(BEAN_DATABASE) val database: CustomElement,
    @Qualifier(BEAN_QUALIDIFY_SERVER) val qualidifySystem: SoftwareSystem,
) : AbstractNamedViewBuilder(workspace) {

    override val name = "Qualidify System Container"
    override val description = "An overview of the Qualidify Application"

    override fun build() {
        views.createContainerView(
            qualidifySystem,
            name,
            description
        ).apply {
            addAllContainers()
            add(database)

        }
    }
}