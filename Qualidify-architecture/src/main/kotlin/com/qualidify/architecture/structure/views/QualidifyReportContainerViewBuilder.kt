package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.qualidify.architecture.structure.model.BeanNames.BEAN_REPORTSERVER
import com.structurizr.Workspace
import com.structurizr.model.CustomElement
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn(value = ["modelSemaphore"])
class QualidifyReportContainerViewBuilder(
    workspace: Workspace,
    @Qualifier(BEAN_DATABASE) val database: CustomElement,
    @Qualifier(BEAN_REPORTSERVER) val reportServer: SoftwareSystem
) : AbstractNamedViewBuilder(workspace) {

    override val name = "Qualidify Reports Container"
    override val description = "An overview of the Qualidify Report Server"

    override fun build() {
        views.createContainerView(
            reportServer,
            name,
            description
        ).apply {
            add(database)
            addAllContainers()
        }
    }

}