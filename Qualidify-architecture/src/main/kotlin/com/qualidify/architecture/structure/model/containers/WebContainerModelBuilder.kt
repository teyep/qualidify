package com.qualidify.architecture.structure.model.containers

import com.qualidify.architecture.structure.Style.TAG_GUI
import com.qualidify.architecture.structure.model.AbstractContainerModelBuilder
import com.qualidify.architecture.structure.model.BeanNames.BEAN_QUALIDIFY_SERVER
import com.qualidify.architecture.structure.model.BeanNames.BEAN_RESTCONTAINER
import com.qualidify.architecture.structure.model.BeanNames.BEAN_WEBCONTAINER
import com.structurizr.model.Container
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class WebContainerModelBuilder(
    @Qualifier(BEAN_QUALIDIFY_SERVER) qualidifyServer: SoftwareSystem
) : AbstractContainerModelBuilder(qualidifyServer) {
    override val name = "GUI"
    override val description = "The graphical user interface"
    override val technology = "Angular"

    @Bean(BEAN_WEBCONTAINER)
    open fun create(
        @Qualifier(BEAN_RESTCONTAINER) restContainer: Container
    ): Container {
        return super.build().apply {
            uses(restContainer, "Request and push data over HTTP")
            addTags(TAG_GUI)
        }
    }
}