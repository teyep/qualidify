package com.qualidify.architecture.structure.model

import com.qualidify.architecture.structure.Style
import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.qualidify.architecture.structure.model.BeanNames.BEAN_PERSON_ADMINISTRATOR
import com.qualidify.architecture.structure.model.BeanNames.BEAN_PERSON_PARTICIPANT
import com.qualidify.architecture.structure.model.BeanNames.BEAN_PERSON_PROJECTLEAD
import com.qualidify.architecture.structure.model.BeanNames.BEAN_QUALIDIFY_SERVER
import com.qualidify.architecture.structure.model.BeanNames.BEAN_REPORTSERVER
import com.qualidify.service.configuration.QualidifyConstants
import com.structurizr.model.CustomElement
import com.structurizr.model.Model
import com.structurizr.model.Person
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class QualidifySoftwareSystemBuilder(model: Model) : AbstractSoftwareSystemModelBuilder(model) {
    override val name = QualidifyConstants.QUALIDIFY_NAME
    override val description = QualidifyConstants.QUALIDIFY_DESCRIPTION

    @Bean(BEAN_QUALIDIFY_SERVER)
    open fun create(
        @Qualifier(BEAN_REPORTSERVER) reportServer: SoftwareSystem,
        @Qualifier(BEAN_DATABASE) database: CustomElement,
        @Qualifier(BEAN_PERSON_ADMINISTRATOR) administrator: Person,
        @Qualifier(BEAN_PERSON_PROJECTLEAD) projectLead: Person,
        @Qualifier(BEAN_PERSON_PARTICIPANT) participant: Person,
    ): SoftwareSystem {

        val qualidifySoftwareSystem = super.build().apply {
            addTags(Style.TAG_QUALIDIFY)

            uses(reportServer, "Used for report requests")
            uses(database, "Loading and persisting all business data to support the business logic")


        }
        administrator.uses(qualidifySoftwareSystem, "Configure System")
        projectLead.uses(qualidifySoftwareSystem, "Creates and configure project")
        participant.uses(qualidifySoftwareSystem, "Participates in project and finish tasks")

        return qualidifySoftwareSystem
    }
}