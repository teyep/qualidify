package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames.BEAN_WEBCONTAINER
import com.structurizr.Workspace
import com.structurizr.model.Container
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn(value = ["modelSemaphore"])
class WebContainerViewBuilder(
    workspace: Workspace,
    @Qualifier(BEAN_WEBCONTAINER) val webContainer: Container
) : AbstractNamedViewBuilder(workspace) {

    override val name = "Web components"
    override val description = "An overview of the components used in the webinterface."


    override fun build() {
        views
            .createComponentView(
                webContainer as Container,
                name,
                description
            )
    }
}