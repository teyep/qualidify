package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames
import com.qualidify.service.configuration.QualidifyConstants
import com.structurizr.Workspace
import com.structurizr.model.SoftwareSystem
import com.structurizr.view.AutomaticLayout.RankDirection
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component


@Component
@DependsOn(value = ["modelSemaphore"])
class SystemLandscapeViewBuilder(
    workspace: Workspace,
    @Qualifier(BeanNames.BEAN_QUALIDIFY_SERVER) val qualidifySystem: SoftwareSystem
) : AbstractNamedViewBuilder(workspace) {

    override val name = "System Landscape"
    override val description = "An overview of the system landscape of ${QualidifyConstants.QUALIDIFY_NAME}."

    override fun build() {
        views
            .createSystemLandscapeView(
                name,
                description
            ).apply {
                addAllPeople()
                add(qualidifySystem)
                enableAutomaticLayout(RankDirection.TopBottom, 300, 600)
            }

    }
}