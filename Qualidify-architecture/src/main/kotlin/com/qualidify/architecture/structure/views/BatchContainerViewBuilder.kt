package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames
import com.structurizr.Workspace
import com.structurizr.model.Container
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn(value = ["modelSemaphore"])
class BatchContainerViewBuilder(
    workspace: Workspace,
    @Qualifier(BeanNames.BEAN_BATCHCONTAINER) val reportContainer: Container
) : AbstractNamedViewBuilder(workspace) {
    override val name = "Batch Components"
    override val description = "An overview of the Batch Job (reports) components"

    override fun build() {

        views.createComponentView(
            reportContainer,
            name,
            description
        ).apply {
            addAllComponents()
        }

    }
}