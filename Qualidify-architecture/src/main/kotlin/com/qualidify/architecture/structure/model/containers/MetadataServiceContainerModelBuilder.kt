package com.qualidify.architecture.structure.model.containers

import com.qualidify.architecture.structure.QualidifyConstants.KOTLIN_SPRING
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_COMPONENT
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_REPOSITORY
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_SERVICE
import com.qualidify.architecture.structure.StructurizrUtil
import com.qualidify.architecture.structure.model.AbstractContainerModelBuilder
import com.qualidify.architecture.structure.model.BeanNames
import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.qualidify.architecture.structure.model.BeanNames.BEAN_METADATASERVICECONTAINER
import com.structurizr.model.Container
import com.structurizr.model.CustomElement
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class MetadataServiceContainerModelBuilder(
    @Qualifier(BeanNames.BEAN_QUALIDIFY_SERVER) qualidifyServer: SoftwareSystem
) : AbstractContainerModelBuilder(qualidifyServer) {

    override val name = "Metadata Service"
    override val description = "Supplies all metadata information for dto's"
    override val technology = KOTLIN_SPRING

    @Bean(BEAN_METADATASERVICECONTAINER)
    open fun create(
        @Qualifier(BEAN_DATABASE) database: CustomElement
    ): Container {
        val container = super.build().apply {
            uses(database, "Persist and load meta data")
        }
        describeComponents(container)
        StructurizrUtil.tagSpringComponents(container.components)
        return container
    }

    private fun describeComponents(container: Container) {
        val metadata = container.addComponent(
            "Metadata",
            "Singleton",
            "Static Helper class",
            "Static"
        )

        val metadataValidationService = container.addComponent(
            "MetadataValidationService",
            "Service",
            "Service for validation of Dto's based on their metadata.",
            SPRING_SERVICE
        )

        val metadataService =
            container.addComponent(
                "MetadataService",
                "Service",
                "Service for metadata questions",
                SPRING_SERVICE
            )
        val metadataRepository =
            container.addComponent(
                "MetadataRepository",
                "Repository",
                "Repository for metadata",
                SPRING_REPOSITORY
            )
        val customFieldRepository =
            container.addComponent(
                "CustomFieldRepository",
                "Repository",
                "Repository for custom fields (user defined configuration to add extra information on dto's",
                SPRING_REPOSITORY
            )

        val hasIdentityService =
            container.addComponent(
                "HasIdentityService",
                "Service",
                "Default CRUD operations on Dto's implenting the HasIdentity interface",
                SPRING_SERVICE
            )
        val dtoMapperLibrary = container.addComponent(
            "dtoMapperLibrary",
            "Component",
            "Collection of all Dto-Entity Mappers defined in the classpath (and annotated as spring bean)",
            SPRING_COMPONENT
        )
        val customMetadataEntityRepository = container.addComponent(
            "customMetadataEntityRepository",
            "Repository",
            "Repository for persisting 'Has-Identity'-Entities",
            SPRING_REPOSITORY

        )

        val objectCreator = container.addComponent(
            "ObjectCreator",
            "Component",
            "Creator of objects based on their definition",
            SPRING_COMPONENT
        )
        val objectConverter = container.addComponent(
            "ObjectConverter",
            "Component",
            "Converter of objects based on their defintion",
            SPRING_COMPONENT
        )


        metadata.uses(metadataService, "Uses")
        metadata.uses(metadataValidationService, "Uses")
        metadataValidationService.uses(metadataService, "Uses")
        metadataService.uses(metadataRepository, "Uses")
        metadataService.uses(customFieldRepository, "Uses")

        hasIdentityService.uses(dtoMapperLibrary, "Uses")
        hasIdentityService.uses(metadataValidationService, "Uses")
        hasIdentityService.uses(customMetadataEntityRepository, "Uses")
    }


}