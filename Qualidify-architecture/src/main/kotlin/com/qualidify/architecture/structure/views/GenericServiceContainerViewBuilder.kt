package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames.BEAN_SERVICECONTAINER
import com.structurizr.Workspace
import com.structurizr.model.Container
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn(value = ["modelSemaphore"])
class GenericServiceContainerViewBuilder(
    workspace: Workspace,
    @Qualifier(BEAN_SERVICECONTAINER) val genericServiceContainer: Container
) : AbstractNamedViewBuilder(workspace) {
    override val name = "Process Service Components"
    override val description =
        "An Overview of the general services - Acting, validating and configuring the business process"


    override fun build() {

        views
            .createComponentView(
                genericServiceContainer as Container,
                name,
                description
            )
            .apply {
                addAllComponents()
            }
    }
}