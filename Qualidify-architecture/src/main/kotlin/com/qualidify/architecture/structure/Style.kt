package com.qualidify.architecture.structure

import com.structurizr.Workspace
import com.structurizr.model.Tags
import com.structurizr.util.ImageUtils
import com.structurizr.view.Border
import com.structurizr.view.Branding
import com.structurizr.view.Font
import com.structurizr.view.Shape
import org.springframework.core.io.ClassPathResource

object Style {

    const val COLOR_DARK_GREEN = "#108136"
    const val COLOR_OCHER = "#fdc301"
    const val COLOR_BLUE = "#4287f5"
    const val COLOR_LIGHT_BLUE = "#87CEEB"
    const val COLOR_GRAY = "#878787"

    const val TAG_OPACITY_50 = "50%"
    const val TAG_QUALIDIFY = "Qualidify"
    const val TAG_DB = "Database"
    const val TAG_OTHER = "OTHER"
    const val TAG_GUI = "GUI"
    const val TAG_LIB = "Library"

    const val TAG_SPRING_REST = "Spring RestController"
    const val TAG_SPRING_COMPONENT = "Spring Component"
    const val TAG_SPRING_SERVICE = "Spring Service"
    const val TAG_SPRING_REPOSITORY = "Spring Repository"
    const val TAG_SPRING_JOB = "Spring Batch Job"
    const val TAG_SPRING_JOB_STEP = "Spring Batch Job Step"


    private fun branding(workspace: Workspace) {
        val logoResource = ClassPathResource("/images/Qualidify-logo.png")
        val logoFile = logoResource.file
        val branding: Branding = workspace.views.configuration.branding

        branding.font = Font("Trebuchet MS")
        branding.logo = ImageUtils.getImageAsDataUri(logoFile)

    }

    fun execute(workspace: Workspace) {
        branding(workspace)

        val styles = workspace.views.configuration.styles

        styles.addElementStyle(Tags.ELEMENT)
            .color("#000000")

        styles.addElementStyle(TAG_QUALIDIFY)
            .background(COLOR_DARK_GREEN)
            .color(COLOR_OCHER)

        styles.addElementStyle(Tags.PERSON)
            .background(COLOR_OCHER)
            .color(COLOR_DARK_GREEN)
            .border(Border.Solid)
            .shape(Shape.Person)

        styles.addElementStyle(TAG_OPACITY_50)
            .opacity(50)

        styles.addElementStyle(Tags.SOFTWARE_SYSTEM)
            .shape(Shape.Box)

        styles
            .addElementStyle(Tags.CONTAINER)
            .background(COLOR_DARK_GREEN)

        styles.addElementStyle(TAG_DB)
            .background(COLOR_BLUE)
            .shape(Shape.Cylinder)

        styles.addElementStyle(TAG_OTHER)
            .background(COLOR_GRAY)
            .shape(Shape.RoundedBox)

        styles.addElementStyle(TAG_GUI)
            .shape(Shape.WebBrowser)
            .width(250 * 5)

        styles.addElementStyle(TAG_SPRING_SERVICE)
            .stroke(COLOR_DARK_GREEN)

        styles.addElementStyle(TAG_SPRING_COMPONENT)
            .stroke(COLOR_DARK_GREEN)
            .border(Border.Dotted)

        styles.addElementStyle(TAG_SPRING_REST)
            .stroke(COLOR_LIGHT_BLUE)

        styles.addElementStyle(TAG_SPRING_REPOSITORY)
            .stroke("#0000FF")
            .border(Border.Dashed)

        styles.addElementStyle(TAG_SPRING_JOB)
            .shape(Shape.Hexagon)
            .stroke("#BA41B6")
            .border(Border.Solid)

        styles.addElementStyle(TAG_SPRING_JOB_STEP)
            .shape(Shape.Diamond)
            .stroke("#ECA5E3")
            .border(Border.Dashed)

        styles.addElementStyle(TAG_LIB)
            .stroke("#FF0000")
            .shape(Shape.RoundedBox)

        styles.addRelationshipStyle(Tags.ASYNCHRONOUS).dashed(true)
        styles.addRelationshipStyle(Tags.SYNCHRONOUS).dashed(false)
    }

}