package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames.BEAN_METADATASERVICECONTAINER
import com.structurizr.Workspace
import com.structurizr.model.Container
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn(value = ["modelSemaphore"])
class MetadataServiceContainerViewBuilder(
    workspace: Workspace,
    @Qualifier(BEAN_METADATASERVICECONTAINER) val metadataServiceContainer: Container
) : AbstractNamedViewBuilder(workspace) {
    override val name = "Metadata Service Components"
    override val description =
        "An Overview of the metadata service - Adding display/verification information for dto's."


    override fun build() {
        views
            .createComponentView(
                metadataServiceContainer,
                name,
                description
            )
            .apply {
                addAllComponents()
            }
    }
}