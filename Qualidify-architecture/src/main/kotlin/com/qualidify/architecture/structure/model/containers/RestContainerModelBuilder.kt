package com.qualidify.architecture.structure.model.containers

import com.qualidify.architecture.structure.QualidifyConstants.KOTLIN_SPRING
import com.qualidify.architecture.structure.model.AbstractContainerModelBuilder
import com.qualidify.architecture.structure.model.BeanNames
import com.qualidify.architecture.structure.model.BeanNames.BEAN_METADATASERVICECONTAINER
import com.qualidify.architecture.structure.model.BeanNames.BEAN_RESTCONTAINER
import com.qualidify.architecture.structure.model.BeanNames.BEAN_SERVICECONTAINER
import com.structurizr.analysis.ComponentFinder
import com.structurizr.analysis.SpringRestControllerComponentFinderStrategy
import com.structurizr.model.Container
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class RestContainerModelBuilder(
    @Qualifier(BeanNames.BEAN_QUALIDIFY_SERVER) qualidifyServer: SoftwareSystem
) : AbstractContainerModelBuilder(qualidifyServer) {

    override val name = "RestController"
    override val description =
        "The REST controller of the system, delivers all information by the REST-protocol over http"
    override val technology = KOTLIN_SPRING

    @Bean(BEAN_RESTCONTAINER)
    open fun create(
        @Qualifier(BEAN_METADATASERVICECONTAINER) metadataServiceContainer: Container,
        @Qualifier(BEAN_SERVICECONTAINER) genericServiceContainer: Container
    ): Container {
        val container = super.build().apply {
            uses(metadataServiceContainer, "Pass requests for Metadata")
            uses(genericServiceContainer, "Pass requests for Projects")
        }

        val componentFinder = ComponentFinder(
            container, "com.qualidify.rest",
            SpringRestControllerComponentFinderStrategy()
        )
        componentFinder.findComponents()
        return container
    }

}