package com.qualidify.architecture.structure.views

import com.structurizr.Workspace
import org.springframework.context.annotation.DependsOn
import javax.annotation.PostConstruct

@DependsOn(value = ["modelSemaphore"])
abstract class AbstractViewBuilder(workspace: Workspace) {
    protected val model = workspace.model
    protected val views = workspace.views


    @PostConstruct
    abstract fun build()
}

abstract class AbstractNamedViewBuilder(workspace: Workspace) : AbstractViewBuilder(workspace) {
    abstract val name: String
    abstract val description: String

}