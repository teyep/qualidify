package com.qualidify.architecture.structure.model.containers

import com.qualidify.architecture.structure.QualidifyConstants.KOTLIN_SPRINGBATCH
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_COMPONENT
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_JOB
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_JOB_STEP
import com.qualidify.architecture.structure.StructurizrUtil
import com.qualidify.architecture.structure.model.AbstractContainerModelBuilder
import com.qualidify.architecture.structure.model.BeanNames
import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.structurizr.model.Container
import com.structurizr.model.CustomElement
import com.structurizr.model.InteractionStyle
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class BatchContainerModelBuilder(
    @Qualifier(BeanNames.BEAN_REPORTSERVER) reportServer: SoftwareSystem
) : AbstractContainerModelBuilder(reportServer) {
    override val name = "Report Service"
    override val description = "Create Reports of ReportTemplates"
    override val technology = KOTLIN_SPRINGBATCH

    @Bean(BeanNames.BEAN_BATCHCONTAINER)
    open fun create(
        @Qualifier(BEAN_DATABASE) database: CustomElement
    ): Container {
        val container = super.build().apply {
            uses(database, "Loading templates, persisting reportinstances")
        }
        describeComponents(container)
        StructurizrUtil.tagSpringComponents(container.components)
        return container
    }

    private fun describeComponents(container: Container) {
        val jobRegistry = container.addComponent(
            "QualidifyJobRegistry",
            "Repository",
            "A runtime service registry interface for registering job configurations by name.",
            KOTLIN_SPRINGBATCH
        )
        val jobLauncher = container.addComponent(
            "JobLauncher",
            "Service",
            "Starts a Job",
            SPRING_COMPONENT
        )

        val jasperBatchStarter = container.addComponent(
            "JasperBatchStarter",
            "Service",
            "Starts Jasper Report job",
            SPRING_COMPONENT
        )

        val processJasperReportJob = container.addComponent(
            "ProcessJasperReportJob",
            "Processor",
            "Process a Jasper Report Job",
            SPRING_COMPONENT
        )
        val jasperReportStep = container.addComponent(
            "JasperReportStep",
            "Flow",
            "Describing the steps to take to fullfill a Jasper Report Job",
            SPRING_JOB
        )
        val batchJobReaderReader = container.addComponent(
            "BatchJobReader",
            "Step [StepScope]",
            "Reads input values from the database",
            SPRING_JOB_STEP
        )
        val jasperReportProcessor = container.addComponent(
            "JasperReportProcessor",
            "Step [StepScope]",
            "Creating the report instance based on input",
            SPRING_JOB_STEP
        )
        val reportWriter = container.addComponent(
            "ReportWriter",
            "Step [StepScope]",
            "Writes the report to the database",
            SPRING_JOB_STEP
        )

        jasperBatchStarter.uses(jobLauncher, "")
        jasperBatchStarter.uses(jobRegistry, "")

        jobLauncher.uses(processJasperReportJob, "", "", InteractionStyle.Asynchronous)
        processJasperReportJob.uses(jasperReportStep, "")

        jasperReportStep.uses(batchJobReaderReader, "Read input")
        jasperReportStep.uses(jasperReportProcessor, "Process report")
        jasperReportStep.uses(reportWriter, "Write output")

    }
}