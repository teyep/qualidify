package com.qualidify.architecture.structure

import com.qualidify.architecture.WebClientStructurizrExporter
import com.qualidify.architecture.structure.views.AbstractViewBuilder
import com.qualidify.service.configuration.QualidifyConstants
import com.structurizr.Workspace
import com.structurizr.model.Enterprise
import com.structurizr.model.Model
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order

/**
 * Configuration for the architecture
 * @author Marcel Pot
 * @since 1.0 QUAL-476
 */
@Configuration
open class ArchitectureConfiguration {
    /**
     * Defines a Structurizr workspace for Qualidify
     * @return a workSpace bean
     */
    @Bean
    open fun workspace(): Workspace =
        Workspace(QualidifyConstants.QUALIDIFY_NAME, QualidifyConstants.QUALIDIFY_DESCRIPTION)

    @Bean
    open fun model(): Model = workspace().model

    @Bean
    open fun enterprise(): Enterprise {
        val enterprise = Enterprise(QualidifyConstants.QUALIDIFY_NAME)
        model().enterprise = enterprise
        return enterprise
    }
}


/**
 * Export configuration for Structurizr
 * @author Marcel Pot
 * @since 1.0 QUAL-476
 */
@Configuration
@Order(Ordered.LOWEST_PRECEDENCE)
open class WorkspaceExportConfiguration {

    /**
     * Exports the views to Structurizr
     * @param[workspace] The workspace
     * @param[exporter] The exporter
     * @param[views] A list of all views (added to make sure all views are processed by spring context)
     */
    @Autowired
    open fun exporter(workspace: Workspace, exporter: WebClientStructurizrExporter, views: List<AbstractViewBuilder>) {
        Style.execute(workspace)
        exporter.export(workspace)
    }
}