package com.qualidify.architecture.structure.views

import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.qualidify.architecture.structure.model.BeanNames.BEAN_QUALIDIFY_SERVER
import com.qualidify.service.configuration.QualidifyConstants
import com.structurizr.Workspace
import com.structurizr.model.CustomElement
import com.structurizr.model.SoftwareSystem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.DependsOn
import org.springframework.stereotype.Component

@Component
@DependsOn(value = ["modelSemaphore"])
class SystemContextViewBuilder(
    workspace: Workspace,
    @Qualifier(BEAN_DATABASE) val database: CustomElement,
    @Qualifier(BEAN_QUALIDIFY_SERVER) val qualidifySystem: SoftwareSystem

) : AbstractNamedViewBuilder(workspace) {

    override val name = "Context View"
    override val description = "An overview of the inner architecture of ${QualidifyConstants.QUALIDIFY_NAME}."

    override fun build() {
        views.createSystemContextView(
            qualidifySystem,
            name,
            description
        ).apply {
            addAllElements()
            add(database)
        }

    }
}