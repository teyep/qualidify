package com.qualidify.architecture.structure.model

import com.qualidify.architecture.structure.Style
import com.qualidify.architecture.structure.model.BeanNames.BEAN_DATABASE
import com.structurizr.model.CustomElement
import com.structurizr.model.Model
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
open class DatabaseModelBuilder(model: Model) : AbstractCustomElementModelBuilder(model) {
    override val name = "Database"
    override val metadata = "Storage of data"
    override val description = ""

    @Bean(BEAN_DATABASE)
    override fun build(): CustomElement {
        return super.build()
            .apply { addTags(Style.TAG_DB) }
    }
}