package com.qualidify.architecture.structure

import com.qualidify.architecture.structure.QualidifyConstants.SPRING_COMPONENT
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_JOB
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_JOB_STEP
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_REPOSITORY
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_REST_CONTROLLER
import com.qualidify.architecture.structure.QualidifyConstants.SPRING_SERVICE
import com.qualidify.architecture.structure.Style.TAG_SPRING_COMPONENT
import com.qualidify.architecture.structure.Style.TAG_SPRING_JOB
import com.qualidify.architecture.structure.Style.TAG_SPRING_JOB_STEP
import com.qualidify.architecture.structure.Style.TAG_SPRING_REPOSITORY
import com.qualidify.architecture.structure.Style.TAG_SPRING_REST
import com.qualidify.architecture.structure.Style.TAG_SPRING_SERVICE
import com.structurizr.model.Component
import kotlin.reflect.full.primaryConstructor


object StructurizrUtil {


    fun linkBasedOnConstructorArguments(components: Set<Component>) {
        for (component in components) {
            val implementation = component.code.asSequence()
                .filter { t -> t.category != "interface" }
                .firstOrNull()
            if (implementation != null) {
                val implementationClass = Class.forName(implementation.type).kotlin
                val constructArguments = implementationClass.primaryConstructor?.parameters
                constructArguments?.asSequence()
                    ?.map { it.name!! }
                    ?.forEach {
                        val usedComponent =
                            components.asSequence().filter { c -> c.name.equals(it, true) }.firstOrNull()
                        if (usedComponent != null) {
                            component.uses(usedComponent, "")
                        }

                    }
            }
        }
    }

    fun tagSpringComponents(components: Set<Component>) {
        components.asSequence()
            .filter { it.getTechnology().equals(SPRING_REST_CONTROLLER) }
            .forEach { it.addTags(TAG_SPRING_REST) }
        components.asSequence()
            .filter { it.getTechnology().equals(SPRING_COMPONENT) }
            .forEach { it.addTags(TAG_SPRING_COMPONENT) }
        components.asSequence()
            .filter { it.getTechnology().equals(SPRING_SERVICE) }
            .forEach { it.addTags(TAG_SPRING_SERVICE) }
        components.asSequence()
            .filter { it.getTechnology().equals(SPRING_REPOSITORY) }
            .forEach { it.addTags(TAG_SPRING_REPOSITORY) }
        components.asSequence()
            .filter { it.getTechnology().equals(SPRING_JOB) }
            .forEach { it.addTags(TAG_SPRING_JOB) }
        components.asSequence()
            .filter { it.getTechnology().equals(SPRING_JOB_STEP) }
            .forEach { it.addTags(TAG_SPRING_JOB_STEP) }
    }
}