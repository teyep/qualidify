package com.qualidify.architecture

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration


@ComponentScan(
//    useDefaultFilters = false,
//    includeFilters = [
//        ComponentScan.Filter(
//            type = FilterType.ASSIGNABLE_TYPE,
//            classes = [
//                StructurizrProperties::class,
//                QualidifyStructurizrRunner::class,
//                WebClientStructurizrExporter::class]
//        )],
    basePackages = ["com.qualidify.architecture"]
)
@EnableConfigurationProperties()
@Configuration
open class QualidifyArchitectureApplication

fun main(args: Array<String>) {
    runApplication<QualidifyArchitectureApplication>(*args)
}
