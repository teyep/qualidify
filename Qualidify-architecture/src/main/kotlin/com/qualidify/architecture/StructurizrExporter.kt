package com.qualidify.architecture

import com.structurizr.Workspace
import com.structurizr.api.StructurizrClientException

/**
 * An interface for Exporting the C4 diagrams
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface StructurizrExporter {
    /**
     * Export a workspace
     * @param workspace the workspace to export
     */
    @Throws(StructurizrClientException::class)
    fun export(workspace: Workspace)
}