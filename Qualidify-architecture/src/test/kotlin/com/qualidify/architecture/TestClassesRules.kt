package com.qualidify.architecture

import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchIgnore
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.library.GeneralCodingRules

@AnalyzeClasses(packages = ["com.qualidify.."])
class TestClassesRules {

    @ArchTest
    @ArchIgnore
    //Does not work, because test classes are not mvn packaged yet
    private val testClassesShouldHaveSamePackageAsImplementation =
        GeneralCodingRules.testClassesShouldResideInTheSamePackageAsImplementation()


}