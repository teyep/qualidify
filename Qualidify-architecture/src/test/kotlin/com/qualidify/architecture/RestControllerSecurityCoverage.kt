package com.qualidify.architecture

import com.tngtech.archunit.base.DescribedPredicate
import com.tngtech.archunit.core.domain.JavaClass
import com.tngtech.archunit.core.domain.JavaMember
import com.tngtech.archunit.core.domain.PackageMatchers
import com.tngtech.archunit.core.domain.properties.CanBeAnnotated.Predicates.annotatedWith
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchIgnore
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.lang.conditions.ArchPredicates
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition
import org.springframework.security.access.prepost.PreAuthorize


@AnalyzeClasses(packages = ["com.qualidify.rest.."])
@ArchIgnore
class RestControllerSecurityCoverage {
    val restPackageMatcher = "..rest.."

    @ArchTest()
    val controllersShouldOnlyCallSecuredMethods: ArchRule =
        ArchRuleDefinition.classes().that().resideInAPackage(restPackageMatcher)
            .should()
            .onlyCallMethodsThat(areDeclaredInController().or(ArchPredicates.are(annotatedWith(PreAuthorize::class.java))))

    @ArchTest
    val controllersShouldOnlyCallSecuredConstructors: ArchRule = ArchRuleDefinition.classes()
        .that().resideInAPackage(restPackageMatcher)
        .should()
        .onlyCallConstructorsThat(areDeclaredInController().or(ArchPredicates.are(annotatedWith(PreAuthorize::class.java))))

    @ArchTest
    val controllersShouldOnlyCallSecuredCodeUnits: ArchRule = ArchRuleDefinition.classes()
        .that().resideInAPackage(restPackageMatcher)
        .should()
        .onlyCallCodeUnitsThat(areDeclaredInController().or(ArchPredicates.are(annotatedWith(PreAuthorize::class.java))))

    @ArchTest
    val controllersShouldOnlyCallSecuredFields: ArchRule = ArchRuleDefinition.classes()
        .that().resideInAPackage(restPackageMatcher)
        .should()
        .onlyAccessFieldsThat(areDeclaredInController().or(ArchPredicates.are(annotatedWith(PreAuthorize::class.java))))

    @ArchTest
    val controllersShouldOnlyCallSecuredMembers: ArchRule = ArchRuleDefinition.classes()
        .that().resideInAPackage(restPackageMatcher)
        .should()
        .onlyAccessMembersThat(areDeclaredInController().or(ArchPredicates.are(annotatedWith(PreAuthorize::class.java))))

    private fun areDeclaredInController(): DescribedPredicate<JavaMember?> {
        val aPackageController =
            JavaClass.Functions.GET_PACKAGE_NAME.`is`(PackageMatchers.of(restPackageMatcher, "java.."))
                .`as`("a rest controller package '..rest..'")
        return ArchPredicates.are(JavaMember.Predicates.declaredIn(aPackageController))
    }

}