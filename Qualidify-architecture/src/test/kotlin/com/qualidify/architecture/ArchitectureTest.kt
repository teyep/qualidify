package com.qualidify.architecture

import com.qualidify.metadata.annotations.DtoMetadata
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses
import com.tngtech.archunit.library.Architectures
import org.springframework.web.bind.annotation.RestController
import javax.persistence.Entity


/**
 * Define architecture constraints on the project and test them with this unit test
 * @author Marcel Pot
 * @since 1.0
 */
@AnalyzeClasses(packages = ["com.qualidify.."])
class ArchitectureTest {

    @ArchTest
    private val layerDependenciesAreRespected: ArchRule =
        Architectures.layeredArchitecture().consideringOnlyDependenciesInLayers()
//            .layer("Gui").definedBy("com.qualidify.gui..")
            .layer("RestControllers").definedBy("com.qualidify.rest..")
            .layer("Services").definedBy("com.qualidify.service..")
            .layer("Model").definedBy("com.qualidify.model..")
//            .whereLayer("Gui").mayNotBeAccessedByAnyLayer()
            .whereLayer("RestControllers").mayNotBeAccessedByAnyLayer()
            .whereLayer("Services").mayOnlyBeAccessedByLayers("RestControllers")  //, "Gui")
            .whereLayer("Model").mayOnlyBeAccessedByLayers("Services")

    @ArchTest
    private val metadataLayerDependenciesAreRespected: ArchRule =
        Architectures.layeredArchitecture().consideringOnlyDependenciesInLayers()
//            .layer("Gui").definedBy("com.qualidify.metadata.gui..")
            .layer("RestControllers").definedBy("com.qualidify.metadata.rest..")
            .layer("Services").definedBy("com.qualidify.metadata.service..")
//            .whereLayer("Gui").mayNotBeAccessedByAnyLayer()
            .whereLayer("RestControllers").mayNotBeAccessedByAnyLayer()
            .whereLayer("Services").mayOnlyBeAccessedByLayers("RestControllers")  //, "Gui")

    @ArchTest
    private val entitiesMustResideInModelPackage =
        classes().that().areAnnotatedWith(Entity::class.java)
            .should().resideInAPackage("..model..")
            .`as`("Entities should reside in a package '..model..'")

    @ArchTest
    private val dtosMustResideInServicePackage =
        classes().that().areAnnotatedWith(DtoMetadata::class.java)
            .should().resideInAPackage("..service..").orShould().resideInAPackage("..annotations..")
            .`as`("Dto's should reside in a package '..service..'")


    @ArchTest
    private val restControllersMustResideInPackage =
        classes().that().areAnnotatedWith(RestController::class.java)
            .should().resideInAPackage("..rest..")
            .`as`("RestControllers must reside in package '..rest..'")

    @ArchTest
    private val restControllersShouldNotUseServiceImplementations =
        noClasses().that().areAnnotatedWith(RestController::class.java)
            .should().dependOnClassesThat().resideInAPackage("..service.impl..")
            .`as`("RestControllers should not use service-implementation packages")

//    @ArchTest
//    private val viewsShouldNotUseServices =
//        noClasses().that().resideInAPackage("com.qualidify.gui.views..")
//            .and().haveNameMatching(".*View")
//            .should().dependOnClassesThat().haveNameMatching(".*Service")
//            .andShould().resideInAPackage("com.qualidify.service..")

    @ArchTest
    private val servicesShouldNotAccessRestControllers =
        noClasses().that().resideInAPackage("..service..")
            .should().accessClassesThat().resideInAPackage("..rest..")


    @ArchTest
    private val servicesShouldNotDependOnRestControllers: ArchRule =
        noClasses().that().resideInAPackage("..service..")
            .should().dependOnClassesThat().resideInAPackage("..rest..")
}