package com.qualidify.architecture

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition
import com.tngtech.archunit.library.GeneralCodingRules

@AnalyzeClasses(packages = ["com.qualidify.."])
class CodingRules {

    @ArchTest
    private val noAccessToStandardStreams = GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS

    @ArchTest
    private fun noAccessToStandardStreamMethods(classes: JavaClasses) {
        ArchRuleDefinition.noClasses().should(GeneralCodingRules.ACCESS_STANDARD_STREAMS).check(classes)
    }

    @ArchTest
    private val noGenericExceptionsUsed = GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS

    @ArchTest
    private val noJavaUtilLoggingIsUsed = GeneralCodingRules.NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING

    @ArchTest
    private val jodaTimeIsNotUsedInProject = GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME

    @ArchTest
    private val noAutowiredFieldsAreUsed = GeneralCodingRules.NO_CLASSES_SHOULD_USE_FIELD_INJECTION
}