package com.qualidify.batch.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.qualidify.batch.repository.BatchJobArtifactRepository
import com.qualidify.batch.repository.BatchJobRepository
import com.qualidify.batch.repository.ReportTemplateRepository
import com.qualidify.batch.service.BatchJobServiceImpl
import com.qualidify.batch.service.ReportServiceImpl
import com.qualidify.batch.starter.BatchStarter
import net.sf.jasperreports.repo.RepositoryService
import org.springframework.batch.core.configuration.JobRegistry
import org.springframework.batch.core.launch.JobLauncher
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import java.util.*
import javax.sql.DataSource

@Suppress("SpringJavaInjectionPointsAutowiringInspection")
@TestConfiguration
@EnableJpaRepositories
open class JobContextConfiguration {

    @Bean
    open fun objectMapper() = ObjectMapper()

    @Bean
    open fun batchStarter(
        jobLauncher: JobLauncher,
        jobRegistry: JobRegistry
    ) = BatchStarter(jobLauncher, jobRegistry)

    @Bean
    open fun reportService(
        reportTemplateRepository: ReportTemplateRepository,
        artifactRepository: BatchJobArtifactRepository,
        resourceLoaders: List<RepositoryService>,
        dataSource: DataSource
    ) = ReportServiceImpl(reportTemplateRepository, artifactRepository, resourceLoaders, dataSource)

    @Bean
    open fun batchJobService(batchJobRepository: BatchJobRepository) = BatchJobServiceImpl(batchJobRepository)

    @Bean
    open fun auditorAware(): AuditorAware<String> {
        return AuditorAware { Optional.of("<Job>UnitTester") }
    }

}
