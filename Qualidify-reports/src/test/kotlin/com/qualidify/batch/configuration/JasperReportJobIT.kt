package com.qualidify.batch.configuration

import com.qualidify.batch.service.BatchJobServiceImpl
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.batch.core.ExitStatus
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.test.JobLauncherTestUtils
import org.springframework.batch.test.JobRepositoryTestUtils
import org.springframework.batch.test.context.SpringBatchTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener
import org.springframework.test.context.support.DirtiesContextTestExecutionListener

@Suppress("SpringJavaInjectionPointsAutowiringInspection")
@SpringBatchTest
@EnableAutoConfiguration(exclude = [LiquibaseAutoConfiguration::class])
@ContextConfiguration(
    classes = [
        BatchConfiguration::class,
        JobContextConfiguration::class]
)
@TestExecutionListeners(
    DependencyInjectionTestExecutionListener::class,
    DirtiesContextTestExecutionListener::class
)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
internal class JasperReportJobIT {

    @Autowired
    lateinit var jobLauncherTestUtils: JobLauncherTestUtils

    @Autowired
    lateinit var jobRepositoryTestUtils: JobRepositoryTestUtils

    @Autowired
    lateinit var batchJobService: BatchJobServiceImpl

    @AfterEach
    fun clean() {
        jobRepositoryTestUtils.removeJobExecutions()
    }

    @Test
    fun `Run jasperReportJob without parameters will fail`() {

        val jobExecution = jobLauncherTestUtils.launchJob()
        val jobInstance = jobExecution.jobInstance
        val exitStatus = jobExecution.exitStatus

        assertThat(jobInstance.jobName).isEqualTo("JasperReportJob")  // TODO: fix in QUAL-435
        assertThat(exitStatus).isEqualTo(ExitStatus.FAILED)
    }

    @Test
    fun `Run jasperReportJob End to End`() {
        val batchJob = batchJobService.createBatchJob("testBatchJob", mapOf("test" to "uftu"))

        val jobParameters = JobParametersBuilder()
            .addString("id", batchJob.id.unbox().toString()).toJobParameters()

        val jobExecution = jobLauncherTestUtils.launchJob(jobParameters)
        val jobInstance = jobExecution.jobInstance
        val exitStatus = jobExecution.exitStatus

        assertThat(jobInstance.jobName).isEqualTo("JasperReportJob")  // TODO: fix in QUAL-435
        assertThat(exitStatus).isEqualTo(ExitStatus.FAILED)
    }

}

