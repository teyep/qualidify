package com.qualidify.batch.starter

import com.qualidify.batch.service.BatchJobServiceImpl
import com.qualidify.model.entities.BatchJob
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
internal class BatchDatabasePollerTest {
    @Mock
    lateinit var batchJobService: BatchJobServiceImpl

    @Mock
    lateinit var batchStarter: BatchStarter

    @InjectMocks
    lateinit var fixture: BatchDatabasePoller

    @Test
    fun `Test DatabasePoller where no job can be found`() {
        val batchJob = DatabasePollerTestEntities.batchJob()

        Mockito.doReturn(
            emptyList<BatchJob>()
        ).`when`(batchJobService).getAvailableBatchJobs(1)

        fixture.poll()

        verify(batchJobService, times(1)).getAvailableBatchJobs(1)
        verify(batchStarter, times(0)).startJob(batchJob)
    }

    @Test
    fun `Test DatabasePoller where one job can be found`() {
        val batchJob = DatabasePollerTestEntities.batchJob()

        Mockito.doReturn(
            listOf(batchJob)
        ).`when`(batchJobService).getAvailableBatchJobs(1)

        Mockito.doReturn(DatabasePollerTestEntities.jobExecution())
            .`when`(batchStarter).startJob(batchJob)

        fixture.poll()

        verify(batchJobService, times(1)).getAvailableBatchJobs(1)
        verify(batchStarter, times(1)).startJob(batchJob)
    }

    @Test
    fun `Test DatabasePoller where multiple jobs can be found`() {
        val batchJob = DatabasePollerTestEntities.batchJob()

        Mockito.doReturn(
            listOf(batchJob, batchJob, batchJob)
        ).`when`(batchJobService).getAvailableBatchJobs(3)

        Mockito.doReturn(DatabasePollerTestEntities.jobExecution())
            .`when`(batchStarter).startJob(batchJob)

        fixture.greediness = 3
        fixture.poll()
        fixture.greediness = 1

        verify(batchJobService, times(1)).getAvailableBatchJobs(3)
        verify(batchStarter, times(3)).startJob(batchJob)
    }
}