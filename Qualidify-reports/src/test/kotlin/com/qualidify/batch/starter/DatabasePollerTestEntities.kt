package com.qualidify.batch.starter

import com.qualidify.model.entities.BatchJob
import com.qualidify.model.entities.BatchJobId
import com.qualidify.model.entities.BatchJobStatus
import com.qualidify.model.entities.BatchJobType
import org.mockito.Mockito
import org.springframework.batch.core.JobExecution
import java.util.*

object DatabasePollerTestEntities {

    fun jobExecution(): JobExecution = Mockito.mock(JobExecution::class.java)

    fun batchJob() = BatchJob(
        id = BatchJobId(UUID.randomUUID()),
        jobName = "TESTJOB",
        jobType = BatchJobType.JasperReportJob,
        parameters = "",
        status = BatchJobStatus.NEW,
        finishedDate = null
    )
}