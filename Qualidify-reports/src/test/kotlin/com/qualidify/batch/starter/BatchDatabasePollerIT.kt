package com.qualidify.batch.starter

import com.qualidify.batch.service.BatchJobServiceImpl
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.Duration


@ContextConfiguration(classes = [ScheduledMockConfiguration::class])
@ExtendWith(SpringExtension::class)
internal class BatchDatabasePollerIT {
    @Autowired
    lateinit var batchJobService: BatchJobServiceImpl

    @Autowired
    lateinit var batchStarter: BatchStarter

    @Autowired
    lateinit var fixture: BatchDatabasePoller

    @Test
    fun `Test databasePoller scheduling`() {
        val batchJob = DatabasePollerTestEntities.batchJob()

        doReturn(
            listOf(batchJob)
        ).`when`(batchJobService).getAvailableBatchJobs(1)

        doReturn(DatabasePollerTestEntities.jobExecution())
            .`when`(batchStarter).startJob(batchJob)

        await().atMost(Duration.ofSeconds(2))
            .untilAsserted {
                verify(batchJobService, atLeast(1)).getAvailableBatchJobs(1)
                verify(batchStarter, times(1)).startJob(batchJob)
            }
    }
}

@TestConfiguration
@EnableScheduling
open class ScheduledMockConfiguration {

    @Bean
    open fun batchJobService(): BatchJobServiceImpl = mock(BatchJobServiceImpl::class.java)

    @Bean
    open fun batchStarter(): BatchStarter = mock(BatchStarter::class.java)

    @Bean
    open fun databasePoller(): BatchDatabasePoller = BatchDatabasePoller(batchJobService(), batchStarter())
}

