package com.qualidify.batch.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.qualidify.batch.repository.BatchJobRepository
import com.qualidify.batch.starter.DatabasePollerTestEntities
import com.qualidify.model.entities.BatchJob
import com.qualidify.model.entities.BatchJobId
import com.qualidify.model.entities.BatchJobStatus
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentCaptor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.data.domain.PageRequest
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class BatchJobServiceImplTest {

    @Spy
    val objectMapper: ObjectMapper = ObjectMapper()

    @Mock
    lateinit var batchJobRepository: BatchJobRepository

    @InjectMocks
    lateinit var fixture: BatchJobServiceImpl

    @Test
    fun `Test find a batchjob by its id`() {
        val batchJob = DatabasePollerTestEntities.batchJob()
        val id = batchJob.id.unbox()

        doReturn(Optional.of(batchJob))
            .`when`(batchJobRepository).findById(BatchJobId(id))

        val jobByUuid = fixture.findById(id)
        val jobByString = fixture.findById(id.toString())

        assertThat(jobByUuid.get()).isEqualTo(batchJob)
        assertThat(jobByString.get()).isEqualTo(batchJob)

        verify(batchJobRepository, times(2)).findById(BatchJobId(id))
        verifyNoInteractions(objectMapper)
    }

    @Test
    fun `Test find not a batchjob by its id`() {
        val id = UUID.randomUUID()

        doReturn(Optional.empty<BatchJob>())
            .`when`(batchJobRepository).findById(BatchJobId(id))

        val jobByUuid = fixture.findById(id)
        val jobByString = fixture.findById(id.toString())

        assertThat(jobByUuid).isEmpty
        assertThat(jobByString).isEmpty

        assertThatThrownBy {
            fixture.findById("Not Transformable String")
        }.isInstanceOf(IllegalArgumentException::class.java)
            .hasMessage("Invalid UUID string: Not Transformable String")

        verify(batchJobRepository, times(2)).findById(BatchJobId(id))
        verifyNoInteractions(objectMapper)
    }

    @Test
    fun `Test poll for available jobs no founds`() {

        doReturn(emptyList<BatchJob>())
            .`when`(batchJobRepository).findByStatus(BatchJobStatus.NEW, PageRequest.of(0, 1))

        val jobs = fixture.getAvailableBatchJobs(1)

        assertThat(jobs).isEmpty()
        verify(batchJobRepository, times(1)).findByStatus(BatchJobStatus.NEW, PageRequest.of(0, 1))
    }

    @Test
    fun `Test poll for available jobs while finding one`() {
        val batchJob = DatabasePollerTestEntities.batchJob()

        doReturn(listOf(batchJob))
            .`when`(batchJobRepository).findByStatus(BatchJobStatus.NEW, PageRequest.of(0, 1))

        val jobs = fixture.getAvailableBatchJobs(1)

        assertThat(jobs.size).isOne()
        assertThat(jobs).contains(batchJob)

        verify(batchJobRepository, times(1)).findByStatus(BatchJobStatus.NEW, PageRequest.of(0, 1))
    }

    @Test
    fun `Test poll for multiple available jobs while finding one`() {
        val batchJob = DatabasePollerTestEntities.batchJob()

        doReturn(listOf(batchJob))
            .`when`(batchJobRepository).findByStatus(BatchJobStatus.NEW, PageRequest.of(0, 5))

        val jobs = fixture.getAvailableBatchJobs(5)

        assertThat(jobs.size).isOne()
        assertThat(jobs).contains(batchJob)

        verify(batchJobRepository, times(1)).findByStatus(BatchJobStatus.NEW, PageRequest.of(0, 5))
    }

    @Test
    fun `Test poll for multiple available jobs while finding two`() {
        val batchJob1 = DatabasePollerTestEntities.batchJob()
        val batchJob2 = DatabasePollerTestEntities.batchJob()

        doReturn(listOf(batchJob1, batchJob2))
            .`when`(batchJobRepository).findByStatus(BatchJobStatus.NEW, PageRequest.of(0, 5))

        val jobs = fixture.getAvailableBatchJobs(5)

        assertThat(jobs.size).isEqualTo(2)
        assertThat(jobs).contains(batchJob1, batchJob2)

        verify(batchJobRepository, times(1)).findByStatus(BatchJobStatus.NEW, PageRequest.of(0, 5))
    }

    @Test
    fun `Test creation of a simple BatchJob without parameters`() {
        `when`(batchJobRepository.save(any(BatchJob::class.java)))
            .thenAnswer { it.getArgument(0) }

        val batchJob = fixture.createBatchJob("TestJob")

        assertThat(batchJob.jobName).isEqualTo("TestJob")
        assertThat(batchJob.parameters).isEqualTo("")
        assertThat(batchJob.status).isEqualTo(BatchJobStatus.NEW)

        verify(batchJobRepository, times(1)).save(batchJob)
    }

    @Test
    fun `Test creation of a BatchJob with parameters`() {
        `when`(batchJobRepository.save(any(BatchJob::class.java)))
            .thenAnswer { it.getArgument(0) }

        val batchJob = fixture.createBatchJob(
            "TestJob",
            mapOf(
                "enemy" to "Satan",
                "friend1" to "Elifaz",
                "friend2" to "Bildad",
                "friend3" to " Sofar",
                "youngestFriend" to "Elihu"
            )
        )

        assertThat(batchJob.jobName).isEqualTo("TestJob")
        assertThat(batchJob.parameters).isEqualTo("{\"enemy\":\"Satan\",\"friend1\":\"Elifaz\",\"friend2\":\"Bildad\",\"friend3\":\" Sofar\",\"youngestFriend\":\"Elihu\"}")
        assertThat(batchJob.status).isEqualTo(BatchJobStatus.NEW)

        verify(batchJobRepository, times(1)).save(batchJob)
    }

    @Test
    fun `Test registering BatchJob as Running`() {
        val batchJob = DatabasePollerTestEntities.batchJob()
        val runningBatchJob = batchJob.apply {
            status = BatchJobStatus.RUN
        }

        val captor = ArgumentCaptor.forClass(BatchJob::class.java)

        `when`(batchJobRepository.save(captor.capture()))
            .thenAnswer { it.getArgument(0) }

        fixture.registerBatchJobInProgress(batchJob)

        assertThat(captor.value).isEqualTo(runningBatchJob)

        verify(batchJobRepository, times(1)).save(runningBatchJob)
    }
}
