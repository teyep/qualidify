package com.qualidify.batch.service

import com.qualidify.batch.repository.BatchJobArtifactRepository
import com.qualidify.batch.repository.BatchJobRepository
import com.qualidify.batch.repository.ReportTemplateRepository
import com.qualidify.batch.resource.ClasspathReportResourceRepository
import com.qualidify.model.entities.ReportTemplate
import com.qualidify.model.entities.ReportTemplateId
import net.sf.jasperreports.repo.RepositoryService
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.core.io.ClassPathResource
import java.util.*
import javax.sql.DataSource

@ExtendWith(MockitoExtension::class)
internal class ReportServiceImplTest {

    @Mock
    lateinit var reportTemplateRepository: ReportTemplateRepository

    @Mock
    lateinit var batchJobArtifactRepository: BatchJobArtifactRepository

    @Mock
    lateinit var batchJobRepository: BatchJobRepository

    @Spy
    var resourceLoaders: List<RepositoryService> = listOf(
        ClasspathReportResourceRepository("/reports/")
    )

    @Mock
    lateinit var dataSource: DataSource

    @InjectMocks
    lateinit var fixture: ReportServiceImpl

    @Test
    fun `Test load report by Id from the repository`() {
        val id = UUID.randomUUID()
        val expectedTemplate = reportTemplate(id)

        doReturn(Optional.of(expectedTemplate))
            .`when`(reportTemplateRepository).findById(ReportTemplateId(id))

        val optionalTemplate = fixture.loadReportTemplate(id)
        assertThat(optionalTemplate).isNotEmpty
        assertThat(optionalTemplate.get()).isEqualTo(expectedTemplate)

        verify(reportTemplateRepository, times(1))
            .findById(ReportTemplateId(id))
        verifyNoInteractions(dataSource)
    }

    @Test
    fun `Test load report by name from the repository`() {
        val expectedTemplate = reportTemplate(UUID.randomUUID())

        doReturn(Optional.of(expectedTemplate))
            .`when`(reportTemplateRepository).findReportTemplateByTemplateName("Test Reporttemplate")

        val optionalTemplate = fixture.loadReportTemplate("Test Reporttemplate")
        assertThat(optionalTemplate).isNotEmpty
        assertThat(optionalTemplate.get()).isEqualTo(expectedTemplate)

        verify(reportTemplateRepository, times(1))
            .findReportTemplateByTemplateName("Test Reporttemplate")
        verifyNoInteractions(dataSource)
    }


    @Test
    fun `Test running a JasperReport by ReportTemplate`() {
        val expectedTemplate = reportTemplate(UUID.randomUUID())

        doReturn(Optional.of(expectedTemplate))
            .`when`(reportTemplateRepository).findReportTemplateByTemplateName("Test Reporttemplate")

        //no connection needed for this report
        doReturn(null)
            .`when`(dataSource).connection

        val result = fixture.runReport(UUID.randomUUID(),"Test Reporttemplate", mapOf())

        assertThat(result).isExactlyInstanceOf(SuccessfullRunReportResult::class.java)
        assertThat((result as SuccessfullRunReportResult).templateName).isEqualTo("Test Reporttemplate")

        verify(reportTemplateRepository, times(1))
            .findReportTemplateByTemplateName("Test Reporttemplate")
        verify(dataSource, times(1)).connection

    }

    @Test
    fun `Test compiling a JasperReport and save a new template`() {
        assertThatThrownBy { fixture.compileAndSave() }
            .isInstanceOf(NotImplementedError::class.java)
    }

    fun reportTemplate(id: UUID) = ReportTemplate(
        id = id,
        templateName = "Test Reporttemplate",
        templateDescription = "Test Reporttemplate description",
        data = minimalJasperReport(),
        parameters = "",
        resources = setOf()
    )

    private fun minimalJasperReport(): ByteArray {
        val resource = ClassPathResource("reports/MinimalTest/MinimalTest.jasper")
        return resource.inputStream.readAllBytes()
    }
}