package com.qualidify.batch.processors

import com.qualidify.batch.service.BatchJobInfo
import com.qualidify.batch.service.BatchJobService
import com.qualidify.batch.service.BatchNotFoundException
import com.qualidify.core.log.LoggerDelegation
import org.springframework.batch.item.ItemReader

/**
 * Get a [BatchJobInfo] from the BatchJobService based on its id and execute it
 *
 * @author Marcel Pot
 * @since 1.0
 */
open class BatchJobReader(
    private val batchJobService: BatchJobService,
    val id: String?
) : ItemReader<BatchJobInfo> {

    private val logger by LoggerDelegation()

    override fun read(): BatchJobInfo {
        if (id == null) {
            throw BatchNotFoundException("Cannot find a batch without a JobId!")
        }

        logger.info("Starting id $id.")
        try {
            return batchJobService.findBatchJobInfoById(id)
        } catch (e: IllegalArgumentException) {
            throw BatchNotFoundException("Cannot find a batch with JobId $id")
        }
    }
}



