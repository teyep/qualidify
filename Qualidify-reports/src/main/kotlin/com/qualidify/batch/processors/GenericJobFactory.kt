package com.qualidify.batch.processors

import org.springframework.batch.core.Job
import org.springframework.batch.core.configuration.JobFactory

/**
 * Minimal JobFactory implementation for use with MapJobRegistry.
 *
 * @author Niels Visscher
 * @since 1.0
 */
data class GenericJobFactory(
    val job: Job,
    val name: String
) : JobFactory {
    override fun createJob(): Job {
        return job
    }

    override fun getJobName(): String {
        return name
    }
}