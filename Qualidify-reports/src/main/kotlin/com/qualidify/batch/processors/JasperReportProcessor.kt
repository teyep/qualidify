package com.qualidify.batch.processors

import com.qualidify.batch.service.BatchJobInfo
import com.qualidify.batch.service.ReportServiceImpl
import com.qualidify.batch.service.RunReportResult
import com.qualidify.core.log.LoggerDelegation
import org.springframework.batch.item.ItemProcessor

/**
 * Report Processor, takes inputparameters to create a Jasper Report
 *
 * @author Marcel Pot
 * @since 1.09
 */
open class JasperReportProcessor(val reportService: ReportServiceImpl) : ItemProcessor<BatchJobInfo, RunReportResult> {

    val logger by LoggerDelegation()

    override fun process(batchJobInfo: BatchJobInfo): RunReportResult {
        logger.info("Starting Report ${batchJobInfo.name} with parameters ${batchJobInfo.parameters}")
        var result = reportService.runReport(batchJobInfo.id, batchJobInfo.name, batchJobInfo.parameters.reportParameters)
        logger.info("Finishing Report ${batchJobInfo.name} with parameters ${batchJobInfo.parameters}")
        return result;
    }
}