package com.qualidify.batch.processors

import com.qualidify.batch.service.ReportServiceImpl
import com.qualidify.batch.service.RunReportResult
import org.springframework.batch.item.ItemWriter

/**
 * Write the results of the JasperReports back to the server
 *
 * @author Marcel Pot
 * @since 1.0
 */
open class ReportWriter(val reportService: ReportServiceImpl) : ItemWriter<RunReportResult> {

    override fun write(results: MutableList<out RunReportResult>) {
        results.forEach { write(it) }
    }

    fun write(result: RunReportResult) = reportService.mentionReportResult(result)

}