package com.qualidify.batch

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling


@SpringBootApplication
@EnableScheduling
open class QualidifyBatchApplication

fun main(args: Array<String>) {
    runApplication<QualidifyBatchApplication>(*args)
}