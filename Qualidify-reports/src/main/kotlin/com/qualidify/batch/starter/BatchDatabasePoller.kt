package com.qualidify.batch.starter

import com.qualidify.batch.service.BatchJobServiceImpl
import com.qualidify.core.log.LoggerDelegation
import com.qualidify.model.entities.BatchJob
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * A database poller, to look for jobs to execute
 *
 * @author Marcel Pot
 * @since 1.0
 */
@Component
open class BatchDatabasePoller(
    val batchJobService: BatchJobServiceImpl,
    val batchStarter: BatchStarter
) {


    val logger by LoggerDelegation()

    @Value("\${qualidify.batch.poller.greediness:1}")
    var greediness: Int = 1

    val suspended: Boolean = false

    @Scheduled(
        initialDelayString = "\${qualidify.batch.poller.initialDelay:1000}",
        fixedDelayString = "\${qualidify.batch.poller.fixedDelay:5000}"
    )
    fun poll() {
        if (!suspended) {
            val jobInfos = readJobsFromDatabase()
            jobInfos.forEach {
                startJob(it)
            }
        }
    }

    private fun readJobsFromDatabase(): Collection<BatchJob> {
        logger.trace("Reading data from the database.")
        return batchJobService.getAvailableBatchJobs(greediness)
    }

    private fun startJob(job: BatchJob) {
        logger.info("Found Job ${job.id}.")
        batchJobService.registerBatchJobInProgress(job)
        batchStarter.startJob(job)
        batchJobService.registerBatchJobDone(job)
        logger.info("Job executed")
    }
}