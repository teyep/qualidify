package com.qualidify.batch.starter

import com.fasterxml.jackson.databind.ObjectMapper
import com.qualidify.core.log.LoggerDelegation
import com.qualidify.model.entities.BatchJob
import org.springframework.batch.core.*
import org.springframework.batch.core.configuration.JobFactory
import org.springframework.batch.core.configuration.JobRegistry
import org.springframework.batch.core.launch.JobLauncher
import java.util.*

/**
 * A service for starting BatchJobs
 *
 * @author Marcel Pot
 * @since 1.0
 */
open class BatchStarter(
    private val jobLauncher: JobLauncher,
    private val jobRegistry: JobRegistry
) : JobExecutionListener {

    private val objectMapper: ObjectMapper = ObjectMapper()

    private val logger by LoggerDelegation()
    private val busyTasks = mutableListOf<BatchJob>()

    fun registerFactory(factory: JobFactory) {
        jobRegistry.register(factory)
    }

    /**
     * Start a new Job based on the given [JobStartInfo]
     * @param startInfo the object containing the job to start and the parameters belonging to this job
     * @return a jobExecution
     */
    fun startJob(batchJob: BatchJob): JobExecution {
        val job = determineJob(batchJob)
        val parameters = determineJobParameters(batchJob.id.unbox(), batchJob.parameters)
        //run job
        logger.info("Scheduling job ${job.name} with parameters $parameters.")
        val jobExecution = jobLauncher.run(job, parameters)
        return jobExecution
    }

    private fun determineJob(batchJob: BatchJob) = jobRegistry.getJob(batchJob.jobType.toString())

    private fun determineJobParameters(id: UUID, parameterString: String): JobParameters {
        val parameterMap = objectMapper.readValue(parameterString, ReportBatchJobParameters::class.java)
        val jobParameters = JobParametersBuilder()
        jobParameters.addParameter("id", determineJobParameter(id.toString()))
        parameterMap.reportParameters.forEach { jobParameters.addParameter(it.key, determineJobParameter(it.value)) }
        return jobParameters.toJobParameters()
    }

    private fun determineJobParameter(parameter: Any): JobParameter {
        logger.trace("Try to add parameter $parameter")
        return when (parameter) {
            is Double -> JobParameter(parameter)
            is String -> JobParameter(parameter)
            is Int -> JobParameter(parameter.toLong())
            is Long -> JobParameter(parameter)
            is Date -> JobParameter(parameter)
            is UUID -> JobParameter(parameter.toString())
            else -> throw BatchParameterException("Cannot cast parameter $parameter to a batchparameter.")
        }
    }

    override fun beforeJob(jobExecution: JobExecution) {
        logger.info("Starting job ${jobExecution.jobInstance}.")
    }

    override fun afterJob(jobExecution: JobExecution) {
        logger.info("Finishing ${jobExecution.jobInstance}.")
    }
}

/**
 * Exception indicating a problem with the conversion of the batchparameters
 * @author Marcel Pot
 * @since 1.0
 */
class BatchParameterException(msg: String, throwable: Throwable? = null) : RuntimeException(msg, throwable)

