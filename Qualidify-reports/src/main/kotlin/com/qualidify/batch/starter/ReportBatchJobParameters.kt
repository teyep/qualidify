package com.qualidify.batch.starter

import java.util.*

/**
 * Structure of the report parameters, for use with deserialization.
 *
 * @author Niels Visscher
 */
 data class ReportBatchJobParameters(
    val reportTemplate: UUID?,
    val reportParameters: HashMap<String, Any>
 ) {
     constructor() : this(null, HashMap<String, Any>())
 }

