package com.qualidify.batch.repository

import com.qualidify.model.entities.BatchJobArtifact
import com.qualidify.model.entities.BatchJobId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BatchJobArtifactRepository : JpaRepository<BatchJobArtifact, BatchJobId>
