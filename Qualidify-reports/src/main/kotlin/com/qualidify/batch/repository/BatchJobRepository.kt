package com.qualidify.batch.repository

import com.qualidify.model.entities.BatchJob
import com.qualidify.model.entities.BatchJobId
import com.qualidify.model.entities.BatchJobStatus
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Lock
import org.springframework.stereotype.Repository
import javax.persistence.LockModeType

@Repository
interface BatchJobRepository : JpaRepository<BatchJob, BatchJobId> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    fun findByStatus(status: BatchJobStatus, pageable: Pageable): List<BatchJob>

}