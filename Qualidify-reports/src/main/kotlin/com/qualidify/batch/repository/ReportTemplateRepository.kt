package com.qualidify.batch.repository

import com.qualidify.model.entities.ReportTemplate
import com.qualidify.model.entities.ReportTemplateId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ReportTemplateRepository : JpaRepository<ReportTemplate, ReportTemplateId> {

    fun findReportTemplateByTemplateName(templateName: String): Optional<ReportTemplate>
}