package com.qualidify.batch.configuration

import com.qualidify.batch.processors.BatchJobReader
import com.qualidify.batch.processors.GenericJobFactory
import com.qualidify.batch.processors.JasperReportProcessor
import com.qualidify.batch.processors.ReportWriter
import com.qualidify.batch.service.BatchJobInfo
import com.qualidify.batch.service.BatchJobService
import com.qualidify.batch.service.ReportServiceImpl
import com.qualidify.batch.service.RunReportResult
import com.qualidify.batch.starter.BatchStarter
import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepScope
import org.springframework.batch.core.configuration.support.MapJobRegistry
import org.springframework.batch.core.launch.JobLauncher
import org.springframework.batch.core.launch.support.RunIdIncrementer
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Component


@Configuration
@EnableJpaAuditing
@EnableBatchProcessing
@EntityScan("com.qualidify.model.entities")
@EnableJpaRepositories(basePackages = ["com.qualidify.batch.repository"])
@Suppress("SpringJavaInjectionPointsAutowiringInspection")
open class BatchConfiguration(
    val jobBuilderFactory: JobBuilderFactory,
    val stepBuilderFactory: StepBuilderFactory
) {

    @Configuration
    open inner class JasperReportJob {
        @Bean
        open fun jasperJobRegistry(): QualidifyJobRegistry {
            return QualidifyJobRegistry()
        }

        @Bean
        open fun jasperBatchStarter(
            jobLauncher: JobLauncher,
            jobRegistry: QualidifyJobRegistry
        ) : BatchStarter {
            return BatchStarter(jobLauncher, jobRegistry)
        }

        @Bean
        open fun processJasperReportJob(
            jasperReportStep: Step,
            batchStarter: BatchStarter,
            jobRegistry: QualidifyJobRegistry): Job {
            val job = jobBuilderFactory["JasperReportJob"]  // TODO: fix in QUAL-435
                .incrementer(RunIdIncrementer())
                .listener(batchStarter)
                .flow(jasperReportStep)
                .end().build()
            jobRegistry.register(GenericJobFactory(job, "JasperReportJob"))  // TODO: fix in QUAL-435
            return job
        }

        @Bean
        open fun jasperReportStep(
            batchJobReader: BatchJobReader,
            jasperReportProcessor: JasperReportProcessor,
            reportWriter: ReportWriter
        ): Step {
            return stepBuilderFactory["JasperReportStep_1"]
                .chunk<BatchJobInfo, RunReportResult>(1)
                .reader(batchJobReader)
                .processor(jasperReportProcessor)
                .writer(reportWriter)
                .build()
        }

        @Bean
        @StepScope
        open fun batchJobReader(batchJobService: BatchJobService, @Value("#{jobParameters['id']}") id: String?) =
            BatchJobReader(
                batchJobService = batchJobService,
                id = id
            )


        @Bean
        open fun jasperReportProcessor(reportService: ReportServiceImpl) =
            JasperReportProcessor(reportService)

        @Bean
        open fun reportWriter(reportService: ReportServiceImpl) = ReportWriter(reportService)

    }
}
open class QualidifyJobRegistry : MapJobRegistry() {
}

@Component
open class Runner(
    private val jobLauncher: JobLauncher,
    private val job: Job
) : ApplicationRunner {

    override fun run(args: ApplicationArguments?) {
    }
}
