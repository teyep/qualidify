package com.qualidify.batch.service

import com.qualidify.batch.repository.BatchJobArtifactRepository
import com.qualidify.batch.repository.ReportTemplateRepository
import com.qualidify.batch.resource.TemplateReportScopedResourceRepository
import com.qualidify.model.entities.BatchJobArtifact
import com.qualidify.model.entities.BatchJobId
import com.qualidify.model.entities.ReportTemplate
import com.qualidify.model.entities.ReportTemplateId
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.SimpleJasperReportsContext
import net.sf.jasperreports.engine.util.JRLoader
import net.sf.jasperreports.repo.RepositoryService
import org.springframework.stereotype.Service
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.util.*
import javax.sql.DataSource
import javax.transaction.Transactional

@Service
@Transactional
open class ReportServiceImpl(
    private val reportTemplateRepository: ReportTemplateRepository,
    private val batchJobArtifactRepository: BatchJobArtifactRepository,
    private val resourceLoader: List<RepositoryService>,
    private val dataSource: DataSource
) : ReportService {

    override fun loadReportTemplate(id: UUID) = reportTemplateRepository.findById(ReportTemplateId(id))

    override fun loadReportTemplate(templateName: String) =
        reportTemplateRepository.findReportTemplateByTemplateName(templateName)

    override fun runReport(batchJobId: UUID, templateName: String, parameters: Map<String, Any?>): RunReportResult {
        val reportTemplate = loadReportTemplate(templateName).get()

        val report: JasperReport =
            JRLoader.loadObject(ByteArrayInputStream(reportTemplate.templateData) as InputStream) as JasperReport

        val context = addExtensions(reportTemplate)
        val fillManager = JasperFillManager.getInstance(context)

        val jasperPrint = fillManager.fill(report, parameters.toMutableMap(), dataSource.connection)

        val outStream = ByteArrayOutputStream()
        JasperExportManager.exportReportToPdfStream(jasperPrint, outStream)

        return SuccessfullRunReportResult(batchJobId, templateName, parameters, outStream)
    }

    override fun mentionReportResult(result: RunReportResult) {
        if(result is SuccessfullRunReportResult) {
            batchJobArtifactRepository.save(
                BatchJobArtifact(
                    BatchJobId(result.batchJobId),
                    0,
                    "application/pdf",
                    result.outputStream!!.toByteArray(),
                    Date()
                )
            )
        }
    }

    override fun compileAndSave(): ReportTemplate {
        TODO("Not yet implemented")
    }

    private fun addExtensions(reportTemplate: ReportTemplate): SimpleJasperReportsContext {
        val context = SimpleJasperReportsContext()
        val combinedResourceLoaders = resourceLoader.toMutableList()
        combinedResourceLoaders.add(TemplateReportScopedResourceRepository(reportTemplate))
        context.setExtensions(RepositoryService::class.java, combinedResourceLoaders)
        return context
    }
}


sealed interface RunReportResult

data class FailingRunReportResult(
    val batchJobId: UUID,
    val templateName: String,
    val parameters: Map<String, Any?>,
    val error: String
) : RunReportResult

data class SuccessfullRunReportResult(
    val batchJobId: UUID,
    val templateName: String,
    val parameters: Map<String, Any?>,
    val outputStream: ByteArrayOutputStream? = null,
) : RunReportResult