package com.qualidify.batch.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.qualidify.batch.repository.BatchJobRepository
import com.qualidify.batch.starter.ReportBatchJobParameters
import com.qualidify.model.entities.BatchJob
import com.qualidify.model.entities.BatchJobId
import com.qualidify.model.entities.BatchJobStatus
import com.qualidify.model.entities.BatchJobType
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
open class BatchJobServiceImpl(
    private val batchJobRepository: BatchJobRepository,
) : BatchJobService {

    private val objectMapper: ObjectMapper = ObjectMapper()
    override fun findBatchJobInfoById(id: String) =
        findById(id)
            .map {
                BatchJobInfo(
                    id = it.id.unbox(),
                    job = it.jobName,
                    name = it.jobName,
                    parameters = convertParametersFromString(it.parameters)
                )
            }
            .orElseThrow { BatchNotFoundException("Cannot find a batch with JobId $id") }


    override fun findById(id: String) =
        batchJobRepository.findById(BatchJobId(UUID.fromString(id)))

    override fun findById(id: UUID) =
        batchJobRepository.findById(BatchJobId(id))

    override fun getAvailableBatchJobs(maxJobs: Int): List<BatchJob> {
        val pageable = PageRequest.of(0, maxJobs)
        return batchJobRepository.findByStatus(BatchJobStatus.NEW, pageable)
    }

    override fun createBatchJob(jobName: String, parameters: Map<String, Any?>) =
        createBatchJob(jobName, convertParametersFromMap(parameters))

    private fun convertParametersFromMap(parameters: Map<String, Any?>) =
        objectMapper.writeValueAsString(parameters)

    private fun convertParametersFromString(json: String) =
        objectMapper.readValue(json, ReportBatchJobParameters::class.java) as ReportBatchJobParameters

    override fun createBatchJob(jobName: String, parameters: String): BatchJob {
        val batchJob = BatchJob(
            id = BatchJobId(UUID.randomUUID()),
            jobType = BatchJobType.JasperReportJob,
            jobName = jobName,
            parameters = parameters,
            status = BatchJobStatus.NEW
        )
        return batchJobRepository.save(batchJob)
    }

    private fun saveWithNewStatus(job: BatchJob, newStatus: BatchJobStatus) {
        val inProgressJob = job.apply {
            status = newStatus
        }
        batchJobRepository.save(inProgressJob)
    }

    override fun registerBatchJobInProgress(job: BatchJob) {
        saveWithNewStatus(job, BatchJobStatus.RUN)
    }

    override fun registerBatchJobDone(job: BatchJob) {
        saveWithNewStatus(job, BatchJobStatus.DONE)
    }

    override fun registerBatchJobError(job: BatchJob) {
        saveWithNewStatus(job, BatchJobStatus.ERROR)
    }

}