package com.qualidify.batch.service

import com.qualidify.batch.starter.ReportBatchJobParameters
import com.qualidify.model.entities.ReportTemplate
import java.util.*

/**
 * Service for all reporting activities as
 * - Loading Report Templates
 * - Running Reports
 * - Saving the result to the repository
 *
 * @author Niels Visscher
 * @author Marcel Pot
 * @since 1.0
 */
interface ReportService {

    /**
     * Load a ReportTemplate based on is id
     * @param[id] the identity of the ReportTemplate
     * @return an optional containing the reporttemplate if found
     */
    fun loadReportTemplate(id: UUID): Optional<ReportTemplate>

    /**
     * Load a ReportTemplate based on is name
     * @param[templateName] the name of the ReportTemplate
     * @return an optional containing the reporttemplate if found
     */
    fun loadReportTemplate(templateName: String): Optional<ReportTemplate>

    /**
     * Run a Report
     * @param[batchJobId] the ID of the batch job, to be returned with the RunReportResult
     * @param[templateName] the name of the ReportTemplate
     * @param[parameters] the parameters for the report
     * @return a reportResult
     */
    fun runReport(batchJobId: UUID, templateName: String, parameters: Map<String, Any?>): RunReportResult

    /**
     * Report the ReportResult
     * @param[result] the result of the runreport
     */
    fun mentionReportResult(result: RunReportResult)

    /**
     * Compile a JasperXml Report and save teh reportTemplate
     */
    fun compileAndSave(): ReportTemplate
}

data class BatchJobInfo(
    val id: UUID,
    val job: String,
    val name: String,
    val parameters: ReportBatchJobParameters

)

class BatchNotFoundException(msg: String? = "", cause: Throwable? = null) : RuntimeException(msg, cause)

