package com.qualidify.batch.service

import com.qualidify.model.entities.BatchJob
import java.util.*

/**
 * Service for acquiring new jobs and keeping track of their progress
 *
 * @author Marcel Pot
 * @since 1.0
 */
interface BatchJobService {

    /**
     * find a BatchJob by its id (uuid format)
     * @param[id] the id in uuid format
     * @return a BatchJobInfo if available
     */
    fun findBatchJobInfoById(id: String): BatchJobInfo

    /**
     * find a BatchJob by its id (string format)
     * @param[id] the id in string format
     * @return a BatchJob if available
     */
    fun findById(id: String): Optional<BatchJob>

    /**
     * find a BatchJob by its id (uuid format)
     * @param[id] the id in uuid format
     * @return a BatchJob if available
     */
    fun findById(id: UUID): Optional<BatchJob>

    /**
     * Get a number of jobs waiting to be pickup
     * @param[maxJobs] the number of jobs to maximaly pickup. Default value is one
     * @return a list of BatchJobs to pickup
     */
    fun getAvailableBatchJobs(maxJobs: Int = 1): List<BatchJob>

    /**
     * Create a new BatchJob
     * @param[jobName] the name of the job to execute
     * @param[parameters] a map with extra parameters to execute the job
     * @return a new BatchJob
     */
    fun createBatchJob(jobName: String, parameters: Map<String, Any?>): BatchJob

    /**
     * Create a new BatchJob
     * @param[jobName] the name of the job to execute
     * @param[parameters] a json String containing the parameters
     * @return a new BatchJob
     */
    fun createBatchJob(jobName: String, parameters: String = ""): BatchJob

    /**
     * Register a job as in progress
     * @param[job] the BatchJob to register in progress
     */
    fun registerBatchJobInProgress(job: BatchJob)

    /**
     * Register a job as done
     * @param[job] the BatchJob to register as done
     */
    fun registerBatchJobDone(job: BatchJob)

    /**
     * Register a job as having resulted in an error
     * @param[job] the BatchJob to register as having resulted in an error
     */
    fun registerBatchJobError(job: BatchJob)

}