package com.qualidify.batch.resource

import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.util.JRLoader
import net.sf.jasperreports.repo.InputStreamResource
import net.sf.jasperreports.repo.ReportResource
import net.sf.jasperreports.repo.RepositoryService
import net.sf.jasperreports.repo.Resource
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Repository

/**
 * Custom repository service for use by JasperReports, returns resources from the entity's bound ElementCollection instead of the local filesystem
 *
 * @author Niels Visscher
 * @since 1.0
 */
@Repository
open class ClasspathReportResourceRepository(
    @Value("\${qualidify.reports.classpathdirectory:reports-common}") private val directory: String
) : RepositoryService {

    private fun isSubreportName(resourceName: String): Boolean {
        return resourceName.endsWith(".jasper")
    }

    override fun getResource(name: String): Resource? {
        val resource = ClassPathResource(directory + "/" + name)

        if (isSubreportName(name)) {
            val reportResource = ReportResource()
            reportResource.setReport(JRLoader.loadObject(resource.inputStream) as JasperReport)
            return reportResource
        } else {
            val output = InputStreamResource()
            output.setInputStream(resource.inputStream)
            return output
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <K : Resource?> getResource(name: String, clazz: Class<K>?): K {
        return getResource(name) as K
    }

    override fun saveResource(p0: String?, p1: Resource?) {
        TODO("Not yet implemented")
    }
}