package com.qualidify.batch.resource

import com.qualidify.model.entities.ReportTemplate
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.util.JRLoader
import net.sf.jasperreports.repo.InputStreamResource
import net.sf.jasperreports.repo.ReportResource
import net.sf.jasperreports.repo.RepositoryService
import net.sf.jasperreports.repo.Resource
import java.io.ByteArrayInputStream
import java.io.InputStream

/**
 * Custom repository service for use by JasperReports, returns resources from the entity's bound ElementCollection instead of the local filesystem
 *
 * @author Niels Visscher
 * @since 1.0
 */
class TemplateReportScopedResourceRepository(
    private val template: ReportTemplate
) : RepositoryService {

    override fun getResource(name: String): Resource? {
        template.resources.forEach {
            if (it.resourceName.equals(name)) {
                if (isSubreportName(name)) {
                    return subreportResourceFromInputStream(ByteArrayInputStream(it.resourceData))
                } else {
                    return inputStreamResourceFromInputStream(ByteArrayInputStream(it.resourceData))
                }
            }
        }
        return null
    }

    private fun inputStreamResourceFromInputStream(stream: InputStream): InputStreamResource {
        val output = InputStreamResource()
        output.setInputStream(stream)
        return output
    }

    private fun subreportResourceFromInputStream(stream: InputStream): ReportResource {
        val reportResource = ReportResource()
        reportResource.setReport(JRLoader.loadObject(stream) as JasperReport)
        return reportResource
    }

    private fun isSubreportName(resourceName: String): Boolean {
        return resourceName.endsWith(".jasper")
    }

    @Suppress("UNCHECKED_CAST")
    override fun <K : Resource?> getResource(name: String, clazz: Class<K>): K {
        return getResource(name) as K
    }

    override fun saveResource(name: String, resource: Resource) {
        TODO("Not yet implemented")
    }
}