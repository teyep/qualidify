package com.qualidify.batch.resource

import net.sf.jasperreports.repo.RepositoryService
import net.sf.jasperreports.repo.Resource

/**
 * Custom repository service for use by JasperReports, returns resources from the entity's bound ElementCollection instead of the local filesystem
 *
 * @author Niels Visscher
 * @since 1.0
 */
//@Repository
class DatabaseReportResourceRepository : RepositoryService {
    override fun getResource(name: String): Resource {
        TODO("Not yet implemented")
    }

    @Suppress("UNCHECKED_CAST")
    override fun <K : Resource?> getResource(name: String, clazz: Class<K>): K {
        return getResource(name) as K
    }

    override fun saveResource(p0: String?, p1: Resource?) {
        TODO("Not yet implemented")
    }
}